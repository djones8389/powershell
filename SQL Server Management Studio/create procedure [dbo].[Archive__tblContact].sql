USE [ODS_RAQ]
GO

/****** Object:  StoredProcedure [dbo].[Archive__tblContact]    Script Date: 13/12/2018 17:22:01 ******/
DROP PROCEDURE [dbo].[Archive__tblContact]
GO

/****** Object:  StoredProcedure [dbo].[Archive__tblContact]    Script Date: 13/12/2018 17:22:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


	
		create procedure [dbo].[Archive__tblContact] (
			@Date date,
			@updatecount int output,
			@NoOfDailyMonths int,
			@NoOfMonthlyQuarters int,
			@NoOfQuarterlyYears int
		) AS
	
		set @updatecount = 0
		
		insert into [ODS_RAQ].[dbo].[Archived__tblContact] (
			ods_DateFrom, ods_DateTo, 
			[ParentTable],[ParentKey],[ContactType],[ContactName],[AccountID],[PayeeName],[Address1],[Address2],[Address3],[Address4],[Postcode],[Salutation],[Telephone],[Fax],[Email],[TheirRef],[ExtraInfo1],[ExtraInfo2],[ExtraInfo3],[ExtraInfo4],[ExtraInfo5],[Rowguid],[ContactID]
		)
		select ods_DateFrom, ods_DateTo, [ParentTable],[ParentKey],[ContactType],[ContactName],[AccountID],[PayeeName],[Address1],[Address2],[Address3],[Address4],[Postcode],[Salutation],[Telephone],[Fax],[Email],[TheirRef],[ExtraInfo1],[ExtraInfo2],[ExtraInfo3],[ExtraInfo4],[ExtraInfo5],[Rowguid],[ContactID]
		from [ODS_RAQ].[dbo].[tblContact]
		where datediff(year, ods_DateFrom, ods_DateTo) = 0
			and datediff(year, ods_DateFrom, @Date) > @NoOfQuarterlyYears
			and ods_DateTo <> '31 Dec 2999'

		delete 
		from [ODS_RAQ].[dbo].[tblContact]
		where datediff(year, ods_DateFrom, ods_DateTo) = 0
			and datediff(year, ods_DateFrom, @Date) > @NoOfQuarterlyYears
			and ods_DateTo <> '31 Dec 2999'
		
		set @updatecount = @updatecount + @@ROWCOUNT
		
		insert into [ODS_RAQ].[dbo].[Archived__tblContact] (
			ods_DateFrom, ods_DateTo, 
			[ParentTable],[ParentKey],[ContactType],[ContactName],[AccountID],[PayeeName],[Address1],[Address2],[Address3],[Address4],[Postcode],[Salutation],[Telephone],[Fax],[Email],[TheirRef],[ExtraInfo1],[ExtraInfo2],[ExtraInfo3],[ExtraInfo4],[ExtraInfo5],[Rowguid],[ContactID]
		)
		select ods_DateFrom, ods_DateTo, [ParentTable],[ParentKey],[ContactType],[ContactName],[AccountID],[PayeeName],[Address1],[Address2],[Address3],[Address4],[Postcode],[Salutation],[Telephone],[Fax],[Email],[TheirRef],[ExtraInfo1],[ExtraInfo2],[ExtraInfo3],[ExtraInfo4],[ExtraInfo5],[Rowguid],[ContactID]
		from [ODS_RAQ].[dbo].[tblContact]
		where datediff(quarter, ods_DateFrom, ods_DateTo) = 0
			and datediff(quarter, ods_DateFrom, @Date) > @NoOfMonthlyQuarters
			and ods_DateTo <> '31 Dec 2999'
	
		delete 
		from [ODS_RAQ].[dbo].[tblContact]
		where datediff(quarter, ods_DateFrom, ods_DateTo) = 0
			and datediff(quarter, ods_DateFrom, @Date) > @NoOfMonthlyQuarters
			and ods_DateTo <> '31 Dec 2999'
		
		set @updatecount = @updatecount + @@ROWCOUNT
		
		insert into [ODS_RAQ].[dbo].[Archived__tblContact] (
			ods_DateFrom, ods_DateTo, 
			[ParentTable],[ParentKey],[ContactType],[ContactName],[AccountID],[PayeeName],[Address1],[Address2],[Address3],[Address4],[Postcode],[Salutation],[Telephone],[Fax],[Email],[TheirRef],[ExtraInfo1],[ExtraInfo2],[ExtraInfo3],[ExtraInfo4],[ExtraInfo5],[Rowguid],[ContactID]
		)
		select ods_DateFrom, ods_DateTo, [ParentTable],[ParentKey],[ContactType],[ContactName],[AccountID],[PayeeName],[Address1],[Address2],[Address3],[Address4],[Postcode],[Salutation],[Telephone],[Fax],[Email],[TheirRef],[ExtraInfo1],[ExtraInfo2],[ExtraInfo3],[ExtraInfo4],[ExtraInfo5],[Rowguid],[ContactID]
		from [ODS_RAQ].[dbo].[tblContact]
		where datediff(month, ods_DateFrom, ods_DateTo) = 0
			and datediff(month, ods_DateFrom, @Date) > @NoOfDailyMonths
			and ods_DateTo <> '31 Dec 2999'
				
		delete
		from [ODS_RAQ].[dbo].[tblContact]
		where datediff(month, ods_DateFrom, ods_DateTo) = 0
			and datediff(month, ods_DateFrom, @Date) > @NoOfDailyMonths
			and ods_DateTo <> '31 Dec 2999'
		
		set @updatecount = @updatecount + @@ROWCOUNT
		
	
GO


