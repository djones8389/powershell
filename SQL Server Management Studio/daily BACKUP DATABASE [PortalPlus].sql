--07:19, 25.3 GB

BACKUP DATABASE [PortalPlus] TO  DISK = N'\\hiscox.com\backup\ReleaseDump\PortalPlus\PortalPlus_2018-09-25.bak' WITH  COPY_ONLY
	, FORMAT, INIT, NAME = N'PortalPlus-Full Database Backup', COMPRESSION,  STATS = 10;

/*
IF EXISTS (
SELECT 1
from sys.databases
where  db_name(database_id) = 'PortalPlus'
	and recovery_model_desc = 'FULL'
)
ALTER DATABASE [PortalPlus] SET RECOVERY SIMPLE WITH NO_WAIT;
*/