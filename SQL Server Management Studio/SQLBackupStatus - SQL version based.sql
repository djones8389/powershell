declare @test nvarchar(max)='';

if  (substring(cast(SERVERPROPERTY('ProductVersion') as nvarchar(10)),0,3) >= 11)

select @test = 

'DELETE FROM [DBA_CDR].[SQL_Inventory].[dbo].[SQLBackupStatus]
WHERE ServerInstance= @@SERVERNAME

INSERT INTO [DBA_CDR].[SQL_Inventory].[dbo].[SQLBackupStatus]
	([ServerInstance]
           ,[Database]
           ,[LastCommvaultFull]
           ,[LastTran]
           ,[LastDiff]
           ,[PopulatedDate]
           ,[RecoveryMode]
           ,[DBCreationTime]
           ,[DBStatus]
		   ,[AGRole]
		   ,[Is_Read_Only]
)

SELECT @@SERVERNAME [ServerInstance]
  , d.name as [Database]
  ,LastCommvaultFull
  , LastTran
  , LastDiff
  , CapturedDate=GETDATE()
  , RecoveryMode=DATABASEPROPERTYEX(d.name, ''Recovery'')
  , CreationTime=d.crdate, Status=DATABASEPROPERTYEX(d.name, ''Status'')
  , hars.role_desc
  , case when d.status = ''1073808392'' then 1 else 0 end as [Is_Read_Only]
from master.dbo.sysdatabases d 
inner join sys.databases s on s.name = d.name
left JOIN sys.dm_hadr_availability_replica_states hars ON s.replica_id = hars.replica_id
left outer join
(select database_name
	, LastCommvaultFull=max(backup_finish_date)
    from msdb.dbo.backupset
    where type = ''D'' and backup_finish_date <= getdate() and name = ''CommVault Galaxy Backup''
    group by database_name
 ) b
on d.name = b.database_name
left outer join
 (select database_name
	, LastTran=max(backup_finish_date)
    from msdb.dbo.backupset
    where type =''L'' and backup_finish_date <= getdate()
    group by database_name
 ) c
on d.name = c.database_name
left outer join
 (select database_name
	, LastDiff=max(backup_finish_date)
    from msdb.dbo.backupset
    where type =''I'' and backup_finish_date <= getdate()
    group by database_name
 ) e
on d.name = e.database_name

 where d.name <> ''tempdb'' 
	 and d.name <> ''model''
	 and DATABASEPROPERTYEX(d.name, ''Status'') = ''ONLINE''
order by [LastCommvaultFull]'


else
	select @test = 

'DELETE FROM [DBA_CDR].[SQL_Inventory].[dbo].[SQLBackupStatus]
WHERE ServerInstance= @@SERVERNAME

INSERT INTO [DBA_CDR].[SQL_Inventory].[dbo].[SQLBackupStatus]
	([ServerInstance]
           ,[Database]
           ,[LastCommvaultFull]
           ,[LastTran]
           ,[LastDiff]
           ,[PopulatedDate]
           ,[RecoveryMode]
           ,[DBCreationTime]
           ,[DBStatus]
		   ,[AGRole]
		   ,[Is_Read_Only]
)

SELECT @@SERVERNAME [ServerInstance]
  , d.name as [Database]
  ,LastCommvaultFull
  , LastTran
  , LastDiff
  , CapturedDate=GETDATE()
  , RecoveryMode=DATABASEPROPERTYEX(d.name, ''Recovery'')
  , CreationTime=d.crdate, Status=DATABASEPROPERTYEX(d.name, ''Status'')
  , NULL
  , case when d.status = ''1073808392'' then 1 else 0 end as [Is_Read_Only]
from master.dbo.sysdatabases d 
left outer join
(select database_name
	, LastCommvaultFull=max(backup_finish_date)
    from msdb.dbo.backupset
    where type = ''D'' and backup_finish_date <= getdate() and name = ''CommVault Galaxy Backup''
    group by database_name
 ) b
on d.name = b.database_name
left outer join
 (select database_name
	, LastTran=max(backup_finish_date)
    from msdb.dbo.backupset
    where type =''L'' and backup_finish_date <= getdate()
    group by database_name
 ) c
on d.name = c.database_name
left outer join
 (select database_name
	, LastDiff=max(backup_finish_date)
    from msdb.dbo.backupset
    where type =''I'' and backup_finish_date <= getdate()
    group by database_name
 ) e
on d.name = e.database_name

 where d.name <> ''tempdb'' 
	 and d.name <> ''model''
	 and DATABASEPROPERTYEX(d.name, ''Status'') = ''ONLINE''
order by [LastCommvaultFull]'



exec (@test)

