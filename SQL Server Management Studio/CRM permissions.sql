use [master];

if not exists (select 1 from sys.syslogins where name = 'NONPROD\BUD_DB_OWNER')
	CREATE LOGIN [NONPROD\BUD_DB_OWNER] FROM WINDOWS WITH DEFAULT_DATABASE=[master]
	
	GRANT ALTER ANY AVAILABILITY GROUP TO [NONPROD\BUD_DB_OWNER];
	ALTER SERVER ROLE [dbcreator] ADD MEMBER [NONPROD\BUD_DB_OWNER]
	GO

use [LM_MSCRM];

if not exists (select 1 from sys.database_principals where name = 'NONPROD\BUD_DB_OWNER')
		begin
		create user [NONPROD\BUD_DB_OWNER] for login [NONPROD\BUD_DB_OWNER]
			exec sp_addrolemember 'db_backupoperator','NONPROD\BUD_DB_OWNER'
		end
	else 
		begin
			exec sp_addrolemember 'db_backupoperator','NONPROD\BUD_DB_OWNER'
		end

--Grant permissions