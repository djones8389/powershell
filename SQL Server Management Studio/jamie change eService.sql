DECLARE
	@Type AS VARCHAR(10) = 'IMs & SRs',
	@Queue AS VARCHAR(2000) = 'INF AD and Messaging'

IF (@Type = 'IMs & SRs')
	SELECT 
		fol.eFolderName AS [Ref],
		CASE	
			WHEN LEN(AssignedTo) > 0 THEN
				ISNULL((SELECT top 1 er_fullname FROM [Ework].[ework].[er_eServiceDesk] WHERE er_defaultTeam = 'yes' AND eUserName = (replace(AssignedTo, 'hiscox\', ''))),'')
			ELSE
				'Team'
		END AS Assignee
	FROM 
		[Ework].ework.eFolder fol
		INNER JOIN [Ework].ework.Incident_Management inc ON fol.eFolderID = inc.EFOLDERID
	WHERE 
		inc.SupportTeam IN (SELECT ParamValue FROM [Ework_LPJ].[dbo].[SplitMultiParamList] (@Queue, ','))
		AND fol.eFolderName LIKE 'IM%'
		AND inc.[Status] IN ('Open','On Hold')
		AND (inc.ProjectReference = '' OR inc.ProjectReference IS NULL)
		AND NOT fol.eStageName IN ('Declined','For Approval')

	UNION

	SELECT 
		fol.eFolderName AS [Ref],		
		CASE	
			WHEN LEN(AssignedTo) > 0 THEN
				ISNULL((SELECT top 1 er_fullname FROM [Ework].[ework].[er_eServiceDesk] WHERE er_defaultTeam = 'yes' AND eUserName = (replace(AssignedTo, 'hiscox\', ''))),'')
			ELSE
				'Team'
		END AS Assignee
		
	FROM [Ework].ework.eFolder fol
	INNER JOIN [Ework].ework.Request_Fulfilment req 
	ON fol.eFolderID = req.EFOLDERID
	WHERE 
		req.SupportTeam IN (SELECT ParamValue FROM [Ework_LPJ].[dbo].[SplitMultiParamList] (@Queue, ','))
		AND fol.eFolderName LIKE 'SR%'
		AND req.[Status] IN ('Open','On Hold')
		AND (req.ProjectReference = '' OR req.ProjectReference IS NULL)
		AND NOT fol.eStageName IN ('Declined','For Approval')



--SELECT distinct er_fullname 
--FROM [Ework].[ework].[er_eServiceDesk]
--order by 1

SELECT *
FROM [Ework].[ework].[er_eServiceDesk] 
WHERE  eUserName = 'BensonJ'
order by 1 --AND eUserName = (replace(AssignedTo, 'hiscox\', ''))


select er_defaultTeam
from [Ework].[ework].[er_eServiceDesk] 
WHERE eUserName = 'BensonJ'

--UPDATE [Ework].[ework].[er_eServiceDesk] 
--set er_defaultTeam = 'yes'
--WHERE eUserName = 'BensonJ'