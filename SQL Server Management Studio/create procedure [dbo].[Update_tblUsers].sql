USE [ODS_RAQ]
GO

/****** Object:  StoredProcedure [dbo].[Update_tblUsers]    Script Date: 13/12/2018 17:15:16 ******/
DROP PROCEDURE [dbo].[Update_tblUsers]
GO

/****** Object:  StoredProcedure [dbo].[Update_tblUsers]    Script Date: 13/12/2018 17:15:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


	
		create procedure [dbo].[Update_tblUsers] (
			@Date date,
			@previousversion int,
			@updatecount int output,
			@InsertCount int output
		) AS
		
		if exists (
			select *
			from [ODS_RAQ].[dbo].[tblUsers]
			where ods_DateFrom > @Date) begin
				
				RAISERROR ('Future data exists in dbo.tblUsers',10,1)
				return
		end
		
		update A
		set A.ods_DateTo = @Date
		from [ODS_RAQ].[dbo].[tblUsers] A
		join CHANGETABLE (CHANGES [RAQ].[dbo].[tblUsers], @PreviousVersion) B 
			on A.[UserName] = B.[UserName]
		where A.ods_DateTo = '31 dec 2999'
				
		set @updatecount = @@ROWCOUNT
		
		insert into [ODS_RAQ].[dbo].[tblUsers] (
			ods_DateFrom, ods_DateTo, [UserId],[UserName],[EmailAddress],[Rowguid],[AddNewPolicies],[ViewExistingPolicies],[ViewStatistics],[AgentID],[ViewManagementInfo],[FullName],[DepartmentID],[JobTitle],[PhoneNumber],[Blocked],[UserGroup],[PasswordLastChanged],[LanguageID],[AuthorityLevel],[DocFolder],[DocFolderLocation]
		)
		select @Date, '31 dec 2999',A.[UserId],A.[UserName],A.[EmailAddress],A.[Rowguid],A.[AddNewPolicies],A.[ViewExistingPolicies],A.[ViewStatistics],A.[AgentID],A.[ViewManagementInfo],A.[FullName],A.[DepartmentID],A.[JobTitle],A.[PhoneNumber],A.[Blocked],A.[UserGroup],A.[PasswordLastChanged],A.[LanguageID],A.[AuthorityLevel],A.[DocFolder],A.[DocFolderLocation]
		from [RAQ].[dbo].[tblUsers] A
		join CHANGETABLE (CHANGES [RAQ].[dbo].[tblUsers], @PreviousVersion) B 
			on A.[UserName] = B.[UserName]
		and SYS_CHANGE_OPERATION <> 'D'
		
		
		set @InsertCount = @@ROWCOUNT

	
GO


