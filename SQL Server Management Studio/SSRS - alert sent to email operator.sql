SELECT 
    	his.server			AS [Server]
    	,job.job_id			AS [JobID]
    	,job.[name]			AS [Name]
    	,REPLACE(his.message,'''','')		AS [Message]
    	,CAST(((SUBSTRING(CAST(his.run_date AS VARCHAR(8)), 1, 4) + '-' 
    		+SUBSTRING(CAST(his.run_date AS VARCHAR(8)), 5, 2) + '-' 
    		+SUBSTRING(CAST(his.run_date AS VARCHAR(8)), 7, 2) + ' '
    		+ SUBSTRING((REPLICATE('0',6-LEN(CAST(his.run_time AS varchar))) 
    		+ CAST(his.run_time AS VARCHAR)), 1, 2) + ':' 
    		+ SUBSTRING((REPLICATE('0',6-LEN(CAST(his.run_time AS VARCHAR))) 
    		+ CAST(his.run_time AS VARCHAR)), 3, 2) + ':'
    		+ SUBSTRING((REPLICATE('0',6-LEN(CAST(his.run_time as varchar)))
    		+ CAST(his.run_time AS VARCHAR)), 5, 2)))AS smalldatetime) AS 'ExecDateTime'
    		,CAST(GETDATE() as smalldatetime) AS SystemEnteredDate
			,'Emailed ' + so.email_address [Alert]
    FROM msdb.dbo.sysjobs job 
    INNER JOIN msdb.dbo.sysjobsteps stp
    	ON job.job_id = stp.job_id
    INNER JOIN msdb.dbo.sysjobhistory his
    	ON job.job_id = his.job_id
    	AND stp.step_id = his.step_id
	LEFT JOIN msdb.dbo.sysoperators so
		ON so.id = notify_email_operator_id
    WHERE job.enabled = 1 
    	AND stp.last_run_outcome = 0
    	AND CAST(((SUBSTRING(CAST(his.run_date AS VARCHAR(8)), 1, 4) + '-' 
    		+SUBSTRING(CAST(his.run_date AS VARCHAR(8)), 5, 2) + '-' 
    		+SUBSTRING(CAST(his.run_date AS VARCHAR(8)), 7, 2) + ' '
    		+ SUBSTRING((REPLICATE('0',6-LEN(CAST(his.run_time AS varchar))) 
    		+ CAST(his.run_time AS VARCHAR)), 1, 2) + ':' 
    		+ SUBSTRING((REPLICATE('0',6-LEN(CAST(his.run_time AS VARCHAR))) 
    		+ CAST(his.run_time AS VARCHAR)), 3, 2) + ':'
    		+ SUBSTRING((REPLICATE('0',6-LEN(CAST(his.run_time as varchar)))
    		+ CAST(his.run_time AS VARCHAR)), 5, 2))) AS DATETIME) > DATEADD(HOUR, -24, GETDATE())

			
			--select * from sysoperators

USE msdb ;  
GO  

-- Show the subject, the time that the mail item row was last  
-- modified, and the log information.  
-- Join sysmail_faileditems to sysmail_event_log   
-- on the mailitem_id column.  
-- In the WHERE clause list items where danw was in the recipients,  
-- copy_recipients, or blind_copy_recipients.  
-- These are the items that would have been sent  
-- to danw.  

SELECT top 100 items.subject,  
    items.last_mod_date  
    ,l.description FROM dbo.sysmail_faileditems as items  
INNER JOIN dbo.sysmail_event_log AS l  
    ON items.mailitem_id = l.mailitem_id  
 order by last_mod_date desc



 SELECT top 100    items.subject ,
       items.recipients ,
       --items.copy_recipients ,
       --items.blind_copy_recipients ,
       items.last_mod_date ,
       l.description
FROM   msdb.dbo.sysmail_faileditems AS items
       LEFT OUTER JOIN msdb.dbo.sysmail_event_log AS l 
                    ON items.mailitem_id = l.mailitem_id
					order by items.last_mod_date desc
WHERE  items.last_mod_date > DATEADD(DAY, -1,GETDATE())

xp_readerrorlog