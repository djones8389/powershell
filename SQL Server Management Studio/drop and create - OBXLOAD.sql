USE [msdb]
GO

/****** Object:  Job [LMDR_Reporting: SP_ExecuteOBXLoad and SP_ExecuteDashboardLoad]    Script Date: 27/11/2018 11:39:41 ******/
EXEC msdb.dbo.sp_delete_job @job_id=N'1f49a45b-9731-474f-a05c-886163428fe2', @delete_unused_schedule=1
GO

/****** Object:  Job [LMDR_Reporting: SP_ExecuteOBXLoad and SP_ExecuteDashboardLoad]    Script Date: 27/11/2018 11:39:41 ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [[Uncategorized (Local)]]    Script Date: 27/11/2018 11:39:41 ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'LMDR_Reporting: SP_ExecuteOBXLoad and SP_ExecuteDashboardLoad', 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=2, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'Contact Gary Goulthorp in case of failure. NB failures are logged to a table he monitors so chances are he will already know about it and have dealt with it.', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=N'dbs', 
		@notify_email_operator_name=N'Gary Goulthorp', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [SP_ExecuteOBXLoad]    Script Date: 27/11/2018 11:39:41 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'SP_ExecuteOBXLoad', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=3, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'
USE [LMDR_Reporting];
GO

EXECUTE AS LOGIN = ''HISCOX\SVC_SQL_PRXY_LM'';
GO


EXECUTE [dbo].[SP_ExecuteOBXLoad];
GO', 
		@database_name=N'LMDR_Reporting', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [spLoadBucket1Thresholds]    Script Date: 27/11/2018 11:39:41 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'spLoadBucket1Thresholds', 
		@step_id=2, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=3, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'EXEC spLoadBucket1Thresholds', 
		@database_name=N'LMDR_Reporting', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [SP_ExecuteDashboardLoad]    Script Date: 27/11/2018 11:39:41 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'SP_ExecuteDashboardLoad', 
		@step_id=3, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'USE [LMDR_Reporting];
GO

EXECUTE AS LOGIN = ''HISCOX\SVC_SQL_PRXY_LM'';
GO

EXECUTE [dbo].[SP_ExecuteDashboardLoad];
GO
', 
		@database_name=N'LMDR_Reporting', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'execute sp sched', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=1, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20170227, 
		@active_end_date=99991231, 
		@active_start_time=10000, 
		@active_end_time=235959, 
		@schedule_uid=N'1d63b339-4a45-4f8a-a210-ec3d93d24bed'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO


