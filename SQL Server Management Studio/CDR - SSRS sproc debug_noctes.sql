USE [SQL_Inventory_DJDev]
GO

--Use view [vw_FailedAgentJobs_SSRSReport_GetCommissionedInstances]
--Use view [vw_FailedAgentJobs_SSRSReport_GetDBAComments]
--Use function [udf_FailedAgentJobs_SSRSReport]

DECLARE @ERRORCODE [int]
	, @ERRMSG varchar(128)
	, @dt AS DATETIME
	, @ScriptName VARCHAR(128) = 'UpsertFailedAgentJobs.ps1'
	, @HideReplication bit

BEGIN
		IF DATENAME(dw,GETDATE()) = 'Monday'
			SELECT @dt = CONVERT(DATETIME,DATEADD(hh,-72,GETDATE()));
		ELSE
			SELECT @dt = CONVERT(DATETIME,DATEADD(hh,-24,GETDATE()));

		SELECT faj.lastRunDate
			,tb.instanceName AS InstanceName
			,tb.hostname AS HostName
			,tb.Environment AS [Environment]
			,tb.lastUpdated
			,saj.jobName AS [Job]
			,faj.errorMessage AS [Error]
			,CONVERT(DATETIME,faj.lastRunDate,120) AS [LastErrorDate]
			,faj.Alerted
			,lfc.[LastFailedComment]
			,tb.[Sorting]
		FROM [vw_FailedAgentJobs_SSRSReport_GetCommissionedInstances] tb

		INNER JOIN dbo.FailedAgentJobs faj ON faj.serverID = tb.serverid
		INNER JOIN dbo.ServerAgentJobs saj ON faj.jobID = saj.jobID
		LEFT JOIN [vw_FailedAgentJobs_SSRSReport_GetDBAComments] lfc 
		on lfc.hostName = tb.hostName
			and lfc.instanceName = tb.instanceName
			and lfc.job = saj.jobName
		WHERE faj.lastRunDate > @dt
			AND faj.lastRunDate = (SELECT MAX(lastRunDate)FROM [SQL_Inventory].[dbo].vw_FailedAgentJobs where jobID = faj.jobID)      
			AND tb.status IN (N'A',N'B',N'C',N'D',N'E',N'F',N'G',N'P',N'R')
			order by Sorting,HostName asc;
END
