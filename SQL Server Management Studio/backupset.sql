select backup_start_date
	, database_name
	, type
	 , user_name
	 , name	
	 , DATEDIFF(minute, backup_start_date,backup_finish_date)
	 --, *
from msdb.dbo.backupset
where type not in ('l','i', 'l')
order by backupset.backup_start_date desc;
 