/****** Script for SelectTopNRows command from SSMS  ******/
SELECT  [DatabaseName]
      --,[SchemaName]
      --,[ObjectName]
      --,[ObjectType]
      --,[IndexName]
      --,[IndexType]
      --,[StatisticsName]
      --,[PartitionNumber]
      --,[ExtendedInfo]
      --,[Command]
      --,[CommandType]
      ,[StartTime]
      ,[EndTime]
      --,[ErrorNumber]
      --,[ErrorMessage]
  FROM [CDR_Local].[dbo].[CommandLog]
  where StartTime > '2018-10-15'
	--and DatabaseName in ('CbtrGRPBaseDataLayerMain_table','cbtr_exp_model_w_version_table','Cbtr_GRPBaseDataLayerMain_view','CbtrGRBDL_MainFC_view','cbtr_exp_model_w_version_view')
  order by StartTime desc;
	
  if OBJECT_ID('tempdb..##checkdb') is not null drop table ##checkdb
  create table ##checkdb (
	dbName sysname null
	,parent nvarchar(MAX) null
	,[object] nvarchar(MAX) null
	,[field] nvarchar(MAX) null
	,[value] nvarchar(MAX) null
  )
  declare @Dynamic nvarchar(MAX)='';
 
  select @Dynamic+=char(13)+
	'insert ##checkdb (parent,[object],[field],[value])
	exec(''DBCC DBINFO('+quotename(name)+') WITH TABLERESULTS'')
	update ##checkdb
	set dbname = '''+name+'''
	where dbname is null
	'
  from sys.databases
  
  exec(@Dynamic)
  
  select *
  from ##checkdb
  where field = 'dbi_dbccLastKnownGood'
	and value  < '2018-10-15'

DBCC DBINFO(act_bi_dmr_table) WITH TABLERESULTSDBCC DBINFO(Act_bi_solvency_II_table) WITH TABLERESULTSDBCC DBINFO(Act_bi_solvency_II_table_risk) WITH TABLERESULTS