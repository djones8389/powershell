USE [master]
DECLARE @DBName sysname = 'IDIT';
DECLARE @DataRestoreLocation nvarchar(MAX)= (select physical_name from sys.master_files where db_name(database_id) = @DBName and type_desc = 'ROWS')
DECLARE @DataLogicalName nvarchar(MAX)= (select name from sys.master_files where db_name(database_id) = @DBName and type_desc = 'ROWS')
DECLARE @LogRestoreLocation nvarchar(MAX)= (select physical_name from sys.master_files where db_name(database_id) = @DBName and type_desc = 'LOG');
DECLARE @LogLogicalName nvarchar(MAX)= (select name from sys.master_files where db_name(database_id) = @DBName and type_desc = 'LOG');
DECLARE @AG nvarchar(50) = (select name from sys.availability_groups);

declare  @dynamic nvarchar(max) = '';
select @dynamic += CHAR(13) +
        'kill ' + cast(spid as char(10))
       from sys.sysprocesses
where db_name(dbid) = @DBName
exec(@dynamic);

declare @RemoveFromAG NVARCHAR(MAX) = '';
select @RemoveFromAG += CHAR(13) + 'ALTER AVAILABILITY GROUP ' + quotename(@AG) + ' REMOVE DATABASE '+quotename(@DBName)+';'
exec (@RemoveFromAG);

RESTORE DATABASE @DBName
FROM
       DISK = '\\HXP205-00047-00\Backups\sqlbackups\IDIT.bak1',
       DISK = '\\HXP205-00047-00\Backups\sqlbackups\IDIT.bak2',
       DISK = '\\HXP205-00047-00\Backups\sqlbackups\IDIT.bak3',
       DISK = '\\HXP205-00047-00\Backups\sqlbackups\IDIT.bak4',
       DISK = '\\HXP205-00047-00\Backups\sqlbackups\IDIT.bak5',
       DISK = '\\HXP205-00047-00\Backups\sqlbackups\IDIT.bak6',
       DISK = '\\HXP205-00047-00\Backups\sqlbackups\IDIT.bak7',
       DISK = '\\HXP205-00047-00\Backups\sqlbackups\IDIT.bak8',
       DISK = '\\HXP205-00047-00\Backups\sqlbackups\IDIT.bak9',
       DISK = '\\HXP205-00047-00\Backups\sqlbackups\IDIT.bak10'
WITH FILE = 1, MOVE @DataLogicalName TO @DataRestoreLocation,
                     MOVE @LogLogicalName TO @LogRestoreLocation
, RECOVERY, NOUNLOAD, REPLACE, NOREWIND, STATS = 1

declare @AddToAG NVARCHAR(MAX) = '';
select @AddToAG += CHAR(13) +  'ALTER AVAILABILITY GROUP ' + quotename(@AG) + ' ADD DATABASE '+quotename(@DBName)+';'
exec (@AddToAG);