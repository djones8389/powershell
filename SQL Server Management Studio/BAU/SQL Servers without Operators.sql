use master;

--SQL Servers without Operators

--ServerID null means cannot connect
--OperatorID null means no operators

select 
	I.ConnectionString
	, O.*
from BAU.dbo.Instances I

left join BAU.dbo.Operators O
on O.ServerID = I.ServerID

where  OperatorID is null

UNION

select 
	I.ConnectionString
	, O.*
from [CDR_NONPROD].[BAU].[dbo].Instances I

left join [CDR_NONPROD].[BAU].[dbo].Operators O
on O.ServerID = I.ServerID

where  OperatorID is null

order by 1,2;

