use master;

if not exists (select 1 from sys.databases where name = 'BAU')
	BEGIN
		create database BAU;
	END

--drop database BAU;

use BAU;

if not exists (select 1 from sys.tables where name = 'Instances')
	
	CREATE TABLE dbo.[Instances] (
		[ServerID] smallint
		, [HostName] varchar(100)
		, [InstanceName] varchar(100)
		, [ConnectionString] AS CAST
		(
			CASE
			WHEN instanceName = 'MSSQLSERVER' then [HostName]
			ELSE [HostName] + '\' + instanceName
			END AS VARCHAR(200)
		)
		, [Commissioned] bit not null default(1)
		PRIMARY KEY CLUSTERED(serverID, [HostName])
	);


if not exists (select 1 from sys.tables where name = 'DBMail')
	CREATE TABLE dbo.[DBMail] (
		[ServerID] smallint not null
		, [DBMailEnabled] bit null default(0)
		, [DBMailTested] bit null default(0)
		, [DBMailTestResult] bit null default(0)
	);



if not exists (select 1 from sys.tables where name = 'Operators')
	CREATE TABLE dbo.[Operators] (
		[ServerID] smallint
		, [OperatorID] tinyint
		, [OperatorName] varchar(100)
		, [EmailAddress] nvarchar(200)
		, [IsEnabled] bit
	);

if not exists (select 1 from sys.tables where name = 'Jobs')
	CREATE TABLE [dbo].[Jobs](
		[ServerID] [smallint] NULL,
		[JobID] [uniqueidentifier] NULL,
		[JobName] [varchar](200) NULL,
		[IsEnabled] [bit] NULL,
		[NotifyViaEmail] [varchar](100) NULL,
		[OperatorID] [varchar](100) NULL
	)