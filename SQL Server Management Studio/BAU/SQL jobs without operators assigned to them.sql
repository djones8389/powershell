use master;

--SQL jobs without operators assigned to them

select 
	I.ConnectionString
	, j.JobName
	, j.NotifyViaEmail
	, j.OperatorID
from BAU.dbo.Instances I

inner join BAU.dbo.Jobs J
on J.ServerID = I.ServerID
 
where j.IsEnabled = 1
	and OperatorID = ''
	and j.JobName like '%CDR%' or j.JobName like 'DBA'
UNION


select 
	I.ConnectionString
	, j.JobName
	, j.NotifyViaEmail
	, j.OperatorID
from [CDR_NONPROD].[BAU].[dbo].Instances I

inner join  [CDR_NONPROD].[BAU].[dbo].Jobs J
on J.ServerID = I.ServerID
 
where j.IsEnabled = 1
	and OperatorID = ''
	and j.JobName like '%CDR%' or j.JobName like 'DBA'
order by 1,2;