select distinct 
	 h.hostName
	 , s.instanceName
	 , d.databaseName
	 , s.instanceName
	 , s.hostID
	 , servicepack  
	 , s.[version]
	from [SQL_Inventory].[dbo].[Servers] s --ON s.[serverID] = faj.[serverID]
	LEFT OUTER JOIN [SQL_Inventory].[dbo].[Clusters] c ON c.[clusterID] = s.[clusterID]
	LEFT OUTER JOIN [SQL_Inventory].[dbo].[ClusterNodes] cn ON cn.[clusterID] = s.[clusterID]
	LEFT OUTER JOIN [SQL_Inventory].[dbo].[Hosts] ch ON ch.[hostID] = cn.[nodeID]
	LEFT OUTER JOIN [SQL_Inventory].[dbo].[Hosts] h ON h.[hostID] = s.[hostID]
	inner join Databases D on D.serverID = s.serverID
	 where h.hostName in ('hxf20240.hiscox.com')
	   and instanceName = 'guernseytrader'

	select * 
	from [Servers] s
	where s.instanceName = 'GUERNSEYTRADER'
		and hostid = 45

	update [Servers]  
	set instanceName = 'GUERNSEYTRADER'
		 , servicepack = 'SP3'
		, [version] = '10.50.6000.34'
	where instanceName = 'GUERNSEYTRADER'
		and hostid = 45