USE [msdb]
GO

/****** Object:  Job [LMDR_Reporting: RepopulateOBXUnallocatedSignings]    Script Date: 25/01/2019 10:38:48 ******/
EXEC msdb.dbo.sp_delete_job @job_id=N'fd22bc26-8d6a-4914-9008-995aea654b2f', @delete_unused_schedule=1
GO

/****** Object:  Job [LMDR_Reporting: RepopulateOBXUnallocatedSignings]    Script Date: 25/01/2019 10:38:48 ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [Data Collector]    Script Date: 25/01/2019 10:38:48 ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'Data Collector' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'Data Collector'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'LMDR_Reporting: RepopulateOBXUnallocatedSignings', 
		@enabled=0, 
		@notify_level_eventlog=0, 
		@notify_level_email=2, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'Configured for Gary Goulthorp - SR157795', 
		@category_name=N'Data Collector', 
		@owner_login_name=N'dbs', 
		@notify_email_operator_name=N'LM OBX Support Team', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Repopulate OBXUnallocatedSignings]    Script Date: 25/01/2019 10:38:48 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Repopulate OBXUnallocatedSignings', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'TRUNCATE TABLE [LMDR_Reporting].[dbo].[OBXUnallocatedSignings];
GO
INSERT INTO [LMDR_Reporting].[dbo].[OBXUnallocatedSignings]
SELECT 
a.u1_r_uw_ref as ''UWReference'',
a.u1_c_sett_ccy_is as ''SettCcy'',
a.u1_d_ac_mth as ''AccountingMonth'',
a.u1_c_category_cd as ''CategoryCode'',
d.i1_s_description as ''Category'',
a.u1_c_qualf_cd as ''QualifierCode'',
a.u1_r_lpso_no as ''LPSONo'',
a.u1_r_lpso_date as ''LPSODate'',
a.u1_r_lpso_ref_ver as ''LPSOVersion'',
a.u1_c_synd as ''Syndicate'',
a.u1_p_synd_sign_line as ''SyndicateSignedLine'',
a.u1_r_bkr_ref_1 as ''brokerRef1'',
a.u1_c_bkr as ''BrokerCode'',
a.u1_c_rclass as ''RiskClass'',
a.u1_d_yr_ac as ''YOA'' ,
a.u1_c_fil_cd as ''FILCode'',
a.u1_a_synd_net_amnt as ''SyndicateNetAmount'',
a.u1_a_synd_war_amnt as ''WarAmount'',
a.u1_a_synd_ri_ceded as ''SyndicateRICededAmount'',
a.u1_a_synd_vat_amnt as ''SyndicateVATAmount'',
a.u1_c_mkt_cat_cd as ''CatCode'',
a.u1_d_actl_pay_date as ''APD'',
a.u1_d_sett_due_date as ''SDD'',
a.u1_c_trust_fund as ''TrustFund'',
a.u1_c_fil1 as ''FilCode1'',
a.u1_c_business as ''BusinessCode'',
a.u1_r_bkr_ref_2 as ''BrokerRef2'',
a.u1_r_msg_ref_no as ''MessageNumber'',
a.u1_p_lld_total_line as ''TotalLine'',
a.u1_r_lmr as ''UMR'',
a.u1_a_100_pc_amnt as ''Amount100PC'',
a.u1_r_orig_lpso_no as ''OrigLPSONo'',
a.u1_r_orig_lpso_date as ''OrigLPSODate'',
u1_p_bkr_ord_pc as ''BrokersOrder'',
b.u1_c_sect_cd as ''ErrorCode'',
c.i1_s_sect_desc as ''ErrorDescription''
FROM [APHXP10S-OPBX01\OPENBOX].[OPENBOX].[dbo].[usmview_open] a
inner join [APHXP10S-OPBX01\OPENBOX].[OPENBOX].[dbo].[u1_8_sectn_cds] b 
On (a.u1_r_transn_key_no = b.u1_r_transn_key_no)
 and (a.u1_r_transn_key_date = b.u1_r_transn_key_date)
 AND (b.u1_q_confirm = ''N'' ) 
 AND  (a.u1_r_transn_key_date = 201801) 
 Inner join [APHXP10S-OPBX01\OPENBOX].[OPENBOX].[dbo].[i1_j_cd_usm_sectn] c
 on b.u1_c_sect_cd = c.i1_c_code
 Inner join  [APHXP10S-OPBX01\OPENBOX].[OPENBOX].[dbo].[i1_u_category] d
 on a.u1_c_category_cd = d.i1_c_code;
 GO', 
		@database_name=N'LMDR_Reporting', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Daily 8am', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=1, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20180111, 
		@active_end_date=99991231, 
		@active_start_time=80000, 
		@active_end_time=235959, 
		@schedule_uid=N'6fcc7d7e-ca17-4bcf-a096-390a38d22cbe'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO


