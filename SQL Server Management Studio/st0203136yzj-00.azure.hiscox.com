SET NOCOUNT ON;

DECLARE @name VARCHAR(50) -- database name
DECLARE @BackupName varchar(100)
DECLARE @path VARCHAR(256) -- path for backup files
DECLARE @fileName VARCHAR(256) -- filename for backup
SET @path = 'E:\SQL_Backups\' -- as same as your created folder' 
DECLARE db_cursor CURSOR FOR
  SELECT name
  FROM MASTER.dbo.sysdatabases
  WHERE name NOT IN ('master','model','msdb','tempdb','CDR_local')
OPEN db_cursor
FETCH NEXT FROM db_cursor INTO @name
WHILE @@FETCH_STATUS = 0
BEGIN
  SET @fileName = @path + @name + '.BAK'
  SET @BackupName  = @name+'-Full Database Backup'
  BACKUP DATABASE @name TO DISK = @fileName
  BACKUP DATABASE @name TO  DISK = @fileName WITH NOFORMAT, INIT,  NAME = @BackupName, SKIP, NOREWIND, NOUNLOAD,  STATS = 10

  BACKUP LOG @name TO DISK = 'NUL'

  FETCH NEXT FROM db_cursor INTO @name
END
CLOSE db_cursor
DEALLOCATE db_cursor


DECLARE @path VARCHAR(30) = 'E:\SQL_Backups\';
DECLARE @Dynamic nvarchar(MAX)='';
 
select @Dynamic +=CHAR(13) + 
	'BACKUP DATABASE '+quotename(s.name)+' TO  DISK = '''+@path+name+'.bak'' WITH NOFORMAT, INIT,  NAME = '''+name+'-Full Database Backup'''+', SKIP, NOREWIND, NOUNLOAD,  STATS = 10;'
	+
	'  BACKUP LOG '+quotename(s.name)+' TO  DISK = ''NUL'';'
from sys.databases s
left JOIN sys.dm_hadr_availability_replica_states hars ON s.replica_id = hars.replica_id
where hars.role_desc = 'Primary'

EXEC(@Dynamic);