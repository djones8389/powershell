--TRUNCATE TABLE T_ASSET_RISK;

--select count(1) from T_ASSET_RISK  --32,783,135

--if OBJECT_ID('tempdb..#test') is not null drop table #test;

--create table #test (

--[ID] [numeric](12, 0) NOT NULL,
--	[PRODUCT_ID] [numeric](12, 0) NOT NULL,
--	[RISK_TYPE_ID] [numeric](12, 0) NOT NULL,
--	[KEY_VALUE] [varchar](255) NOT NULL,
--	[RISK_VALUE] [numeric](20, 8) NOT NULL,
--	[UPDATE_USER] [numeric](12, 0) NOT NULL,
--	[UPDATE_DATE] [datetime] NOT NULL,
--	[UPDATE_VERSION] [numeric](12, 0) NOT NULL,
--	[RESERVED_VALUE_FLAG] [numeric](12, 0) NOT NULL,
--	[IS_DEFAULT] [numeric](12, 0) NOT NULL,
--	[EXTERNAL_CODE] [varchar](20) NULL,
--	[DISCONTINUE_DATE] [datetime] NULL,
--	[DEVELOPER_DESC] [varchar](4000) NULL,
--	[SORT_ORDER] [numeric](12, 0) NULL
-- )

TRUNCATE TABLE T_ASSET_RISK;

bulk insert #test
from 'T:\Datafix1903\PostcodeFile1.csv'
with (fieldterminator = 'r' , rowterminator = '\n')

bulk insert T_ASSET_RISK
from 'T:\Datafix1903\PostcodeFile2.csv'
with (fieldterminator = 'r' , rowterminator = '\n')

bulk insert T_ASSET_RISK
from 'T:\Datafix1903\PostcodeFile3.csv'
with (fieldterminator = 'r' , rowterminator = '\n')

bulk insert T_ASSET_RISK
from 'T:\Datafix1903\PostcodeFile4.csv'
with (fieldterminator = 'r' , rowterminator = '\n')


UPDATE T_ASSET_RISK 
SET UPDATE_DATE = getdate();

select top 1 * from T_ASSET_RISK

select * from PostcodeFile1 where id = 3883496

update PostcodeFile1
set update_date = replace(update_date,'"','')
 
update PostcodeFile3
set update_date = replace(update_date,'"','')

update PostcodeFile4
set update_date = replace(update_date,'"','')


INSERT T_ASSET_RISK
select * from PostcodeFile1
union
select * from PostcodeFile3
union
select * from PostcodeFile4