USE [master]
GO

/****** Object:  LinkedServer [COGNOS]    Script Date: 25/07/2018 12:32:34 ******/
EXEC master.dbo.sp_dropserver @server=N'COGNOS', @droplogins='droplogins'
GO

/****** Object:  LinkedServer [COGNOS]    Script Date: 25/07/2018 12:29:12 ******/
EXEC master.dbo.sp_addlinkedserver @server = N'COGNOS', @srvproduct=N'SQL_Server', @provider=N'SQLNCLI', @datasrc=N'prod-cognos2-db\cognos_primary'
 /* For security reasons the linked server remote logins password is changed with ######## */
EXEC master.dbo.sp_addlinkedsrvlogin @rmtsrvname=N'COGNOS',@useself=N'False',@locallogin=NULL,@rmtuser='HXP20290_dwrpt_prod_db_50187_Login',@rmtpassword='vmzcWT9t1ZahOtc63tDK'
GO

EXEC master.dbo.sp_serveroption @server=N'COGNOS', @optname=N'collation compatible', @optvalue=N'false'
GO

EXEC master.dbo.sp_serveroption @server=N'COGNOS', @optname=N'data access', @optvalue=N'true'
GO

EXEC master.dbo.sp_serveroption @server=N'COGNOS', @optname=N'dist', @optvalue=N'false'
GO

EXEC master.dbo.sp_serveroption @server=N'COGNOS', @optname=N'pub', @optvalue=N'false'
GO

EXEC master.dbo.sp_serveroption @server=N'COGNOS', @optname=N'rpc', @optvalue=N'true'
GO

EXEC master.dbo.sp_serveroption @server=N'COGNOS', @optname=N'rpc out', @optvalue=N'true'
GO

EXEC master.dbo.sp_serveroption @server=N'COGNOS', @optname=N'sub', @optvalue=N'false'
GO

EXEC master.dbo.sp_serveroption @server=N'COGNOS', @optname=N'connect timeout', @optvalue=N'0'
GO

EXEC master.dbo.sp_serveroption @server=N'COGNOS', @optname=N'collation name', @optvalue=null
GO

EXEC master.dbo.sp_serveroption @server=N'COGNOS', @optname=N'lazy schema validation', @optvalue=N'false'
GO

EXEC master.dbo.sp_serveroption @server=N'COGNOS', @optname=N'query timeout', @optvalue=N'0'
GO

EXEC master.dbo.sp_serveroption @server=N'COGNOS', @optname=N'use remote collation', @optvalue=N'true'
GO

EXEC master.dbo.sp_serveroption @server=N'COGNOS', @optname=N'remote proc transaction promotion', @optvalue=N'true'
GO


