DECLARE @Login nvarchar(100)= 'awkward_name=123-3543';
DECLARE @Enabled bit = 0;

IF @Enabled = 0
BEGIN

	IF EXISTS (
		SELECT 1
		FROM sys.server_principals
		WHERE NAME = @login
			and is_disabled = 0
	)

	DECLARE @DISABLE nvarchar(MAX) = '';
	SELECT @DISABLE = 'ALTER LOGIN '+QUOTENAME(@login)+' DISABLE;'
	EXEC(@DISABLE)

END
ELSE
	BEGIN

	IF EXISTS (
		SELECT 1
		FROM sys.server_principals
		where name = @login
			and is_disabled = 1
	)
	DECLARE @ENABLE nvarchar(MAX) = '';
	SELECT @ENABLE = 'ALTER LOGIN '+QUOTENAME(@login)+' ENABLE;'
	EXEC(@ENABLE)

END	