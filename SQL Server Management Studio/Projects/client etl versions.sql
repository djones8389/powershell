use msdb

select b.name
	, a.command
from sysjobsteps a
inner join sysjobs b
on a.job_id = b.job_id
where name like '%ETL%'
order by 1