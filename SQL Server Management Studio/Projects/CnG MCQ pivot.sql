set transaction isolation level read uncommitted

IF OBJECT_ID('tempdb..#Questions') IS NOT NULL DROP TABLE #Questions;
IF OBJECT_ID('tempdb..#Answers') IS NOT NULL DROP TABLE #Answers;

CREATE TABLE #Questions (
	ItemID VARCHAR(20)
	, Question VARCHAR(max)
	, ID VARCHAR(50)
	, Answers VARCHAR(max)
);

INSERT #Questions(ItemID,Question,ID)
select distinct 
	itt.itemID
	, itt.Question
	, itt.ID
FROM ItemMultipleChoiceTable MCQ
INNER JOIN  (
	select a.itemID
		, a.Question
		,a.ID
	from (
		select b.ItemID
			, cast(b.Question.query('ItemValue/TEXTFORMAT/P//FONT/text()') as nvarchar(MAX)) + cast(b.Question.query('ItemValue/TEXTFORMAT/P//FONT/B/text()') as nvarchar(MAX)) Question
			,b.ID
		from (
			select  substring(ID, 0, charindex('S',ID)) [ItemID]
				, itt.id	
				, CAST(ItemValue AS xml)  Question
				--, ROW_NUMBER() OVER(PARTITION BY substring(ITT.ID, 0, charindex('S',ITT.ID)) ORDER BY ITT.Y ASC) R
			from [ItemTextBoxTable] ITT
			 where ID like '344P%'
		) b
	) a 
) ITT
on ITT.itemID = substring(MCQ.ID, 0, charindex('S',MCQ.ID));


SELECT --TOP(10000)
 ROW_NUMBER() 
  OVER(PARTITION BY ParentID
    ORDER BY ID, [asC]) AS [N],
 ParentID,
 ID, 
 [asC] AS [AnswerAlias], 
 CAST(ItemXml.query('.//text()') AS nvarchar(max)) AS [AnswerText]
INTO #Answers
FROM (
 SELECT ID,
  ParentID,
  [asC],
  CAST(ItemValue AS xml) AS [ItemXml]
 FROM dbo.ItemMultipleChoiceTable WITH (READUNCOMMITTED)
 where parentid like '344P%'
) AS [X];




SELECT p.ParentID, q.Question, [1] AS [Answer1], [2] AS [Answer2], [3] AS [Answer3], [4] AS [Answer4], [5] AS [Answer5]
FROM (
 SELECT ParentID, N, AnswerAlias + N': ' + AnswerText AS [AnswerText]
 FROM #Answers
) AS [S]
PIVOT (
 MAX(AnswerText)
 FOR N IN ([1], [2], [3], [4], [5])
) AS [P]
INNER JOIN (
	SELECT
		(
			SELECT Question + '  '
			from #QUESTIONS Q2
			where substring(Q2.id,0,charindex('C',Q2.id)) = substring(b.parentid,0,charindex('C',b.parentid))
				and (cast(substring(b.Parentid, charindex('C',b.Parentid)+1, 10) as int) - cast(substring(q2.id, charindex('C',Q2.id)+1, (LEN(Q2.id) - CHARINDEX('I', q2.id))) as int)) in (1,2)
			for xml path ('')
		) Question
		, b.parentid
	FROM (
			SELECT 
				  a.*
				, ROW_NUMBER() over (PARTITION BY ParentID ORDER BY ParentID) R
			FROM #QUESTIONS Q
			INNER JOIN #ANSWERS A
			on substring(Q.id,0,charindex('C',Q.id)) = substring(A.parentid,0,charindex('C',A.parentid))
				and substring(q.id, charindex('C',Q.id)+1, (LEN(Q.id) - CHARINDEX('I', q.id))) < substring(A.Parentid, charindex('C',A.Parentid)+1, 10)
			where (cast(substring(A.Parentid, charindex('C',A.Parentid)+1, 10) as int) - cast(substring(q.id, charindex('C',Q.id)+1, (LEN(Q.id) - CHARINDEX('I', q.id))) as int)) in (1,2)
	) B
	where R = 1
) Q
on q.parentid = p.ParentID


--select * 
--from #QUESTIONS














--SELECT --TOP(10000)
-- ROW_NUMBER() 
--  OVER(PARTITION BY ParentID
--    ORDER BY ID, [asC]) AS [N],
-- ParentID,
-- ID, 
-- [asC] AS [AnswerAlias], 
-- CAST(ItemXml.query('.//text()') AS nvarchar(max)) AS [AnswerText]
--INTO #Answers
--FROM (
-- SELECT ID,
--  ParentID,
--  [asC],
--  CAST(ItemValue AS xml) AS [ItemXml]
-- FROM dbo.ItemMultipleChoiceTable WITH (READUNCOMMITTED)
-- where parentid like '344P%'
--) AS [X];

--SELECT ParentID, [1] AS [Answer1], [2] AS [Answer2], [3] AS [Answer3], [4] AS [Answer4], [5] AS [Answer5]
--FROM (
-- SELECT ParentID, N, AnswerAlias + N': ' + AnswerText AS [AnswerText]
-- FROM #Answers
--) AS [S]
--PIVOT (
-- MAX(AnswerText)
-- FOR N IN ([1], [2], [3], [4], [5])
--) AS [P]




--DROP TABLE #Answers;





--SELECT ParentID, [1] AS [Answer1], [2] AS [Answer2], [3] AS [Answer3], [4] AS [Answer4], [5] AS [Answer5]
--FROM (
-- SELECT ParentID, N, AnswerAlias + N': ' + AnswerText AS [AnswerText]
-- FROM #Answers
--) AS [S]
--PIVOT (
-- MAX(AnswerText)
-- FOR N IN ([1], [2], [3], [4], [5])
--) AS [P]
