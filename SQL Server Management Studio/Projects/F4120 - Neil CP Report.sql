SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

SELECT @@SERVERNAME
	, COUNT(PageID) [No. of Items]
	, ProjectType

FROM (

select 
	  ID as [ProjectID]
	 , Name as [ProjectName]
	 , p.r.value('@ID','nvarchar(12)') as PageID
	 , CASE WHEN OutputMedium = 0 THEN 'Computer' ELSE 'Paper' END AS 'ProjectType'
from ProjectListTable
	
cross apply ProjectStructureXml.nodes('Pro//Pag') p(r)

where
    (
		p.r.value('@markingSchemeFor','int') = '-1'
			or 
		p.r.value('@markingSchemeFor','int') IS NULL
	)
	
EXCEPT		
		
select 
	  ID as [ProjectID]
	 , Name as [ProjectName]
	 , p.r.value('@ID','nvarchar(12)') as PageID
	 , CASE WHEN OutputMedium = 0 THEN 'Computer' ELSE 'Paper' END AS 'ProjectType'
from ProjectListTable
	
cross apply ProjectStructureXml.nodes('Pro/Rec//Pag') p(r)

where 
    (
		p.r.value('@markingSchemeFor','int') = '-1'
			or 
		p.r.value('@markingSchemeFor','int') IS NULL
	)
) A
 group by  ProjectType