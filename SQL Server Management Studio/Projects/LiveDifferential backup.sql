Use master;

DECLARE @Dynamic NVARCHAR(MAX) = '';

SELECT @Dynamic +=CHAR(13) + N'BACKUP DATABASE ['+name+N'] TO DISK = N''T:\Backup\Differential.' + CONVERT(VARCHAR(10), GETDATE(), 102) + '.bak'' WITH NAME = N'''+name+N'- Database Backup'', DIFFERENTIAL, NOFORMAT, NOINIT, SKIP, COMPRESSION, NOREWIND, NOUNLOAD, STATS = 10;'  
FROM sys.databases 
where database_id > 4
ORDER BY name;

exec(@Dynamic);
