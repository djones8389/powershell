SET NOCOUNT ON;

IF EXISTS (
SELECT 1 from sys.configurations where Name = 'xp_cmdshell' and value_in_use = 0
)
	PRINT 'You need to enable CMDShell, or copy files manually!!'

IF (SELECT CONVERT(SYSNAME, SERVERPROPERTY('instancedefaultdatapath'))) IS NULL
	PRINT 'Default Data Path isn''t defined, set this in the variable manually'
	
IF (SELECT CONVERT(SYSNAME, SERVERPROPERTY('instancedefaultlogpath'))) IS NULL
	PRINT 'Default Log Path isn''t defined, set this in the variable manually'

DECLARE @SQL NVARCHAR(MAX) = ''
	  , @OnlineOffline NVARCHAR(MAX) = '';

DECLARE @DefaultDataLoc NVARCHAR(1000) = CONVERT(SYSNAME, SERVERPROPERTY('instancedefaultdatapath'))
	  ,  @DefaultLogLoc NVARCHAR(1000) = CONVERT(SYSNAME, SERVERPROPERTY('instancedefaultlogpath'));

SELECT @DefaultDataLoc = 'D:\Data\'
SELECT @DefaultLogLoc = 'D:\Log\'

DECLARE @Tables TABLE(databaseName nvarchar(100), [LogicalName] nvarchar(250), [CurrentLocation] nvarchar(1000), [NewLocation] nvarchar(1000))
INSERT @Tables
SELECT D.[Name] [DatabaseName]
	  , F.Name  [LogicalName]
	  , F.physical_name [CurrentLocation]
	  ,  case f.type_desc when 'ROWS' then   @DefaultDataLoc + SUBSTRING(F.physical_name,LEN(REVERSE(SUBSTRING(REVERSE(F.physical_name), CHARINDEX('\',REVERSE(F.physical_name)), LEN(F.physical_name))))+1,LEN(F.physical_name))
					 when 'LOG' then   @DefaultLogLoc + SUBSTRING(F.physical_name,LEN(REVERSE(SUBSTRING(REVERSE(F.physical_name), CHARINDEX('\',REVERSE(F.physical_name)), LEN(F.physical_name))))+1,LEN(F.physical_name))
	END as [NewLocation]
FROM sys.master_files as F
inner join (
	SELECT database_id, [Name]
	FROM sys.databases
	) as D
on D.database_id = F.database_id 
where D.[Name] in ('PRV_master');

SELECT @OnlineOffline +=CHAR(13) + 
 
 'ALTER DATABASE ' + databaseName
	FROM  (
	SELECT DISTINCT QUOTENAME (databaseName) [databaseName]
	FROM @Tables
	) A	;

SELECT @OnlineOffline = @OnlineOffline + ' SET OFFLINE;';
SELECT @SQL = @OnlineOffline;

SELECT @SQL +=CHAR(13) +  'ALTER DATABASE ' + QUOTENAME (databaseName) + ' MODIFY FILE (NAME = ''' + [LogicalName] + ''',' + 'Filename = ''' + NewLocation + ''');'
	+ ' EXECUTE xp_cmdshell ''' + 'move /Y ' + CurrentLocation + '  ' +  NewLocation + ''''
from @Tables;

SELECT @SQL += REPLACE(@OnlineOffline,'OFFLINE','ONLINE');

PRINT(@SQL);


/*

ALTER DATABASE [PRV_master] SET OFFLINE;ALTER DATABASE [PRV_master] MODIFY FILE (NAME = 'master',Filename = 'D:\Data\PRV_master.mdf'); EXECUTE xp_cmdshell 'move /Y D:\Backup\PRV_master.mdf  D:\Data\PRV_master.mdf'ALTER DATABASE [PRV_master] MODIFY FILE (NAME = 'mastlog',Filename = 'D:\Log\PRV_master.ldf'); EXECUTE xp_cmdshell 'move /Y D:\Backup\PRV_master.ldf  D:\Log\PRV_master.ldf'ALTER DATABASE [PRV_master] SET ONLINE;


*/


