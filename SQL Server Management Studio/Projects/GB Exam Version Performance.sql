DECLARE @DateStart datetime
	,@DateEnd datetime
	,@ExamVersionKey int

set @DateStart='2016-03-10 00:00:00' 
set @DateEnd='2016-05-10 00:00:00' 
set @ExamVersionKey=1358
------------------------------------------  Date keys  ------------------------------------------
DECLARE @DateKeyStart int, @DateKeyEnd int, @DateCompletedKey int
SELECT @DateKeyStart = (
	CASE 
		WHEN @DateStart<(SELECT FullDateAlternateKey FROM DimTime WHERE TimeKey = 1) THEN 1 
		ELSE (SELECT TimeKey FROM DimTime WHERE FullDateAlternateKey = CAST(@DateStart AS date)) 
	END)
	,@DateKeyEnd = (
	CASE 
		WHEN @DateStart>(SELECT FullDateAlternateKey FROM DimTime WHERE TimeKey = IDENT_CURRENT('DimTime')) 
			THEN IDENT_CURRENT('DimTime') 
		ELSE (SELECT TimeKey FROM DimTime WHERE FullDateAlternateKey = CAST(@DateEnd AS date)) 
	END)
	--,@DateCompletedKey = (SELECT TimeKey FROM DimTime WHERE FullDateAlternateKey = CAST(@DateCompleted AS date)) 
	
--SELECT 	@DateKeyStart, @DateKeyEnd
----------------------------------------  Exam sessions  ----------------------------------------
IF OBJECT_ID('tempdb..#ES') IS NOT NULL DROP TABLE #ES
CREATE TABLE #ES(
	ExamSessionKey int PRIMARY KEY CLUSTERED
	,SeqN int
	,CentreKey int	
	,ExamVersionKey int
	,DateCompleted datetime
	,ExamUserMark decimal(9,4)
	,ExamTotalMark decimal(9,4)
	,ExamScaledMark decimal(9,4)
	,FinalExamState int
	--,ItemNumber int
)

INSERT INTO #ES
SELECT
	FES.ExamSessionKey
	,NULL --ROW_NUMBER() OVER (PARTITION BY FES.ExamVersionKey, FES.CentreKey ORDER BY FES.UserMarks ASC, FES.ExamSessionKey ASC) SeqN
	,CentreKey
	,ExamVersionKey
	,DT.FullDateAlternateKey +  FES.CompletionTime DateCompleted
	,FES.UserMarks 
	,FES.TotalMarksAvailable
	,NULL --FES.UserMarks/FES.TotalMarksAvailable*50
	,FES.FinalExamState
	--,COUNT(CID) 
FROM 
	FactExamSessions FES
	JOIN DimTime DT ON FES.CompletionDateKey = DT.TimeKey

WHERE 
	FES.FinalExamState<>10
	--AND FES.CompletionDateKey BETWEEN @DateKeyStart AND @DateKeyEnd
	AND (FES.CompletionDateKey = @DateCompletedKey OR @DateCompletedKey IS NULL)
	AND FES.CentreKey IN (784,749,796,113,442,724,347,138,150,207,196,312,186,179,202,149,168,695,220,564,41,70,39,69,73,112,104,431,483,422,30,597,798,742,552,775,785,786,350,797,794,769,783)
	AND FES.ExamVersionKey IN (@ExamVersionKey)
	AND FES.TotalMarksAvailable>0



---------------------------------------  Item(task) level - to obtain TPM marks ---------------------------------------
IF OBJECT_ID('tempdb..#Items_raw') IS NOT NULL DROP TABLE #Items_raw
CREATE TABLE #Items_raw (
	CentreKey int 
	,ExamVersionKey int 
	,ExamSessionKey int 
	,SeqN int
	,N int
	,SDp float
	,ExamUserMark decimal(9,4)
	--,ExamTotalMark float
	,CPID nvarchar(50)
	,CPVersion int
--	,MarkerMark float
--	,UserMark float
	,ItemUserMark decimal(9,4)
	,ItemTotalMark decimal(9,4)
	,FinalExamState int
	,AutoMarked int
	,TPMScaled bit
	,PRIMARY KEY CLUSTERED (
		CentreKey ASC
		,ExamVersionKey ASC 
		,ExamSessionKey ASC
		,CPID ASC
	)
)

;WITH 
MarkerMarks AS(
      SELECT 
            ROW_NUMBER() OVER(PARTITION BY FMR.ExamSessionKey, FMR.CPID ORDER BY SeqNo DESC) N
            ,FMR.ExamSessionKey
            ,FMR.CPID
            ,FMR.assignedMark
      FROM 
            #ES ES
            JOIN FactMarkerResponse FMR
                  ON FMR.ExamSessionKey = ES.ExamSessionKey
), Items_raw AS (
	SELECT 
		ES.CentreKey
		,ES.ExamVersionKey
		,FQR.ExamSessionKey
		,NULL SeqN
		,NULL N
		,NULL SDp
		,NULL ExamUserMark
		--,ES.ExamTotalMark
		,FQR.CPID
		,FQR.CPVersion
		,ROUND(COALESCE(TPM.Mark, MM.assignedMark, FQR.Mark*DQ.TotalMark),4) ItemUserMark
		--,MM.assignedMark MarkerMark
		--,FQR.Mark*DQ.TotalMark UserMark
		,ISNULL(TPM.TotalMark, DQ.TotalMark) ItemTotalMark
		,FinalExamState
		,CASE WHEN MM.assignedMark IS NULL THEN 1 ELSE 0 END AutoMarked
		,CASE WHEN TPM.Mark IS NULL THEN 0 ELSE 1 END TPMScaled
	FROM 
		#ES ES
		--JOIN #EV EV ON ES.ExamVersionKey = EV.ExamVersionKey AND ES.CentreKey = EV.CentreKey
		JOIN FactQuestionResponses FQR ON FQR.ExamSessionKey = ES.ExamSessionKey
		JOIN DimQuestions DQ ON DQ.CPID = FQR.CPID AND DQ.CPVersion = FQR.CPVersion
		LEFT JOIN MarkerMarks MM ON MM.CPID = FQR.CPID AND MM.ExamSessionKey = FQR.ExamSessionKey AND MM.N = 1 
		LEFT JOIN FactTestPackageMarks TPM ON TPM.CPID = FQR.CPID AND TPM.ExamSessionKey = FQR.ExamSessionKey
)
INSERT INTO #Items_raw
SELECT * FROM Items_raw 

;WITH TPMMarks AS (
	SELECT
		ExamSessionKey
		,SUM(ItemUserMark) ExamUserMark
		,SUM(ItemTotalMark) ExamTotalMark
	FROM #Items_raw
	GROUP BY
		ExamSessionKey
)
UPDATE ES
SET
	ExamUserMark = ISNULL(TPM.ExamUserMark, ES.ExamUserMark) 
	,ExamTotalMark = ISNULL(TPM.ExamTotalMark, ES.ExamTotalMark)  
FROM #ES ES
	JOIN TPMMarks TPM 
	ON ES.ExamSessionKey = TPM.ExamSessionKey
	
;WITH Seq AS (
	SELECT 
		ExamSessionKey
		,ROW_NUMBER() OVER (PARTITION BY ExamVersionKey, CentreKey ORDER BY ExamUserMark ASC, ExamSessionKey ASC) SeqN
		,ExamUserMark/ExamTotalMark*50 ExamScaledMark
	FROM #ES
)
UPDATE ES 
SET 
	SeqN = S.SeqN
	,ExamScaledMark = S.ExamScaledMark
FROM #ES ES JOIN Seq S ON ES.ExamSessionKey = S.ExamSessionKey
	
--SELECT * FROM #ES
	
	SELECT 
		#ES.ExamSessionKey
		,ExamUserMark
		,ExamTotalMark
		, KeyCode
		,ROW_NUMBER() OVER (PARTITION BY #ES.ExamVersionKey, #ES.CentreKey ORDER BY #ES.ExamUserMark ASC, #ES.ExamSessionKey ASC) SeqN
		,ExamUserMark/ExamTotalMark*50 ExamScaledMark
	FROM #ES
	inner join FactExamSessions FES
	on FES.ExamSessionKey = #ES.ExamSessionKey

--------------------------------------  Exam version level  --------------------------------------
IF OBJECT_ID('tempdb..#EV') IS NOT NULL DROP TABLE #EV
CREATE TABLE #EV (
	CentreKey int
	,ExamVersionKey int
	--,DateCompleted datetime
	,ItemN int
	,N int
	,Mean float
	,Median float
	,Mode float
	,MaxScore decimal(9,4)
	,MinScore decimal(9,4)
	,SDs float
	,SDp float
	,SDs_scaled float
	,SDp_scaled float
	,Reliability float
	,SE float
	,MeanDI float
	,MeanPBis float
	,Skewness float
	,Kurtosis float
)
INSERT INTO #EV

SELECT 
	CentreKey
	,ExamVersionKey
	--,DateCompleted
	,NULL ItemN
	,COUNT(ExamSessionKey) N
	,AVG(ExamScaledMark) Mean 
	,NULL Median
	,NULL Mode
	,MAX(ExamScaledMark) MaxScore
	,MIN(ExamScaledMark) MinScore
	,STDEV(ExamUserMark)  SDs
	,STDEVP(ExamUserMark) SDp
	,STDEV(ExamScaledMark)  SDs_scaled
	,STDEVP(ExamScaledMark) SDp_scaled
	,NULL Reliability
	,NULL SE
	,NULL MeanDI
	,NULL MeanPBis
	,NULL Skewness
	,NULL Kurtosis
FROM #ES ES
GROUP BY 
	CentreKey
	, ExamVersionKey
	
----------------------  Median & Mode  ----------------------
;WITH ES_indices AS (
	SELECT 
		CentreKey
		,ExamVersionKey
		,CEILING(MAX(SeqN)/2.+0.5) ceil
		, FLOOR(MAX(SeqN)/2.+0.5) flr
	FROM #ES ES
	GROUP BY 
		CentreKey
		,ExamVersionKey
), medians AS (
	SELECT 
		ES.CentreKey
		,ES.ExamVersionKey
		,AVG(ES.ExamScaledMark) median
	FROM 
		#ES ES
		JOIN ES_indices ESI 
			ON ES.ExamVersionKey = ESI.ExamVersionKey 
			AND ES.CentreKey = ESI.CentreKey
			AND (ES.SeqN = ESI.ceil OR ES.SeqN = ESI.flr)
	GROUP BY ES.CentreKey
		,ES.ExamVersionKey
), modes_raw AS (
	SELECT 
		ROW_NUMBER() OVER(PARTITION BY CentreKey, ExamVersionKey ORDER BY COUNT(ExamSessionKey) DESC, ExamScaledMark ASC) N
		,ROW_NUMBER() OVER(PARTITION BY CentreKey, ExamVersionKey ORDER BY COUNT(ExamSessionKey) DESC, ExamScaledMark DESC) N2
		,CentreKey
		,ExamVersionKey
		,ExamScaledMark mode
		,COUNT(ExamSessionKey) cnt 
	FROM #ES ES
	GROUP BY 
		CentreKey
		,ExamVersionKey
		,ExamScaledMark
)
UPDATE EV
SET 
	Median = ME.median
	,Mode = MO.mode
FROM 
	#EV EV
	JOIN medians ME 
		ON EV.CentreKey = ME.CentreKey 
		AND EV.ExamVersionKey = ME.ExamVersionKey
	LEFT JOIN modes_raw MO 
		ON MO.CentreKey = ME.CentreKey 
		AND MO.ExamVersionKey = ME.ExamVersionKey
		AND MO.N=1 AND MO.N2=1

----------------------  Skewness & Kurtosis  ----------------------
;WITH SessionStats_raw AS (
	SELECT 
		EV.CentreKey
		,EV.ExamVersionKey
		,(ES.ExamScaledMark - EV.Mean)/SDs_scaled us
		,N
	FROM 
		#ES ES 
		JOIN #EV EV ON ES.ExamVersionKey = EV.ExamVersionKey AND ES.CentreKey = EV.CentreKey
	WHERE 
		EV.N>2
		AND SDs_scaled<>0
), SessionStats AS (
	SELECT 
		CentreKey
		,ExamVersionKey
		,SUM(POWER(us,3))*N/((N-1.)*(N-2)) Skewness
		,CASE WHEN N=3 THEN NULL ELSE SUM(POWER(us,4))*N*(N+1.)/((N-1.)*(N-2)*(N-3))-3.*(N-1)*(N-1)/((N-2.)*(N-3)) END Kurtosis
	FROM SessionStats_raw
	GROUP BY 
		CentreKey
		,ExamVersionKey
		,N
)
UPDATE EV 
SET 
	Skewness = SS.Skewness
	,Kurtosis = SS.Kurtosis
FROM 
	#EV EV 
	JOIN SessionStats SS ON SS.CentreKey = EV.CentreKey AND SS.ExamVersionKey = EV.ExamVersionKey 

--SELECT * FROM #EV

---- Back to item(task) level: updating session- and version-level stats, calculate item-level stats ----
UPDATE IR
SET	
	ExamUserMark = ES.ExamUserMark
	,SeqN = ES.SeqN
	,N = EV.N
	,SDp = EV.SDp
FROM #Items_raw IR
	JOIN #ES ES ON IR.ExamSessionKey = ES.ExamSessionKey
	JOIN #EV EV ON IR.ExamVersionKey = EV.ExamVersionKey AND IR.CentreKey = EV.CentreKey
--SELECT * FROM #Items_raw


IF OBJECT_ID('tempdb..#Items') IS NOT NULL DROP TABLE #Items
CREATE TABLE #Items (
	CentreKey int 
	,ExamVersionKey int 	
	,CPID nvarchar(50)	
	,ItemMeanScore float
	,ItemCor float
	,PRIMARY KEY CLUSTERED (
		CentreKey ASC
		,ExamVersionKey ASC 
		,CPID ASC
	)
)

;WITH Items_MeanScores AS (
	SELECT 
		ExamVersionKey
		, CentreKey
		, CPID
		, AVG(ExamUserMark) ExamMeanMark
		, AVG(ItemUserMark) ItemMeanMark
	FROM #Items_raw IR
	GROUP BY 
		ExamVersionKey
		, CentreKey
		, CPID
), Items_DiffScores AS (
	SELECT 
		  IM.ExamVersionKey
		, IM.CentreKey
		, IM.CPID
		, IM.ItemMeanMark
		, ExamUserMark - ExamMeanMark ExamDiff
		, ItemUserMark - ItemMeanMark ItemDiff
	FROM #Items_raw I
		JOIN Items_MeanScores IM 
			ON I.ExamVersionKey = IM.ExamVersionKey AND I.CentreKey = IM.CentreKey AND I.CPID = IM.CPID
), Items_cor AS (
	SELECT 
		ID.CentreKey
		, ID.ExamVersionKey
		, ID.CPID
		, AVG(ItemMeanMark)ItemMeanMark
		, SUM(ExamDiff*ItemDiff)/SQRT(SUM(ExamDiff*ExamDiff)*SUM(ItemDiff*ItemDiff)) cor
	FROM Items_DiffScores ID
	GROUP BY 
			ExamVersionKey
			, CentreKey
			, CPID
	HAVING
		SUM(ExamDiff*ExamDiff)*SUM(ItemDiff*ItemDiff)<>0
)
INSERT INTO #Items
SELECT * FROM Items_cor


--SELECT * FROM #Items

------------------------------------  Component(item) level  ------------------------------------
IF OBJECT_ID('tempdb..#Components') IS NOT NULL DROP TABLE #Components
CREATE TABLE #Components (
	CentreKey int
	,ExamVersionKey int
	,CPID nvarchar(50)
	,CID nvarchar(50)
	,OptionKey int
	--,FV float
	,Variance float
	,DI float
	,PBis float
	,PRIMARY KEY CLUSTERED (
		CentreKey ASC
		,ExamVersionKey ASC 
		,CID ASC
		,OptionKey ASC
	)
)
IF OBJECT_ID('tempdb..#Components_raw') IS NOT NULL DROP TABLE #Components_raw
CREATE TABLE #Components_raw (
	CentreKey int 
	,ExamVersionKey int 
	,ExamSessionKey int 
	,SeqN int
	,N int
	,SDp float
	,ExamUserMark decimal(9,4)
	,CPID nvarchar(50)
	,CID nvarchar(50)
	,OptionKey int
	,CPVersion int
	,QuestionType int
	,ComponentUserMark decimal(9,4)
	,ComponentTotalMark decimal(9,4)
	,HasCPAuditEntry int
	,CalculatePbis bit

	,PRIMARY KEY CLUSTERED (
		CentreKey ASC
		,ExamVersionKey ASC 
		,ExamSessionKey ASC
		,CID ASC
		,OptionKey ASC
	)
)

;WITH Components_raw AS (	
	SELECT 
		I.CentreKey
		,I.ExamVersionKey
		,I.ExamSessionKey
		,I.SeqN
		,I.N
		,I.SDp
		,I.ExamUserMark
		,I.CPID
		,DC.CID
		,-1 OptionKey
		,I.CPVersion
		,DC.QuestionType
		, ISNULL(
			ROUND(
				CASE 
					WHEN I.AutoMarked = 1 --MCQ
						AND I.FinalExamState = 12 --W/O SecureMarker
						THEN FCR.Mark*DC.TotalMark
					ELSE I.ItemUserMark*DC.Weight
				END
			,4)
		  , 0
		) ComponentUserMark
		,DC.TotalMark ComponentTotalMark
		,DC.HasCPAuditEntry
		,CASE 
			WHEN QuestionType = 10 AND HasCPAuditEntry = 1 AND DC.TotalMark = 1 AND DC.ItemXML.value('count(/C/I[@cor=1])','int')=1 THEN 1
			WHEN QuestionType = 12 THEN 1
			ELSE NULL
		END CalculatePbis

	FROM #Items_raw I
		JOIN DimComponents DC
			ON DC.CPID = I.CPID
			AND DC.CPVersion = I.CPVersion
			AND DC.Weight>0
			AND DC.TotalMark>0
		LEFT JOIN FactComponentResponses FCR 
			ON DC.CID = FCR.CID
			AND I.CPVersion = FCR.CPVersion
			AND I.ExamSessionKey = FCR.ExamSessionKey
	WHERE 
		DC.QuestionType<>16
		AND DC.QuestionType<>13
		AND (
			DC.QuestionType<>12
			OR DC.ItemXML.value('count(C/I)[1]', 'integer') = 1
		)
		OR DC.MarkingMethod=1
		OR I.AutoMarked=0
)
INSERT INTO #Components_raw
SELECT * FROM Components_raw

;WITH Components_raw2 AS (	
	SELECT 
		I.CentreKey
		,I.ExamVersionKey
		,I.ExamSessionKey
		,I.SeqN
		,I.N
		,I.SDp
		,I.ExamUserMark
		,I.CPID
		,DO.CID
		,DO.OptionKey
		,I.CPVersion
		,DO.OptionType
		,ISNULL(FR.Mark,
			CASE WHEN 
				FR.ExamSessionKey IS NULL AND DO.CorrectAnswersCount>0 --If a drop zone has correct answer but a candidate hasn't placed any item there, he gets 0.
			THEN 0 ELSE NULL END
		) ComponentUserMark
		,1 ComponentTotalMark
		,0 HasCPAuditEntry
		,CASE WHEN DO.OptionType = 17 AND DO.CorrectAnswersCount <> 1 THEN 0 ELSE 1 END CalculatePbis --For re-ordering and grouped picklists we always have only 1 correct answer

	FROM #Items_raw I
		JOIN DimOptions DO
			ON DO.CPID = I.CPID
			AND DO.CPVersion = I.CPVersion
			AND (
				(DO.OptionType=17 AND DO.CorrectAnswersCount IS NOT NULL) --exclude distractor drop zones 
				OR DO.OptionType=13
				OR DO.OptionType=12
			)
			AND DO.ComponentMarkingMethod = 0
			AND I.AutoMarked = 1
		LEFT JOIN FactOptionResponses FR
			ON DO.OID = FR.OID
			AND DO.CPVersion = FR.CPVersion
			AND I.ExamSessionKey = FR.ExamSessionKey
	--WHERE DO.CorrectAnswersCount=1
)
INSERT INTO #Components_raw
SELECT * FROM Components_raw2

--SELECT * FROM #Components_raw


;WITH ComponentStats AS (
	SELECT 
		CentreKey
		,ExamVersionKey
		,CPID
		,CID
		,OptionKey
		--,AVG(ComponentUserMark/ComponentTotalMark) FV
		,VAR(ComponentUserMark) Variance
	FROM #Components_raw
	GROUP BY ExamVersionKey, CentreKey, CID, CPID, OptionKey
), DI_lo AS (
	SELECT 
		CentreKey
		, ExamVersionKey
		, CID
		, OptionKey
		, AVG(ComponentUserMark/ComponentTotalMark) FV
	FROM #Components_raw
	WHERE 
		--Round .5 to the nearest even number (R-3/SAS-2 type from http://en.wikipedia.org/wiki/Quantile)
		SeqN<=CASE WHEN CEILING(0.27*N) - 0.27*N = 0.27*N - FLOOR(0.27*N) AND ROUND(0.27*N, 0)%2<>0 THEN FLOOR(0.27*N) ELSE ROUND(0.27*N,0) END
	GROUP BY CID, ExamVersionKey, CentreKey, OptionKey
), DI_hi AS (
	SELECT 
		CentreKey
		, ExamVersionKey
		, CID
		, OptionKey
		, AVG(ComponentUserMark/ComponentTotalMark) FV
	FROM #Components_raw
	WHERE 
		--Round .5 to the nearest even number (R-3/SAS-2 type from http://en.wikipedia.org/wiki/Quantile)
		SeqN>=CASE WHEN CEILING(0.73*N) - 0.73*N = 0.73*N - FLOOR(0.73*N) AND ROUND(0.73*N, 0)%2<>0 THEN FLOOR(0.73*N) ELSE ROUND(0.73*N,0) END
	GROUP BY CID, ExamVersionKey, CentreKey, OptionKey
), DIs AS (
	SELECT 
		DI_lo.ExamVersionKey
		, DI_lo.CentreKey
		, DI_lo.CID
		, DI_lo.OptionKey
		, DI_hi.FV - DI_lo.FV DI
	FROM  
		DI_lo JOIN DI_hi 
			ON DI_lo.CID = DI_hi.CID 
			AND DI_lo.ExamVersionKey = DI_hi.ExamVersionKey 
			AND DI_lo.CentreKey = DI_hi.CentreKey
			AND DI_lo.OptionKey = DI_hi.OptionKey
			
), Components AS (
	SELECT 
		CS.*
		,DIs.DI
		,NULL PBis
	FROM 
		ComponentStats CS
		LEFT JOIN DIs ON CS.ExamVersionKey = DIs.ExamVersionKey AND CS.CentreKey = DIs.CentreKey AND CS.CID = DIs.CID AND CS.OptionKey = DIs.OptionKey
)
INSERT INTO #Components
SELECT * FROM Components

--SELECT * FROM #Components

;WITH ComponentsMCQ AS (
	SELECT 
		C.*
	FROM #Components_raw C
	WHERE 
		C.CalculatePbis = 1 --implies CorrectAnswerCount=1
		AND ComponentTotalMark=1
), ComponentsPBis_raw AS (
	SELECT 
		CentreKey
		, ExamVersionKey
		, CID
		, OptionKey
		, SDp Sx 
		, SUM(ComponentUserMark*ExamUserMark)/SUM(ComponentUserMark) Xp
		, SUM((1-ComponentUserMark)*ExamUserMark)/SUM(1-ComponentUserMark) Xq
		, CAST(SUM(ComponentUserMark) as float)/COUNT(ComponentUserMark) p
	FROM  ComponentsMCQ

	GROUP BY ExamVersionKey, CentreKey, CID, SDp, OptionKey
	HAVING 
			SUM(ComponentUserMark)>0
			AND SUM(1-ComponentUserMark)>0
), ComponentsPBis AS (
	SELECT 
		CentreKey
		,ExamVersionKey
		,CID
		,OptionKey
		,CASE WHEN Sx=0 THEN NULL ELSE (Xp-Xq)/Sx*SQRT(p*(1-p)) END PBis
	FROM ComponentsPBis_raw
)

UPDATE C
	SET PBis = CPB.PBis
--SELECT *
FROM
	#Components C
	JOIN ComponentsPBis CPB ON C.ExamVersionKey = CPB.ExamVersionKey AND C.CentreKey = CPB.CentreKey AND C.CID = CPB.CID AND C.OptionKey = CPB.OptionKey
	
--SELECT * FROM #Components

---------------------- Reliability, SE, Component number, mean DI & PBis ----------------------
;WITH ComponentStats AS (
	SELECT 
		EV.CentreKey
		,EV.ExamVersionKey	
		, CASE 
			WHEN 
				COUNT(CID) = 1 
				OR SUM(Variance)=0
				OR SDs=0
			THEN NULL 
			ELSE COUNT(CID)/(COUNT(CID)-1.) * (1-SUM(Variance)/(SDs*SDs)) 
		 END Cronbach
		,COUNT(CID) k
		,AVG(DI) MeanDI
		,AVG(PBis) MeanPBis
	FROM #Components C 
	JOIN #EV EV ON EV.CentreKey = C.CentreKey AND EV.ExamVersionKey = C.ExamVersionKey
	--WHERE SDp<>0
	GROUP BY EV.CentreKey
		,EV.ExamVersionKey
		,EV.SDs
)
UPDATE EV 
SET 
	ItemN = CS.k
	,Reliability = CASE WHEN CS.Cronbach>1 THEN NULL ELSE CS.Cronbach END
	,MeanDI = CS.MeanDI 
	,MeanPBis = CS.MeanPBis
	,SE = CASE WHEN CS.Cronbach>1 THEN NULL ELSE SDp_scaled*SQRT(1-CS.Cronbach) END
FROM 
	#EV EV 
	JOIN ComponentStats CS ON EV.CentreKey = CS.CentreKey AND EV.ExamVersionKey = CS.ExamVersionKey


---------------------- Quintiles & final query ----------------------
;WITH ScoreFrequency_raw AS (
	SELECT 
		ES.ExamVersionKey
		,ES.CentreKey
		,ROUND(ExamScaledMark,0) X 
		,COUNT(ExamSessionKey) QU_cnt
	FROM #ES ES
	GROUP BY 
		ES.ExamVersionKey
		,ES.CentreKey
		,ROUND(ExamScaledMark,0)
), ScoreFrequency AS (
	SELECT * 
	FROM 
		ScoreFrequency_raw
	PIVOT (
		SUM(QU_cnt)
		FOR X IN ([0], [1], [2], [3], [4], [5], [6], [7], [8], [9], [10], [11], [12], [13], [14], [15], [16], [17], [18], [19], [20], [21], [22], [23], [24], [25], [26], [27], [28], [29], [30], [31], [32], [33], [34], [35], [36], [37], [38], [39], [40], [41], [42], [43], [44], [45], [46], [47], [48], [49], [50])
	) AS p
)





SELECT 
	DC.CentreName
	,EV.CentreKey
	,EV.ExamVersionKey
	,EV.N
	,EV.ItemN
	,EV.Mean
	,EV.Median
	,EV.Mode
	,EV.MaxScore
	,EV.MinScore
	,EV.SDs_scaled
	,EV.SDp_scaled
	,EV.Reliability
	,EV.SE
	,EV.MeanDI
	,EV.MeanPBis
	,EV.Skewness
	,EV.Kurtosis
	,SF.[0], SF.[1], SF.[2], SF.[3], SF.[4], SF.[5], SF.[6], SF.[7], SF.[8], SF.[9], SF.[10], SF.[11], SF.[12], SF.[13], SF.[14], SF.[15], SF.[16], SF.[17], SF.[18], SF.[19], SF.[20], SF.[21], SF.[22], SF.[23], SF.[24], SF.[25], SF.[26], SF.[27], SF.[28], SF.[29], SF.[30], SF.[31], SF.[32], SF.[33], SF.[34], SF.[35], SF.[36], SF.[37], SF.[38], SF.[39], SF.[40], SF.[41], SF.[42], SF.[43], SF.[44], SF.[45], SF.[46], SF.[47], SF.[48], SF.[49], SF.[50]
FROM 
	#EV EV 
	JOIN DimCentres DC ON DC.CentreKey = EV.CentreKey
	JOIN ScoreFrequency SF ON SF.ExamVersionKey = EV.ExamVersionKey AND SF.CentreKey = EV.CentreKey
	
IF OBJECT_ID('tempdb..#ES') IS NOT NULL DROP TABLE #ES
IF OBJECT_ID('tempdb..#Items_raw') IS NOT NULL DROP TABLE #Items_raw
IF OBJECT_ID('tempdb..#EV') IS NOT NULL DROP TABLE #EV
IF OBJECT_ID('tempdb..#Items') IS NOT NULL DROP TABLE #Items
IF OBJECT_ID('tempdb..#Components') IS NOT NULL DROP TABLE #Components
IF OBJECT_ID('tempdb..#Components_raw') IS NOT NULL DROP TABLE #Components_raw