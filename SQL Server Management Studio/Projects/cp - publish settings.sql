select ID [ProjectID]
	, Name [ProjectName]
	, PublishSettingsXml.value('data(PublishConfig/ib3Address)[1]','nvarchar(max)') PublishAddress
from ProjectListTable
where PublishSettingsXml.exist('PublishConfig/ib3Address/text()') = 1
 