SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

--declare @url nvarchar(MAX) = 'https://windesheim.surpass.com/#ItemAuthoring/Subject/1046/Item/Edit'

declare @CAItems table (
	Corrupted_ItemId INT
	, mathMI nvarchar(MAX)
)
INSERT @CAItems
SELECT [Id] 
	, [MathMl] 
FROM [Windesheim_ContentAuthor].[dbo].[Items] 
WHERE [MathMl] Like N'%Computed%' OR [MathMl] Like N'%Error:%'  



SELECT  'bcp "select itemFile from Windesheim_ItemBank.dbo.ItemTable where projectID = '+cast(ProjectID as nvarchar(10))+' and ItemRef = '+cast(ItemRef as nvarchar(10))+' and [version] = '+cast([version] as nvarchar(10))+'"
		queryout T:\Wiris\'+cast(ProjectID as nvarchar(10))+'_'+cast(ItemRef as nvarchar(10))+'_'+cast(Version as nvarchar(10))+'.zip -S '+@@SERVERNAME+' -T -N	
	'
FROM (
	select 
		a.ProjectID
		, a.ItemRef
		, a.Version
		, a.ItemFile
		, ROW_NUMBER() OVER(
			PARTITION BY a.ProjectID
				, a.ItemRef
					ORDER BY
						a.ProjectID
		, a.ItemRef
		, a.Version
			asc) r
	from Windesheim_ItemBank..ItemTable a
	inner join @CaItems b
	on a.ItemRef = b.Corrupted_ItemId
) a
--order by  1,2,3