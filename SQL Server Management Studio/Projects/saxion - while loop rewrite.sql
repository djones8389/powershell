﻿use saxion_Secureassess

DECLARE @examInstanceId   int  = 262893

SET STATISTICS IO ON 
--BEGIN  
  
DECLARE @errorReturnString  nvarchar(max)  
DECLARE @errorNum  nvarchar(100)  
DECLARE @errorMess  nvarchar(max)  
DECLARE @myExamState int
DECLARE @structureXml xml
DECLARE @defaultDuration INT
DECLARE @computedTotalDuration INT
DECLARE @durationMode INT
DECLARE @enableCandidateBreak TINYINT
DECLARE @unscheduledBreakMinutes INT
DECLARE @unscheduledBreakMinutesUsed INT
DECLARE @candidateBreakStyle INT
DECLARE @scheduledBreakType INT
DECLARE @breakPooledTime INT
DECLARE @maximumNumberOfBreaksPerSection INT
DECLARE @totalBreakTime INT
DECLARE @totalBreakTimeUsed INT
DECLARE @isUnlimitedBreaks TINYINT
DECLARE @totalSectionBreakTime INT
DECLARE @candidateBreakExtraDurationType INT
DECLARE @candidateBreakExtraDuration INT
DECLARE @numberOfExtraBreaksPerSection INT
DECLARE @candidateBreakExtraBreakReasonType INT
DECLARE @candidateBreakExtraDurationReasonText nvarchar(100)
DECLARE @candidateBreakExtraDurationReasonOther nvarchar(250)
DECLARE @originalBreakPooledTime INT
DECLARE @originalMaximumNumberOfBreaksPerSection INT
DECLARE @originalTotalBreakTime INT		
DECLARE @extendedXml XML
DECLARE @customRoundingEnabled INT
DECLARE @customRoundingDigits INT
DECLARE @rowId INT
DECLARE @assessmentSectionType INT
DECLARE @sectionId INT
DECLARE @duration INT
DECLARE @overrideLockdown INT
DECLARE @hasSectionBreak TINYINT
DECLARE @breakDuration INT
DECLARE @originalBreakDuration INT
DECLARE @cancellableBreak TINYINT
DECLARE @numberOfBreaksUsed SMALLINT
DECLARE @scheduledBreakStarted TINYINT
DECLARE @sectionSelectorID INT,
		@description NVARCHAR(50),
		@sectionSelected TINYINT,
		@forwardOnly TINYINT,
		@requiredResponse TINYINT,
		@isBranchingSection TINYINT,
		@adaptiveEnabled BIT,
		@adaptiveType  NVARCHAR(50),
		@noReturnToSection TINYINT,
		@forwardOnlyAllowItemReview TINYINT,
		@forwardOnlyShowBreadcrumbs TINYINT

DECLARE @TEMP_EXAM_SECTIONS TABLE (
	[RowID] INT IDENTITY(1, 1) NOT NULL
	,[ExamSessionId] INT
	,[SectionID] INT
	,[ExamSectionType] INT
	,[Duration] INT
	,[OverrideLockdown] BIT
	,[HasSectionBreak] BIT
	,[BreakDuration] INT
	,[OriginalBreakDuration] INT
	,[CancellableBreak] BIT
	,[NumberOfBreaksUsed] SMALLINT
	,[ScheduledBreakStarted] BIT
	,[Description] NVARCHAR(50)
	,[SectionSelectorID] INT
	,SectionSelected BIT
	,ForwardOnly BIT
	,RequiredResponse BIT
	,IsBranchingSection BIT
	,[AdaptiveEnabled] BIT
	,[AdaptiveType] NVARCHAR(50)
	,[NoReturnToSection] BIT
	,[ForwardOnlyAllowItemReview] BIT
	,[ForwardOnlyShowBreadcrumbs] BIT
	)


DECLARE @TEMP_SCALES_SCORES TABLE (
	[RowID] INT IDENTITY(1, 1) NOT NULL
	,[RawScore] DECIMAL(18, 4)
	,[ScaleScore] DECIMAL(18, 4)
	)


  IF (EXISTS(SELECT id FROM [dbo].[ExamSessionTable] WHERE [ID] = @examInstanceId))  
   BEGIN  
    SELECT @myExamState=[examState], 
		   @defaultDuration = [DefaultDuration] , 
		   @computedTotalDuration = [ComputedTotalDuration]
    FROM [dbo].[ExamSessionTable] WHERE [ID] = @examInstanceId    

 --   -- if exam is in progress then we need to re-sync live session data before  
 --   -- the exam script is fetched from the ExamSessionTable  
	--IF ((@myExamState = 6) OR (@myExamState = 7))  
	--BEGIN  
	--	EXEC [sa_ASSESSMENTSERVICE_SyncStructureXmlWithLiveTables_sp] @examInstanceId  
 --   END  
    
    
    SELECT @totalSectionBreakTime = SUM(ISNULL([BreakDuration], 0))
                                       FROM [dbo].[ExamSectionsTable]
                                       Where ExamSessionID = @examInstanceId
                                       Group by ExamSessionID    

	SELECT @structureXml = [StructureXML],--.query('assessmentDetails/assessment'), 
			@durationMode = DurationMode,
			@enableCandidateBreak = enableCandidateBreak,
			@candidateBreakStyle = candidateBreakStyle,  -- Scheduled / Unscheduled
			@scheduledBreakType = scheduledBreakType,    -- Fixed/Pooled
			@breakPooledTime = breakPooledTime,
			@originalBreakPooledTime = breakPooledTime,
			@maximumNumberOfBreaksPerSection = maximumNumberOfBreaksPerSection,
			@originalMaximumNumberOfBreaksPerSection = maximumNumberOfBreaksPerSection,						
			@totalBreakTime = totalBreakTime,
			@originalTotalBreakTime = totalBreakTime,
			@totalBreakTimeUsed = totalBreakTimeUsed,
			@isUnlimitedBreaks = isUnlimitedBreaks,
			@unscheduledBreakMinutes = UnscheduledBreakMin,
			@unscheduledBreakMinutesUsed = UnscheduledBreakMinUsed,
			@candidateBreakExtraDurationType = ISNULL(CandidateBreakExtraDurationType, 0),
            @candidateBreakExtraDuration = ISNULL(CandidateBreakExtraDuration, 0),
            @numberOfExtraBreaksPerSection = ISNULL(NumberOfExtraBreaksPerSection, 0),
            @candidateBreakExtraBreakReasonType = ISNULL(CandidateBreakExtraBreakReasonType, 0),
            @candidateBreakExtraDurationReasonText = ISNULL(CandidateBreakExtraDurationReasonText, 0),
            @candidateBreakExtraDurationReasonOther = ISNULL(CandidateBreakExtraDurationReasonOther, 0),
		    @extendedXml	= [ExtendedXml],
		    @customRoundingEnabled = CustomRoundingEnabled,
		    @customRoundingDigits = CustomRoundingDigits
	FROM [dbo].[ExamSessionTable] WHERE [ID] = @examInstanceId  
    
	 
		INSERT INTO @TEMP_EXAM_SECTIONS ([ExamSessionId]
					,[SectionID]
					,[ExamSectionType]
					,[Duration]
					,[OverrideLockdown]
					,[HasSectionBreak]
					,[BreakDuration]
					,[OriginalBreakDuration]
					,[CancellableBreak]
					,[NumberOfBreaksUsed]
					,[ScheduledBreakStarted]
					,[Description]
					,[SectionSelectorID]
					,[SectionSelected]
					,[ForwardOnly]
					,[RequiredResponse]
					,[IsBranchingSection]
					,[AdaptiveEnabled]
					,[AdaptiveType]
					,[NoReturnToSection]
					,[ForwardOnlyAllowItemReview]
					,[ForwardOnlyShowBreadcrumbs])
	SELECT			 [ExamSessionId]
					,[SectionID]
					,[ExamSectionType]
					,[Duration]
					,[OverrideLockdown]
					,[HasSectionBreak]
					,CEILING((CASE					    
								WHEN @enableCandidateBreak = 1 AND @candidateBreakStyle = 0 AND @scheduledBreakType = 0 AND @candidateBreakExtraDurationType = 0  THEN ([BreakDuration] + (CONVERT(decimal, [BreakDuration])/CONVERT(decimal, @totalSectionBreakTime) * @candidateBreakExtraDuration))
								WHEN @enableCandidateBreak = 1 AND @candidateBreakStyle = 0 AND @scheduledBreakType = 0 AND @candidateBreakExtraDurationType = 1  Then ([BreakDuration] + (CONVERT(decimal, @candidateBreakExtraDuration)/100 * [BreakDuration]))
								ELSE  [BreakDuration]					   
							  END))
                    ,[BreakDuration]							  
					,[CancellableBreak]
					,[NumberOfBreaksUsed]
					,[ScheduledBreakStarted]
					,[Description]
					,[SectionSelectorID]
					,[SectionSelected]
					,[ForwardOnly]
					,[RequiredResponse]
					,CASE WHEN BranchingResultSet.r.value('@sectionID', 'INT') = 0 THEN 1
					      WHEN BranchingResultSet.r.value('@sectionID', 'INT') IS NULL THEN 0
						  ELSE BranchingResultSet.r.value('@sectionID', 'INT') END AS [BranchingSection]
					,[AdaptiveEnabled]
					,[AdaptiveType]
					,[NoReturnToSection]
					,[ForwardOnlyAllowItemReview]
					,[ForwardOnlyShowBreadcrumbs]
			FROM [dbo].[ExamSectionsTable]                                           
			LEFT JOIN @extendedXml.nodes('/extendedXml/branching/fileContent//group') AS BranchingResultSet(r) ON  BranchingResultSet.r.value('@sectionID', 'INT') = SectionID	
			WHERE [ExamSessionId] = @examInstanceId
			ORDER BY [ExamSectionType]

			-- if this is a timed sections exam, we want default duration to be the sum of the section durations, not the DefaultDuration from exam session table
			-- this calls out to a table valued function
			IF @durationMode is not null and @durationMode = 2
				BEGIN
					select @defaultDuration = TotalDuration from [dbo].[CalculateTotalDurationForExamSession](@examInstanceId)
				END
   

	IF @enableCandidateBreak = 1 -- candidate break enabled
	 BEGIN
		IF @candidateBreakStyle = 0  -- Scheduled break
		BEGIN
			 IF @scheduledBreakType = 1 -- Pool time
			 BEGIN
		    		IF @candidateBreakExtraDurationType = 0  -- Miniutes
						SET @breakPooledTime = @breakPooledTime + @candidateBreakExtraDuration
					ELSE  -- Percentage
						SET @breakPooledTime = @breakPooledTime + CEILING((@breakPooledTime * CONVERT(decimal, @candidateBreakExtraDuration)/100))                       
			  END					
		END
		ELSE IF @candidateBreakStyle = 1 -- Unschedule break 
		BEGIN
			IF @candidateBreakExtraDurationType = 0  -- Miniutes
			   SET @totalBreakTime = @totalBreakTime + @candidateBreakExtraDuration
			ELSE  -- Percentage
			   SET @totalBreakTime = @totalBreakTime + CEILING((@totalBreakTime * CONVERT(decimal, @candidateBreakExtraDuration)/100))    
		                         
			IF @isUnlimitedBreaks = 0 -- If not unlimited number of breaks
				   SET @maximumNumberOfBreaksPerSection = @maximumNumberOfBreaksPerSection + @numberOfExtraBreaksPerSection
		END            
	END


	WHILE (SELECT COUNT(*) FROM @TEMP_EXAM_SECTIONS) > 0
	BEGIN

		SELECT TOP 1 @rowId = [RowID], 
					@assessmentSectionType = [ExamSectionType],
					@sectionId = [SectionID], 
					@duration = [Duration],
					@overrideLockdown = [OverrideLockdown],
					@hasSectionBreak = [HasSectionBreak],
					@breakDuration = [BreakDuration],
					@cancellableBreak = [CancellableBreak],
					@numberOfBreaksUsed = [NumberOfBreaksUsed],
					@scheduledBreakStarted = [ScheduledBreakStarted],
					@originalBreakDuration = [OriginalBreakDuration],
					@sectionSelectorID = [SectionSelectorID],
					@description = [Description],
					@sectionSelected = SectionSelected,
					@forwardOnly = ForwardOnly,
					@requiredResponse = RequiredResponse,
					@isBranchingSection = [IsBranchingSection],
					@adaptiveEnabled = [AdaptiveEnabled],
					@adaptiveType = [AdaptiveType],
					@noReturnToSection = [NoReturnToSection],
					@forwardOnlyAllowItemReview = [ForwardOnlyAllowItemReview],
					@forwardOnlyShowBreadcrumbs = [ForwardOnlyShowBreadcrumbs]
			FROM @TEMP_EXAM_SECTIONS ORDER BY [ExamSectionType]


			-- alter the assessment sections xml to include Duration and OverrideLockdown as attribute to Intro, Outro and Section nodes

				-- check if section requires extra time due to special requirements
				DECLARE @timeExtensionRatio FLOAT 
				DECLARE @sectionScheduledDuration FLOAT		
				SET @timeExtensionRatio = 
					CASE 
						WHEN @defaultDuration > 0 
							AND @computedTotalDuration > @defaultDuration --this also removes all NULL combinations
							THEN CAST(@computedTotalDuration AS FLOAT) / CAST(@defaultDuration AS FLOAT)
						ELSE 
							-- if no special requirement bonus time, just set ratio to 1
							1
					END
				SET @sectionScheduledDuration = ROUND((@duration * @timeExtensionRatio),4)

				-- Intro
				IF @assessmentSectionType = 1
					BEGIN
						--Intro and Outro don't support sectionScheduledDuration atribute so have to modify duration
						SET @structureXml.modify('insert attribute duration {sql:variable("@sectionScheduledDuration")} into (/assessmentDetails/assessment/intro)[1]')
						SET @structureXml.modify('insert attribute overrideLockdown {sql:variable("@overrideLockdown")} into (/assessmentDetails/assessment/intro)[1]')
					END
				-- Outro
				ELSE IF @assessmentSectionType = 2
					BEGIN
						--Intro and Outro don't support sectionScheduledDuration atribute so have to modify duration
						SET @structureXml.modify('insert attribute duration {sql:variable("@sectionScheduledDuration")} into (/assessmentDetails/assessment/outro)[1]')
						SET @structureXml.modify('insert attribute overrideLockdown {sql:variable("@overrideLockdown")} into (/assessmentDetails/assessment/outro)[1]')
					END
				-- Section (there may be several therefore search by id)
				ELSE IF @assessmentSectionType = 4
					BEGIN
						SET @structureXml.modify('insert attribute duration {sql:variable("@duration")} into (/assessmentDetails/assessment/section[@id = sql:variable("@sectionId")])[1]')
						SET @structureXml.modify('insert attribute overrideLockdown {sql:variable("@overrideLockdown")} into (/assessmentDetails/assessment/section[@id = sql:variable("@sectionId")])[1]')
						SET @structureXml.modify('insert attribute scheduledDuration {sql:variable("@sectionScheduledDuration")} into (/assessmentDetails/assessment/section[@id = sql:variable("@sectionId")])[1]')

						-- Candidate Breaks
						SET @structureXml.modify('insert attribute hasSectionBreak {sql:variable("@hasSectionBreak")} into (/assessmentDetails/assessment/section[@id = sql:variable("@sectionId")])[1]')
						SET @structureXml.modify('insert attribute breakDuration {sql:variable("@breakDuration")} into (/assessmentDetails/assessment/section[@id = sql:variable("@sectionId")])[1]')
						SET @structureXml.modify('insert attribute cancellableBreak {sql:variable("@cancellableBreak")} into (/assessmentDetails/assessment/section[@id = sql:variable("@sectionId")])[1]')
						SET @structureXml.modify('insert attribute numberOfBreaksUsed {sql:variable("@numberOfBreaksUsed")} into (/assessmentDetails/assessment/section[@id = sql:variable("@sectionId")])[1]')
						SET @structureXml.modify('insert attribute scheduledBreakStarted {sql:variable("@scheduledBreakStarted")} into (/assessmentDetails/assessment/section[@id = sql:variable("@sectionId")])[1]')
						SET @structureXml.modify('insert attribute originalBreakDuration {sql:variable("@originalBreakDuration")} into (/assessmentDetails/assessment/section[@id = sql:variable("@sectionId")])[1]')

						--Section Selector
						SET @structureXml.modify('insert attribute description {sql:variable("@description")} into (/assessmentDetails/assessment/section[@id = sql:variable("@sectionId")])[1]')

						IF @sectionSelectorID IS NOT NULL
						BEGIN
							SET @structureXml.modify('insert attribute sectionSelectorId {sql:variable("@sectionSelectorID")} into (/assessmentDetails/assessment/section[@id = sql:variable("@sectionId")])[1]')
							SET @structureXml.modify('insert attribute selected {sql:variable("@sectionSelected")} into (/assessmentDetails/assessment/section[@id = sql:variable("@sectionId")])[1]')							
						END

						--Forward Only Section, Required Response Section
						SET @structureXml.modify('insert attribute forwardOnly {sql:variable("@forwardOnly")} into (/assessmentDetails/assessment/section[@id = sql:variable("@sectionId")])[1]')
						SET @structureXml.modify('insert attribute requiredResponse {sql:variable("@requiredResponse")} into (/assessmentDetails/assessment/section[@id = sql:variable("@sectionId")])[1]')



						-- No Return to section, Allow item review, show breadcrumbs
						SET @structureXml.modify('insert attribute noReturnToSection {sql:variable("@noReturnToSection")} into (/assessmentDetails/assessment/section[@id = sql:variable("@sectionId")])[1]')
						SET @structureXml.modify('insert attribute forwardOnlyAllowItemReview {sql:variable("@forwardOnlyAllowItemReview")} into (/assessmentDetails/assessment/section[@id = sql:variable("@sectionId")])[1]')
						SET @structureXml.modify('insert attribute forwardOnlyShowBreadcrumbs {sql:variable("@forwardOnlyShowBreadcrumbs")} into (/assessmentDetails/assessment/section[@id = sql:variable("@sectionId")])[1]')
						
						--Extended XML -> Branching XML -> Extended XML column located in the ExamSession table, is an extensible column which could be used to add additional XML information required within the
						                                -- ExamSession instead of creating a new column.
						SET @structureXml.modify('insert attribute isBranchingSection {sql:variable("@isBranchingSection")} into (/assessmentDetails/assessment/section[@id = sql:variable("@sectionId")])[1]')

						--AdaptiveEnabled and AdaptiveType
						IF @adaptiveEnabled = 1
						BEGIN
							SET @structureXml.modify('insert attribute adaptiveEnabled {"1"} into (/assessmentDetails/assessment/section[@id = sql:variable("@sectionId")])[1]')
							SET @structureXml.modify('insert attribute adaptiveType {sql:variable("@adaptiveType")} into (/assessmentDetails/assessment/section[@id = sql:variable("@sectionId")])[1]')
						END

					END

		Delete @TEMP_EXAM_SECTIONS Where [RowID] = @rowId
	END
END

SELECT @structureXml