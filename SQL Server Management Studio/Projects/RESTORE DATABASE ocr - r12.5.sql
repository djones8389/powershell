declare @OCR_SecureAssess nvarchar(MAX)=''
select @OCR_SecureAssess +=CHAR(13)  + 'kill ' + convert(varchar(5),spid)
from sys.sysprocesses s
where dbid > 4
	and  db_name(dbid) = 'OCR_SecureAssess'
EXEC(@OCR_SecureAssess)   
 
RESTORE DATABASE [OCR_SecureAssess]
FROM DISK = N'T:\BACKUP\Ocr_SecureAssess.bak'
WITH FILE = 2
	,NOUNLOAD
	,NOREWIND
	,REPLACE
	,STATS = 10
	,MOVE 'Data' TO N'S:\DATA\OCR_SecureAssess.mdf'
	,MOVE 'Log' TO N'L:\LOGS\OCR_SecureAssess.ldf';


declare @Ocr_ItemBank nvarchar(MAX)=''
select @Ocr_ItemBank +=CHAR(13)  + 'kill ' + convert(varchar(5),spid)
from sys.sysprocesses s
where dbid > 4
	and  db_name(dbid) = 'Ocr_ItemBank'
EXEC(@Ocr_ItemBank)   

RESTORE DATABASE [Ocr_ItemBank]
FROM DISK = N'T:\Backup\Ocr_ItemBank.bak'
WITH FILE = 1
	,NOUNLOAD
	,NOREWIND
	,REPLACE
	,STATS = 10
	,MOVE 'Ocr_ItemBank_2017-06-23_095535' TO N'S:\DATA\Ocr_ItemBank.mdf'
	,MOVE 'Ocr_ItemBank_2017-06-23_095535_log' TO N'L:\LOGS\Ocr_ItemBank.ldf';



declare @Kill nvarchar(MAX)=''
select @Kill +=CHAR(13)  + 'kill ' + convert(varchar(5),spid)
from sys.sysprocesses s
where dbid > 4
	and  db_name(dbid) like 'OCR%'
EXEC(@Kill)   
RESTORE DATABASE [Ocr_AnalyticsManagement]
FROM DISK = N'T:\BACKUP\R12.5 KH DBs\OCR R12.5.2017.06.26.bak'
WITH FILE = 1
	,NOUNLOAD
	,NOREWIND
	,REPLACE
	,STATS = 10
	,MOVE 'Data' TO N'S:\DATA\Ocr_AnalyticsManagement.mdf'
	,MOVE 'Log' TO N'L:\LOGS\Ocr_AnalyticsManagement.ldf';
declare @Kill nvarchar(MAX)=''
select @Kill +=CHAR(13)  + 'kill ' + convert(varchar(5),spid)
from sys.sysprocesses s
where dbid > 4
	and  db_name(dbid) like 'OCR%'
EXEC(@Kill)   
RESTORE DATABASE [Ocr_ContentAuthor]
FROM DISK = N'T:\BACKUP\R12.5 KH DBs\OCR R12.5.2017.06.26.bak'
WITH FILE = 2
	,NOUNLOAD
	,NOREWIND
	,REPLACE
	,STATS = 10
	,MOVE 'Data' TO N'S:\DATA\Ocr_ContentAuthor.mdf'
	,MOVE 'Log' TO N'L:\LOGS\Ocr_ContentAuthor.ldf';
declare @Kill nvarchar(MAX)=''
select @Kill +=CHAR(13)  + 'kill ' + convert(varchar(5),spid)
from sys.sysprocesses s
where dbid > 4
	and  db_name(dbid) like 'OCR%'
EXEC(@Kill)   
RESTORE DATABASE [Ocr_SurpassDataWarehouse]
FROM DISK = N'T:\BACKUP\R12.5 KH DBs\OCR R12.5.2017.06.26.bak'
WITH FILE = 3
	,NOUNLOAD
	,NOREWIND
	,REPLACE
	,STATS = 10
	,MOVE 'Data' TO N'S:\DATA\Ocr_SurpassDataWarehouse.mdf'
	,MOVE 'Log' TO N'L:\LOGS\Ocr_SurpassDataWarehouse.ldf';
declare @Kill nvarchar(MAX)=''
select @Kill +=CHAR(13)  + 'kill ' + convert(varchar(5),spid)
from sys.sysprocesses s
where dbid > 4
	and  db_name(dbid) like 'OCR%'
EXEC(@Kill)  
 
RESTORE DATABASE [Ocr_SurpassManagement]
FROM DISK = N'T:\BACKUP\R12.5 KH DBs\OCR R12.5.2017.06.26.bak'
WITH FILE = 4
	,NOUNLOAD
	,NOREWIND
	,REPLACE
	,STATS = 10
	,MOVE 'Data' TO N'S:\DATA\Ocr_SurpassManagement.mdf'
	,MOVE 'Log' TO N'L:\LOGS\Ocr_SurpassManagement.ldf';
	--4 GB of storage needed