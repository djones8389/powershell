SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

if OBJECT_ID('tempdb..##Mismatched') is not null drop table ##Mismatched;

DECLARE @count SMALLINT;
DECLARE @query NVARCHAR(MAX);
DECLARE @subject NVARCHAR(100);

SELECT WEST.ID
	, Keycode
	, WarehouseExamState
	, resultDataFull.value('count(/assessmentDetails/assessment/section/item/p/s/c/i/documents/document/name)', 'nvarchar(MAX)') [XMLDocCount]
	, TableDocCount
INTO ##Mismatched
FROM Warehouse_ExamSessionTable WEST 
INNER JOIN (
	SELECT	count(ID) [TableDocCount]
		, warehouseExamSessionID
	FROM WAREHOUSE_ExamSessionDocumentTable
	group by warehouseExamSessionID
) WESDT
ON WEST.ID = WESDT.warehouseExamSessionID
where ExportToSecureMarker = 1
	and warehouseTime > DATEADD(MINUTE, -30, getDATE())
	AND resultDataFull.value('count(/assessmentDetails/assessment/section/item/p/s/c/i/documents/document/name)', 'nvarchar(MAX)') > [TableDocCount]
	and WarehouseExamState not in (1,99);

if((SELECT COUNT(*) FROM ##Mismatched) > 0)
BEGIN
	
	SELECT @count = (SELECT COUNT(*) FROM ##Mismatched);
	SELECT @subject = cast(@count as varchar(100)) + ' exams have mismatched documents';
	
	SET @query = '

		SELECT *
		FROM ##Mismatched
	';

	EXECUTE	 msdb.dbo.sp_send_dbmail
			 @profile_name = N'WEB1 SMTP Virtual Server',
			 --@recipients = N'liveservices@btl.com; support@btl.com',
			 @recipients = N'dave.jones@btl.com',
			 @subject = @subject,
			 @body = N'See attached for mismatched document exams.',
			 @execute_query_database = N'AAT_SecureAssess',
			 @query = @query,
			 @attach_query_result_as_file = 1,
			 @query_attachment_filename = N'AAT mismatched document exams.csv',
			 @query_result_separator = N',',
			 @query_result_no_padding = 1;
END







	
/*
	
SELECT	
	count(ID) [TableDocCount]
	, warehouseExamSessionID
FROM WAREHOUSE_ExamSessionDocumentTable
where warehouseExamSessionID in (2689531,2695611,2695852)
group by warehouseExamSessionID

SELECT WEST.ID
	, Keycode
	, WarehouseExamState
	, resultDataFull.value('count(/assessmentDetails/assessment/section/item/p/s/c/i/documents/document/name)', 'nvarchar(MAX)') [XMLDocCount]
	,resultDataFull
FROM Warehouse_ExamSessionTable WEST 
where ID in (2689531,2695611,2695852)
order by ID

SELECT	warehouseExamSessionID, itemId, documentName, uploadDate, DocumentId, ID	
FROM WAREHOUSE_ExamSessionDocumentTable
where warehouseExamSessionID in (2689531,2695611,2695852)
order by warehouseExamSessionID


*/