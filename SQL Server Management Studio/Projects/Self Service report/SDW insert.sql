;WITH cte AS (
	SELECT 
		EST.ID ExamSessionKey
		,WSET.OriginatorID OriginatorKey
		,EST.warehouseTime WarehouseTime
		,CAST(CAST(EST.[completionDate] AS date) AS datetime) CompletionDate
		,CAST(CAST(EST.[completionDate] AS time) AS datetime) CompletionTime
		,Convert(varchar(12), EST.KeyCode) KeyCode
		,WSET.[ExamVersionId] ExamVersionKey
		,WSET.[ExamID] ExamKey,WSET.[qualificationId]
		,WCT.CentreID CentreKey
		,MAXUID.UserId CandidateKey
		,EST.resultData.value('data(/exam/@originalGrade)[1]','NVARCHAR(50)') Grade
		,EST.resultData.value('data(/exam/@grade)[1]','NVARCHAR(50)') AdjustedGrade
		,EST.resultData.value('data(/exam/@passValue)[1]','bit') Pass
		,EST.resultData.value('data(/exam/@totalTimeSpent)[1]','int') TimeTaken
		,EST.resultData.value('data(/exam/@userMark)[1]','NVARCHAR(50)') UserMarks
		,EST.resultData.value('data(/exam/@totalMark)[1]','NVARCHAR(50)') TotalMarksAvailable
		,EST.resultData.value('data(/exam[1]/@passType)','int') passType
		,EST.resultData.value('data(/exam[1]/@passMark)','NVARCHAR(50)') PassMark
		,WSET.invigilated Invigilated
		,CONVERT(VARCHAR(1000),CONVERT(VARCHAR(MAX),EST.examIPAuditData.query('data(/ipData[1]/entry/ipAddress)')))  ClientMachine
		,EST.[PreviousExamState] FinalExamState
		,EST.[ExamStateChangeAuditXml].value('data(/exam/stateChange[newStateID=''10'']/information/stateChangeInformation/reason)[1]','int') VoidReasonKey
		,EST.[ExamStateChangeAuditXml].value('data(/exam/stateChange[newStateID=''10'']/information/stateChangeInformation/description)[1]','NVARCHAR(100)') VoidReason
		,CASE WHEN ISNULL(copiedFromExamSessionID, 0) = 0 
			Then Convert(bit , 0) 
			ELSE  Convert(bit , 1) 
		 End IsMysteryShopper
		 ,ExamStatus  = CASE When EST.StructureXML.value('data(assessmentDetails/qualityReview)[1]','nvarchar(10)') = '1' 
			Then N'Quality review' 
			ELSE N'Live' 
		 End 
		,EST.StructureXML.value('data(assessmentDetails/duration)[1]','NVARCHAR(50)') Duration
		FROM [dbo].[WAREHOUSE_ExamSessionTable] EST 
		JOIN [dbo].[WAREHOUSE_UserTable] MAXUID ON EST.WAREHOUSEUserID = MAXUID.ID 
		JOIN [dbo].[WAREHOUSE_ScheduledExamsTable] WSET ON EST.WAREHOUSEScheduledExamID = WSET.ID 		
		LEFT OUTER JOIN [dbo].[WAREHOUSE_CentreTable] WCT ON WSET.WAREHOUSECentreID = WCT.ID 
	WHERE 
		EST.warehouseTime > '1900-01-01 00:00:00.000'    
		AND EST.warehouseTime < '2050-01-01 00:00:00.000' 
		AND EST.warehouseExamState = 1   
)
SELECT 
	ExamSessionKey
	,OriginatorKey
	,WarehouseTime
	,CompletionDate
	,CompletionTime
	,KeyCode
	,ExamVersionKey
	,ExamKey
	,qualificationId
	,CentreKey
	,CandidateKey
	,Grade
	,Pass
	,TimeTaken
	,CASE WHEN ISNUMERIC(UserMarks)=1 THEN CAST(UserMarks AS float) ELSE NULL END UserMarks
	,CASE WHEN ISNUMERIC(TotalMarksAvailable)=1 THEN CAST(TotalMarksAvailable AS float) ELSE NULL END TotalMarksAvailable
	,CASE passType 
		WHEN 0 THEN CASE WHEN ISNUMERIC(PassMark)=1 THEN CAST(PassMark AS float) ELSE NULL END
		ELSE 
			CASE WHEN ISNUMERIC(PassMark)=1 THEN CAST(PassMark AS float) ELSE NULL END/100.
			* CASE WHEN ISNUMERIC(TotalMarksAvailable)=1 THEN CAST(TotalMarksAvailable AS float) ELSE NULL END
	END PassMark
	,Invigilated
	,ClientMachine
	,FinalExamState
	,ISNULL(VoidReasonKey,0) VoidReasonKey
	,CASE WHEN VJLT.[name] IS NULL 
		THEN ISNULL(VoidReason,'')
		ELSE VJLT.[name] 
	 END VoidReason
	,IsMysteryShopper
	,ExamStatus
	,CASE WHEN ISNUMERIC(Duration)=1 THEN CAST(Duration AS float) ELSE NULL END Duration
	,AdjustedGrade
FROM cte
	LEFT OUTER JOIN dbo.VoidJustificationLookupTable VJLT ON VoidReasonKey = VJLT.id