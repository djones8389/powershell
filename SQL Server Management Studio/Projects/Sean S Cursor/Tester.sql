select ItemID, ItemResponseData.value('(/p/@ua)[1]', 'int')
from ExamSessionItemResponseTable
where examSessionid = 166333

select StructureXML
from ExamSessionTable
where ID = 166333


SELECT EST.ID,a.Structure.value('(@userAttempted)[1]', 'int'),ESIRT.ItemResponseData.value('(/p/@ua)[1]', 'int')
FROM ExamSessionTable EST
CROSS APPLY StructureXML.nodes('assessmentDetails/assessment/section/item') A(Structure)
INNER JOIN ExamSessionItemResponseTable ESIRT
	ON EST.ID = ESIRT.ExamSessionID
		AND a.Structure.value('data(@id)[1]', 'nvarchar(10)') = ESIRT.ItemID
where est.ID = 166333
and a.Structure.value('(@userAttempted)[1]', 'int') != ESIRT.ItemResponseData.value('(/p/@ua)[1]', 'int');



SELECT EST.ID
FROM ExamSessionTable EST
CROSS APPLY StructureXML.nodes('assessmentDetails/assessment/section/item') A(Structure)
INNER JOIN ExamSessionItemResponseTable ESIRT
	ON EST.ID = ESIRT.ExamSessionID
		AND a.Structure.value('(@id)[1]', 'nvarchar(10)') = ESIRT.ItemID
WHERE a.Structure.value('(@userAttempted)[1]', 'int') != ESIRT.ItemResponseData.value('(/p/@ua)[1]', 'int');


USE STG_EAL_SecureAssess

SELECT EST.ID, ItemID
FROM ExamSessionTable EST
CROSS APPLY StructureXML.nodes('assessmentDetails/assessment/section') A(Structure)
INNER JOIN ExamSessionItemResponseTable ESIRT
	ON EST.ID = ESIRT.ExamSessionID
		AND a.Structure.value('(item/@id)[1]', 'nvarchar(10)') = ESIRT.ItemID
WHERE a.Structure.value('(item/@userAttempted)[1]', 'int') != ESIRT.ItemResponseData.value('(/p/@ua)[1]', 'int')
	and  EST.ID IN (166333)



SELECT EST.ID
	, ItemID
FROM ExamSessionTable EST
CROSS APPLY StructureXML.nodes('assessmentDetails/assessment/section/item') A(Structure)
INNER JOIN ExamSessionItemResponseTable ESIRT
	ON EST.ID = ESIRT.ExamSessionID
		AND A.Structure.value('(@id)[1]', 'nvarchar(10)') = ESIRT.ItemID
WHERE EST.ID IN (166333)