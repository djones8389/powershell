USE [_AnalyticsManagement]

/*Backup old report*/

	SELECT [Id]
		  ,[name]
		  ,[ReportGroupId]
		  ,[OriginatorId]
		  ,[reportSQL]
	INTO [RescoringReport_Backup]
	FROM [Reports]
	WHERE [name] = 'RescoringReport';

/*Update to BIGINT*/

	UPDATE [Reports]
	SET reportSQL = REPLACE(reportSQL,',AVG(TimeTaken) AS MeanTime',',AVG(CAST(TimeTaken as BIGINT)) AS MeanTime')
	WHERE [name] = 'RescoringReport';

/*View changed SQL*/

	SELECT [Id]
		  ,[name]
		  ,[ReportGroupId]
		  ,[OriginatorId]
		  ,[reportSQL]
	--INTO [RescoringReport_Backup]
	FROM [Reports]
	WHERE [name] = 'RescoringReport';

/*Test using the Reporting UI*/


/*Drop backup*/

DROP TABLE [RescoringReport_Backup];

/*Rollback - if required*/

	UPDATE [Reports]
	SET reportSQL = b.reportSQL
	FROM [Reports] A
	INNER JOIN [RescoringReport_Backup] B
	ON A.Id = B.Id;