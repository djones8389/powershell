sp_whoisactive

use master

select 'kill ' + convert(varchar(5),spid)
	,db_name(dbid)
from sys.sysprocesses s
where dbid > 4
	--and  db_name(dbid) = 'SaxionDefect_ItemBank'
order by 2; 

SELECT 'ALTER DATABASE ' + QUOTENAME(Name) + ' SET OFFLINE;'
from sys.databases
where database_id > 4
	and name like '%EAL%' or name like '%SQA%'
	--and state_desc = 'Offline';

select 'ALTER LOGIN ' + QUOTENAME(NAME) + ' DISABLE;'
from sys.syslogins
WHERE name like '%EAL%' or name like '%SQA%'

order by Name


--ALTER DATABASE [PRV_EAL_CPProjectAdmin] SET OFFLINE;
--ALTER DATABASE [PRV_EAL_ItemBank] SET OFFLINE;
--ALTER DATABASE [PRV_EAL_SecureAssess] SET OFFLINE;
--ALTER LOGIN [PRV_EAL_CPProjectAdmin_Login] DISABLE;
--ALTER LOGIN [PRV_EAL_ItemBank_Login] DISABLE;
--ALTER LOGIN [PRV_EAL_SecureAssess_Login] DISABLE;


--ALTER DATABASE [STG_EAL_ContentProducer] SET OFFLINE;
--ALTER DATABASE [STG_EAL_ItemBank] SET OFFLINE;
--ALTER DATABASE [STG_EAL_SecureAssess] SET OFFLINE;
--ALTER LOGIN [STG_EAL_ContentProducer_Login] DISABLE;
--ALTER LOGIN [STG_EAL_ItemBank_Login] DISABLE;
--ALTER LOGIN [STG_EAL_SecureAssess_Login] DISABLE;



--ALTER DATABASE [PRV_SQA_BTLSurpassAudit] SET OFFLINE;
--ALTER DATABASE [PRV_SQA_CPProjectAdmin] SET OFFLINE;
--ALTER DATABASE [PRV_SQA_ItemBank] SET OFFLINE;
--ALTER DATABASE [PRV_SQA_SecureAssess] SET OFFLINE;
--ALTER DATABASE [PRV_SQA_SurpassDataWarehouse] SET OFFLINE;
--ALTER LOGIN [PRV_SQA_BTLSurpassAudit_Login] DISABLE;
--ALTER LOGIN [PRV_SQA_CPProjectAdmin_Login] DISABLE;
--ALTER LOGIN [PRV_SQA_ItemBank_Login] DISABLE;
--ALTER LOGIN [PRV_SQA_SecureAssess_Login] DISABLE;


--ALTER DATABASE [SANDBOX_SQA_CPProjectAdmin] SET OFFLINE;
--ALTER LOGIN [SANDBOX_SQA_CPProjectAdmin_Login] DISABLE;

--ALTER DATABASE [STG_SQA_CPProjectAdmin] SET OFFLINE;
--ALTER DATABASE [STG_SQA_ItemBank] SET OFFLINE;
--ALTER DATABASE [STG_SQA_OpenAssess] SET OFFLINE;
--ALTER DATABASE [STG_SQA_SecureAssess] SET OFFLINE;
--ALTER LOGIN [STG_SQA_CPProjectAdmin_Login] DISABLE;
--ALTER LOGIN [STG_SQA_ItemBank_Login] DISABLE;
--ALTER LOGIN [STG_SQA_OpenAssess_Login] DISABLE;
--ALTER LOGIN [STG_SQA_SecureAssess_Login] DISABLE;

	

