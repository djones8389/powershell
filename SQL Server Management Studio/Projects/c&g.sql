declare @ID int = 1234;

--set to human marked

	update warehouse_examsessiontable
	set resultdata = replace(cast(resultdata as nvarchar(max)), '@markingType="0"','@markingType="1"')
		, resultdatafull = replace(cast(resultdatafull as nvarchar(max)), '@markingType="0"','@markingType="1"')
		, structurexml = replace(cast(structurexml as nvarchar(max)), '@markingType="0"','@markingType="1"')
	where id = @ID;

	update warehouse_examsessiontable_shreded
	set resultdata = replace(cast(resultdata as nvarchar(max)), '@markingType="0"','@markingType="1"')
		, structurexml = replace(cast(structurexml as nvarchar(max)), '@markingType="0"','@markingType="1"')
	where examSessionID = @ID;

--flag for re-marking in the UI (easier/less risk than doing this in SQL)

--ask SDesk to open script in Re-Marking UI > open it up > next > mark > next > mark for each itemresponse > submit > check in the Audit UI that it's marked
--ask them to look out for these things:  do the itemresponses show?  does the UI auto populate - i.e. 0/1 or 1/1 per item...


--if everything is happy, revert the changes

	update warehouse_examsessiontable
	set resultdata = replace(cast(resultdata as nvarchar(max)), '@markingType="1"','@markingType="0"')
		, resultdatafull = replace(cast(resultdatafull as nvarchar(max)), '@markingType="1"','@markingType="0"')
		, structurexml = replace(cast(structurexml as nvarchar(max)), '@markingType="1"','@markingType="0"')
	where id = @ID;

	update warehouse_examsessiontable_shreded
	set resultdata = replace(cast(resultdata as nvarchar(max)), '@markingType="1"','@markingType="0"')
		, structurexml = replace(cast(structurexml as nvarchar(max)), '@markingType="1"','@markingType="0"')
	where examSessionID = @ID;