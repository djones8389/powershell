/*
Item type key
Introduction = 0,
       MultipleChoice = 2,
        MultipleResponse = 3,
        EitherOr = 4,
        NumericalEntry = 5,
        ShortAnswer = 7,
        Essay = 8,
        InformationPage = 13,
        FinishPage = 20,
        MultipleChoiceSurvey = 23,
        MultipleResponseSurvey = 24,
        EssaySurvey = 25,
*/

IF OBJECT_ID('tempdb..##ResultHolder') IS NOT NULL DROP TABLE ##ResultHolder;

CREATE TABLE ##ResultHolder (
	Client sysname
	, itemType tinyint
	, itemID nvarchar(20)
	, ver tinyint
	, ExamSessionID int
	, Keycode nvarchar(10)
	, examState tinyint
);

SELECT '

use '+quotename(name)+'

set transaction isolation level read uncommitted;

declare @updateDate'+SUBSTRING(Name,0,CHARINDEX('_',Name))+' datetime

declare @itemIds'+SUBSTRING(Name,0,CHARINDEX('_',Name))+' table (ID nvarchar(21), Type int)

SET @updateDate'+SUBSTRING(Name,0,CHARINDEX('_',Name))+' = ''2016/08/12 00:00:00'' 
insert into @itemIds'+SUBSTRING(Name,0,CHARINDEX('_',Name))+'
select convert(nvarchar(10), S.ProjectId) + ''P'' + convert(nvarchar(10), I.id), I.Type
from Items I
inner join Subjects S
on S.Id = I.SubjectId
where I.[Type] in (0,2,3,4,13,20) and I.UpdateDate < @updateDate'+SUBSTRING(Name,0,CHARINDEX('_',Name))+' and ContentUpdateDate < @updateDate'+SUBSTRING(Name,0,CHARINDEX('_',Name))+'

SET @updateDate'+SUBSTRING(Name,0,CHARINDEX('_',Name))+' = ''2017/01/12 00:00:00''
insert into @itemIds'+SUBSTRING(Name,0,CHARINDEX('_',Name))+'
select convert(nvarchar(10), S.ProjectId) + ''P'' + convert(nvarchar(10), I.id), I.Type
from Items I
inner join Subjects S
on S.Id = I.SubjectId
where I.[Type] in (8,23,34,25) and I.UpdateDate < @updateDate'+SUBSTRING(Name,0,CHARINDEX('_',Name))+' and ContentUpdateDate < @updateDate'+SUBSTRING(Name,0,CHARINDEX('_',Name))+'

SET @updateDate'+SUBSTRING(Name,0,CHARINDEX('_',Name))+' = ''2017/07/05 00:00:00'' 
insert into @itemIds'+SUBSTRING(Name,0,CHARINDEX('_',Name))+'
select convert(nvarchar(10), S.ProjectId) + ''P'' + convert(nvarchar(10), I.id), I.Type
from Items I
inner join Subjects S
on S.Id = I.SubjectId
where I.[Type] in (5,7) and I.UpdateDate < @updateDate'+SUBSTRING(Name,0,CHARINDEX('_',Name))+' and ContentUpdateDate < @updateDate'+SUBSTRING(Name,0,CHARINDEX('_',Name))+'

;with usedItems as
(
      select y.value(''@id[1]'', ''nvarchar(20)'') ItemId
		, y.value(''@version[1]'', ''nvarchar(3)'') ver
		, E.ID ExamSessionId
		, Keycode
		, examState
      from '+SUBSTRING(Name,0,CHARINDEX('_',Name)) + '_SecureAssess.dbo.ExamSessionTable E
      cross apply structurexml.nodes(''assessmentDetails/assessment//item'') x(y)
      where examState not in (13) 
) 

INSERT ##ResultHolder
select SUBSTRING(db_name(),0,CHARINDEX(''_'',db_name()))  [Client]
	,Items.Type
	, ItemId
	, ver
	, ExamSessionId
	, Keycode
	, examState
from usedItems
inner join @itemIds'+SUBSTRING(Name,0,CHARINDEX('_',Name))+' Items
on Items.ID = ItemId

'
FROM SYS.Databases
where name like '%ContentAuthor%'
