RESTORE DATABASE [CompexJTLtd_AnalyticsManagement] FROM DISK = N'T:\Backup\CompexJTLtd.ALL.bak' WITH FILE = 1, NOUNLOAD, NOREWIND, REPLACE, STATS = 10, MOVE 'Data' TO N'S:\DATA\CompexJTLtd_AnalyticsManagement.mdf', MOVE 'Log' TO N'L:\LOGS\CompexJTLtd_AnalyticsManagement.ldf';	
RESTORE DATABASE [CompexJTLtd_ContentAuthor] FROM DISK = N'T:\Backup\CompexJTLtd.ALL.bak' WITH FILE = 2, NOUNLOAD, NOREWIND, REPLACE, STATS = 10, MOVE 'Data' TO N'S:\DATA\CompexJTLtd_ContentAuthor.mdf', MOVE 'Log' TO N'L:\LOGS\CompexJTLtd_ContentAuthor.ldf';	
RESTORE DATABASE [CompexJTLtd_ItemBank] FROM DISK = N'T:\Backup\CompexJTLtd.ALL.bak' WITH FILE = 3, NOUNLOAD, NOREWIND, REPLACE, STATS = 10, MOVE 'Data' TO N'S:\DATA\CompexJTLtd_ItemBank.mdf', MOVE 'Log' TO N'L:\LOGS\CompexJTLtd_ItemBank.ldf';	
RESTORE DATABASE [CompexJTLtd_SecureAssess] FROM DISK = N'T:\Backup\CompexJTLtd.ALL.bak' WITH FILE = 4, NOUNLOAD, NOREWIND, REPLACE, STATS = 10, MOVE 'Data' TO N'S:\DATA\CompexJTLtd_SecureAssess.mdf', MOVE 'Log' TO N'L:\LOGS\CompexJTLtd_SecureAssess.ldf';	
RESTORE DATABASE [CompexJTLtd_SurpassDataWarehouse] FROM DISK = N'T:\Backup\CompexJTLtd.ALL.bak' WITH FILE = 5, NOUNLOAD, NOREWIND, REPLACE, STATS = 10, MOVE 'Data' TO N'S:\DATA\CompexJTLtd_SurpassDataWarehouse.mdf', MOVE 'Log' TO N'L:\LOGS\CompexJTLtd_SurpassDataWarehouse.ldf';	
RESTORE DATABASE [CompexJTLtd_SurpassManagement] FROM DISK = N'T:\Backup\CompexJTLtd.ALL.bak' WITH FILE = 6, NOUNLOAD, NOREWIND, REPLACE, STATS = 10, MOVE 'Data' TO N'S:\DATA\CompexJTLtd_SurpassManagement.mdf', MOVE 'Log' TO N'L:\LOGS\CompexJTLtd_SurpassManagement.ldf';	



--declare @dynamic nvarchar(MAX) = ''

--select @dynamic += char(13) + 'kill ' + convert(varchar(5),spid)
--	--,db_name(dbid)
--from sys.sysprocesses s
--where dbid > 4
--	and  db_name(dbid) = 'CompexJTLtd_ContentAuthor'

--exec(@dynamic);
