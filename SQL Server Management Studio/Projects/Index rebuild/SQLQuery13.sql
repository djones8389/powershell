--use master;

--exec sp_MSforeachdb '

--USE [?];

--IF (DB_ID() > 4)

--BEGIN

--select * from sys.dm_db_index_physical_stats (DB_ID(), NULL, NULL, NULL, NULL)
--	where avg_fragmentation_in_percent > 30
--		and index_id > 0

--END
--'




--select 		
--		db_NAME()
--		, OBJECT_NAME(a.object_id)
--		--, I.name
--		, (used_page_count) [usedpages]
--		,  (
--			CASE
--				WHEN (a.index_id < 2) THEN (in_row_data_page_count + lob_used_page_count + row_overflow_used_page_count)
--				ELSE lob_used_page_count + row_overflow_used_page_count
--			END
--		) [pages]	 
--from sys.dm_db_index_physical_stats (DB_ID(), NULL, NULL, NULL, NULL) A

--INNER JOIN sys.dm_db_partition_stats B
--on A.object_id = b.object_id
--	and a.index_id = b.index_id


----INNER JOIN sys.indexes I
----on i.index_id = a.index_id
----	and i.object_id = a.object_id

--	where avg_fragmentation_in_percent > 30
--		and a.index_id > 0


SELECT B.*
FROM (

	select 		
			object_id
			, index_id
			, avg_fragmentation_in_percent

	from sys.dm_db_index_physical_stats (DB_ID(), NULL, NULL, NULL, NULL) A
		where avg_fragmentation_in_percent > 30
			and a.index_id > 0
) B
INNER JOIN (

	SELECT
			object_id
			, index_id
			, used_page_count
			, in_row_data_page_count
			, lob_used_page_count 
			, row_overflow_used_page_count

	FROM sys.dm_db_partition_stats C

) C
on b.object_id = c.object_id
	and b.index_id = c.index_id