USE [PRV_BritishCouncil_SecureAssess]

SET STATISTICS IO ON

;WITH MyCTE
AS (
	SELECT x.*
	FROM (
		SELECT ROW_NUMBER() OVER (
				ORDER BY [WAREHOUSE_ExamSessionTable_Shreded].[warehouseTime]
				) AS 'RowNumber'
			,[WAREHOUSE_ExamSessionTable_Shreded].[examSessionid] AS 'ID'
			,[WAREHOUSE_ScheduledExamsTable].[ExamID] AS 'ExamID'
			,[WAREHOUSE_ExamSessionTable_Shreded].[warehouseTime] AS 'WarehousedTime'
			,[WAREHOUSE_ExamSessionTable_Shreded].[examRef] AS 'ExamRef'
			,[WAREHOUSE_ExamSessionTable_Shreded].[examVersionRef] AS 'ExamVersionRef'
			,[WAREHOUSE_ScheduledExamsTable].[examVersionId] AS 'ExamVersionID'
			,[WAREHOUSE_ExamSessionTable_Shreded].[qualificationRef] AS 'qualificationReference'
			,[WAREHOUSE_ExamSessionTable_Shreded].[validFromDate] AS 'start'
			,[WAREHOUSE_ExamSessionTable_Shreded].[expiryDate] AS 'end'
			,[WAREHOUSE_ExamSessionTable_Shreded].[totalTimeSpent] AS 'ActualDuration'
			,Convert(NVARCHAR(max), [WAREHOUSE_ExamSessionTable_Shreded].[scheduledDurationValue]) AS 'allowedDuration'
			,[WAREHOUSE_ExamSessionTable_Shreded].[defaultDuration] AS 'defaultDuration'
			,[WAREHOUSE_ExamSessionTable_Shreded].[scheduledDurationReason] AS 'reason'
			,[WAREHOUSE_CentreTable].[CentreID] AS 'CentreId'
			,[WAREHOUSE_CentreTable].[CentreName] AS 'CentreName'
			,[WAREHOUSE_UserTable].[Userid] AS 'CandidateID'
			,[WAREHOUSE_UserTable].[Gender] AS 'CandidateGender'
			,[WAREHOUSE_UserTable].[DOB] AS 'CandidateDOB'
			,[WAREHOUSE_UserTable].[EthnicOrigin] AS 'CandidateEthnicOriginID'
			,[WAREHOUSE_UserTable].[CandidateRef] AS 'CandidateRef'
			,[WAREHOUSE_UserTable].[Surname] AS 'CandidateSurname'
			,[WAREHOUSE_UserTable].[Forename] AS 'CandidateForename'
			,[WAREHOUSE_ExamSessionTable_Shreded].[userMark] AS 'CandidateMark'
			,[WAREHOUSE_ExamSessionTable_Shreded].[passMark] AS 'PassMark'
			,[WAREHOUSE_ExamSessionTable_Shreded].[totalMark] AS 'TotalMark'
			,[WAREHOUSE_ExamSessionTable_Shreded].[previousExamState] AS 'PreviousExamState'
			,[VoidJustificationLookupTable].[name] AS 'VoidReason'
			,Convert(NVARCHAR(max), [WAREHOUSE_ExamSessionTable_Shreded].[passValue]) AS 'PassValue'
			,[WAREHOUSE_ExamSessionTable_Shreded].[automaticVerification] AS 'AutoVerify'
			,[WAREHOUSE_ExamSessionTable_Shreded].[resultSampled] AS 'resultSampled'
			,SUBSTRING(CONVERT(NVARCHAR(max), [WAREHOUSE_ExamSessionTable_Shreded].[started], 113), 0, 24) AS 'started'
			,[WAREHOUSE_ExamSessionTable_Shreded].[submitted] AS 'submitted'
			,Convert(XML, Convert(NVARCHAR(max), '<sectionDataRaw>' + Convert(NVARCHAR(max), [WAREHOUSE_ExamSessionTable_Shreded].[resultData].query('/exam/section')) + '</sectionDataRaw>')) AS 'sectionDataRaw'
			,dbo.fn_getMarkers([WAREHOUSE_ExamSessionTable_Shreded].[examSessionId]) AS 'Marker'
			,[WAREHOUSE_ScheduledExamsTable].[ScheduledStartDateTime] AS 'WindowsStartTime'
			,[WAREHOUSE_ExamSessionTable_Shreded].[TakenThroughLocalScan] AS 'TakenThroughLocalScan'
			,[WAREHOUSE_CentreTable].[CentreCode] AS 'CentreRef'
			,CASE 
				WHEN [WAREHOUSE_ExamSessionTable_Shreded].[adjustedGrade] = ''
					THEN [WAREHOUSE_ExamSessionTable_Shreded].[originalGrade]
				ELSE ISNULL([WAREHOUSE_ExamSessionTable_Shreded].[adjustedGrade], [WAREHOUSE_ExamSessionTable_Shreded].[originalGrade])
				END AS 'Grade'
			,[WAREHOUSE_ExamSessionTable_Shreded].[KeyCode] AS 'keyCode'
			,[WAREHOUSE_UserTable].[ULN] AS 'ULN'
			,[WAREHOUSE_ExamSessionTable_Shreded].[reMarkStatus] AS 'ReMarkStatus'
			,[WAREHOUSE_ExamSessionTable_Shreded].[CQN] AS 'CQN'
			,[WAREHOUSE_ExamSessionTable_Shreded].[passType] AS 'PassType'
			,[WAREHOUSE_ExamSessionTable_Shreded].[WarehouseExamState] AS 'WarehouseExamState'
			,[WAREHOUSE_ExamSessionTable_Shreded].[examName] AS 'ExamName'
			,[WAREHOUSE_ExamSessionTable_Shreded].[examVersionName] AS 'ExamVersionName'
			,[WAREHOUSE_ExamSessionTable_Shreded].[itemDataFull] AS 'itemDataFull'
			,[WAREHOUSE_ExamSessionTable_Shreded].[userPercentage] AS 'userPercentage'
		FROM [dbo].[WAREHOUSE_ExamSessionTable_Shreded]
		INNER JOIN [dbo].[WAREHOUSE_ScheduledExamsTable] ON [dbo].[WAREHOUSE_ScheduledExamsTable].[ID] = [WAREHOUSE_ExamSessionTable_Shreded].[WAREHOUSEScheduledExamID]
		INNER JOIN [dbo].[WAREHOUSE_CentreTable] ON [dbo].[WAREHOUSE_CentreTable].[ID] = [WAREHOUSE_ScheduledExamsTable].[WAREHOUSECentreID]
		INNER JOIN [dbo].[WAREHOUSE_UserTable] ON [dbo].[WAREHOUSE_UserTable].[ID] = [WAREHOUSE_ExamSessionTable_Shreded].[WAREHOUSEUserID]
		LEFT JOIN [dbo].[VoidJustificationLookupTable] ON [dbo].[WAREHOUSE_ExamSessionTable_Shreded].[voidJustificationLookupTableId] = [VoidJustificationLookupTable].[ID]
		WHERE (
				[WAREHOUSE_ExamSessionTable_Shreded].[warehouseTime] >= '10 Dec 2015 13:38:17'
				AND [WAREHOUSE_ExamSessionTable_Shreded].[warehouseTime] <= '22 Jan 2016 02:10:08'
				)
			AND ([WAREHOUSE_CentreTable].[CentreName] NOT LIKE '%BTL Test Centre%')
			AND ([WAREHOUSE_ExamSessionTable_Shreded].[examRef] NOT LIKE '%/PRACTICE%')
			AND (NOT WAREHOUSE_ExamSessionTable_Shreded.previousexamstate = 17)
			AND (
				(
					1081 = 1
					OR (
						[WAREHOUSE_CentreTable].[CentreID] IN (
							SELECT [AssignedUserRolesTable].CentreID
							FROM AssignedUserRolesTable
							INNER JOIN RolePermissionsTable ON AssignedUserRolesTable.RoleID = RolePermissionsTable.RoleID
							WHERE (
									(RolePermissionsTable.PermissionID = 29)
									AND [AssignedUserRolesTable].UserID = 1081
									)
								AND (
									[WAREHOUSE_ExamSessionTable_Shreded].[qualificationid] IN (
										SELECT QualificationID
										FROM UserQualificationsTable
										WHERE UserID = 1081
										)
									)
							)
						)
					)
				OR (
					EXISTS (
						SELECT UserID
						FROM AssignedUserRolesTable
						INNER JOIN RolePermissionsTable ON AssignedUserRolesTable.RoleID = RolePermissionsTable.RoleID
						WHERE PermissionID = 29
							AND UserID = 1081
							AND CentreID = 1
						)
					)
				)
			AND [WAREHOUSE_ExamSessionTable_Shreded].WarehouseExamState IN (1)
		) AS X
	)
SELECT *
	,(
		SELECT COUNT(RowNumber)
		FROM MyCTE
		) AS 'TotalResultsAvailable'
	,100 AS 'PageSize'
FROM MyCTE
WHERE RowNumber > (100 * (10 - 1))
	AND RowNumber <= (100 * 10)
ORDER BY [RowNumber] ASC
	,[ExamVersionRef] ASC
	,[CentreId] ASC
	,[CandidateSurname] ASC
	,[ID] ASC

