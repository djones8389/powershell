CREATE USER [zohaibg] FOR LOGIN [zohaibg]
exec sp_addrolemember 'db_owner','zohaibg'
ALTER USER [zohaibg] WITH DEFAULT_SCHEMA  = dbo;


SET DATEFORMAT DMY;

--USE PRV_BCGuilds_SurpassDataWarehouse

--CREATE TABLE [dbo].[_MigrationScripts](
--	[ID] [int] IDENTITY(1,1) NOT NULL,
--	[FileName] [nvarchar](500) NOT NULL,
--	[Checksum] [nchar](32) NOT NULL,
--	[StartDateTime] [datetime] NOT NULL,
--	[EndDateTime] [datetime] NOT NULL,
--	[WindowsUserName] [nvarchar](128) NOT NULL,
--	[MachineName] [nvarchar](128) NOT NULL,
--	[TimeTaken]  AS (datediff(second,[StartDateTime],[EndDateTime]))
--) ON [PRIMARY]

--GO
