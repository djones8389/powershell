/*

SET	TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

--if OBJECT_ID('tempdb..#Exams') IS NOT NULL DROP TABLE #Exams;

--CREATE TABLE #Exams (
--	ID INT
--	,CompletionDate datetime
--)

--/*Get exams older than 10 days*/
 
--INSERT INTO #Exams(ID, CompletionDate)
SELECT ExamSessionID,StateChangeDate--,  CAST(DATEADD(DAY, -10, GETDATE()) AS DATE)
FROM dbo.ExamStateChangeAuditTable WITH(NOLOCK)
WHERE NewState = 9
	AND CAST(StateChangeDate AS DATE) >= CAST(DATEADD(DAY, -50, GETDATE()) AS DATE)
	*/

SELECT --CompletionDate
	 forename
	, surname
	, CandidateRef
	, IB.QualificationName [Subject]
	, examName [Test Form]
	, est.ID
FROM ExamSessionTable EST
INNER JOIN UserTable UT
on UT.ID = EST.UserID
--LEFT JOIN ExamSessionItemResponseTable ESIRT
--on ESIRT.ExamSessionID = EST.ID
INNER JOIN ScheduledExamsTable SCET
on SCET.ID = EST.ScheduledExamID
INNER JOIN CentreTable CT
on CT.ID = SCET.CentreID
INNER JOIN IB3QualificationLookup IB
on IB.id = SCET.qualificationId
--INNER JOIN #Exams A
--on A.ID = EST.ID
where est.id = 131066



USE SANDBOX_Saxion_SecureAssess

--SELECT SUBSTRING (CAST( 

select 	
	','+E.itemName

	+','+Letter
FROM (

SELECT a.b.value('@id','nvarchar(20)') ItemID
		,Item.Selection.value('@ac','char(1)') Letter
		,Item.Selection.value('@sl','bit') Selected
		,ExamSessionID
FROM ExamSessionItemResponseTable
cross apply ItemResponseData.nodes('p') a(b)
cross apply a.b.nodes('s/c/i') Item(Selection)
where examsessionid = 131066
	and Item.Selection.value('@sl','bit') = '1'
) D

INNER JOIN (
	
SELECT ID
	, ROW_NUMBER() OVER (ORDER BY (SELECT 1)) PresentedOrder
	, c.d.value('data(@id)[1]','nvarchar(20)') ItemID
	, c.d.value('(@name)[1]','nvarchar(20)') itemName
FROM ExamSessionTable
cross apply StructureXML.nodes('assessmentDetails/assessment/section/item') c(d)
where ID = 131066

) E
on E.ID = D.ExamsessionID
	and e.ItemID = d.ItemID
ORDER BY PresentedOrder

 FOR XML PATH(''), TYPE



















------UNION

----SELECT ExamSessionID,StateChangeDate
----FROM (
----  SELECT ExamSessionID
----	,  audit.stateChange.value('(changeDate)[1]', 'datetime') StateChangeDate
----  FROM dbo.WAREHOUSE_ExamSessionTable WITH(NOLOCK)
----  CROSS APPLY ExamStateChangeAuditXml.nodes('/exam/stateChange[newStateID="9"]') audit(stateChange)
----) X
----WHERE CAST(StateChangeDate AS DATE) >= CAST(DATEADD(DAY, -50, GETDATE()) AS DATE)

------OPTION (MAXDOP 1)