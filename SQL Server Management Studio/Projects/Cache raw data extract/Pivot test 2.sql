--IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES where TABLE_NAME = '#Items')
if OBJECT_ID('tempdb..#Items')  IS NOT NULL
	DROP TABLE #Items;
if OBJECT_ID('tempdb..#Tester')  IS NOT NULL
	DROP TABLE #Tester;

 CREATE TABLE #Items
(
	AUTOID tinyint IDENTITY(1,1) 
	, ItemID   NVARCHAR(MAX)
	,Letter     VARCHAR(MAX)
	, ESID int
) 

 INSERT INTO #Items (ItemID,Letter, ESID)
SELECT '154p918','A', '131066'
UNION SELECT '624p917','C','131066'
UNION SELECT '924p918','B','131066'
UNION SELECT '144p938','D','131066'
UNION SELECT '1064p918','D','131068'
UNION SELECT '1524p918','B','131068'
UNION SELECT '124p999','C','131068'
UNION SELECT '524p918','A' ,'131068'


 ;WITH CTE_Concatenated AS
    (
    SELECT  
		AUTOID
		, ESID
		, ItemID
		, convert(nvarchar(max),Letter) Letter
    FROM    #Items
    WHERE   AUTOID = 1

    UNION ALL

    SELECT 
		   #Items.AUTOID
		   , #Items.ESID
		   ,CTE_Concatenated.ItemID + ',' + #Items.ItemID	
           ,convert(nvarchar(max),CTE_Concatenated.Letter) + ',' + convert(nvarchar(max),#Items.Letter)  Letter
    FROM    CTE_Concatenated
    JOIN    #Items
    ON      #Items.AUTOID =CTE_Concatenated.AUTOID + 1
    )

    SELECT  ItemID	
		, Letter	
		, ESID
	INTO  #Tester
    FROM    CTE_Concatenated
    WHERE   AUTOID = (SELECT MAX(AUTOID) FROM #Items) 

SELECT * FROM #Tester

SELECT --top (1)--CompletionDate
	 forename
	, surname
	, CandidateRef
	, IB.QualificationName [Subject]
	, examName [Test Form]
	, est.ID
	, f.ItemID
	--, f.Letter
FROM ExamSessionTable EST
INNER JOIN UserTable UT
on UT.ID = EST.UserID
--LEFT JOIN ExamSessionItemResponseTable ESIRT
--on ESIRT.ExamSessionID = EST.ID
INNER JOIN ScheduledExamsTable SCET
on SCET.ID = EST.ScheduledExamID
INNER JOIN CentreTable CT
on CT.ID = SCET.CentreID
INNER JOIN IB3QualificationLookup IB
on IB.id = SCET.qualificationId
INNER JOIN #Tester F
on F.Esid = EST.ID
--INNER JOIN #Exams A
--on A.ID = EST.ID
where est.id in (select esid from #Items)-- = 131066
	--order by LEN(LETTER) DESC

UNION

SELECT 
	NULL
	, NULL
	, NULL
	, NULL
	, NULL
	, NULL
	, (select Letter from #Tester)
	--, NULL	
FROM ExamSessionTable










--SELECT --top (1)--CompletionDate
--	 forename + ','
--	, surname
--	, CandidateRef
--	, IB.QualificationName [Subject]
--	, examName [Test Form]
--	, est.ID
--	--, f.ItemID
--	--, f.Letter
--FROM ExamSessionTable EST
--INNER JOIN UserTable UT
--on UT.ID = EST.UserID
----LEFT JOIN ExamSessionItemResponseTable ESIRT
----on ESIRT.ExamSessionID = EST.ID
--INNER JOIN ScheduledExamsTable SCET
--on SCET.ID = EST.ScheduledExamID
--INNER JOIN CentreTable CT
--on CT.ID = SCET.CentreID
--INNER JOIN IB3QualificationLookup IB
--on IB.id = SCET.qualificationId
----INNER JOIN #Tester F
----on F.Esid = EST.ID

--where est.id = 131068
----est.id > 131064
----	and est.id < 131080


sp_whoisactive







select * from #Items








DECLARE @cols AS NVARCHAR(MAX),
    @query  AS NVARCHAR(MAX)

select @cols = STUFF((SELECT ',' + QUOTENAME(Itemid) 
                    from #Items
                    group by ItemID, Letter
                    order by ItemID
            FOR XML PATH(''), TYPE
            ).value('.', 'NVARCHAR(MAX)') 
        ,1,1,'')

SELECT @cols

set @query = N'SELECT ' + @cols + N' from 
             (
                select ItemID, Letter,autoid
                from #Items
            ) x
            pivot 
            (
                max(Letter)
                for Itemid in (' + @cols + N')
            ) p '


exec sp_executesql @query;

DECLARE @STRING NVARCHAR(MAX) = '[1064p918],[124p999],[144p938],[1524p918],[154p918],[524p918],[624p917],[924p918]'

SELECT 
