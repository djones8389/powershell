--IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES where TABLE_NAME = '#Items')
if OBJECT_ID('tempdb..#Items')  IS NOT NULL
	DROP TABLE #Items;

 CREATE TABLE #Items
(
	AUTOID tinyint IDENTITY(1,1) 
	, ItemID   NVARCHAR(MAX)
	,Letter     VARCHAR(MAX)
	, ESID int
) 

 INSERT INTO #Items (ItemID,Letter, ESID)
SELECT '154p918','A', '131066'
UNION SELECT '624p917','C','131066'
UNION SELECT '924p918','B','131066'
UNION SELECT '144p938','D','131066'
UNION SELECT '1064p918','D','131066'
UNION SELECT '1524p918','B','131066'
UNION SELECT '124p999','C','131066'
UNION SELECT '524p918','A' ,'131066'


 ;WITH CTE_Concatenated AS
    (
    SELECT  
		AUTOID
		, ESID
		, ItemID
		, convert(nvarchar(max),Letter) Letter
    FROM    #Items
    WHERE   AUTOID = 1

    UNION ALL

    SELECT 
		   #Items.AUTOID
		   , #Items.ESID
		   ,CTE_Concatenated.ItemID + ',' + #Items.ItemID	
           ,convert(nvarchar(max),CTE_Concatenated.Letter) + ',' + convert(nvarchar(max),#Items.Letter)  Letter
    FROM    CTE_Concatenated
    JOIN    #Items
    ON      #Items.AUTOID =CTE_Concatenated.AUTOID + 1
    )

  --  SELECT  ItemID	
		--, Letter	
		--, ESID
  --  FROM    CTE_Concatenated
  --  WHERE   AUTOID = (SELECT MAX(AUTOID) FROM #Items) 


SELECT --CompletionDate
	 forename
	, surname
	, CandidateRef
	, IB.QualificationName [Subject]
	, examName [Test Form]
	, est.ID

FROM ExamSessionTable EST
INNER JOIN UserTable UT
on UT.ID = EST.UserID
--LEFT JOIN ExamSessionItemResponseTable ESIRT
--on ESIRT.ExamSessionID = EST.ID
INNER JOIN ScheduledExamsTable SCET
on SCET.ID = EST.ScheduledExamID
INNER JOIN CentreTable CT
on CT.ID = SCET.CentreID
INNER JOIN IB3QualificationLookup IB
on IB.id = SCET.qualificationId
INNER JOIN CTE_Concatenated F
on F.Esid = EST.ID
--INNER JOIN #Exams A
--on A.ID = EST.ID
where est.id = 131066





--	, (    SELECT  ItemID	
--		, Letter	
--    FROM    CTE_Concatenated
--    WHERE   AUTOID = (SELECT MAX(AUTOID) FROM #Items) 
--)















CREATE FUNCTION [dbo].[Split]
(   
    @DelimitedList nvarchar(max)
    , @Delimiter varchar(2) = ','
)
RETURNS TABLE 
AS
RETURN 
    (
    With CorrectedList As
        (
        Select Case When Left(@DelimitedList, DataLength(@Delimiter)) <> @Delimiter Then @Delimiter Else '' End
            + @DelimitedList
            + Case When Right(@DelimitedList, DataLength(@Delimiter)) <> @Delimiter Then @Delimiter Else '' End
            As List
            , DataLength(@Delimiter) As DelimiterLen
        )
        , Numbers As 
        (
        Select TOP (Coalesce(Len(@DelimitedList),1)) Row_Number() Over ( Order By c1.object_id ) As Value
        From sys.objects As c1
            Cross Join sys.columns As c2
        )
    Select CharIndex(@Delimiter, CL.list, N.Value) + CL.DelimiterLen As Position
        , Substring (
                    CL.List
                    , CharIndex(@Delimiter, CL.list, N.Value) + CL.DelimiterLen     
                    , CharIndex(@Delimiter, CL.list, N.Value + 1)                           
                        - ( CharIndex(@Delimiter, CL.list, N.Value) + CL.DelimiterLen ) 
                    ) As Value
    From CorrectedList As CL
        Cross Join Numbers As N
    Where N.Value < Len(CL.List)
        And Substring(CL.List, N.Value, CL.DelimiterLen) = @Delimiter
    )


EXEC [Split] @DelimitedList = '[1064p918],[124p999],[144p938],[1524p918],[154p918],[524p918],[624p917],[924p918]'
	, @Delimiter = ','



Select Name, Values
From Table1 As T1
Where Exists    (
                Select 1
                From Table2 As T2
                    Cross Apply dbo.Split (T1.Values, ',') As T1Values
                    Cross Apply dbo.Split (T2.Values, ',') As T2Values
                Where T2.Values.Value = T1Values.Value
                    And T1.Name = T2.Name
                )