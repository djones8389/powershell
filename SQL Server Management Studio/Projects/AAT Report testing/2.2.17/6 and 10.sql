exec sp_executesql N'SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

DECLARE @IDs TABLE (ID int);
DECLARE @esidpart nvarchar(max);
DECLARE @esidinput nvarchar(max) = @ExamSessionIDs;

DECLARE @examStatesTable TABLE (ID int);
DECLARE @examStatepart nvarchar(max);
DECLARE @examStateinput nvarchar(max) =  @ExamStates;


WHILE LEN(@examStateinput) > 0
BEGIN
	IF PATINDEX(''%,%'', @examStateinput) > 0
	BEGIN
		SELECT @examStatepart = SUBSTRING(@examStateinput, 0, PATINDEX(''%,%'', @examStateinput));
		SET @examStateinput = SUBSTRING(@examStateinput, LEN(@examStatepart + N'','') + 1, LEN(@examStateinput))
	END
	ELSE
	BEGIN
		SET @examStatepart = @examStateinput;
		SET @examStateinput = NULL;
	END
	INSERT INTO @examStatesTable
	SELECT CAST(@examStatepart AS int);
END



WHILE LEN(@esidinput) > 0
BEGIN
	IF PATINDEX(''%,%'', @esidinput) > 0
	BEGIN
		SELECT @esidpart = SUBSTRING(@esidinput, 0, PATINDEX(''%,%'', @esidinput));
		SET @esidinput = SUBSTRING(@esidinput, LEN(@esidpart + N'','') + 1, LEN(@esidinput))
	END
	ELSE
	BEGIN
		SET @esidpart = @esidinput;
		SET @esidinput = NULL;
	END
	INSERT INTO @examStatesTable
	SELECT CAST(@esidpart AS int);
END


BEGIN

declare @singleState varchar(50) = @examStates;
declare @myStartDate datetime = (select @StartDate);
declare @myEndDate datetime = (select @EndDate);	

	declare @myString varchar(max) = ''
		SELECT ID
		FROM WAREHOUSE_ExamSessionTable 
		where  WarehouseExamState != 1
		and cast(EXAMSTATECHANGEAUDITXML.value(''''data(exam/stateChange[newStateID=(@examStates)]/changeDate)[1]'''',''''datetime'''') as DATE)  between ''''@StartDate'''' and ''''@EndDate''''
		UNION
		SELECT ID
		FROM WAREHOUSE_ExamSessionTable 
		where  WarehouseExamState = 1
		and warehouseTime > cast(GETDATE() as date)
		and cast(EXAMSTATECHANGEAUDITXML.value(''''data(exam/stateChange[newStateID=(@examStates)]/changeDate)[1]'''',''''datetime'''') as DATE)  between ''''@StartDate'''' and ''''@EndDate''''
		''

		select @myString  = replace(replace(replace(@myString , ''@examStates'', @singleState), ''@StartDate'',@myStartDate), ''@EndDate'',@myEndDate)
			
		INSERT INTO @IDs
		exec(@myString)
	
END


SELECT 
	ExamSessionTable.ID AS [ExamSessionKey],
	CentreTable.CentreCode AS [CentreCode],
	CentreTable.CentreName AS [CentreName],
	CentreTable.ID AS [CentreKey],
	IB3QualificationLookup.QualificationRef AS [QualificationRef],
	IB3QualificationLookup.QualificationName AS [QualificationName],
	ScheduledExamsTable.ExamRef AS [ExamReference],
	ScheduledExamsTable.ExamName AS [ExamName],
	ScheduledExamsTable.purchaseOrder as [purchaseOrder],
	ScheduledExamsTable.examID AS [ExamKey],
	ExamSessionTable.KeyCode AS [Keycode],
	StartedTime.StateChangeDate AS [StartedDate],
	CompletedTime.StateChangeDate AS [CompletedDate],
	UserTable.CandidateRef AS [CandidateRef],
	UserTable.Surname AS [Surname],
	UserTable.Forename AS [Forename],
	NULL AS [Exported],
	VoidedTime.StateInformation.value(''(stateChangeInformation[1]/reason[1])[1]'', ''int'') AS [VoidReasonID]
INTO #LiveExamData	
FROM dbo.ScheduledExamsTable
INNER JOIN dbo.IB3QualificationLookup
ON ScheduledExamsTable.QualificationID = IB3QualificationLookup.ID
INNER JOIN dbo.CentreTable
ON ScheduledExamsTable.CentreID = CentreTable.ID
INNER JOIN dbo.ExamSessionTable
ON ScheduledExamsTable.ID = ExamSessionTable.ScheduledExamID
INNER JOIN dbo.UserTable
ON ExamSessionTable.UserID = UserTable.ID
INNER JOIN (
	SELECT ExamStateChangeAuditTable.ExamSessionID
	FROM (
		SELECT Exams.ExamSessionID
		FROM (
			SELECT COUNT(ExamStateChangeAuditTable.ID) AS [No],
				ExamStateChangeAuditTable.ExamSessionID,
				ExamStateChangeAuditTable.NewState,
				MIN(ExamStateChangeAuditTable.StateChangeDate) AS [StateChangeDate]
			FROM dbo.ExamStateChangeAuditTable
			WHERE ExamStateChangeAuditTable.NewState IN ((SELECT ID FROM @examStatesTable))
			GROUP BY ExamStateChangeAuditTable.ExamSessionID,
				ExamStateChangeAuditTable.NewState
		) AS [Exams]
		WHERE CAST(StateChangeDate AS date) BETWEEN CAST(@StartDate AS date) AND CAST(@EndDate AS date)
	) AS [ExamStateChangeAuditTable]
) AS [ExamStateChangeAuditTable]
ON ExamStateChangeAuditTable.ExamSessionID = ExamSessionTable.ID
LEFT JOIN (
	SELECT COUNT(ExamStateChangeAuditTable.ID) AS [No],
		ExamStateChangeAuditTable.ExamSessionID,
		ExamStateChangeAuditTable.NewState,
		MIN(ExamStateChangeAuditTable.StateChangeDate) AS [StateChangeDate]
	FROM dbo.ExamStateChangeAuditTable
	WHERE ExamStateChangeAuditTable.NewState = 6
	GROUP BY ExamStateChangeAuditTable.ExamSessionID,
		ExamStateChangeAuditTable.NewState) AS StartedTime
ON ExamSessionTable.ID = StartedTime.ExamSessionID
LEFT JOIN (
	SELECT COUNT(ExamStateChangeAuditTable.ID) AS [No],
		ExamStateChangeAuditTable.ExamSessionID,
		ExamStateChangeAuditTable.NewState,
		MIN(ExamStateChangeAuditTable.StateChangeDate) AS [StateChangeDate]
	FROM dbo.ExamStateChangeAuditTable
	WHERE ExamStateChangeAuditTable.NewState = 9
	GROUP BY ExamStateChangeAuditTable.ExamSessionID,
		ExamStateChangeAuditTable.NewState) AS [CompletedTime]
ON ExamSessionTable.ID = CompletedTime.ExamSessionID
LEFT JOIN dbo.ExamStateChangeAuditTable AS [VoidedTime]
ON ExamSessionTable.ID = VoidedTime.ExamSessionID
AND VoidedTime.NewState = 10;

CREATE CLUSTERED INDEX [PK_LiveExamData] ON #LiveExamData (ExamsessionKey);

SELECT  
	WAREHOUSE_ExamSessionTable.ExamSessionID  AS [ExamSessionKey],
	WAREHOUSE_ExamSessionTable_Shreded.CentreCode AS [CentreCode],
	WAREHOUSE_ExamSessionTable_Shreded.CentreName AS [CentreName],
	WAREHOUSE_ExamSessionTable_Shreded.CentreID AS [CentreKey],
	WAREHOUSE_ExamSessionTable_Shreded.QualificationRef AS [QualificationRef],
	WAREHOUSE_ExamSessionTable_Shreded.QualificationName AS [QualificationName],
	WAREHOUSE_ExamSessionTable_Shreded.examRef AS [ExamReference],
	WAREHOUSE_ExamSessionTable_Shreded.ExamName AS [ExamName],
	WAREHOUSE_ScheduledExamsTable.purchaseOrder as [purchaseOrder],
	WAREHOUSE_ExamSessionTable_Shreded.examID AS [ExamKey],
	WAREHOUSE_ExamSessionTable.KeyCode AS [Keycode],
	WAREHOUSE_ExamSessionTable_Shreded.[started] AS [StartedDate],
	WAREHOUSE_ExamSessionTable.completionDate AS [completionDate],
	WAREHOUSE_ExamSessionTable_Shreded.CandidateRef AS [CandidateRef],
	WAREHOUSE_ExamSessionTable_Shreded.Surname AS [Surname],
	WAREHOUSE_ExamSessionTable_Shreded.Forename AS [Forename],
	WAREHOUSE_ExamSessionTable.submissionExported AS [Exported], 
	WAREHOUSE_ExamSessionTable_Shreded.examStateInformation.value(''(stateChangeInformation/reason)[1]'', ''int'') AS [VoidReasonID]
FROM WAREHOUSE_ExamSessionTable_Shreded
INNER JOIN WAREHOUSE_ExamSessionTable
	ON WAREHOUSE_ExamSessionTable.ID = WAREHOUSE_ExamSessionTable_Shreded.examSessionID
INNER JOIN WAREHOUSE_ScheduledExamsTable
	on WAREHOUSE_ScheduledExamsTable.ID = WAREHOUSE_ExamSessionTable.WAREHOUSEScheduledExamID
WHERE WAREHOUSE_ExamSessionTable.ID IN (SELECT ID FROM @IDs)	
	AND WAREHOUSE_ExamSessionTable.ExamSessionID NOT IN (SELECT ExamSessionKey FROM #LiveExamData WITH (READUNCOMMITTED))

UNION

SELECT *
FROM #LiveExamData;',N'@ExamStates nvarchar(4),@ExamSessionIDs nvarchar(3831),@StartDate datetime,@EndDate 
datetime',@ExamStates=N'6,10',@ExamSessionIDs=N'2095381,2095464,2094863,2095281,2095364,2095049,2095132,2095046,2094949,2095032,2094963,2094832,2095367,2095164,2095081,2094763,2095267,2095264,2094849,2095450,2094932,2095181,2094795,2095499,2095213,2094981,2094881,2095399,2095250,2095150,2094764,2095096,2095013,2095382,2095282,2094864,2095431,2095417,2094896,2094813,2095099,2095182,2095082,2095414,2094982,2095231,2095314,2095131,2095214,2094882,2095400,2094799,2094796,2095300,2095449,2095432,2095031,2095114,2095163,2095149,2095146,2094914,2094831,2095249,2095332,2094814,2095232,2094943,2095109,2094843,2094777,2095152,2095218,2094975,2095284,2095018,2094786,2094809,2094852,2095479,2094818,2094875,2095379,2094918,2095084,2094775,2095388,2095179,2094950,2094884,2095279,2095445,2094784,2095488,2094841,2095245,2095411,2095311,2095477,2095354,2095145,2094850,2095111,2095254,2095154,2095320,2094816,2095377,2095420,2095277,2095045,2095211,2095386,2094845,2095011,2095177,2094911,2095452,2095220,2095077,2095243,2095120,2095286,2095418,2094877,2095186,2095043,2095209,2095352,2094920,2095252,2095309,2094977,2094811,2095215,2095298,2094780,2094966,2095384,2095115,2095029,2094797,2095433,2094915,2095347,2095430,2094829,2095247,2095098,2095230,2094898,2095416,2094815,2095147,2095047,2095333,2095130,2094812,2095448,2095133,2094847,2095216,2095116,2095365,2095465,2094916,2095434,2095397,2094830,2095497,2094979,2095265,2095348,2095165,2095248,2094779,2095148,2095048,2094965,2095380,2095383,2095366,2095283,2094765,2094848,2095183,2094762,2095180,2095097,2094865,2094948,2095080,2094980,2094897,2095415,2095398,2095315,2095166,2095083,2095095,2095404,2095261,2095427,2094866,2095361,2095161,2095470,2095204,2095393,2094766,2094895,2095436,2095170,2095359,2095104,2095402,2094861,2095127,2095270,2095293,2095093,2095259,2095236,2095136,2095468,2095368,2094827,2095125,2094870,2095268,2094893,2094793,2095168,2094959,2095102,2094936,2095059,2095463,2095134,2094836,2095234,2095025,2094859,2095002,2095363,2094902,2094802,2094825,2094968,2095329,2094868,2094934,2095495,2095429,2094900,2095229,2095395,2094791,2095129,2094768,2095438,2094834,2095275,2095341,2095424,2095407,2095258,2095175,2094820,2095241,2095373,2095456,2095141,2095224,2095290,2095207,2095390,2095473,2095158,2095356,2095439,2095124,2095422,2095339,2094941,2095107,2095273,2095190,2095090,2095256,2094990,2095156,2095239,2095139,2094824,2095222,2095405,2094873,2094956,2095122,2094807,2095205,2095022,2094856,2094939,2095188,2095105,2094790,2094773,2094839,2094788,2094871,2095409,2094888,2094971,2095358,2095441,2094937,2094771,2095392,2094837,2095375,2095178,2095321,2094803,2094860,2095192,2095235,2095135,2095421,2095035,2094892,2095078,2094935,2095410,2095453,2094978,2095110,2095396,2094878,2094792,2095167,2094835,2095153,2094778,2095242,2094867,2095385,2095010,2095428,2095285,2095185,2094810,2094853,2095142,2094767,2095403,2095446,2095460,2094942,2095085,2095217,2094842,2095128,2094885,2094785,2095117,2094828,2095017,2095160,2094817,2094960,2095103,2095378,2094917,2095435,2095344,2094883,2095401,2094783,2094969,2095112,2095444,2095101,2095244,2095287,2095144,2094769,2094826,2095419,2095462,2094858,2095187,2095044,2095376,2095176,2094801,2095362,2094844,2095262,2095451,2094976,2095251,2095394,2094876,2095162,2095437,2094933,2095237,2095151,2095294,2094776,2094819,2095426,2095412,2094808,2095369,2095137,2094794,2095126,2095269,2095312,2095169,2095455,2094851,2095212,2095226,2094840,2095238,2094823,2095072,2095255,2095138,2094806,2095121,2094972,2094789,2094955,2094855,2094938,2094838,2095004,2094921,2095425,2094904,2095408,2094804,2094787,2095374,2095457,2094770,2095225,2095391,2095108,2095274,2095357,2095440,2095257,2095174,2095340,2095423,2095140,2095223,2095389,2095306,2095472,2094974,2095057,2094891,2095157,2095123,2095206,2095372,2095189,2095106,2095355,2094874,2094857,2094940'
--,@StartDate='2016-04-05 00:00:00',@EndDate='2017-01-31 00:00:00'
, @StartDate ='05 Apr 2016 00:00:00'
, @EndDate  ='31 Jan 2017 00:00:00'