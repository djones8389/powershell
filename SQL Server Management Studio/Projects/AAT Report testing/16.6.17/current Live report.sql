USE PPD_AAT_SecureAssess

exec sp_executesql N'SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

if object_ID(''tempdb..##ESIDs'') is not null drop table ##ESIDs;
if object_ID(''tempdb..#LiveExamData'') is not null drop table #LiveExamData;

DECLARE @examStatesTable TABLE (ID int);
DECLARE @examStatepart nvarchar(max);
DECLARE @examStateinput nvarchar(max) =  @ExamStates;

WHILE LEN(@examStateinput) > 0
BEGIN
		IF PATINDEX(''%,%'', @examStateinput) > 0
		BEGIN
			SELECT @examStatepart = SUBSTRING(@examStateinput, 0, PATINDEX(''%,%'', @examStateinput));
			SET @examStateinput = SUBSTRING(@examStateinput, LEN(@examStatepart + N'','') + 1, LEN(@examStateinput))
		END
		ELSE
		BEGIN
			SET @examStatepart = @examStateinput;
			SET @examStateinput = NULL;
		END
		INSERT INTO @examStatesTable
		SELECT CAST(@examStatepart AS int);
END


BEGIN

	declare @singleState varchar(50) = @examStates;
	declare @myStartDate datetime = (select @StartDate);
	declare @myEndDate datetime = (select @EndDate);	

		declare @myString varchar(max) = ''

			CREATE TABLE ##ESIDs (ID int);
			INSERT INTO ##ESIDs
			SELECT ID
			FROM WAREHOUSE_ExamSessionTable 
			where  WarehouseExamState != 1
			and cast(EXAMSTATECHANGEAUDITXML.value(''''data(exam/stateChange[newStateID=(@examStates)]/changeDate)[1]'''',''''datetime'''') as DATE)  between ''''@StartDate'''' and ''''@EndDate''''
			''

			select @myString  = replace(replace(replace(@myString , ''@examStates'', @singleState), ''@StartDate'',@myStartDate), ''@EndDate'',@myEndDate)
			exec(@myString)
	
END

SELECT 
	ExamSessionTable.ID AS [ExamSessionKey],
	CentreTable.CentreCode AS [CentreCode],
	CentreTable.CentreName AS [CentreName],
	CentreTable.ID AS [CentreKey],
	IB3QualificationLookup.QualificationRef AS [QualificationRef],
	IB3QualificationLookup.QualificationName AS [QualificationName],
	ScheduledExamsTable.ExamRef AS [ExamReference],
	ScheduledExamsTable.ExamName AS [ExamName],
	ScheduledExamsTable.purchaseOrder as [purchaseOrder],
	ScheduledExamsTable.examID AS [ExamKey],
	ExamSessionTable.KeyCode AS [Keycode],
	StartedTime.StateChangeDate AS [StartedDate],
	CompletedTime.StateChangeDate AS [CompletedDate],
	UserTable.CandidateRef AS [CandidateRef],
	UserTable.Surname AS [Surname],
	UserTable.Forename AS [Forename],
	NULL AS [Exported],
	VoidedTime.StateInformation.value(''(stateChangeInformation[1]/reason[1])[1]'', ''int'') AS [VoidReasonID],
	case when stateinformation.value(''data(stateChangeInformation/description/text())[1]'',''nvarchar(max)'') is null then ''''
		else stateinformation.value(''data(stateChangeInformation/description/text())[1]'',''nvarchar(max)'')
		end as [VoidReasonOther]
INTO #LiveExamData	
FROM dbo.ScheduledExamsTable
INNER JOIN dbo.IB3QualificationLookup
ON ScheduledExamsTable.QualificationID = IB3QualificationLookup.ID
INNER JOIN dbo.CentreTable
ON ScheduledExamsTable.CentreID = CentreTable.ID
INNER JOIN dbo.ExamSessionTable
ON ScheduledExamsTable.ID = ExamSessionTable.ScheduledExamID
INNER JOIN dbo.UserTable
ON ExamSessionTable.UserID = UserTable.ID
INNER JOIN (
	SELECT ExamStateChangeAuditTable.ExamSessionID
	FROM (
		SELECT Exams.ExamSessionID
		FROM (
			SELECT COUNT(ExamStateChangeAuditTable.ID) AS [No],
				ExamStateChangeAuditTable.ExamSessionID,
				ExamStateChangeAuditTable.NewState,
				MIN(ExamStateChangeAuditTable.StateChangeDate) AS [StateChangeDate]
			FROM dbo.ExamStateChangeAuditTable
			WHERE ExamStateChangeAuditTable.NewState IN ((SELECT ID FROM @examStatesTable))
			GROUP BY ExamStateChangeAuditTable.ExamSessionID,
				ExamStateChangeAuditTable.NewState
		) AS [Exams]
		WHERE CAST(StateChangeDate AS date) BETWEEN CAST(@StartDate AS date) AND CAST(@EndDate AS date)
	) AS [ExamStateChangeAuditTable]
) AS [ExamStateChangeAuditTable]
ON ExamStateChangeAuditTable.ExamSessionID = ExamSessionTable.ID
LEFT JOIN (
	SELECT COUNT(ExamStateChangeAuditTable.ID) AS [No],
		ExamStateChangeAuditTable.ExamSessionID,
		ExamStateChangeAuditTable.NewState,
		MIN(ExamStateChangeAuditTable.StateChangeDate) AS [StateChangeDate]
	FROM dbo.ExamStateChangeAuditTable
	WHERE ExamStateChangeAuditTable.NewState = 6
	GROUP BY ExamStateChangeAuditTable.ExamSessionID,
		ExamStateChangeAuditTable.NewState) AS StartedTime
ON ExamSessionTable.ID = StartedTime.ExamSessionID
LEFT JOIN (
	SELECT COUNT(ExamStateChangeAuditTable.ID) AS [No],
		ExamStateChangeAuditTable.ExamSessionID,
		ExamStateChangeAuditTable.NewState,
		MIN(ExamStateChangeAuditTable.StateChangeDate) AS [StateChangeDate]
	FROM dbo.ExamStateChangeAuditTable
	WHERE ExamStateChangeAuditTable.NewState = 9
	GROUP BY ExamStateChangeAuditTable.ExamSessionID,
		ExamStateChangeAuditTable.NewState) AS [CompletedTime]
ON ExamSessionTable.ID = CompletedTime.ExamSessionID
LEFT JOIN dbo.ExamStateChangeAuditTable AS [VoidedTime]
ON ExamSessionTable.ID = VoidedTime.ExamSessionID
AND VoidedTime.NewState = 10;

create clustered index [IX_LiveData] on #LiveExamData (ExamsessionKey);

SELECT  
	WAREHOUSE_ExamSessionTable.ExamSessionID  AS [ExamSessionKey],
	WAREHOUSE_ExamSessionTable_Shreded.CentreCode AS [CentreCode],
	WAREHOUSE_ExamSessionTable_Shreded.CentreName AS [CentreName],
	WAREHOUSE_ExamSessionTable_Shreded.CentreID AS [CentreKey],
	WAREHOUSE_ExamSessionTable_Shreded.QualificationRef AS [QualificationRef],
	WAREHOUSE_ExamSessionTable_Shreded.QualificationName AS [QualificationName],
	WAREHOUSE_ExamSessionTable_Shreded.examRef AS [ExamReference],
	WAREHOUSE_ExamSessionTable_Shreded.ExamName AS [ExamName],
	WAREHOUSE_ScheduledExamsTable.purchaseOrder AS [PurchaseOrder],
	WAREHOUSE_ExamSessionTable_Shreded.examID AS [ExamKey],
	WAREHOUSE_ExamSessionTable.KeyCode AS [Keycode],
	WAREHOUSE_ExamSessionTable_Shreded.[started] AS [StartedDate],
	WAREHOUSE_ExamSessionTable.completionDate AS [completionDate],
	WAREHOUSE_ExamSessionTable_Shreded.CandidateRef AS [CandidateRef],
	WAREHOUSE_ExamSessionTable_Shreded.Surname AS [Surname],
	WAREHOUSE_ExamSessionTable_Shreded.Forename AS [Forename],
	WAREHOUSE_ExamSessionTable.submissionExported AS [Exported], 
	WAREHOUSE_ExamSessionTable_Shreded.examStateInformation.value(''(stateChangeInformation/reason)[1]'', ''int'') AS [VoidReasonID],
	case when examstateinformation.value(''data(stateChangeInformation/description/text())[1]'',''nvarchar(max)'') is null then ''''
		else examstateinformation.value(''data(stateChangeInformation/description/text())[1]'',''nvarchar(max)'')
		end as [VoidReasonOther]
FROM WAREHOUSE_ExamSessionTable_Shreded

INNER JOIN WAREHOUSE_ExamSessionTable
on WAREHOUSE_ExamSessionTable.ID = WAREHOUSE_ExamSessionTable_Shreded.examSessionID 

INNER JOIN WAREHOUSE_ScheduledExamsTable
on WAREHOUSE_ScheduledExamsTable.ID = WAREHOUSE_ExamSessionTable.WAREHOUSEScheduledExamID

where WAREHOUSE_ExamSessionTable.ID IN (

select Exams.ExamSessionKey
	FROM (
		SELECT ExamSessionKey
			,min(StateChangeDate) StateChangeDate
		FROM [PPD_AAT_SurpassDataWarehouse].[dbo].[FactExamSessionsStateAudit]
			where ExamStateID IN ((SELECT ID FROM @examStatesTable))
			group by ExamSessionKey
	) Exams
	where CAST(StateChangeDate AS DATE) BETWEEN CAST(@StartDate AS DATE) AND CAST(@EndDate AS DATE)
	UNION
	select id
	from ##ESIDS
)
	AND WAREHOUSE_ExamSessionTable.ExamSessionID not in (select ExamSessionKey from #LiveExamData)

UNION

select *
from #LiveExamData;


if object_ID(''tempdb..##ESIDs'') is not null drop table ##ESIDs;
if object_ID(''tempdb..#LiveExamData'') is not null drop table #LiveExamData;',N'@ExamStates nvarchar(3),@StartDate datetime,@EndDate datetime',@ExamStates=N'6,9',@StartDate='2017-01-01 00:00:00',@EndDate='2017-01-01 00:00:00'