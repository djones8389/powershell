exec sp_executesql N'DECLARE @tblStates TABLE (StateID TINYINT);

INSERT INTO @tblStates
 SELECT ExamStateKey FROM dbo.DimExamState WHERE ExamStateKey IN (6,9);

IF ((SELECT COUNT(StateID) FROM @tblStates WHERE StateID IN (6, 10)) = (SELECT COUNT(StateID) FROM @tblStates))
 DELETE FROM @tblStates WHERE StateID = 10;

SELECT FactExamSessions.ExamSessionKey,
	DimCentres.CentreKey,
	DimCentres.CentreCode,
	DimCentres.CentreName,
	DimQualifications.QualificationRef,
	DimQualifications.QualificationName,
	DimExams.ExamKey,
	DimExams.ExamReference,
	DimExams.ExamName,
	FactExamSessions.KeyCode,
	FactExamSessionsStateAudit.StartedDate,
	CASE WHEN FactExamSessionsStateAudit.NineCount > 1 THEN 1 ELSE 0 END AS [Local],
	CompletedTime.FullDateAlternateKey + FactExamSessions.CompletionTime AS [CompletedDate],
	DimCandidate.CandidateRef,
	DimCandidate.Surname,
	DimCandidate.Forename,
	CASE FactExamSessions.FinalExamState WHEN 10 THEN 1 ELSE 0 END AS [Voided],
	FactExamSessions.VoidReason
FROM dbo.FactExamSessions
INNER JOIN dbo.DimCentres
ON FactExamSessions.CentreKey = DimCentres.CentreKey
INNER JOIN dbo.DimQualifications
ON FactExamSessions.QualificationKey = DimQualifications.QualificationKey
INNER JOIN dbo.DimExams
ON DimQualifications.QualificationKey = DimExams.QualificationKey
AND FactExamSessions.ExamKey = DimExams.ExamKey
INNER JOIN dbo.DimCandidate
ON FactExamSessions.CandidateKey = DimCandidate.CandidateKey
LEFT JOIN (
	SELECT FactExamSessionsStateAudit.ExamSessionKey, MIN(FactExamSessionsStateAudit.StateChangeDate) AS [StartedDate], COUNT(FactExamSessionsStateAudit.ExamSessionKey) AS [NineCount]
	FROM dbo.FactExamSessionsStateAudit
	WHERE FactExamSessionsStateAudit.ExamStateID = 9
	GROUP BY FactExamSessionsStateAudit.ExamSessionKey
) AS [FactExamSessionsStateAudit]
ON FactExamSessions.ExamSessionKey = FactExamSessionsStateAudit.ExamSessionKey
INNER JOIN dbo.DimTime AS [CompletedTime]
ON FactExamSessions.CompletionDateKey = CompletedTime.TimeKey
WHERE FactExamSessions.ExamSessionKey IN (
	SELECT FactExamSessionsStateAudit.ExamSessionKey
	FROM dbo.FactExamSessionsStateAudit
	WHERE FactExamSessionsStateAudit.ExamStateID 
	IN (select stateID from @tblStates)
	AND CAST(FactExamSessionsStateAudit.StateChangeDate AS DATE) BETWEEN CAST(@StartDate AS DATE) AND CAST(@EndDate AS DATE)
)',N'@StartDate datetime,@EndDate datetime',@StartDate='01 apr 2016  00:00:00',@EndDate='30 apr 2016 00:00:00'