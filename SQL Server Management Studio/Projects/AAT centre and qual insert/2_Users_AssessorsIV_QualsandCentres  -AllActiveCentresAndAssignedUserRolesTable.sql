USE AAT_SecureAssess

--C:\Users\977496-davej2\Desktop\AATBulkAssign\1_Centres_AssignQuals - CentreQualTable.csv
--C:\Users\977496-davej2\Desktop\AATBulkAssign\2_Users_AssessorsIV_QualsandCentres  -AllActiveCentresAndAssignedUserRolesTable.csv
--C:\Users\977496-davej2\Desktop\AATBulkAssign\3_Users_CentreAdmin_QualsandCentres.csv


if OBJECT_ID('tempdb..#table2') is not null drop table #table2;

create table #table2 (
	username nvarchar(200)
	,forename nvarchar(200)
	,surname nvarchar(200)
	,qual1 nvarchar(200)
	,qual2 nvarchar(200)
	,qualRole nvarchar(200)
);
go

bulk insert #table2
from 'C:\Users\977496-davej2\Desktop\AATBulkAssign\2_Users_AssessorsIV_QualsandCentres  -AllActiveCentresAndAssignedUserRolesTable.csv'
with (fieldterminator = ',', rowterminator='\n')
go



DECLARE @UserCentreInsert TABLE (
	userID int
	,centreID int
);

INSERT @UserCentreInsert
SELECT UT.ID

FROM #table2 A
INNER JOIN UserTable UT
on UT.Username = A.username

select Forename, Surname, username, *
from UserTable
where username like 'EP%'