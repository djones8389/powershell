USE AAT_SecureAssess;

IF OBJECT_ID('tempdb..#changesmade') IS NOT NULL DROP TABLE #changesmade
IF OBJECT_ID('tempdb..#1_Centres_AssignQuals') is not null  DROP TABLE #1_Centres_AssignQuals;
IF OBJECT_ID('tempdb..#2_Combine') is not null  DROP TABLE #2_Combine;

create table #changesmade (
	statement nvarchar(MAX)
	, userID int null
	, roleid int null
	, CentreID int null
	, qualID int null
);

create table #1_Centres_AssignQuals (
	centrename nvarchar(200)
	,centrecode nvarchar(200)
	,qual1 nvarchar(200)
	,qual2 nvarchar(200)
	,qual3 nvarchar(200)
	,qual4 nvarchar(200)
);

create table #2_Combine (
	username nvarchar(200)
	,forename nvarchar(200)
	,surname nvarchar(200)
	,qual1 nvarchar(200)
	,qual2 nvarchar(200)
	,qual3 nvarchar(200)
	,qual4 nvarchar(200)
	,centrename nvarchar(200)
	,centrecode nvarchar(200)
	,[Role] nvarchar(200)
);



bulk insert #1_Centres_AssignQuals
from 'C:\Users\977496-davej2\Desktop\AATBulkAssign\1_Centres_AssignQuals.csv'
with (fieldterminator = ',', rowterminator='\n')

bulk insert #2_Combine
from 'C:\Users\977496-davej2\Desktop\AATBulkAssign\2_Combine.csv'
with (fieldterminator = ',', rowterminator='\n')

--Assign only the Centres approved as per the spreadsheet for the quals as specified in the spreadsheet (tab 2)--


	SELECT *
	INTO CentreQualificationsTable_F10882_Backup
	FROM CentreQualificationsTable;

	MERGE INTO CentreQualificationsTable as TGT
	USING (
		SELECT CT.ID CentreID	
			, B.ID QualificationID
		FROM #1_Centres_AssignQuals A
		INNER JOIN CentreTable CT
		on CT.CentreName = A.centrename
			and CT.CentreCode = A.centrecode
		INNER JOIN IB3QualificationLookup B
		on B.QualificationName in (a.qual1, a.qual2, a.qual3, a.qual4)
	) as SRC
	on TGT.CentreID = SRC.CentreID
		and TGT.QualificationID = SRC.QualificationID
	WHEN NOT MATCHED BY TARGET THEN
		insert(CentreID, QualificationID)
		values(SRC.CentreID, SRC.QualificationID)

			OUTPUT $action, inserted.CentreID, inserted.QualificationID 
			INTO #changesmade([statement], CentreID, QualID);

--Assign Assessors and Internal Verifiers to all active centres on SecureAssess (tab 1)--
	
	SELECT *
	INTO AssignedUserRolesTable_F10882_Backup
	FROM AssignedUserRolesTable;

	MERGE INTO AssignedUserRolesTable as TGT
	USING (
	SELECT B.ID [UserID]
		, RT.ID [RoleID]
		, C.CentreID
	FROM #2_Combine A
	INNER JOIN UserTable B
	ON A.username = b.Username
	INNER JOIN RolesTable RT
	on RT.Name = A.Role
	CROSS APPLY (
		SELECT ID CentreID
		FROM CENTRETable
		where retired = 0
		) C
	where A.Role in ('Internal verifier','Assessor')
	) as SRC

	ON TGT.UserID = SRC.UserID
		AND TGT.RoleID = SRC.RoleID
		AND TGT.CentreID = SRC.CentreID
	WHEN NOT MATCHED THEN
	INSERT(UserID, RoleID, CentreID)
	VALUES(SRC.UserID, SRC.RoleID, SRC.CentreID)
	
		OUTPUT $action, inserted.UserID, inserted.RoleID, inserted.CentreID 
		INTO #changesmade([statement], UserID, RoleID, CentreID);

--Assign Assessors and Internal Verifiers only to the quals as specified on the spreadsheet against each user (tab 1)--
	


	0 ROWS!!!!

	SELECT *
	INTO UserQualificationsTable_F10882_Backup
	FROM UserQualificationsTable;


	MERGE INTO UserQualificationsTable as TGT
	USING (
		SELECT B.ID [UserID]
			, C.ID [QualificationID]
		FROM #2_Combine A
		INNER JOIN UserTable B
		ON A.username = b.Username
		INNER JOIN IB3QualificationLookup C
		on C.QualificationName in (a.qual1, a.qual2, a.qual3, a.qual4)
		--where A.Role in ('Internal verifier','Assessor')  --DO ALL
		) AS SRC
	ON TGT.UserID = SRC.UserID
		AND TGT.[QualificationID] = SRC.[QualificationID]
	WHEN NOT MATCHED THEN
		INSERT(UserID, [QualificationID])
		VALUES(SRC.UserID, SRC.[QualificationID])

			OUTPUT $action, inserted.UserID, inserted.[QualificationID]
			INTO #changesmade([statement], UserID, [QualID]);

		

--Assign Centre Administrators to only the centres as specified on the spreadsheet (tab 1)--

	MERGE INTO AssignedUserRolesTable as TGT
	USING (
	SELECT B.ID [UserID]
		, RT.ID [RoleID]
		, CT.ID CentreID
	FROM #2_Combine A
	INNER JOIN UserTable B
	ON A.username = b.Username
	INNER JOIN RolesTable RT
	on RT.Name = A.Role
	INNER JOIN CentreTable CT
	on CT.CentreName = A.CentreName
	where A.Role = 'EPA Centre Administrator'
	) as SRC

	ON TGT.UserID = SRC.UserID
		AND TGT.RoleID = SRC.RoleID
		AND TGT.CentreID = SRC.CentreID
	WHEN NOT MATCHED THEN
	INSERT(UserID, RoleID, CentreID)
	VALUES(SRC.UserID, SRC.RoleID, SRC.CentreID)

		OUTPUT $action, inserted.UserID, inserted.RoleID, inserted.CentreID 
		INTO #changesmade([statement], UserID, RoleID, CentreID);


/*
SELECT B.ID [UserID]
	, b.username
		, RT.ID [RoleID]
		, C.CentreID
FROM #2_Combine A
INNER JOIN UserTable B
ON A.username = b.Username
INNER JOIN RolesTable RT
on RT.Name = A.Role
CROSS APPLY (
	SELECT ID CentreID
	FROM CENTRETable
	where retired = 0
	) C
where A.Role in ('Internal verifier','Assessor')
order by b.username


SELECT B.ID [UserID]
	, b.username
		, RT.ID [RoleID]
		, CT.ID CentreID
FROM #2_Combine A
INNER JOIN UserTable B
ON A.username = b.Username
INNER JOIN RolesTable RT
on RT.Name = A.Role
INNER JOIN CentreTable CT
on CT.CentreName = A.CentreName
where A.Role = 'EPA Centre Administrator'
order by b.username


SELECT B.ID [UserID]
, b.username
			, C.ID [QualificationID]
FROM #2_Combine A
INNER JOIN UserTable B
ON A.username = b.Username
INNER JOIN IB3QualificationLookup C
on C.QualificationName in (a.qual1, a.qual2, a.qual3, a.qual4)
order by b.username


SELECT CT.ID CentreID	
		, B.ID QualificationID
		, ct.centrename
FROM #1_Centres_AssignQuals A
INNER JOIN CentreTable CT
on CT.CentreName = A.centrename
	and CT.CentreCode = A.centrecode
INNER JOIN IB3QualificationLookup B
on B.QualificationName in (a.qual1, a.qual2, a.qual3, a.qual4)
order by ct.centrename


	Please bear in mind the following when applying the bulk update:
	�	Assign only the Centres approved as per the spreadsheet for the quals as specified in the spreadsheet (tab 2)
	�	Assign Assessors and Internal Verifiers to all active centres on SecureAssess (tab 1)
	�	Assign Assessors and Internal Verifiers only to the quals as specified on the spreadsheet against each user (tab 1)
	�	Assign Centre Administrators to only the centres as specified on the spreadsheet (tab 1)
	�	Assign Centre Administrators to only the qualifications as specified against each user (tab 1)

*/


/*
SELECT B.ID [UserID]
, C.ID [QualificationID]
, c.QualificationName
, b.Username
FROM #2_Combine A
INNER JOIN UserTable B
ON A.username = b.Username
INNER JOIN IB3QualificationLookup C
on C.QualificationName in (a.qual1, a.qual2, a.qual3, a.qual4)
where b.username = 'EPASamanthaFord'


SELECT *
FROM #changesmade
where userID = 307521

SELECT *
FROM UserQualificationsTable
where userID = 307521

select *
from AssignedUserRolesTable aurt
left join CentreTable ct
on ct.id = aurt.CentreID
where userID = 307521



select ib.QualificationName 
from CentreQualificationsTable cq
inner join CentreTable ct
on ct.id = cq.CentreID
inner join IB3QualificationLookup ib
on ib.id = cq.qualificationid
where ct.CentreName = 'First College Louth'
order by 1

*/