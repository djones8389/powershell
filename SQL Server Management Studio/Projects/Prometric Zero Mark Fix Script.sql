SET NOCOUNT ON;

IF OBJECT_ID('tempdb..#ExamXML') IS NOT NULL DROP TABLE #ExamXML
IF OBJECT_ID('tempdb..#Item') IS NOT NULL DROP TABLE #Item

SELECT	 E.ID
		,CAST(E.StructureXML AS XML) AS [StructureXML]
		,CAST(E.ResultData AS XML) AS [ResultData]
		,CAST(E.ResultDataFull AS XML) AS [ResultDataFull]
INTO	 #ExamXML
FROM	 dbo.WAREHOUSE_ExamSessionTable E
WHERE	 E.Keycode = 'HG3JC34C'; --<< Exam(s) you want to update

SELECT	 E.ID
		,E.ResultData.value('(/exam/@passMark)[1]', 'int') AS [ExamPassMark]
		,E.ResultData.value('(/exam/@passType)[1]', 'tinyint') AS [ExamPassType]
		,E.ResultData.value('(/exam/@totalMark)[1]', 'int') AS [ExamTotalMark]
		,E.ResultData.value('(/exam/@userMark)[1]', 'decimal(6,3)') AS [ExamUserMark]
		,E.ResultData.value('(/exam/@userPercentage)[1]', 'decimal(6,3)') AS [ExamUserPercentage]
		,E.ResultData.value('(/exam/@passValue)[1]', 'tinyint') AS [ExamPassValue]
		,E.ResultData.value('(/exam/@originalPassValue)[1]', 'tinyint') AS [ExamOriginalPassValue]
		,E.ResultData.value('(/exam/@grade)[1]', 'nvarchar(100)') AS [ExamGrade]
		,E.ResultData.value('(/exam/@originalGrade)[1]', 'nvarchar(100)') AS [ExamOriginalGrade]
		,Exam.Section.value('@id', 'int') AS [SectionID]
		,Exam.Section.value('@passMark', 'int') AS [SectionPassMark]
		,Exam.Section.value('@passType', 'tinyint') AS [SectionPassType]
		,Exam.Section.value('@totalMark', 'int') AS [SectionTotalMark]
		,Exam.Section.value('@userMark', 'decimal(6,3)') AS [SectionUserMark]
		,Exam.Section.value('@userPercentage', 'decimal(6,3)') AS [SectionUserPercentage]
		,Exam.Section.value('@passValue', 'tinyint') AS [SectionPassValue]
		,Section.Item.value('@id', 'nvarchar(50)') AS [ItemID]
		,Section.Item.value('@totalMark', 'int') AS [ItemTotalMark]
		,Section.Item.value('@userMark', 'decimal(6,3)') AS [ItemUserMark]
		,Section.Item.value('@actualUserMark', 'decimal(6,3)') AS [ItemActualUserMark]
		,Section.Item.value('@userAttempted', 'tinyint') AS [ItemUserAttempted]
INTO	 #Item
FROM	 #ExamXML E
CROSS APPLY E.ResultData.nodes('/exam/section') Exam(Section)
CROSS APPLY Exam.Section.nodes('item') Section(Item)
where ISNULL(Section.Item.value('@nonScored', 'bit'), 0) = '0';


/* Update item user attempted and item user mark */
UPDATE #Item
SET ItemUserAttempted = WAREHOUSE_ExamSessionItemResponseTable.ItemResponseData.value('(/p/@ua)[1]', 'tinyint')
   ,ItemUserMark = (WAREHOUSE_ExamSessionItemResponseTable.ItemResponseData.value('(/p/@um)[1]', 'decimal(6,3)') * ItemTotalMark)
FROM #Item
INNER JOIN WAREHOUSE_ExamSessionItemResponseTable
ON #Item.ID = WAREHOUSE_ExamSessionItemResponseTable.WAREHOUSEExamSessionID
AND #Item.ItemID = WAREHOUSE_ExamSessionItemResponseTable.ItemID

/* Update item user mark based on section marking percentage
   Correct the ones over 100% */
UPDATE #Item
SET ItemUserMark = CASE
				       WHEN SectionPassType = 1 AND ItemTotalMark > 1
							THEN CAST((ItemUserMark / ItemTotalMark) AS DECIMAL(6,3))
					   ELSE ItemUserMark
				   END;
/* Update item actual user mark */
UPDATE #Item
SET ItemActualUserMark = ROUND(CASE
						     WHEN SectionPassType = 1 THEN ItemTotalMark * ItemUserMark
							 ELSE ItemActualUserMark
						 END, 0);

/* Update section mark based on item mark */

UPDATE #Item
SET SectionUserMark = SectionMarks.SectionUserMark
   ,SectionTotalMark = SectionMarks.SectionTotalMark
   ,SectionUserPercentage = 
	  CASE WHEN SectionMarks.SectionTotalMark > 0
		THEN  CAST((SectionMarks.SectionUserMark / SectionMarks.SectionTotalMark) AS DECIMAL(6,3)) * 100
		ELSE SectionUserPercentage
		END	
FROM	#Item Item
INNER JOIN (
	SELECT	 ID
			,SectionID
			,SUM(CASE
					WHEN SectionPassType = 1 THEN ItemActualUserMark
					WHEN SectionPassType = 0 THEN ItemUserMark
				END) AS [SectionUserMark]
			,SUM(ItemTotalMark) AS [SectionTotalMark]
	FROM	 #Item
	GROUP BY ID
			,SectionPassType
			,SectionID
		) SectionMarks
ON Item.ID = SectionMarks.ID
AND Item.SectionID = SectionMarks.SectionID;


/* Update section pass value */
UPDATE #Item
SET SectionPassValue = CASE
						   WHEN CASE SectionPassType
								    WHEN 1 THEN 
									    SectionUserPercentage
								    WHEN 0 THEN
									    SectionUserMark
							    END >= SectionPassMark THEN 1
						   ELSE 0
					   END;

/* Update exam metadata */
UPDATE	 #Item
SET ExamTotalMark = ExamMarks.ExamTotalMark
   , ExamUserMark = ExamMarks.ExamUserMark
FROM #Item AS [Item]
INNER JOIN (
	SELECT	 ID
			,SUM(SectionTotalMark) AS [ExamTotalMark]
			,SUM(SectionUserMark) AS [ExamUserMark]
	FROM	(
		SELECT	 ID
				,SectionTotalMark
				,SectionUserMark
		FROM #Item
		GROUP BY ID
				,SectionID
				,SectionTotalMark
				,SectionUserMark
			) Section
	GROUP BY ID
	) ExamMarks
ON Item.ID = ExamMarks.ID;

/* Update exam user percentage */
UPDATE #Item
SET ExamUserPercentage = (ExamUserMark / ExamTotalMark) * 100;

/* Update exam pass value */
UPDATE #Item
SET ExamPassValue = CASE
						WHEN CASE ExamPassType
								WHEN 1 THEN 
									ExamUserPercentage
								WHEN 0 THEN
									ExamUserMark
							END >= ExamPassMark THEN 1
						ELSE 0
					END;

UPDATE #Item
SET ExamOriginalPassValue = ExamPassValue;

UPDATE #Item
SET ExamGrade = CASE ExamPassValue
					WHEN 1 THEN N'Pass'
					WHEN 0 THEN N'Fail'
				END;

UPDATE #Item
SET ExamOriginalGrade = ExamGrade;

DECLARE Exams CURSOR FOR
	SELECT DISTINCT ID, ExamTotalMark, ExamUserMark, ExamUserPercentage, ExamPassValue, ExamOriginalPassValue, ExamGrade, ExamOriginalGrade FROM #Item;
OPEN Exams;
DECLARE @ID INT, @ExamTotalMark INT, @ExamUserMark DECIMAL(6,3), @ExamUserPercentage DECIMAL(6,3), @ExamPassValue TINYINT, @ExamOriginalPassValue TINYINT, @ExamGrade NVARCHAR(100), @ExamOriginalGrade NVARCHAR(100);
FETCH NEXT FROM Exams INTO @ID, @ExamTotalMark, @ExamUserMark, @ExamUserPercentage, @ExamPassValue, @ExamOriginalPassValue, @ExamGrade, @ExamOriginalGrade;

WHILE @@FETCH_STATUS = 0
BEGIN
	/* Update exam */
	/* Update exam total mark */
	UPDATE #ExamXML
	SET StructureXML.modify('replace value of (/assessmentDetails/assessment/@totalMark)[1] with sql:variable("@ExamTotalMark")')
	   ,ResultData.modify('replace value of (/exam/@totalMark)[1] with sql:variable("@ExamTotalMark")')
	   ,ResultDataFull.modify('replace value of (/assessmentDetails/assessment/@totalMark)[1] with sql:variable("@ExamTotalMark")')
	WHERE ID = @ID;

	/* Update exam user mark */
	UPDATE #ExamXML
	SET StructureXML.modify('replace value of (/assessmentDetails/assessment/@userMark)[1] with sql:variable("@ExamUserMark")')
	   ,ResultData.modify('replace value of (/exam/@userMark)[1] with sql:variable("@ExamUserMark")')
	   ,ResultDataFull.modify('replace value of (/assessmentDetails/assessment/@userMark)[1] with sql:variable("@ExamUserMark")')
	WHERE ID = @ID;

	/* Update exam user percentage */
	UPDATE #ExamXML
	SET StructureXML.modify('replace value of (/assessmentDetails/assessment/@userPercentage)[1] with sql:variable("@ExamUserPercentage")')
	   ,ResultData.modify('replace value of (/exam/@userPercentage)[1] with sql:variable("@ExamUserPercentage")')
	   ,ResultDataFull.modify('replace value of (/assessmentDetails/assessment/@userPercentage)[1] with sql:variable("@ExamUserPercentage")')
	WHERE ID = @ID;

	/* Update exam pass value */
	UPDATE #ExamXML
	SET StructureXML.modify('replace value of (/assessmentDetails/assessment/@passValue)[1] with sql:variable("@ExamPassValue")')
	   ,ResultData.modify('replace value of (/exam/@passValue)[1] with sql:variable("@ExamPassValue")')
	   ,ResultDataFull.modify('replace value of (/assessmentDetails/assessment/@passValue)[1] with sql:variable("@ExamPassValue")')
	WHERE ID = @ID;

	/* Update exam original pass value */
	UPDATE #ExamXML
	SET ResultData.modify('replace value of (/exam/@originalPassValue)[1] with sql:variable("@ExamOriginalPassValue")')
	WHERE ID = @ID;

	/* Update exam grade */
	UPDATE #ExamXML
	SET ResultData.modify('replace value of (/exam/@grade)[1] with sql:variable("@ExamGrade")')
	WHERE ID = @ID;

	/* Update exam original grade */
	UPDATE #ExamXML
	SET StructureXML.modify('replace value of (/assessmentDetails/assessment/@originalGrade)[1] with sql:variable("@ExamOriginalGrade")')
	   ,ResultData.modify('replace value of (/exam/@originalGrade)[1] with sql:variable("@ExamOriginalGrade")')
	   ,ResultDataFull.modify('replace value of (/assessmentDetails/assessment/@originalGrade)[1] with sql:variable("@ExamOriginalGrade")')
	WHERE ID = @ID;

	DECLARE Sections CURSOR FOR
		SELECT DISTINCT SectionID, SectionTotalMark, SectionUserMark, SectionUserPercentage, SectionPassValue FROM #Item WHERE ID = @ID;
	OPEN Sections;
	DECLARE @SectionID TINYINT, @SectionTotalMark INT, @SectionUserMark DECIMAL(6,3), @SectionUserPercentage DECIMAL(6,3), @SectionPassValue TINYINT;
	FETCH NEXT FROM Sections INTO @SectionID, @SectionTotalMark, @SectionUserMark, @SectionUserPercentage, @SectionPassValue;
	
	WHILE @@FETCH_STATUS = 0
	BEGIN
		/* Update exam section */
		/* Update exam section total mark */
		UPDATE #ExamXML
		SET StructureXML.modify('replace value of (/assessmentDetails/assessment/section[@id = sql:variable("@SectionID")]/@totalMark)[1] with sql:variable("@SectionTotalMark")')
		   ,ResultData.modify('replace value of (/exam/section[@id = sql:variable("@SectionID")]/@totalMark)[1] with sql:variable("@SectionTotalMark")')
		   ,ResultDataFull.modify('replace value of (/assessmentDetails/assessment/section[@id = sql:variable("@SectionID")]/@totalMark)[1] with sql:variable("@SectionTotalMark")')
		WHERE ID = @ID;

		/* Update exam section user mark */
		UPDATE #ExamXML
		SET StructureXML.modify('replace value of (/assessmentDetails/assessment/section[@id = sql:variable("@SectionID")]/@userMark)[1] with sql:variable("@SectionUserMark")')
		   ,ResultData.modify('replace value of (/exam/section[@id = sql:variable("@SectionID")]/@userMark)[1] with sql:variable("@SectionUserMark")')
		   ,ResultDataFull.modify('replace value of (/assessmentDetails/assessment/section[@id = sql:variable("@SectionID")]/@userMark)[1] with sql:variable("@SectionUserMark")')
		WHERE ID = @ID;

		/* Update exam section user percentage */
		UPDATE #ExamXML
		SET StructureXML.modify('replace value of (/assessmentDetails/assessment/section[@id = sql:variable("@SectionID")]/@userPercentage)[1] with sql:variable("@SectionUserPercentage")')
		   ,ResultData.modify('replace value of (/exam/section[@id = sql:variable("@SectionID")]/@userPercentage)[1] with sql:variable("@SectionUserPercentage")')
		   ,ResultDataFull.modify('replace value of (/assessmentDetails/assessment/section[@id = sql:variable("@SectionID")]/@userPercentage)[1] with sql:variable("@SectionUserPercentage")')
		WHERE ID = @ID;

		/* Update exam section pass value */
		UPDATE #ExamXML
		SET StructureXML.modify('replace value of (/assessmentDetails/assessment/section[@id = sql:variable("@SectionID")]/@passValue)[1] with sql:variable("@SectionPassValue")')
		   ,ResultData.modify('replace value of (/exam/section[@id = sql:variable("@SectionID")]/@passValue)[1] with sql:variable("@SectionPassValue")')
		   ,ResultDataFull.modify('replace value of (/assessmentDetails/assessment/section[@id = sql:variable("@SectionID")]/@passValue)[1] with sql:variable("@SectionPassValue")')
		WHERE ID = @ID;

		DECLARE Items CURSOR FOR
			SELECT DISTINCT ItemID, ItemUserMark, ItemActualUserMark, ItemUserAttempted FROM #Item WHERE ID = @ID AND SectionID = @SectionID;
		OPEN Items;
		DECLARE @ItemID NVARCHAR(100), @ItemUserMark DECIMAL(6,3), @ItemActualUserMark DECIMAL(6,3), @ItemUserAttempted TINYINT;
		FETCH NEXT FROM Items INTO @ItemID, @ItemUserMark, @ItemActualUserMark, @ItemUserAttempted;

		WHILE @@FETCH_STATUS = 0
		BEGIN
			/* Update exam section item */
			/* Update exam section item user mark */
			UPDATE #ExamXML
			SET StructureXML.modify('replace value of (/assessmentDetails/assessment/section[@id = sql:variable("@SectionID")]/item[@id = sql:variable("@ItemID")]/@userMark)[1] with sql:variable("@ItemUserMark")')
			   ,ResultData.modify('replace value of (/exam/section[@id = sql:variable("@SectionID")]/item[@id = sql:variable("@ItemID")]/@userMark)[1] with sql:variable("@ItemUserMark")')
			   ,ResultDataFull.modify('replace value of (/assessmentDetails/assessment/section[@id = sql:variable("@SectionID")]/item[@id = sql:variable("@ItemID")]/@userMark)[1] with sql:variable("@ItemUserMark")')
			WHERE ID = @ID;

			/* Update exam section item actual user mark */
			UPDATE #ExamXML
			SET ResultData.modify('replace value of (/exam/section[@id = sql:variable("@SectionID")]/item[@id = sql:variable("@ItemID")]/@actualUserMark)[1] with sql:variable("@ItemActualUserMark")')
			WHERE ID = @ID;

			/* Directly update the marker response */
			--UPDATE WAREHOUSE_ExamSessionItemResponseTable
			--SET MarkerResponseData.modify('replace value of (/entries/entry[userId = -1]/assignedMark/text())[1] with sql:variable("@ItemActualUserMark")')
			--WHERE WAREHOUSEExamSessionID = @ID
			--	AND ItemID = @ItemID;

			/* Update exam section item user attempted */
			UPDATE #ExamXML
			SET StructureXML.modify('replace value of (/assessmentDetails/assessment/section[@id = sql:variable("@SectionID")]/item[@id = sql:variable("@ItemID")]/@userAttempted)[1] with sql:variable("@ItemUserAttempted")')
			   ,ResultData.modify('replace value of (/exam/section[@id = sql:variable("@SectionID")]/item[@id = sql:variable("@ItemID")]/@userAttempted)[1] with sql:variable("@ItemUserAttempted")')
			   ,ResultDataFull.modify('replace value of (/assessmentDetails/assessment/section[@id = sql:variable("@SectionID")]/item[@id = sql:variable("@ItemID")]/@userAttempted)[1] with sql:variable("@ItemUserAttempted")')
			WHERE ID = @ID;

			FETCH NEXT FROM Items INTO @ItemID, @ItemUserMark, @ItemActualUserMark, @ItemUserAttempted;
		END

		CLOSE Items;
		DEALLOCATE Items;

		FETCH NEXT FROM Sections INTO @SectionID, @SectionTotalMark, @SectionUserMark, @SectionUserPercentage, @SectionPassValue;
	END

	CLOSE Sections;
	DEALLOCATE Sections;

	FETCH NEXT FROM Exams INTO @ID, @ExamTotalMark, @ExamUserMark, @ExamUserPercentage, @ExamPassValue, @ExamOriginalPassValue, @ExamGrade, @ExamOriginalGrade;
END

CLOSE Exams;
DEALLOCATE Exams;
			
SELECT * FROM #ExamXML;
SELECT * FROM #Item;

/*

/* Update database tables */
UPDATE WAREHOUSE_ExamSessionTable
SET StructureXML = #ExamXML.StructureXML, ResultData = #ExamXML.ResultData, ResultDataFull = #ExamXML.ResultDataFull
FROM WAREHOUSE_ExamSessionTable
INNER JOIN #ExamXML ON WAREHOUSE_ExamSessionTable.ID = #ExamXML.ID;

UPDATE WAREHOUSE_ExamSessionTable_Shreded
SET ResultData = #ExamXML.ResultData
FROM WAREHOUSE_ExamSessionTable_Shreded
INNER JOIN #ExamXML ON WAREHOUSE_ExamSessionTable_Shreded.ExamSessionID = #ExamXML.ID;

/* Delete shredded & shredded items */
DELETE FROM dbo.WAREHOUSE_ExamSessionTable_ShrededItems WHERE ExamSessionID IN (SELECT ID FROM #ExamXML);
DELETE FROM dbo.WAREHOUSE_ExamSessionTable_Shreded WHERE ExamSessionID IN (SELECT ID FROM #ExamXML);

/* Re-Shred */
     insert into WAREHOUSE_ExamSessionTable_Shreded  
     (examSessionId,      
      examVersionName,      
      examVersionRef,      
      examVersionId,      
      examName,      
      examRef,      
      qualificationid,      
      qualificationName,      
      qualificationRef,      
      resultData,      
      submittedDate,      
      originatorId,      
      centreName,      
      centreCode,      
      foreName,      
      dateOfBirth,      
      gender,      
      candidateRef,      
      surName,      
      scheduledDurationValue,      
      previousExamState,      
      examStateInformation,      
      examResult,      
      passValue,      
      closeValue,      
      --0,      
      externalReference,      
      examType,      
      warehouseTime,      
      CQN,      
      actualDuration,      
      appeal,      
      reMarkStatus,        
      qualificationLevel,   
      centreId,     
      uln,  
      AllowPackageDelivery,  
      ExportToSecureMarker,  
      ContainsBTLOffice,
      ExportedToIntegration,
      WarehouseExamState,
      [language],
      KeyCode,
      TargetedForVoid,
      EnableOverrideMarking,
      AddressLine1,
      AddressLine2,
      Town,
      County,
      Country,
      Postcode,
      ScoreBoundaryData,
      itemMarksUploaded,
      passMark,
      totalMark,
      passType,
      voidJustificationLookupTableId,
      automaticVerification,
      resultSampled,
      [started],
      submitted,
      validFromDate,
      expiryDate,
      totalTimeSpent,
      defaultDuration,
      scheduledDurationReason,
      WAREHOUSEScheduledExamID,
      WAREHOUSEUserID,
      itemDataFull,
      examId,
      userId,
      TakenThroughLocalScan,
	  LocalScanDownloadDate,
	  LocalScanUploadDate,
	  LocalScanNumPages,
	  CertifiedForTablet,
	  IsProjectBased)
     SELECT      
      examSessionId,      
      examVersionName,      
      examVersionRef,      
      examVersionId,      
      examName,      
      examRef,      
      qualificationid,      
      qualificationName,      
      qualificationRef,      
      resultData,      
      submittedDate,      
      originatorId,      
      centreName,      
      centreCode,      
      foreName,      
      dateOfBirth,      
      gender,      
      candidateRef,      
      surName,      
      scheduledDurationValue,      
      previousExamState,      
      examStateInformation,      
      examResult,      
      passValue,      
      closeValue,      
      --0,      
      externalReference,      
      examType,      
      warehouseTime,      
      CQN,      
      actualDuration,      
      appeal,      
      reMarkStatus,        
      qualificationLevel,   
      centreId,     
      uln,  
      AllowPackageDelivery,  
      ExportToSecureMarker,  
      ContainsBTLOffice,
      ExportedToIntegration,
      WarehouseExamState,
      [language],
      KeyCode,
      TargetedForVoid,
      EnableOverrideMarking,
      AddressLine1,
      AddressLine2,
      Town,
      County,
      Country,
      Postcode,
      ScoreBoundaryData,
      itemMarksUploaded,
      passMark,
      totalMark,
      passType,
      voidJustificationLookupTableId,
      automaticVerification,
      resultSampled,
      [started],
      submitted,
      validFromDate,
      expiryDate,
      totalTimeSpent,
      defaultDuration,
      scheduledDurationReason,
      WAREHOUSEScheduledExamID,
      WAREHOUSEUserID,
      itemDataFull,
      examId,
      userId,
      TakenThroughLocalScan,
	  LocalScanDownloadDate,
	  LocalScanUploadDate,
	  LocalScanNumPages,
	  CertifiedForTablet,
	  IsProjectBased
     FROM sa_CandidateExamAudit_View  
WHERE examSessionId IN (
		SELECT ID
		FROM #ExamXML
		);

/* Re-Shred items */
INSERT INTO WAREHOUSE_ExamSessionTable_ShrededItems (
	ExamSessionID
	,ItemRef
	,ItemName
	,UserMark
	,MarkerUserMark
	,UserAttempted
	,ItemVersion
	,MarkingIgnored
	,MarkedMetadataCount
	,ExamPercentage
	,TotalMark
	,ResponseXML
	,OptionsChosen
	,SelectedCount
	,CorrectAnswerCount
	,itemType
	,FriendItems
	)
SELECT WAREHOUSE_ExamSessionTable.ID AS [examSessionId]
	,Result.Item.value('@id', 'nvarchar(15)') AS [itemRef]
	,Result.Item.value('@name', 'nvarchar(200)') AS [itemName]
	,Result.Item.value('@userMark', 'decimal(6, 3)') AS [userMark]
	,Result.Item.value('@markerUserMark', 'nvarchar(max)') AS [markerUserMark]
	,Result.Item.value('@userAttempted', 'bit') AS [userAttempted]
	,Result.Item.value('@version', 'int') AS [itemVersion]
	,Result.Item.value('@markingIgnored', 'tinyint') AS [markingIgnored]
	,Result.Item.value('count(mark)', 'int') AS [markedMetadataCount]
	,WAREHOUSE_ExamSessionTable.ResultData.value('(exam/@userPercentage)[1]', 'decimal(6, 3)') AS [examPercentage]
	,Result.Item.value('@totalMark', 'decimal(6, 3)') AS [totalMark]
	,WAREHOUSE_ExamSessionItemResponseTable.ItemResponseData AS [responseXml]
	,CAST(ISNULL(WAREHOUSE_ExamSessionItemResponseTable.ItemResponseData.query('data(p/s/c[@typ = "10"]/i[@sl = "1"]/@ac)'), ' UA ') AS NVARCHAR(200)) AS [optionsChosen]
	,WAREHOUSE_ExamSessionItemResponseTable.ItemResponseData.value('count(p/s/c/i[@sl = "1"])', 'int') AS [selectedCount]
	,WAREHOUSE_ExamSessionItemResponseTable.ItemResponseData.value('count(p/s/c/i[@ca = "1"])', 'int') AS [correctAnswerCount]
	,Result.Item.value('@type', 'int') AS ItemType
	,Result.Item.value('@SurpassFriendItems', 'NVARCHAR(MAX)') AS FriendItems
FROM WAREHOUSE_ExamSessionTable
CROSS APPLY WAREHOUSE_ExamSessionTable.ResultData.nodes('exam/section/item') Result(Item)
LEFT JOIN WAREHOUSE_ExamSessionItemResponseTable ON WAREHOUSE_ExamSessionItemResponseTable.WAREHOUSEExamSessionID = WAREHOUSE_ExamSessionTable.ID
	AND WAREHOUSE_ExamSessionItemResponseTable.ItemID = Result.Item.value('@id', 'nvarchar(15)')
WHERE WAREHOUSE_ExamSessionTable.ID IN (
		SELECT ID
		FROM #ExamXML
		);

*/

/* Cleanup */
DROP TABLE #Item;
DROP TABLE #ExamXML;












