DECLARE @command1 nvarchar(1000) = '

USE [?];

select ''?''  as DBName
		, u.name as ''User''
		, (select SUSER_SNAME (u.sid)) as ''Login''
		, r.name  as RoleName
		--, u.*
from sys.database_role_members RM 
	right join sys.database_principals U on U.principal_id = RM.member_principal_id
	left join sys.database_principals R on R.principal_id = RM.role_principal_id
where u.type<>''R''
	and U.type = ''S''
	and r.name IS NOT NULL
	and ''?'' not in (''msdb'',''tempdb'',''model'',''master'')
'


DECLARE @UserLogins TABLE (DBName nvarchar(150), userName nvarchar(150), loginName nvarchar(150), RoleName nvarchar(50))
INSERT @UserLogins
EXEC sp_MSforeachdb  @command1




SELECT * FROM @UserLogins where loginName = 'Dave.Jones'


	where RoleName = 'db_owner'	
	and (dbname like '%itembank%'
	or dbname  like '%secureassess%' 
	or dbname  like '%content%' )
	and userName != 'dbo'
ORDER BY 1


--IF NOT EXISTS Create Login []
--USE [DBName] Create User [] for Login []
--EXEC sp_addrolemember 'RoleName' 'UserName'


SELECT *, 'USE '  + QUOTENAME(DBNAME) +  ' DROP USER ' + QUOTENAME(Username) ';'
FROM @UserLogins
where loginname is null
	and username !='dbo'


SELECT DISTINCT

	'IF NOT EXISTS (SELECT 1 FROM Sys.sql_logins where name =''' + Name + '_Login' + ''') BEGIN
		 CREATE LOGIN [' + Name + '_Login]'  + ' WITH PASSWORD = ''s_!85WrenukEtrut+4f5a#-a&ac*aKUs'';' +
    'USE ' + QUOTENAME(Name) + ';  CREATE USER ' + Name + '_User' + ' FOR LOGIN ' + Name + '_Login' +
    '; EXEC sp_addrolemember ''db_owner'',  ''' + Name + '_User'' END ' +
    
    'IF  EXISTS (SELECT 1 FROM Sys.sql_logins where name =''' + Name + '_Login' + ''') BEGIN
		
    USE ' + QUOTENAME(Name) + ';  CREATE USER ' + Name + '_User' + ' FOR LOGIN ' + Name + '_Login' +
    '; EXEC sp_addrolemember ''db_owner'',  ''' + Name + '_User'' END'

from sys.databases
where name like '%PRV_OCR%';



SELECT DISTINCT --*,--'USE [' + DBName + '] CREATE USER [' + userName + '] FOR LOGIN [' + loginName +']; EXEC sp_addrolemember '''+ RoleName +''','''+ userName  +''''

	'IF NOT EXISTS (SELECT 1 FROM Sys.sql_logins where name =''' + DBName + '_Login' + ''') BEGIN
		 CREATE LOGIN [' + DBName + '_Login]'  + ' WITH PASSWORD = ''s_!85WrenukEtrut+4f5a#-a&ac*aKUs'';' +
    'USE ' + QUOTENAME(DBNAME) + ';  CREATE USER ' + DBName + '_User' + ' FOR LOGIN ' + DBName + '_Login' +
    '; EXEC sp_addrolemember ''db_owner'',  ''' + DBNAME + '_User'' END ' +
    
    'IF  EXISTS (SELECT 1 FROM Sys.sql_logins where name =''' + DBName + '_Login' + ''') BEGIN
		
    USE ' + QUOTENAME(DBNAME) + ';  CREATE USER ' + DBName + '_User' + ' FOR LOGIN ' + DBName + '_Login' +
    '; EXEC sp_addrolemember ''db_owner'',  ''' + DBNAME + '_User'' END' 
        
FROM @UserLogins
	where DBName like '%PRV_OCR%';
