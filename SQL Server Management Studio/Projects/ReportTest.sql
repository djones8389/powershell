DECLARE @ExpandVersions int

IF @CPID = '' SET @CPID = NULL
IF @CPID IS NOT NULL SET @ExpandVersions = 1 ELSE SET @ExpandVersions = 0

DECLARE @ES TABLE(
		ExamSessionKey int
		,CentreKey int
		,QualificationKey int
		,ExamKey int
		,ExamVersionKey int
		,UserMarks float
		,CompletionDateTime datetime
)

INSERT INTO @ES
SELECT 
	ES.ExamSessionKey
	,ES.CentreKey
	,ES.QualificationKey
	,ES.ExamKey
	,ES.ExamVersionKey
	,ES.UserMarks
	,T.FullDateAlternateKey + ES.CompletionTime  CompletionDateTime
FROM	 
	dbo.FactExamSessions AS ES
	INNER JOIN dbo.DimTime AS T
		 ON ES.CompletionDateKey = T.TimeKey
WHERE	 
	(T.FullDateAlternateKey + ES.CompletionTime) BETWEEN @dateStartRange AND @dateEndRange
	AND ES.FinalExamState <> 10
	AND ES.CentreKey IN (SELECT Value FROM dbo.fn_ParamsToList(@centres,0))
	AND ES.QualificationKey IN (SELECT Value FROM dbo.fn_ParamsToList(@subjects,0))
	AND ES.ExamVersionKey = @examVersion
OPTION (MAXRECURSION 0)


DECLARE @Items TABLE (
	Inx INT
	,Inx5 INT
	,ExamSessionKey INT
	,ExamVersionKey INT
	,CPID NVARCHAR(50)
	,CPVersion INT
	,OriginalVersion INT
	,UserMarks FLOAT
	,MarkerMark FLOAT
	,FQRMark FLOAT
	,QuestionMarks FLOAT
	,QuestionTotalMarks FLOAT
	,QuestionName NVARCHAR(100)
	,QuestionTypeKey INT
	,CAQuestionTypeKey INT
	,ViewingTime INT
	,Attempted FLOAT
	,TestOrder INT
	,CAId INT
	,CompletionDateTime DATETIME
	,ModifiedBy INT

	)



/*
CREATE INDEX [DeploymentStatuses_733] ON [CMS].[dbo].[DeploymentStatuses] ([ParentId])

CREATE CLUSTERED INDEX [Cluster_ESID_CPID] ON [@Items] (
  
  	ExamSessionKey
	,CPID
)


*/

--;WITH items AS (
---------------Query for NULL @unit and @LO -----------
--	SELECT	
--		NULL Inx --NTILE(3) OVER(PARTITION BY ES.ExamVersionKey, QR.CPID, Q.CPVersion ORDER BY UserMarks DESC, QR.ExamSessionKey ASC) Inx
--		,NULL Inx5 --,NTILE(5) OVER(PARTITION BY ES.ExamVersionKey, QR.CPID, Q.CPVersion ORDER BY UserMarks DESC, QR.ExamSessionKey ASC) Inx5
--		,ES.ExamSessionKey
--		,ES.ExamVersionKey
--		,QR.CPID
--		,QR.CPVersion CPVersion --IF @ExpandVersions=0 it will be overwritten below
--		,QR.CPVersion OriginalVersion
--		,ES.UserMarks
--		,(SELECT TOP 1 AssignedMark FROM dbo.FactMarkerResponse MR WHERE MR.ExamSessionKey = QR.ExamSessionKey AND MR.CPID = QR.CPID ORDER BY SEQNO DESC) AssM
--		,QR.Mark FQRMark
--		,NULL AssM2
--		,NULL QuestionTotalMarks
--		,NULL QuestionName
--		,NULL QuestionTypeKey
--		,NULL CAQuestionTypeKey
--		,NULL CAId
--		,QR.ViewingTime
--		,QR.Attempted
--		,QR.ItemPresentationOrder
--		,ES.CompletionDateTime
--		,NULL ModifiedByUserKey
--	FROM	
--	@ES ES
--	INNER JOIN	FactQuestionResponses QR
--		 ON ES.ExamSessionKey = QR.ExamSessionKey
--		 AND (@CPID IS NULL OR QR.CPID = @CPID)
--)
INSERT INTO @Items 
	SELECT	
		NULL Inx --NTILE(3) OVER(PARTITION BY ES.ExamVersionKey, QR.CPID, Q.CPVersion ORDER BY UserMarks DESC, QR.ExamSessionKey ASC) Inx
		,NULL Inx5 --,NTILE(5) OVER(PARTITION BY ES.ExamVersionKey, QR.CPID, Q.CPVersion ORDER BY UserMarks DESC, QR.ExamSessionKey ASC) Inx5
		,ES.ExamSessionKey
		,ES.ExamVersionKey
		,QR.CPID
		,QR.CPVersion CPVersion --IF @ExpandVersions=0 it will be overwritten below
		,QR.CPVersion OriginalVersion
		,ES.UserMarks
		,(SELECT TOP 1 AssignedMark FROM dbo.FactMarkerResponse MR WHERE MR.ExamSessionKey = QR.ExamSessionKey AND MR.CPID = QR.CPID ORDER BY SEQNO DESC) AssM
		,QR.Mark FQRMark
		,NULL AssM2
		,NULL QuestionTotalMarks
		,NULL QuestionName
		,NULL QuestionTypeKey
		,NULL CAQuestionTypeKey
		,NULL CAId
		,QR.ViewingTime
		,QR.Attempted
		,QR.ItemPresentationOrder
		,ES.CompletionDateTime
		,NULL ModifiedByUserKey
	FROM	
	@ES ES
	INNER JOIN	FactQuestionResponses QR
		 ON ES.ExamSessionKey = QR.ExamSessionKey
		 AND (@CPID IS NULL OR QR.CPID = @CPID)


IF @ExpandVersions = 0 BEGIN
	;WITH Versions AS (
		SELECT CPID, MAX(CPVersion) MaxVersion
		FROM @items
		GROUP BY CPID
	)
	UPDATE @Items 
	SET 
		CPVersion = V.MaxVersion
	FROM 
		@Items I 
		JOIN Versions V ON I.CPID = V.CPID
END

;WITH items2 AS (
	SELECT 
		I.ExamSessionKey
		,I.CPID
		,NTILE(3) OVER(PARTITION BY ExamVersionKey, I.CPID, I.CPVersion ORDER BY UserMarks DESC, ExamSessionKey ASC) Inx
		,NTILE(5) OVER(PARTITION BY ExamVersionKey, I.CPID, I.CPVersion ORDER BY UserMarks DESC, ExamSessionKey ASC) Inx5
		,Q.TotalMark * I.FQRMark AssM2
		,Q.TotalMark QuestionTotalMarks
		,Q.QuestionName
		,Q.QuestionTypeKey
		,Q.CAQuestionTypeKey
		,CASE WHEN Q.CAQuestionTypeKey IS NOT NULL THEN Q.ExternalId ELSE NULL END CAId
		,Q.ModifiedByUserKey
	FROM 
		@Items I
		JOIN DimQuestions Q 
			ON I.CPID = Q.CPID
			AND I.CPVersion = Q.CPVersion
)
UPDATE I
SET
	 Inx = I2.Inx
	,Inx5 = I2.Inx5
	,QuestionMarks = ISNULL(I.MarkerMark, I2.AssM2)
	,QuestionTotalMarks = I2.QuestionTotalMarks
	,QuestionName = I2.QuestionName
	,QuestionTypeKey = I2.QuestionTypeKey
	,CAQuestionTypeKey = I2.CAQuestionTypeKey
	,CAId = I2.CAId
	,ModifiedBy = I2.ModifiedByUserKey
FROM 
	@Items I 
	JOIN items2 I2 
		ON I.CPID = I2.CPID
		AND I.ExamSessionKey = I2.ExamSessionKey

DELETE FROM @Items WHERE QuestionTotalMarks=0

;WITH FVs AS (
	SELECT 
		Inx
		,CPID
		,CPVersion
		,AVG(QuestionMarks/QuestionTotalMarks) FV
	FROM @Items
	WHERE	 
		Inx IN (1, 3)
	GROUP BY 
		Inx
		,CPID
		,CPVersion
), FVQs AS (
	SELECT 
		Inx5 Inx
		,CPID
		,CPVersion
		,AVG(QuestionMarks/QuestionTotalMarks) FV
	FROM @Items
	GROUP BY 
		Inx5
		,CPID
		,CPVersion
)
SELECT (
	SELECT 
		ESI.ExamKey AS [@Key]
		,DE.ExamReference AS [@Ref]
		,DE.ExamName AS [@Name]
		,ESI.ExamSessionKey AS [@SessionKey]
		,ESI.CompletionDateTime AS [@Date]
		,ESI.CentreKey AS [Centre/@Key]
		,DC.CentreCode AS [Centre/@Ref]
		,DC.CentreName AS [Centre/@Name]
		,ESI.QualificationKey AS [Qualification/@Key]
		,DQ.QualificationRef AS [Qualification/@Ref]
		,DQ.QualificationName AS [Qualification/@Name]
	FROM 
		@ES ESI
		JOIN DimExams DE
			ON DE.ExamKey = ESI.ExamKey

		JOIN DimCentres DC
			ON DC.CentreKey = ESI.CentreKey
		JOIN DimQualifications DQ
			ON DQ.QualificationKey = ESI.QualificationKey
	WHERE ESI.ExamSessionKey = (
		SELECT MAX(ExamSessionKey) 
		FROM 
			@Items ESII 
		WHERE 
			ESII.CompletionDateTime = (
				SELECT MAX(ESO.CompletionDateTime) 
				FROM @Items ESO 
				/*WHERE
					(CPID = @CPID OR @CPID IS NULL)
					AND (CPVersion = @CPVersion OR @ExpandVersions=0)
					*/
			)
	)	
	FOR XML PATH('LastUsedExam'), TYPE
)
,( 
	SELECT * FROM (
		SELECT 
			1 AS [Tag]
			,NULL AS [Parent]
			,NULL AS [Items!1] -- root element
			,NULL AS [Item!2!CPID]
			,NULL AS [Item!2!CPVersion]
			,NULL AS [Item!2!VersionCount]
			,NULL AS [Item!2!CAID]
			,NULL AS [Item!2!Name]
			,NULL AS [Item!2!Type]
			,NULL AS [Item!2!QuestionTypeKey]
			,NULL AS [Item!2!AuthoringQuestionTypeKey]
			,NULL AS [Item!2!TotalMark]
			,NULL AS [Item!2!AverageMark]
			,NULL AS [Item!2!PercentUnAnswered]
			,NULL AS [Item!2!FacilityValue]
			,NULL AS [Item!2!DI]
			,NULL AS [Item!2!AttemptedCount]
			,NULL AS [Item!2!ViewedUnattemptedCount]
			,NULL AS [Item!2!NotViewedCount]
			,NULL AS [Item!2!AverageViewingTime]
			,NULL AS [Item!2!TestOrder]
			,NULL AS [Item!2!TestOrderIsFixed]
			,NULL AS [Item!2!LastModifierUserId]
			,NULL AS [Item!2!LastModifierFirstName]
			,NULL AS [Item!2!LastModifierLastName]
			,NULL AS [Quintile!3!Quintile]
			,NULL AS [Quintile!3!FacilityValue]

		UNION ALL

		SELECT 
			2
			,1
			,NULL
			,I.CPID
			,I.CPVersion
			,COUNT(DISTINCT I.OriginalVersion) VersionCount
			,I.CAId
			,I.QuestionName 
			,DQT.QuestionType 
			,I.QuestionTypeKey
			,I.CAQuestionTypeKey
			,AVG(I.QuestionTotalMarks)
			,AVG(I.QuestionMarks) 
			,(1 - (SUM(CAST(I.Attempted AS FLOAT)) / COUNT(I.CPID)))
			,AVG(I.QuestionMarks/I.QuestionTotalMarks) 
			,(FV1.FV - FV3.FV)
			--,COUNT(I.CPID)
			,CAST(SUM(I.Attempted) AS int) [Item!2!AttemptedCount]
			,SUM(CASE WHEN I.Attempted = 0 AND I.ViewingTime>0 THEN 1 ELSE 0 END) [Item!2!ViewedUnattemptedCount]
			,SUM(CASE WHEN I.Attempted = 0 AND I.ViewingTime=0 THEN 1 ELSE 0 END) [Item!2!NotViewedCount]
			,AVG(I.ViewingTime)
			,MIN(I.TestOrder)
			--,CASE WHEN VAR(I.TestOrder)<>0 THEN -1 ELSE MIN(I.TestOrder) END 
			,CASE WHEN VAR(I.TestOrder)<>0 THEN 0 ELSE 1 END OrderIsFixed
			,I.ModifiedBy AS [Item!2!LastModifierUserId]
			,DCU.FirstName AS [Item!2!LastModifierFirstName]
			,DCU.LastName AS [Item!2!LastModifierLastName]
			,NULL
			,NULL
		FROM @Items I
			JOIN DimQuestionTypes DQT 
				ON DQT.QuestionTypeKey = I.QuestionTypeKey
		LEFT JOIN FVs FV1 ON I.CPID = FV1.CPID
			AND FV1.Inx = 1
			AND I.CPVersion = FV1.CPVersion
		LEFT JOIN FVs FV3 ON I.CPID = FV3.CPID
			AND FV3.Inx = 3
			AND I.CPVersion = FV3.CPVersion
		LEFT JOIN DimContentUsers DCU
			ON DCU.UserKey = I.ModifiedBy
		GROUP BY 
			I.CPID
			,I.CPVersion
			,I.CAId
			,I.QuestionName
			,DQT.QuestionType
			,I.QuestionTypeKey
			,(FV1.FV - FV3.FV)
			,I.CAQuestionTypeKey
			,I.ModifiedBy 
			,DCU.FirstName
			,DCU.LastName 
		UNION ALL

		SELECT 
			3 AS [Tag]
			,2 AS [Parent]
			,NULL AS [Items!1]
			,FVQs.CPID AS [Item!2!CPID]
			,FVQs.CPVersion AS [Item!2!CPVersion]
			,NULL AS [Item!2!VersionCount]
			,NULL AS [Item!2!CAID]
			,NULL AS [Item!2!Name]
			,NULL AS [Item!2!Type]
			,NULL AS [Item!2!QuestionTypeKey]
			,NULL AS [Item!2!authQuestionTypeKey]
			,NULL AS [Item!2!TotalMark]
			,NULL AS [Item!2!AverageMark]
			,NULL AS [Item!2!PercentUnAnswered]
			,NULL AS [Item!2!FacilityValue]
			,NULL AS [Item!2!DI]
			,NULL AS [Item!2!AttemptedCount]
			,NULL AS [Item!2!ViewedUnattemptedCount]
			,NULL AS [Item!2!NotViewedCount]
			,NULL AS [Item!2!AverageViewingTime]
			,NULL AS [Item!2!TestOrder]
			,NULL AS [Item!2!TestOrderIsFixed]
			,NULL AS [Item!2!LastModifierUserId]
			,NULL AS [Item!2!LastModifierFirstName]
			,NULL AS [Item!2!LastModifierLastName]
			,FVQs.Inx 
			,FVQs.FV AS [@FacilityValue]
		FROM FVQs

	) t
	ORDER BY [Item!2!CPID], [Item!2!CPVersion], Tag, [Quintile!3!Quintile]
	FOR XML EXPLICIT, TYPE
)
FOR XML PATH('Report')
OPTION (RECOMPILE)</reportSQL>
</test>