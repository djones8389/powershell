IF OBJECT_ID('tempdb..##DATALENGTH') IS NOT NULL DROP TABLE ##DATALENGTH;

CREATE TABLE ##DATALENGTH (
	client nvarchar(1000)
	, DecimalSeparator nvarchar(10)
);


DECLARE @dynamic nvarchar(MAX)='';

select @dynamic +=CHAR(13)+ '
use '+QUOTENAME([NAME])+'

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

INSERT ##DATALENGTH
select db_name()
	,	DecimalSeparator
from dbo.SiteSettings
'
from sys.databases
where state_desc = 'ONLINE'
	and name like '%[_]surpassmanagement';

exec(@dynamic);




select distinct *
from ##DATALENGTH
order by 1






