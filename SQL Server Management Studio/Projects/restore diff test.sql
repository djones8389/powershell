--Use master;

--DECLARE @DefaultLocation TABLE (
--     Value NVARCHAR(100)  
--     , Data NVARCHAR(100)
--) 

--INSERT @DefaultLocation
--EXEC  master.dbo.xp_instance_regread 
-- N'HKEY_LOCAL_MACHINE', N'Software\Microsoft\MSSQLServer\MSSQLServer',N'BackupDirectory'

----DECLARE @Location nvarchar(100) = 'T:\Backup'
--DECLARE @Location nvarchar(100) = (SELECT Data from @DefaultLocation)

--SELECT N'BACKUP DATABASE ['+name+N'] TO DISK = N''' +  @Location + '\' + NAME + '.' + CONVERT(VARCHAR(10), GETDATE(), 102) + '.bak'' WITH NAME = N'''+name+N'- Database Backup'',  NOFORMAT, NOINIT, SKIP, COMPRESSION, NOREWIND, NOUNLOAD, STATS = 10;'  
--FROM sys.databases 
--where database_id > 4
--ORDER BY name;

	use master;

	--backup fully

	BACKUP DATABASE [Abcawards_SecureAssess] TO DISK = N'D:\Backup\Abcawards_SecureAssess.2018.03.27.bak' 
		WITH NAME = N'Abcawards_SecureAssess- Database Backup',  NOFORMAT, NOINIT, SKIP, COMPRESSION, NOREWIND, NOUNLOAD, STATS = 10;

	use [Abcawards_SecureAssess]
	create table hi2 (id int);

	use master;

	--restore fully
	RESTORE DATABASE [Abcawards_SecureAssess] FROM DISK = N'D:\Backup\Abcawards_SecureAssess.2018.03.27.bak' 
		WITH FILE = 1, NOUNLOAD, NOREWIND, REPLACE, STATS = 10, MOVE 'Data' TO N'E:\Data\Abcawards_SecureAssess.mdf', MOVE 'Log' TO N'E:\Log\Abcawards_SecureAssess.ldf'
		, noRECOVERY

	--create differential

	BACKUP DATABASE [Abcawards_SecureAssess] TO DISK = N'D:\Backup\Abcawards_SecureAssessDiff.2018.03.27.bak' 
		WITH NAME = N'Abcawards_SecureAssess- Database Backup', DIFFERENTIAL, NOFORMAT, NOINIT, SKIP, COMPRESSION, NOREWIND, NOUNLOAD, STATS = 10;

	--restore differential
		
	RESTORE DATABASE [Abcawards_SecureAssess] FROM DISK = N'D:\Backup\Abcawards_SecureAssessDiff.2018.03.27.bak' 
		WITH FILE = 1, NOUNLOAD, NOREWIND, REPLACE, STATS = 10, MOVE 'Data' TO N'E:\Data\Abcawards_SecureAssess.mdf', MOVE 'Log' TO N'E:\Log\Abcawards_SecureAssess.ldf'		
		, RECOVERY




