USE [Performance.Staging]
GO

/****** Object:  UserDefinedFunction [dbo].[fn_GetSqlStateAggregates]    Script Date: 07/24/2017 15:54:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[fn_GetSqlStateAggregates]
(
	@testId INT
)
RETURNS @aggregates TABLE (
	StateManagementTypeId TINYINT,
	StateId INT,
	Minimum INT,
	Maximum INT,
	[Starting Amount] INT
)
AS
BEGIN
	-- Get all states that have progressed / changed in the time window
	DECLARE @StateStatistics TABLE (StateId INT, Minimun INT, Maximum INT, Diff INT)
	
	DECLARE @Dynamic NVARCHAR(MAX) = ''
	
	SELECT @Dynamic = 
	
	IF EXISTS(SELECT TOP (1) * FROM [dbo].[Aggregations_StateManagemet_TimeLine] WHERE TestId = 431)
	BEGIN
	
	INSERT INTO @StateStatistics
		SELECT 
			StateId,
			MIN([Count]) AS Minimum,
			MAX([Count]) AS Maximum,
			MAX([Count]) - MIN([Count]) AS [Difference]
		  FROM [Aggregations_StateManagemet_TimeLine]
		  WHERE TestID = 431
		  GROUP BY StateId
	END
	ELSE
	BEGIN
	INSERT INTO @StateStatistics
		SELECT 
			StateId,
			MIN([Count]) AS Minimum,
			MAX([Count]) AS Maximum,
			MAX([Count]) - MIN([Count]) AS [Difference]
		  FROM StateManagement
		  WHERE TestID = 431
		  GROUP BY StateId
	END
	
	
	
	-- Get all state changes
	; WITH StateStarterValues 
	AS (
		SELECT *,
			seq = ROW_NUMBER() OVER (PARTITION BY TestId,StateManagementTypeId,StateId ORDER BY [TimeStamp])
		FROM StateManagement 
		WHERE TestId = @testId
	)

	-- Select the first entry point of each recorded state to be removed from the total count (no point showing state progression in graphs that have pre existing values)
	INSERT INTO @aggregates
		SELECT 		
			StateManagementTypeId, 
			Q.StateId,
			SS.Minimun - Q.[Count] AS Minimum, 
			SS.Maximum - Q.[Count] AS Maximum,
			Q.[Count] AS [Statring Amount]
		FROM (
			SELECT 
				StateManagementTypeId, 
				StateId, 
				[Count]
			FROM StateStarterValues
			WHERE seq = 1
				AND StateId IN (
					SELECT StateId FROM @StateStatistics
					WHERE Diff > 0
				)
			) AS Q
		INNER JOIN @StateStatistics SS ON Q.StateId = SS.StateId
		ORDER BY Q.StateId
		-- With state 4 if negative on min and 0 on max then reverse
		-- With state 6 if negative then start at 0	
	RETURN 
END

GO


