SELECT DP1.name AS DatabaseRoleName,   
   isnull (DP2.name, 'No members') AS DatabaseUserName   
 FROM sys.database_role_members AS DRM  
 RIGHT OUTER JOIN sys.database_principals AS DP1  
   ON DRM.role_principal_id = DP1.principal_id  
 LEFT OUTER JOIN sys.database_principals AS DP2  
   ON DRM.member_principal_id = DP2.principal_id  
WHERE DP1.type = 'R'
ORDER BY DP1.name;  



use Ocr_ItemBank

if not exists (select 1 from sys.database_principals where name = 'ETLRole')
	CREATE ROLE [ETLRole];
GO

if exists (

select dp2.name
from sys.database_role_members AS DRM
 LEFT OUTER JOIN sys.database_principals AS DP2  
   ON DRM.member_principal_id = DP2.principal_id  
where name = 'OCR_ETLUser'
)
	DROP USER OCR_ETLUser;
	CREATE USER OCR_ETLUser FOR LOGIN OCR_ETLUser
	exec sp_addrolemember 'ETLRole','OCR_ETLUser'

if not exists (

select dp2.name
from sys.database_role_members AS DRM
 LEFT OUTER JOIN sys.database_principals AS DP2  
   ON DRM.member_principal_id = DP2.principal_id  
where name = 'OCR_ETLUser'
)
	CREATE USER [OCR_ETLUser] FOR LOGIN OCR_ETLUser
	exec sp_addrolemember 'ETLRole','OCR_ETLUser'
GO



