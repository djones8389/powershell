SELECT  S.Title [Subject]
       , i.id [ItemID]
       , i.Name [ItemName]
       , I.Version [ContentAuthor Version]
       , IT.Version [TestCreation Version]
	   , 'https://'+substring(DB_NAME(),0,charindex('_',DB_NAME()))+'.btlsurpass.com/#ItemAuthoring/Subject/'+cast(S.id as varchar(10))+'/Item/Edit/'+cast(i.id  as varchar(10))+ ''
FROM [Demo_ContentAuthor].[dbo].[Subjects] S
INNER JOIN [Demo_ContentAuthor].[dbo].[Items]  I
ON I.SubjectId = S.Id
INNER JOIN (
       SELECT ProjectID
              , ItemRef
              , MAX(Version) [Version]
       FROM Demo_ItemBank.[dbo].[ItemTable] 
       group by ProjectID
              , ItemRef
       ) IT
on IT.ProjectID = S.ProjectId
       and IT.ItemRef = I.id
where I.[Status] = 3               /*  Live Items  */                       
	--and i.id = 18205
      and I.Version > IT.Version;

