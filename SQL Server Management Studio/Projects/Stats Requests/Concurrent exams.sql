USE [SHA_BIS_SecureAssess_11.0]


select 
	ID
	--, examstatechangeauditxml
	, a.b.value('data(newStateID)[1]','int') [State]
	, a.b.value('data(changeDate)[1]','datetime') [Date]
INTO #TEST
from WAREHOUSE_ExamSessionTable NOLOCK
cross apply examstatechangeauditxml.nodes('exam/stateChange') a(b)
where examstatechangeauditxml.exist('exam/stateChange/newStateID[text() = (6,9)]') = 1	
	and a.b.value('data(newStateID)[1]','int') in (6,9)
	--and id = 193


--SELECT * FROM #TEST where State = 6

--SELECT * FROM #TEST where State = 9

SELECT A.ID
	, A.State6Date
	, b.State9Date
	, DATEDIFF(SECOND,A.State6Date,b.State9Date)/60
FROM (
SELECT ID [ID]
	, State [State]
	, Date [State6Date]
FROM #TEST 
where state = 6
) A
INNER JOIN (
SELECT ID [ID]
	, State [State]
	, Date [State9Date]
FROM #Test
where state = 9 
) B
on A.ID = b.id
	and a.State != b.state
	order by 4 desc


SELECT ID, ExamStateChangeAuditXml
FROM WAREHOUSE_ExamSessionTable
where id = 7992







select 
	ID
	, examstatechangeauditxml.value('data(exam/stateChange[newStateID=6]/changeDate)[1]','datetime') state6
	, examstatechangeauditxml.value('data(exam/stateChange[newStateID=9]/changeDate)[1]','datetime') state9
INTO #TEST
from WAREHOUSE_ExamSessionTable NOLOCK
where examstatechangeauditxml.exist('exam/stateChange/newStateID[text() = (6,9)]') = 1	
	--AND id = 7992




SELECT TOP(1) Concurrent = 
                (
                                SELECT COUNT(i.ID)
                                FROM #TEST i
                                WHERE o.state6 <= i.state9
                                AND o.state9 >= i.state6
                )
FROM #TEST o
ORDER BY Concurrent DESC



