SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

IF OBJECT_ID('tempdb..#TEST') IS NOT NULL DROP TABLE #TEST;

SELECT Exams.ID
	, State6
	, State9
INTO #TEST
FROM (

select 
	ID
from WAREHOUSE_ExamSessionTable  NOLOCK
where examstatechangeauditxml.exist('exam/stateChange/newStateID[text() = (6)]') = 1
	AND examstatechangeauditxml.exist('exam/stateChange/newStateID[text() = (9)]') = 1
) Exams
	
LEFT JOIN (
	
	SELECT ID
			, examstatechangeauditxml.value('data(exam/stateChange[newStateID=6]/changeDate)[1]','datetime') [State6]
	FROM WAREHOUSE_ExamSessionTable

) [Started]
ON [Started].ID = Exams.ID

LEFT JOIN (

	SELECT ID
			, examstatechangeauditxml.value('data(exam/stateChange[newStateID=9]/changeDate)[1]','datetime') [State9]
	FROM WAREHOUSE_ExamSessionTable

) [Finished]
ON [Finished].ID = Exams.ID

OPTION (MAXDOP 2)

--05:54

CREATE CLUSTERED INDEX [Test_Index] ON #TEST (

	ID
)

SELECT TOP(1) Concurrent = 
                (
                                SELECT COUNT(i.ID)
                                FROM #TEST i
                                WHERE o.state6 <= i.state9
                                AND o.state9 >= i.state6
                )
FROM #TEST o
ORDER BY Concurrent DESC

OPTION (MAXDOP 2)


