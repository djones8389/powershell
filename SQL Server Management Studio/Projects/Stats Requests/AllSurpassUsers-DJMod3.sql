IF OBJECT_ID('tempdb..##Users') IS NOT NULL DROP TABLE ##Users;

CREATE TABLE ##Users (
 server_name SYSNAME,
 database_Name SYSNAME,
 user_forename NVARCHAR(100),
 user_surname NVARCHAR(100),
 user_username NVARCHAR(100),
 user_disabled BIT,
 user_email NVARCHAR(100),
 AccountExpiryDate datetime,
 isCandidate nvarchar(5)
);

EXECUTE sp_MSforeachdb N'
USE [?];

IF (db_ID() >4)
BEGIN
 DECLARE @tSQL NVARCHAR(MAX);  SELECT @tSQL = N''
 SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; SELECT @@SERVERNAME
	, N''''?'''', ''+  CASE WHEN EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME IN (N''UserTable'', N''Users'') AND COLUMN_NAME = N''Forename'') THEN N''Forename'' ELSE N''FirstName'' END+N''
  , ''  +CASE WHEN EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME IN (N''UserTable'', N''Users'') AND COLUMN_NAME = N''Surname'') THEN N''Surname'' ELSE N''LastName''
  END+N'' , ''  +CASE WHEN EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME IN (N''UserTable'', N''Users'') AND COLUMN_NAME = N''Username'') THEN N''Username'' ELSE N''Username''
  END+N'' , ''  +CASE WHEN EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = N''UserTable'' AND COLUMN_NAME = N''UserDisabled'') THEN N''UserDisabled'' ELSE N''Retired'' END +N''
  , email ''  +CASE WHEN EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME in(N''UserTable'') AND COLUMN_NAME in(N''AccountExpiryDate'')) THEN N'',AccountExpiryDate'' ELSE N'''' END+N''  '' +CASE WHEN EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME in(N''Users'') AND COLUMN_NAME in(N''ExpiryDate'')) THEN N'',ExpiryDate'' ELSE N'''' END+N'' ''
    +CASE   WHEN NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME in (N''UserTable'', N''Users'')  AND COLUMN_NAME in (N''ExpiryDate'', N''AccountExpiryDate'')) THEN N'',''''2000-01-01 00:00:00'''''' ELSE N''''
  END+N'' '' +CASE  WHEN EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME in(N''UserTable'') AND COLUMN_NAME in(N''IsCandidate'')) THEN N'',IsCandidate'' ELSE N'''' END+N'' ''  +CASE WHEN NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = N''UserTable''  AND COLUMN_NAME = ''IsCandidate'') THEN N'',''''N/A'''''' ELSE N''''
  END+N'' FROM dbo.'' +CASE WHEN EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = N''UserTable'') THEN N''UserTable'' ELSE N''Users'' END   +N'''';  INSERT ##Users EXEC(@tSQL); 
END
';


SELECT name 
FROM SYS.databases
 where database_id > 4
 and name not in (
	SELECT database_Name 
	FROM ##Users
	)
order by 1

--DROP TABLE ##Users;