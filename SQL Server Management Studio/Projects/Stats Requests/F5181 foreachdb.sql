USE MASTER

exec sp_MSforeachdb '

USE [?];

SET QUOTED_IDENTIFIER ON;

if (''?'' like ''%SecureAssess'')

SELECT DB_NAME() ClientName 
	 ,A.Month
	, a.OfflineExams
	, b.OnlineExams
FROM (
select 
	 MONTH(WarehouseTime) [Month]
	 , COUNT(ID) [OfflineExams]
from WAREHOUSE_ExamSessionTable 
where examstatechangeauditxml.exist(''exam/stateChange/newStateID[text() = (6)]'') = 1	
	AND examstatechangeauditxml.exist(''exam/stateChange/newStateID[text() = (8)]'') = 1	
	group by MONTH(WarehouseTime)
	--ORDER BY MONTH(WarehouseTime)
) A
INNER JOIN (
	select 
		 MONTH(WarehouseTime) [Month]
		 , COUNT(ID) [OnlineExams]
	from WAREHOUSE_ExamSessionTable 
	where examstatechangeauditxml.exist(''exam/stateChange/newStateID[text() = (6)]'') = 1	
		AND examstatechangeauditxml.exist(''exam/stateChange/newStateID[text() = (9)]'') = 1	
		group by MONTH(WarehouseTime)
) B

ON A.Month = B.Month
	ORDER BY A.Month

OPTION (MAXDOP 0);
'