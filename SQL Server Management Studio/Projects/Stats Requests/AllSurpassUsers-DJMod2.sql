IF OBJECT_ID('tempdb..##Users') IS NOT NULL DROP TABLE ##Users;

CREATE TABLE ##Users (
 server_name SYSNAME,
 database_Name SYSNAME,
 user_forename NVARCHAR(100),
 user_surname NVARCHAR(100),
 user_username NVARCHAR(100),
 user_disabled BIT,
 user_email NVARCHAR(100),
 AccountExpiryDate datetime
);

EXECUTE sp_MSforeachdb
N'
USE [?];

IF (SELECT SUM(1) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME IN (N''UserTable'', N''Users'') AND COLUMN_NAME IN (N''Forename'', N''FirstName'', N''Surname'', N''LastName'', N''Email'',N''Retired'', N''IsDisabled'',N''Disabled'',N''UserDisabled'',N''Username'')) > 3
BEGIN
 DECLARE @tSQL NVARCHAR(MAX);
 SELECT @tSQL = N''

SELECT @@SERVERNAME
	, N''''?''''
	, ''+
  CASE 
   WHEN EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME IN (N''UserTable'', N''Users'') AND COLUMN_NAME = N''Forename'') THEN N''Forename''
   ELSE N''FirstName''
  END+N''
  , ''
  +CASE 
   WHEN EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME IN (N''UserTable'', N''Users'') AND COLUMN_NAME = N''Surname'') THEN N''Surname''
   ELSE N''LastName''
  END+N''
    , ''
  +CASE 
   WHEN EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME IN (N''UserTable'', N''Users'') AND COLUMN_NAME = N''Username'') THEN N''Username''
   ELSE N''Username''
  END+N''
  , ''
  +CASE 
   WHEN EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = N''UserTable'' AND COLUMN_NAME = N''UserDisabled'') THEN N''UserDisabled''
   ELSE N''Retired''
  END +N''
  , email
    ''
  +CASE 
   WHEN EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME in(N''UserTable'') AND COLUMN_NAME in(N''AccountExpiryDate'')) 
	THEN N'',AccountExpiryDate''
   ELSE N''''
  END+N''
      ''
  +CASE 
   WHEN EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME in(N''Users'') AND COLUMN_NAME in(N''ExpiryDate'')) 
	THEN N'',ExpiryDate''
      ELSE N''''
  END+N''
	 ''
    +CASE 
   WHEN NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME in (N''UserTable'', N''Users'')  AND COLUMN_NAME in (N''ExpiryDate'', N''AccountExpiryDate'')) 
	THEN N'',''''2000-01-01 00:00:00''''''
      ELSE N''''
  END+N''
   FROM dbo.''
  +CASE
   WHEN EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = N''UserTable'') THEN N''UserTable''
   ELSE N''Users''
  END  
  +N'''';  
 PRINT(@tSQL);
 
END
';

SELECT * FROM ##Users;  --2000-01-01 00:00:00

--DROP TABLE ##Users;