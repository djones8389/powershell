SELECT 
		@@SERVERNAME ServerName
		, db_Name() DBName
		,DistinctCandidates.forename
		,DistinctCandidates.surname
		,DistinctCandidates.candidateRef
		,DistinctCandidates.centreName
		,DistinctCandidates.qualificationName
	FROM (

	SELECT ROW_NUMBER() OVER (
				PARTITION BY forename
				,surname
				,candidateref
				,dob
					 order by (SELECT 1)
				) R
				, forename
				, surname
				, candidateref
				, centreName
				, qualificationName
				, examName
	FROM WAREHOUSE_UserTable
	INNER JOIN (
			select 
				ID
				, WAREHOUSE_ExamSessionTable.WAREHOUSEUserID
				, centreName
				, qualificationName
				, examName
			from WAREHOUSE_ExamSessionTable 
			INNER JOIN WAREHOUSE_ExamSessionTable_Shreded
			ON WAREHOUSE_ExamSessionTable_Shreded.examSessionID = WAREHOUSE_ExamSessionTable.ID
			where ExamStateChangeAuditXml.exist('exam/stateChange[newStateID=6]') = 1
		) SatExams
		on SatExams.WAREHOUSEUserID = WAREHOUSE_UserTable.ID
	) DistinctCandidates
WHERE R = 1
--group by Forename, Surname, CandidateRef, centreName, qualificationName

--EXCEPT

--SELECT distinct
--	forename
--	, surname
--	, candidateref
--FROM WAREHOUSE_ExamSessionTable_Shreded
--INNER JOIN WAREHOUSE_ExamSessionTable
--ON WAREHOUSE_ExamSessionTable_Shreded.examSessionID = WAREHOUSE_ExamSessionTable.ID
--where ExamStateChangeAuditXml.exist('exam/stateChange[newStateID=6]') = 1




SELECT 	forename
	, surname
	, candidateref
	, WAREHOUSE_ExamSessionTable_Shreded.KeyCode
	, WAREHOUSE_ExamSessionTable.WAREHOUSEUserID
FROM WAREHOUSE_ExamSessionTable_Shreded
INNER JOIN WAREHOUSE_ExamSessionTable
ON WAREHOUSE_ExamSessionTable_Shreded.examSessionID = WAREHOUSE_ExamSessionTable.ID
where ExamStateChangeAuditXml.exist('exam/stateChange[newStateID=6]') = 1
 AND candidateRef = 'OVL8252'


 /*
 forename	surname						candidateref	dob
WELIGODAGE  THILINI	MADUSHANI DE SILVA	OVL8252			1996-09-20 00:00:00
 */