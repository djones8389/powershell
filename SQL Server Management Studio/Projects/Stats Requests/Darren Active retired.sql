SELECT Active.Database_name [Client Name]
	, Active.Counter [Active]
	, ISNULL(Retired.Counter, 0) [Retired]
FROM (
SELECT Database_name [Database_name]
	, count(*) [Counter]	
FROM [Darren.UserLogins] WITH (NOLOCK)
where AccountExpiryDate > GETDATE()
	and user_Disabled = 0
	and iscandidate = 0
	group by Database_name
	
) Active

LEFT JOIN (

SELECT Database_name [Database_name]
	, count(*) [Counter]	
FROM [Darren.UserLogins] WITH (NOLOCK)
where AccountExpiryDate < GETDATE()
	and user_Disabled = 1
	and iscandidate = 0
	group by Database_name

) Retired

ON Active.Database_name = Retired.Database_name
where Active.Database_name not in ('SQA_SecueAssessOpenAssess_Backup_01102013', 'OCR_OpenAssessAdmin_10.0', 'OCR_OpenAssessAdmin_10.0_STANDARD', 'SQA_SecueAssessOpenAssess_Backup_01102013')
ORDER BY [Client Name];





--UPDATE [Darren.UserLogins]
--set AccountExpiryDate = '2050-01-01 00:00:00.000'
--where AccountExpiryDate = '2000-01-01 00:00:00.000'

--UPDATE [Darren.UserLogins]
--set iscandidate = 0
--where iscandidate = 'N/A'
