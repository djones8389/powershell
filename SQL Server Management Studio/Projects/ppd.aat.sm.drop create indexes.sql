USE [PPD_AAT_SecureMarker]


DROP INDEX [GDSCR_GroupDefinitionID] ON [dbo].[GroupDefinitionStructureCrossRef]

DROP INDEX [IX_GroupDefinitionStructureCrossRef_Status] ON [dbo].[GroupDefinitionStructureCrossRef]


CREATE NONCLUSTERED INDEX [IX_GroupDefinitionStructureCrossRef_Status] ON [dbo].[GroupDefinitionStructureCrossRef]
(
	[Status] ASC
)
INCLUDE ( 	[GroupDefinitionID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

/****** Object:  Index [GDSCR_GroupDefinitionID]    Script Date: 09/06/2016 09:44:47 ******/
CREATE NONCLUSTERED INDEX [GDSCR_GroupDefinitionID] ON [dbo].[GroupDefinitionStructureCrossRef]
(
	[GroupDefinitionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO









DROP  INDEX [IX_ExamVersionStructureID_Status] ON [dbo].[GroupDefinitionStructureCrossRef]
(
	[ExamVersionStructureID]
	,[Status]
)
INCLUDE ( 	[GroupDefinitionID])
GO
