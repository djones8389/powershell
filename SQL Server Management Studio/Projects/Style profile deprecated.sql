--use Demo_ItemBank

--select *
--from AssessmentTable AT
--Where at.externalreference = 'Legacy Deprecated - test reference'


	
use master

IF OBJECT_ID('tempdb..##DATALENGTH') IS NOT NULL DROP TABLE ##DATALENGTH;

CREATE TABLE ##DATALENGTH (
	client nvarchar(1000)
	, QualificationName nvarchar(300)
	, QualificationStatus nvarchar(300)
	, Assessment nvarchar(300)
	, AssessmentStatus nvarchar(300)
);

DECLARE @dynamic nvarchar(MAX)='';

select @dynamic +=CHAR(13)+ '
use '+QUOTENAME([NAME])+'

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

INSERT ##DATALENGTH
select 
	db_NAME()
	,QT.[QualificationName]
	, QL.Name [QualificationStatus]
	, AGT.name [Assessment]
	, AST.Name [Assessment Status]
	
from AssessmentGroupTable AGT 
inner join QualificationTable QT
on qt.ID = agt.QualificationID
inner join [dbo].[QualificationStatusLookupTable] QL
on QL.ID = QT.QualificationStatus
inner join AssessmentStatusLookupTable AST
on AST.ID = agt.Status
where StyleProfileVersion is null
'
from sys.databases
where state_desc = 'ONLINE'
	and name like '%[_]ItemBank';

exec(@dynamic);



select substring(client, 0, charindex('_',client)) Client
	, QualificationName
	, QualificationStatus
	, Assessment
	, AssessmentStatus
from ##DATALENGTH
order by 1

select distinct client
from ##DATALENGTH






