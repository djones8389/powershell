	SELECT 
		ESID
		, ItemID
		, MAX(RowNumber) Max_RowNumber
	FROM (
		SELECT ESIRT.ExamSessionID [ESID] 
			, ItemID
			, ROW_NUMBER() over (partition by ESIRT.ExamSessionID, ESIRT.itemID order by (SELECT 1)) RowNumber
		FROM  ExamSessionItemResponseTable ESIRT

		CROSS APPLY ESIRT.ItemResponseData.nodes('p/s/c/i/t/r[@h="22"]') a(b)

		where ItemResponseData.exist('p/s/c[@typ=20]') = 1
			and  ExamSessionID =  1368
	) f
	group by ESID
		, ItemID




SELECT 
	itemID
	--, ItemResponseData.query('//text')
	, ItemResponseData
	--, a.b.query('.')
FROM  ExamSessionItemResponseTable ESIRT
--CROSS APPLY ESIRT.ItemResponseData.nodes('p/s/c/i/t/r[@h="22"]') a(b)
where ExamSessionID =  1368
	and ItemResponseData.exist('p/s/c[@typ=20]') = 1




SELECT ESIRT.ExamSessionID [ESID] 
	, ItemID
	, ROW_NUMBER() over (partition by ESIRT.ExamSessionID, ESIRT.itemID order by (SELECT 1)) RowNumber
FROM  ExamSessionItemResponseTable ESIRT

CROSS APPLY ESIRT.ItemResponseData.nodes('p/s/c/i/t/r[@h="22"]') a(b)

where ItemResponseData.exist('p/s/c[@typ=20]') = 1
	and  ExamSessionID =  1368








SELECT E.*
FROM (

SELECT ESIRT.ExamSessionID
	, ItemID
	, c.d.query('.') [Full]
	, ROW_NUMBER() over (partition by ESIRT.ExamSessionID, ESIRT.itemID order by (SELECT 1)) RowNumber
FROM  ExamSessionItemResponseTable ESIRT

CROSS APPLY ESIRT.ItemResponseData.nodes('p/s/c/i/t/r[@h="22"]') a(b)
CROSS APPLY a.b.nodes('c') c(d)

where ExamSessionID =  1368
	and ItemResponseData.exist('p/s/c[@typ=20]') = 1
	and itemid = '392P3367'
) E
where [Full].exist('//text') = 1