use master

CREATE DATABASE DE31116
GO

--use DE31116
--GO


SELECT DQX.*
INTO DE31116..[Query1]
FROM Ocr_SurpassDataWarehouse..FactExamSessions FES
INNER JOIN Ocr_SurpassDataWarehouse..FactQuestionResponses FQR
JOIN Ocr_SurpassDataWarehouse..DimQuestionXML DQX ON DQX.CPID = FQR.CPID AND DQX.CPVersion = FQR.CPVersion
on FES.ExamSessionKey = FQR.ExamSessionKey
where KeyCode = 'VG4FGD4J' 
       and FQR.cpid in ('4208P5359','4208P5366','4208P5383','4208P5384','4208P5386','4208P5387','4208P5388')


SELECT DC.*
INTO DE31116..[Query2]
FROM Ocr_SurpassDataWarehouse..FactExamSessions FES
INNER JOIN Ocr_SurpassDataWarehouse..FactQuestionResponses FQR
JOIN Ocr_SurpassDataWarehouse..DimComponents DC ON DC.CPID = FQR.CPID AND DC.CPVersion = FQR.CPVersion
on FES.ExamSessionKey = FQR.ExamSessionKey
where KeyCode = 'VG4FGD4J' 
       and FQR.cpid in ('4208P5359','4208P5366','4208P5383','4208P5384','4208P5386','4208P5387','4208P5388')


Use master;

BACKUP DATABASE [DE31116] TO DISK = N'F:\Backup\DE31116.2018.04.19.bak' 
WITH NAME = N'DE31116- Database Backup', COPY_ONLY, NOFORMAT, NOINIT, SKIP, COMPRESSION, NOREWIND, NOUNLOAD, STATS = 10;

DROP DATABASE [DE31116]