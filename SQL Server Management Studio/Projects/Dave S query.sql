--Use TagGroups

if exists(select 1 from sys.objects where name = 'vw_TagIndexDS' and type = 'V')
	DROP VIEW dbo.vw_TagIndexDS
GO

CREATE VIEW dbo.vw_TagIndexDS 
WITH SCHEMABINDING  
AS  
SELECT i.Id as ItemId, stg.TagTypeName TagGroup, stv.text as TagValue
FROM dbo.Items i
  INNER JOIN dbo.SubjectTagValueItems si ON si.Item_Id = i.id
  INNER JOIN dbo.SubjectTagValues stv ON si.SubjectTagValue_Id = stv.Id
  INNER JOIN dbo.SubjectTagTypes stg on stv.SubjectTagTypeId = stg.Id AND stg.SubjectId = i.SubjectId
GO  
--Create an index on the view.  

CREATE UNIQUE CLUSTERED INDEX IDX_V1   
    ON dbo.vw_TagIndexDS (ItemId, TagGroup, TagValue);  
GO




Declare @Start datetime = getDate() 

SET STATISTICS IO, TIME ON

	declare @TagTypeName1 NVARCHAR(100) = 'TAG_TYPE_10'
				  ,@TagTypeName2 NVARCHAR(100) = 'TAG_TYPE_1'
				  ,@TagTypeName3 NVARCHAR(100) = 'TAG_TYPE_2'
				  ,@StvText NVARCHAR(100) = 'One'
				  ,@count tinyint


	SELECT i.subjectId, i.Id
	FROM Items i
	INNER JOIN dbo.vw_TagIndexDS (NOEXPAND) ti1 ON ti1.ItemId = i.id
	INNER JOIN dbo.vw_TagIndexDS (NOEXPAND) ti2 ON ti2.ItemId = i.id
	INNER JOIN dbo.vw_TagIndexDS (NOEXPAND) ti3 ON ti3.ItemId = i.id

	WHERE
	   ti1.TagGroup = @TagTypeName1 AND ti1.TagValue = @StvText AND
	   ti2.TagGroup = @TagTypeName2 AND ti2.TagValue = @StvText AND
	   ti3.TagGroup = @TagTypeName3 AND ti3.TagValue = @StvText

Declare @End datetime = getDate() 

SELECT cast(DATEDIFF(MILLISECOND, @Start, @End) as varchar(10)) + ' Milliseconds runtime'