if OBJECT_ID('tempdb..#Capture1') is not null drop table #Capture1;
if OBJECT_ID('tempdb..#Capture2') is not null drop table #Capture2;

BEGIN
	SELECT ROW_NUMBER() OVER(ORDER BY object_name, RTRIM(counter_name)) AS R
		,*
	INTO #Capture1
	from sys.dm_os_performance_counters
	where (
			object_name = 'SQLServer:Buffer Manager'
			and 
			counter_name in ('Buffer cache hit ratio','Buffer cache hit ratio base','Checkpoint pages/sec','Database pages','Free pages','Lazy writes/sec','Page life expectancy','Page lookups/sec','Page reads/sec','Page writes/sec','Readahead pages/sec','Reserved pages','Stolen pages','Target pages','Total pages')
		   )
		   OR
		   (
		   object_name ='SQLServer:Memory Manager'
		   and
		   counter_name in ('Total Server Memory (KB)','Target Server Memory (KB)','SQL Cache Memory (KB)','Optimizer Memory (KB)','Memory Grants Outstanding','Memory Grants Pending')
		   )
		   or
		   (
		   object_name = 'SQLServer:Access Methods'
		   and
		   counter_name in ('Full Scans/sec','Page Splits/sec','Table Lock Escalations/sec', 'Extents Allocated/sec', 'Index Searches/sec', 'Mixed page allocations/sec')
		   )
		   or 
		   (
		   object_name = 'SQLServer:Locks'
		   and
		   counter_name in ('Lock Requests/sec','Number of Deadlocks/sec','Lock Waits/sec')
		   )
		   or 
		   (
		   object_name = 'SQLServer:SQL Statistics'
		   and
		   counter_name in ('SQL Compilations/sec','Batch Requests/sec')
		   )
	order by 1, 2
END
BEGIN
	WAITFOR DELAY '00:01';  

	SELECT ROW_NUMBER() OVER(ORDER BY object_name, RTRIM(counter_name)) AS R
		,*
	INTO #Capture2
	from sys.dm_os_performance_counters
	where (
			object_name = 'SQLServer:Buffer Manager'
			and 
			counter_name in ('Buffer cache hit ratio','Buffer cache hit ratio base','Checkpoint pages/sec','Database pages','Free pages','Lazy writes/sec','Page life expectancy','Page lookups/sec','Page reads/sec','Page writes/sec','Readahead pages/sec','Reserved pages','Stolen pages','Target pages','Total pages')
		   )
		   OR
		   (
		   object_name ='SQLServer:Memory Manager'
		   and
		   counter_name in ('Total Server Memory (KB)','Target Server Memory (KB)','SQL Cache Memory (KB)','Optimizer Memory (KB)','Memory Grants Outstanding','Memory Grants Pending')
		   )
		   or
		   (
		   object_name = 'SQLServer:Access Methods'
		   and
		   counter_name in ('Full Scans/sec','Page Splits/sec','Table Lock Escalations/sec', 'Extents Allocated/sec', 'Index Searches/sec', 'Mixed page allocations/sec')
		   )
		   or 
		   (
		   object_name = 'SQLServer:Locks'
		   and
		   counter_name in ('Lock Requests/sec','Number of Deadlocks/sec','Lock Waits/sec')
		   )
		   or 
		   (
		   object_name = 'SQLServer:SQL Statistics'
		   and
		   counter_name in ('SQL Compilations/sec','Batch Requests/sec')
		   )
	order by 1, 2

END



select a.R
	, a.object_name
	, a.counter_name
	, a.instance_name
	, a.cntr_type
	, ABS(a.cntr_value - b.cntr_value) [Difference After 1 minute]
from #Capture1 A
INNER JOIN #Capture2 B
ON A.R = B.R



--272696576 Cumulative
--65792     Last observed value directly

/*

SELECT object_name
	, RTRIM(counter_name)
	, instance_name
	, cntr_value
	, cntr_type
from sys.dm_os_performance_counters                                                                                                       '

*/

