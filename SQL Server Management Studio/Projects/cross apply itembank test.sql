declare @xml xml = '<PaperRules MinEasyItems="0" MaxEasyItems="0" MinHardItems="0" MaxHardItems="0" ApplyRanges="0" EasyFV="0.7" HardFV="0.3" MaxEasyFV="0.9" MinHardFV="0.1" MinAverageFV="0.4" MaxAverageFV="0.6" ActionScriptMode="0">
  <Intro ID="0" Name="Introduction" Description="This is section description" Duration="" SectionType="Section" TotalQuestions="0" Randomise="0" PassLevelValue="0" PassLevelType="0" Fixed="1" OverrideLockdown="0" ItemsToMark="0" EnableSectionBreak="0" HasSectionBreak="0" BreakDuration="0" CancellableBreak="0" />
  <Outro ID="0" Name="Finish" Description="This is section description" Duration="" SectionType="Section" TotalQuestions="0" Randomise="0" PassLevelValue="0" PassLevelType="0" Fixed="1" OverrideLockdown="0" ItemsToMark="0" EnableSectionBreak="0" HasSectionBreak="0" BreakDuration="0" CancellableBreak="0" />
  <Tools ID="0" Name="Tools" Description="This is section description" Duration="" SectionType="Section" TotalQuestions="0" Randomise="0" PassLevelValue="0" PassLevelType="0" Fixed="1" OverrideLockdown="0" ItemsToMark="0" EnableSectionBreak="0" HasSectionBreak="0" BreakDuration="0" CancellableBreak="0" />
  <Section ID="1" Name="Section" Description="This is section description" Duration="10" SectionType="Section" TotalQuestions="3" Randomise="0" PassLevelValue="0" PassLevelType="1" Fixed="1" OverrideLockdown="0" ItemsToMark="0" EnableSectionBreak="0" HasSectionBreak="0" BreakDuration="0" CancellableBreak="1">
    <Rule MinQuestionNum="1" MaxQuestionNum="1" Name="Rule 1" Union="0">
      <Param Type="3">
        <XPath>@ID="4185P4192"</XPath>
        <NonScored>0</NonScored>
        <ProjectIDList>
          <ID>4185</ID>
        </ProjectIDList>
      </Param>
    </Rule>
    <Rule MinQuestionNum="1" MaxQuestionNum="1" Name="Rule 2" Union="0">
      <Param Type="3">
        <XPath>@ID="4185P4193"</XPath>
        <NonScored>0</NonScored>
        <ProjectIDList>
          <ID>4185</ID>
        </ProjectIDList>
      </Param>
    </Rule>
    <Rule MinQuestionNum="1" MaxQuestionNum="1" Name="Rule 3" Union="0">
      <Param Type="3">
        <XPath>@ID="4185P4194"</XPath>
        <NonScored>0</NonScored>
        <ProjectIDList>
          <ID>4185</ID>
        </ProjectIDList>
      </Param>
    </Rule>
  </Section>
  <Section ID="2" Name="Section Selector" Description="This is section selector description" Duration="106" RequiredSections="2" SectionType="Selector" TotalQuestions="0" Randomise="0" PassLevelValue="0" PassLevelType="0" Fixed="0" OverrideLockdown="1" ItemsToMark="0" EnableSectionBreak="0" HasSectionBreak="0" BreakDuration="0" CancellableBreak="0">
    <Section ID="3" Name="Section" Description="This is section description" Duration="20" SectionType="Section" TotalQuestions="3" Randomise="0" PassLevelValue="0" PassLevelType="1" Fixed="1" OverrideLockdown="0" ItemsToMark="0" EnableSectionBreak="0" HasSectionBreak="0" BreakDuration="0" CancellableBreak="1">
      <Rule MinQuestionNum="1" MaxQuestionNum="1" Name="Rule 1" Union="0">
        <Param Type="3">
          <XPath>@ID="4185P4195"</XPath>
          <NonScored>0</NonScored>
          <ProjectIDList>
            <ID>4185</ID>
          </ProjectIDList>
        </Param>
      </Rule>
      <Rule MinQuestionNum="1" MaxQuestionNum="1" Name="Rule 2" Union="0">
        <Param Type="3">
          <XPath>@ID="4185P4196"</XPath>
          <NonScored>0</NonScored>
          <ProjectIDList>
            <ID>4185</ID>
          </ProjectIDList>
        </Param>
      </Rule>
      <Rule MinQuestionNum="1" MaxQuestionNum="1" Name="Rule 3" Union="0">
        <Param Type="3">
          <XPath>@ID="4185P4197"</XPath>
          <NonScored>0</NonScored>
          <ProjectIDList>
            <ID>4185</ID>
          </ProjectIDList>
        </Param>
      </Rule>
    </Section>
    <Section ID="4" Name="Section" Description="This is section description" Duration="20" SectionType="Section" TotalQuestions="3" Randomise="0" PassLevelValue="0" PassLevelType="1" Fixed="1" OverrideLockdown="0" ItemsToMark="0" EnableSectionBreak="0" HasSectionBreak="0" BreakDuration="0" CancellableBreak="1">
      <Rule MinQuestionNum="1" MaxQuestionNum="1" Name="Rule 1" Union="0">
        <Param Type="3">
          <XPath>@ID="4185P4199"</XPath>
          <NonScored>0</NonScored>
          <ProjectIDList>
            <ID>4185</ID>
          </ProjectIDList>
        </Param>
      </Rule>
      <Rule MinQuestionNum="1" MaxQuestionNum="1" Name="Rule 2" Union="0">
        <Param Type="3">
          <XPath>@ID="4185P4200"</XPath>
          <NonScored>0</NonScored>
          <ProjectIDList>
            <ID>4185</ID>
          </ProjectIDList>
        </Param>
      </Rule>
      <Rule MinQuestionNum="1" MaxQuestionNum="1" Name="Rule 3" Union="0">
        <Param Type="3">
          <XPath>@ID="4185P4201"</XPath>
          <NonScored>0</NonScored>
          <ProjectIDList>
            <ID>4185</ID>
          </ProjectIDList>
        </Param>
      </Rule>
    </Section>
    <Section ID="5" Name="Section" Description="This is section description" Duration="20" SectionType="Section" TotalQuestions="3" Randomise="0" PassLevelValue="0" PassLevelType="1" Fixed="1" OverrideLockdown="0" ItemsToMark="0" EnableSectionBreak="0" HasSectionBreak="0" BreakDuration="0" CancellableBreak="1">
      <Rule MinQuestionNum="1" MaxQuestionNum="1" Name="Rule 1" Union="0">
        <Param Type="3">
          <XPath>@ID="4185P4202"</XPath>
          <NonScored>0</NonScored>
          <ProjectIDList>
            <ID>4185</ID>
          </ProjectIDList>
        </Param>
      </Rule>
      <Rule MinQuestionNum="1" MaxQuestionNum="1" Name="Rule 2" Union="0">
        <Param Type="3">
          <XPath>@ID="4185P4203"</XPath>
          <NonScored>0</NonScored>
          <ProjectIDList>
            <ID>4185</ID>
          </ProjectIDList>
        </Param>
      </Rule>
      <Rule MinQuestionNum="1" MaxQuestionNum="1" Name="Rule 3" Union="0">
        <Param Type="3">
          <XPath>@ID="4185P4204"</XPath>
          <NonScored>0</NonScored>
          <ProjectIDList>
            <ID>4185</ID>
          </ProjectIDList>
        </Param>
      </Rule>
    </Section>
  </Section>
</PaperRules>'

IF OBJECT_ID('tempdb..#SectionData') IS NOT NULL DROP TABLE #SectionData;

CREATE TABLE #SectionData (
	ID smallint
	,Duration smallint
	,RequiredSections smallint
	,SectionType varchar(100)
	,ParentId smallint
);

INSERT #SectionData	
SELECT	
	[top].[level].value('@ID[1]','int')  ID
	,[top].[level].value('data(@Duration)[1]', 'smallint') Duration
	,[top].[level].value('../@RequiredSections[1]','int') RequiredSections
	,[top].[level].value('data(@SectionType)[1]','varchar(100)') SectionType
	,[top].[level].value('../@ID', 'varchar(50)') ParentId
FROM (VALUES (@xml)) xx(x)
CROSS APPLY xx.x.nodes('PaperRules//Section') [top]([level]);

SELECT SUM(ISNULL(DurationSection,DurationSectionSelector))
FROM (
  select c.*
	, CASE WHEN DurationSectionSelector IS NULL 
		then 
			ROW_NUMBER() OVER (PARTITION BY Duration, ParentID, ID ORDER BY ID ASC) 
		else
			ROW_NUMBER() OVER (PARTITION BY Duration, ParentID ORDER BY ID ASC) 
		END as 'R'
  FROM (
	select 
		CASE WHEN (a.requiredSections is null and a.parentid is null and a.SectionType = 'Section')
			then a.Duration end as 'DurationSection'
		, a.Duration * a.RequiredSections 'DurationSectionSelector'
		, Duration
		, ParentId
		,ID
	FROM #SectionData a
	) C
) b
where R = 1;
