use master

--select database_id
--from sys.databases
--where name in ('PPD_AAT_ItemBank','PPD_AAT_SecureAssess')



EXECUTE sp_MSforeachdb N'
		SET QUOTED_IDENTIFIER ON;
		USE [?];
		IF (DB_ID() IN (18,20))
		BEGIN
		DECLARE IndexMaintenance CURSOR FOR
			SELECT	distinct ''ALTER INDEX '' + quotename(ix.name) +  '' ON '' + quotename(schema_name(schema_id)) + ''.'' + quotename(B.name) + '' REBUILD;''

				FROM	 sys.dm_db_index_physical_stats(DB_ID(''?''), NULL, NULL, NULL, NULL) ix_phy
				INNER JOIN sys.indexes ix
				ON ix_phy.object_id = ix.object_id
					AND ix_phy.index_id = ix.index_id
				INNER JOIN sys.objects B
				ON ix_phy.object_id = B.object_id
			WHERE ix.index_id > 0
				and ix.name != ''IX_WAREHOUSE_ExamSessionItemResponseTable_R11_3PT_script3''
		
		DECLARE @SQL NVARCHAR(1000);
		OPEN IndexMaintenance;
		FETCH NEXT FROM IndexMaintenance INTO @SQL;
		WHILE @@FETCH_STATUS = 0
		BEGIN
			PRINT(@SQL);
			FETCH NEXT FROM IndexMaintenance INTO @SQL;
		END
		CLOSE IndexMaintenance;
		DEALLOCATE IndexMaintenance;
		END';