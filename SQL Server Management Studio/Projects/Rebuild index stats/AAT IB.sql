

	select 	 (
					SELECT CONVERT(DECIMAL(18, 2), SUM(CAST(df.size AS FLOAT)) * 8 / 1024.0)
					FROM sys.database_files AS df
					WHERE df.type IN (0,2,4)
					) AS [DbSize]
				, CONVERT(DECIMAL(18, 2), SUM(a.total_pages) * 8 / 1024.0)  AS [SpaceUsed]
				,(
					SELECT CONVERT(DECIMAL(18, 2), SUM(CAST(df.size AS FLOAT)) * 8 / 1024.0)
					FROM sys.database_files AS df
					WHERE df.type IN (1,3)
					) AS [LogSize]
	FROM sys.partitions p
	INNER JOIN sys.allocation_units a ON p.partition_id = a.container_id;

/*
DbSize	SpaceUsed	LogSize
22874.00	22873.37	107.19

DbSize	SpaceUsed	LogSize
23074.00	22873.41	107.19


*/
--DBCC SHRINKFILE('ItemBank_R6.5.32.0',22874)

--SELECT CAST(((SUM(total_pages) * 8192) / 1048576.0) AS FLOAT) FROM sys.allocation_units