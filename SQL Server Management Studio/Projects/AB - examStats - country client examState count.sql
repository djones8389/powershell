use master

if OBJECT_ID('tempdb..#examstats') is not null drop table #examstats;

create table #examstats (

	ClientName sysname
	, ExamCount smallint
	, PreviousExamState tinyint
	, Country nvarchar(200)
)

INSERT #examstats
exec sp_MSforeachdb '

use [?];

if(''?'' like ''%SecureAssess%'')

BEGIN

	SELECT 
		db_name() [Client]
		, count(west.ID) [ExamCount]
		, PreviousExamState	
		, Country
	FROM WAREHOUSE_ExamSessionTable as WEST (NOLOCK)

	Inner Join WAREHOUSE_ScheduledExamsTable as WSCET (NOLOCK)
	on WSCET.ID = WEST.WAREHOUSEScheduledExamID  

	Inner Join WAREHOUSE_CentreTable as WCT (NOLOCK)
	on WCT.ID = WSCET.WAREHOUSECentreID

	where warehouseTime > ''2016-09-28 00:00:00''

	group by 
		  PreviousExamState	
			, Country
END

'


SELECT *
FROM #examstats
Order by 1;