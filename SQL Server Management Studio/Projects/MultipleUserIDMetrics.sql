select ServerName as DBName
	, convert(int,DBName) as 'Count'
	, Keycode as UserId
	, ExamState as 'Version'
from dbo.LimboExams 
where ServerName != 'PerformanceTest1_SecureAssess_2014-02-20_010543'
order by convert(int,DBName) desc;

select 
 ROW_NUMBER() OVER ( PARTITION BY ServerName, DBName, Keycode,ExamState  ORDER BY ServerName, DBName DESC, Keycode,ExamState)
	, ServerName as DBName 
	, DBName as 'Count'
	, Keycode as UserId
	, ExamState as 'Version'
from dbo.LimboExams 
order by DBName desc;