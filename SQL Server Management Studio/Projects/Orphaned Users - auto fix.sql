use master

if OBJECT_ID('tempdb..##test') IS NOT NULL DROP TABLE ##test;

create table ##test(
	dbName sysname null
	,userName nvarchar(120)	
	, sID varbinary(85)
)

exec sp_MSforeachdb '
use [?];

if(''?'' in (select name from sys.databases where database_id > 4 and state_desc=''ONLINE''))

declare @test table (
	dbName sysname null
	,userName nvarchar(120)	
	, sID varbinary(85)
)

insert @test(userName, sid)
exec sp_change_users_login ''Report'';

update @test
set dbName = db_NAME()
where dbName is null;

INSERT ##test
select *
from @test

'

select  'use ' + QUOTENAME(##test.dbname) + '; exec sp_change_users_login ''auto_fix'',''' + username + ''', NULL '
from ##test
inner join sys.syslogins
on sys.syslogins.name = ##test.userName
where userName != 'dbo'
order by 1