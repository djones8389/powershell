/*
CREATE TABLE deadlock_example_table_1 (column1 int)
CREATE TABLE deadlock_example_table_2 (column1 int)

INSERT INTO deadlock_example_table_1 (column1)
SELECT 1
UNION ALL
SELECT 2
UNION ALL
SELECT 3
GO

INSERT INTO deadlock_example_table_2 (column1)
SELECT 1
UNION ALL
SELECT 2
UNION ALL
SELECT 3
GO

Open two query windows in SQL Server Management Studio.
Execute this in Query Window 1:

BEGIN TRAN
DELETE FROM deadlock_example_table_1 WHERE column1 = 2

Execute this in Query Window 2:

BEGIN TRAN
DELETE FROM deadlock_example_table_2 WHERE column1 = 2
DELETE FROM deadlock_example_table_1 WHERE column1 = 2

Execute this in Query Window 1:

DELETE FROM deadlock_example_table_2 WHERE column1 = 2

*/
