SELECT cev.ExternalSessionID [examSessionID]
	  ,State3.[Timestamp]
	  ,State4.[Timestamp]      
	  ,DATEDIFF(SECOND, State3.[Timestamp],State4.[Timestamp]) [Diff-Seconds]
FROM [BCGuilds_SecureMarker].[dbo].[CandidateExamVersionStatuses] State3
INNER JOIN (
	SELECT [CandidateExamVersionID]
		  , cev.ExternalSessionID [examSessionID]
		  ,[Timestamp]      
	FROM [CandidateExamVersionStatuses] cevs
	inner join CandidateExamVersions cev on cev.id = cevs.CandidateExamVersionID
	where stateid = 4
	) State4
	on State3.CandidateExamVersionID = State4.CandidateExamVersionID
inner join CandidateExamVersions cev on cev.id = State3.CandidateExamVersionID
where stateid = 3

SELECT cevs.*
FROM [CandidateExamVersionStatuses] cevs
inner join CandidateExamVersions cev 
on cev.id = cevs.CandidateExamVersionID
WHERE ExternalSessionID  = 26287