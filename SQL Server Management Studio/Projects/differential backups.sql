select database_name
	, backup_start_date
	, backup_finish_date
	, type
	, is_copy_only
	, user_name
from msdb.dbo.backupset
where --database_name like 'ICAEW[_]%'
	 backup_start_date < DATEADD(DAY, -7, GETDATE())
	--and type = 'i'
	and user_name <> 'LON\977496-davej2'
	and user_name = 'NT AUTHORITY\SYSTEM'
order by backup_start_date desc;