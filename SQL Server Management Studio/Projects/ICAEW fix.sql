use ICAEW_SecureAssess

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

select west.id
	, wesirt.ItemID
	, ItemResponseData
	--, a.b.value('.','nvarchar(MAX)')	
into [AQT_Fix]
from WAREHOUSE_ExamSessionItemResponseTable wesirt

inner join WAREHOUSE_ExamSessionTable west
on west.id = wesirt.WAREHOUSEExamSessionID

CROSS APPLY itemresponsedata.nodes('p/s/c/i/t/r/c/text') a(b)
where LEFT(a.b.value('.', 'nvarchar(MAX)'), 7) = '%253CP%'



UPDATE WAREHOUSE_ExamSessionItemResponseTable
set itemresponsedata = replace(cast(a.ItemResponseData as nvarchar(MAX)),'%25','%')
from WAREHOUSE_ExamSessionItemResponseTable A
inner join [AQT_Fix] b
on a.WAREHOUSEExamSessionID = b.ID
	and b.ItemID = a.ItemID



	--and keycode in ('RX9MWNF6','YXFWMTF6','RVH6VHF6','HRQ4KWF6')
/*
--	and itemid = '368P981'
	

DECLARE @String varbinary(MAX) = '%253CP%2520ALIGN%253D%2522LEFT%2522%253E%253CFONT%2520FACE%253D%2522Arial%2522%2520SIZE%253D%252212%2522%2520COLOR%253D%2522%2523000000%2522%2520LETTERSPACING%253D%25220%2522%2520KERNING%253D%25220%2522%253E%253CB%253ECGT%2520@%252010%2525%253C/B%253E%253C/FONT%253E%253C/P%253E'	

SELECT CONVERT(nvarchar(MAX), @String)


use ICAEW_SecureAssess

SELECT	
	ItemResponseData.query('p/s/c/i/t/r/c/text')
from ExamSessionItemResponseTable
CROSS APPLY itemresponsedata.nodes('p/s/c/i/t/r/c/text') a(b)
where examsessionid = 5794
	and itemid = '368P936'
	and LEFT(a.b.value('.', 'nvarchar(MAX)'), 7) = '%253CP%'

	*/


Use master;
BACKUP DATABASE [ICAEW_SecureAssess] TO DISK = N'T:\Backup\ICAEW_SecureAssess.2017.06.08.bak' WITH NAME = N'ICAEW_SecureAssess- Database Backup', COPY_ONLY, NOFORMAT, NOINIT, SKIP, COMPRESSION, NOREWIND, NOUNLOAD, STATS = 10;
