--exec sm_MARKINGSERVICE_CheckUserIsOkayToBeMarkingThisGroup_sp @userId=258,@groupDefinitionId=2370,@trainingMode=0,@TokenStoreID=9875944,@isMarkOneItemPerScript=1  --aat


--set statistics io on
SELECT 'DBCC FREEPROCCACHE (',cp.plan_handle, ')'
FROM sys.dm_exec_cached_plans AS cp 
CROSS APPLY sys.dm_exec_sql_text(plan_handle) AS st
WHERE [text] LIKE '%sm_MARKINGSERVICE_CheckUserIsOkayToBeMarkingThisGroup_sp%'
	and dbid = db_id()

declare
	@userId				INT,
	@groupDefinitionId	INT,
	@trainingMode		BIT,
	@TokenStoreID		INT,
	@isMarkOneItemPerScript		BIT

set @userId=258
set @groupDefinitionId=2370
set @trainingMode=0 
set @TokenStoreID=9875944 
set @isMarkOneItemPerScript=1 


--select userid, GroupID
--from QuotaManagement
--where userid in (
--SELECT userid
--FROM AssignedUserRoles
--INNER JOIN RolePermissions
--ON AssignedUserRoles.RoleID = RolePermissions.RoleID
--WHERE  RolePermissions.PermissionID != 42
--)



BEGIN
	
	SET NOCOUNT ON;

	IF NOT EXISTS (SELECT TOP (1) [UserId] FROM QuotaManagement
						WHERE UserId = @userId
						AND GroupId = @groupDefinitionId)
		BEGIN
			SELECT 4
			RETURN		
		END	
				
	DECLARE @numResponsesLeftToMark	int
	
	DECLARE @IsUserHasUnrestrictedExaminerPermission BIT = 
		CASE WHEN 
			(SELECT COUNT(*)
				FROM AssignedUserRoles
				INNER JOIN RolePermissions
				ON AssignedUserRoles.RoleID = RolePermissions.RoleID
				WHERE AssignedUserRoles.UserID = @userId AND RolePermissions.PermissionID = 42) > 0 
			THEN 1 
			ELSE 0 
		END


	
	IF(@isMarkOneItemPerScript = 1)--AND @IsUserHasUnrestrictedExaminerPermission = 0)
		BEGIN	
			SELECT  @numResponsesLeftToMark = COUNT(UGR.[id])
				FROM    UniqueGroupResponses UGR
							INNER JOIN GroupDefinitions GD ON UGR.GroupDefinitionID = GD.ID
				WHERE   ( UGR.TokenId IS NULL
							  OR UGR.TokenId = @TokenStoreID) 
							-- unecessary check as we are passing in item id which determines this 
							AND UGR.IsEscalated = 0
							AND 
								(
									UGR.[confirmedmark] IS NULL
									OR
									@trainingMode = 1
								)
							AND UGR.CI = 0
							AND GD.ID = @groupDefinitionId
							AND UGR.ID NOT IN 
							(
								SELECT CGR1.UniqueGroupResponseID
								FROM CandidateGroupResponses CGR1
								INNER JOIN UniqueGroupResponses UGR2
								ON UGR2.ID = CGR1.UniqueGroupResponseID
								INNER JOIN
								(	-- all Scripts which contain current group and affected by current user 
									SELECT DISTINCT ACEV.CandidateExamVersionId AS AffectedScript
									FROM UniqueGroupResponses UGR1
									INNER JOIN CandidateGroupResponses CGR
									ON CGR.UniqueGroupResponseID = UGR1.ID
									INNER JOIN AffectedCandidateExamVersions ACEV
									ON ACEV.CandidateExamVersionId = CGR.CandidateExamVersionID
									WHERE UGR1.GroupDefinitionID = GD.ID
									AND ACEV.UserId = @userId
									AND ACEV.GroupDefinitionID IN 
									(
										SELECT GDSCR.GroupDefinitionID 
										FROM dbo.GroupDefinitionStructureCrossRef GDSCR
											INNER JOIN dbo.ExamVersionStructures EVS ON EVS.ID = GDSCR.ExamVersionStructureID
										WHERE GDSCR.Status = 0 -- 0 = Active
											AND EVS.StatusID = 0 -- 0 = Released
									)
								) AS AffectedScripts
								ON CGR1.CandidateExamVersionID = AffectedScripts.AffectedScript
								WHERE UGR2.GroupDefinitionID = GD.ID
							)
		END
	ELSE
		BEGIN
			SELECT  @numResponsesLeftToMark = COUNT(UGR.[id])
				FROM    UniqueGroupResponses UGR
							INNER JOIN GroupDefinitions GD ON UGR.GroupDefinitionID = GD.ID
				WHERE   ( UGR.TokenId IS NULL
							  OR UGR.TokenId = @TokenStoreID) 
							-- unecessary check as we are passing in item id which determines this 
							AND UGR.IsEscalated = 0
							AND 
								(
									UGR.[confirmedmark] IS NULL
									OR
									@trainingMode = 1
								)
							AND UGR.CI = 0
							AND GD.ID = @groupDefinitionId
		END
                            
	IF (@numResponsesLeftToMark < 1)
			BEGIN
			SELECT 5
			RETURN
		END		
				
	DECLARE @markingSuspended bit
	DECLARE @numMarked int
	DECLARE @assignedQuota	int

	SELECT 
		@markingSuspended=MarkingSuspended, 
		@numMarked=NumberMarked, 
		@assignedQuota=AssignedQuota
	FROM QuotaManagement
	WHERE 
		UserId = @userId
		AND 
		GroupId = @groupDefinitionId
		
	IF (@trainingMode = 1)
		BEGIN
			SET @markingSuspended = 0
		END

	IF (@numMarked >= @assignedQuota AND @assignedQuota != -1)
		BEGIN
			SELECT 3
			RETURN
		END
		
	IF (@markingSuspended = 1)
		BEGIN
			SELECT 1
			RETURN
		END
		
	DECLARE @groupReleased bit
			
	SELECT @groupReleased = ISNULL(IsReleased, 0)
	FROM GroupDefinitions
	WHERE ID = @groupDefinitionId

	IF (@groupReleased != 1)
		BEGIN
			SELECT 2
			RETURN
		END
	
	-- if we're here, we're good to mark!
	SELECT 0
 
END
