USE AAT_SecureAssess

if OBJECT_ID ('tempdb..#checker') is not null drop table #checker;

create table #checker (
	keycode nvarchar(10)
	, correctMark float
	, assignedMark float
);


bulk insert #checker
from 'C:\Users\977496-davej2\Desktop\AATMarkChecker.csv'
with (fieldterminator=',',rowterminator='\n')

--select count(*) FROM #checker

SELECT A.keycode
	, a.correctMark
	, a.assignedMark
	, wests.userMark
	, userPercentage
	, west.resultData.value('data(exam/@passMark)[1]','tinyint') [ExamPassMark]
	, west.resultData
	 ,WESTI.ItemRef
	 ,wesirt.ItemVersion
	 --,WESIRT.ItemResponseData
	, WESTI.TotalMark
	, (a.assignedMark * WESTI.TotalMark) [AwardedMarkForItem]	
	--, ROW_NUMBER() over (partition by west.Keycode order by wesirt.itemid) R
FROM #checker A
INNER JOIN WAREHOUSE_ExamSessionTable_Shreded wests WITH (NOLOCK)
on wests.keycode = a.keycode
inner join WAREHOUSE_ExamSessionTable west WITH (NOLOCK)
on west.id = wests.examSessionId
INNER JOIN WAREHOUSE_ExamSessionItemResponseTable WESIRT WITH (NOLOCK)
ON WESIRT.WAREHOUSEExamSessionID = WEST.ID	
	and assignedMark = WESIRT.ItemResponseData.value('data(p/@um)[1]','float')
INNER JOIN WAREHOUSE_ExamSessionTable_ShrededItems WESTI WITH (NOLOCK)
on WESTI.examSessionId = west.ID
	and WESTI.ItemRef = WESIRT.ItemID
	and wesirt.itemid in ('974P1252','974P1125','974P1180','974P1169','974P1132','974P1064')
where west.KeyCode not in ('3HGJY9A6','3JGQCRA6','4CTKNNA6','4NLHKYA6','4PMHYPA6','6LVVT3A6','6LYV9DA6','94R79FA6','9B9T3QA6','9MVCRFA6','CHXRT8A6','CVQMNGA6','FD77FDA6','FLW8HRA6','FX6HQ4A6','J89GHTA6','JBVQ9XA6','JQ4CRJA6','JTLWRPA6','KD3FM8A6','LYLHTGA6','PK8RXNA6','PR7LNBA6','PRQQP6A6','PT7476A6','Q6337GA6','Q6BK4TA6','QLMR3HA6','T3V4MYA6','VVDJGFA6','W46HCYA6','XKGLTCA6','XL3PRXA6','XXX96QA6','Y4J67BA6')

order by keycode, WESIRT.ItemID


/*

SELECT WESIRT.*
FROM WAREHOUSE_ExamSessionTable west WITH (NOLOCK)

INNER JOIN WAREHOUSE_ExamSessionItemResponseTable WESIRT WITH (NOLOCK)
ON WESIRT.WAREHOUSEExamSessionID = WEST.ID
INNER JOIN (

	SELECT [Project Name]
			, itemid
			, [ItemVersion]
			,[AQT]
	FROM [430327-AAT-SQL2\SQL2].[AAT_CPProjectAdmin].dbo.[ledgerincomplete] WITH (NOLOCK)
) CP
ON CP.ItemID = WESIRT.ItemID	
	and CP.ItemVersion = WESIRT.ItemVersion

where west.KeyCode = 'CX48HRA6'
	and wesirt.itemid in ('974P1252','974P1125','974P1180','974P1169','974P1132','974P1064')

	*/