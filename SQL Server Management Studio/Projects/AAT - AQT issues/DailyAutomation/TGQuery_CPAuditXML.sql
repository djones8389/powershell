SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

select cp.ItemID
	, cp.ItemVersion
	, replace(replace(convert(nvarchar(max), cp.ItemXML), CHAR(10), ''), CHAR(13), '') [ItemXML]
from (	
	select ItemID, ItemVersion
	from CPAuditTable
	where ItemXML.exist('P/S/C/I/extension/t/r/c[@dt]') = 1
) i
inner join CPAuditTable cp
on cp.ItemID = i.itemId 
	and cp.ItemVersion = i.itemVersion
INNER JOIN (
	select distinct ItemID, ItemVersion
	from WAREHOUSE_ExamSessionItemResponseTable weisrt
	INNER JOIN WAREHOUSE_ExamSessionTable west
	on weisrt.WAREHOUSEExamSessionID = west.ID
	where warehousetime > '2017-09-15'
) Sat
on Sat.ItemID = cp.ItemID
	and Sat.ItemVersion = cp.ItemVersion

	