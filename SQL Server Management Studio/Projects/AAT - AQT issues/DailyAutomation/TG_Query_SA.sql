    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	--select distinct b.ItemID, b.ItemVersion
	--from (

	select 
		weisrt.ID
		, weisrt.ItemID
		, weisrt.ItemVersion
		, KeyCode
		, ItemResponseData
		--,
		--CPAUdit.*
	from WAREHOUSE_ExamSessionItemResponseTable weisrt
	inner join (
			select cp.ItemID, cp.ItemVersion, replace(replace(convert(nvarchar(max), cp.ItemXML), CHAR(10), ''), CHAR(13), '') [ItemXML]
			from (	select ItemID, ItemVersion
					from CPAuditTable
					where ItemXML.exist('P/S/C/I/extension/t/r/c[@dt]') = 1
			) i
			inner join CPAuditTable cp
			on cp.ItemID = i.itemId 
				and cp.ItemVersion = i.itemVersion
	) CPAUdit
	on CPAUdit.ItemID = weisrt.ItemID
		and CPAUdit.ItemVersion = weisrt.ItemVersion
	INNER JOIN WAREHOUSE_ExamSessionTable west
	on weisrt.WAREHOUSEExamSessionID = west.ID
	where warehousetime > '2017-09-15'
--) b

	--where cp.ItemID in ('1005P1101','1005P1103')

	--('974P1252','974P1225','974P1180','974P1169','974P1132','974P1064')