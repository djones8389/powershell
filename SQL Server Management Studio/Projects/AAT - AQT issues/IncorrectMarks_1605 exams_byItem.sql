/*

USE AAT_SecureAssess

if OBJECT_ID ('tempdb..#checker') is not null drop table #checker;
IF OBJECT_ID('tempdb..#ESIDS') IS NOT NULL DROP TABLE #ESIDS;
IF OBJECT_ID('tempdb..#temp') IS NOT NULL DROP TABLE #temp;

create table #checker (
	[num] int
	, keycode nvarchar(10)
	, fullitemid nvarchar(50)
	, extension nvarchar(100)
	, correctMark float
	, assignedMark float
);


bulk insert #checker
from 'C:\Users\977496-davej2\Desktop\incorrectMarks_3.csv'
with (fieldterminator=',',rowterminator='\n')


CREATE TABLE #ESIDS (ID INT);

INSERT #ESIDS
SELECT WEST.ID
FROM #checker C
INNER JOIN Warehouse_ExamSessionTable WEST
on WEST.Keycode = C.Keycode

CREATE CLUSTERED INDEX [IX] ON #ESIDS(ID);

*/


SELECT west.ID [ESID] 
	,c.keycode
	, c.fullitemid
	, c.extension
	, c.correctmark
	, c.assignedmark
	, wei
	, um
	, WESTI.TotalMark [ItemTotalMark]
	, c.assignedmark * wei [ComponentMark_SetTo]
	, c.correctmark * wei [ComponentMark_ShouldBe]
INTO #temp
FROM #checker  C

INNER JOIN Warehouse_ExamSessionTable WEST WITH (NOLOCK)
on WEST.Keycode = C.Keycode

INNER JOIN (
	SELECT distinct
		 WAREHOUSEExamSessionID
		, ItemID
		, replace(ItemID + + 'S' + cast(a.b.value('../@id','int') as char(5)) + 'C' + cast(a.b.value('./@id','int') as char(5)), ' ','') as [fullitemid]
		--, ItemResponseData
		, a.b.value('@wei','float') [wei]
		, a.b.value('@um','float')  [um]
	FROM WAREHOUSE_ExamSessionItemResponseTable WITH (NOLOCK)
	
	INNER JOIN #ESIDS ESIDS
	ON ESIDS.ID = WAREHOUSE_ExamSessionItemResponseTable.WAREHOUSEExamSessionID

	CROSS APPLY ItemResponseData.nodes('p/s/c') a(b)

) WESIRT
on	WESIRT.WAREHOUSEExamSessionID = WEST.ID
	and wesirt.[fullitemid] =  substring(C.fullitemid, 0, CHARINDEX('I',C.fullitemid)) 

INNER JOIN WAREHOUSE_ExamSessionTable_ShrededItems WESTI WITH (NOLOCK)
on WESTI.examSessionId = west.ID
	and WESTI.ItemRef =  substring(C.fullitemid, 0, CHARINDEX('S',C.fullitemid)) 

--where C.keycode in (

--	select a.keycode
--	FROM (
--	SELECT *
--		, ROW_NUMBER() OVER(Partition by keycode order by num) R
--	FROM #checker
--	) a
--	where R > 1
--)
--and c.keycode = '6ZDUBJA6'


SELECT Results.*
FROM (
	SELECT 	
		 forename + ' ' + surName [Candidate Name]
		, candidateRef [Membership No.]
		, qualificationName
		, examName [AssessmentName]
		, centreName
		, WEST.KeyCode
		, ItemID
		, [ItemMark_SetTo]
		, [ItemMark_ShouldBe]
		, wests.userMark
		, (wests.userMark-[Aggregate].markstodeduct) + [Aggregate].markstoadd [userMark_shouldbe]
		, userPercentage
		, (((wests.userMark-[Aggregate].markstodeduct) + [Aggregate].markstoadd) / wests.resultData.value('data(exam/@totalMark)[1]','tinyint'))*100 [userPercent_shouldbe]
		, originalGrade
		, CASE
			WHEN (((wests.userMark-[Aggregate].markstodeduct) + [Aggregate].markstoadd) / wests.resultData.value('data(exam/@totalMark)[1]','tinyint'))*100 > wests.resultData.value('data(exam/@passMark)[1]','tinyint') THEN 'Pass'
				ELSE originalGrade
				END AS [Grade_ShouldBe]
		, wests.resultData.value('data(exam/@passMark)[1]','tinyint') [ExamPassMark]
		, wests.resultData.value('data(exam/@totalMark)[1]','tinyint') [ExamTotalMark]
		, clientInformation.value('data(clientInformation/systemConfiguration/flashPlayer/version)[1]','nvarchar(200)') [Flash]
		, downloadInformation.value('data(downloads/information/date)[1]','nvarchar(MAX)') [DownloadedToPC]
		, clientInformation.value('data(clientInformation/systemConfiguration/timeZone/timeZoneName)[1]','nvarchar(200)') [Timezone]
		, clientInformation.value('data(clientInformation/systemConfiguration/timeZone/daylightSavingTime)[1]','bit') [DaylightSavingTime]
		, [started] as [State 6]
	FROM WAREHOUSE_ExamSessionTable_Shreded WESTS WITH (NOLOCK)
	 
	INNER JOIN WAREHOUSE_ExamSessionTable WEST  WITH (NOLOCK)
	ON WEST.ID = WESTS.examSessionId

	INNER JOIN (

		SELECT A.ESID
			, Keycode
			, ItemID
			, (westi.userMark * westi.TotalMark) [ItemMark_SetTo]
			, ((cast((westi.userMark * westi.TotalMark) as float)-ComponentMark_SetTo) + (ComponentMark_ShouldBe)) [ItemMark_ShouldBe]
		FROM (
		SELECT
			ESID
			, Keycode
			, substring(fullitemid, 0, CHARINDEX('S',fullitemid))  ItemID
			, ItemTotalMark
			, sum(ComponentMark_ShouldBe) ComponentMark_ShouldBe
			, sum([ComponentMark_SetTo]) [ComponentMark_SetTo]
		from #temp t
		--where keycode = 'QR8UA8A6'
		group by
			ESID
			, Keycode
			 ,substring(fullitemid, 0, CHARINDEX('S',fullitemid)) 
			,ItemTotalMark
		) A
		inner join WAREHOUSE_ExamSessionTable_ShrededItems westi WITH (NOLOCK)
		on westi.examSessionId = a.ESID
			and westi.ItemRef = ItemID
		--group by A.ESID
	) Adjuster
	on Adjuster.ESID = wests.examSessionId


	INNER JOIN (

		SELECT 
			A.ESID
			, sum(cast((westi.userMark * westi.TotalMark) as float)) [markstodeduct]
			, sum(((cast((westi.userMark * westi.TotalMark) as float)-ComponentMark_SetTo) + (ComponentMark_ShouldBe))) [markstoadd] --/ItemTotalMark*10 [ItemMarkShouldBe]
		FROM (
		SELECT
			ESID
			, Keycode
			, substring(fullitemid, 0, CHARINDEX('S',fullitemid))  ItemID
			, ItemTotalMark
			, sum(ComponentMark_ShouldBe) ComponentMark_ShouldBe
			, sum([ComponentMark_SetTo]) [ComponentMark_SetTo]
		from #temp t
		--where keycode = 'QR8UA8A6'
		group by
			ESID
			, Keycode
			 ,substring(fullitemid, 0, CHARINDEX('S',fullitemid)) 
			,ItemTotalMark
		) A
		inner join WAREHOUSE_ExamSessionTable_ShrededItems westi WITH (NOLOCK)
		on westi.examSessionId = a.ESID
			and westi.ItemRef = ItemID
		group by A.ESID

	) [Aggregate]
	on [Aggregate].ESID = wests.examSessionId


) Results
WHERE originalGrade <> [Grade_ShouldBe]
order by KeyCode


