SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

select ItemResponseData, Keycode , warehouseTime
from WAREHOUSE_ExamSessionItemResponseTable r
inner join WAREHOUSE_ExamSessionTable s
on s.ID = r.WAREHOUSEExamSessionID
where ItemId in ('974P1252','974P1225','974P1180','974P1169','974P1132','974P1064')
	order by warehouseTime;