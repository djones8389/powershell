USE AAT_SecureAssess
/*
if OBJECT_ID ('tempdb..#checker') is not null drop table #checker;

create table #checker (
	[num] int
	, keycode nvarchar(10)
	, fullitemid nvarchar(50)
	, extension nvarchar(100)
	, correctMark float
	, assignedMark float
);


bulk insert #checker
from 'C:\Users\977496-davej2\Desktop\incorrectMarks_2.csv'
with (fieldterminator=',',rowterminator='\n')


use AAT_SecureAssess

IF OBJECT_ID('tempdb..#ESIDS') IS NOT NULL DROP TABLE #ESIDS;

CREATE TABLE #ESIDS (ID INT);

INSERT #ESIDS
SELECT WEST.ID
FROM #checker C
INNER JOIN Warehouse_ExamSessionTable WEST
on WEST.Keycode = C.Keycode

CREATE CLUSTERED INDEX [IX] ON #ESIDS(ID);
*/


SELECT west.ID [ESID] 
	,c.keycode
	, c.fullitemid
	, c.extension
	, c.correctmark
	, c.assignedmark
	, wei
	, um
	, WESTI.TotalMark [ItemTotalMark]
	, c.assignedmark * wei [ComponentMark_SetTo]
	, c.correctmark * wei [ComponentMark_ShouldBe]
into #temp
FROM #checker  C

INNER JOIN Warehouse_ExamSessionTable WEST WITH (NOLOCK)
on WEST.Keycode = C.Keycode

INNER JOIN (
	SELECT distinct
		 WAREHOUSEExamSessionID
		, ItemID
		, replace(ItemID + + 'S' + cast(a.b.value('../@id','int') as char(5)) + 'C' + cast(a.b.value('./@id','int') as char(5)), ' ','') as [fullitemid]
		--, ItemResponseData
		, a.b.value('@wei','float') [wei]
		, a.b.value('@um','float')  [um]
	FROM WAREHOUSE_ExamSessionItemResponseTable WITH (NOLOCK)
	
	INNER JOIN #ESIDS ESIDS
	ON ESIDS.ID = WAREHOUSE_ExamSessionItemResponseTable.WAREHOUSEExamSessionID

	CROSS APPLY ItemResponseData.nodes('p/s/c') a(b)

) WESIRT
on	WESIRT.WAREHOUSEExamSessionID = WEST.ID
	and wesirt.[fullitemid] =  substring(C.fullitemid, 0, CHARINDEX('I',C.fullitemid)) 

INNER JOIN WAREHOUSE_ExamSessionTable_ShrededItems WESTI WITH (NOLOCK)
on WESTI.examSessionId = west.ID
	and WESTI.ItemRef =  substring(C.fullitemid, 0, CHARINDEX('S',C.fullitemid)) 

where C.keycode in (

	select a.keycode
	FROM (
	SELECT *
		, ROW_NUMBER() OVER(Partition by keycode order by num) R
	FROM #checker
	) a
	where R > 1
)
and c.keycode = '6ZDUBJA6'


SELECT A.*
	, ItemRef
	, TotalMark
	, userMark
	, markerUserMark
	, ((markerUserMark-ComponentMark_SetTo) + (ComponentMark_ShouldBe)) [ItemMarkShouldBe] --/ItemTotalMark*10 [ItemMarkShouldBe]
FROM (
SELECT
	ESID
	, Keycode
	, substring(fullitemid, 0, CHARINDEX('S',fullitemid))  ItemID
	, ItemTotalMark
	, sum(ComponentMark_ShouldBe) ComponentMark_ShouldBe
	, sum([ComponentMark_SetTo]) [ComponentMark_SetTo]
from #temp t
where keycode = '6ZDUBJA6'
group by
	ESID
	, Keycode
	 ,substring(fullitemid, 0, CHARINDEX('S',fullitemid)) 
	,ItemTotalMark
) A
inner join WAREHOUSE_ExamSessionTable_ShrededItems westi
on westi.examSessionId = a.ESID
	and westi.ItemRef = ItemID




SELECT ItemRef, TotalMark, userMark, markerUserMark
from WAREHOUSE_ExamSessionTable_ShrededItems
where ItemRef  in (
'923P1285',
'923P1310'
)
and examSessionID = 1402691

select * from WAREHOUSE_ExamSessionItemResponseTable where itemid = '923P1285' and WAREHOUSEExamSessionID = 1402691
select resultdata from WAREHOUSE_ExamSessionTable where id = 1402691

/*
SELECT itemid,ItemResponseData, west.id
FROM  Warehouse_ExamSessionTable WEST WITH (NOLOCK)

INNER JOIN WAREHOUSE_ExamSessionItemResponseTable WESIRT  WITH (NOLOCK)
on WESIRT.WAREHOUSEExamSessionID = west.ID

where keycode = '6ZDUBJA6'
	and itemid in ('923P1285','923P1310')


SELECT ItemRef, TotalMark, *
from WAREHOUSE_ExamSessionTable_ShrededItems
where ItemRef  in (
'923P1285',
'923P1310'
)
and examSessionID = 1402691

*/

/*

Tricky one..

SELECT C.*
	, a.b.value('@wei','float') [Weight]
	, a.b.value('@um','float')  [UM]
	, WESIRT.id [WESIRT_ID]

FROM #checker C

INNER JOIN Warehouse_ExamSessionTable WEST WITH (NOLOCK)
on WEST.Keycode = C.Keycode

INNER JOIN WAREHOUSE_ExamSessionItemResponseTable WESIRT  WITH (NOLOCK)
on WESIRT.WAREHOUSEExamSessionID = west.ID
	and WESIRT.ItemID = c.itemid

CROSS APPLY ItemResponseData.nodes('p/s/c[@typ=20]') a(b)

where C.keycode in (

	select a.keycode
	FROM (
	SELECT *
		, ROW_NUMBER() OVER(Partition by keycode, itemid order by num) R
	FROM #checker
	) a
	where R > 1
)
and c.keycode = 'MG73BFA6'


SELECT *
FROM WAREHOUSE_ExamSessionItemResponseTable
WHERE ID = 20477796
	

SELECT *
FROM #checker
where keycode = 'MG73BFA6'


*/





/*


SELECT B.*
FROM (
SELECT A.Keycode
	, forename + ' ' + surName [Candidate Name]
	, candidateRef [Membership No.]
	, centreName
	, a.correctMark
	, a.assignedMark
	, wests.userMark
	,(wests.userMark-(a.assignedMark * WESTI.TotalMark))+(correctMark*WESTI.TotalMark) [userMark_shouldbe]
	, userPercentage
	, (((wests.userMark-(a.assignedMark * WESTI.TotalMark))+(correctMark*WESTI.TotalMark))/west.resultData.value('data(exam/@totalMark)[1]','tinyint'))*100 [userPercent_shouldbe]
	, cast(WESTI.TotalMark as float) [ItemTotalMark]
	, west.resultData.value('data(exam/@passMark)[1]','tinyint') [ExamPassMark]
	, west.resultData.value('data(exam/@totalMark)[1]','tinyint') [ExamTotalMark]
	, WESTI.ItemRef 
	, originalGrade
	--, west.resultData
	, case when
		((((wests.userMark-(a.assignedMark * WESTI.TotalMark))+(correctMark*WESTI.TotalMark))/west.resultData.value('data(exam/@totalMark)[1]','tinyint'))*100 < west.resultData.value('data(exam/@passMark)[1]','tinyint')) then 'Fail'
			ELSE 'Pass'
			END AS [result_shouldbe]
	, clientInformation.value('data(clientInformation/systemConfiguration/flashPlayer/version)[1]','nvarchar(200)') [Flash]
	, downloadInformation.value('data(downloads/information/date)[1]','nvarchar(MAX)') [DownloadedToPC]
	, clientInformation.value('data(clientInformation/systemConfiguration/timeZone/timeZoneName)[1]','nvarchar(200)') [Timezone]
	, clientInformation.value('data(clientInformation/systemConfiguration/timeZone/daylightSavingTime)[1]','bit') [DaylightSavingTime]
	, [started] as [State 6]
	, ROW_NUMBER() over (partition by west.Keycode order by wesirt.itemid) R
FROM #checker A
INNER JOIN WAREHOUSE_ExamSessionTable_Shreded wests WITH (NOLOCK)
on wests.keycode = a.keycode
inner join WAREHOUSE_ExamSessionTable west WITH (NOLOCK)
on west.id = wests.examSessionId
INNER JOIN WAREHOUSE_ExamSessionItemResponseTable WESIRT WITH (NOLOCK)
ON WESIRT.WAREHOUSEExamSessionID = WEST.ID	
	--and assignedMark = WESIRT.ItemResponseData.value('data(p/@um)[1]','float')
	and wesirt.itemid = a.itemid
INNER JOIN WAREHOUSE_ExamSessionTable_ShrededItems WESTI WITH (NOLOCK)
on WESTI.examSessionId = west.ID
	and WESTI.ItemRef = WESIRT.ItemID
	--and wesirt.itemid in ('974P1252','974P1225','974P1180','974P1169','974P1132','974P1064')
--where west.Keycode in ('3W7PK6A6','6CRRCRA6','73YPBYA6','7FKGBYA6','7J7GDPA6','8FRV76A6','8TV9KMA6','9J4K3RA6','9K34T4A6','BNLKF6A6','BWLNP7A6','DPGTC9A6','FYDWBLA6','HHVBLGA6','J4K4PTA6','J6G4XWA6','HM8WRTA6','H6T4WWA6','KR3TF9A6','GP9XLWA6','GNKQNVA6','NG74CFA6','FNPXKYA6','PNJKPCA6','PRGGN6A6','F9Y6N3A6','F6WNFFA6','DR6JGJA6','DQP4MYA6','QCKM9BA6','QDBNFHA6','DKK9W8A6','CDPCL4A6','C3FMH3A6','VDGXBXA6','VPV7GJA6','BNRD9DA6','WMPNGDA6','9VKJ7VA6','8RYNQMA6','XF78BGA6','Y4D44BA6','6YH9T9A6','6P8JTDA6','6MR3NMA6','4N4DL7A6','4CTYLHA6','3XHPJRA6','3MWC7BA6','3FPJ3PA6','33L4VPA6','K4HRFKA6','KKDRN4A6','L68DHWA6','M8VYLRA6','PHWXDWA6','PVWT6DA6','Q6M9PGA6','Q766KYA6','Q87KLYA6','QQTK99A6','TQ7PCWA6','TTCGLLA6','VQTC9QA6','WNYJFKA6','WRJ8WWA6','YTTM7HA6')
	--and west.KeyCode in ('7FKGBYA6','GP9XLWA6','J4K4PTA6','7J7GDPA6','QCKM9BA6','4N4DL7A6','YTTM7HA6','6MR3NMA6','73YPBYA6','BWLNP7A6','PRGGN6A6','FNPXKYA6','C3FMH3A6','DQP4MYA6')
) B
where R = 1
--order by keycode, WESIRT.ItemID
	and  [result_shouldbe] <> originalGrade


*/