if OBJECT_ID ('tempdb..#checker') is not null drop table #checker;

create table #checker (
	keycode nvarchar(10)
	, correctMark float
	, assignedMark float
);


bulk insert #checker
from 'C:\Users\977496-davej2\Desktop\AATMarkCheckerCombine.csv'
with (fieldterminator=',',rowterminator='\n')

SELECT B.*
FROM (
SELECT A.keycode
	, a.correctMark
	, a.assignedMark
	, wests.userMark
	,(wests.userMark-(a.assignedMark * WESTI.TotalMark))+(correctMark*WESTI.TotalMark) [userMark_shouldbe]
	, userPercentage
	, (((wests.userMark-(a.assignedMark * WESTI.TotalMark))+(correctMark*WESTI.TotalMark))/west.resultData.value('data(exam/@totalMark)[1]','tinyint'))*100 [userPercent_shouldbe]
	, WESTI.TotalMark [ItemTotalMark]
	, west.resultData.value('data(exam/@passMark)[1]','tinyint') [ExamPassMark]
	, west.resultData.value('data(exam/@totalMark)[1]','tinyint') [ExamTotalMark]
	,WESTI.ItemRef 
	, originalGrade
	--, west.resultData
	, case when
		((((wests.userMark-(a.assignedMark * WESTI.TotalMark))+(correctMark*WESTI.TotalMark))/west.resultData.value('data(exam/@totalMark)[1]','tinyint'))*100 < west.resultData.value('data(exam/@passMark)[1]','tinyint')) then 'Fail'
			ELSE 'Pass'
			END AS [result_shouldbe]
	, clientInformation.value('data(clientInformation/systemConfiguration/flashPlayer/version)[1]','nvarchar(200)') [Flash]
	, downloadInformation.value('data(downloads/information/date)[1]','nvarchar(MAX)') [DownloadedToPC]
	, clientInformation.value('data(clientInformation/systemConfiguration/timeZone/timeZoneName)[1]','nvarchar(200)') [Timezone]
	, clientInformation.value('data(clientInformation/systemConfiguration/timeZone/daylightSavingTime)[1]','bit') [DaylightSavingTime]
	, [started] as [State 6]
	, ROW_NUMBER() over (partition by west.Keycode order by wesirt.itemid) R
FROM #checker A
INNER JOIN WAREHOUSE_ExamSessionTable_Shreded wests WITH (NOLOCK)
on wests.keycode = a.keycode
inner join WAREHOUSE_ExamSessionTable west WITH (NOLOCK)
on west.id = wests.examSessionId
INNER JOIN WAREHOUSE_ExamSessionItemResponseTable WESIRT WITH (NOLOCK)
ON WESIRT.WAREHOUSEExamSessionID = WEST.ID	
	and assignedMark = WESIRT.ItemResponseData.value('data(p/@um)[1]','float')
INNER JOIN WAREHOUSE_ExamSessionTable_ShrededItems WESTI WITH (NOLOCK)
on WESTI.examSessionId = west.ID
	and WESTI.ItemRef = WESIRT.ItemID
	and wesirt.itemid in ('974P1252','974P1125','974P1180','974P1169','974P1132','974P1064')
where west.KeyCode not in ('3HGJY9A6','3JGQCRA6','4CTKNNA6','4NLHKYA6','4NRCQBA6','4PMHYPA6','6LVVT3A6','6LYV9DA6','93PWF8A6','94R79FA6','9B9T3QA6','9FTWX7A6','9MVCRFA6','BTMJNBA6','CHXRT8A6','CVQMNGA6','DCBMKGA6','DKRJ69A6','DL4PYQA6','FD77FDA6','FLW8HRA6','FX6HQ4A6','HWYFBVA6','J89GHTA6','JBVQ9XA6','JQ4CRJA6','JQDYJXA6','JTLWRPA6','KD3FM8A6','LYLHTGA6','N7XWWDA6','P9NJDYA6','PK8RXNA6','PR7LNBA6','PRQQP6A6','PT7476A6','Q6337GA6','Q6BK4TA6','Q7MCGFA6','QGMQ3YA6','QLMR3HA6','R4JNB6A6','T3V4MYA6','TB7RXFA6','VVDJGFA6','W46HCYA6','XKGLTCA6','XL3PRXA6','XXX96QA6','Y4J67BA6')
	--and west.KeyCode in ('7FKGBYA6','GP9XLWA6','J4K4PTA6','7J7GDPA6','QCKM9BA6','4N4DL7A6','YTTM7HA6','6MR3NMA6','73YPBYA6','BWLNP7A6','PRGGN6A6','FNPXKYA6','C3FMH3A6','DQP4MYA6')
) B
--where R > 1
--order by keycode, WESIRT.ItemID
where [result_shouldbe] <> originalGrade


select KeyCode 
from WAREHOUSE_ExamSessionTable west WITH (NOLOCK)
--left JOIN WAREHOUSE_ExamSessionItemResponseTable WESIRT WITH (NOLOCK)
--ON WESIRT.WAREHOUSEExamSessionID = WEST.ID
where west.KeyCode in ('7FKGBYA6','GP9XLWA6','J4K4PTA6','7J7GDPA6','QCKM9BA6','4N4DL7A6','YTTM7HA6','6MR3NMA6','73YPBYA6','BWLNP7A6','PRGGN6A6','FNPXKYA6','C3FMH3A6','DQP4MYA6')
	and wesirt.itemid in ('974P1252','974P1125','974P1180','974P1169','974P1132','974P1064')

	

select KeyCode 
from WAREHOUSE_ExamSessionTable_Shreded west WITH (NOLOCK)
--left JOIN WAREHOUSE_ExamSessionItemResponseTable WESIRT WITH (NOLOCK)
--ON WESIRT.WAREHOUSEExamSessionID = WEST.ID
where west.KeyCode in ('7FKGBYA6','GP9XLWA6','J4K4PTA6','7J7GDPA6','QCKM9BA6','4N4DL7A6','YTTM7HA6','6MR3NMA6','73YPBYA6','BWLNP7A6','PRGGN6A6','FNPXKYA6','C3FMH3A6','DQP4MYA6')