--/****** Script for SelectTopNRows command from SSMS  ******/
--SELECT top 100 *
--  FROM [AAT_SecureAssess].[dbo].[ExamSessionCandidateInteractionLogsTable]
--  order by [TimeStamp] desc;

--  /****** Script for SelectTopNRows command from SSMS  ******/
--SELECT top 100 *
--  FROM [AAT_SecureAssess].[dbo].[WAREHOUSE_ExamSessionCandidateInteractionLogsTable]
--  order by [TimeStamp] desc;

USE AAT_SecureAssess

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

SELECT qualificationname
	, examname
	--, examversionref
	--, ExternalReference
	, centrename
	, centreCode
	, west.KeyCode
	, WESIRT.ItemID
	, WESIRT.[ItemVersion]
	, [AQT]
	, west.warehouseTime
FROM WAREHOUSE_ExamSessionTable WEST
INNER JOIN WAREHOUSE_ExamSessionItemResponseTable WESIRT
ON WESIRT.WAREHOUSEExamSessionID = WEST.ID
INNER JOIN WAREHOUSE_ExamSessionTable_Shreded WESTS
on wests.examSessionID = west.id
INNER JOIN (

	SELECT [Project Name]
			, itemid
			, [ItemVersion]
			,[AQT]
	FROM [430327-AAT-SQL2\SQL2].[AAT_CPProjectAdmin].dbo.[ledgerincomplete] WITH (NOLOCK)
) CP
ON CP.ItemID = WESIRT.ItemID	
	and CP.ItemVersion = WESIRT.ItemVersion

where west.warehouseTime > '2016-11-01'
	and west.PreviousExamState != 10
	and ExternalReference not like '%PRACTICE%'
order by warehouseTime desc, KeyCode