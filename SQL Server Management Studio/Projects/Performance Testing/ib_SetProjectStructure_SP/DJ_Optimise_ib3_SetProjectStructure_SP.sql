USE [DI_BC_ItemBank]
GO
/****** Object:  StoredProcedure [dbo].[ib3_SetProjectStructure_SP]    Script Date: 07/21/2015 15:55:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET STATISTICS IO ON
--ALTER PROCEDURE [dbo].[DJ_ib3_SetProjectStructure_SP]
DECLARE
	@ProjectID int = 327,
	@ProjectMetaDataXml xml = '<Defaults>
  <GridSize>5</GridSize>
  <ContentWidth>1010</ContentWidth>
  <ContentHeight>680</ContentHeight>
  <DefaultFontSize>14</DefaultFontSize>
  <Fonts Embedded="1" XLIFF="0">
    <Font>Arial</Font>
    <Font>Arial SubScript</Font>
    <Font>Arial SuperScript</Font>
    <Font>Arial_Symbols</Font>
    <Font>Comic Sans MS</Font>
    <Font>ComicSans_Symbols</Font>
    <Font>SassoonSans</Font>
    <Font>Times New Roman</Font>
    <Font>Trebuchet MS</Font>
    <Font>Verdana</Font>
    <Font>BTL_Symbols</Font>
    <Font>Surpass Greek</Font>
    <Font>Surpass Maths</Font>
    <Font>Surpass Symbols</Font>
  </Fonts>
  <AdditionalPageAttributes />
  <StatusList advW="1" val="-1" nId="10">
    <Status val="-2" id="-2">Guest<users><user id="412" /><user id="633" /><user id="434" /><user id="627" /><user id="612" /><user id="132" /><user id="310" /><user id="43" /><user id="440" /><user id="441" /><user id="442" /><user id="443" /><user id="444" /><user id="445" /><user id="450" /><user id="449" /><user id="448" /><user id="447" /><user id="446" /><user id="467" /><user id="477" /><user id="486" /><user id="484" /><user id="483" /><user id="482" /><user id="479" /><user id="485" /><user id="481" /><user id="487" /><user id="465" /><user id="494" /><user id="495" /><user id="496" /><user id="492" /><user id="428" /><user id="468" /><user id="498" /><user id="464" /><user id="499" /><user id="497" /><user id="478" /><user id="506" /><user id="508" /><user id="3" /><user id="505" /><user id="507" /><user id="510" /><user id="50" /><user id="427" /><user id="451" /><user id="2" /><user id="426" /><user id="514" /><user id="515" /><user id="516" /><user id="518" /><user id="517" /><user id="72" /><user id="519" /><user id="532" /><user id="529" /><user id="520" /><user id="525" /><user id="526" /><user id="521" /><user id="524" /><user id="539" /><user id="542" /><user id="527" /><user id="547" /><user id="582" /><user id="585" /><user id="584" /><user id="583" /><user id="599" /><user id="602" /><user id="603" /><user id="601" /><user id="619" /><user id="607" /><user id="616" /><user id="620" /><user id="613" /><user id="621" /><user id="615" /><user id="606" /><user id="614" /><user id="622" /><user id="611" /><user id="605" /><user id="609" /><user id="600" /><user id="608" /><user id="618" /><user id="617" /><user id="623" /><user id="604" /><user id="610" /><user id="425" /><user id="548" /><user id="630" /><user id="655" /><user id="656" /><user id="650" /><user id="666" /><user id="667" /><user id="470" /><user id="701" /><user id="78" /><user id="73" /><user id="79" /><user id="80" /><user id="76" /><user id="77" /></users></Status>
    <Status id="0" val="0">Draft<users><user id="516" noinherit="true" /></users></Status>
    <Status id="1" val="1">Ready for review</Status>
    <Status id="2" val="2">Proof read<users><user id="426" noinherit="true" /></users></Status>
    <Status id="3" val="3">Ready for approval</Status>
    <Status id="4" val="4">Approved<users><user id="514" /><user id="516" /><user id="449" /></users></Status>
    <Status id="9" val="5">Released<users><user id="542" /><user id="468" /><user id="3" /><user id="498" /><user id="630" /><user id="666" /></users></Status>
    <Status id="10" val="6">Withdrawn<users><user id="2" /><user id="510" /><user id="310" /><user id="467" /><user id="43" /><user id="542" /><user id="449" /><user id="497" /><user id="426" /><user id="470" /><user id="701" /><user id="656" /><user id="78" /><user id="73" /><user id="79" /><user id="80" /><user id="76" /><user id="77" /></users><roles /></Status>
  </StatusList>
  <Actors>
    <Actor>Character 1</Actor>
    <Actor>Character 2</Actor>
    <Actor>Character 3</Actor>
    <Actor>Character 4</Actor>
    <Actor>Character 5</Actor>
  </Actors>
  <Languages>
    <Language>English</Language>
  </Languages>
  <Stacking>1</Stacking>
  <qti>1</qti>
  <VisibleContentWidth>1000</VisibleContentWidth>
  <VisibleContentHeight>560</VisibleContentHeight>
  <ContentLocationX>0</ContentLocationX>
  <ContentLocationY>70</ContentLocationY>
  <ScenePadding>30</ScenePadding>
  <FooterPadding>0</FooterPadding>
  <HeaderPadding>30</HeaderPadding>
  <AS3Engine>0</AS3Engine>
  <StructureAttributes>
    <Attribute Name="TotalMark" Type="Page" />
    <Attribute Name="unit" Type="Page" />
    <Attribute Name="LO" Type="Page" />
    <Attribute Name="markingCriteria" Type="HTML" />
    <Attribute Name="ImageDescription" Type="Page" Mandatory="0" />
    <Attribute Name="ImageTitle" Type="Page" Mandatory="0" />
    <Attribute Name="ImageKeyword" Type="Page" Mandatory="0" />
    <Attribute Name="ImageURL" Type="Page" Mandatory="0" />
    <Attribute Name="Desc" Type="Page" Mandatory="0" />
    <Attribute Name="Keywords" Type="Page" Mandatory="0" />
    <Attribute Name="Title" Type="Page" Mandatory="0" />
    <Attribute Name="MarkingType" Type="Page" Mandatory="0">
      <Value display="0">0</Value>
      <Value display="1">1</Value>
      <Value display="3">2</Value>
    </Attribute>
    <Attribute Name="BTLOffice" Type="Page" Mandatory="0">
      <Value display="0">0</Value>
      <Value display="1">1</Value>
    </Attribute>
  </StructureAttributes>
  <ToolbarFontRange>
    <from>14</from>
    <to>72</to>
  </ToolbarFontRange>
  <QualificationReferences />
</Defaults>',
	@ProjectStructureXml xml = 
	
	'<P ID="327" Nam="BTL Surpass Item Reference Guide">
  <F ID="466" Nam="Toolbox_PLEASE DO NOT EDIT ANY PAGES IN THIS FOLDER">
    <I ID="327P562" Nam="Introduction" sta="9" pMU="1" pMD="02/08/2011 10:53:59" TotalMark="0" hotspotToolData="" uC="0" fV="0.500" />
    <F ID="353" Nam="Standard Question Toolkit">
      <F ID="355" Nam="Mutiple Choice">
        <I ID="327P336" Nam="Standard" quT="10" sta="9" pMU="1" pMD="25/08/2011 14:00:52" LO="Chess" unit="Sport and Leisure" TotalMark="1" cmt="Recursive Check-in" hotspotToolData="" uC="0" fV="0.500" />
        <I ID="327P365" Nam="Objective Fact or Opinion" sta="9" pMU="1" pMD="25/08/2011 14:00:56" quT="10" LO="Swimming" unit="Sport and Leisure" TotalMark="4" cmt="Recursive Check-in" hotspotToolData="" uC="0" fV="0.500" />
        <I ID="327P364" Nam="Yes No Cant Tell" sta="9" pMU="1" pMD="25/08/2011 14:00:52" quT="10" LO="Snowboarding" unit="Sport and Leisure" TotalMark="5" cmt="Recursive Check-in¬System Generated Message: Checked out to update a shared library item¬" hotspotToolData="" uC="0" fV="0.500" />
        <I ID="327P348" Nam="True or False" quT="10" sta="9" pMU="467" pMD="04/08/2011 08:59:55" LO="Miscellaneous" unit="Science and Nature" TotalMark="4" cmt="Recursive Check-in" hotspotToolData="" uC="0" fV="0.500" />
        <I ID="327P363" Nam="Multiple Response" sta="9" pMU="467" pMD="04/08/2011 09:01:02" quT="10" LO="Probability" unit="Mathematics" TotalMark="2" cmt="Recursive Check-in" hotspotToolData="" uC="0" fV="0.500" />
        <I ID="327P346" Nam="Multi Choice Subscript" quT="10" sta="9" pMU="467" pMD="04/08/2011 09:02:21" LO="Hexadecimal" unit="Mathematics" TotalMark="1" cmt="Recursive Check-in" hotspotToolData="" uC="0" fV="0.500" />
      </F>
      <F ID="554" Nam="Quickfire">
        <I ID="327P367" Nam="Standard" sta="9" pMU="1" pMD="25/08/2011 14:00:46" quT="10" LO="Cost" unit="Mathematics" TotalMark="1" cmt="Recursive Check-in" hotspotToolData="" uC="0" fV="0.500" />
        <I ID="327P366" Nam="True false cant tell Quickfire" sta="9" pMU="1" pMD="03/08/2011 16:05:10" quT="10" unit="Miscellaneous" TotalMark="4" cmt="Recursive Check-in¬System Generated Message: Checked out to update a shared library item¬¬System Generated Message: Checked out to update a shared library item¬¬System Generated Message: Checked out to update a shared library item¬" hotspotToolData="" uC="0" fV="0.500" />
        <I ID="327P400" Nam="Table Quickfire" quT="10" sta="9" pMU="1" pMD="03/08/2011 16:05:10" LO="Probability" unit="Mathermatics" TotalMark="1" cmt="Recursive Check-in" hotspotToolData="" uC="0" fV="0.500" />
        <I ID="327P654" Nam="Image" quT="10" sta="9" pMU="467" pMD="25/11/2011 14:41:21" LO="Birds" unit="Sceince and Nature" TotalMark="4" hotspotToolData="" uC="0" fV="0.500" />
      </F>
      <F ID="356" Nam="Gapfill">
        <I ID="327P374" Nam="Gapfill labelling" sta="9" pMU="630" pMD="09/11/2011 15:29:17" quT="11" LO="Physics" unit="Sceince and Nature" TotalMark="4" cmt="Recursive Check-in" MarkingType="0" hotspotToolData="" uC="0" fV="0.500" />
        <I ID="327P331" Nam="Gapfill shared answer" quT="11" sta="9" pMU="1" pMD="25/08/2011 14:00:51" Unit="(unspecified)" LO="Language" unit="English" TotalMark="3" cmt="Automatic Check-in" hotspotToolData="" uC="0" fV="0.500" />
        <I ID="327P338" Nam="Multi Gapfill in a sentence" quT="11" sta="9" pMU="1" pMD="25/08/2011 14:00:48" LO="Moon" unit="Science and Nature" TotalMark="4" cmt="Recursive Check-in" hotspotToolData="" uC="0" fV="0.500" />
      </F>
      <F ID="555" Nam="Extended Text Entry">
        <I ID="327P376" Nam="Standard" sta="9" pMU="1" pMD="11/10/2011 12:03:03" quT="11" MarkingType="1" cmt="Recursive Check-in" TotalMark="10" hotspotToolData="" uC="10" fV="0.130001" />
        <I ID="327P559" Nam="Text Formatting Math Symbol " sta="9" pMU="1" pMD="11/10/2011 12:03:04" quT="11" MarkingType="1" TotalMark="10" cmt="Recursive Check-in" hotspotToolData="" uC="10" fV="0.25" />
        <I ID="327P560" Nam="Text Formatting Language Symbol " sta="9" pMU="1" pMD="11/10/2011 12:03:04" quT="11" MarkingType="1" cmt="Recursive Check-in" TotalMark="10" hotspotToolData="" uC="10" fV="0.11" />
        <I ID="327P561" Nam="Text Formatting Greek Symbol Symbol " sta="9" pMU="1" pMD="25/08/2011 14:00:45" quT="11" MarkingType="1" cmt="Recursive Check-in" TotalMark="10" hotspotToolData="" uC="12" fV="0.141667" />
      </F>
      <F ID="359" Nam="Picklist">
        <I ID="327P337" Nam="Standard" quT="12" sta="9" pMU="1" pMD="04/08/2011 09:55:54" LO="Rivers" unit="Geography" TotalMark="1" cmt="Recursive Check-in" hotspotToolData="" uC="0" fV="0.500" />
        <I ID="327P369" Nam="Multi pick list" sta="9" pMU="467" pMD="04/08/2011 09:50:42" quT="12" LO="Chemistry" unit="Sceince and Nature" TotalMark="3" cmt="Recursive Check-in" hotspotToolData="" uC="0" fV="0.500" />
        <I ID="327P370" Nam="Multi picklist in text" sta="9" pMU="1" pMD="03/08/2011 15:25:09" quT="12" unit="English" TotalMark="3" cmt="Recursive Check-in" hotspotToolData="" uC="0" fV="0.500" />
        <I ID="327P371" Nam="Diagram picklist" sta="9" pMU="1" pMD="25/08/2011 14:00:53" quT="12" LO="Chemistry" unit="Science and Nature" TotalMark="2" cmt="Recursive Check-in" MarkingType="0" hotspotToolData="" uC="0" fV="0.500" />
      </F>
      <F ID="360" Nam="Re-Ordering">
        <I ID="327P385" Nam="Re-order text alphabetically" sta="9" pMU="426" pMD="03/02/2012 12:12:40" quT="10,13" unit="English" TotalMark="4" hotspotToolData="" uC="0" fV="0.500" />
        <I ID="327P439" Nam="Re-order paragraphs" quT="13" sta="9" pMU="1" pMD="03/08/2011 15:25:09" TotalMark="4" hotspotToolData="" uC="0" fV="0.500" />
        <I ID="327P340" Nam="Re ordering text and Image" quT="13" sta="9" pMU="1" pMD="25/08/2011 14:00:46" LO="Birds" unit="Sceince and Nature" TotalMark="4" hotspotToolData="" uC="0" fV="0.500" />
      </F>
      <F ID="358" Nam="Drag and Drop">
        <I ID="327P612" Nam="Single Correct Image DnD" quT="16" sta="9" hotspotToolData="" uC="0" fV="0.500" TotalMark="1" />
        <I ID="327P339" Nam="Horizontal stack drag and drop" quT="16" sta="9" pMU="1" pMD="09/06/2011 14:12:42" hotspotToolData="" uC="0" fV="0.500" TotalMark="1" />
        <I ID="327P342" Nam="Vertical stack drag and drop" quT="16" sta="9" pMU="1" pMD="04/08/2011 11:25:56" hotspotToolData="" uC="0" fV="0.500" TotalMark="1" />
        <I ID="327P345" Nam="Drag and Drop in a sentence" quT="16" sta="9" pMU="1" pMD="09/06/2011 14:12:42" hotspotToolData="" uC="0" fV="0.500" TotalMark="1" />
        <I ID="327P378" Nam="Fish award Click and Click" sta="9" pMU="1" pMD="25/08/2011 14:05:46" quT="16" hotspotToolData="" uC="0" fV="0.500" TotalMark="1" />
        <I ID="327P450" Nam="Ratio drag and drop abs positioning and clones" quT="16" sta="9" pMU="467" pMD="04/08/2011 11:39:19" hotspotToolData="" uC="0" fV="0.500" TotalMark="1" />
        <I ID="327P398" Nam="Either or drag and drop required" quT="16" sta="9" pMU="1" pMD="09/06/2011 14:22:43" hotspotToolData="" uC="0" fV="0.500" TotalMark="1" />
        <I ID="327P402" Nam="Video Drag and Drop" quT="16" sta="9" pMU="1" pMD="25/08/2011 14:00:50" hotspotToolData="" uC="0" fV="0.500" TotalMark="1" />
        <I ID="327P629" Nam="n minus 1 marking" markingSchemeFor="-1" isWordTemplate="F" isSourceItem="F" quT="16" sta="9" pMU="1" pMD="01/09/2011 15:09:14" TotalMark="3" cmt="set N minus 1 property to TRUE¬Item not working because N-1 option doesn&amp;apos;t support large dummy dropzone which is on page.  Removing this dummy will make n-1 marking work.  Next build will resolve this and will allow dummy dropzone to be in place." hotspotToolData="" uC="0" fV="0.500" />
      </F>
      <F ID="362" Nam="Image Map">
        <I ID="327P444" Nam="Image map follow audio" quT="15" sta="9" pMU="1" pMD="25/08/2011 14:00:58" cmt="Recursive Check-in" hotspotToolData="" uC="0" fV="0.500" TotalMark="1" />
        <I ID="327P384" Nam="Image map two answers" sta="9" pMU="1" pMD="03/08/2011 15:35:08" quT="15" cmt="Recursive Check-in" hotspotToolData="" uC="0" fV="0.500" TotalMark="1" />
        <I ID="327P383" Nam="Image map one answer" sta="9" pMU="1" pMD="03/08/2011 15:35:09" quT="15" cmt="Recursive Check-in" hotspotToolData="" uC="0" fV="0.500" TotalMark="1" />
      </F>
      <F ID="548" Nam="Linking Boxes">
        <I ID="327P386" Nam="Linking Boxes" sta="9" pMU="1" pMD="03/08/2011 15:25:08" quT="20" cmt="Recursive Check-in" hotspotToolData="" uC="0" fV="0.500" TotalMark="1" />
        <I ID="327P387" Nam="Linking Images" sta="9" pMU="1" pMD="25/08/2011 14:00:55" quT="20" cmt="Recursive Check-in" TotalMark="6" MarkingType="0" hotspotToolData="" uC="0" fV="0.500" />
      </F>
      <F ID="549" Nam="Attach File">
        <I ID="327P479" Nam="File Upload" sta="9" cmt="Added file upload parameters" pMU="1" pMD="05/10/2011 09:43:08" MarkingType="1" TotalMark="10" hotspotToolData="" uC="10" fV="0.25" />
      </F>
    </F>
    <F ID="544" Nam="Graph Question Toolkit">
      <F ID="550" Nam="Bar Chart">
        <I ID="327P335" Nam="Bar Chart Extension with Gap fill Bouncing Ball" quT="11,20" MarkingType="0" TotalMark="6" sta="9" pMU="1" pMD="25/08/2011 14:00:57" hotspotToolData="" uC="0" fV="0.500" />
      </F>
      <F ID="551" Nam="Pie Chart">
        <I ID="327P343" Nam="Standard" quT="20" sta="9" pMU="1" pMD="03/08/2011 15:35:09" hotspotToolData="" uC="0" fV="0.500" TotalMark="1" />
      </F>
      <F ID="552" Nam="Scatter Graph">
        <I ID="327P344" Nam="Standard" quT="20" sta="9" pMU="1" pMD="25/08/2011 14:00:53" chO="2" chO2="BALTI" hotspotToolData="" uC="0" fV="0.500" TotalMark="1" />
      </F>
      <F ID="553" Nam="Line Graph">
        <I ID="327P563" Nam="Standard" quT="20" sta="9" pMU="1" pMD="25/08/2011 14:00:57" hotspotToolData="" uC="0" fV="0.500" TotalMark="1" />
      </F>
    </F>
    <F ID="545" Nam="Maths Question Toolkit">
      <F ID="556" Nam="Number Entry">
        <I ID="327P334" Nam="Standard" sta="9" pMU="1" pMD="25/08/2011 14:00:54" quT="11" MarkingType="0" TotalMark="4" LO="Speed" unit="Mathematics" hotspotToolData="" uC="0" fV="0.500" />
        <I ID="327P436" Nam="Follow on marking gap fill" quT="11" sta="9" pMU="630" pMD="10/11/2011 11:54:35" TotalMark="3" cmt="Automatic Check-in" hotspotToolData="" uC="0" fV="0.500" />
        <I ID="327P437" Nam="Formula marking gap fill " quT="11" sta="9" pMU="1" pMD="10/11/2011 09:29:32" cmt="Recursive Check-in" hotspotToolData="" uC="0" fV="0.500" TotalMark="1" />
        <I ID="327P438" Nam="Decimal places gap fill" quT="11" sta="9" pMU="1" pMD="10/11/2011 09:29:32" TotalMark="2" cmt="Recursive Check-in" hotspotToolData="" uC="0" fV="0.500" />
      </F>
      <F ID="557" Nam="Range Number Entry">
        <I ID="327P405" Nam="Standard" quT="11" sta="9" pMU="1" pMD="09/06/2011 11:22:34" hotspotToolData="" uC="0" fV="0.500" TotalMark="1" />
      </F>
    </F>
    <F ID="546" Nam="Literacy Question Toolkit">
      <F ID="361" Nam="Text Selector">
        <I ID="327P368" Nam="Multi choice text" sta="9" pMU="1" pMD="09/06/2011 15:12:44" quT="14" hotspotToolData="" uC="0" fV="0.500" TotalMark="1" />
        <I ID="327P440" Nam="Text selector single word answer" quT="14" sta="9" pMU="1" pMD="25/08/2011 14:00:56" TotalMark="1" hotspotToolData="" uC="0" fV="0.500" />
        <I ID="327P341" Nam="Text highlight 4 answers" quT="14" sta="9" pMU="1" pMD="09/06/2011 15:12:44" hotspotToolData="" uC="0" fV="0.500" TotalMark="1" />
        <I ID="327P441" Nam="Text selector sentence" quT="14" sta="9" pMU="1" pMD="25/08/2011 14:00:48" TotalMark="1" hotspotToolData="" uC="0" fV="0.500" />
      </F>
      <F ID="558" Nam="Proof Reading">
        <I ID="327P451" Nam="Proof-reading" quT="20" sta="9" pMU="1" pMD="25/08/2011 14:00:48" MarkingType="1" TotalMark="6" markingCriteria="Identify and describe main point/ideas.&#xD;&#xA;Read to summarise information/ideas/purpose of texts." cmt="¬System Generated Message: Checked out to update a shared library item¬¬System Generated Message: Checked out to update a shared library item¬¬System Generated Message: Checked out to update a shared library item¬¬System Generated Message: Checked out to update a shared library item¬¬System Generated Message: Checked out to update a shared library item¬¬System Generated Message: Checked out to update a shared library item¬¬System Generated Message: Checked out to update a shared library item¬¬System Generated Message: Checked out to update a shared library item¬" hotspotToolData="" uC="10" fV="0.166666" />
      </F>
    </F>
    <F ID="547" Nam="ICT Question Toolkit">
      <F ID="481" Nam="BTL Office websites">
        <F ID="587" Nam="www.learn2discover.com">
          <I ID="327P588" Nam="home.htm" sta="9" pMU="1" pMD="25/08/2011 14:00:48" cmt="Automatic Check-in" TotalMark="0" hotspotToolData="" uC="0" fV="0.500" />
          <I ID="327P600" Nam="about.htm" sta="9" pMU="467" pMD="29/06/2011 11:19:17" cmt="Recursive Check-in" TotalMark="0" hotspotToolData="" uC="0" fV="0.500" />
          <I ID="327P601" Nam="news.htm" sta="9" pMU="467" pMD="29/06/2011 11:19:38" cmt="Recursive Check-in" TotalMark="0" hotspotToolData="" uC="0" fV="0.500" />
          <I ID="327P602" Nam="contacts.htm" sta="9" pMU="467" pMD="29/06/2011 11:19:57" cmt="Recursive Check-in" TotalMark="0" hotspotToolData="" uC="0" fV="0.500" />
          <I ID="327P603" Nam="loanboxes.htm" sta="9" pMU="467" pMD="29/06/2011 11:20:16" cmt="Recursive Check-in" TotalMark="0" hotspotToolData="" uC="0" fV="0.500" />
        </F>
        <F ID="583" Nam="www.stilllooking.com">
          <I ID="327P584" Nam="search.htm" sta="9" pMU="467" pMD="24/06/2011 09:07:18" TotalMark="0" hotspotToolData="" uC="0" fV="0.500" />
          <I ID="327P586" Nam="error.htm" sta="9" pMU="467" pMD="24/06/2011 09:17:01" cmt="Recursive Check-in" TotalMark="0" hotspotToolData="" uC="0" fV="0.500" />
        </F>
        <F ID="569" Nam="www.btl.com">
          <I ID="327P570" Nam="home.htm" sta="9" pMU="1" pMD="25/08/2011 14:00:56" Keywords="e-Assessment, e-Learning, BTL, Surpass, ContentProducer, ItemBank, SecureAssess" Title="e-Assessment solutions by BTL - Computer Based Testing and Online ..." Desc="BTL is a leading UK e-Assessment, learning development and computer based testing company. Our content development and delivery tools are helping some of ..." chO4r="449" TotalMark="0" hotspotToolData="" uC="0" fV="0.500" />
          <I ID="327P571" Nam="about.htm" sta="9" pMU="467" pMD="23/06/2011 15:25:52" cmt="Recursive Check-in" TotalMark="0" hotspotToolData="" uC="0" fV="0.500" />
          <I ID="327P572" Nam="news.htm" sta="9" pMU="467" pMD="29/06/2011 11:39:39" cmt="Recursive Check-in" TotalMark="0" hotspotToolData="" uC="0" fV="0.500" />
          <I ID="327P573" Nam="partners.htm" sta="9" pMU="467" pMD="23/06/2011 15:25:52" cmt="Recursive Check-in" TotalMark="0" hotspotToolData="" uC="0" fV="0.500" />
          <I ID="327P574" Nam="resources.htm" sta="9" pMU="467" pMD="23/06/2011 15:25:53" cmt="Recursive Check-in" TotalMark="0" hotspotToolData="" uC="0" fV="0.500" />
          <I ID="327P575" Nam="events.htm" sta="9" pMU="467" pMD="23/06/2011 15:25:53" cmt="Recursive Check-in" TotalMark="0" hotspotToolData="" uC="0" fV="0.500" />
          <I ID="327P576" Nam="careers.htm" sta="9" pMU="467" pMD="23/06/2011 15:25:54" cmt="Recursive Check-in" TotalMark="0" hotspotToolData="" uC="0" fV="0.500" />
          <I ID="327P577" Nam="contact.htm" sta="9" pMU="467" pMD="23/06/2011 15:28:13" cmt="Recursive Check-in" quT="11,12" hotspotToolData="" uC="0" fV="0.500" TotalMark="1" />
          <I ID="327P578" Nam="solutions.htm" sta="9" pMU="467" pMD="29/06/2011 10:34:03" cmt="Recursive Check-in" TotalMark="0" hotspotToolData="" uC="0" fV="0.500" />
          <I ID="327P579" Nam="tools.htm" sta="9" pMU="467" pMD="23/06/2011 15:25:55" cmt="Recursive Check-in" TotalMark="0" hotspotToolData="" uC="0" fV="0.500" />
          <I ID="327P580" Nam="services.htm" sta="9" pMU="467" pMD="23/06/2011 15:25:55" cmt="Recursive Check-in" TotalMark="0" hotspotToolData="" uC="0" fV="0.500" />
          <I ID="327P582" Nam="error.htm" sta="9" pMU="467" pMD="23/06/2011 15:43:50" TotalMark="0" hotspotToolData="" uC="0" fV="0.500" />
        </F>
        <F ID="483" Nam="www.futurescope.org.uk">
          <I ID="327P484" Nam="home.htm" sta="9" pMU="426" pMD="29/09/2010 15:49:19" TotalMark="0" hotspotToolData="" uC="0" fV="0.500" />
          <I ID="327P482" Nam="imagesearch.htm" sta="9" pMU="426" pMD="29/09/2010 14:57:20" TotalMark="0" hotspotToolData="" uC="0" fV="0.500" />
        </F>
        <F ID="486" Nam="www.royaltyfreepics.org.uk">
          <I ID="327P485" Nam="padlock.htm" sta="9" pMU="1" pMD="22/06/2011 15:35:17" ImageURL="http://www.royaltyfreepics.org.uk/padlock.htm" ImageTitle="Gold Padlock" ImageKeyword="padlock" ImageDescription="extra secure padlock" TotalMark="0" hotspotToolData="" uC="0" fV="0.500" />
        </F>
        <I ID="327P487" Nam="Images" sta="9" pMU="1" pMD="23/06/2011 15:36:15" cmt="Automatic Check-in" TotalMark="0" hotspotToolData="" uC="0" fV="0.500" />
        <I ID="327P585" Nam="Sophie Presentation Task" quT="20" sta="9" pMU="1" pMD="25/08/2011 14:00:51" cmt="Automatic Check-in" BTLOffice="1" hotspotToolData="" uC="0" fV="0.500" TotalMark="1" />
      </F>
      <I ID="327P525" Nam="BTL Office Human Marking" quT="11,20" sta="9" pMU="1" pMD="25/08/2011 14:00:50" cmt="Automatic Check-in" TotalMark="12" MarkingType="1" BTLOffice="1" hotspotToolData="" uC="2" fV="0.291667" />
      <I ID="327P480" Nam="BTL Office" quT="20" sta="9" pMU="1" pMD="23/09/2011 16:30:25" cmt="Marking has been set up. Needs further testing." BTLOffice="1" hotspotToolData="" uC="0" fV="0.500" TotalMark="1" />
    </F>
  </F>
</P>' 
--AS
--DROP TABLE #MyTemp

	SET NOCOUNT ON;
   
    --Add all relevent Assessment ID's into a table variable to use later in the update statement, to prevent an XPath update statement--
    
	--CREATE TABLE #MyTemp (projectID INT, ItemID NVARCHAR(20), TotalMark tinyint)
	--INSERT INTO #MyTemp(projectID)		
	--SELECT 
	--	DISTINCT ID
	--FROM dbo.AssessmentTable
	--WHERE AssessmentRules.exist('/PaperRules/Section/Rule/Param/ProjectIDList/ID[text() = sql:variable("@ProjectID")]') = 1;
	----CROSS APPLY AssessmentRules.nodes('//ProjectIDList/ID') a(b)
	----WHERE a.b.value('(.)[1]','int') =  @ProjectID
    
  
    --Store all items and their totalMarks    
        
    DECLARE @XMLholder TABLE (ProjectStructureXML XML)    
		INSERT @XMLholder 
			SELECT @ProjectStructureXml
    
    --DECLARE @ItemTotalMark TABLE (ItemID NVARCHAR(20), TotalMark tinyint)
		INSERT INTO #MyTemp(ItemID, TotalMark)		
			SELECT 
				my.item.value('@ID[1]','nvarchar(20)') AS ItemID
			  , my.item.value('@TotalMark[1]','tinyint') AS TotalMark
			FROM @XMLholder
			CROSS APPLY ProjectStructureXML.nodes('/P//I') my(item)
		
	--Validation
	IF (SELECT COUNT(ProjectID)FROM ProjectTable WHERE ProjectID=@ProjectID) < 1
		BEGIN		
			SELECT 0
			RETURN
		END
	ELSE	
	
		BEGIN TRY
			BEGIN TRANSACTION
			
			   BEGIN
		
				UPDATE [dbo].[ItemTable] 
				SET [TotalMark]=B.TotalMark 
				FROM [dbo].[ItemTable] A
				INNER JOIN #MyTemp B
				ON A.[ItemRef] = B.ItemID                                
				WHERE  A.[ProjectID]=@ProjectID			--Only do the one project
						AND A.[ItemRef] = B.ItemID		--Extra step to ensure they match
						AND A.[TotalMark]=-99			--Presumably to omit certains ones?
						AND A.TotalMark != B.TotalMark;	--Only update the ones that need it
	           END 


				BEGIN
				
				-- Update project structure
				UPDATE [dbo].[ProjectTable]
				SET [ProjectMetaData] = @ProjectMetaDataXml
				  ,[ProjectStructure] = @ProjectStructureXml
				WHERE [ProjectID] = @ProjectID;
				
				END
				-- Due to updated project structure, existing assessment rules
				-- may now be invalid so
				-- update requiresValidation field for each assessment that has rules
				-- based on this project structure
				BEGIN
				 
				UPDATE [dbo].[AssessmentTable]
				SET [RequiresValidation]=1, [VersionNumber] = [VersionNumber]+1
				FROM [dbo].[AssessmentTable] A
				WHERE AssessmentRules.exist('/PaperRules/Section/Rule/Param/ProjectIDList/ID[text() = sql:variable("@ProjectID")]') = 1;
				
				END		
						
				DELETE #MyTemp     		
							
			COMMIT TRANSACTION		
		END TRY
BEGIN CATCH
    SELECT 
        ERROR_NUMBER() AS ErrorNumber
        ,ERROR_SEVERITY() AS ErrorSeverity
        ,ERROR_STATE() AS ErrorState
        ,ERROR_PROCEDURE() AS ErrorProcedure
        ,ERROR_LINE() AS ErrorLine
        ,ERROR_MESSAGE() AS ErrorMessage;

    IF @@TRANCOUNT > 0
        ROLLBACK TRANSACTION;
END CATCH;

IF @@TRANCOUNT > 0
    COMMIT TRANSACTIOn
GO
