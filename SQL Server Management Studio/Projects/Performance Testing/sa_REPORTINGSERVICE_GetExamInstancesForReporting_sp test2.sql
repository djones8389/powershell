set statistics io on

DECLARE @RowStart tinyint = 1
	 , @RowEnd tinyint = 50

SELECT examSearch.*
FROM (

SELECT  ROW_NUMBER() OVER (ORDER BY Surname ASC) as RowNumber,      
			examSessionId, examName, previousExamState, examStateInformation,foreName, surName, centreName, qualificationName, SubmittedDate,resultData,scheduledDurationValue,candidateRef, ULN, keycode
			, ExternalReference, qualificationRef , CQN, examType, ContainsBTLOffice,WarehouseExamState,examResult, grade, originalgrade, adjustedGrade, userPercentage, userMark, TargetedForVoid,IsExternal
				,[DisableReportingCandidateReport], [DisableReportingSummaryReport], [DisableReportingCandidateBreakdownReport], [DisableReportingExamBreakdownReport], [DisableReportingResultSlip]
			FROM     
				(select    
					examSessionId,     
					WAREHOUSE_ExamSessionTable_Shreded.examName,     
					previousExamState,
					examStateInformation,
					Forename,     
					Surname,    
					CentreName,    
					WAREHOUSE_ExamSessionTable_Shreded.qualificationName,    
					submittedDate,
					resultData,
					scheduledDurationValue,
					CandidateRef,     
					ULN,
					KeyCode,    
					WAREHOUSE_ExamSessionTable_Shreded.ExternalReference,
					WAREHOUSE_ExamSessionTable_Shreded.qualificationRef,
					CQN,
					examType,
					ContainsBTLOffice,
					WarehouseExamState,
					examResult,    
					case when wset.SuppressResultsScreenResult = 1 AND previousExamState != 10 then 'na' else grade end as grade,
					originalgrade,
					adjustedGrade,
					case when wset.SuppressResultsScreenPercent = 0 then userPercentage else null end as userPercentage,
					case when wset.SuppressResultsScreenMark = 0 then userMark else null end as userMark, 
					TargetedForVoid,
					WAREHOUSE_ExamSessionTable_Shreded.IsExternal,
					[DisableReportingCandidateReport],
					[DisableReportingSummaryReport],
					[DisableReportingCandidateBreakdownReport],
					[DisableReportingExamBreakdownReport],
					[DisableReportingResultSlip]
					--wset.SuppressResultsScreenPercent
					FROM  WAREHOUSE_ExamSessionTable_Shreded WITH (NOLOCK)
					INNER JOIN WAREHOUSE_ScheduledExamsTable wset on WAREHOUSE_ExamSessionTable_Shreded.WAREHOUSEScheduledExamID = wset.ID
WHERE WarehouseExamState = 1  
AND submittedDate >= Convert(smalldatetime, '25/03/2016 00:00:00')  
AND submittedDate <= Convert(smalldatetime, '23/07/2016 23:59:59') 
AND Surname LIKE '%SIKALESELE%'   --SIKALESELE
AND EXISTS(
	SELECT TOP (1) [qualLevel] 
	FROM ##TEMP_CentreQualificationLevels_TABLE   
		WHERE qualLevel=dbo.WAREHOUSE_ExamSessionTable_Shreded.QualificationLevel 
			AND (centreId=1 OR centreId=dbo.WAREHOUSE_ExamSessionTable_Shreded.centreId)
		)
					
	AND WAREHOUSE_ExamSessionTable_Shreded.qualificationID IN (205,203,41,69,199,18,200,77,201,202,141,147,82,109,150,120,33,39,23,40,159,50,22,36,71,30,37,72,19,80,175,107,100,35,118,148,138,42,197,6,193,27,85,113,49,124,128,144,131,25,96,92,24,88,45,170,164,178,154,184,160,156,167,181,139,142,163,91,99,106,117,21,84,111,47,122,89,198,32,56,185,195,186,162,212,187,29,90,55,28,87,112,54,123,43,86,114,53,125,206,44,97,126,224,146,74,132,165,176,179,168,183,172,135,52,137,10,78,104,188,207,208,209,93,189,15,95,127,145,133,26,48,57,190,14,83,110,151,121,191,79,105,98,116,20,173,174,153,158,143,129,38,134,211,210,166,171,180,149,155,140,161,152,157,169,182,194,94,81,177,108,101,119,70,76,192)
				 
) as ExamDetails
) examSearch