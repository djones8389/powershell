USE [DI_BC_ItemBank]
GO
/****** Object:  Trigger [dbo].[assessmentValidityChange_Trigger]    Script Date: 07/22/2015 15:51:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Dave Dixon
-- Create date: 30/04/2008
-- Description:	This trigger fires when the validity of an assessment is set, and it updates
--				the validities of the parent assessment group and qualification accordingly

-- Modified:	Tom Gomersall
-- Date:		13/03/2013
-- Description:	Changed the logic so that an exam group is valid if it has a valid version, and invalid otherwise.

-- Modified:	Tom Gomersall
-- Date:		21/03/2013
-- Description:	Changed the qualification logic as for exam group above.

-- Modified:	Tom Gomersall
-- Date:		19/05/2013
-- Description:	Added state 3 (quality review) to the possible states for 
--				valid exams.
-- =============================================
ALTER TRIGGER [dbo].[assessmentValidityChange_Trigger] 
   ON  [dbo].[AssessmentTable] 
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON;

	PRINT 'IN assessmentValidityChange_Trigger TRIGGER'

    IF UPDATE([IsValid])
		BEGIN
			DECLARE @assessmentGroupId	int
			SELECT @assessmentGroupId=[AssessmentGroupId] FROM INSERTED

			DECLARE @isAssessmentGroupValid bit

			IF	
			(
				(
					SELECT COUNT(ID) 
					FROM [dbo].[AssessmentTable] 
					WHERE 
						[AssessmentGroupID]=@assessmentGroupId 
						AND 
						[IsValid]=1 
						AND 
						(
							[AssessmentStatus]=2
							OR
							[AssessmentStatus] = 3
						)
				) > 0
			)
				BEGIN
					SET @isAssessmentGroupValid=1
				END
			ELSE 
				BEGIN
					SET @isAssessmentGroupValid=0
				END

			UPDATE [dbo].[AssessmentGroupTable]
			   SET [IsValid]=@isAssessmentGroupValid
			 WHERE [ID]=@assessmentGroupId

			-- now check to see if this affects the validity of the assessment group's parent qualification
			DECLARE @qualificationId	int
			SELECT @qualificationId=[QualificationId] FROM [dbo].[AssessmentGroupTable]
			WHERE [ID]=@assessmentGroupId

			DECLARE @isQualificationValid bit

			IF	
			(
				(
					SELECT COUNT(ID) 
					FROM [dbo].[AssessmentGroupTable] 
					WHERE 
						[QualificationID]=@qualificationId 
						AND 
						[IsValid]=1 
						AND 
						[Status]=2
				) > 0
			)
				BEGIN
					SET @isQualificationValid=1
				END
			ELSE
				BEGIN
					SET @isQualificationValid=0
				END

			UPDATE [dbo].[QualificationTable]
			   SET [IsValid]=@isQualificationValid
			 WHERE [ID]=@qualificationId
		END

END
