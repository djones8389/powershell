Select PrimaryKey.*
FROM (

SELECT 
     TableName = t.name,
     IndexName = ind.name,
     IndexId = ind.index_id,
     ColumnId = ic.index_column_id,
     ColumnName = col.name
FROM 
sys.indexes ind 
INNER JOIN  sys.index_columns ic ON  ind.object_id = ic.object_id and ind.index_id = ic.index_id 
INNER JOIN  sys.columns col ON ic.object_id = col.object_id and ic.column_id = col.column_id 
INNER JOIN  sys.tables t ON ind.object_id = t.object_id 
WHERE  ind.type_desc = 'CLUSTERED'
	and t.name = 'ExamSessionItemResponseTable'
	) PrimaryKey
ORDER BY TableName, IndexName, IndexId , ColumnId, ColumnName



Select FKey.*
FROM (

SELECT 
     TableName = t.name,
     IndexName = ind.name,
     IndexId = ind.index_id,
     ColumnId = ic.index_column_id,
     ColumnName = col.name
FROM 
sys.indexes ind 
INNER JOIN  sys.index_columns ic ON  ind.object_id = ic.object_id and ind.index_id = ic.index_id 
INNER JOIN  sys.columns col ON ic.object_id = col.object_id and ic.column_id = col.column_id 
INNER JOIN  sys.tables t ON ind.object_id = t.object_id 
WHERE  ind.type_desc = 'NONCLUSTERED'
	and t.name = 'ExamSessionItemResponseTable'
	) FKey
ORDER BY TableName, IndexName, IndexId , ColumnId, ColumnName