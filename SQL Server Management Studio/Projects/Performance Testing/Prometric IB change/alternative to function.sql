IF OBJECT_ID('tempdb..#SectionData') IS NOT NULL DROP TABLE #SectionData;

set statistics io, time on

declare  @totalDuration table ( 
	TotalDuration int not null
	 )

Declare @assessmentId int = 1
declare @durationMode int
declare @assessmentDuration int
declare @assessmentRules xml


select
	@durationMode = DurationMode,
	@assessmentDuration = AssessmentDuration,
	@assessmentRules = AssessmentRules
from AssessmentTable
where ID = @assessmentId

CREATE TABLE #SectionData (
	ID smallint
	,Duration smallint
	,RequiredSections smallint
	,SectionType varchar(100)
	,ParentId smallint
);

INSERT #SectionData	
SELECT	
	[top].[level].value('@ID[1]','int')  ID
	,[top].[level].value('data(@Duration)[1]', 'smallint') Duration
	,[top].[level].value('../@RequiredSections[1]','int') RequiredSections
	,[top].[level].value('data(@SectionType)[1]','varchar(100)') SectionType
	,[top].[level].value('../@ID', 'varchar(50)') ParentId
FROM (VALUES (@assessmentRules)) xx(x)
CROSS APPLY xx.x.nodes('PaperRules//Section') [top]([level]);

SELECT SUM(ISNULL(DurationSection,DurationSectionSelector))
FROM (
  select c.*
	, CASE WHEN DurationSectionSelector IS NULL 
		then 
			ROW_NUMBER() OVER (PARTITION BY Duration, ParentID, ID ORDER BY ID ASC) 
		else
			ROW_NUMBER() OVER (PARTITION BY Duration, ParentID ORDER BY ID ASC) 
		END as 'R'
  FROM (
	select 
		CASE WHEN (a.requiredSections is null and a.parentid is null and a.SectionType = 'Section')
			then a.Duration end as 'DurationSection'
		, a.Duration * a.RequiredSections 'DurationSectionSelector'
		, Duration
		, ParentId
		,ID
	FROM #SectionData a
	) C
) b
where R = 1;
