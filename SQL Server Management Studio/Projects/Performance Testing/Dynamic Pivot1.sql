IF OBJECT_ID('tempdb..#Table') IS NOT NULL DROP TABLE #Table;

DECLARE @Storage TABLE (
	DJCol nvarchar(max)
)

DECLARE @DynamicallyFindTop nvarchar(MAX) = '';

SELECT @DynamicallyFindTop += CHAR(13) + '

select ''Select MAX ('' + C.name + '') AS '' + C.name + '' from '' + O.name +'';''
FROM sys.all_objects as O	
	inner join sys.tables as T on T.object_id = O.object_id	
	inner join sys.columns c ON c.object_id = t.object_id
where o.schema_id = 1
	and o.Type = ''u''
	and user_type_id = ''56'' --system_type_id

'
--INSERT @Storage
--EXEC (@DynamicallyFindTop)


EXECUTE @DynamicallyFindTop

SELECT * FROM  @Storage

DECLARE   @MaxLength INT;

SELECT @MaxLength = (SUM(LEN(Name)) * COUNT(*))
FROM  #Table;

DECLARE @MyString nvarchar(MAX);
SELECT @MyString = 

 SUBSTRING(
       (
              SELECT N',' + QUOTENAME(CAST(Name AS NVARCHAR(500)))
              FROM  #Table
              FOR XML PATH('')
       ), 2, @MaxLength
);


DECLARE @MyDynamicSQL nvarchar(MAX) = '';

Select @MyDynamicSQL +=CHAR(13) + 
	'
		SELECT ''Col'' as MyCols, ' + @MyString + '
		FROM
		(
			SELECT Name
			FROM #Table
					
		) as TEST
		PIVOT
		(
		MAX(Col)
		FOR #Table IN (' + @MyString + ')
		
'

PRINT(@MyDynamicSQL)
		



select 'Select MAX (' + C.name + ') AS ' + C.name + ' from ' + O.name
FROM sys.all_objects as O	
	inner join sys.tables as T on T.object_id = O.object_id	
	inner join sys.columns c ON c.object_id = t.object_id
where o.schema_id = 1
	and o.Type = 'u'
	and user_type_id = '56' --system_type_id





--DECLARE  @ColumnCount  AS  TABLE
--(
--	  rowNum tinyint
--	 , ColName nvarchar(MAX)	    
--)

--INSERT @ColumnCount(rowNum, ColName)
--SELECT row_number() over (ORDER BY c.name) as rowNum
--	, c.name
--FROM sys.all_objects as O	
--	inner join sys.tables as T on T.object_id = O.object_id	
--	inner join sys.columns c ON c.object_id = t.object_id
--where o.schema_id = 1
--	and o.Type = 'u'
--	and user_type_id = '56' --system_type_id
  
 
--DECLARE @ReportColumnNames  AS NVARCHAR(MAX)

--SET @ReportColumnNames = N''

--DECLARE @Interator tinyint =
--							(     
--								SELECT MIN(rowNum)
--								  FROM   @ColumnCount	
--								 )

--WHILE (@Interator IS NOT NULL)

--  BEGIN

--    SET @ReportColumnNames = @ReportColumnNames + N',' + QUOTENAME(CAST(@Interator AS NVARCHAR(max)))
	

--    SET @Interator = (
--						SELECT MIN(rowNum)
--						  FROM   @ColumnCount	
--                          WHERE  rowNum > @Interator
--                          )

--  END

--SET @ReportColumnNames = SUBSTRING(@ReportColumnNames,2,LEN(@ReportColumnNames))

--PRINT @ReportColumnNames
