set statistics io on

DECLARE @myExamSessionId2 int = 1406250

DECLARE @TEMPINSERTTABLE TABLE (
	[ExamSessionId] INT
	,[StructureXml] XML
	,[resultData] XML
	)

INSERT @TEMPINSERTTABLE
SELECT [ID]
	,[structureXml]
	,[resultData]
FROM [ExamSessionTable]
WHERE [ID] IN (
		@myExamSessionId2
		)

DECLARE @Itemresponses table (
	Examsessionid int
    ,ItemID nvarchar(20)
	,ItemVersion smallint
	,ItemResponseData xml   
)

INSERT @ItemResponses
select ExamSessionID
	, ItemID
	, ItemVersion
	, ItemResponseData    
FROM dbo.[ExamSessionItemResponseTable]
where examsessionid IN (
		@myExamSessionId2
		)

--INSERT INTO [ExamSessionTable_ShrededItems]
SELECT @myExamSessionId2
		,ParamValues.Item.value('@id', 'nvarchar(30)')
		,ParamValues.Item.value('@name', 'nvarchar(150)')
		,ParamValues.Item.value('@userMark', 'DECIMAL(6, 3)')
		,ParamValues.Item.value('@markerUserMark', 'nvarchar(max)')
		,CONVERT(DECIMAL(6, 3), resultData.query('data(/exam/@userPercentage)').value('.', 'nvarchar(max)'))
		,ItemVersion
		,ItemResponseData
		,convert(nvarchar(MAX),(ISNULL(ItemResponseData.query('data(//c[@typ=''10'']/i[@sl=''1'']/@ac)'), 'UA')))
		,convert(nvarchar(MAX),(ISNULL(ItemResponseData.query('count(//i[@sl=''1''])'), '0')))
		,convert(nvarchar(MAX),(ISNULL(ItemResponseData.query('count(//i[@ca=''1''])'), '1')))
		,ParamValues.Item.value('@totalMark', 'nvarchar(max)')
	FROM @TEMPINSERTTABLE A
	CROSS APPLY A.StructureXml.nodes('/assessmentDetails/assessment/section/item') AS ParamValues(Item)
	LEFT JOIN @Itemresponses B
	ON A.ExamSessionID = B.Examsessionid
		and B.ItemID = ParamValues.Item.value('@id', 'nvarchar(30)')
	
	
