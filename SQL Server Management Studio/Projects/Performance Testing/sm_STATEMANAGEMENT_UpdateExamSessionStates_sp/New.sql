USE [DI_BC_SecureMarker]
GO
/****** Object:  StoredProcedure [dbo].[sm_STATEMANAGEMENT_UpdateExamSessionStates_sp]    Script Date: 23/07/2015 10:02:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:		Dave Dixon
-- Create date: 26/05/2011
-- Description:	sproc to update the states of the Candidate Exam Versions (sessions)
-- =================================================================================
--CREATE PROCEDURE [dbo].[DJ_sm_STATEMANAGEMENT_UpdateExamSessionStates_sp]

--AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    BEGIN TRY
    
		BEGIN TRANSACTION

			UPDATE dbo.CandidateExamVersions
			SET ExamSessionState = 5
				, PercentageMarkingComplete = CEVView.PercentageMarkingComplete
				, Mark = CEVView.Mark
			FROM dbo.CandidateExamVersions
			INNER JOIN CandidateExamVersionsForStateUpdate_view AS CEVView ON CEVView.cevId = CandidateExamVersions.ID
			WHERE ID IN ( 
					SELECT DISTINCT CEV.ID
					FROM dbo.CandidateExamVersions CEV
						INNER JOIN dbo.CandidateGroupResponses CGR ON CEV.ID = CGR.CandidateExamVersionID
						INNER JOIN dbo.AssignedGroupMarks AGM ON AGM.UniqueGroupResponseId = CGR.UniqueGroupResponseID
					WHERE CEV.ExamSessionState = 4
						AND	AGM.isConfirmedMark = 1
						AND	AGM.[timestamp] > CEV.lastExportDate
				)
			
			IF SCOPE_IDENTITY()  IS NOT NULL 

			INSERT INTO CandidateExamVersionStatuses([Timestamp],[StateID],[CandidateExamVersionID])
			SELECT GETDATE(), 5, SCOPE_IDENTITY() 
		
		COMMIT TRANSACTION
		
	END TRY
        BEGIN CATCH
           
            DECLARE @myErrorNum INT = ERROR_NUMBER()
            DECLARE @myMessage NVARCHAR(MAX) = ERROR_MESSAGE()
            DECLARE @myFullMessage NVARCHAR(MAX) = CONVERT(NVARCHAR(10), @myErrorNum) + ':' + @myMessage
            RAISERROR (@myFullMessage, 16, 1)

			 IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
        END CATCH

END
