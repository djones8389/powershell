use SecureAssess_Populated

set statistics io on

DBCC DROPCLEANBUFFERS
DBCC FREEPROCCACHE

			BEGIN TRANSACTION;
			/*20 seconds, delete 808 rows*/
			DECLARE @isLocalMode BIT = 1
					DELETE 
					FROM dbo.ScheduledExamsTable
					WHERE ID IN
			----DECLARE @ScheduledIDs TABLE (ID INT)					
			----INSERT @ScheduledIDs
			(
						SELECT ScheduledExamsTable.ID as [ScheduledExamId]						
	 						FROM dbo.ScheduledExamsTable
							INNER JOIN dbo.ExamSessionTable
											ON ScheduledExamsTable.ID = ExamSessionTable.ScheduledExamID
							INNER JOIN dbo.ExamStateChangeAuditTable
											ON ExamSessionTable.ID = ExamStateChangeAuditTable.ExamSessionID
							LEFT JOIN dbo.WAREHOUSE_ExamSessionTable
											ON ExamSessionTable.ID = WAREHOUSE_ExamSessionTable.ExamSessionID
						 WHERE ExamStateChangeAuditTable.NewState = 13
						   AND DATEDIFF(MINUTE, ExamStateChangeAuditTable.StateChangeDate, GETDATE()) >= 1440
						   AND (@isLocalMode = 1
								OR (@isLocalMode = 0
								AND WAREHOUSE_ExamSessionTable.ExamSessionID IS NOT NULL)
								)					  
						 GROUP BY ScheduledExamsTable.id
						 HAVING (SELECT COUNT(EST.ID) FROM dbo.ExamSessionTable EST WHERE EST.ScheduledExamID = ScheduledExamsTable.id) = COUNT(ExamSessionTable.ID)
					)

			--DELETE FROM ScheduledExamsTable where ID IN (SELECT ID FROM @ScheduledIDs)

			ROLLBACK TRANSACTION;



/*
Table '#1BFD2C07'. Scan count 0, logical reads 33, physical reads 0, read-ahead reads 0, lob logical reads 0, lob physical reads 0, lob read-ahead reads 0.
Table 'ExamSessionTable'. Scan count 66, logical reads 149, physical reads 7, read-ahead reads 0, lob logical reads 0, lob physical reads 0, lob read-ahead reads 0.
Table 'WAREHOUSE_ExamSessionTable'. Scan count 0, logical reads 66, physical reads 2, read-ahead reads 0, lob logical reads 0, lob physical reads 0, lob read-ahead reads 0.
Table 'ScheduledExamsTable'. Scan count 0, logical reads 66, physical reads 2, read-ahead reads 0, lob logical reads 0, lob physical reads 0, lob read-ahead reads 0.
Table 'ExamStateChangeAuditTable'. Scan count 1, logical reads 2, physical reads 2, read-ahead reads 0, lob logical reads 0, lob physical reads 0, lob read-ahead reads 0.

*/