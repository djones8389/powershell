USE [PPD_AAT_SecureAssess]

--declare @i tinyint = 0;
--while @i < 10
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;	
	SET STATISTICS IO ON

	DECLARE @errorReturnString 	nvarchar(max)
	DECLARE @errorNum		nvarchar(100)
	DECLARE @errorMess		nvarchar(max)
	DECLARE @examSessionId int = 1559085
	DECLARE @originatorID int = 1

	SET NOCOUNT ON;

	BEGIN TRY

		SELECT '0' AS '@errorCode',
		(
			SELECT	
				[WAREHOUSE_ExamSessionTable].ID							AS 		'examInstanceId',
				[WAREHOUSE_ScheduledExamsTable].ExamID					AS		'examId',
				[WAREHOUSE_ScheduledExamsTable].examName				AS		'examName',
				[WAREHOUSE_ExamSessionTable].[WAREHOUSEUserID]			AS		'candidateId',
				REPLACE([WAREHOUSE_UserTable].Forename, '&apos;', '''')	AS		'candidateForename',
				REPLACE([WAREHOUSE_UserTable].Surname, '&apos;', '''')	AS		'candidateSurname',
				[WAREHOUSE_ExamSessionTable].StructureXML.query('assessmentDetails/*')	AS 'assessmentDetails',
				[WAREHOUSE_ExamSessionTable].MarkerData									AS 'markerData',
				[WAREHOUSE_UserTable].SpecialRequirements								AS 'specialRequirements',
				[WAREHOUSE_UserTable].MiddleName										AS 'candidateMiddleName',
				[WAREHOUSE_UserTable].DOB												AS 'candidateDOB',
				[WAREHOUSE_UserTable].Gender											AS 'gender',
				[WAREHOUSE_UserTable].AddressLine1										AS 'candidateAddressLine1',
				[WAREHOUSE_UserTable].AddressLine2										AS 'candidateAddressLine2',
				[WAREHOUSE_UserTable].Town												AS 'candidateTown',
				[WAREHOUSE_UserTable].PostCode											AS 'candidatePostcode',
				[WAREHOUSE_UserTable].email												AS 'candidateEmail',
				[WAREHOUSE_UserTable].Telephone											AS 'candidateTelephone',
				[WAREHOUSE_UserTable].County											AS 'candidateCounty',
				[WAREHOUSE_UserTable].Country											AS 'candidateCountry',
				[WAREHOUSE_UserTable].EthnicOrigin										AS 'candidateEthnicOrigin',
				CAST([WAREHOUSE_ExamSessionTable].StructureXML AS XML).query('requiresValidation//*')AS 'requiredResultsRelease',
				[WAREHOUSE_ExamSessionTable].ExamStateChangeAuditXML.query('exam/*')	AS 'stateChanges',
				dbo.sp_wareHouse_GetExamSessionAudit([WAREHOUSE_ExamSessionTable].[ID]).query('ExamDurationAudit/*')   AS 'ExamDurationAudit',
				dbo.fn_getMarkersasxml([WAREHOUSE_ExamSessionTable].[ID]).query('//marker')		AS 'markers',
				dbo.fn_getResultsReleaseUser([WAREHOUSE_ExamSessionTable].[ID]).query('//user/*')	AS 'resultsReleaseUser',
				[WAREHOUSE_ExamSessionTable].EnableOverrideMarking AS 'EnableOverrideMarking',
				[WAREHOUSE_ExamSessionTable].IsProjectBased AS 'isProjectBased'
			FROM [WAREHOUSE_ExamSessionTable]
			INNER JOIN [WAREHOUSE_ScheduledExamsTable] ON
			[WAREHOUSE_ScheduledExamsTable].ID = [WAREHOUSE_ExamSessionTable].[WAREHOUSEScheduledExamID]
			INNER JOIN [WAREHOUSE_UserTable] ON
				[WAREHOUSE_ExamSessionTable].[WAREHOUSEUserID] = [WAREHOUSE_UserTable].ID
			WHERE [WAREHOUSE_ExamSessionTable].ID =  @examSessionId	
			AND [WAREHOUSE_ScheduledExamsTable].[OriginatorId] = @originatorId
			FOR XML PATH ('examScript'), ROOT('return'), TYPE
		)
		FOR XML PATH ('result')

	END TRY
	BEGIN CATCH
		SET @errorNum = (SELECT ERROR_NUMBER() AS ErrorNumber)
		SET @errorMess = (SELECT ERROR_MESSAGE() AS ErrorMessage)
		SET @errorReturnString = '<result errorCode="2"><return>SQL Error, Number: ' + @errorNum + ' MESSAGE: ' + @errorMess + '</return></result>'
		SELECT @errorReturnString
	END CATCH

	--SET @i = @i+1
END