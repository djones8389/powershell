USE SANDBOX_BritishCouncil_SecureMarker
GO
/****** Object:  StoredProcedure [dbo].[sm_STATEMANAGEMENT_UpdateExamSessionStates_sp]    Script Date: 01/07/2016 15:44:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

SET STATISTICS IO ON
--ALTER PROCEDURE [dbo].[sm_STATEMANAGEMENT_UpdateExamSessionStates_sp]
-- Add the parameters for the stored procedure here
DECLARE
	@examSessionState INT= 2
--AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @cevs TABLE(
            cevId INT
           ,ExamSessionState INT
           ,PercentageMarkingComplete INT
           ,Mark DECIMAL(18 ,10)
        )
    DECLARE @date DATETIME
    
    BEGIN TRY
    
		IF @examSessionState = 5
		BEGIN
			BEGIN TRAN
				-- calculate if any 'Result Pending' exam sessions have had items remarked
				-- check to see if they've had any items marked more recently than when the result was exported
				INSERT INTO @cevs(
					cevId		
				)	
				SELECT DISTINCT CEV.ID
				FROM dbo.CandidateExamVersions CEV WITH(NOLOCK)
					INNER JOIN dbo.CandidateGroupResponses CGR WITH(NOLOCK) ON CEV.ID = CGR.CandidateExamVersionID
					INNER JOIN dbo.AssignedGroupMarks AGM WITH(NOLOCK) ON AGM.UniqueGroupResponseId = CGR.UniqueGroupResponseID
				WHERE CEV.ExamSessionState = 4
					AND	AGM.isConfirmedMark = 1
					AND	AGM.[timestamp] > CEV.lastExportDate

				UPDATE dbo.CandidateExamVersions
					SET ExamSessionState = 5,
					[LastManagedDate] = GETDATE()
				WHERE ID IN (SELECT cevId FROM @cevs)
	
				SET @date = GETDATE()

				INSERT INTO CandidateExamVersionStatuses
				(
					-- ID -- this column value is auto-generated
					[Timestamp],
					StateID,
					CandidateExamVersionID
				)
				SELECT @date, 5, cevId FROM @cevs

				DELETE @cevs
			COMMIT TRAN
		END

		-- normal state progression		
		BEGIN TRAN
			
			DECLARE @cevUpdate TABLE(cevId INT, ExamSessionState INT)
			INSERT INTO @cevUpdate(cevId, ExamSessionState)
			SELECT TOP 500 CV.ID, CV.ExamSessionState FROM dbo.ScriptToUpdate_view CV WITH(NOLOCK) WHERE CV.ExamSessionState = @examSessionState ORDER BY CV.LastManagedDate
						
			;WITH ScriptMarks_CTE AS
			(SELECT CEV.cevId AS 'ID'
				,ExamSessionState	AS 'ExamSessionState'
				,COUNT(NULLIF(UGR.confirmedMark, NULL))*100/COUNT(UGR.Id) AS 'PercentageMarkingComplete'
				,ISNULL(SUM(ISNULL(m.[Mark], UGRL.confirmedMark)), 0) as 'CurrentMark'
			FROM @cevUpdate CEV
				INNER JOIN dbo.CandidateGroupResponses CGR WITH(NOLOCK) ON CGR.CandidateExamVersionID = CEV.cevId
				INNER JOIN dbo.UniqueGroupResponses UGR WITH(NOLOCK) on UGR.id = CGR.UniqueGroupResponseID							
				INNER JOIN dbo.ActiveGroups_view ACT WITH(NOLOCK) ON  ACT.GroupDefinitionID = UGR.GroupDefinitionID
				INNER JOIN dbo.UniqueGroupResponseLinks UGRL WITH(NOLOCK) ON  UGRL.UniqueGroupResponseId = UGR.ID
				INNER JOIN [dbo].[CandidateExamVersionGroups_view] CEVG WITH(NOLOCK) 
					ON CEVG.CandidateExamVersionID = CEV.cevId 
					AND CEVG.GroupDefinitionID = UGR.GroupDefinitionID
				LEFT JOIN dbo.Moderations M WITH(NOLOCK) ON  M.UniqueResponseId = UGRL.UniqueResponseId AND M.CandidateExamVersionID = CEV.cevId AND m.IsActual = 1
				
			GROUP BY CEV.cevId, CEV.ExamSessionState),
			LiveStates_CTE AS
			(
			SELECT	ScriptMarks_CTE.ID, LiveState =
				CASE 
					WHEN (PercentageMarkingComplete = 0 AND ExamSessionState NOT IN (4)) THEN 1
					WHEN (PercentageMarkingComplete = 0 AND ExamSessionState = 4) THEN 5
					WHEN (PercentageMarkingComplete > 0 AND PercentageMarkingComplete < 100 AND ExamSessionState NOT IN (4,5)) THEN 2
					WHEN (PercentageMarkingComplete > 0 AND PercentageMarkingComplete < 100 AND ExamSessionState = 4) THEN 5
					WHEN (PercentageMarkingComplete = 100 AND ExamSessionState != 4) THEN 3
					WHEN (PercentageMarkingComplete = 100 AND ExamSessionState = 4) THEN 4
					WHEN (PercentageMarkingComplete < 100 AND ExamSessionState = 5) THEN 2
				END 
				,PercentageMarkingComplete
				,ScriptMarks_CTE.CurrentMark
			FROM ScriptMarks_CTE)

			INSERT INTO @cevs(
				cevId
			   ,ExamSessionState
			   ,PercentageMarkingComplete
			   ,Mark	
			)
			SELECT
			CEV0.ID as CevId,
			Y.LiveState as ExamSessionState, 
			ROUND(Y.PercentageMarkingComplete,0) as PercentageMarkingComplete,
			Y.CurrentMark as Mark		
			FROM dbo.CandidateExamVersions CEV0 WITH(NOLOCK)
			JOIN LiveStates_CTE AS Y ON Y.ID = CEV0.ID
			WHERE 
				(
					CEV0.ExamSessionState != Y.LiveState 
					OR ISNULL(CEV0.PercentageMarkingComplete,0) != ROUND(Y.PercentageMarkingComplete,0)
					OR  (Y.LiveState IN (3, 4) AND ROUND(Y.PercentageMarkingComplete, 0) = 100 AND Y.CurrentMark != CEV0.Mark)
				)
				AND
				--The state should never be set to NULL
				Y.LiveState IS NOT NULL	
						
			SET @date = GETDATE()
				
			INSERT INTO CandidateExamVersionStatuses
			(
				-- ID -- this column value is auto-generated
				[Timestamp],
				StateID,
				CandidateExamVersionID
			)
			SELECT @date, cevs.ExamSessionState, cevs.cevId
			FROM @cevs AS cevs
			INNER JOIN CandidateExamVersions cev WITH(NOLOCK) ON cevs.cevId = cev.ID
			WHERE cevs.ExamSessionState <> cev.ExamSessionState
		
			UPDATE CEV
			SET [LastManagedDate] = GETDATE()
			FROM CandidateExamVersions AS CEV
			INNER JOIN @cevUpdate AS cevs ON cevs.cevId = CEV.ID

			UPDATE CEV
			SET
				ExamSessionState = cevs.ExamSessionState, 		
				PercentageMarkingComplete = cevs.PercentageMarkingComplete,
				[Mark] = cevs.[Mark],
				[LastManagedDate] = GETDATE()
			FROM CandidateExamVersions AS CEV
			INNER JOIN @cevs AS cevs ON cevs.cevId = CEV.ID
		
			SELECT @@ROWCOUNT
		
		COMMIT TRAN
		
	END TRY
    BEGIN CATCH
        IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
        DECLARE @myErrorNum INT = ERROR_NUMBER()
        DECLARE @myMessage NVARCHAR(MAX) = ERROR_MESSAGE()
        DECLARE @myFullMessage NVARCHAR(MAX) = CONVERT(NVARCHAR(10), @myErrorNum) + ':' + @myMessage
        RAISERROR (@myFullMessage, 16, 1)
    END CATCH
END
