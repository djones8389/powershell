DECLARE
       @examSessionState INT = 2

 
		DECLARE @cevUpdate TABLE(cevId INT, ExamSessionState INT, PercentageMarkingComplete float, currentMark decimal(10,3))
		INSERT INTO @cevUpdate(cevId, ExamSessionState,PercentageMarkingComplete,currentMark)
		select ID
			, ExamSessionState
			, PercentageMarkingComplete
			, CurrentMark
		FROM (
		SELECT TOP 500
			CEV.ID AS 'ID'
			, CASE 
					WHEN (PercentageMarkingComplete = 0 AND ExamSessionState NOT IN (4)) THEN 1
					WHEN (PercentageMarkingComplete = 0 AND ExamSessionState = 4) THEN 5
					WHEN (PercentageMarkingComplete > 0 AND PercentageMarkingComplete < 100 AND ExamSessionState NOT IN (4,5)) THEN 2
					WHEN (PercentageMarkingComplete > 0 AND PercentageMarkingComplete < 100 AND ExamSessionState = 4) THEN 5
					WHEN (PercentageMarkingComplete = 100 AND ExamSessionState != 4) THEN 3
					WHEN (PercentageMarkingComplete = 100 AND ExamSessionState = 4) THEN 4
					WHEN (PercentageMarkingComplete < 100 AND ExamSessionState = 5) THEN 2
				END AS ExamSessionState    
			, LastManagedDate
			,COUNT(NULLIF(UGR.confirmedMark, NULL))*100/COUNT(UGR.Id) AS 'PercentageMarkingComplete'
			,ISNULL(SUM(ISNULL(m.[Mark], UGRL.confirmedMark)), 0) as 'CurrentMark'
					   
		FROM CandidateExamVersions CEV  WITH (INDEX (IX_StateManagement))
		INNER JOIN CandidateGroupResponses CGR 
		ON CGR.CandidateExamVersionID = CEV.ID
		INNER JOIN UniqueGroupResponses UGR
		on UGR.ID =CGR.UniqueGroupResponseID
		INNER JOIN [dbo].[CandidateExamVersionGroups_view] CEVG WITH(NOLOCK) 
					ON CEVG.CandidateExamVersionID = CEV.ID 
					AND CEVG.GroupDefinitionID = UGR.GroupDefinitionID
		--INNER JOIN (
		--	SELECT DISTINCT
		--		UniqueGroupResponses.ID
		--		,UniqueGroupResponses.confirmedMark
		--		,CandidateGroupResponses.CandidateExamVersionID
		--		, CandidateGroupResponses.UniqueGroupResponseID
		--		, UniqueGroupResponses.GroupDefinitionID
		--	FROM dbo.CandidateGroupResponses 
		--	INNER JOIN dbo.UniqueGroupResponses 
		--	ON UniqueGroupResponses.ID = CandidateGroupResponses.UniqueGroupResponseID
		--	) CGR 
		--	ON CGR.CandidateExamVersionID = CEV.ID					                                           
		INNER JOIN dbo.ActiveGroups_view ACT WITH(NOLOCK) ON  ACT.GroupDefinitionID = UGR.GroupDefinitionID
		INNER JOIN dbo.UniqueGroupResponseLinks UGRL WITH(NOLOCK) ON  UGRL.UniqueGroupResponseId = UGR.ID
		LEFT JOIN dbo.Moderations M WITH(NOLOCK) ON  M.UniqueResponseId = UGRL.UniqueResponseId AND M.CandidateExamVersionID = CEV.ID AND m.IsActual = 1					   
		where ExamSessionState = @examSessionState  
		GROUP BY CEV.ID, CEV.ExamSessionState, CEV.LastManagedDate, PercentageMarkingComplete
		ORDER BY CEV.LastManagedDate ASC
		) ScriptMarks_CTE


		
DECLARE
       @examSessionState INT = 2

				
			DECLARE @cevUpdate TABLE(cevId INT, ExamSessionState INT)
			INSERT INTO @cevUpdate(cevId, ExamSessionState)
			SELECT TOP 500 CV.ID
				, CV.ExamSessionState 
			FROM CandidateExamVersions CV  WITH (INDEX (IX_StateManagement))
			WHERE CV.ExamSessionState = @examSessionState
			ORDER BY CV.LastManagedDate
						
			--;WITH ScriptMarks_CTE AS
			--(
			SELECT CEV.cevId AS 'ID'
				,ExamSessionState	AS 'ExamSessionState'
				,COUNT(NULLIF(A.confirmedMark, NULL))*100/COUNT([UGRID]) AS 'PercentageMarkingComplete'
				--,ISNULL(SUM(ISNULL(m.[Mark], UGRL.confirmedMark)), 0) as 'CurrentMark'
			FROM @cevUpdate CEV

			INNER JOIN (
				SELECT CandidateGroupResponses.CandidateExamVersionID
					 ,UniqueGroupResponses.ID [UGRID]
					 ,UniqueGroupResponses.confirmedMark
					 ,UniqueGroupResponses.GroupDefinitionID
				FROM dbo.CandidateGroupResponses
				INNER JOIN dbo.UniqueGroupResponses ON UniqueGroupResponses.ID = CandidateGroupResponses.UniqueGroupResponseID
				INNER JOIN dbo.UniqueGroupResponseLinks ON  UniqueGroupResponseLinks.UniqueGroupResponseId = UniqueGroupResponses.ID
			    INNER JOIN GroupDefinitionStructureCrossRef ON GroupDefinitionStructureCrossRef.GroupDefinitionID = UniqueGroupResponses.GroupDefinitionID
				INNER JOIN dbo.ExamVersionStructures ON ExamVersionStructures.ID = GroupDefinitionStructureCrossRef.ExamVersionStructureID
				WHERE  GroupDefinitionStructureCrossRef.Status = 0
						   AND ExamVersionStructures.StatusID = 0 
			) A
			 ON A.CandidateExamVersionID = CEV.cevId
				--INNER JOIN dbo.CandidateGroupResponses CGR WITH(NOLOCK) ON CGR.CandidateExamVersionID = CEV.cevId
				--INNER JOIN dbo.UniqueGroupResponses UGR WITH(NOLOCK) on UGR.id = CGR.UniqueGroupResponseID							
				--INNER JOIN (
				--	SELECT GDSCR.GroupDefinitionID
				--	FROM   dbo.GroupDefinitionStructureCrossRef GDSCR
				--	INNER JOIN dbo.ExamVersionStructures EVS
				--	ON  EVS.ID = GDSCR.ExamVersionStructureID
				--	WHERE  GDSCR.Status = 0
				--		   AND EVS.StatusID = 0 
				--	GROUP BY GDSCR.GroupDefinitionID
				--	)
				--	 ACT ON  ACT.GroupDefinitionID = A.GroupDefinitionID
						
				--INNER JOIN dbo.UniqueGroupResponseLinks UGRL WITH(NOLOCK) ON  UGRL.UniqueGroupResponseId = A.[UGRID]
					----INNER JOIN (
					----		SELECT  UGR2.GroupDefinitionID
					----			, CGR2.CandidateExamVersionID
					----		FROM dbo.CandidateGroupResponses CGR2
					----			INNER JOIN dbo.UniqueGroupResponses UGR2 ON UGR2.ID = CGR2.UniqueGroupResponseID
					----		GROUP BY UGR2.GroupDefinitionID, CGR2.CandidateExamVersionID
					----) CEVG 
					----ON CEVG.CandidateExamVersionID = CEV.cevId 
					----	AND CEVG.GroupDefinitionID = A.GroupDefinitionID
		
					--LEFT JOIN dbo.Moderations M WITH(NOLOCK) ON  M.UniqueResponseId = UGRL.UniqueResponseId AND M.CandidateExamVersionID = CEV.cevId AND m.IsActual = 1
				
			GROUP BY CEV.cevId, CEV.ExamSessionState
			--)

			--SELECT * FROM ScriptMarks_CTE






		
DECLARE
       @examSessionState INT = 2

			DECLARE @cevUpdate TABLE(cevId INT, ExamSessionState INT)
			INSERT INTO @cevUpdate(cevId, ExamSessionState)
			SELECT TOP 500 CV.ID, CV.ExamSessionState FROM dbo.ScriptToUpdate_view CV WITH(NOLOCK) WHERE CV.ExamSessionState = @examSessionState ORDER BY CV.LastManagedDate
						
			SELECT CEV.cevId AS 'ID'
				,ExamSessionState	AS 'ExamSessionState'
				,COUNT(NULLIF(UGR.confirmedMark, NULL))*100/COUNT(UGR.Id) AS 'PercentageMarkingComplete'
				,ISNULL(SUM(ISNULL(m.[Mark], UGRL.confirmedMark)), 0) as 'CurrentMark'
			FROM @cevUpdate CEV
				INNER JOIN dbo.CandidateGroupResponses CGR WITH(NOLOCK) ON CGR.CandidateExamVersionID = CEV.cevId
				INNER JOIN dbo.UniqueGroupResponses UGR WITH(NOLOCK) on UGR.id = CGR.UniqueGroupResponseID		
				--INNER JOIN GroupDefinitionStructureCrossRef ON GroupDefinitionStructureCrossRef.GroupDefinitionID = UGR.GroupDefinitionID
				--INNER JOIN ExamVersionStructures ON ExamVersionStructures.ID = GroupDefinitionStructureCrossRef.ExamVersionStructureID				
									
				INNER JOIN dbo.ActiveGroups_view ACT WITH(NOLOCK) ON  ACT.GroupDefinitionID = UGR.GroupDefinitionID
				INNER JOIN dbo.UniqueGroupResponseLinks UGRL WITH(NOLOCK) ON  UGRL.UniqueGroupResponseId = UGR.ID
				--INNER JOIN [dbo].[CandidateExamVersionGroups_view] CEVG WITH(NOLOCK) 
				--	ON CEVG.CandidateExamVersionID = CEV.cevId 
				--	AND CEVG.GroupDefinitionID = UGR.GroupDefinitionID
				LEFT JOIN dbo.Moderations M WITH(NOLOCK) ON  M.UniqueResponseId = UGRL.UniqueResponseId AND M.CandidateExamVersionID = CEV.cevId AND m.IsActual = 1
			--WHERE  GroupDefinitionStructureCrossRef.Status = 0 -- 0 = Active
			--   AND ExamVersionStructures.StatusID = 0 -- 0 = Released
			GROUP BY CEV.cevId, CEV.ExamSessionState