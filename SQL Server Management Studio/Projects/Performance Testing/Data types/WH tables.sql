
SELECT examsessionid
	, SUM(DATALENGTH(county) +  DATALENGTH(country))
	, SUM(DATALENGTH(county) +  DATALENGTH(country)) - 4
from WAREHOUSE_ExamSessionTable_Shreded NOLOCK
where county IS NOT NULL AND country IS NOT NULL
group by examsessionid


select sum(totalwastedspaceperexam)/1024/1024
FROM (
SELECT  examsessionid
, SUM(DATALENGTH(county) +  DATALENGTH(country)) [totalspaceused]
,	SUM(DATALENGTH(county) +  DATALENGTH(country)) - 4 [totalwastedspaceperexam] --size of a smallint + a smallint, both columns
from WAREHOUSE_ExamSessionTable_Shreded NOLOCK
where county IS NOT NULL AND country IS NOT NULL
group by examsessionid

) A

select MAX([selectedCount]), MAX([correctAnswerCount]),MAX([markedMetadataCount]),MAX([ItemType]),MAX([ItemVersion])
from [dbo].[WAREHOUSE_ExamSessionTable_ShrededItems]

/*MAX([selectedCount]), MAX([correctAnswerCount]),MAX([markedMetadataCount]),MAX([ItemType]),MAX([ItemVersion])
32						14							20						3				156

6						6							0						3				135

20987553 rows
4 cols = 4 bytes each.  16 bytes per row

335800848 bytes for these 4 cols @int


20987553 rows
4 cols = 1 bytes each.  4 bytes per row

83950212 bytes for these 4 cols @tinyint


difference = 251850636 = 240MB

*/