USE [PRV_AAT_SecureAssess]
GO
   
BEGIN      
	SET NOCOUNT ON;      


	BEGIN TRY      
	
		DECLARE @spReturnString    NVARCHAR(MAX)        
		DECLARE @ID INT    
    
		SELECT @ID = ID 
			FROM dbo.ExamSessionTable 
			WHERE KeyCode  = 'HG6EMFA6' --@keycode   
			And examState = 15 -- Awaiting Marking  
    
		IF @ID IS NOT NULL       
		
			SELECT '0' as '@errorCode', --Attribute to our root (btl standard 'result') node      
			(      
				SELECT     
				(      
					SELECT     
						dbo.ExamSessionTable.ID            AS 'examInstanceId',      
						dbo.ExamSessionTable.examState          AS 'examInstanceStatus',      
						dbo.ScheduledExamsTable.examId          AS 'examId',       
						dbo.ScheduledExamsTable.examName         AS 'examName',      
						dbo.ScheduledExamsTable.CentreId         AS 'centreId',      
						REPLACE(dbo.CentreTable.CentreName  ,'&','&amp;')     AS 'centreName',           
						dbo.ScheduledExamsTable.ScheduledStartDateTime      AS 'startDate',      
						dbo.ScheduledExamsTable.ScheduledEndDateTime      AS 'endDate',      
						dbo.ScheduledExamsTable.ActiveStartTime        AS 'startTime',      
						dbo.ScheduledExamsTable.ActiveEndTime        AS 'endTime',      
						REPLACE(dbo.UserTable.Forename, '&apos;', '''')      AS 'candidateForename',        
						REPLACE(dbo.UserTable.Surname, '&apos;', '''')      AS 'candidateSurname',      
						REPLACE(dbo.IB3QualificationLookup.QualificationName ,'&','&amp;')  AS 'qualification',                     
						dbo.IB3QualificationLookup.ID AS qualId,
						Convert(datetime,dbo.ExamStateChangeAuditTable.StateChangeDate,113) AS 'completedDate',
						dbo.ExamSessionTable.EnableOverrideMarking AS  'EnableOverrideMarking',
						(dbo.fn_GetUserRelationForExamSession(dbo.ExamSessionTable.ID, 1)) AS userAssociation,
						dbo.GetExamSessionUserAssociations(dbo.ExamSessionTable.ID)
					FROM [dbo].[ExamSessionTable]      
					INNER JOIN  dbo.ExamStateChangeAuditTable ON (dbo.ExamStateChangeAuditTable.ExamSessionID = dbo.ExamSessionTable.ID AND dbo.ExamStateChangeAuditTable.NewState = 9)      
					INNER JOIN  dbo.ScheduledExamsTable ON dbo.ScheduledExamsTable.ID = dbo.ExamSessionTable.ScheduledExamID       
					INNER JOIN  dbo.UserTable ON dbo.ExamSessionTable.UserID = dbo.UserTable.ID      
					INNER JOIN  dbo.CentreTable ON dbo.ScheduledExamsTable.CentreID = dbo.CentreTable.ID      
					INNER JOIN  dbo.IB3QualificationLookup ON dbo.IB3QualificationLookup.ID = dbo.ScheduledExamsTable.qualificationId     

					WHERE dbo.ExamSessionTable.ID = @ID

					FOR XML PATH('exam') ,TYPE           
				)      
			FOR XML PATH('return') ,TYPE       
			)      
			FOR XML PATH('result')      
		ELSE    
		BEGIN    
			SET @spReturnString = '<result errorCode="4"><return>Invalid keycode</return></result>'         
			SELECT @spReturnString    
		END           
	END TRY      
	BEGIN CATCH      
		EXEC sa_SHARED_GetErrorDetails_sp      
	END CATCH;       
END

