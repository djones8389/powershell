SET STATISTICS IO ON

DECLARE  @examInstanceId  int = 4684,--906939,      
		 @OriginatorID  int = 1,      
		 @returnString  nvarchar(max),
		 @mySAScheduledExamId int
		, @mySACentreId int
		, @mySACreatedById int
		, @mySAUserId int
		, @myKeyCode nvarchar(12)      
		, @AvailableForCentreReview bit -- Centre Direct     
		, @ResultDataFull xml
		, @TodaysDate nvarchar(50)
		, @mySACentreVersion int      
		, @mySACreatedByIdVersion int
		, @mySAUserIdVersion int   
		, @myWarehouseExamState INT
		, @ValidationCount INT
		, @completionDate smalldatetime 
		, @qualificationid int
		, @qualificationName nvarchar(100)
		, @AssignedMarkerID		int
		, @AssignedModeratorID		int
		, @MarkerUserName		nvarchar(max)
		, @ModeratorUserName		nvarchar(max)
		, @UserVer int

	
  SELECT     
	   @AssignedMarkerID = (SELECT UserId FROM UserSessionRelationTable WHERE ExamSessionId = @examInstanceId AND RelationTypeId = 1)
		
  FROM ExamSessionTable    
    
  INNER JOIN ScheduledExamsTable 
  ON ScheduledExamsTable.ID = ExamSessionTable.ScheduledExamID      
  Inner join CentreTable
  ON CentreTable.ID = ScheduledExamsTable.CentreID
  INNER JOIN UserTable
  on UserTable.ID = ExamSessionTable.UserID
  INNER Join IB3QualificationLookup
  on IB3QualificationLookup.ID = ScheduledExamsTable.qualificationID
  LEFT JOIN dbo.UserSessionRelationTable
  ON UserSessionRelationTable.ExamSessionId = @examInstanceId
  WHERE ExamSessionTable.ID = @examInstanceId;
  
SELECT @AssignedMarkerID

  
DECLARE @WarehouseUserTableInsert TABLE
(
	ID INT
); 


	INSERT @WarehouseUserTableInsert
	SELECT @mySAScheduledExamId;
	
	INSERT @WarehouseUserTableInsert
	SELECT @mySACreatedById;
	
	INSERT @WarehouseUserTableInsert
	SELECT @mySAUserId;
	
	INSERT @WarehouseUserTableInsert
	SELECT distinct a.b.value('(.)[1]','int') as markerID
	FROM ExamSessionItemResponseTable (NOLOCK)
	inner join ExamSessionTable
	on ExamSessionTable.ID = ExamSessionItemResponseTable.ExamSessionID
	CROSS APPLY [MarkerResponseData].nodes('/entries/entry/userId') a(b)
	WHERE ExamSessionID = @examInstanceId;
	
	INSERT @WarehouseUserTableInsert
	SELECT UserID
	FROM ExamSessionDurationAuditTable as ESD
	INNER JOIN UserTable as UT
	on UT.ID = ESD.UserID
	WHERE ESD.ExamSessionID = @examInstanceId;    
	
	
BEGIN   
	
     --Insert new user records where required      
		 INSERT [dbo].[WAREHOUSE_UserTable]      
			 ([UserId]      
			 ,[CandidateRef]      
			 ,[Forename]      
			 ,[Surname]      
			 ,[Middlename]      
			 ,[DOB]      
			 ,[Gender]      
			 ,[SpecialRequirements]      
			 ,[AddressLine1]      
			 ,[AddressLine2]      
			 ,[Town]      
			 ,[County]      
			 ,[Country]      
			 ,[PostCode]      
			 ,[Telephone]      
			 ,[Email]      
			 ,[EthnicOrigin]      
			 ,[AccountCreationDate]      
			 ,[AccountExpiryDate]      
			 ,[ExtraInfo]      
			 ,[version]      
			 ,[OriginatorID]    
			 ,[ULN]
			 ,[Username])      
		 SELECT      
		   [UserTable].[ID]      
		   ,[UserTable].[CandidateRef]      
		   ,[UserTable].[Forename]      
		   ,[UserTable].[Surname]      
		   ,[UserTable].[Middlename]      
		   ,[UserTable].[DOB]      
		   ,[UserTable].[Gender]      
		   ,[UserTable].[SpecialRequirements]      
		   ,[UserTable].[AddressLine1]      
		   ,[UserTable].[AddressLine2]      
		   ,[UserTable].[Town]      
		   ,[CountyLookupTable].[County]      
		   ,[CountryLookupTable].[Country]      
		   ,[UserTable].[PostCode]      
		   ,[UserTable].[Telephone]      
		   ,[UserTable].[Email]      
		   ,[EnthnicOriginLookupTable].[Name]      
		   ,[UserTable].[AccountCreationDate]      
		   ,[UserTable].[AccountExpiryDate]      
		   ,[UserTable].[ExtraInfo]      
		   ,[UserTable].[version]      
		   ,@OriginatorID     
		   ,[UserTable].[ULN] 
		   ,[UserTable].[Username]   
		 FROM [dbo].[UserTable]
				LEFT JOIN dbo.WAREHOUSE_UserTable
				ON UserTable.ID = WAREHOUSE_UserTable.UserID
					AND UserTable.Version >= WAREHOUSE_UserTable.Version
				INNER JOIN dbo.EnthnicOriginLookupTable
				ON UserTable.EthnicOriginID = EnthnicOriginLookupTable.ID
				INNER JOIN dbo.CountyLookupTable
				ON UserTable.County = CountyLookupTable.ID
				INNER JOIN dbo.CountryLookupTable
				ON UserTable.Country = CountryLookupTable.ID
				WHERE UserTable.ID in (SELECT ID FROM @WarehouseUserTableInsert)
				AND WAREHOUSE_UserTable.ID IS NULL

    END      
  







/*


--SA User who scheduled the exam

	DECLARE @MyMaxWHScheduledUserVersion int 

Select 1
FROM

(	
	SELECT  MAX(UT.version) as Live,  MAX(WUT.version) as WH
	FROM UserTable as UT
	LEFT JOIN WAREHOUSE_UserTable as WUT
	on UT.ID = WUT.UserId
	WHERE UT.ID = @mySACreatedById
)  C	 
   
WHERE Live > WH


	      
    DECLARE @MaxWHScheduledUserVersion int = ( 
	SELECT MAX(version)
	FROM WAREHOUSE_UserTable    
	WHERE UserId = @mySACreatedById      
	AND OriginatorID = @OriginatorID   
	)

	IF  ISNULL(@MaxWHScheduledUserVersion, 0) <= @mySACreatedByIdVersion
	AND
	NOT EXISTS ( SELECT 1 
		  FROM WAREHOUSE_UserTable      
		  WHERE UserId = @mySACreatedById      
		  AND OriginatorID = @OriginatorID
		  AND WAREHOUSE_UserTable.version = @MaxWHScheduledUserVersion
	)
	      
	INSERT @WarehouseUserTableInsert(SACreatedByUser, InsertNeeded)
	VALUES (@mySACreatedById,'1')

--SA Candidate who sat the exam

	DECLARE @MaxWHCandidateUserVersion int = ( 
	SELECT MAX(version)
	FROM WAREHOUSE_UserTable    
	WHERE UserId = @mySAUserId      
	AND OriginatorID = @OriginatorID   
	)

	IF  ISNULL(@MaxWHCandidateUserVersion, 0) <= @mySAUserIdVersion
	AND
	NOT EXISTS ( SELECT 1 
		  FROM WAREHOUSE_UserTable      
		  WHERE UserId = @mySAUserId      
		  AND OriginatorID = @OriginatorID
		  AND WAREHOUSE_UserTable.version = @MaxWHCandidateUserVersion
	)
	    
	INSERT @WarehouseUserTableInsert(SACandidateID, InsertNeeded)
	VALUES (@mySAUserId,'1')
      

--SA Marker User Data

     /*FIX for bug id 2560; Add the marker into the warehouse user table if they       
     a; dont exist or      
     b; have been updated since their last entry*/   
     
	DECLARE @markerUser TABLE (markerID int, Version int)	
	INSERT @markerUser(markerID, Version)
	SELECT distinct a.b.value('(.)[1]','int')
		  , (select version from UserTable where ID = a.b.value('(.)[1]','int'))
	FROM ExamSessionItemResponseTable (NOLOCK)
	inner join ExamSessionTable
	on ExamSessionTable.ID = ExamSessionItemResponseTable.ExamSessionID
	CROSS APPLY [MarkerResponseData].nodes('/entries/entry/userId') a(b)
	WHERE ExamSessionID = @examInstanceId
	  

	IF NOT EXISTS (
		
		select 1
		FROM WAREHOUSE_UserTable
		
		INNER JOIN @markerUser A
		on A.markerID = WAREHOUSE_UserTable.UserId
		
		where A.Version = WAREHOUSE_UserTable.version

	)

	INSERT @WarehouseUserTableInsert(SAMarkerUser, InsertNeeded)
	select markerID, '1' from @markerUser

--WarehouseUserID's for ExamSessionDurationAuditTable

    DECLARE @myAuditUser TABLE (auditID int, version int)
	INSERT @myAuditUser(auditID, Version)
	SELECT UserID, Version
	FROM ExamSessionDurationAuditTable as ESD
	INNER JOIN UserTable as UT
	on UT.ID = ESD.UserID
	WHERE ESD.ExamSessionID = @examInstanceId      
	  

	IF NOT EXISTS (
		
		select 1
		FROM WAREHOUSE_UserTable		
		INNER JOIN @myAuditUser A
		on A.auditID = WAREHOUSE_UserTable.UserId		
		where A.Version = WAREHOUSE_UserTable.version
	)
	
	INSERT @WarehouseUserTableInsert(AuditUser, InsertNeeded)
	select auditID, '1' from @myAuditUser
	
--WarehouseUserID's for ExamSessionDocumentInfoTable
	
    DECLARE @myDocumentUser TABLE (documentUserID int, version int)
	INSERT @myDocumentUser(documentUserID, Version)
	SELECT UserID, Version
	FROM ExamSessionDurationAuditTable as ESD
	INNER JOIN UserTable as UT
	on UT.ID = ESD.UserID
	WHERE ESD.ExamSessionID = @examInstanceId      
	  

	IF NOT EXISTS (
		
		select 1
		FROM WAREHOUSE_UserTable		
		INNER JOIN @myDocumentUser A
		on A.documentUserID = WAREHOUSE_UserTable.UserId		
		where A.Version = WAREHOUSE_UserTable.version
	)
	
	INSERT @WarehouseUserTableInsert(documentUserID, InsertNeeded)
	select documentUserID, '1' from @myDocumentUser
*/
	





/*
      
SELECT ESD.*  FROM ExamSessionDurationAuditTable as ESD
INNER JOIN ExamSessionTable as EST
on EST.ID = ESD.ExamSessionID
INNER JOIN UserTable as UT
on UT.ID = EST.UserID
WHERE ESD.ExamSessionID = @examInstanceId      
      
      
      
    DECLARE @myAuditUser TABLE (auditID int, version int)
	INSERT @myAuditUser(auditID, Version)
	SELECT UserID, Version
	FROM ExamSessionDurationAuditTable as ESD
	INNER JOIN UserTable as UT
	on UT.ID = ESD.UserID
	WHERE ESD.ExamSessionID = @examInstanceId      
	  

	IF NOT EXISTS (
		
		select 1
		FROM WAREHOUSE_UserTable
		
		INNER JOIN @myAuditUser A
		on A.auditID = WAREHOUSE_UserTable.UserId
		
		where A.Version >= WAREHOUSE_UserTable.version

	)

	    
	INSERT @WarehouseUserTableInsert(SACandidateID, InsertNeeded)
	VALUES (@mySAUserId,'1')








      /* Warehousing ExamSessionDurationAuditTable table*/   
      
      -- table to store warehoused userIds 
      DECLARE @durationAuditUsers TABLE
      (
		LiveUserId INT,
		WarehousedUserId INT NULL		
      )






      --@Warehouse users if needed -----------
      INSERT INTO @durationAuditUsers (LiveUserId, WarehousedUserId)
      SELECT DISTINCT CAT.UserID, WUT.ID 
      FROM dbo.ExamSessionDurationAuditTable CAT      
      INNER JOIN dbo.UserTable UT ON UT.ID = CAT.UserID
      LEFT OUTER JOIN WAREHOUSE_UserTable WUT ON WUT.UserId = UT.ID
      AND WUT.ID = (SELECT MAX(ID) FROM WAREHOUSE_UserTable WU WHERE  
					  WU.UserId = UT.ID
					  AND UT.Version = WU.version
					  AND WU.OriginatorID = @OriginatorID)        
      WHERE CAT.ExamSessionID = @examInstanceId
      
      DECLARE @newDurationAuditUsers TABLE
      (
		LiveUserId INT,
		WarehousedUserId INT		
      )
      
      
      --Create users if they don't exist
      INSERT [dbo].[WAREHOUSE_UserTable]      
            ([UserId]      
            ,[CandidateRef]      
            ,[Forename]      
            ,[Surname]      
            ,[Middlename]      
            ,[DOB]      
            ,[Gender]      
            ,[SpecialRequirements]      
            ,[AddressLine1]      
            ,[AddressLine2]      
            ,[Town]      
            ,[County]      
            ,[Country]      
            ,[PostCode]      
            ,[Telephone]      
            ,[Email]      
            ,[EthnicOrigin]      
            ,[AccountCreationDate]      
            ,[AccountExpiryDate]      
            ,[ExtraInfo]      
            ,[version]      
            ,[OriginatorID]    
            ,[ULN]
            ,[Username])  
        OUTPUT inserted.UserId, inserted.ID
           INTO @newDurationAuditUsers(LiveUserId, WarehousedUserId)   
        SELECT      
          UT.[ID]      
          ,UT.[CandidateRef]      
          ,UT.[Forename]      
          ,UT.[Surname]      
          ,UT.[Middlename]      
          ,UT.[DOB]      
          ,UT.[Gender]      
          ,UT.[SpecialRequirements]      
          ,UT.[AddressLine1]      
          ,UT.[AddressLine2]      
          ,UT.[Town]      
          ,CLT.[County]      
          ,CRLT.[Country]      
          ,UT.[PostCode]      
          ,UT.[Telephone]      
          ,UT.[Email]      
          ,EOLT.[Name]      
          ,UT.[AccountCreationDate]      
          ,UT.[AccountExpiryDate]      
          ,UT.[ExtraInfo]      
          ,UT.[version]      
          ,@OriginatorID     
          ,UT.[ULN] 
          ,UT.[Username]    
        FROM [dbo].[UserTable] UT
        INNER JOIN [dbo].[CountyLookupTable] CLT ON CLT.ID = UT.County
        INNER JOIN [dbo].[CountryLookupTable] CRLT ON CRLT.ID = UT.Country
        INNER JOIN [dbo].[EnthnicOriginLookupTable]  EOLT ON EOLT.ID =  UT.[EthnicOriginID]    
        INNER JOIN @durationAuditUsers CAU ON CAU.LiveUserId = UT.ID AND CAU.WarehousedUserId IS NULL
              
        UPDATE AU
        SET WarehousedUserId = NU.WarehousedUserId
        FROM @newDurationAuditUsers NU
        INNER JOIN @durationAuditUsers AU ON AU.LiveUserId = NU.LiveUserId
        
*/