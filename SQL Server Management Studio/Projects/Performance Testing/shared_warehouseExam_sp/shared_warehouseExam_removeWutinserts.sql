USE [DI_BC_SecureAssess]
GO
/****** Object:  StoredProcedure [dbo].[sa_SHARED_warehouseExam_sp_DJ]    Script Date: 07/16/2015 11:22:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[sa_SHARED_warehouseExam_sp_DJ]      
 @examInstanceId  int,      
 @OriginatorID  int,      
 @returnString  nvarchar(max) output      
--WITH_ENCRYPTION_REPLACE_ME_FOR_LOCAL--      
AS      
       
BEGIN     
      
SET NOCOUNT ON;      
          
          
DECLARE @errorReturnString nvarchar(max)      
DECLARE @errorNum  nvarchar(100)      
DECLARE @errorMess nvarchar(max)      
DECLARE @ReturnVal nvarchar(max)      

 BEGIN TRY      
        
 
DECLARE @mySAScheduledExamId int
	, @mySACentreId int
	, @mySACreatedById int
	, @mySAUserId int
	, @myKeyCode nvarchar(12)      
    , @AvailableForCentreReview bit -- Centre Direct     
    , @ResultDataFull xml
    , @TodaysDate nvarchar(50)
    , @mySACentreVersion int      
    , @mySACreatedByIdVersion int
    , @mySAUserIdVersion int   
	, @myWarehouseExamState INT
	, @ValidationCount INT
	, @completionDate smalldatetime 
	, @qualificationid int
	, @qualificationName nvarchar(100)

	
  SELECT       
	  @mySAScheduledExamId = ExamSessionTable.ScheduledExamID,      
	  @mySACentreId = ScheduledExamsTable.CentreID,      
	  @mySACreatedById = ScheduledExamsTable.CreatedBy,      
	  @mySAUserId = ExamSessionTable.UserID,      
	  @myKeyCode = ExamSessionTable.KeyCode,    
	  @AvailableForCentreReview = scheduledForCentreDirect,
	  @mySACentreVersion = CentreTable.version,
	  @mySACreatedByIdVersion = UserTable.version,
	  @mySAUserIdVersion = UserTable.version,
	  @ValidationCount = ValidationCount,
	  @ResultDataFull = resultDataFull,
	  @qualificationid = qualificationid,
	  @qualificationName = qualificationName
  FROM ExamSessionTable    
    
  INNER JOIN ScheduledExamsTable 
  ON ScheduledExamsTable.ID = ExamSessionTable.ScheduledExamID      
  Inner join CentreTable
  ON CentreTable.ID = ScheduledExamsTable.CentreID
  INNER JOIN UserTable
  on UserTable.ID = ExamSessionTable.UserID
  INNER Join IB3QualificationLookup
  on IB3QualificationLookup.ID = ScheduledExamsTable.qualificationID
  WHERE ExamSessionTable.ID = @examInstanceId  

  IF (EXISTS(SELECT [ExamSessionID] FROM Warehouse_ExamSessionTable WHERE ExamSessionID=@examInstanceId))      
   BEGIN      
    -- exam has already been archived, report the error      
    SET @returnString = '<result errorCode="1"><return>The exam session already exists in the archive</return></result>'      
    RETURN      
   END    
      
        
  /*############################################################################################      
  SET UP A LINK TO THE WAREHOUSE SERVER, THIS WILL BE IMPLEMENTED LATER      
  SEE PAGE 300 OF SERVER BIBLE      
  ##############################################################################################*/      
      
  /*Check whther the schuled exam already exists in the warehouse tables      
  If it doesnt we must create a record for and select the wareHouseScheduledExam ID      
  If it already exists, we just need to select the wareHouseScheduledExam ID*/      
        
  IF NOT EXISTS ( SELECT [ID] 
				  FROM WAREHOUSE_ScheduledExamsTable      
				  WHERE ScheduledExamID = @mySAScheduledExamId      
				  AND OriginatorID = @OriginatorID
				)      
  BEGIN      
      

    /*      
   Write entry into the Warehouse_CentreTable, based on:
   
   CentreTable.Version
   
   Below logic checks to see if they are not present in Warehouse_CentreTable with the same version number.  If this is the case > Insert
   */  
     
     DECLARE @MaxWHCentreVersion smallint = ( 
		SELECT MAX(version)
	    FROM WAREHOUSE_CentreTable      
	    WHERE CentreID = @mySACentreId      
	    AND OriginatorID = @OriginatorID   
      )
      
    IF NOT EXISTS  (SELECT 1 
		FROM WAREHOUSE_CentreTable  
		WHERE CentreID = @mySACentreId  
		AND OriginatorID = @OriginatorID
		AND WAREHOUSE_CentreTable.version = @MaxWHCentreVersion
		)      

	   BEGIN          
	      	 INSERT [dbo].[WAREHOUSE_CentreTable]      
				 ([CentreID]      
				 ,[CentreName]      
				 ,[CentreCode]      
				 ,[AddressLine1]      
				 ,[AddressLine2]      
				 ,[County]      
				 ,[Country]      
				 ,[PostCode]      
				 ,[version]      
				 ,[OriginatorID]      
				 ,[Town])      
			 SELECT [dbo].[CentreTable].[ID]      
				,[dbo].[CentreTable].[CentreName]      
				,[dbo].[CentreTable].[CentreCode]      
				,[dbo].[CentreTable].[AddressLine1]      
				,[dbo].[CentreTable].[AddressLine2]      
				,[dbo].[CountyLookupTable].[County]      
				,[dbo].[CountryLookupTable].[Country]      
				,[dbo].[CentreTable].[PostCode]      
				,[dbo].[CentreTable].[version]      
				,@OriginatorID      
				,[dbo].[CentreTable].[Town]       
			 FROM [dbo].[CentreTable],[dbo].[CountryLookupTable],[dbo].[CountyLookupTable]      
			 WHERE [dbo].[CentreTable].[ID] = @mySACentreId      
			 AND [dbo].[CentreTable].[County] = [dbo].[CountyLookupTable].[ID]      
			 AND [dbo].[CentreTable].[Country] = [dbo].[CountryLookupTable].[ID]    		   
		END      
         
   -- Store the latest ID for the affected centre from the warehouse table, to use later      
   
   DECLARE @myWarehouseCentreId  int = 
	(  SELECT TOP 1 [ID]       
	   FROM WAREHOUSE_CentreTable      
	   WHERE CentreID = @mySACentreId      
	   AND OriginatorID = @OriginatorID      
	   AND version = @mySACentreVersion
	   ORDER BY ID DESC      
     )
     
   /*      
   Write entries into the Warehouse_UserTable, based on:
   
   ScheduledUsersTable.CreatedBy
   ExamSessionTable.UserID
   MarkerResponseData (MarkerID)
   
   Below logic checks to see if they are not present in Warehouse_UserTable with the same version number.  If this is the case > Insert
   */      
   
	DECLARE @WarehouseUserTableInsert TABLE
		(
			SACreatedByUser int
			, SACandidateID int
			, SAMarkerUser int
			, AuditUser int
			, documentUserID int
			, InsertNeeded bit
		)

--SA User who scheduled the exam
	      
    DECLARE @MaxWHScheduledUserVersion int = ( 
	SELECT MAX(version)
	FROM WAREHOUSE_UserTable    
	WHERE UserId = @mySACreatedById      
	AND OriginatorID = @OriginatorID   
	)

	IF  ISNULL(@MaxWHScheduledUserVersion, 0) <= @mySACreatedByIdVersion
	AND
	NOT EXISTS ( SELECT 1 
		  FROM WAREHOUSE_UserTable      
		  WHERE UserId = @mySACreatedById      
		  AND OriginatorID = @OriginatorID
		  AND WAREHOUSE_UserTable.version = @MaxWHScheduledUserVersion
	)
	      
	INSERT @WarehouseUserTableInsert(SACreatedByUser, InsertNeeded)
	VALUES (@mySACreatedById,'1')

--SA Candidate who sat the exam

	DECLARE @MaxWHCandidateUserVersion int = ( 
	SELECT MAX(version)
	FROM WAREHOUSE_UserTable    
	WHERE UserId = @mySAUserId      
	AND OriginatorID = @OriginatorID   
	)

	IF  ISNULL(@MaxWHCandidateUserVersion, 0) <= @mySAUserIdVersion
	AND
	NOT EXISTS ( SELECT 1 
		  FROM WAREHOUSE_UserTable      
		  WHERE UserId = @mySAUserId      
		  AND OriginatorID = @OriginatorID
		  AND WAREHOUSE_UserTable.version = @MaxWHCandidateUserVersion
	)
	    
	INSERT @WarehouseUserTableInsert(SACandidateID, InsertNeeded)
	VALUES (@mySAUserId,'1')
      

--SA Marker User Data

     /*FIX for bug id 2560; Add the marker into the warehouse user table if they       
     a; dont exist or      
     b; have been updated since their last entry*/   
     
	DECLARE @markerUser TABLE (markerID int, Version int)	
	INSERT @markerUser(markerID, Version)
	SELECT distinct a.b.value('(.)[1]','int')
		  , (select version from UserTable where ID = a.b.value('(.)[1]','int'))
	FROM ExamSessionItemResponseTable (NOLOCK)
	inner join ExamSessionTable
	on ExamSessionTable.ID = ExamSessionItemResponseTable.ExamSessionID
	CROSS APPLY [MarkerResponseData].nodes('/entries/entry/userId') a(b)
	WHERE ExamSessionID = @examInstanceId
	  

	IF NOT EXISTS (
		
		select 1
		FROM WAREHOUSE_UserTable		
		INNER JOIN @markerUser A
		on A.markerID = WAREHOUSE_UserTable.UserId		
		where A.Version = WAREHOUSE_UserTable.version
		and WAREHOUSE_UserTable.OriginatorID = @OriginatorID
	)

	INSERT @WarehouseUserTableInsert(SAMarkerUser, InsertNeeded)
	select markerID, '1' from @markerUser

--WarehouseUserID's for ExamSessionDurationAuditTable

    DECLARE @myAuditUser TABLE (auditID int, version int)
	INSERT @myAuditUser(auditID, Version)
	SELECT UserID, Version
	FROM ExamSessionDurationAuditTable as ESD
	INNER JOIN UserTable as UT
	on UT.ID = ESD.UserID
	WHERE ESD.ExamSessionID = @examInstanceId      
	  

	IF NOT EXISTS (
		
		select 1
		FROM WAREHOUSE_UserTable		
		INNER JOIN @myAuditUser A
		on A.auditID = WAREHOUSE_UserTable.UserId		
		where A.Version = WAREHOUSE_UserTable.version
		and WAREHOUSE_UserTable.OriginatorID = @OriginatorID
	)
	
	INSERT @WarehouseUserTableInsert(AuditUser, InsertNeeded)
	select auditID, '1' from @myAuditUser
	
--WarehouseUserID's for ExamSessionDocumentInfoTable
	
    DECLARE @myDocumentUser TABLE (documentUserID int, version int)
	INSERT @myDocumentUser(documentUserID, Version)
	SELECT UploadingUserID, Version
	FROM ExamSessionDocumentInfoTable as ESD
	INNER JOIN UserTable as UT
	on UT.ID = ESD.UploadingUserID
	WHERE ESD.ID = @examInstanceId      
	  

	IF NOT EXISTS (
		
		select 1
		FROM WAREHOUSE_UserTable		
		INNER JOIN @myDocumentUser A
		on A.documentUserID = WAREHOUSE_UserTable.UserId		
		where A.Version = WAREHOUSE_UserTable.version
		and WAREHOUSE_UserTable.OriginatorID = @OriginatorID
	)
	
	INSERT @WarehouseUserTableInsert(documentUserID, InsertNeeded)
	select documentUserID, '1' from @myDocumentUser
	
	
	

  BEGIN  
    
	;with CTE (IDsToInsert) AS (

	SELECT SACreatedByUser FROM @WarehouseUserTableInsert where InsertNeeded = 1
	UNION
	SELECT SACandidateID FROM @WarehouseUserTableInsert where InsertNeeded = 1
	UNION
	SELECT SAMarkerUser FROM @WarehouseUserTableInsert where InsertNeeded = 1
	UNION
	SELECT AuditUser FROM @WarehouseUserTableInsert where InsertNeeded = 1
	UNION
	SELECT documentUserID FROM @WarehouseUserTableInsert where InsertNeeded = 1
	)
	
     --Insert new user records where required      
		 INSERT [dbo].[WAREHOUSE_UserTable]      
			 ([UserId]      
			 ,[CandidateRef]      
			 ,[Forename]      
			 ,[Surname]      
			 ,[Middlename]      
			 ,[DOB]      
			 ,[Gender]      
			 ,[SpecialRequirements]      
			 ,[AddressLine1]      
			 ,[AddressLine2]      
			 ,[Town]      
			 ,[County]      
			 ,[Country]      
			 ,[PostCode]      
			 ,[Telephone]      
			 ,[Email]      
			 ,[EthnicOrigin]      
			 ,[AccountCreationDate]      
			 ,[AccountExpiryDate]      
			 ,[ExtraInfo]      
			 ,[version]      
			 ,[OriginatorID]    
			 ,[ULN]
			 ,[Username])      
		 SELECT      
		   [UserTable].[ID]      
		   ,[UserTable].[CandidateRef]      
		   ,[UserTable].[Forename]      
		   ,[UserTable].[Surname]      
		   ,[UserTable].[Middlename]      
		   ,[UserTable].[DOB]      
		   ,[UserTable].[Gender]      
		   ,[UserTable].[SpecialRequirements]      
		   ,[UserTable].[AddressLine1]      
		   ,[UserTable].[AddressLine2]      
		   ,[UserTable].[Town]      
		   ,[CountyLookupTable].[County]      
		   ,[CountryLookupTable].[Country]      
		   ,[UserTable].[PostCode]      
		   ,[UserTable].[Telephone]      
		   ,[UserTable].[Email]      
		   ,[EnthnicOriginLookupTable].[Name]      
		   ,[UserTable].[AccountCreationDate]      
		   ,[UserTable].[AccountExpiryDate]      
		   ,[UserTable].[ExtraInfo]      
		   ,[UserTable].[version]      
		   ,@OriginatorID     
		   ,[UserTable].[ULN] 
		   ,[UserTable].[Username]   
		 FROM [dbo].[UserTable],[dbo].[CountyLookupTable]
			,[dbo].[CountryLookupTable]
			,[dbo].[EnthnicOriginLookupTable]
		       
		 WHERE [UserTable].[ID] IN (SELECT IDsToInsert 
									from CTE 
									where IDsToInsert IS NOT NULL 
									and IDsToInsert != '-1'
									)
		 AND [dbo].[UserTable].[County] = [dbo].[CountyLookupTable].[ID]      
		 AND [dbo].[UserTable].[Country] = [dbo].[CountryLookupTable].[ID]      
		 AND [dbo].[EnthnicOriginLookupTable].[ID] = [UserTable].[EthnicOriginID]

    END      
  
  
   -- Store the latest id for the affected createdByUser from the warehouse table to use for later   
   DECLARE @myWarehouseCreatedById  int  = 
   (
	   select TOP 1 [ID]       
	   FROM [WAREHOUSE_UserTable]      
	   WHERE [UserId] = @mySACreatedById      
	   AND OriginatorID = @OriginatorID      
	   AND [WAREHOUSE_UserTable].version = @mySACreatedByIdVersion
	   ORDER BY ID DESC      
   )

         
      
   -- we are now in a position to insert the scheduled exam into the warehouse table      
   INSERT [dbo].[WAREHOUSE_ScheduledExamsTable]      
       ([ScheduledExamID]      
       ,[ExamID]      
       ,[WAREHOUSECentreID]      
       ,[WAREHOUSECreatedBy]      
       ,[CreatedDateTime]      
       ,[ScheduledStartDateTime]      
       ,[ScheduledEndDateTime]      
       ,[ActiveStartTime]      
       ,[ActiveEndTime]      
       ,[qualificationId]      
       ,[examName]      
       ,[groupState]      
       ,[invigilated]      
       ,[humanMarked]      
       ,[AdvanceContentDownloadTimespanInHours]      
       ,[OriginatorID]      
       ,[qualificationName]      
       ,[qualificationRef]      
       ,[examVersionId]      
       ,[ExternalReference]      
       ,[language]      
       ,[qualificationLevel]  
       ,[purchaseOrder]    
       )      
   SELECT [dbo].[ScheduledExamsTable].[ID]      
       ,[dbo].[ScheduledExamsTable].[ExamID]      
       ,@myWarehouseCentreId      
       ,@myWarehouseCreatedById      
       ,[dbo].[ScheduledExamsTable].[CreatedDateTime]      
       ,[dbo].[ScheduledExamsTable].[ScheduledStartDateTime]      
       ,[dbo].[ScheduledExamsTable].[ScheduledEndDateTime]      
       ,[dbo].[ScheduledExamsTable].[ActiveStartTime]      
       ,[dbo].[ScheduledExamsTable].[ActiveEndTime]      
       ,[dbo].[ScheduledExamsTable].[qualificationId]      
       ,[dbo].[ScheduledExamsTable].[examName]      
       ,[dbo].[ScheduledExamsTable].[groupState]      
       ,[dbo].[ScheduledExamsTable].[invigilated]      
       ,[dbo].[ScheduledExamsTable].[humanMarked]      
       ,[dbo].[ScheduledExamsTable].[AdvanceContentDownloadTimespanInHours]      
       ,@OriginatorID      
       ,[dbo].[IB3QualificationLookup].[QualificationName]      
       ,[dbo].[IB3QualificationLookup].[QualificationRef]      
       ,[dbo].[ScheduledExamsTable].[examVersionId]      
       ,Convert(varchar(200),CAST([ExamSessionTable].StructureXml AS XML).query('/assessmentDetails/externalReference/text()')) -- Fix by DD - 15/01/09      
       ,[dbo].[ScheduledExamsTable].[language]      
       ,[dbo].[IB3QualificationLookup].[QualificationLevel]  
       ,[dbo].[ScheduledExamsTable].[purchaseOrder]     
   FROM [dbo].[ScheduledExamsTable]      
    INNER JOIN [dbo].[IB3QualificationLookup] ON      
     [dbo].[IB3QualificationLookup].[ID] = [dbo].[ScheduledExamsTable].[qualificationId]      
    INNER JOIN [dbo].[ExamSessionTable] ON [dbo].[ScheduledExamsTable].[ID] = [dbo].[ExamSessionTable].[ScheduledExamID]      
   WHERE [dbo].[ScheduledExamsTable].[ID] = @mySAScheduledExamId      
      
  END       
      
  -- Store the warehouse scheduled exam id version to use for later 
  DECLARE @myWarehouseScheduledExamId  int  = 
  (
	  Select [ID]       
	  FROM WAREHOUSE_ScheduledExamsTable    
	  WHERE ScheduledExamID = @mySAScheduledExamId      
	  AND OriginatorID = @OriginatorID      
  )
  

  
  
  DECLARE @AssignedMarkerID		int
  DECLARE @AssignedModeratorID	int
  DECLARE @MarkerUserName		nvarchar(max)
  DECLARE @ModeratorUserName	nvarchar(max)
  
  DECLARE @UserVer int
  
  SELECT @AssignedMarkerID = UserId FROM UserSessionRelationTable WHERE ExamSessionId = @examInstanceId AND RelationTypeId = 1
  SELECT @AssignedModeratorID = UserId FROM UserSessionRelationTable WHERE ExamSessionId = @examInstanceId AND RelationTypeId = 2
  
  SELECT @UserVer = [version] FROM UserTable WHERE Id = @AssignedMarkerID
  exec sa_SHARED_warehouseUser_sp @OriginatorID, @AssignedMarkerID, @UserVer,@AssignedMarkerID out
  SELECT @MarkerUserName = UserName FROM WAREHOUSE_UserTable WHERE UserId = @AssignedMarkerID
      
  SELECT @UserVer = [version] FROM UserTable WHERE Id = @AssignedModeratorID
  exec sa_SHARED_warehouseUser_sp @OriginatorID, @AssignedModeratorID, @UserVer, @AssignedModeratorID out
  SELECT @ModeratorUserName = UserName FROM WAREHOUSE_UserTable WHERE UserId = @AssignedModeratorID
     
      
   -- get the latest id for the affected user from the warehouse table      
   DECLARE @myWarehouseUserId  int  = 
	(
		select  [ID]       
		FROM [WAREHOUSE_UserTable]      
		WHERE [UserId] = @mySAUserId      
		AND OriginatorID = @OriginatorID      
		AND version = @mySAUserIdVersion      
     )    
        
   -- select the all entries in the ExamStateChangeAuditTable for the current exam session      
   -- into an xml variable      
   DECLARE @examStateChangeAuditXml xml      
   SET @examStateChangeAuditXml =       
   (
   SELECT TAG, PARENT, [exam!1!Element], [exam!1!id],       
   [stateChange!2!newStateID!Element], [stateChange!2!newState!Element],       
   [stateChange!2!changeDate!Element], [stateChange!2!information!xml]      
         
   FROM      
   (
		SELECT 1 AS TAG,      
		 NULL  AS  Parent,      
		 NULL  as  [exam!1!Element],      
		 NULL  as  [exam!1!id],      
		 NULL  AS  [stateChange!2!newStateID!Element],      
		 NULL  AS  [stateChange!2!newState!Element],      
		 NULL  AS  [stateChange!2!changeDate!Element],      
		 NULL  AS  [stateChange!2!information!xml]      
	           
		 UNION ALL      
		 SELECT      
		 2   AS Tag,      
		 1   AS Parent,      
		 NULL,      
		 [ExamStateChangeAuditTable].[ExamSessionID],      
		 [ExamStateChangeAuditTable].[NewState],      
		 [ExamStateLookupTable].[StateName],      
		 CONVERT(VARCHAR(23), [ExamStateChangeAuditTable].[StateChangeDate], 113),      
		 [ExamStateChangeAuditTable].[StateInformation]      
	           
		 FROM [dbo].[ExamStateChangeAuditTable]      
		  INNER JOIN [dbo].[ExamStateLookupTable] ON      
		   [ExamStateLookupTable].[ID] = [ExamStateChangeAuditTable].[NewState]      
	           
		 WHERE [ExamStateChangeAuditTable].[ExamSessionID] = @examInstanceId
     )      
    AS stateChangeXml      
    FOR XML EXPLICIT
    )          
          
   /*######################D Naylor 08/06/09#########################      
   * Code select to retrive the exam audit trial as xml and store this in the warehouse      
   */      
    DECLARE @IpAuditXml xml      
    SET @IpAuditXml =      
    (SELECT       
     ID     AS  '@id',      
     examInstanceID  AS  'examInstanceId',      
     IPAddress   AS  'ipAddress',      
     methodCalled  AS  'methodCalled'      
    FROM ExamSessionAuditTrail      
    WHERE examInstanceID = @examInstanceId      
    FOR XML PATH('entry'), ROOT('ipData')      
    )      
         
   -- place if not exists on here for concurrency reasons      
IF NOT EXISTS (SELECT ID FROM [dbo].[WAREHOUSE_ExamSessionTable]      
       WHERE [ExamSessionID] = @examInstanceId AND WAREHOUSEScheduledExamID = @myWarehouseScheduledExamId)      
    BEGIN      
  
     -- get the completionDate into a variable      
 
     SET @completionDate = CONVERT(smalldatetime, CONVERT(varchar(max), @examStateChangeAuditXml.query('//stateChange[newState[text()=''Exam Submitted'']][1]/changeDate/text()')))      
          
     ---Modify LastInUseDate in ResultData    
     --SELECT @ResultDataFull = ResultDataFull FROM [dbo].[ExamSessionTable] WHERE [ID] = @examInstanceId      
     SET @TodaysDate = GETDATE()
     SET @ResultDataFull.modify('replace value of (//assessmentDetails/lastInUseDate/text())[1]  with (sql:variable("@TodaysDate"))')  
     
     SELECT @myWarehouseExamState = CASE WHEN ExportToSecureMarker = 1 AND examState <> 10 AND [ItemMarksUploaded] = 0 THEN 2 ELSE 1 END
     FROM ExamSessionTable
     WHERE ID = @examInstanceId
      
     -- now insert the exam session from the SA db into the warehouse version      
     INSERT [dbo].[WAREHOUSE_ExamSessionTable]      
       ( [ExamSessionID]      
          ,[WAREHOUSEScheduledExamID]      
          ,[WAREHOUSEUserID]      
          ,[StructureXML]      
          ,[MarkerData]      
          ,[KeyCode]      
          ,[PreviousExamState]      
          ,[pinNumber]     
          ,[ExamStateChangeAuditXml]      
          ,[resultData]      
          ,[warehouseTime]      
          ,[resultDataFull]      
          ,[completionDate]      
          ,[CQN]      
          ,[examIPAuditData]      
          ,[downloadInformation]      
          ,[clientInformation]      
		  ,[availableForCentreReview]    
		  ,[ExportToSecureMarker]    
		  ,[WarehouseExamState]
		  ,[AllowPackageDelivery]  
		  ,[ContainsBTLOffice]      
		  ,[appeal]   
		  ,[submissionExported]
		  ,[TargetedForVoid]
		  ,[EnableOverrideMarking]
		  ,[itemMarksUploaded]
		  ,[TakenThroughLocalScan]
		  ,[LocalScanDownloadDate]
		  ,[LocalScanUploadDate]
		  ,[LocalScanNumPages]
		  ,[CertifiedForTablet]
		  ,[IsProjectBased]
		  ,AssignedMarkerUserID
		  ,AssignedModeratorUserID
         )      
         SELECT  [ExamSessionTable].[ID]      
         ,@myWarehouseScheduledExamId      
         ,@myWarehouseUserId      
         ,[StructureXML]      
         ,[MarkerData]      
         ,[KeyCode]      
         ,[examState]      
         ,[pinNumber]           
         ,@examStateChangeAuditXml      
         ,[resultData]      
         ,GETDATE()      
         ,@ResultDataFull       
         ,@completionDate      
         ,[UserQualificationsTable].[CQN]      
         ,@IpAuditXml      
         ,[downloadInformation]      
         ,[clientInformation]      
         ,@availableForCentreReview    
         ,[ExportToSecureMarker]    
         ,0
         ,[AllowPackageDelivery]    
         ,[ContainsBTLOffice]  
         ,ISNULL(StructureXml.value('(assessmentDetails/autoViewExam)[1]','INT'), 0)
         ,[submissionExported]
         ,[TargetedForVoid]
         ,[ExamSessionTable].[EnableOverrideMarking]
         ,[ItemMarksUploaded]
         ,[TakenThroughLocalScan]
         ,[LocalScanDownloadDate]
         ,[LocalScanUploadDate]
         ,[LocalScanNumPages]
         ,[CertifiedForTablet]
         ,[IsProjectBased]
         ,@AssignedMarkerID
		 ,@AssignedModeratorID
     FROM [dbo].[ExamSessionTable]   
     INNER JOIN [WAREHOUSE_ScheduledExamsTable]      
      ON [WAREHOUSE_ScheduledExamsTable].[ID] = @myWarehouseScheduledExamId      
     INNER JOIN [UserQualificationsTable]      
      ON (      
       [UserQualificationsTable].[UserId] = [ExamSessionTable].[UserId]      
       AND      
       [UserQualificationsTable].[QualificationId] = [WAREHOUSE_ScheduledExamsTable].[QualificationId]      
       )      
     WHERE [ExamSessionTable].[ID] = @examInstanceId;      
            
     --Store this to use later on 
	DECLARE @myNewWarehouseExamSessionId int  = 
	   (
		   select [ID] 
		   from WAREHOUSE_ExamSessionTable 
		   where ExamSessionID = @examInstanceId
	   )
      

     INSERT INTO dbo.WAREHOUSE_ExamSessionDurationAuditTable(     
      [Warehoused_ExamSessionID]
      ,[Warehoused_UserID]
      ,[ChangeDateSubmitted]
      ,[OriginalExamDuration]
      ,[NewExamDuration]
      ,[Reason]
      )
      SELECT DISTINCT
      @myNewWarehouseExamSessionId
      ,WUT.ID
      ,[ChangeDateSubmitted]
      ,[OriginalExamDuration]
      ,[NewExamDuration]
      ,[Reason]
      FROM dbo.ExamSessionDurationAuditTable CAT
      INNER JOIN WAREHOUSE_UserTable as WUT on WUT.UserId = CAT.UserID	
      WHERE CAT.ExamSessionID = @examInstanceId     


     --insert the exam session item response from sa into the warehouse      
     INSERT [dbo].[WAREHOUSE_ExamSessionItemResponseTable]      
         ([WAREHOUSEExamSessionID]      
         ,[ItemID]      
         ,[ItemVersion]      
         ,[ItemResponseData]      
         ,[MarkerResponseData]      
         ,[ItemMark]
         ,[MarkingIgnored])      
     SELECT       
       @myNewWarehouseExamSessionId      
       ,[ItemID]      
       ,[ItemVersion]      
       ,[ItemResponseData]      
       ,[MarkerResponseData]      
       ,[ItemMark]
       ,[MarkingIgnored]      
     FROM [dbo].[ExamSessionItemResponseTable]      
     WHERE ExamSessionID = @examInstanceId;      
           
    
    
           
/*D NAYLOR - R7 05/09#################################################################      
Performance improvements, new shredded item tables have been created in order to remove the need       
for X-Query within the warehouse data requests. These new tables are populated below using the data      
just inserted into the warehouse and making use of the existing views.      
*/      
      
           
      
     --Insert into the Shredded data version of the candidate exam audit view      
     insert into WAREHOUSE_ExamSessionTable_Shreded  
     (examSessionId,      
      structureXml,      
      examVersionName,      
      examVersionRef,      
      examVersionId,      
      examName,      
      examRef,      
      qualificationid,      
      qualificationName,      
      qualificationRef,      
      resultData,      
      submittedDate,      
      originatorId,      
      centreName,      
      centreCode,      
      foreName,      
      dateOfBirth,      
      gender,      
      candidateRef,      
      surName,      
      scheduledDurationValue,      
      previousExamState,      
      examStateInformation,      
      examResult,      
      passValue,      
      closeValue,      
      --0,      
      externalReference,      
      examType,      
      warehouseTime,      
      CQN,      
      actualDuration,      
      appeal,      
      reMarkStatus,        
      qualificationLevel,   
      centreId,     
      uln,  
      AllowPackageDelivery,  
      ExportToSecureMarker,  
      ContainsBTLOffice,
      ExportedToIntegration,
      WarehouseExamState,
      [language],
      KeyCode,
      TargetedForVoid,
      EnableOverrideMarking,
      AddressLine1,
      AddressLine2,
      Town,
      County,
      Country,
      Postcode,
      ScoreBoundaryData,
      itemMarksUploaded,
      passMark,
      totalMark,
      passType,
      voidJustificationLookupTableId,
      automaticVerification,
      resultSampled,
      [started],
      submitted,
      validFromDate,
      expiryDate,
      totalTimeSpent,
      defaultDuration,
      scheduledDurationReason,
      WAREHOUSEScheduledExamID,
      WAREHOUSEUserID,
      itemDataFull,
      examId,
      userId,
      TakenThroughLocalScan,
	  LocalScanDownloadDate,
	  LocalScanUploadDate,
	  LocalScanNumPages,
	  CertifiedForTablet,
	  IsProjectBased,
	  ValidationCount)
     SELECT      
      examSessionId,      
      structureXml,      
      examVersionName,      
      examVersionRef,      
      examVersionId,      
      examName,      
      examRef,      
      qualificationid,      
      qualificationName,      
      qualificationRef,      
      resultData,      
      submittedDate,      
      originatorId,      
      centreName,      
      centreCode,      
      foreName,      
      dateOfBirth,      
      gender,      
      candidateRef,      
      surName,      
      scheduledDurationValue,      
      previousExamState,      
      examStateInformation,      
      examResult,      
      passValue,      
      closeValue,      
      --0,      
      externalReference,      
      examType,      
      warehouseTime,      
      CQN,      
      actualDuration,      
      appeal,      
      reMarkStatus,        
      qualificationLevel,   
      centreId,     
      uln,  
      AllowPackageDelivery,  
      ExportToSecureMarker,  
      ContainsBTLOffice,
      ExportedToIntegration,
      WarehouseExamState,
      [language],
      KeyCode,
      TargetedForVoid,
      EnableOverrideMarking,
      AddressLine1,
      AddressLine2,
      Town,
      County,
      Country,
      Postcode,
      ScoreBoundaryData,
      itemMarksUploaded,
      passMark,
      totalMark,
      passType,
      voidJustificationLookupTableId,
      automaticVerification,
      resultSampled,
      [started],
      submitted,
      validFromDate,
      expiryDate,
      totalTimeSpent,
      defaultDuration,
      scheduledDurationReason,
      WAREHOUSEScheduledExamID,
      WAREHOUSEUserID,
      itemDataFull,
      examId,
      userId,
      TakenThroughLocalScan,
	  LocalScanDownloadDate,
	  LocalScanUploadDate,
	  LocalScanNumPages,
	  CertifiedForTablet,
	  IsProjectBased,
	  @validationCount
     FROM sa_CandidateExamAudit_View      
     WHERE examSessionId = @myNewWarehouseExamSessionId                
     
	/* Update the lookup table */
     IF NOT EXISTS( SELECT 1 FROM WAREHOUSE_DataLookupTable WHERE DataRefString =@qualificationName AND IDRef=@qualificationid AND LookUpType=1)
	 BEGIN
	     INSERT INTO WAREHOUSE_DataLookupTable (IDRef,DataRefString,LookupType) VALUES (@qualificationid,@qualificationName,1)
	 END


	 UPDATE WAREHOUSE_ExamSessionTable_Shreded
	 SET AssignedMarkerUserID = @AssignedMarkerID 
		, AssignedModeratorUserID = @AssignedModeratorID
	 WHERE examSessionId = @myNewWarehouseExamSessionId
	 	
	/* New insert to shred new columns */
	BEGIN TRANSACTION;
		INSERT INTO WAREHOUSE_ExamSessionTable_ShrededItems
		 (	 ExamSessionID
			,ItemRef
			,ItemName
			,UserMark
			,MarkerUserMark
			,UserAttempted
			,ItemVersion
			,MarkingIgnored
			,MarkedMetadataCount
			,ExamPercentage
			,TotalMark
			,ResponseXML
			,OptionsChosen
			,SelectedCount
			,CorrectAnswerCount
			,itemType
			,FriendItems)
			SELECT	 @myNewWarehouseExamSessionId AS [examSessionId]
					,Result.Item.value('@id', 'nvarchar(15)') AS [itemRef]
					,Result.Item.value('@name', 'nvarchar(200)') AS [itemName]
					,Result.Item.value('@userMark', 'decimal(6, 3)') AS [userMark]
					,Result.Item.value('@markerUserMark', 'nvarchar(max)') AS [markerUserMark]
					,Result.Item.value('@userAttempted', 'bit') AS [userAttempted]
					,Result.Item.value('@version', 'int') AS [itemVersion]
					,Result.Item.value('@markingIgnored', 'tinyint') AS [markingIgnored]
					,Result.Item.value('count(mark)', 'int') AS [markedMetadataCount]
					,WAREHOUSE_ExamSessionTable.ResultData.value('(exam/@userPercentage)[1]', 'decimal(6, 3)') AS [examPercentage]
					,Result.Item.value('@totalMark', 'decimal(6, 3)') AS [totalMark]
					,WAREHOUSE_ExamSessionItemResponseTable.ItemResponseData AS [responseXml]
					,CAST(ISNULL(WAREHOUSE_ExamSessionItemResponseTable.ItemResponseData.query('data(p/s/c[@typ = "10"]/i[@sl = "1"]/@ac)'), ' UA ') AS nvarchar(200)) AS [optionsChosen] --Why?
					,WAREHOUSE_ExamSessionItemResponseTable.ItemResponseData.value('count(p/s/c/i[@sl = "1"])', 'int') AS [selectedCount]
					,WAREHOUSE_ExamSessionItemResponseTable.ItemResponseData.value('count(p/s/c/i[@ca = "1"])', 'int') AS [correctAnswerCount]
					,Result.Item.value('@type', 'int') AS ItemType
					,Result.Item.value('@SurpassFriendItems', 'NVARCHAR(MAX)') AS FriendItems
			 FROM	 WAREHOUSE_ExamSessionTable
			 CROSS APPLY WAREHOUSE_ExamSessionTable.ResultData.nodes('exam/section/item') Result(Item)
			 LEFT JOIN WAREHOUSE_ExamSessionItemResponseTable -- we want to include nulls in the the item response table so that non attempted items are included
					 ON WAREHOUSE_ExamSessionItemResponseTable.WAREHOUSEExamSessionID = @myNewWarehouseExamSessionId
					AND WAREHOUSE_ExamSessionItemResponseTable.ItemID = Result.Item.value('@id', 'nvarchar(15)')
			WHERE	 WAREHOUSE_ExamSessionTable.ID = @myNewWarehouseExamSessionId;
	COMMIT TRANSACTION;

	BEGIN TRANSACTION;
		INSERT INTO WAREHOUSE_ExamSessionAvailableItemsTable
		 (	 ExamSessionID
			,ItemID
			,ItemVersion
			,ItemXML	)
			SELECT	 @myNewWarehouseExamSessionId AS [examSessionId]
					,StructureXML.Item.value('(@id)[1]', 'nvarchar(15)') AS [itemId]
					,StructureXML.Item.value('(@version)[1]', 'int') AS [itemVersion]
					,StructureXML.Item.query('.') AS [itemXml]
			 FROM	 WAREHOUSE_ExamSessionTable
			 CROSS APPLY WAREHOUSE_ExamSessionTable.StructureXML.nodes('assessmentDetails/assessment/section/item') StructureXML(Item)
			WHERE	 WAREHOUSE_ExamSessionTable.ID = @myNewWarehouseExamSessionId;
	COMMIT TRANSACTION;        
      
     -- update userMark where it has been overridden by a human marker      
     UPDATE WAREHOUSE_ExamSessionTable_ShrededItems      
     SET [userMark] = CONVERT(decimal(6,3),[markerUserMark]) / (CASE [TotalMark] WHEN 0 THEN 1 ELSE [TotalMark] END)
     WHERE examSessionId = @myNewWarehouseExamSessionId      
     AND [markerUserMark] != ''      
           
     SET @returnString = '<success/>'     
     
    /* Warehousing ExamSessionDocumentInfoTable table*/   
           
     DECLARE @insertedDocumentIds TABLE(ID INT)
     
     --insert the exam session uploaded documents from sa into the warehouse      
     INSERT INTO [dbo].[WAREHOUSE_ExamSessionDocumentTable]    
           ([ID]    
           ,[warehouseExamSessionID]    
           ,[itemId]    
           ,[documentName]    
           ,[Document]    
           ,[uploadDate])   
     OUTPUT inserted.ID
           INTO @insertedDocumentIds(ID)    
     SELECT 
      [ID]  
      ,@myNewWarehouseExamSessionId    
      ,[itemId]    
      ,[documentName]    
      ,[Document]    
      ,[uploadDate]    
     FROM [dbo].[ExamSessionDocumentTable]    
     WHERE ([ExamSessionDocumentTable].[examSessionId]=@examInstanceId)   
     
      
      --Insert into dbo.WAREHOUSE_ExamSessionDocumentInfoTable
      INSERT INTO dbo.WAREHOUSE_ExamSessionDocumentInfoTable(     
       [Warehoused_ExamSessionDocumentID]
      ,[Warehoused_UploadingUserID]
      ,[UserTypeID]
      ,[Reason])
		SELECT	
		ESDIT.ExamSessionDocumentID,
		WUT.ID,
		ESDIT.UserTypeID,
		ESDIT.Reason
		FROM  ExamSessionDocumentInfoTable as ESDIT
		INNER JOIN WAREHOUSE_UserTable as WUT on WUT.ID = ESDIT.UploadingUserID
	 
     -----------------------------------------------------------------------------
     --Shred the ScoreBoundary data from the structure xml
    
    END      
   ELSE      
    BEGIN      
     -- exam has already been archived, report the error      
      SET @returnString = '<result errorCode="1"><return>The exam session already exists in the archive</return></result>'      
     END  

--DJ 16/05/2013 - Insert into dbo.WAREHOUSE_ExamSessionTable_ShreddedItems_Mark table the MarkedMetadata mark node details.
	BEGIN TRANSACTION;
		INSERT INTO WAREHOUSE_ExamSessionTable_ShreddedItems_Mark
		(	ExamSessionID
		   ,ItemID
		   ,Mark
		   ,LearningOutcome
		   ,DisplayText
		   ,MaxMark)
			SELECT  WAREHOUSE_ExamSessionTable.ID AS [ExamSessionID]
				   ,Result.Item.value('@id', 'varchar(15)') AS [ItemID]
				   ,Item.Mark.value('@mark', 'decimal(6, 3)') AS [Mark]
				   ,Item.Mark.value('@learningOutcome', 'int') AS [LearningOutcome]
				   ,Item.Mark.value('@displayName', 'nvarchar(200)') AS [DisplayName]
				   ,Item.Mark.value('@maxMark', 'decimal(6, 3)') AS [MaxMark]
			FROM   WAREHOUSE_ExamSessionTable
			CROSS APPLY WAREHOUSE_ExamSessionTable.ResultData.nodes('exam/section/item') Result(Item)
			CROSS APPLY Result.Item.nodes('mark') Item(Mark)
			WHERE   WAREHOUSE_ExamSessionTable.ID = @myNewWarehouseExamSessionId;
	COMMIT TRANSACTION; 
	
	-- warehouse CentreChangeAuditTable
	-- There is a data copy as is to save development time for not inportant data
	-- (this data is never used on UI and added just for developers)
	BEGIN TRANSACTION;
	INSERT INTO WAREHOUSE_CentreChangeAuditTable
		(Date
		, UserID
		, OldCentreID
		, NewCentreID
		, ExamSessionID
		, OldScheduledExamID
		, NewScheduledExamID)
		SELECT Date
				, UserID
				, OldCentreID
				, NewCentreID
				, ExamSessionID
				, OldScheduledExamID
				, NewScheduledExamID
		FROM CentreChangeAuditTable
		WHERE ExamSessionID = @examInstanceId
	COMMIT TRANSACTION; 
	
		--- Update the OriginalGradewithVoidReason column in which contains both grades and any user enterted reasons. 
	BEGIN TRANSACTION;			
	WITH ReasonWith(OriginalGrade, VoidReason, OtherVoidReason, ExamSessionId) AS (
			SELECT CASE 
					WHEN ME.OriginalGrade = 'Voided'
						AND ET.TargetedForVoid IS NOT NULL
						THEN 'Offline Voided'
					ELSE ME.OriginalGrade
					END OriginalGrade
				,VL.NAME					
				,CASE 
					WHEN VL.requiresInput = 1
						THEN examStateInformation.value('stateChangeInformation[1]/description[1]', 'nvarchar(200)')
					ELSE ''
					END OtherVoidReason
				,ME.ExamSessionId
			FROM [WAREHOUSE_ExamSessionTable_Shreded] ME
			JOIN [WAREHOUSE_ExamSessionTable] ET ON ET.ID = ME.examSessionId
			LEFT JOIN VoidJustificationLookupTable VL ON VL.ID = ME.voidJustificationLookupTableId
			)

	UPDATE ME
	SET ME.OriginalGradewithVoidReason = Substring(RW.OriginalGrade + ISNULL(VoidReason,'') + ISNULL(OtherVoidReason, ''), 0, 400) -- Only first 400 Characters
	FROM ReasonWith RW
	JOIN [WAREHOUSE_ExamSessionTable_Shreded] ME ON ME.examSessionId = RW.ExamSessionId
	WHERE ME.examSessionId = @myNewWarehouseExamSessionId

	COMMIT TRANSACTION; 
	
	UPDATE WAREHOUSE_ExamSessionTable
	SET WarehouseExamState = @myWarehouseExamState
	WHERE ID = @myNewWarehouseExamSessionId
	
 END TRY      
 BEGIN CATCH    
	WHILE @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;


  SET @errorNum = (SELECT ERROR_NUMBER() AS ErrorNumber)      
  SET @errorMess = (SELECT ERROR_MESSAGE() AS ErrorMessage)      
  SET @returnString = '<result errorCode="2"><return>SQL Error, Number: ' + @errorNum + ' MESSAGE: ' + @errorMess + '</return></result>'      
END CATCH      
END 
