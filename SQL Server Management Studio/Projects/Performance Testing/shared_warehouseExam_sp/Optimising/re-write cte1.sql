DECLARE @myNewWarehouseExamSessionId int = 62868

	BEGIN TRANSACTION;			
	
	UPDATE [WAREHOUSE_ExamSessionTable_Shreded]
	SET OriginalGradewithVoidReason = Substring(V.OriginalGrade + ISNULL(V.VoidReason,'') + ISNULL(V.OtherVoidReason, ''), 0, 400) -- Only first 400 Characters
	FROM [WAREHOUSE_ExamSessionTable_Shreded]
		INNER JOIN (
			SELECT CASE 
				WHEN ME.OriginalGrade = 'Voided'
					AND ET.TargetedForVoid IS NOT NULL
					THEN 'Offline Voided'
				ELSE ME.OriginalGrade
				END OriginalGrade
			,VL.NAME as VoidReason					
			,CASE 
				WHEN VL.requiresInput = 1
					THEN examStateInformation.value('stateChangeInformation[1]/description[1]', 'nvarchar(200)')
				ELSE ''
				END OtherVoidReason
			,ME.ExamSessionId

			FROM [WAREHOUSE_ExamSessionTable_Shreded] ME
			JOIN [WAREHOUSE_ExamSessionTable] ET ON ET.ID = ME.examSessionId
			LEFT JOIN VoidJustificationLookupTable VL ON VL.ID = ME.voidJustificationLookupTableId
			WHERE ME.examSessionId = @myNewWarehouseExamSessionId
		) V
	On V.ExamSessionID = [WAREHOUSE_ExamSessionTable_Shreded].ExamSessionID
	LEFT JOIN VoidJustificationLookupTable VL ON VL.ID = [WAREHOUSE_ExamSessionTable_Shreded].voidJustificationLookupTableId
	WHERE [WAREHOUSE_ExamSessionTable_Shreded].examSessionId = @myNewWarehouseExamSessionId
	
	COMMIT TRANSACTION; 
	
