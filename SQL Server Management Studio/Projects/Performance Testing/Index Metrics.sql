/****** Script for SelectTopNRows command from SSMS  ******/
SELECT  [database]
      ,[table]
      ,[index]
      ,[user_updates]
      --,[last_user_update]
      ,[user_seeks]
      ,[user_scans]
      ,[user_lookups]
      ,[last_user_seek]
      ,[last_user_scan]
      ,[last_user_lookup]
	  ,s.user_seeks + s.user_scans + s.user_lookups AS [user_selects]
      , user_updates - (user_seeks + user_scans + user_lookups) As [Difference] 
--SELECT count([index])
--	,[index]
FROM [PSCollector].[dbo].[IndexMetrics] s 
where (s.user_seeks + s.user_scans + s.user_lookups) = 0
--where [index] in ('IX_ExamSessionTable_90','missing_index_12_11_ExamSessionTable','missing_index_2_1_ExamSessionTable','idx_NC_ExamStateChangeAuditTable_NewState','PK_ExamStateChangeAuditTable') -- = 'IX_UserTable_Retired_Seeded_ID_NCI'
	--and [table] != 'AuditTrails'
--group by [index]
order by  3
  --where [index] = 'IX_ItemId'
  --order by 1,2,3 desc
  
select *
FROM [PSCollector].[dbo].[IndexMetrics] s 
where [table] = 'ExamStateChangeAuditTable'
	--and [index] = 'IX_WarehouseTimeExamState'
order by 1


/*Safe to drop:


ContentAuthor	AuditTrails	IX_UserId

where [index] = 'IX_UserId'
	and [table] != 'Notifications'

SurpassDataWarehouse	FactExamSessions	IX_FactExamSessions
where [index] = 'IX_FactExamSessions'


SecureAssess	
where [index] = 'IX_WAREHOUSE_ExamSessionTable_BTLOffice_CompDate_ExamState'
	*/

  /*
  DELETE
  FROM  [IndexMetrics] 
  where [database] 
	not like '%SecureAssess%'
	and [database] not like '%CPProjectAdmin%'
	and [database] not like '%ContentProducer%'
	and [database] not like '%SecureMarker%'
	and [database] not like '%ItemBank%'
	and [database] not like '%TestPackage%'
	and [database] not like '%SurpassDataWarehouse%'
	and [database] not like '%ContentAuthor%'
	and [database] not like '%SurpassManagement%'
	and [database] not like '%OpenAssess%'
	and [database] not like '%LocalScan%'
	and [database] not like '%PhoneExamSystemContext%'
	and [database] not like '%SurpassCmsPortal%'
	and [database] not like '%OcrDataManagement%'
	and [database] not like '%EPCAdaptor%';

	DELETE
	from IndexMetrics
	where [database] like '%deleted%';

	*/