USE [PPD_AAT_SecureAssess]

SET STATISTICS IO ON

--ALTER PROCEDURE [dbo].[sa_REPORTINGSERVICE_GetExamInstancesForReporting_sp]    
DECLARE

	@centreQualLevelXml  nvarchar(max) =N'<list><i level="1" centre="186" editable="0"/></list>',    
	@qualificationIdList  nvarchar(max) =N'',    
	@filterXml    nvarchar(max) =N'<request><filter pageSize="50" pageIndex="0" filterType="AND"><i name="surname">%SIKALESELE%</i><i name="forename">%</i><i name="centreName">%</i><i name="examName">%</i><i name="completedDateStartRange">25/03/2016 00:00:00</i><i name="completedDateEndRange">23/07/2016 23:59:59</i><i name="candidateRef">%</i><i name="uln">%</i><i name="keycode">%</i><i name="pass">1</i><i name="fail">1</i><i name="void">1</i><i name="na">1</i><i name="close">1</i><i name="examResult"></i><i name="examResultStartRange" ></i><i name="examResultEndRange" ></i> <i name="sorting">candidateSurname ASC</i><i name="warehouseExamState">1,2,3,4,5</i></filter></request>',    
	@closeDefinition  nvarchar(max) =N'0 marks',    
	@assignedQualIdList  nvarchar(max) =N'205,203,41,69,199,18,200,77,201,202,141,147,82,109,150,120,33,39,23,40,159,50,22,36,71,30,37,72,19,80,175,107,100,35,118,148,138,42,197,6,193,27,85,113,49,124,128,144,131,25,96,92,24,88,45,170,164,178,154,184,160,156,167,181,139,142,163,91,99,106,117,21,84,111,47,122,89,198,32,56,185,195,186,162,212,187,29,90,55,28,87,112,54,123,43,86,114,53,125,206,44,97,126,224,146,74,132,165,176,179,168,183,172,135,52,137,10,78,104,188,207,208,209,93,189,15,95,127,145,133,26,48,57,190,14,83,110,151,121,191,79,105,98,116,20,173,174,153,158,143,129,38,134,211,210,166,171,180,149,155,140,161,152,157,169,182,194,94,81,177,108,101,119,70,76,192',  
	@gradeList	nvarchar(max) =N'''Fail'',''Pass'',''Did not Meet'',''na'''

    
BEGIN    
	-- SET NOCOUNT ON added to prevent extra result sets from    
	-- interfering with SELECT statements.    
	SET NOCOUNT ON;    

	DECLARE @errorReturnString nvarchar(max)    
	DECLARE @errorNum  nvarchar(100)    
	DECLARE @errorMess nvarchar(max)    

	BEGIN TRY    

		set dateformat dmy;    

		DECLARE @spReturnString    nvarchar(max)    

		/*    
		paging vars    
		*/    
		declare @PageSize     int    
		declare @PageNumber     int    
		declare @TotalCount     int     
		declare @TotalResultCount   int     
		declare @RowStart     int     
		declare @RowEnd      int       

		-- select vars    
		declare @tempTableInsert varchar(max)     
		declare @filterType     varchar(3)    
		Declare @sorting     varchar(max)    
		declare @selectDetails    varchar(max)    
		declare @whereclause    varchar(max)    
		declare @dateRangeWhereClause  varchar(max)    
		declare @pagingwhereclause   varchar(100)    
		declare @sqlPrePaging    nvarchar(max)    
		declare @sqlPostPaging    nvarchar(max)    
		declare @xmlUserSearch    varchar(max)    
		declare @orderby     varchar(max)    
		Declare @raiserror     varchar(max)    
		Declare @dateRangeType    varchar(max)    

		--create filter holding variables    
		DECLARE @surname     nvarchar(max)    
		DECLARE @forename varchar(max)
		DECLARE @centreName     nvarchar(max)    
		DECLARE @examName     nvarchar(max)    
		DECLARE @completedDateStartRange    nvarchar(max)    
		DECLARE @completedDateEndRange    nvarchar(max)    
		DECLARE @candidateRef    nvarchar(max)    
		DECLARE @uln    nvarchar(max)
		DECLARE @keycode   nvarchar(max)
		DECLARE @warehouseExamStates    nvarchar(10)  

		DECLARE @examResultPercentage nvarchar(max)
		DECLARE @examResultPercentageStart nvarchar(max)
		DECLARE @examResultPercentageEnd nvarchar(max)

		DECLARE @pass      bit    
		DECLARE @fail      bit    
		DECLARE @void      bit    
		DECLARE @close     bit    
		DECLARE @na        bit    

		DECLARE @gradeCaseStatement nvarchar(max)

		--Open the filter XML passed into the sp    
		declare @hdoc  int    

		exec sp_xml_preparedocument @hdoc OUTPUT, @filterXml    

		-- initialise the filter values    
		--SET filter vars    
		--SET filter vars    

		SELECT  @PageNumber    = pageIndex,     
		@PageSize     = pageSize,     
		@filterType     = filterType,    
		@surname     = [surname],    
		@forename     = [forename],       
		@centreName     = [centreName],    
		@examName     = [examName],    
		@completedDateStartRange    = [completedDateStartRange],
		@completedDateEndRange    = [completedDateEndRange],    
		@pass      = [pass],     
		@fail      = [fail],    
		@void      = [void],    
		@close      = [close],       
		@na         = [na],
		@sorting     = [sorting],    
		@raiserror     = [raisError],    
		@dateRangeType    = [dateRangeType],    
		@candidateRef    = [candidateRef],    
		@uln           = [uln],
		@keycode     = [keycode],
		@warehouseExamStates = [warehouseExamState],
		@examResultPercentage = [examResultPercentage],
		@examResultPercentageStart = [examResultPercentageStart],
		@examResultPercentageEnd = [examResultPercentageEnd]

		FROM    
		OPENXML(@hdoc, '//filter',1)    
		WITH (    
				pageSize     int,    
				pageIndex     int,    
				filterType     nvarchar(3),    
				[surname]     nvarchar(max) './i[@name="surname"]',    
				[forename]     nvarchar(max) './i[@name="forename"]',       
				[centreName]    nvarchar(max) './i[@name="centreName"]',    
				[examName]     nvarchar(max) './i[@name="examName"]',    
				[completedDateStartRange]   nvarchar(max) './i[@name="completedDateStartRange"]',    
				[completedDateEndRange]    nvarchar(max) './i[@name="completedDateEndRange"]',    
				[pass]      bit    './i[@name="pass"]',    
				[fail]      bit    './i[@name="fail"]',    
				[void]      bit    './i[@name="void"]',    
				[close]     bit    './i[@name="close"]',    
				[na]        bit    './i[@name="na"]',
				[sorting]     varchar(max) './i[@name="sorting"]',    
				[raiserror]     varchar(max) '@raiserror',    
				[dateRangeType]    varchar(max) '@dateRangeType',    
				[candidateRef]    varchar(max) './i[@name="candidateRef"]',    
				[uln]      varchar(max) './i[@name="uln"]',
				[keycode]      varchar(max) './i[@name="keycode"]',
				[warehouseExamState]  nvarchar(10) './i[@name="warehouseExamState"]',
				[examResultPercentage] nvarchar(max)  './i[@name="examResult"]',
				[examResultPercentageStart] nvarchar(max)  './i[@name="examResultStartRange"]' ,
				[examResultPercentageEnd] nvarchar(max)  './i[@name="examResultEndRange"]'
			)    


		-- remove the xml document from memory as soon as finishing using to ensure it is run     
		-- in all cases (including when an error is reaised)    
		exec sp_xml_removedocument @hdoc    

		--SET row request vars    
		SET @RowStart = @PageSize * @PageNumber +1;    
		SET @RowEnd = @RowStart + @PageSize -1;    

		exec sp_xml_preparedocument @hdoc OUTPUT, @centreQualLevelXML    

		

		CREATE TABLE ##TEMP_CentreQualificationLevels_TABLE     
			(    
				[qualLevel] int,    
				[centreId] int    
			)    

		INSERT INTO ##TEMP_CentreQualificationLevels_TABLE     
			SELECT  [level], [centre]  
			FROM    
			OPENXML(@hdoc, '//list/i',1)    
			WITH (    
					[level]      int  '@level',    
					[centre]     int  '@centre'    
				 )    

		create nonclustered index idx_TEMP_CentreQualificationLevels_TABLE_qualLevel_centreId on #TEMP_CentreQualificationLevels_TABLE (qualLevel,centreId)
		create nonclustered index idx_TEMP_CentreQualificationLevels_TABLE_qualLevel_CI on #TEMP_CentreQualificationLevels_TABLE (qualLevel) INCLUDE (centreId)

		exec sp_xml_removedocument @hdoc    

		-- Improved start

		DECLARE @myCentreQualClause nvarchar(max)      
		SET @myCentreQualClause = ''    
		DECLARE @TempQualLevelCount BIGINT, @WHSETQualLevelCount BIGINT
			select @TempQualLevelCount = COUNT(DISTINCT qualLevel) FROM #TEMP_CentreQualificationLevels_TABLE WHERE centreId = 1
			SELECT @WHSETQualLevelCount = COUNT(DISTINCT qualificationLevel) FROM WAREHOUSE_ScheduledExamsTable WITH (NOLOCK)
		--select @TempQualLevelCount, @WHSETQualLevelCount

		IF @TempQualLevelCount < @WHSETQualLevelCount	
		BEGIN    
		SET @myCentreQualClause = ' AND EXISTS(SELECT TOP (1) [qualLevel] FROM #TEMP_CentreQualificationLevels_TABLE WHERE qualLevel=dbo.WAREHOUSE_ExamSessionTable_Shreded.QualificationLevel AND centreId=1   
			UNION SELECT TOP (1) [qualLevel] FROM #TEMP_CentreQualificationLevels_TABLE WHERE qualLevel=dbo.WAREHOUSE_ExamSessionTable_Shreded.QualificationLevel
				AND centreId=dbo.WAREHOUSE_ExamSessionTable_Shreded.centreId)'     
		END    
		-- Improved end

		IF @sorting = 'qualification ASC, examName ASC, candidateSurname ASC, candidateForename ASC'
		BEGIN
			SET @sorting = 'completedDate DESC'
		END


		DECLARE @myQualClause nvarchar(max)    
		SET @myQualClause = ''    
		IF(LEN(@qualificationIdList) > 0)    
			BEGIN    
				SET @myQualClause = ' AND (WAREHOUSE_ExamSessionTable_Shreded.qualificationID IN (' + @qualificationIdList + '))'    
			END    


		SET @gradeCaseStatement = 'case when wset.SuppressResultsScreenResult = 1 AND previousExamState != 10 then ''na'' else grade end'

		DECLARE @myGradeClause nvarchar(max)
		SET @myGradeClause = ''
		IF LEN(@gradeList) > 0
			BEGIN
				set @myGradeClause = @myGradeClause + ' AND '+@gradeCaseStatement+' IN (' + @gradeList + ') '
				set @void = 1
			END  

		DECLARE @myAssignedQualClause nvarchar(max)    
		SET @myAssignedQualClause = ''    
		IF(LEN(@assignedQualIdList) > 0)    
			BEGIN    
				SET @myAssignedQualClause = ' AND (WAREHOUSE_ExamSessionTable_Shreded.qualificationID IN (' + @assignedQualIdList + '))'    
			END    

		DECLARE @myUlnClause nvarchar(max)    
		SET @myUlnClause = ''    
		IF @uln <> '%'    
			BEGIN    
				SET @myUlnClause = 'AND  (ULN LIKE ''' + @uln + ''')'    
			END

		DECLARE @myKeycodeClause nvarchar(max)    
		SET @myKeycodeClause = ''    
		IF @keycode <> '%'    
			BEGIN    
				SET @myKeycodeClause = 'AND  (KeyCode LIKE ''' + @keycode + ''')'    
			END      

		DECLARE @myCandidateRefClause nvarchar(max)    
		SET @myCandidateRefClause = ''    
		IF @candidateRef <> '%'    
			BEGIN    
				SET @myCandidateRefClause = 'AND CandidateRef LIKE ''' + @candidateRef + ''''    
			END

		DECLARE @myExamResultPercentageClause Nvarchar(max)
		IF @examResultPercentage <> ''
			SET @myExamResultPercentageClause = ' AND examResult = ' + @examResultPercentage + ' AND SuppressResultsScreenPercent = 0'  
		ELSE IF @examResultPercentageStart <> ''
			SET @myExamResultPercentageClause = ' AND examResult >= ' + @examResultPercentageStart + ' AND examResult  <= ' + @examResultPercentageEnd + ' AND SuppressResultsScreenPercent = 0' 
		ELSE
			SET @myExamResultPercentageClause = ''

		DECLARE @ForenameClause Nvarchar(max)
		SET @ForenameClause = ''
		IF @forename <> '%'
			SET @ForenameClause  = ' AND forename like '''+@forename+ '''' 


		-- Mani
		DECLARE @SurNameClause Nvarchar(max)
		SET @SurNameClause = ''
		IF @surname <> '%'
			SET @SurNameClause  =  'AND Surname LIKE ''' + @surname + ''''

		DECLARE @CentreNameClause Nvarchar(max)
		SET @CentreNameClause = ''
		IF @centreName <> '%'
			SET @CentreNameClause  =  'AND centreName LIKE ''' + @centreName + ''''

		DECLARE @ExamNameClause Nvarchar(max)
		SET @ExamNameClause = ''
		IF @examName <> '%'
			SET @ExamNameClause  =  'AND WAREHOUSE_ExamSessionTable_Shreded.examName LIKE ''' + @examName + ''''



		-- Create a temporary table to keep selected test data for the group    
		--CREATE TABLE #TEMP_ExamDetailsSearch_WAREHOUSE_TABLE    
		DECLARE @TEMP_ExamDetailsSearch_WAREHOUSE_TABLE AS TABLE
		( 
			[RowNumber]	int,
			[examSessionId]      int,    
			[examName]     nvarchar(200),
			[previousexamState] tinyint,
			[examStateInformation] xml,
			[foreName]    nvarchar(50),    
			[surName]    nvarchar(50),    
			[centreName]    nvarchar(100),    
			[qualificationName]   nvarchar(100),    
			[submittedDate]    smalldatetime,    			
			[resultData] xml,
			[scheduledDurationValue] decimal,	
			[candidateRef]    nvarchar(300),    
			[uln] nvarchar(10),
			[keycode]      nvarchar(300),   
			[externalReference]  nvarchar(100), 
			[qualificationRef] nvarchar(100), 
			[CQN] nvarchar(20), 
			[examResult]    float,  
			[examType] int,
			[containsBTLOffice] bit,
			[warehouseExamState] tinyint,
			[grade]	nvarchar(100),
			[originalGrade] nvarchar(50),
			[adjustedGrade] nvarchar(50),
			[userPercentage] float,
			[userMark] float,
			[targetedForVoid] xml,
			[isExternal] bit,
			[DisableReportingCandidateReport] bit,
			[DisableReportingSummaryReport] bit,
			[DisableReportingCandidateBreakdownReport] bit,
			[DisableReportingExamBreakdownReport] bit,
			[DisableReportingResultSlip] bit

			UNIQUE CLUSTERED (examSessionId)
		) 

		--REPLACE SORTING WITH DB COLS    
		SET @sorting = REPLACE(@sorting, 'qualification', 'qualificationName')
		--SET @sorting = REPLACE(@sorting, 'examName', 'examName')
		SET @sorting = REPLACE(@sorting, 'candidateSurname', 'Surname')
		SET @sorting = REPLACE(@sorting, 'candidateForename', 'Forename')
		SET @sorting = REPLACE(@sorting, 'completedDate', 'SubmittedDate')
		--SET @sorting = REPLACE(@sorting, 'centreName', 'centreName')
		SET @sorting = REPLACE(@sorting, 'candidateReference', 'candidateRef')
		--SET @sorting = REPLACE(@sorting, 'uln', 'uln')
		--SET @sorting = REPLACE(@sorting, 'keycode', 'keycode')
		SET @sorting = REPLACE(@sorting, 'examResult DESC', 'SuppressResultsScreenPercent, userPercentage DESC')
		SET @sorting = REPLACE(@sorting, 'examResult ASC', 'userPercentage, SuppressResultsScreenPercent ASC')
		SET @sorting = REPLACE(@sorting, 'resultData', 'userMark')
		SET @sorting = REPLACE(@sorting, 'duration', 'scheduledDurationValue')
		SET @sorting = REPLACE(@sorting, 'gradeText', 'grade')


		-- SET the selection details. This should map to columns identified in the select xml above, it must include the full select. If all the data cannot be retreived at this level    
		-- then the procedure must be re designed. the use of subqueries and joins is advised for complex queries.    
		-- This select also include the sort order passed in    

		-- Added Mani to restrict year data   
		IF @completedDateStartRange IS NULL
			BEGIN
				SET @completedDateStartRange = CONVERT(VARCHAR(20),DateAdd(MONTH,-12,GETDATE()),103) + ' 00:00:00'
				--SET @sorting = 'SubmittedDate DESC'
			END   

		IF @completedDateEndRange IS NULL
			BEGIN
				SET @completedDateEndRange = CONVERT(VARCHAR(20),GETDATE(),103) + ' 00:00:00'
				--SET @sorting = 'SubmittedDate DESC'
			END	   
		--PRINT @completedDateStartRange
		--PRINT @completedDateEndRange

		--IF @completedDateStartRange = '' OR @completedDateEndRange = ''
		--	BEGIN
		--		SET @completedDateStartRange = CONVERT(VARCHAR(20),DateAdd(MONTH,-12,GETDATE()),103) + ' 00:00:00'
		--		SET @completedDateEndRange = CONVERT(VARCHAR(20),GETDATE(),103) + ' 00:00:00'
		--	END   

		set @selectDetails =     
			'select RowNumber, examSessionId, examName, foreName, surName, centreName, qualificationName, SubmittedDate, candidateRef, keycode, examResult, grade, userPercentage, userMark, scheduledDurationValue
			 from 
				(SELECT  ROW_NUMBER() OVER (ORDER BY ' + @sorting + ') as RowNumber,      
				 examSessionId, examName, foreName, surName, centreName, qualificationName, SubmittedDate, candidateRef, ULN, keycode, examResult, grade, userPercentage, userMark, scheduledDurationValue, SuppressResultsScreenPercent
					FROM     
						(select    
							examSessionId,     
							WAREHOUSE_ExamSessionTable_Shreded.examName,     
							Forename,     
							Surname,    
							CentreName,    
							WAREHOUSE_ExamSessionTable_Shreded.qualificationName,    
							submittedDate,
							CandidateRef,     
							ULN,
							KeyCode,    
							examResult,    
							'+@gradeCaseStatement+' as grade,
							case when wset.SuppressResultsScreenPercent = 0 then userPercentage else null end as userPercentage,
							case when wset.SuppressResultsScreenMark = 0 then userMark else null end as userMark, 
							scheduledDurationValue,
							wset.SuppressResultsScreenPercent
							FROM  WAREHOUSE_ExamSessionTable_Shreded WITH (NOLOCK)
							INNER JOIN WAREHOUSE_ScheduledExamsTable wset on WAREHOUSE_ExamSessionTable_Shreded.WAREHOUSEScheduledExamID = wset.ID'

		--declare @AdditionalRestriction nvarchar(1000)
		--SET @AdditionalRestriction = ' AND warehouseTime between ''' + CONVERT(VARCHAR(20),DATEADD(dd, 0, DATEDIFF(dd, 0, DATEADD(MONTH, - 12, GETDATE()))),120) + ''' AND ''' + CONVERT(VARCHAR(20),DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE())),120) + ''''
		--PRINT @AdditionalRestriction


		PRINT(@selectDetails)

		SET @dateRangeWhereClause = ' '
		-- handle date range types    
		IF @dateRangeType = 'warehouse'    
			BEGIN
				IF @completedDateStartRange != ''
					BEGIN
						SET @dateRangeWhereClause = ' AND WAREHOUSE_ExamSessionTable_Shreded.warehouseTime >= Convert(smalldatetime, ''' + @completedDateStartRange +''') '
					END
				IF @completedDateEndRange != ''
					BEGIN
						SET @dateRangeWhereClause = @dateRangeWhereClause + ' AND WAREHOUSE_ExamSessionTable_Shreded.warehouseTime <= Convert(smalldatetime, ''' + @completedDateEndRange +''') '
					END    

			END    
		ELSE    
			BEGIN  
				IF @completedDateStartRange != ''
					BEGIN
						SET @dateRangeWhereClause = ' AND submittedDate >= Convert(smalldatetime, ''' + @completedDateStartRange +''') '
					END
				IF @completedDateEndRange != ''
					BEGIN
						SET @dateRangeWhereClause = @dateRangeWhereClause + ' AND submittedDate <= Convert(smalldatetime, ''' + @completedDateEndRange +''') '
					END  

			END 


		BEGIN    
		set @whereclause =  ' WHERE WarehouseExamState = 1 '   
			 --+ @AdditionalRestriction +
			 + @dateRangeWhereClause + 			 
			 + @SurNameClause +
			 + @myCandidateRefClause +    
			 + @myUlnClause + 
			 + @myKeycodeClause +   
			 + @CentreNameClause +
			 + @ExamNameClause +
			 + @myCentreQualClause +     
			 + @myQualClause +     
			 + @myAssignedQualClause +  
			 + @myExamResultPercentageClause +
			 + @ForenameClause +
			 + @myGradeClause +
			 + '
			 AND (    

				(PreviousExamState!=10 AND (' + CONVERT(nvarchar(max),@pass) + ' =  1) AND    
					CAST(WAREHOUSE_ExamSessionTable_Shreded.passValue AS varchar(1000)) = 1)    -- PASS TICK box    

			 OR (PreviousExamState!=10 AND (' + CONVERT(nvarchar(max),@fail) + ' =  1) AND    
			   (CAST(WAREHOUSE_ExamSessionTable_Shreded.passValue AS varchar(1000)) = 0) AND    
				(CAST(WAREHOUSE_ExamSessionTable_Shreded.closeValue AS varchar(1000))  != 1)) -- FAIL TICK box    

			 OR (PreviousExamState!=10 AND (' + CONVERT(nvarchar(max),@na) + ' =  1) AND
			   (wset.SuppressResultsScreenResult = 1))

			 OR  PreviousExamState=10 AND (' + CONVERT(nvarchar(max),@void) + ' =  1)      -- VOID Tick Box    

			 OR  (PreviousExamState!=10 AND (' + CONVERT(nvarchar(max),@close) + ' =  1) AND    
			  (CAST(WAREHOUSE_ExamSessionTable_Shreded.closeValue AS varchar(1000))  = 1))    -- CLOSE Tick Box    

			  )    

			 ) as ExamDetails) as examSearch'    


		END    

		PRINT(@whereclause)


		-- build the pre and post paging strings    
		set @pagingwhereclause = ' WHERE RowNumber BETWEEN @RowStart AND @RowEnd ORDER BY ' + @sorting    

		print(@pagingwhereclause)
		--set @sqlPrePaging = 'SELECT @TotalResultCount =  Count(1) FROM (' + @selectDetails + @whereclause + ') as TotalRowCount  OPTION (RECOMPILE)'    

		set @sqlPrePaging = 'SELECT @TotalResultCount = (SELECT Count(1) FROM WAREHOUSE_ExamSessionTable_Shreded WITH (NOLOCK) INNER JOIN WAREHOUSE_ScheduledExamsTable wset on WAREHOUSE_ExamSessionTable_Shreded.WAREHOUSEScheduledExamID = wset.ID' + replace(@whereclause, 'as ExamDetails) as examSearch', ' ') --+ 'OPTION (RECOMPILE)' --+ ')'    		


		  --PRINT @sqlPrePaging

		--set @sqlPostPaging =  @tempTableInsert + @selectDetails + @whereclause + @pagingwhereclause    
		set @sqlPostPaging =  @selectDetails + @whereclause + @pagingwhereclause    

		-- ** we need to execute as an ansi string for the parameterised sorting to work **    
		exec sp_executesql @sqlPrePaging, N'@TotalResultCount INT OUTPUT', @TotalResultCount OUTPUT    

		--PRINT @TotalResultCount

	    --PRINT CHAR(13) + @selectDetails
	    --PRINT CHAR(13) + @whereclause
	    --PRINT CHAR(13) + @pagingwhereclause


		-- required variables set before paging applied    
		set @TotalCount = CEILING((@TotalResultCount * 1.0) / @PageSize)    

		--PRINT CHAR (13) + @sqlPostPaging

		-- execute post paging, this will select the page of records we are interested in.    

		INSERT INTO @TEMP_ExamDetailsSearch_WAREHOUSE_TABLE     
		  (
			  [RowNumber],
			  [examSessionId],    
			  [examName],    
			  [foreName],    
			  [surName],    
			  [centreName],    
			  [qualificationName],    
			  [submittedDate],    
			  [candidateRef],    
			  [keycode],    
			  [examResult],    
			  [grade],
			  [userPercentage],
			  [userMark],
			  [scheduledDurationValue]
		)      
		exec sp_executesql @sqlPostPaging, N'@RowStart INT, @RowEnd INT, @TotalResultCount INT, @TotalCount INT,    
		@PageNumber INT, @PageSize INT', @RowStart, @RowEnd, @TotalResultCount, @TotalCount, @PageNumber, @PageSize    

		select * from @TEMP_ExamDetailsSearch_WAREHOUSE_TABLE

		SELECT @RowStart, @RowEnd

		---------------START Nice XML PATH CODE---------------    
		   --The nested 'select for xml' statements are simply wrapping nodes round the inner statements    
		 SELECT '0' as '@errorCode', --Attribute to our root (btl standard 'result') node    
		 (    
		  SELECT @PageNumber as '@pageIndex', @PageSize as '@pageSize', @TotalCount as '@totalCount', @TotalResultCount as '@totalRecords', @closeDefinition as '@closeDef',    
		 (    
				SELECT
				TempTBL.[examSessionId]     AS 'id',
				TempTBL.[examName]    AS 'examName',
				[previousExamState]    AS 'examState',
				[examStateInformation] AS '*',
				REPLACE(TempTBL.[forename], '&apos;', '''')   AS 'candidateForename',
				REPLACE(TempTBL.[Surname], '&apos;', '''')   AS 'candidateSurname',
				TempTBL.[centreName]   AS 'centreName',
				TempTBL.[qualificationName]  AS 'qualification',
				TempTBL.[submittedDate]   AS 'completedDate',
				[resultData]   AS 'resultData',
				CAST(TempTBL.scheduledDurationValue as decimal) AS 'scheduledDuration',
				TempTBL.[candidateRef]   AS 'candidateRef',
				ISNULL([uln], '')     AS 'uln',
				TempTBL.[keycode]     AS 'keycode',
				WES.[externalReference]  AS 'externalReference',
				WES.[qualificationRef]  AS 'QualificationRef',
				TempTBL.[submittedDate]  AS 'completionDate',
				[CQN]     AS 'CQN',
				TempTBL.[examResult] AS 'examResult',
				[examType]    AS 'examType',
				ISNULL([containsBTLOffice], 0) AS 'containsBTLOffice',
				[WarehouseExamState]     AS 'WarehouseExamState',
				ISNULL(TempTBL.[grade], '') AS 'grade',
				[originalGrade],
				[adjustedGrade],
				ISNULL(CAST(TempTBL.[userPercentage] as varchar(30)), '') AS 'userPercentage',
				ISNULL(CAST(TempTBL.[userMark] as varchar(30)), '') AS 'userMark',
				ISNULL([TargetedForVoid],'') AS 'TargetedForVoid',
				WES.[IsExternal],
				WSE.DisableReportingCandidateReport AS 'disableReportingCandidateReport',
				WSE.DisableReportingSummaryReport AS 'disableReportingSummaryReport',
				WSE.DisableReportingCandidateBreakdownReport AS 'disableReportingCandidateBreakdownReport',
				WSE.DisableReportingExamBreakdownReport AS 'disableReportingExamBreakdownReport',
				WSE.DisableReportingResultSlip AS 'disableReportingResultSlip'

		   from @TEMP_ExamDetailsSearch_WAREHOUSE_TABLE TempTBL
				--inner join WAREHOUSE_ExamSessionTable_Shreded WES
				--on TempTBL.examSessionId = WES.examSessionId
				--inner join WAREHOUSE_ScheduledExamsTable WSE
				--on WES.WAREHOUSEScheduledExamID = WSE.ID

		   ORDER BY [RowNumber]     

		   FOR XML PATH('exam') ,TYPE         
		  )    
		  FOR XML PATH('return') ,TYPE --Next node in from root (btl standard 'return') node    
		 )    
		 FOR XML PATH('result')--Our outermost/root node    
		----------------END Nice XML PATH CODE ----------------    


		--clean up the sp    
		--DROP TABLE #TEMP_ExamDetailsSearch_WAREHOUSE_TABLE    
		DROP TABLE #TEMP_CentreQualificationLevels_TABLE    

	END TRY    
	BEGIN CATCH    
		IF @raiserror = 1    
		BEGIN    
			DECLARE @ErrorMessage NVARCHAR(4000);    
			DECLARE @ErrorSeverity INT;    
			DECLARE @ErrorState INT;    

			SELECT @ErrorMessage = ERROR_MESSAGE(),    
				@ErrorSeverity = ERROR_SEVERITY(),    
				@ErrorState = ERROR_STATE();    

				-- Use RAISERROR inside the CATCH block to return     
				-- error information about the original error that     
				-- caused execution to jump to the CATCH block.    
				RAISERROR (@ErrorMessage, -- Message text.    
					@ErrorSeverity, -- Severity.    
					@ErrorState -- State.    
					);    
		END    
		ELSE    
			EXEC sa_SHARED_GetErrorDetails_sp    
	END CATCH;

END    

