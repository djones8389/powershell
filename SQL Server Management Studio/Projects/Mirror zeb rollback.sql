--sp_whoisactive

USE master

DECLARE @KillSpids nvarchar(MAX) = '';

SELECT @KillSpids +=CHAR(13) + 'kill ' + convert(varchar(5),spid)
from sys.sysprocesses s
where DB_NAME(dbid) like 'OCR%'
	
EXEC(@KillSpids);

--RESTORE DATABASE [OCR_SecureAssess] FROM DISK = N'T:\BACKUP\OCR R11\OCR_SecureAssess.2017.06.20.bak' WITH FILE = 1, NOUNLOAD, NOREWIND, REPLACE
--	, MEDIAPASSWORD = 's_!85WrenukEtrut+4f5a#-a&ac*aKUs', STATS = 10, MOVE 'Data' TO N'S:\DATA\OCR_SecureAssess.mdf', MOVE 'Log' TO N'L:\LOGS\OCR_SecureAssess.ldf'; 

--RESTORE DATABASE [OCR_ItemBank] FROM DISK = N'T:\BACKUP\OCR R11\OCR_ItemBank.2017.06.20.bak' WITH FILE = 1, NOUNLOAD, NOREWIND, REPLACE
--	, MEDIAPASSWORD = 's_!85WrenukEtrut+4f5a#-a&ac*aKUs', STATS = 10, MOVE 'Data' TO N'S:\DATA\OCR_ItemBank.mdf', MOVE 'Log' TO N'L:\LOGS\OCR_ItemBank.ldf'; 

RESTORE DATABASE [OCR_SecureAssess] FROM DISK = N'T:\BACKUP\OCR_SecureAssess.bak' WITH FILE = 3, NOUNLOAD, NOREWIND, REPLACE
	,STATS = 10, MOVE 'Data' TO N'S:\DATA\OCR_SecureAssess.mdf', MOVE 'Log' TO N'L:\LOGS\OCR_SecureAssess.ldf'; 
	
RESTORE DATABASE [OCR_ItemBank] FROM DISK = N'T:\BACKUP\OCR_ItemBank.12.5.bak' WITH FILE = 1, NOUNLOAD, NOREWIND, REPLACE
	,STATS = 10, MOVE 'Data' TO N'S:\DATA\OCR_ItemBank.mdf', MOVE 'Log' TO N'L:\LOGS\OCR_ItemBank.ldf'; 

RESTORE DATABASE [Ocr_AnalyticsManagement]
FROM DISK = N'T:\Backup\R12.5 KH DBs\OCR_12.5.bak'
WITH FILE = 1
	,NOUNLOAD
	,NOREWIND
	,REPLACE
	,STATS = 10
	,MOVE 'Data' TO N'S:\DATA\Ocr_AnalyticsManagement.mdf'
	,MOVE 'Log' TO N'L:\LOGS\Ocr_AnalyticsManagement.ldf';

RESTORE DATABASE [Ocr_ContentAuthor]
FROM DISK = N'T:\Backup\R12.5 KH DBs\OCR_12.5.bak'
WITH FILE = 2
	,NOUNLOAD
	,NOREWIND
	,REPLACE
	,STATS = 10
	,MOVE 'Data' TO N'S:\DATA\Ocr_ContentAuthor.mdf'
	,MOVE 'Log' TO N'L:\LOGS\Ocr_ContentAuthor.ldf';

RESTORE DATABASE [Ocr_SurpassDataWarehouse]
FROM DISK = N'T:\Backup\R12.5 KH DBs\OCR_12.5.bak'
WITH FILE = 3
	,NOUNLOAD
	,NOREWIND
	,REPLACE
	,STATS = 10
	,MOVE 'Data' TO N'S:\DATA\Ocr_SurpassDataWarehouse.mdf'
	,MOVE 'Log' TO N'L:\LOGS\Ocr_SurpassDataWarehouse.ldf';

RESTORE DATABASE [Ocr_SurpassManagement]
FROM DISK = N'T:\Backup\R12.5 KH DBs\OCR_12.5.bak'
WITH FILE = 4
	,NOUNLOAD
	,NOREWIND
	,REPLACE
	,STATS = 10
	,MOVE 'Data' TO N'S:\DATA\Ocr_SurpassManagement.mdf'
	,MOVE 'Log' TO N'L:\LOGS\Ocr_SurpassManagement.ldf';