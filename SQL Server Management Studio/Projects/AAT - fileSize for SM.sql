SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

IF OBJECT_ID('tempdb..#Results') IS NOT NULL DROP TABLE #Results;

;WITH exams AS (
SELECT TOP 5000 ID
FROM dbo.WAREHOUSE_ExamSessionTable
WHERE ExportToSecureMarker = 1
	ORDER BY id DESC
	)



SELECT A.*
	, DATEDIFF(MINUTE, A.MinDate,A.MaxDate) Diff
	--, DL
INTO #Results
FROM (
	SELECT wesat.WarehouseExamSessionID
		, MIN(wesat.Date) MinDate
		, MAX(wesat.date) MaxDate
	FROM dbo.WAREHOUSE_ExamStateAuditTable wesat
	INNER JOIN exams  E
	ON E.ID = wesat.WarehouseExamSessionID
	WHERE EXISTS (
			SELECT *
			FROM WAREHOUSE_ExamStateAuditTable A
			WHERE a.WarehouseExamSessionID = wesat.WarehouseExamSessionID
				AND a.ExamState = 2
		)
			AND EXISTS (
			SELECT *
			FROM WAREHOUSE_ExamStateAuditTable A
			WHERE a.WarehouseExamSessionID = wesat.WarehouseExamSessionID
				AND a.ExamState = 3
		)
	AND wesat.ExamState IN (2,3)
	GROUP BY WarehouseExamSessionID
) A
INNER JOIN (
	SELECT wesdt.warehouseExamSessionID
		, SUM(DATALENGTH(document)) DL
    FROM dbo.WAREHOUSE_ExamSessionDocumentTable wesdt
	GROUP BY wesdt.warehouseExamSessionID
) B
ON a.WarehouseExamSessionID = b.warehouseExamSessionID	


SELECT TOP 1 * 
FROM #Results
