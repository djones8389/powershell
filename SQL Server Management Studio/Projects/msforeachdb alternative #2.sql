IF OBJECT_ID('tempdb..##DATALENGTH') IS NOT NULL DROP TABLE ##DATALENGTH;

CREATE TABLE ##DATALENGTH (
	client nvarchar(1000)
	, WAREHOUSEExamSessionID int
	, ItemID nvarchar(20)
	, CID tinyint
);

DECLARE @dynamic nvarchar(MAX)='';

select @dynamic +=CHAR(13)+ '
use '+QUOTENAME([NAME])+'

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

INSERT ##DATALENGTH
select c.*
FROM (

select  db_name() Client
	, WAREHOUSEExamSessionID
	,   ItemID
	,	a.b.value(''@id'',''tinyint'') CID
from WAREHOUSE_ExamSessionItemResponseTable
INNER JOIN (select ID
			from WAREHOUSE_ExamSessionTable
			where warehouseTime > DATEADD(MONTH, -6, getDATE())
) d
on d.ID = WAREHOUSE_ExamSessionItemResponseTable.WAREHOUSEExamSessionID
cross apply ItemResponseData.nodes(''p/s//c'') a(b)
where a.b.value(''@id'',''tinyint'') is not null
 ) C
group by Client
	,  WAREHOUSEExamSessionID
	,   ItemID
	,	CID
having count(*) > 1
'
from sys.databases
where state_desc = 'ONLINE'
	and name like '%[_]SecureAssess';

exec(@dynamic);



select *
from ##DATALENGTH
order by 1





