DECLARE @TableUpdater TABLE (
	ID INT
	, version tinyint
	, CentreCode nvarchar(250)
	, CentreName nvarchar(250)
)

INSERT @TableUpdater
 select  
  ct.ID
  , ct.Version
  , ct.CentreCode
  , ct.CentreName
 from CentreTable CT
 INNER JOIN WAREHOUSE_CentreTable WCT
 on CT.ID = WCT.CentreID
   and ct.version = wct.version
 where CT.CentreName != WCT.CentreName;

update WAREHOUSE_CentreTable
set WAREHOUSE_CentreTable.CentreName = TU.CentreName
	, WAREHOUSE_CentreTable.CentreCode = TU.CentreCode
FROM WAREHOUSE_CentreTable
INNER JOIN @TableUpdater TU
ON TU.ID = WAREHOUSE_CentreTable.CentreID
	and TU.version = WAREHOUSE_CentreTable.version;

update WAREHOUSE_ExamSessionTable_Shreded
set WAREHOUSE_ExamSessionTable_Shreded.centreName = TU.CentreName
	, WAREHOUSE_ExamSessionTable_Shreded.centreCode = TU.centreCode
from WAREHOUSE_ExamSessionTable_Shreded
INNER JOIN @TableUpdater TU
ON TU.ID = WAREHOUSE_ExamSessionTable_Shreded.CentreID;

	