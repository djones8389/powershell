Select     
	CASE 
	   WHEN ServerName = '704276-PREVSTA1' then 'PRP-PRV' 
	   WHEN ServerName = '430069-AAT-SQL\SQL1' then 'AAT'
	   WHEN ServerName = '430327-AAT-SQL2\SQL2' then 'AAT'
	   WHEN ServerName = '430088-BC-SQL\SQL1' then 'BritishCouncil'
	   WHEN ServerName = '430326-BC-SQL2\SQL2' then 'BritishCouncil'
	   WHEN ServerName = '553092-PRDSQL' then 'Editions Live'
	   WHEN ServerName = '524778-SQL' then 'Editios Mirror'
	   WHEN ServerName = '425732-OCR-SQL\SQL1' then'OCR'
	   WHEN ServerName = '430325-OCR-SQL2\SQL2' then'OCR'
	   WHEN ServerName = '311619-WEB3' then 'WJEC'
	   WHEN ServerName = '335657-APP1' then 'SQA'
	   WHEN ServerName = '335658-APP2' then 'SQA'
	   WHEN ServerName = '420304-EAL2-LIV' then 'EAL'
	   WHEN ServerName = '338301-WEB4' then'Skillsfirst'
	   WHEN ServerName = '343553-WEB5' then'AQA'
	   WHEN ServerName = '407710-WEB3' then'NCFE'
		    ELSE ServerName  
		END AS ServerName
		, MaintenancePlans
FROM  PSCollector.dbo.FindMaintenancePlans (NOLOCK)
order by 1 desc;


--DROP TABLE PSCollector.dbo.FindMaintenancePlans