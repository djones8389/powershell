USE master
GO
ALTER DATABASE [AdventureWorks2008R2]
   ADD FILEGROUP [Secondary];
GO
ALTER DATABASE [AdventureWorks2008R2] 
ADD FILE 
(
    NAME = NewTable1,
    FILENAME = 'D:\DATA\NewTable1.ndf',
    SIZE = 5MB,
    MAXSIZE = 100MB,
    FILEGROWTH = 5MB
)
--,
--(
--    NAME = NewTable2,
--    FILENAME = 'D:\DATA\NewTable2.ndf',
--    SIZE = 5MB,
--    MAXSIZE = 100MB,
--    FILEGROWTH = 5MB
--)
TO FILEGROUP [Secondary];
GO




CREATE UNIQUE CLUSTERED INDEX [PK_BusinessEntity_BusinessEntityID] ON [Person].[BusinessEntity]
(
	[BusinessEntityID] ASC
) 

WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, DROP_EXISTING = ON)

ON [Secondary]









