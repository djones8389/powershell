USE [Saxion_SurpassDataWarehouse]
GO
/****** Object:  StoredProcedure [ETL].[sp_stgPopulateStgFactComponentResponses]    Script Date: 13/07/2017 15:27:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROC [ETL].[sp_stgPopulateStgFactComponentResponses] AS BEGIN
		TRUNCATE TABLE ETL.stg_FactComponentResponses;
			WITH	cte
					  AS ( SELECT	ExamSessionKey,
									Section,
									CPID,
									CPVersion,
									Section.response.value('@id','nvarchar(50)') SceneID,
									Component.response.value('@id','nvarchar(50)') ComponentID,
									Component.response.value('@typ','int') QuestionType,
									Component.response.value('@um','nvarchar(100)') ComponentUserMark,
									Component.response.query('.') ComponentRawResponse
						   FROM		ETL.stg_FactQuestionResponses FQR
									CROSS APPLY FQR.RawResponse.nodes('p/s') Section ( response )
									CROSS APPLY Section.response.nodes('c[@typ<99]') Component ( response )
						   WHERE	FQR.UseForMerge = 1
						 )
			--INSERT	INTO ETL.stg_FactComponentResponses
			--		( [ExamSessionKey],
			--		  [Section],
			--		  [CID],
			--		  [SID],
			--		  [CPID],
			--		  [CPVersion],
			--		  [Mark],
			--		  [RawResponse],
			--		  [DerivedResponse],
			--		  [ShortDerivedResponse]
			--		)
					SELECT	ExamSessionKey,
							Section,
							CPID + 'S' + SceneID + 'C' + ComponentID [CID],
							CPID + 'S' + SceneID [SID],
							CPID,
							CPVersion
		--,CAST(SceneID as int) SceneID
		--,CAST(ComponentId as int) ComponentId
		--,QuestionType
							,
							CASE WHEN ISNUMERIC(ComponentUserMark) = 1 THEN CAST(ComponentUserMark AS FLOAT)
								 ELSE NULL
							END ComponentUserMark,
							ComponentRawResponse,
							NULL,
							ETL.stg_fn_getShortComponentResponse(ComponentRawResponse,QuestionType,' | ') ShortDerivedResponse
					FROM	cte
					where cpid = '381P631';

		UPDATE	FCR
		SET		DerivedResponse = CASE DC.HasCPAuditEntry
									WHEN 1 THEN ETL.stg_fn_getComponentResponse(FCR.RawResponse,DC.ItemXML,DC.QuestionType,FCR.ShortDerivedResponse,' | ')
									ELSE FCR.ShortDerivedResponse
								  END,
				ResponseKey = CASE DC.HasCPAuditEntry
								WHEN 1 THEN ETL.stg_fn_getComponentResponseKey(FCR.RawResponse,DC.QuestionType,',')
								ELSE FCR.ShortDerivedResponse
							  END
		FROM	ETL.stg_FactComponentResponses FCR
				JOIN DimComponents DC ON FCR.CID = DC.CID
										 AND FCR.CPVersion = DC.CPVersion;	
	END




USE Saxion_SecureAssess

SELECT resultDataFull
FROM WAREHOUSE_ExaMSessioNTable
where id = 3368

SELECT *
FROM CPAUdittable
where itemid = '381P631'

--UPDATE WAREHOUSE_ExaMSessioNTable
--Set WarehouseExamState = 5
--where id = 3368
--UPDATE WAREHOUSE_ExaMSessioNTable_Shreded
--Set WarehouseExamState = 5
--where examsessionid = 3368


USE Saxion_ContentAuthor

--<item id="381P631" name="FSKM201E Q10"

select top 1 *
from Items A
where A.Name = 'FSKM201E Q10'