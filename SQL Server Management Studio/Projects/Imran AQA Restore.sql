use master;

declare @kill nvarchar(max)='';
select @kill +=CHAR(13)+ 'KILL ' + convert(varchar(3),s.spid)
from sys.databases D
INNER JOIN sys.sysprocesses S
on D.database_id = s.dbid
where d.name = 'SANDBOX_AQA_SecureAssess'

exec(@kill);

RESTORE DATABASE [SANDBOX_AQA_SecureAssess] FROM DISK = N'T:\FTP\AQA\AQATraining.2016.10.13.bak' 
WITH FILE = 3, NOUNLOAD, NOREWIND, REPLACE, MEDIAPASSWORD = 's_!85WrenukEtrut+4f5a#-a&ac*aKUs', STATS = 10
, MOVE 'SecureAssess7.1' TO N'S:\DATA\SANDBOX_AQA_SecureAssess.mdf'
, MOVE 'SecureAssess7.1_log' TO N'L:\LOGS\SANDBOX_AQA_SecureAssess.ldf'; 

USE [SANDBOX_AQA_SecureAssess]; 

BEGIN
	IF NOT EXISTS(SELECT 1 FROM sys.database_principals WHERE name = N'LON\3168686-Admins')
	BEGIN
		CREATE USER [LON\3168686-Admins] FOR LOGIN [LON\3168686-Admins]
	END

	IF EXISTS(SELECT 1 FROM sys.database_principals WHERE name = N'LON\3168686-Admins')
	BEGIN
		EXEC sp_addrolemember N'db_owner', N'LON\3168686-Admins'
		DENY BACKUP DATABASE TO [LON\3168686-Admins];
		DENY BACKUP LOG TO [LON\3168686-Admins];
		DENY TAKE OWNERSHIP TO [LON\3168686-Admins];
	END

	IF NOT EXISTS(SELECT 1 FROM sys.database_principals WHERE name = N'Powershell')
		CREATE USER [Powershell] FOR LOGIN [Powershell]
		EXEC sp_addrolemember N'db_owner', N'Powershell'
	IF NOT EXISTS(SELECT 1 FROM sys.database_principals WHERE name = N'SANDBOX_AQA_SecureAssess_User')
		CREATE USER [SANDBOX_AQA_SecureAssess_User] FOR LOGIN [SANDBOX_AQA_SecureAssess_Login]
		EXEC sp_addrolemember N'db_owner', N'SANDBOX_AQA_SecureAssess_User'
END