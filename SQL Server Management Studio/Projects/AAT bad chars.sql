SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

select a.*
	, d.e.value('.[1]','nvarchar(max)') 
	--, resultdata
from (

select west.ID	
	, west.KeyCode
	, ItemID
	, west.warehouseTime
	--, resultdata
	, cast(replace(replace(cast(ItemResponseData as nvarchar(MAX)), '&lt;','<'), '&gt;','>') as xml) [ItemResponseData]
from WAREHOUSE_ExamSessionItemResponseTable WESIRT
INNER JOIN WAREHOUSE_ExamSessionTable WEST
ON WEST.ID = WESIRT.WAREHOUSEExamSessionID
	where warehouseTime > DATEADD(MONTH, -3, getDATE())
	--where KeyCode = '9CJCDLA6'
) a
 CROSS APPLY [ItemResponseData].nodes('p/s/c/i/table') b(c)
 CROSS APPLY b.c.nodes('r/c') d(e)
 
 where b.c.value('@extensionName', 'nvarchar(100)') in ('protean','HIGHLIGHT_TABLE')
	and (
		 d.e.value('.[1]','nvarchar(max)') like '%&lt;%'
		 or
		 d.e.value('.[1]','nvarchar(max)') like '%]%'
		 or
		 d.e.value('.[1]','nvarchar(max)') like '%,%'
		 or
		 d.e.value('.[1]','nvarchar(max)') like '%>%'
		 or
		 d.e.value('.[1]','nvarchar(max)') like '%/%'
		 or
		 d.e.value('.[1]','nvarchar(max)') like '%|%'
		 )

	--FYA (100% &lt;75g/km)




--where warehouseTime > DATEADD(MONTH, -6, GETDATE())
	--and
	--(
	--cast(itemresponsedata as nvarchar(MAX)) like '%]%'
	--or
	--cast(itemresponsedata as nvarchar(MAX)) like '%&amp;lt;%'
	--)
