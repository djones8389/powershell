DECLARE @command1 nvarchar(1000) = '

USE [?];

select ''?''  as DBName
		, u.name as ''User''
		, (select SUSER_SNAME (u.sid)) as ''Login''
		, r.name  as RoleName
		--, u.*
from sys.database_role_members RM 
	right join sys.database_principals U on U.principal_id = RM.member_principal_id
	left join sys.database_principals R on R.principal_id = RM.role_principal_id
where u.type<>''R''
	and U.type = ''S''
	and r.name IS NOT NULL
	and ''?'' not in (''msdb'',''tempdb'',''model'',''master'')
'

DECLARE @UserLogins TABLE (DBName nvarchar(150), userName nvarchar(150), loginName nvarchar(150), RoleName nvarchar(50))
INSERT @UserLogins
EXEC sp_MSforeachdb  @command1

DECLARE @prefix nvarchar(4) = 'PRV_'

DECLARE @PrvPrefix nvarchar(4) = 'PRV_';

/*Alter DB Names*/

select --DISTINCT
	-- '--'+A.Name as DBName
	--, PRV as RenameDB
	--, '--'+loginName
	--, b.CreateLogin
	--, b.CreateUser
	 AssignDBO
FROM
(

SELECT DISTINCT
	Name
	, 'USE ' + name + '; EXEC sp_renamedb ''' + name + ''', ''' + @PrvPrefix + name + ''';' as PRV
FROM sys.databases
) A

INNER JOIN (

SELECT A.DbName
, A.loginName
, 'IF(SUSER_ID('+QUOTENAME(@prefix + A.Dbname + '_Login' /*+ p.name*/,'''')+') IS NULL)BEGIN CREATE LOGIN '+QUOTENAME(@prefix + A.Dbname + '_Login')-- + p.name)
   +
       CASE WHEN p.type_desc = 'SQL_LOGIN'
            THEN ' WITH PASSWORD = '+CONVERT(NVARCHAR(MAX),L.password_hash,1)+' HASHED'
            ELSE ' FROM WINDOWS'
       END + ';/*'+p.type_desc+'*/ END;
       '
       COLLATE SQL_Latin1_General_CP1_CI_AS
       as CreateLogin
       , 'USE [' + @prefix +  A.DBNAME + ']; CREATE USER '+ @prefix + A.Dbname + '_' + 'User' +' FOR LOGIN '+ @prefix + A.Dbname + '_' + 'Login'+' '
       as CreateUser
       , 'EXEC sp_addrolemember ' + ''''+ A.roleName + '''' + ',' + ''''+ @prefix + A.Dbname + '_' + 'User' + ''''+  '      '  --'''Production''', ''+@prefix + A.Dbname + '_Login'+''
       as AssignDBO
       --EXEC sp_addrolemember 'Production', 'Mary5';
  FROM sys.server_principals AS p
  LEFT JOIN sys.sql_logins AS L
    ON p.principal_id = L.principal_id
  INNER JOIN @UserLogins A
  On A.loginName COLLATE SQL_Latin1_General_CP1_CI_AS = P.name
WHERE p.type_desc IN ('SQL_LOGIN','WINDOWS_GROUP','WINDOWS_LOGIN')
   AND p.name NOT IN ('SA')
   AND p.name NOT LIKE '##%##'

) B

ON A.Name = B.DBName

