DBCC FREEPROCCACHE;
DBCC DROPCLEANBUFFERS;

DECLARE @subjects VARCHAR(MAX) = '103,104,110,112,116,121,129,130,133,135,136,137,140,142,144,145,146,148,150,156,161,162,168,173,187,191,195,198,204,205,209,210,214,220,223'

SELECT 
	DP.QualificationKey '@qualificationKey'
	,DQ.ProjectKey '@projectKey'
	,DQ.ExternalID '@id'
	,ItemParentKey '@parentId'
	,QuestionName '@name'
FROM 
	DimQuestions DQ
	JOIN DimProjects DP ON DP.ProjectKey = DQ.ProjectKey
WHERE 
	DQ.CAQuestionTypeKey=1
	AND Source=2
	AND Deleted=0
	AND QualificationKey IN (SELECT Value FROM dbo.fn_ParamsToList(@subjects,0))
FOR XML PATH('folder'), ROOT('folders')
OPTION (MAXRECURSION 0)
