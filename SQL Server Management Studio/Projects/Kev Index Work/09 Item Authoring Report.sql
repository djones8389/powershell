DBCC FREEPROCCACHE;
DBCC DROPCLEANBUFFERS;

DECLARE @subject INT = 223;
DECLARE @datefrom DATE = '2000-01-01';
DECLARE @dateto DATE = '2015-12-31';
DECLARE @folder INT = NULL;
DECLARE @status INT = 0;
--------------------------------------------------------
--If, for whatever reasons, the latest item version 
--is transferred from SecureAssess, not Content Author, 
--the item will not be displayed in the report
--------------------------------------------------------

--Added since Analytics cannot handle NULL parameters
IF @folder = -2 SET @folder = NULL
IF @status = -2 SET @status = NULL


IF OBJECT_ID('tempdb..#Items') IS NOT NULL DROP TABLE #Items
CREATE TABLE #Items(
	CPID nvarchar(50)
	,CPVersion int
	,ProjectKey int
	,QualificationKey int
	--,PRIMARY KEY CLUSTERED (CPID, CPVersion)
)

;WITH folders AS (
	SELECT ItemKey, ExternalId
	FROM DimQuestions  DQ
	WHERE DQ.ExternalId = @folder
	
	UNION ALL
	
	SELECT DQ.ItemKey, DQ.ExternalId
	FROM DimQuestions  DQ
		JOIN folders F ON F.ExternalId = DQ.ItemParentKey
	WHERE 
		DQ.CAQuestionTypeKey=1
		AND DQ.Source=2
),Items AS (
	SELECT 
		DQ.CPID
		,DQ.CPVersion
		,DQ.ProjectKey
		,DP.QualificationKey
	FROM 
		DimQuestions  DQ
		JOIN DimProjects DP ON DP.ProjectKey = DQ.ProjectKey
	WHERE
		(DP.QualificationKey = @subject OR @subject IS NULL)
		AND (DQ.CAQuestionTypeKey<>1)
		AND Deleted<>1
		AND (DQ.Status = @status OR @status IS NULL)
		AND DQ.CPVersion = DQ.LatestVersion
		AND DQ.LastModifiedDate BETWEEN @datefrom AND @dateto
		AND Source=2
		--AND (DQ.ProjectKey = @project 
		AND (
			EXISTS (SELECT * FROM folders F WHERE F.ExternalId = DQ.ItemParentKey) --subfolders
			OR @folder IS NULL -- all items
			OR (DQ.ItemParentKey = 0 AND @folder = 0) --not in folder
		)
)
INSERT INTO #Items
SELECT * FROM Items

;WITH ExamSessions AS(
	SELECT 
		ROW_NUMBER() OVER(PARTITION BY FQR.CPID ORDER BY FES.CompletionDateKey DESC, FES.CompletionTime DESC) N
		,I.CPID
		,FES.CompletionDateKey
		,FES.CompletionTime 
	FROM #Items I
		JOIN FactQuestionResponses FQR ON I.CPID = FQR.CPID --AND I.CPVersion = FQR.CPVersion --getting last used time regardless of item`s version
		JOIN FactExamSessions FES ON FQR.ExamSessionKey = FES.ExamSessionKey
), LastUsed AS( 
	SELECT 
		CPID
		,DT.FullDateAlternateKey + ES.CompletionTime LastUsedDateTime
	FROM ExamSessions ES
	JOIN DimTime DT ON DT.TimeKey = ES.CompletionDateKey
	WHERE N=1
), Comments AS (
	SELECT I.CPID, DCU.FirstName, DCU.LastName, ActionDate, Comment, EventType
	FROM 
		DimQuestionHistory DQH
		JOIN #Items I ON I.CPID = DQH.CPID
		JOIN DimContentUsers DCU ON DQH.UserKey = DCU.UserKey
	WHERE Comment IS NOT NULL
), SourceMaterials AS (
	SELECT 
		I.CPID
		,DMI.MediaType
		,ISNULL(SMcr.name, DMI.Name) Name
		,DMI.Size
	FROM 
		#Items I 
		JOIN DimItemsSourceMaterialsCrossRef SMcr 
			ON I.CPID = SMcr.CPID 
			AND I.CPVersion = SMcr.CPVersion --Show source materials only for latest item version
		JOIN DimMediaItems DMI
			ON DMI.MediaItemKey = SMcr.MediaItemKey
), Metadata AS (
	SELECT 
		DQM.CPID
		,attrib
		,attribType
		,attribval
		,DQM.externalId
		,deleted
	FROM 
		#Items I
		JOIN DimQuestionMetaData DQM 
			ON I.CPID = DQM.CPID 
			AND I.CPVersion = DQM.CPVersion --Show tags only for latest item version
	WHERE 
		attribType IS NOT NULL
), Answers AS (
	SELECT 
		I.CPID
		,ContentType
		,ShortText
		,[text]
		,IsCorrect 
		,OrderNumber
		,PlaceholderId
		,PlaceholderOrderNumber
		,WeightedMark
	FROM
		#Items I 
		JOIN DimOptions DO
			ON DO.CPID = I.CPID AND DO.CPVersion = I.CPVersion
)
, Combinations AS (
	SELECT 
		I.CPID
		,DC.AnswerText [text]
		,DC.Mark
	FROM
		#Items I 
		JOIN DimCombinations DC
			ON DC.CPID = I.CPID AND DC.CPVersion = I.CPVersion
)

------------------------Header------------------------
SELECT  
	1 AS [Tag]
	,NULL AS [Parent]
	,NULL AS [report!1]
	,NULL AS [item!2!Id]
	,NULL AS [item!2!CAID]
	,NULL AS [item!2!Version]
	,NULL AS [item!2!QualificationName]
	,NULL AS [item!2!QuestionName]
	,NULL AS [item!2!QuestionStem]
	,NULL AS [item!2!QuestionTypeId]
	,NULL AS [item!2!Status]
	,NULL AS [item!2!CreatorFirstName]
	,NULL AS [item!2!CreatorLastName]
	,NULL AS [item!2!CreationDate]
	,NULL AS [item!2!ModifierFirstName]
	,NULL AS [item!2!ModifierLastName]
	,NULL AS [item!2!LastModifiedDate]
	,NULL AS [item!2!LastUsedDateTime]
	,NULL AS [item!2!MediaName]
	,NULL AS [item!2!MediaType]
	,NULL AS [item!2!MediaSize]
	,NULL AS [item!2!QuestionPath]
	,NULL AS [item!2!TotalMark]
	,NULL AS [item!2!MarkingType]
	,NULL AS [item!2!UsageCount]
	,NULL AS [item!2!FacilityValue]
	,NULL AS [item!2!SeedUsageCount]
	,NULL AS [item!2!SeedPValue]
	,NULL AS [item!2!MarkType]
	,NULL AS [sourceMaterials!3]
	,NULL AS [sourceMaterial!4!MediaType]
	,NULL AS [sourceMaterial!4!Name]
	,NULL AS [sourceMaterial!4!Size]
	,NULL AS [answers!5]
	,NULL AS [answer!6!contentType]
	,NULL AS [answer!6!character]
	,NULL AS [answer!6!text]
	,NULL AS [answer!6!isCorrect]
	,NULL AS [answer!6!orderNumber]
	,NULL AS [answer!6!placeholderId]
	,NULL AS [answer!6!placeholderOrderNumber]
	,NULL AS [answer!6!mark]
	,NULL AS [metadata!7]
	,NULL AS [tag!8!name]	
	,NULL AS [tag!8!type]
	,NULL AS [tag!8!id]
	,NULL AS [tag!8!deleted]
	,NULL AS [value!9]
	,NULL AS [comments!10]
	,NULL AS [comment!11!date]
	,NULL AS [comment!11!EventType]
	,NULL AS [comment!11!FirstName]
	,NULL AS [comment!11!LastName]	
	,NULL AS [comment!11!text]	
	,NULL AS [combinations!12]
	,NULL AS [combination!13!text]
	,NULL AS [combination!13!mark]
	
UNION ALL
------------------------Items------------------------
SELECT 
	2
	,1
	,NULL
	,DQ.CPID
	,DQ.ExternalId
	,DQ.CPVersion
	,DQua.QualificationName
	,DQ.QuestionName
	,DQ.QuestionStem
	,DQ.CAQuestionTypeKey
	,DQ.[Status]
	,CU.FirstName
	,CU.LastName
	,DQ.CreationDate
	,MU.FirstName
	,MU.LastName
	,DQ.LastModifiedDate
	,LU.LastUsedDateTime
	,DMI.Name
	,DMI.MediaType
	,DMI.Size	
	,DQ.QuestionPath
	,TotalMark
	,MarkingTypeKey
	,UsageCount
	,FacilityValue
	,SeedUsageCount
	,SeedPValue
	,MarkType
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
FROM 
	#Items I
	JOIN DimQuestions DQ ON DQ.CPID = I.CPID AND DQ.CPVersion = I.CPVersion
	JOIN DimProjects DP ON DP.ProjectKey = DQ.ProjectKey
	JOIN DimQualifications DQua ON DQua.QualificationKey = DP.QualificationKey
	JOIN DimContentUsers CU ON CU.UserKey = DQ.CreatedByUserKey
	LEFT JOIN DimContentUsers MU ON MU.UserKey = DQ.ModifiedByUserKey
	LEFT JOIN DimMediaItems DMI ON DMI.MediaItemKey = DQ.MediaItemKey
	LEFT JOIN LastUsed LU ON I.CPID = LU.CPID
	
UNION ALL
-----------------Source materials root----------------
SELECT  
	3 AS [Tag]
	,2 AS [Parent]
	,NULL AS [report!1]
	,CPID AS [item!2!Id]
	,NULL AS [item!2!CAID]
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,''
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
FROM #Items

UNION ALL
-------------------Source materials-------------------
SELECT  
	4
	,3
	,NULL
	,CPID AS [item!2!Id]
	,NULL AS [item!2!CAID]
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,''
	,MediaType AS [sourceMaterial!4!MediaType]
	,Name AS [sourceMaterial!4!Name]
	,Size AS [sourceMaterial!4!Size]
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
FROM 
	SourceMaterials

UNION ALL
---------------------Answers root----------------------
SELECT  
	5 AS [Tag]
	,2 AS [Parent]
	,NULL AS [report!1]
	,CPID AS [item!2!Id]
	,NULL AS [item!2!CAID]
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,''
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
FROM #Items

UNION ALL

------------------------Answers------------------------
SELECT  
	6 AS [Tag]
	,5 AS [Parent]
	,NULL AS [report!1]
	,CPID AS [item!2!Id]
	,NULL AS [item!2!CAID]
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,''
	,ContentType
	,ShortText
	,[text]
	,IsCorrect
	,OrderNumber AS [answer!6!orderNumber]
	,PlaceholderId AS [answer!6!placeholderId]
	,PlaceholderOrderNumber AS [answer!6!placeholderOrderNumber]
	,WeightedMark
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
FROM Answers

UNION ALL
---------------------Metadata root---------------------
SELECT  
	7 AS [Tag]
	,2 AS [Parent]
	,NULL AS [report!1]
	,CPID AS [item!2!Id]
	,NULL AS [item!2!CAID]
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,''
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
FROM #Items

UNION ALL
-------------------------Tags--------------------------
SELECT DISTINCT
	8 AS [Tag]
	,7 AS [Parent]
	,NULL AS [report!1]
	,CPID AS [item!2!Id]
	,NULL AS [item!2!CAID]
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,''
	,attrib AS [tag!8!name]
	,attribType AS [tag!8!type]
	,externalId AS [tag!8!id]
	,deleted AS [tag!8!deleted]
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
FROM Metadata

UNION ALL

SELECT 
	9 AS [Tag]
	,8 AS [Parent]
	,NULL AS [report!1]
	,CPID AS [item!2!Id]
	,NULL AS [item!2!CAID]
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,''
	,attrib AS [tag!8!name]
	,attribType AS [tag!8!type]
	,externalId AS [tag!8!id]
	,deleted AS [tag!8!deleted]
	,attribval AS [tag!8!value]
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
FROM Metadata

UNION ALL
---------------------Comments root---------------------
SELECT  
	10 AS [Tag]
	,2 AS [Parent]
	,NULL AS [report!1]
	,CPID AS [item!2!Id]
	,NULL AS [item!2!CAID]
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,''
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
FROM #Items
UNION ALL
-----------------------Comments-----------------------
SELECT  
	11
	,10
	,NULL
	,CPID AS [item!2!Id]
	,NULL AS [item!2!CAID]
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,''
	,ActionDate AS [comment!3!date]
	,EventType AS [comment!11!EventType]
	,FirstName AS [comment!11!FirstName]
	,LastName AS [comment!11!LastName]	
	,Comment AS [comment!3!text]
	,NULL
	,NULL
	,NULL
FROM 
	Comments

UNION ALL
---------------------Combinations root---------------------
SELECT  
	12 AS [Tag]
	,2 AS [Parent]
	,NULL AS [report!1]
	,CPID AS [item!2!Id]
	,NULL AS [item!2!CAID]
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,''
	,NULL
	,NULL
FROM #Items

UNION ALL
---------------------Combinations---------------------
SELECT  
	13 AS [Tag]
	,12 AS [Parent]
	,NULL AS [report!1]
	,CPID AS [item!2!Id]
	,NULL AS [item!2!CAID]
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,''
	,[text]
	,Mark
FROM Combinations

ORDER BY [item!2!Id]
	, [combinations!12]
	, [comments!10]
	, [tag!8!name]
	, [metadata!7]
	, [answers!5]
	, [sourceMaterials!3]
	, Tag
FOR XML Explicit
OPTION (RECOMPILE) --Added to avoid parameter sniffing which produces ineffective plans

IF OBJECT_ID('tempdb..#Items') IS NOT NULL DROP TABLE #Items

