USE [Demo_SDWH_Perf2]
GO

/****** Object:  Index [06_idx_cpidver]    Script Date: 03/09/2016 16:05:20 ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[DimComponents]') AND name = N'06_idx_cpidver')
DROP INDEX [06_idx_cpidver] ON [dbo].[DimComponents] WITH ( ONLINE = OFF )
GO

USE [Demo_SDWH_Perf2]
GO

/****** Object:  Index [06_idx_cpidver]    Script Date: 03/09/2016 16:05:20 ******/
CREATE NONCLUSTERED INDEX [06_idx_cpidver] ON [dbo].[DimComponents] 
(
	[CPID] ASC,
	[CPVersion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO


