USE [Demo_SDWH_Perf2]
GO

/****** Object:  Index [idxExamVersion]    Script Date: 02/19/2016 08:49:24 ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[FactExamSessions]') AND name = N'01_idx_ev')
DROP INDEX [01_idx_ev] ON [dbo].[FactExamSessions] WITH ( ONLINE = OFF )
GO

USE [Demo_SDWH_Perf2]
GO

/****** Object:  Index [idxExamVersion]    Script Date: 02/19/2016 08:49:24 ******/
CREATE NONCLUSTERED INDEX [01_idx_ev] ON [dbo].[FactExamSessions] 
([ExamVersionKey])
INCLUDE ([CompletionDateKey],[QualificationKey]) 
GO


