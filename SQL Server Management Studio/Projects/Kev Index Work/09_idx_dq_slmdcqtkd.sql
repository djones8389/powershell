USE [Demo_SDWH_Perf2]
GO

/****** Object:  Index [09_idx_dq_slmdcqtkd]    Script Date: 03/08/2016 10:57:58 ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[DimQuestions]') AND name = N'09_idx_dq_slmdcqtkd')
DROP INDEX [09_idx_dq_slmdcqtkd] ON [dbo].[DimQuestions] WITH ( ONLINE = OFF )
GO

USE [Demo_SDWH_Perf2]
GO

/****** Object:  Index [09_idx_dq_slmdcqtkd]    Script Date: 03/08/2016 10:57:58 ******/
CREATE NONCLUSTERED INDEX [09_idx_dq_slmdcqtkd] ON [dbo].[DimQuestions] 
(
	[Source] ASC,
	[LastModifiedDate] ASC,
	[CAQuestionTypeKey] ASC,
	[Deleted] ASC
)
INCLUDE ( [CPID],
[CPVersion],
[ProjectKey],
[ItemParentKey],
[Status],
[LatestVersion]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO


