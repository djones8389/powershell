DBCC FREEPROCCACHE;
DBCC DROPCLEANBUFFERS;

DECLARE @centres VARCHAR(MAX) = '30,32,33,34,36,43,45,48,49,56'
DECLARE @subjects VARCHAR(MAX) = '103,104,110,112,116,121,129'
DECLARE @datefrom DATE = '2015-01-01';
DECLARE @dateto DATE = '2015-12-31';

DECLARE @FacilityValues TABLE(Inx int, ExamVersionKey int, CPID nvarchar(50), FacilityValue float)

DECLARE @ES TABLE(ExamSessionKey int
		,N int
		,Inx int
		,CentreKey int
		,QualificationKey int
		,ExamKey int
		,ExamVersionKey int
		,UserMarks float
		,CompletionDateTime datetime
		,Pass bit)
INSERT INTO @ES
SELECT 
	ES.ExamSessionKey
	,ROW_NUMBER() OVER (PARTITION BY ExamVersionKey ORDER BY ES.UserMarks ASC, ES.ExamSessionKey ASC) N
	,NTILE(3) OVER(PARTITION BY ES.ExamVersionKey ORDER BY ES.UserMarks DESC, ES.ExamSessionKey ASC) Inx
	,ES.CentreKey
	,ES.QualificationKey
	,ES.ExamKey
	,ES.ExamVersionKey
	,ES.UserMarks
	,T.FullDateAlternateKey + ES.CompletionTime  CompletionDateTime
	,ES.Pass
FROM	 
	dbo.FactExamSessions AS ES
	INNER JOIN dbo.DimTime AS T
		 ON ES.CompletionDateKey = T.TimeKey
WHERE	 
	(T.FullDateAlternateKey + ES.CompletionTime) BETWEEN @datefrom AND @dateto
	AND	 ES.FinalExamState <> 10
	AND	 ES.CentreKey IN (SELECT Value FROM dbo.fn_ParamsToList(@centres,0))
	AND	 ES.QualificationKey IN (SELECT Value FROM dbo.fn_ParamsToList(@subjects,0))
OPTION (MAXRECURSION 0)

DECLARE @Items TABLE(Inx int, ExamSessionKey int, ExamVersionKey int, CPID nvarchar(50), CPVersion int, UserMarks float, QuestionMarks float, QuestionTotalMarks float, Attempted float, CAID int, Scored bit)

;WITH items AS (
	SELECT	
		ES.Inx
		,ES.ExamSessionKey
		,ES.ExamVersionKey
		,QR.CPID
		,QR.CPVersion
		,CASE WHEN Q.CAQuestionTypeKey IS NOT NULL THEN Q.ExternalId ELSE NULL END CAID
		,ES.UserMarks
		,(SELECT TOP 1 AssignedMark FROM dbo.FactMarkerResponse MR WHERE MR.ExamSessionKey = QR.ExamSessionKey AND MR.CPID = QR.CPID ORDER BY SEQNO DESC) AssM
		,Q.TotalMark * QR.Mark AssM2
		,Q.TotalMark QuestionTotalMarks
		,QR.Attempted
		,QR.Scored
	FROM	
	@ES ES
	INNER JOIN	FactQuestionResponses QR
		 ON ES.ExamSessionKey = QR.ExamSessionKey
	INNER JOIN dbo.DimQuestions AS Q
		 ON QR.CPID = Q.CPID
			AND QR.CPVersion = Q.CPVersion
			AND Q.TotalMark>0
)
INSERT INTO @Items 
SELECT 
	Inx
	,ExamSessionKey
	,ExamVersionKey
	,CPID
	,CPVersion
	,UserMarks
	,ISNULL(AssM, AssM2) QuestionMarks
	,QuestionTotalMarks
	,Attempted
	,CAID
	,Scored
FROM items
          
;WITH QuestionCount AS (
	SELECT 
		ExamVersionKey
		, COUNT(DISTINCT CPID) QuestionCount 
		, SUM(Attempted)/COUNT(CPID) att
		FROM @Items
	GROUP BY ExamVersionKey
), FVs AS (
	SELECT 
		Inx
		,ExamVersionKey
		,CPID
		,AVG(QuestionMarks/QuestionTotalMarks) FV
	FROM @Items
	WHERE	 
		Inx IN (1, 3)
	GROUP BY 
		Inx
		,ExamVersionKey
		,CPID
), 
FV_Overall AS (
	SELECT 
		ExamVersionKey
		,CPID
		,CAID
		,AVG(QuestionMarks/QuestionTotalMarks) FV
	FROM @Items
	GROUP BY 
		ExamVersionKey
		,CPID
		,CAID
), 
ES_indices AS (
	SELECT ExamVersionKey
		,CEILING(MAX(N)/2.+0.5) ceil, FLOOR(MAX(N)/2.+0.5) flr
	FROM @ES ES
	GROUP BY ExamVersionKey
), 
medians AS (
	SELECT ES.ExamVersionKey, AVG(UserMarks) median
	FROM @ES ES
	JOIN ES_indices ESI ON ES.ExamVersionKey = ESI.ExamVersionKey AND (ES.N = ESI.ceil OR ES.N = ESI.flr)
	GROUP BY ES.ExamVersionKey
), 
modes_raw AS (
	SELECT 
		ROW_NUMBER() OVER(PARTITION BY ExamVersionKey ORDER BY COUNT(ExamSessionKey) DESC, UserMarks) N
		,ROW_NUMBER() OVER(PARTITION BY ExamVersionKey ORDER BY COUNT(ExamSessionKey) DESC, UserMarks DESC) N2
		,ExamVersionKey
		,UserMarks mode
		,COUNT(ExamSessionKey) cnt 
	FROM @ES ES
	GROUP BY ExamVersionKey, UserMarks
), 
stderr AS (
	SELECT 
		ExamVersionKey
		,STDEV(UserMarks) SD
		,STDEVP(UserMarks) SDb
		,COUNT(ExamSessionKey) cnt
	FROM @ES ES
	GROUP BY ExamVersionKey
), 
Vars AS (
	SELECT 
		ExamVersionKey
		,CPID
		,CASE WHEN QuestionMarks > 0 THEN 1 ELSE 0 END Mp
		,UserMarks
	FROM @Items
)
, 
Numbers AS (
	SELECT 
		V.ExamVersionKey
		,V.CPID
		,CAST(SUM(V.Mp*V.UserMarks) AS numeric) M1_raw
		,CAST(SUM((1-V.Mp)*V.UserMarks) AS numeric) M0_raw
		,CAST(SUM(V.Mp) AS numeric) n1
		,CAST(SUM(1-V.Mp) AS numeric) n0
		,CAST(COUNT(V.Mp) AS numeric) n
		,SDb
	FROM Vars V
		JOIN stderr SD 
		ON SD.ExamVersionKey = V.ExamVersionKey
	GROUP BY 
		V.ExamVersionKey
		,V.CPID
		,SD.SDb
),
PassRates AS (
SELECT 
	examVersionKey,  
	CAST(ROUND((SUM(CAST(pass AS INT)* 1.)/COUNT(examVersionKey)*1.) * 100,0) AS INT) AS PassRate
FROM @ES
GROUP BY examVersionKey),
Components AS (	
	SELECT 
		 I.ExamVersionKey
		,I.CPID
		,DC.CID
		,I.CPVersion
		,I.Scored
		,I.QuestionMarks*DC.Weight ComponentUserMark
	FROM @Items I
		JOIN DimComponents DC ON DC.CPID = I.CPID AND DC.CPVersion = I.CPVersion AND DC.Weight>0
		LEFT JOIN FactComponentResponses FCR ON DC.CID = FCR.CID AND I.CPVersion = FCR.CPVersion AND I.ExamSessionKey = FCR.ExamSessionKey
),
ComponentVariance AS (
	SELECT 
		 ExamVersionKey
		,CPID
		,CID
		,VARP(ComponentUserMark) Variance
	FROM Components
	WHERE Scored=1
	GROUP BY ExamVersionKey, CID, CPID
),
Cronbach AS (
	SELECT 
		C.ExamVersionKey	
		,CASE 
			WHEN 
				COUNT(C.CID) = 1 
				OR SUM(C.Variance)=0
				OR SD.SDb=0
			THEN NULL 
			ELSE COUNT(C.CID)/(COUNT(C.CID)-1.) * (1-SUM(C.Variance)/(SD.SDb*SD.SDb)) 
		 END CronbachA
	FROM ComponentVariance C 
	LEFT JOIN stderr SD ON SD.ExamVersionKey = C.ExamVersionKey
	GROUP BY C.ExamVersionKey,SD.SDb
)
SELECT DISTINCT 
		1 AS [Tag]
		,NULL AS [Parent]
		,NULL AS [Report!1] -- root element
		,NULL AS [Qualification!3!QualificationKey]
		,NULL AS [Qualification!3!QualificationName]
		,NULL AS [Qualification!3!QualificationRef]
		,NULL AS [Exam!4!ExamKey]
		,NULL AS [Exam!4!ExamName]
		,NULL AS [Exam!4!ExamReference]
		,NULL AS [ExamVersion!5!ExamVersionKey]
		,NULL AS [ExamVersion!5!ExamVersionName]
		,NULL AS [ExamVersion!5!ExamVersionReference]
		,NULL AS [ExamVersion!5!ExamCount]
		,NULL AS [ExamVersion!5!QuestionCount]
		,NULL AS [ExamVersion!5!LastOccurrence]
		,NULL AS [ExamVersion!5!AverageTimeTaken]
		,NULL AS [ExamVersion!5!TotalTimeTaken]
		,NULL AS [ExamVersion!5!TotalMark]
		,NULL AS [ExamVersion!5!AttemptedPercentage]
		,NULL AS [ExamVersion!5!Mean]
		,NULL AS [ExamVersion!5!Median]
		,NULL AS [ExamVersion!5!Mode]
		,NULL AS [ExamVersion!5!StandardDeviation]
		,NULL AS [ExamVersion!5!StandardError]
		,NULL AS [ExamVersion!5!PassRate]
		,NULL AS [ExamVersion!5!CronbachA]
		,NULL AS [ExamVersion!5!SEM]
		,NULL AS [Centres!6]
		,NULL AS [Centre!7!CentreKey]
		,NULL AS [Centre!7!CentreName]
		,NULL AS [Centre!7!CentreCode]
		,NULL AS [Items!8]
		,NULL AS [Item!9!CPID]
		,NULL AS [Item!9!CAID]
		,NULL AS [Item!9!FV]
		,NULL AS [Item!9!DI]
		,NULL AS [Item!9!PBCC] --http://en.wikipedia.org/wiki/Point-biserial_correlation_coefficient
UNION ALL
SELECT	 DISTINCT
		 3
		,1
		,NULL
		,Q.QualificationKey
		,Q.QualificationName
		,Q.QualificationRef
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL	
		,NULL
		,NULL
		,NULL
		,NULL	
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
 FROM	dbo.DimQualifications AS Q
		JOIN @ES ES ON ES.QualificationKey = Q.QualificationKey
UNION ALL
SELECT DISTINCT
		 4
		,3
		,NULL
		,ES.QualificationKey
		,NULL
		,NULL
		,ES.ExamKey
		,E.ExamName
		,E.ExamReference
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL	
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
FROM
	@ES FES
	JOIN FactExamSessions ES
		ON ES.ExamSessionKey = FES.ExamSessionKey
	INNER JOIN dbo.DimExams E
		 ON ES.ExamKey = E.ExamKey	 
UNION ALL
SELECT
		  5
		,4
		,NULL
		,ES.QualificationKey
		,NULL
		,NULL
		,ES.ExamKey
		,NULL
		,NULL
		,ES.ExamVersionKey
		,EV.ExamVersionName
		,EV.ExamVersionReference
		,SE.cnt
		,ISNULL(QC.QuestionCount, 0) QuestionCount
		,MAX(CompletionDateTime) CompletionDateTime
		,AVG(CAST(ES.TimeTaken AS BIGINT))  TimeTaken
		,AVG(CAST(ROUND(ES.Duration*60*1000,0) AS BIGINT)) Duration
		,AVG(ES.TotalMarksAvailable) TotalMarksAvailable
		,QC.att attemptedPerc
		,AVG(ES.UserMarks) mean
		,ME.median
		,MO.mode
		,SE.SD
		,SE.SD/SQRT(SE.cnt) SE
		,PR.PassRate
		,CB.CronbachA
		,CASE WHEN CB.CronbachA<=1 THEN SE.SDb*SQRT(1-CB.CronbachA) ELSE NULL END SEM
		,NULL
		,NULL
		,NULL
		,NULL	
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
 FROM
	@ES FES
	INNER JOIN FactExamSessions ES
		ON ES.ExamSessionKey = FES.ExamSessionKey
	INNER JOIN dbo.DimExamVersions EV
		ON ES.ExamVersionKey = EV.ExamVersionKey
	LEFT JOIN QuestionCount QC 
		ON QC.ExamVersionKey = ES.ExamVersionKey
	LEFT JOIN medians ME 
		ON ME.ExamVersionKey = ES.ExamVersionKey
	LEFT JOIN modes_raw MO 
		ON MO.ExamVersionKey = ME.ExamVersionKey
		AND MO.N=1 AND MO.N2=1
	LEFT JOIN stderr SE 
		ON SE.ExamVersionKey = ES.ExamVersionKey
	LEFT JOIN PassRates PR 
		ON SE.ExamVersionKey = PR.ExamVersionKey
	LEFT JOIN Cronbach CB
		ON SE.ExamVersionKey = CB.ExamVersionKey				
GROUP BY ES.QualificationKey
		,ES.ExamKey
		,ES.ExamVersionKey
		,EV.ExamVersionName
		,EV.ExamVersionReference
		,MO.mode
		,ME.median
		,SE.SD
		,SE.cnt
		,QC.QuestionCount
		,QC.att
		,PR.PassRate
		,CB.CronbachA
		,SE.SDb
UNION ALL
SELECT	DISTINCT 
		 6
		,5
		,NULL
		,FES.QualificationKey
		,NULL
		,NULL
		,FES.ExamKey
		,NULL
		,NULL
		,FES.ExamVersionKey
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,''''
		,NULL
		,NULL
		,NULL	
		,NULL
		,NULL		
		,NULL
		,NULL
		,NULL
		,NULL
 FROM
	@ES FES
UNION ALL
SELECT	DISTINCT 
		 7
		,6
		,NULL
		,FES.QualificationKey
		,NULL
		,NULL
		,FES.ExamKey
		,NULL
		,NULL
		,FES.ExamVersionKey
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,DC.CentreKey AS [Centre!7!CentreKey]
		,DC.CentreName AS [Centre!7!CentreName]
		,DC.CentreCode AS [Centre!7!CentreCode]	
		,NULL
		,NULL		
		,NULL
		,NULL
		,NULL
		,NULL
 FROM
	@ES FES
	JOIN DimCentres DC ON FES.CentreKey = DC.CentreKey
UNION ALL
SELECT	DISTINCT 
		 8
		,5
		,NULL
		,FES.QualificationKey
		,NULL
		,NULL
		,FES.ExamKey
		,NULL
		,NULL
		,FES.ExamVersionKey
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL	
		,''''
		,NULL		
		,NULL
		,NULL
		,NULL
		,NULL
 FROM
	@ES FES
UNION ALL
SELECT	 9
		,8
		,NULL
		,ES.QualificationKey
		,NULL
		,NULL
		,ES.ExamKey
		,NULL
		,NULL
		,ES.ExamVersionKey
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL	
		,NULL
		,NULL
		,FV0.CPID
		,FV0.CAID
		,FV0.FV
		,(FV1.FV - FV3.FV) DI
		,CASE WHEN n1=0 OR n0=0 OR SDb = 0 THEN NULL ELSE (M1_raw/n1-M0_raw/n0)/SDb*SQRT(n0*n1/N.n/N.n) END
 FROM
	@ES ES
	INNER JOIN FV_Overall FV0
		ON ES.ExamVersionKey = FV0.ExamVersionKey
	INNER JOIN FVs FV1
		ON FV1.CPID = FV0.CPID
		AND FV1.ExamVersionKey = FV0.ExamVersionKey
		AND FV1.Inx = 1
	INNER JOIN FVs FV3
		ON FV3.CPID = FV0.CPID
		AND FV3.ExamVersionKey = FV0.ExamVersionKey
		AND FV3.Inx = 3
	JOIN Numbers N 
		ON N.ExamVersionKey = ES.ExamVersionKey
		AND N.CPID = FV0.CPID
GROUP BY ES.QualificationKey
		,ES.ExamKey
		,ES.ExamVersionKey
		,FV0.CPID
		,FV0.FV
		,(FV1.FV - FV3.FV) 
		,N.n
		,N.n0
		,N.n1
		,N.SDb
		,N.M0_raw
		,N.M1_raw
		,FV0.CAID
ORDER BY 
		[Qualification!3!QualificationKey]
		,[Exam!4!ExamKey]
		,[ExamVersion!5!ExamVersionKey]
		,[Item!9!CPID]
		,[Items!8]
		,[Centre!7!CentreKey]
		,[Centres!6]
FOR XML EXPLICIT
OPTION (RECOMPILE)

