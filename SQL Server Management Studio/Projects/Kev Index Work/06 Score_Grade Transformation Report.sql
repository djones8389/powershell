DBCC FREEPROCCACHE;
DBCC DROPCLEANBUFFERS;

DECLARE @centres VARCHAR(MAX) = '30,32,33,34,36,43,45,48,49,56,57,58,59,60,61,62,63,65,68,72,73,77,78,80,82,83,84,86,87,88,93,94,95,96,97,99,101,103,105,107,108,111,112,114,117,120,121,126,129,130,131,134,135,137,141,143,144,146,148,151,153,154,156,158,160,161,163,166,167,170,173,174,175,176,177,180,181,183,187,188,189,194,195,198,199,201,203,205,208,209,212,213,215,216,219,222,223,224,226,227,230,231,235,237,238,239,241,242,244,246,250,251,254,255,259,264,267,272,274,275,277,279,281,283,286,288,289,290,292,293,294,296,297,299,301,302,303,305,309,310,311,312,317,318,322,323,324,328,329,330,331,332,334,337,339,343,345,348,349,352,353,358,359,361,363,364,367,368,370,374,375,376,377,378,379,380,382,383,386,387,389,392,393,394,396,398,399,400,401,402,403,405,407,409,410,412,413,415,417,419,421'
DECLARE @subject INT = 103;
DECLARE @dateStartRange DATE = '2000-01-01';
DECLARE @dateEndRange DATE = '2015-12-31';
DECLARE @testform INT = 1;
DECLARE @test INT = 1;

;WITH ExamSessions AS (
	SELECT FES.ExamSessionKey
	FROM FactExamSessions FES
		JOIN DimTime DT ON DT.TimeKey = FES.CompletionDateKey
	WHERE FES.CentreKey IN  (
		SELECT Value FROM dbo.fn_ParamsToList(@centres,0)
	)
	AND QualificationKey = @subject
	AND ExamKey = @test
	AND ExamVersionKey = @testForm
	AND (DT.FullDateAlternateKey+FES.CompletionTime) BETWEEN @dateStartRange and @dateEndRange
),Items AS (
	SELECT 
		FQR.*
		,CASE WHEN Scored=1 THEN 1 ELSE 0 END Items
		,CASE WHEN Scored=1 AND DC.QuestionType = 10 THEN 1 ELSE 0 END MCQItems
		,DC.ItemXML.value('count(C/I[not(@cor) or @cor=0])','int') Distractors
		,CAST(Scored as int)*DC.ItemXML.value('count(C/I)','int') Options
	FROM 
		ExamSessions FES 
		JOIN FactQuestionResponses FQR ON FES.ExamSessionKey = FQR.ExamSessionKey
		JOIN DimComponents DC ON 
			DC.CPID = FQR.CPID 
			AND DC.CPVersion = FQR.CPVersion  
			AND DC.HasCPAuditEntry=1
			AND DC.TotalMark>0
)
,GroupedItems AS (
	SELECT 
		ExamSessionKey
		,SUM(Items) totalItems
		,SUM(MCQItems) totalMCQItems
		--,SUM(Distractors*MCQItems) totalDistractors --If question is non-MCQ, MCQItems=0 so totalDistractors will be 0
		,SUM(Options*MCQItems) totalOptions --If question is non-MCQ, MCQItems=0 so totalOptions will be 0
	FROM 
		Items
	GROUP BY ExamSessionKey	
)
	SELECT 
		ES.ExamSessionKey '@id'
		,CASE FES.FinalExamState WHEN 10 THEN 1 ELSE 0 END '@isVoided'
		,CandidateRef '@candidateRef'
		,Surname '@lastName'
		,Forename '@firstName'
		,UserMarks '@markAchieved'
		,FullDateAlternateKey + CompletionTime '@testDate'
		,CentreName '@centre'	
		,KeyCode '@keyCode'
		,TotalMarksAvailable '@totalMarks'
		,PassMark '@passMark'
		,TimeTaken '@totalTimeTaken'
		,CASE WHEN ISNULL(TotalMarksAvailable,0)=0 THEN NULL ELSE PassMark/TotalMarksAvailable END '@passPercentage'
		,totalItems '@totalQuestionItems'
		--,CASE WHEN ISNULL(totalMCQItems,0)=0 THEN NULL ELSE totalDistractors/(totalMCQItems+0.) END '@avgDistractors'
		,CASE WHEN ISNULL(totalMCQItems,0)=0 THEN NULL ELSE totalOptions/(totalMCQItems+0.) END '@avgOptions'
		,CASE 
			WHEN totalItems-totalMCQItems IS NULL THEN NULL
			WHEN totalItems-totalMCQItems=0 THEN 0 
			ELSE 1 
		END '@containsNonMCQ'
		 
		
	FROM 
		ExamSessions ES
		JOIN FactExamSessions FES ON FES.ExamSessionKey = ES.ExamSessionKey
		JOIN DimTime DT ON DT.TimeKey = FES.CompletionDateKey
		JOIN DimCandidate DC ON DC.CandidateKey = FES.CandidateKey
		JOIN DimCentres DCe ON DCe.CentreKey = FES.CentreKey
		LEFT JOIN GroupedItems D ON ES.ExamSessionKey = D.ExamSessionKey
	FOR XML PATH('examSession'), ROOT('report')
	OPTION (MAXRECURSION 0)


