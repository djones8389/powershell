DBCC FREEPROCCACHE;
DBCC DROPCLEANBUFFERS;

DECLARE @centres VARCHAR(MAX) = '30,32,33,34,36,43,45,48,49,56,57,58,59,60,61,62,63,65,68,72,73,77,78,80,82,83,84,86,87,88,93,94,95,96,97,99,101,103,105,107,108,111,112,114,117,120,121,126,129,130,131,134,135,137,141,143,144,146,148,151,153,154,156,158,160,161,163,166,167,170,173,174,175,176,177,180,181,183,187,188,189,194,195,198,199,201,203,205,208,209,212,213,215,216,219,222,223,224,226,227,230,231,235,237,238,239,241,242,244,246,250,251,254,255,259,264,267,272,274,275,277,279,281,283,286,288,289,290,292,293,294,296,297,299,301,302,303,305,309,310,311,312,317,318,322,323,324,328,329,330,331,332,334,337,339,343,345,348,349,352,353,358,359,361,363,364,367,368,370,374,375,376,377,378,379,380,382,383,386,387,389,392,393,394,396,398,399,400,401,402,403,405,407,409,410,412,413,415,417,419,421'
DECLARE @subjects VARCHAR(MAX) = '103,104,110,112,116,121,129,130,133,135,136,137,140,142,144,145,146,148,150,156,161,162,168,173,187,191,195,198,204,205,209,210,214,220,223'
DECLARE @dateStartRange DATE = '2000-01-01';
DECLARE @dateEndRange DATE = '2015-12-31';
DECLARE @examVersion INT = 1;
DECLARE @exam INT = 1;

DECLARE @StartDateKey int = (SELECT TimeKey FROM DimTime WHERE CAST(FullDateAlternateKey AS DATE)=CAST(@dateStartRange AS DATE))
DECLARE @EndDateKey int =	(SELECT TimeKey FROM DimTime WHERE CAST(FullDateAlternateKey AS DATE)=CAST(@dateEndRange AS DATE))
IF @StartDateKey IS NULL BEGIN
	IF @dateStartRange<=(SELECT MIN(FullDateAlternateKey) FROM DimTime) SELECT @StartDateKey = MIN(TimeKey) FROM DimTime
	ELSE SELECT @StartDateKey = MAX(TimeKey)+1 FROM DimTime
END
IF @EndDateKey IS NULL BEGIN
	IF @dateEndRange<=(SELECT MIN(FullDateAlternateKey) FROM DimTime) SELECT @EndDateKey = MIN(TimeKey)-1 FROM DimTime
	ELSE SELECT @EndDateKey = MAX(TimeKey) FROM DimTime
END

DECLARE @ES TABLE(
	ExamSessionKey int PRIMARY KEY CLUSTERED
	,KeyCode varchar(12)
	,CandidateKey int
	,CentreKey int
	,QualificationKey int
	,ExamKey int
	,ExamVersionKey int
	,UserMarks decimal(9,4)
	,TotalMarksAvailable decimal(9,4)
	,AdjustedGradeKey int
	,CompletionDateTime datetime
	,FinalExamState int
)
		
INSERT INTO @ES
SELECT 
	FES.ExamSessionKey
	,FES.KeyCode
	,FES.CandidateKey
	,FES.CentreKey
	,FES.QualificationKey
	,@exam ExamKey
	,FES.ExamVersionKey
	,FES.UserMarks
	,FES.TotalMarksAvailable
	,FES.AdjustedGradeKey
	,DT.FullDateAlternateKey + FES.CompletionTime
	,FES.FinalExamState
FROM [dbo].[FactExamSessions] FES
	JOIN DimTime DT ON FES.CompletionDateKey = DT.TimeKey
WHERE 
	FES.CompletionDateKey BETWEEN @StartDateKey AND @EndDateKey
	AND FES.ExamKey = @exam 
	AND (FES.ExamVersionKey = @examVersion OR @examVersion = 0)
	AND FES.QualificationKey IN (
		SELECT Value
		FROM [dbo].[fn_ParamsToList](@subjects,0)
		)
	AND FES.CentreKey IN (
		SELECT Value
		FROM [dbo].[fn_ParamsToList](@centres,0)
		)
OPTION(MAXRECURSION 0)
			
DECLARE @Items TABLE(
	CPID nvarchar(50)
	,CPVersion int
	,PRIMARY KEY CLUSTERED(CPID,CPVersion)
)		
	
INSERT INTO @Items
SELECT DISTINCT 
	CPID
	,CPVersion
FROM @ES ES
JOIN FactQuestionResponses FQR ON FQR.ExamSessionKey = ES.ExamSessionKey



;WITH Metadata AS (
	SELECT 
		DQM.CPID
		,DQM.CPVersion
		,attrib
		,attribType
		,attribval
		,externalId
		,deleted
	FROM 
		@Items I
		JOIN DimQuestionMetaData DQM 
			ON I.CPID = DQM.CPID 
			AND I.CPVersion = DQM.CPVersion
	WHERE 
		attribType IS NOT NULL
), ComboKeys AS (
	SELECT 
		I.CPID
		,I.CPVersion
		,dbo.ag_CLR_Concat(ISNULL(DO.ShortText, DC.AnswerText)+','+CAST(ISNULL(DO.WeightedMark, DC.Mark) AS nvarchar(100)),'|')  [Key]
	FROM @Items I
		JOIN DimQuestions DQ
			ON I.CPID = DQ.CPID
			AND I.CPVersion = DQ.CPVersion
		LEFT JOIN DimOptions DO 
			ON DO.CPID = I.CPID 
			AND DO.CPVersion = I.CPVersion
			AND DQ.MarkType=1
			--AND DO.WeightedMark IS NOT NULL
		LEFT JOIN DimCombinations DC
			ON DC.CPID = I.CPID 
			AND DC.CPVersion = I.CPVersion
			AND DQ.MarkType=2
	WHERE MarkType IN (1,2)
	GROUP BY I.CPID, I.CPVersion
)

SELECT 
	1 [Tag]
	,NULL [Parent]
	,NULL [report!1]
	,NULL [items!2]
	,NULL [i!3!CPID]
	,NULL [i!3!CPV]
	,NULL [i!3!CAID]
	,NULL [i!3!nam]
	,NULL [i!3!tMk]
	,NULL [i!3!key]
	,NULL [i!3!quT]
	,NULL [i!3!mkT]
	,NULL [md!4]
	,NULL [t!5!nam]	
	,NULL [t!5!typ]
	,NULL [t!5!id]
	,NULL [t!5!del]
	,NULL [v!6]
	,NULL [sessions!7]
	,NULL [s!8!id]
	,NULL [s!8!dtC]
	,NULL [s!8!kCd]
	,NULL [s!8!fnm]
	,NULL [s!8!snm]
	,NULL [s!8!cRf]
	,NULL [s!8!sex]
	,NULL [s!8!DOB]
	,NULL [s!8!sub]
	,NULL [s!8!cnt]
	,NULL [s!8!tst]
	,NULL [s!8!tsF]
	,NULL [s!8!aMk]
	,NULL [s!8!tMk]
	,NULL [s!8!grd]
	,NULL [s!8!iVd]
	,NULL [r!9]
	,NULL [i!10!CPID]
	,NULL [i!10!CPV]
	,NULL [i!10!aMk]
	,NULL [i!10!rsp]
	,NULL [i!10!vTm]
	,NULL [i!10!prO]
	,NULL [i!10!sc]

UNION ALL

SELECT 
	2 [Tag]
	,1 [Parent]
	,NULL [report!1]
	,NULL [items!2]
	,NULL [item!3!CPID]
	,NULL [item!3!CPVersion]
	,NULL [item!3!CAID]
	,NULL [item!3!name]
	,NULL [item!3!totalMark]
	,NULL [item!3!key]
	,NULL [item!3!questionType]
	,NULL [i!3!markType]
	,NULL [metadata!4]
	,NULL [tag!5!name]	
	,NULL [tag!5!type]
	,NULL [tag!5!id]
	,NULL [tag!5!deleted]
	,NULL [value!6]
	,NULL [sessions!7]
	,NULL [examSession!8!id]
	,NULL [examSession!8!dateCompleted]
	,NULL [examSession!8!keyCode]
	,NULL [examSession!8!forename]
	,NULL [examSession!8!surname]
	,NULL [examSession!8!candidateRef]
	,NULL [examSession!8!gender]
	,NULL [examSession!8!DOB]
	,NULL [examSession!8!subject]
	,NULL [examSession!8!centre]
	,NULL [examSession!8!test]
	,NULL [examSession!8!testForm]
	,NULL [examSession!8!actualMark]
	,NULL [examSession!8!totalMark]
	,NULL [examSession!8!grade]
	,NULL [examSession!8!isVoided]
	,NULL [responses!9]
	,NULL [item!10!CPID]
	,NULL [item!10!CPVersion]
	,NULL [item!10!actualMark]
	,NULL [item!10!response]
	,NULL [item!10!viewingTime]
	,NULL [item!10!presentedOrder]
	,NULL [i!10!sc]
	
UNION ALL

SELECT 
	3 [Tag]
	,2 [Parent]
	,NULL [report!1]
	,NULL [items!2]
	,I.CPID
	,I.CPVersion
	,CASE WHEN DQ.CAQuestionTypeKey IS NOT NULL THEN DQ.ExternalId ELSE NULL END CAID
	,DQ.QuestionName [Name]
	,CAST(DQ.TotalMark AS decimal(9,4)) [Total Mark]
	,CASE DQ.MarkType
		WHEN 0 THEN 
			CONVERT(NVARCHAR(4000), ISNULL(
			CASE DQ.QuestionTypeKey
				WHEN 11 THEN dbo.fn_CLR_StripHTML(DQ.DerivedCorrectAnswer)
				ELSE DQ.ShortDerivedCorrectAnswer
			END, ''))
		ELSE ISNULL(CK.[Key],'')
	END [Key]
	,DQ.CAQuestionTypeKey QuestionType 
	,CASE DQ.MarkType
		WHEN 1 THEN 'Weighted'
		WHEN 2 THEN 'Combination'
		ELSE 'Standard'
	END [i!3!markType]
	,NULL [metadata!4]
	,NULL [tag!5!name]	
	,NULL [tag!5!type]
	,NULL [tag!5!id]
	,NULL [tag!5!deleted]
	,NULL [value!6]
	,NULL [sessions!7]
	,NULL [examSession!8!id]
	,NULL [examSession!8!dateCompleted]
	,NULL [examSession!8!keyCode]
	,NULL [examSession!8!forename]
	,NULL [examSession!8!surname]
	,NULL [examSession!8!candidateRef]
	,NULL [examSession!8!gender]
	,NULL [examSession!8!DOB]
	,NULL [examSession!8!subject]
	,NULL [examSession!8!centre]
	,NULL [examSession!8!test]
	,NULL [examSession!8!testForm]
	,NULL [examSession!8!actualMark]
	,NULL [examSession!8!totalMark]
	,NULL [examSession!8!grade]
	,NULL [examSession!8!isVoided]
	,NULL [responses!9]
	,NULL [item!10!CPID]
	,NULL [item!10!CPVersion]
	,NULL [item!10!actualMark]
	,NULL [item!10!response]
	,NULL [item!10!viewingTime]
	,NULL [item!10!presentedOrder]
	,NULL [i!10!sc]
FROM @Items I
	JOIN DimQuestions DQ ON I.CPVersion = DQ.CPVersion AND I.CPID = DQ.CPID
	LEFT JOIN ComboKeys CK ON I.CPVersion = CK.CPVersion AND I.CPID = CK.CPID

UNION ALL

SELECT 
	4 [Tag]
	,3 [Parent]
	,NULL [report!1]
	,NULL [items!2]
	,CPID [item!3!CPID]
	,CPVersion [item!3!CPV]
	,NULL [item!3!CAID]
	,NULL [item!3!name]
	,NULL [item!3!tMk]
	,NULL [item!3!key]
	,NULL [item!3!quT]
	,NULL [i!3!markType]
	,NULL [metadata!4]
	,NULL [tag!5!name]	
	,NULL [tag!5!type]
	,NULL [tag!5!id]
	,NULL [tag!5!deleted]
	,NULL [value!6]
	,NULL [sessions!7]--7
	,NULL [examSession!8!id]--8
	,NULL [examSession!8!dtC]
	,NULL [examSession!8!kCd]
	,NULL [examSession!8!fnm]
	,NULL [examSession!8!snm]
	,NULL [examSession!8!cRf]
	,NULL [examSession!8!sex]
	,NULL [examSession!8!DOB]
	,NULL [examSession!8!sub]
	,NULL [examSession!8!cnt]
	,NULL [examSession!8!tst]
	,NULL [examSession!8!tsF]
	,NULL [examSession!8!aMk]
	,NULL [examSession!8!tMk]
	,NULL [examSession!8!grd]
	,NULL [examSession!8!iVd]
	,NULL [responses!9]--9
	,NULL [item!10!CPID]--10
	,NULL [item!10!CPV]
	,NULL [item!10!aMk]
	,NULL [item!10!rsp]
	,NULL [item!10!vTm]
	,NULL [item!10!prO]
	,NULL [i!10!sc]
FROM @Items

UNION ALL

SELECT DISTINCT
	5 [Tag]
	,4 [Parent]
	,NULL [report!1]
	,NULL [items!2]
	,CPID [item!3!CPID]
	,CPVersion [item!3!CPV]
	,NULL [item!3!CAID]
	,NULL [item!3!name]
	,NULL [item!3!tMk]
	,NULL [item!3!key]
	,NULL [item!3!quT]
	,NULL [i!3!markType]
	,NULL [metadata!4]
	,attrib [tag!5!name]
	,attribType [tag!5!type]
	,externalId [tag!5!id]
	,deleted [tag!5!deleted]
	,NULL [value!6]
	,NULL [sessions!7]--7
	,NULL [examSession!8!id]--8
	,NULL [examSession!8!dtC]
	,NULL [examSession!8!kCd]
	,NULL [examSession!8!fnm]
	,NULL [examSession!8!snm]
	,NULL [examSession!8!cRf]
	,NULL [examSession!8!sex]
	,NULL [examSession!8!DOB]
	,NULL [examSession!8!sub]
	,NULL [examSession!8!cnt]
	,NULL [examSession!8!tst]
	,NULL [examSession!8!tsF]
	,NULL [examSession!8!aMk]
	,NULL [examSession!8!tMk]
	,NULL [examSession!8!grd]
	,NULL [examSession!8!iVd]
	,NULL [responses!9]--9
	,NULL [item!10!CPID]--10
	,NULL [item!10!CPV]
	,NULL [item!10!aMk]
	,NULL [item!10!rsp]
	,NULL [item!10!vTm]
	,NULL [item!10!prO]
	,NULL [i!10!sc]
FROM Metadata

UNION ALL

SELECT 
	6 [Tag]
	,5 [Parent]
	,NULL [report!1]
	,NULL [items!2]
	,CPID [item!3!CPID]
	,CPVersion [item!3!CPV]
	,NULL [item!3!CAID]
	,NULL [item!3!name]
	,NULL [item!3!tMk]
	,NULL [item!3!key]
	,NULL [item!3!quT]
	,NULL [i!3!markType]
	,NULL [metadata!4]
	,attrib [tag!5!name]
	,NULL [tag!5!type]
	,NULL [tag!5!id]
	,NULL [tag!5!deleted]
	,attribval [value!6]
	,NULL [sessions!7]--7
	,NULL [examSession!8!id]--8
	,NULL [examSession!8!dtC]
	,NULL [examSession!8!kCd]
	,NULL [examSession!8!fnm]
	,NULL [examSession!8!snm]
	,NULL [examSession!8!cRf]
	,NULL [examSession!8!sex]
	,NULL [examSession!8!DOB]
	,NULL [examSession!8!sub]
	,NULL [examSession!8!cnt]
	,NULL [examSession!8!tst]
	,NULL [examSession!8!tsF]
	,NULL [examSession!8!aMk]
	,NULL [examSession!8!tMk]
	,NULL [examSession!8!grd]
	,NULL [examSession!8!iVd]
	,NULL [responses!9]--9
	,NULL [item!10!CPID]--10
	,NULL [item!10!CPV]
	,NULL [item!10!aMk]
	,NULL [item!10!rsp]
	,NULL [item!10!vTm]
	,NULL [item!10!prO]
	,NULL [i!10!sc]
FROM Metadata

UNION ALL

SELECT 
	7 [Tag]
	,1 [Parent]
	,NULL [report!1]
	,NULL [items!2]
	,'z' [item!3!CPID]
	,NULL [item!3!CPVersion]
	,NULL [item!3!CAID]
	,NULL [item!3!name]
	,NULL [item!3!totalMark]
	,NULL [item!3!key]
	,NULL [item!3!questionType]
	,NULL [i!3!markType]
	,NULL [metadata!4]
	,NULL [tag!5!name]	
	,NULL [tag!5!type]
	,NULL [tag!5!id]
	,NULL [tag!5!deleted]
	,NULL [value!6]
	,NULL [sessions!7]
	,NULL [examSession!8!id]
	,NULL [examSession!8!dateCompleted]
	,NULL [examSession!8!keyCode]
	,NULL [examSession!8!forename]
	,NULL [examSession!8!surname]
	,NULL [examSession!8!candidateRef]
	,NULL [examSession!8!gender]
	,NULL [examSession!8!DOB]
	,NULL [examSession!8!subject]
	,NULL [examSession!8!centre]
	,NULL [examSession!8!test]
	,NULL [examSession!8!testForm]
	,NULL [examSession!8!actualMark]
	,NULL [examSession!8!totalMark]
	,NULL [examSession!8!grade]
	,NULL [examSession!8!isVoided]
	,NULL [responses!9]
	,NULL [item!10!CPID]
	,NULL [item!10!CPVersion]
	,NULL [item!10!actualMark]
	,NULL [item!10!response]
	,NULL [item!10!viewingTime]
	,NULL [item!10!presentedOrder]
	,NULL [i!10!sc]

UNION ALL

SELECT 
	8 [Tag]
	,7 [Parent]
	,NULL [report!1]
	,NULL [items!2]
	,NULL [item!3!CPID]
	,NULL [item!3!CPVersion]
	,NULL [item!3!CAID]
	,NULL [item!3!name]
	,NULL [item!3!totalMark]
	,NULL [item!3!key]
	,NULL [item!3!questionType]
	,NULL [i!3!markType]
	,NULL [metadata!4]
	,NULL [tag!5!name]	
	,NULL [tag!5!type]
	,NULL [tag!5!id]
	,NULL [tag!5!deleted]
	,NULL [value!6]
	,NULL [sessions!7]
	,FES.ExamSessionKey 
	,FES.CompletionDateTime 
	,FES.KeyCode 
	,DC.Forename 
	,DC.Surname 
	,DC.CandidateRef 
	,DC.Gender 
	,DC.DOB 
	,DQu.QualificationName 
	,DCe.CentreName 
	,DE.ExamName 
	,DEV.ExamVersionName 
	,CAST(FES.UserMarks AS decimal(9,4))
	,CAST(FES.TotalMarksAvailable AS decimal(9,4))
	,DG.Grade
	,CASE FinalExamState WHEN 10 THEN 1 ELSE 0 END [examSession!2!isVoided]
	,NULL [responses!9]
	,NULL [item!10!CPID]
	,NULL [item!10!CPVersion]
	,NULL [item!10!actualMark]
	,NULL [item!10!response]
	,NULL [item!10!viewingTime]
	,NULL [item!10!presentedOrder]
	,NULL [i!10!sc]
FROM @ES FES
	LEFT JOIN [dbo].[DimCandidate] DC ON DC.CandidateKey = FES.CandidateKey
	LEFT JOIN [dbo].[DimQualifications] DQu ON DQu.QualificationKey = FES.QualificationKey
	LEFT JOIN [dbo].[DimCentres] DCe ON DCe.CentreKey = FES.CentreKey
	LEFT JOIN [dbo].[DimExams] DE ON DE.ExamKey = FES.ExamKey
	LEFT JOIN [dbo].[DimExamVersions] DEV ON DEV.ExamKey = FES.ExamKey
		AND DEV.ExamVersionKey = FES.ExamVersionKey
	LEFT JOIN [dbo].[DimGrades] DG ON DG.GradeKey = FES.AdjustedGradeKey

UNION ALL

SELECT 
	9 [Tag]
	,8 [Parent]
	,NULL [report!1]
	,NULL [items!2]
	,NULL [item!3!CPID]
	,NULL [item!3!CPVersion]
	,NULL [item!3!CAID]
	,NULL [item!3!name]
	,NULL [item!3!totalMark]
	,NULL [item!3!key]
	,NULL [item!3!questionType]
	,NULL [i!3!markType]
	,NULL [metadata!4]
	,NULL [tag!5!name]	
	,NULL [tag!5!type]
	,NULL [tag!5!id]
	,NULL [tag!5!deleted]
	,NULL [value!6]
	,NULL [sessions!7]
	,ExamSessionKey [examSession!8!id]
	,NULL [examSession!8!dateCompleted]
	,NULL [examSession!8!keyCode]
	,NULL [examSession!8!forename]
	,NULL [examSession!8!surname]
	,NULL [examSession!8!candidateRef]
	,NULL [examSession!8!gender]
	,NULL [examSession!8!DOB]
	,NULL [examSession!8!subject]
	,NULL [examSession!8!centre]
	,NULL [examSession!8!test]
	,NULL [examSession!8!testForm]
	,NULL [examSession!8!actualMark]
	,NULL [examSession!8!totalMark]
	,NULL [examSession!8!grade]
	,NULL [examSession!8!isVoided]
	,NULL [responses!9]
	,NULL [item!10!CPID]
	,NULL [item!10!CPVersion]
	,NULL [item!10!actualMark]
	,NULL [item!10!response]
	,NULL [item!10!viewingTime]
	,NULL [item!10!presentedOrder]
	,NULL [i!10!sc]

FROM @ES FES

UNION ALL

SELECT 
	10 [Tag]
	,9 [Parent]
	,NULL [report!1]
	,NULL [items!2]
	,NULL [item!3!CPID]
	,NULL [item!3!CPVersion]
	,NULL [item!3!CAID]
	,NULL [item!3!name]
	,NULL [item!3!totalMark]
	,NULL [item!3!key]
	,NULL [item!3!questionType]
	,NULL [i!3!markType]
	,NULL [metadata!4]
	,NULL [tag!5!name]	
	,NULL [tag!5!type]
	,NULL [tag!5!id]
	,NULL [tag!5!deleted]
	,NULL [value!6]
	,NULL [sessions!7]
	,ES.ExamSessionKey [examSession!8!id]
	,NULL [examSession!8!dateCompleted]
	,NULL [examSession!8!keyCode]
	,NULL [examSession!8!forename]
	,NULL [examSession!8!surname]
	,NULL [examSession!8!candidateRef]
	,NULL [examSession!8!gender]
	,NULL [examSession!8!DOB]
	,NULL [examSession!8!subject]
	,NULL [examSession!8!centre]
	,NULL [examSession!8!test]
	,NULL [examSession!8!testForm]
	,NULL [examSession!8!actualMark]
	,NULL [examSession!8!totalMark]
	,NULL [examSession!8!grade]
	,NULL [examSession!8!isVoided]
	,NULL [responses!9]
	,FQR.CPID [item!10!CPID]
	,FQR.CPVersion [item!10!CPVersion]
	,CAST(FQR.Mark*DQ.TotalMark AS decimal(9,4)) [item!10!actualMark]
	,ISNULL(CASE QuestionTypeKey
				WHEN 11 THEN REPLACE(dbo.fn_CLR_StripHTML(FQR.DerivedResponse),N'#!#',' ')
				ELSE FQR.ShortDerivedResponse
				END, '') [item!10!response]
	,FQR.ViewingTime/1000 [item!10!viewingTime]
	,FQR.ItemPresentationOrder [item!10!presentedOrder]
	,FQR.Scored [i!10!sc]
FROM 
	@ES ES
	JOIN FactQuestionResponses FQR ON FQR.ExamSessionKey = ES.ExamSessionKey
	JOIN DimQuestions DQ ON DQ.CPID = FQR.CPID AND DQ.CPVersion = FQR.CPVersion
ORDER BY  [s!8!id],[i!3!CPID],[i!3!CPV],[t!5!nam], [tag]
FOR XML EXPLICIT