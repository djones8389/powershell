DBCC FREEPROCCACHE;
DBCC DROPCLEANBUFFERS;

DECLARE @centres VARCHAR(MAX) = '30,32,33,34,36,43,45,48,49,56,57,58,59,60,61,62,63,65,68,72,73,77,78,80,82,83,84,86,87,88,93,94,95,96,97,99,101,103,105,107,108,111,112,114,117,120,121,126,129,130,131,134,135,137,141,143,144,146,148,151,153,154,156,158,160,161,163,166,167,170,173,174,175,176,177,180,181,183,187,188,189,194,195,198,199,201,203,205,208,209,212,213,215,216,219,222,223,224,226,227,230,231,235,237,238,239,241,242,244,246,250,251,254,255,259,264,267,272,274,275,277,279,281,283,286,288,289,290,292,293,294,296,297,299,301,302,303,305,309,310,311,312,317,318,322,323,324,328,329,330,331,332,334,337,339,343,345,348,349,352,353,358,359,361,363,364,367,368,370,374,375,376,377,378,379,380,382,383,386,387,389,392,393,394,396,398,399,400,401,402,403,405,407,409,410,412,413,415,417,419,421'
DECLARE @subjects VARCHAR(MAX) = '103,104,110,112,116,121,129,130,133,135,136,137,140,142,144,145,146,148,150,156,161,162,168,173,187,191,195,198,204,205,209,210,214,220,223'
DECLARE @dateStartRange DATE = '2000-01-01';
DECLARE @dateEndRange DATE = '2015-12-31';
DECLARE @examVersion INT = 1;
DECLARE @exam INT = 1;

;WITH ES AS (
	SELECT 
		FES.ExamSessionKey
		,DT.FullDateAlternateKey + FES.CompletionTime [dateCompleted]
	FROM [dbo].[FactExamSessions] FES
		JOIN [dbo].[DimTime] DT ON DT.TimeKey = FES.CompletionDateKey
	WHERE 
	(DT.FullDateAlternateKey + FES.CompletionTime) BETWEEN @dateStartRange
			AND @dateEndRange
		AND (FES.ExamKey = @exam AND (CAST(@examVersion AS int) = 0 OR FES.ExamVersionKey = @examVersion))
		AND FES.QualificationKey IN (
			SELECT Value
			FROM [dbo].[fn_ParamsToList](@subjects,0)
			)
		AND FES.CentreKey IN (
			SELECT Value
			FROM [dbo].[fn_ParamsToList](@centres,0)
			)
)
, items AS
(
	SELECT FQR.ExamSessionKey
		,FQR.CPID
		,CASE WHEN DQ.CAQuestionTypeKey IS NOT NULL THEN DQ.ExternalId ELSE NULL END CAID
		,FQR.CPVersion
		,CONVERT(NVARCHAR(4000), DQ.QuestionName) [Name]
		,CONVERT(NVARCHAR(4000), FQR.Mark) [Actual Mark]
		,CONVERT(NVARCHAR(4000), DQ.TotalMark) [Total Mark]
		,CONVERT(NVARCHAR(4000), ISNULL(CASE DQ.QuestionTypeKey
					WHEN 11 THEN REPLACE(dbo.fn_CLR_StripHTML(FQR.DerivedResponse),N'#!#',' ')
					ELSE FQR.ShortDerivedResponse
					END, '')) [Response]
		,CONVERT(NVARCHAR(4000), ISNULL(CASE DQ.QuestionTypeKey
					WHEN 11 THEN dbo.fn_CLR_StripHTML(DQ.DerivedCorrectAnswer)
					ELSE DQ.ShortDerivedCorrectAnswer
					END, '')) [Key]
		,CONVERT(NVARCHAR(4000), FQR.ViewingTime/1000) [ViewingTime]
		,CONVERT(NVARCHAR(4000), FQR.ItemPresentationOrder) [Presented Order]
		,DQ.CAQuestionTypeKey QuestionType
		--,(SELECT dbo.ag_CLR_Concat(attribval, ' | ') FROM DimQuestionMetaData DQM WHERE CPID = FQR.CPID AND CPVersion = FQR.CPVersion AND attrib = 'LO') [LO]
		--,(SELECT dbo.ag_CLR_Concat(attribval, ' | ') FROM DimQuestionMetaData DQM WHERE CPID = FQR.CPID AND CPVersion = FQR.CPVersion AND attrib = 'Unit') [Unit] --As of 2014-05-21 there may be only one unit, but DB structure allows multiple units.
	FROM 
		ES
		JOIN [dbo].[FactQuestionResponses] FQR ON ES.ExamSessionKey = FQR.ExamSessionKey
		JOIN [dbo].[DimQuestions] DQ ON DQ.CPID = FQR.CPID AND DQ.CPVersion = FQR.CPVersion
	
), Metadata AS (
	SELECT 
		I.ExamSessionKey
		,DQM.CPID
		,attrib
		,attribType
		,attribval
		,externalId
		,deleted
	FROM 
		Items I
		JOIN DimQuestionMetaData DQM 
			ON I.CPID = DQM.CPID 
			AND I.CPVersion = DQM.CPVersion
	WHERE 
		attribType IS NOT NULL
)
SELECT 
	1 [Tag]
	,NULL [Parent]
	,NULL [report!1]
	,NULL [examSession!2!id]
	,NULL [examSession!2!dateCompleted]
	,NULL [examSession!2!keyCode]
	,NULL [examSession!2!forename]
	,NULL [examSession!2!surname]
	,NULL [examSession!2!candidateRef]
	,NULL [examSession!2!gender]
	,NULL [examSession!2!DOB]
	,NULL [examSession!2!subject]
	,NULL [examSession!2!centre]
	,NULL [examSession!2!test]
	,NULL [examSession!2!testForm]
	,NULL [examSession!2!actualMark]
	,NULL [examSession!2!totalMark]
	,NULL [examSession!2!grade]
	,NULL [examSession!2!isVoided]
	,NULL [items!3]
	,NULL [item!5!id]
	,NULL [item!5!CAID]
	,NULL [item!5!name]
	,NULL [item!5!actualMark]
	,NULL [item!5!totalMark]
	,NULL [item!5!response]
	,NULL [item!5!key]
	,NULL [item!5!viewingTime]
	,NULL [item!5!presentedOrder]
	,NULL [item!5!questionType]
	--,NULL [item!5!lo]
	--,NULL [item!5!unit]
	,NULL [metadata!7]
	,NULL [tag!8!name]	
	,NULL [tag!8!type]
	,NULL [tag!8!id]
	,NULL [tag!8!deleted]
	,NULL [value!9]
	
UNION ALL

SELECT 
	2 [Tag]
	,1 [Parent]
	,NULL [report!1]
	,FES.ExamSessionKey '@id'
	,ES.dateCompleted '@dateCompleted'
	,FES.KeyCode '@keyCode'
	,DC.Forename '@forename'
	,DC.Surname '@surname'
	,DC.CandidateRef '@candidateRef'
	,DC.Gender '@gender'
	,DC.DOB '@DOB'
	,DQu.QualificationName '@subject'
	,DCe.CentreName '@centre'
	,DE.ExamName '@test'
	,DEV.ExamVersionName '@testForm'
	,FES.UserMarks '@actualMark'
	,FES.TotalMarksAvailable '@totalMark'
	,DG.Grade '@grade'
	,CASE FinalExamState WHEN 10 THEN 1 ELSE 0 END [examSession!2!isVoided]
	,NULL [items!3]
	,NULL [item!5!id]
	,NULL [item!5!CAID]
	,NULL [item!5!name]
	,NULL [item!5!actualMark]
	,NULL [item!5!totalMark]
	,NULL [item!5!response]
	,NULL [item!5!key]
	,NULL [item!5!viewingTime]
	,NULL [item!5!presentedOrder]
	,NULL [item!5!questionType]
	--,NULL [item!5!lo]
	--,NULL [item!5!unit]
	,NULL [metadata!7]
	,NULL [tag!8!name]	
	,NULL [tag!8!type]
	,NULL [tag!8!id]
	,NULL [tag!8!deleted]
	,NULL [value!9]
FROM ES
	JOIN FactExamSessions FES ON ES.ExamSessionKey = FES.ExamSessionKey
	LEFT JOIN [dbo].[DimCandidate] DC ON DC.CandidateKey = FES.CandidateKey
	LEFT JOIN [dbo].[DimQualifications] DQu ON DQu.QualificationKey = FES.QualificationKey
	LEFT JOIN [dbo].[DimCentres] DCe ON DCe.CentreKey = FES.CentreKey
	LEFT JOIN [dbo].[DimExams] DE ON DE.ExamKey = FES.ExamKey
	LEFT JOIN [dbo].[DimExamVersions] DEV ON DEV.ExamKey = FES.ExamKey
		AND DEV.ExamVersionKey = FES.ExamVersionKey
	LEFT JOIN [dbo].[DimGrades] DG ON DG.GradeKey = FES.AdjustedGradeKey

UNION ALL

SELECT 
	3 [Tag]
	,2 [Parent]
	,NULL [report!1]
	,ExamSessionKey [examSession!2!id]
	,NULL [examSession!2!dateCompleted]
	,NULL [examSession!2!keyCode]
	,NULL [examSession!2!forename]
	,NULL [examSession!2!surname]
	,NULL [examSession!2!candidateRef]
	,NULL [examSession!2!gender]
	,NULL [examSession!2!DOB]
	,NULL [examSession!2!subject]
	,NULL [examSession!2!centre]
	,NULL [examSession!2!test]
	,NULL [examSession!2!testForm]
	,NULL [examSession!2!actualMark]
	,NULL [examSession!2!totalMark]
	,NULL [examSession!2!grade]
	,NULL [examSession!2!isVoided]
	,'' [items!3]
	,NULL [item!5!id]
	,NULL [item!5!CAID]
	,NULL [item!5!name]
	,NULL [item!5!actualMark]
	,NULL [item!5!totalMark]
	,NULL [item!5!response]
	,NULL [item!5!key]
	,NULL [item!5!viewingTime]
	,NULL [item!5!presentedOrder]
	,NULL [item!5!questionType]
	--,NULL [item!5!lo]
	--,NULL [item!5!unit]
	,NULL [metadata!7]
	,NULL [tag!8!name]	
	,NULL [tag!8!type]
	,NULL [tag!8!id]
	,NULL [tag!8!deleted]
	,NULL [value!9]
FROM ES

UNION ALL

SELECT 
	5
	,3
	,NULL [report!1]
	,ExamSessionKey [examSession!2!id]
	,NULL [examSession!2!dateCompleted]
	,NULL [examSession!2!keyCode]
	,NULL [examSession!2!forename]
	,NULL [examSession!2!surname]
	,NULL [examSession!2!candidateRef]
	,NULL [examSession!2!gender]
	,NULL [examSession!2!DOB]
	,NULL [examSession!2!subject]
	,NULL [examSession!2!centre]
	,NULL [examSession!2!test]
	,NULL [examSession!2!testForm]
	,NULL [examSession!2!actualMark]
	,NULL [examSession!2!totalMark]
	,NULL [examSession!2!grade]
	,NULL [examSession!2!isVoided]
	,NULL
	,CPID 'item/@id'
	,CAID 'item/@CAID'
	,[Name] 'item/@name'
	,CAST([Actual Mark] AS FLOAT)*CAST([Total Mark] AS FLOAT) 'item/@actualMark'
	,CAST([Total Mark] AS FLOAT) 'item/@totalMark'
	,[Response] 'item/@response'
	,[Key] 'item/@key'
	,CAST([ViewingTime] AS FLOAT) 'item/@viewingTime'
	,[Presented Order] 'item/@presentedOrder'
	,QuestionType [item!5!questionType]
	--,[LO] 'item/@lo'
	--,[Unit] 'item/@unit'
	,NULL [metadata!7]
	,NULL [tag!8!name]	
	,NULL [tag!8!type]
	,NULL [tag!8!id]
	,NULL [tag!8!deleted]
	,NULL [value!9]
FROM items

UNION ALL

SELECT DISTINCT
	7 [Tag]
	,5 [Parent]
	,NULL [report!1]
	,ExamSessionKey [examSession!2!id]
	,NULL [examSession!2!dateCompleted]
	,NULL [examSession!2!keyCode]
	,NULL [examSession!2!forename]
	,NULL [examSession!2!surname]
	,NULL [examSession!2!candidateRef]
	,NULL [examSession!2!gender]
	,NULL [examSession!2!DOB]
	,NULL [examSession!2!subject]
	,NULL [examSession!2!centre]
	,NULL [examSession!2!test]
	,NULL [examSession!2!testForm]
	,NULL [examSession!2!actualMark]
	,NULL [examSession!2!totalMark]
	,NULL [examSession!2!grade]
	,NULL [examSession!2!isVoided]
	,NULL
	,CPID [item!2!id]
	,NULL [item!5!CAID]
	,NULL [item!2!name]
	,NULL [item!2!actualMark]
	,NULL [item!2!totalMark]
	,NULL [item!2!response]
	,NULL [item!2!key]
	,NULL [item!2!viewingTime]
	,NULL [item!2!presentedOrder]
	,NULL [item!5!questionType]
	--,NULL [item!2!lo]
	--,NULL [item!2!unit]
	,'' [metadata!7]
	,NULL [tag!8!name]	
	,NULL [tag!8!type]
	,NULL [tag!8!id]
	,NULL [tag!8!deleted]
	,NULL [value!9]
FROM Metadata

UNION ALL

SELECT DISTINCT
	8 [Tag]
	,7 [Parent]
	,NULL [report!1]
	,ExamSessionKey [examSession!2!id]
	,NULL [examSession!2!dateCompleted]
	,NULL [examSession!2!keyCode]
	,NULL [examSession!2!forename]
	,NULL [examSession!2!surname]
	,NULL [examSession!2!candidateRef]
	,NULL [examSession!2!gender]
	,NULL [examSession!2!DOB]
	,NULL [examSession!2!subject]
	,NULL [examSession!2!centre]
	,NULL [examSession!2!test]
	,NULL [examSession!2!testForm]
	,NULL [examSession!2!actualMark]
	,NULL [examSession!2!totalMark]
	,NULL [examSession!2!grade]
	,NULL [examSession!2!isVoided]
	,NULL
	,CPID [item!2!id]
	,NULL [item!5!CAID]
	,NULL [item!2!name]
	,NULL [item!2!actualMark]
	,NULL [item!2!totalMark]
	,NULL [item!2!response]
	,NULL [item!2!key]
	,NULL [item!2!viewingTime]
	,NULL [item!2!presentedOrder]
	,NULL [item!5!questionType]
	--,NULL [item!2!lo]
	--,NULL [item!2!unit]
	,''
	,attrib AS [tag!8!name]
	,attribType AS [tag!8!type]
	,externalId AS [tag!8!id]
	,deleted AS [tag!8!deleted]
	,NULL [value!9]
FROM Metadata

UNION ALL

SELECT DISTINCT
	9 [Tag]
	,8 [Parent]
	,NULL [report!1]
	,ExamSessionKey [examSession!2!id]
	,NULL [examSession!2!dateCompleted]
	,NULL [examSession!2!keyCode]
	,NULL [examSession!2!forename]
	,NULL [examSession!2!surname]
	,NULL [examSession!2!candidateRef]
	,NULL [examSession!2!gender]
	,NULL [examSession!2!DOB]
	,NULL [examSession!2!subject]
	,NULL [examSession!2!centre]
	,NULL [examSession!2!test]
	,NULL [examSession!2!testForm]
	,NULL [examSession!2!actualMark]
	,NULL [examSession!2!totalMark]
	,NULL [examSession!2!grade]
	,NULL [examSession!2!isVoided]
	,NULL
	,CPID [item!2!id]
	,NULL [item!5!CAID]
	,NULL [item!2!name]
	,NULL [item!2!actualMark]
	,NULL [item!2!totalMark]
	,NULL [item!2!response]
	,NULL [item!2!key]
	,NULL [item!2!viewingTime]
	,NULL [item!2!presentedOrder]
	,NULL [item!5!questionType]
	--,NULL [item!2!lo]
	--,NULL [item!2!unit]
	,''
	,attrib AS [tag!8!name]
	,NULL AS [tag!8!type]
	,NULL AS [tag!8!id]
	,NULL AS [tag!8!deleted]
	,attribval AS [value!9]
FROM Metadata

ORDER BY [examSession!2!id],[item!5!id], [tag!8!name], [tag]
FOR XML EXPLICIT
OPTION (MAXRECURSION 0)