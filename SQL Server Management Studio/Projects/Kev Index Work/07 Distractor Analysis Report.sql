DBCC FREEPROCCACHE;
DBCC DROPCLEANBUFFERS;

DECLARE @centres VARCHAR(MAX) = '30,32,33,34,36,43,45,48,49,56,57,58,59,60,61,62,63,65,68,72,73,77,78,80,82,83,84,86,87,88,93,94,95,96,97,99,101,103,105,107,108,111,112,114,117,120,121,126,129,130,131,134,135,137,141,143,144,146,148,151,153,154,156,158,160,161,163,166,167,170,173,174,175,176,177,180,181,183,187,188,189,194,195,198,199,201,203,205,208,209,212,213,215,216,219,222,223,224,226,227,230,231,235,237,238,239,241,242,244,246,250,251,254,255,259,264,267,272,274,275,277,279,281,283,286,288,289,290,292,293,294,296,297,299,301,302,303,305,309,310,311,312,317,318,322,323,324,328,329,330,331,332,334,337,339,343,345,348,349,352,353,358,359,361,363,364,367,368,370,374,375,376,377,378,379,380,382,383,386,387,389,392,393,394,396,398,399,400,401,402,403,405,407,409,410,412,413,415,417,419,421'
DECLARE @subjects INT = 104;
DECLARE @dateStartRange DATE = '2000-01-01';
DECLARE @dateEndRange DATE = '2015-12-31';
DECLARE @testform INT = 1;
DECLARE @CPID VARCHAR(50) = '5001P1';
DECLARE @CPVersion INT = 2;

DECLARE @StartDateKey int = (SELECT TimeKey FROM DimTime WHERE CAST(FullDateAlternateKey AS DATE)=CAST(@dateStartRange AS DATE))
DECLARE @EndDateKey int =	(SELECT TimeKey FROM DimTime WHERE CAST(FullDateAlternateKey AS DATE)=CAST(@dateEndRange AS DATE))
IF @StartDateKey IS NULL BEGIN
	IF @dateStartRange<=(SELECT MIN(FullDateAlternateKey) FROM DimTime) SELECT @StartDateKey = MIN(TimeKey) FROM DimTime
	ELSE SELECT @StartDateKey = MAX(TimeKey)+1 FROM DimTime
END
IF @EndDateKey IS NULL BEGIN
	IF @dateEndRange<=(SELECT MIN(FullDateAlternateKey) FROM DimTime) SELECT @EndDateKey = MIN(TimeKey)-1 FROM DimTime
	ELSE SELECT @EndDateKey = MAX(TimeKey) FROM DimTime
END

DECLARE @MarkType int, @ItemVersion int, @CAQuestionType int

SELECT 
	@MarkType = ISNULL(MarkType, 0)
	,@CAQuestionType = CAQuestionTypeKey --If the version isn't specified -- use the LatestVersion to get item's data
	,@ItemVersion = CPVersion --If the version isn't specified - use the LatestVersion to get item's data
FROM 
	DimQuestions 
WHERE 
	CPID = @CPID 
	AND (
		CPVersion = @CPVersion
		OR (
			@CPVersion IS NULL
			AND CPVersion = LatestVersion
		)
	)

DECLARE @Answers TABLE (AnswerText nvarchar(4000), Mark nvarchar(100), IsCorrect bit)

IF @CAQuestionType<>3
	INSERT INTO @Answers
	SELECT 
		ShortText
		,CASE @MarkType 
			WHEN 1 THEN ISNULL(WeightedMark, 0)
			ELSE NULL
		END Mark
		,CASE @MarkType 
			WHEN 1 THEN 
				CASE 
					WHEN WeightedMark>0 THEN 1
					ELSE NULL
				END
			ELSE IsCorrect
		END IsCorrect
	FROM 
		DimOptions DO 
	WHERE 
		CPID = @CPID
		AND CPVersion = @ItemVersion


;WITH Responses_raw AS (
	SELECT 
		NTILE(3) OVER(PARTITION BY ES.ExamKey ORDER BY ES.UserMarks DESC, ES.ExamSessionKey) Inx --It`s okay to make NTILE() over joined set since the report requires exactly one item in parameters so for every exam session in FES there should be only one corresponding response in FQR
		,ES.ExamSessionKey
		,ES.CentreKey
		,ES.QualificationKey
		,ES.ExamKey
		,FQR.CPID
		,FQR.CPVersion
		,CASE WHEN Attempted=0 THEN NULL ELSE FQR.RawResponse END RawResponse
		,CAST(CAST(CAST(DQ.TotalMark*FQR.Mark AS decimal(9,2)) AS float) AS nvarchar(10)) Mark --to avoid trailing zeros
	FROM 
		dbo.FactExamSessions AS ES
		JOIN FactQuestionResponses FQR
			ON FQR.ExamSessionKey = ES.ExamSessionKey
			AND (@CPID IS NULL OR FQR.CPID = @CPID)
			AND (@CPVersion IS NULL OR FQR.CPVersion = @CPVersion)
		JOIN DimQuestions DQ
				ON FQR.CPID = DQ.CPID
				AND FQR.CPVersion = DQ.CPVersion
	WHERE	 
		ES.CompletionDateKey BETWEEN @StartDateKey AND @EndDateKey
		AND	ES.FinalExamState <> 10
		AND	ES.CentreKey IN (SELECT Value FROM dbo.fn_ParamsToList(@centres,0))
		AND	ES.QualificationKey IN (SELECT Value FROM dbo.fn_ParamsToList(@subjects,0))
		AND ES.ExamVersionKey = @testForm 
		AND DQ.QuestionTypeKey=10
), Chars_raw AS (
	SELECT 
		Inx
		,ExamSessionKey
		,CPID
		,CPVersion
		,ISNULL(c.i.value('data(@ac)', 'nvarchar(10)'), 'UA') ac
		,Mark
	FROM Responses_raw
	OUTER APPLY RawResponse.nodes('p/s/c/i[@sl=1]') c(i)
), Responses AS (
	SELECT 
		Inx
		,ExamSessionKey
		,CPID
		,CPVersion
		,
			(SELECT 
				ac 
			  FROM 
				Chars_raw CR2
			WHERE 
				CR.ExamSessionKey = CR2.ExamSessionKey
				AND CR.CPID = CR2.CPID
				AND CR.CPVersion = CR2.CPVersion
				AND CR.Inx = CR2.Inx
			ORDER BY ac
			FOR XML PATH(''), TYPE).value('.', 'varchar(max)')
		txt
		,Mark
	FROM 
		Chars_raw CR
	GROUP BY 
		Inx
		,ExamSessionKey
		,CPID
		,CPVersion
		--,CASE @MarkType WHEN 2 THEN '1' ELSE CR.ac END
		,Mark
), Responses_Grouped AS (
	SELECT 
		Inx
		,txt
		,Mark
		,COUNT(ExamSessionKey) [count]
	FROM 
		Responses RF
	GROUP BY
		Inx
		,txt
		,Mark
), fnl AS (
	SELECT 
		ISNULL(Inx, 1) Inx
		,ISNULL(txt, AnswerText) [character]
		,ISNULL([count], 0) [count]
		,ISNULL(IsCorrect, 0) IsCorrect
		,RG.Mark
		,A.Mark AMark
	FROM 
		Responses_Grouped RG
		FULL OUTER JOIN @Answers A ON RG.txt = A.AnswerText
)
SELECT  
	1 AS [Tag]
	,NULL AS [Parent]
	,NULL AS [report!1]
	,NULL AS [item!2!Id]
	,NULL AS [item!2!markType]
	,NULL AS [option!3!group]
	,NULL AS [option!3!character]
	,NULL AS [option!3!isCorrect]
	,NULL AS [option!3!count]
	,NULL AS [option!3!mark]

UNION ALL

SELECT
	2
	,1
	,NULL
	,@CPID CPID
	,@MarkType
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL

UNION ALL

SELECT
	3
	,2
	,NULL
	,NULL
	,NULL
	,Inx 
	,[character]
	,IsCorrect
	,[count]
	,Mark
FROM fnl
ORDER BY  [Tag]
FOR XML EXPLICIT
OPTION (MAXRECURSION 0)


