USE [Demo_SDWH_Perf2]
GO

/****** Object:  Index [idxExamVersion]    Script Date: 02/19/2016 08:49:24 ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[FactExamSessions]') AND name = N'02_idx_fes_qk')
DROP INDEX [02_idx_fes_qk] ON [dbo].[FactExamSessions] WITH ( ONLINE = OFF )
GO


USE [Demo_SDWH_Perf2]
GO
CREATE NONCLUSTERED INDEX [02_idx_fes_qk]
ON [dbo].[FactExamSessions] ([QualificationKey])
INCLUDE ([CompletionDateKey],[CompletionTime],[CentreKey],[FinalExamState])
GO
