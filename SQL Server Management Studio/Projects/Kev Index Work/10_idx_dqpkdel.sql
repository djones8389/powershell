USE [Demo_SDWH_Perf2]
GO

/****** Object:  Index [10_idx_dqpkdel]    Script Date: 02/19/2016 08:50:01 ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[DimQuestions]') AND name = N'10_idx_dqpkdel')
DROP INDEX [10_idx_dqpkdel] ON [dbo].[DimQuestions] WITH ( ONLINE = OFF )
GO

USE [Demo_SDWH_Perf2]
GO

/****** Object:  Index [10_idx_dqpkdel]    Script Date: 02/19/2016 08:50:01 ******/
CREATE NONCLUSTERED INDEX [10_idx_dqpkdel] ON [dbo].[DimQuestions] 
(
	[ProjectKey] ASC,
	[Deleted] ASC
)
INCLUDE ( [CAQuestionTypeKey],
[CPID]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO


