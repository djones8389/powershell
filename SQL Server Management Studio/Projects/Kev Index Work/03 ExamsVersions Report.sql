DBCC FREEPROCCACHE;
DBCC DROPCLEANBUFFERS;

DECLARE @subjects VARCHAR(MAX) = '103,104,110,112,116,121,129,130,133,135,136,137,140,142,144,145,146,148,150,156,161,162,168,173,187,191,195,198,204,205,209,210,214,220,223'

DECLARE @Q TABLE (QKey int PRIMARY KEY CLUSTERED)

INSERT INTO @Q
SELECT Value From dbo.[fn_ParamsToList](@subjects,0)
OPTION (MAXRECURSION 0)

SELECT (
		SELECT
		      DE.QualificationKey as [@SubjectKey]
		     ,DE.ExamKey AS [@Key]
			 ,DE.ExamName AS [@Name]
			 ,DE.ExamReference AS [@Reference]
			,(
				SELECT DEV.ExamVersionKey AS [@Key]
					,DEV.ExamVersionName AS [@Name]
					,DEV.ExamVersionReference AS [@Reference]
					,CAST(CAST(MIN(FES.CompletionDateKey)+39080 AS datetime) AS date) AS [@FirstSat]
					,CAST(CAST(MAX(FES.CompletionDateKey)+39080 AS datetime) AS date) AS [@LastSat]
				FROM 
					DimExamVersions DEV
					JOIN FactExamSessions FES ON DEV.ExamVersionKey = FES.ExamVersionKey
				WHERE 
					DEV.ExamKey = DE.ExamKey
				GROUP BY 
					FES.QualificationKey
					,DEV.ExamVersionKey 
					,DEV.ExamVersionName
					,DEV.ExamVersionReference
				FOR XML PATH('Version')
					,ROOT('Version')
					,TYPE
				)
		FROM 
			DimExams DE
			JOIN @Q Q ON Q.QKey = DE.QualificationKey
		FOR XML PATH('Exam')
			,ROOT('Exams')
			,TYPE
		)
FOR XML PATH('Report')	,TYPE

