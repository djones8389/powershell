DBCC FREEPROCCACHE;
DBCC DROPCLEANBUFFERS;

DECLARE @centres VARCHAR(MAX) = '30,32,33,34,36,43,45,48,49,56'
--DECLARE @centres VARCHAR(MAX) = '30,32,33,34,36,43,45,48,49,56,57,58,59,60,61,62,63,65,68,72,73,77,78,80,82,83,84,86,87,88,93,94,95,96,97,99,101,103,105,107,108,111,112,114,117,120,121,126,129,130,131,134,135,137,141,143,144,146,148,151,153,154,156,158,160,161,163,166,167,170,173,174,175,176,177,180,181,183,187,188,189,194,195,198,199,201,203,205,208,209,212,213,215,216,219,222,223,224,226,227,230,231,235,237,238,239,241,242,244,246,250,251,254,255,259,264,267,272,274,275,277,279,281,283,286,288,289,290,292,293,294,296,297,299,301,302,303,305,309,310,311,312,317,318,322,323,324,328,329,330,331,332,334,337,339,343,345,348,349,352,353,358,359,361,363,364,367,368,370,374,375,376,377,378,379,380,382,383,386,387,389,392,393,394,396,398,399,400,401,402,403,405,407,409,410,412,413,415,417,419,421'
DECLARE @subjects VARCHAR(MAX) = '103,104,110,112,116,121,129,130,133,135,136,137,140,142,144,145,146,148,150,156,161,162,168,173,187,191,195,198,204,205,209,210,214,220,223'
DECLARE @datefrom DATE = '2015-01-01';
DECLARE @dateto DATE = '2015-12-31';

SELECT DISTINCT 1 AS [Tag]
	,0 AS [Parent]
	,NULL AS [report!1] -- root element
	,NULL AS [Centre!2!Key!id]
	,NULL AS [Centre!2!Name!idref]
	,NULL AS [Qualification!3!Key!id]
	,NULL AS [Qualification!3!Name!idref]
	,NULL AS [Exam!4!Key!id]
	,NULL AS [Exam!4!Completed]

UNION ALL

SELECT DISTINCT 2 AS [Tag]
	,1 AS [Parent]
	,NULL
	,[DimCentres].[CentreKey]
	,[DimCentres].[CentreName]
	,NULL
	,NULL
	,NULL
	,NULL
FROM [dbo].[FactExamSessions]
INNER JOIN [dbo].[DimCentres] ON [FactExamSessions].[CentreKey] = [DimCentres].[CentreKey]
INNER JOIN [dbo].[DimQualifications] ON [FactExamSessions].[QualificationKey] = [DimQualifications].[QualificationKey]
INNER JOIN [dbo].[DimTime] ON [FactExamSessions].[CompletionDateKey] = [DimTime].[TimeKey]
WHERE [FactExamSessions].[FinalExamState] <> 10
	AND [DimCentres].[CentreKey] IN (
		SELECT Value
		FROM dbo.fn_ParamsToList(@centres,0)
		)
	AND [DimQualifications].[QualificationKey] IN (
		SELECT Value
		FROM dbo.fn_ParamsToList(@subjects,0)
		)
	AND ([DimTime].[FullDateAlternateKey] + [FactExamSessions].[CompletionTime]) BETWEEN @datefrom
		AND @dateto

UNION ALL

SELECT DISTINCT 3
	,2
	,NULL
	,[DimCentres].[CentreKey]
	,NULL
	,[DimQualifications].[QualificationKey]
	,[DimQualifications].[QualificationName]
	,NULL
	,NULL
FROM [dbo].[FactExamSessions]
INNER JOIN [dbo].[DimCentres] ON [FactExamSessions].[CentreKey] = [DimCentres].[CentreKey]
INNER JOIN [dbo].[DimQualifications] ON [FactExamSessions].[QualificationKey] = [DimQualifications].[QualificationKey]
INNER JOIN [dbo].[DimTime] ON [FactExamSessions].[CompletionDateKey] = [DimTime].[TimeKey]
WHERE [FactExamSessions].[FinalExamState] <> 10
	AND [DimCentres].[CentreKey] IN (
		SELECT Value
		FROM dbo.fn_ParamsToList(@centres,0)
		)
	AND [DimQualifications].[QualificationKey] IN (
		SELECT Value
		FROM dbo.fn_ParamsToList(@subjects,0)
		)
	AND ([DimTime].[FullDateAlternateKey] + [FactExamSessions].[CompletionTime]) BETWEEN @datefrom
		AND @dateto

UNION ALL

SELECT 4
	,3
	,NULL
	,[DimCentres].[CentreKey]
	,NULL
	,[DimQualifications].[QualificationKey]
	,NULL
	,[FactExamSessions].[ExamSessionKey]
	,[DimTime].[FullDateAlternateKey] + [FactExamSessions].[CompletionTime]
FROM [dbo].[FactExamSessions]
INNER JOIN [dbo].[DimCentres] ON [FactExamSessions].[CentreKey] = [DimCentres].[CentreKey]
INNER JOIN [dbo].[DimQualifications] ON [FactExamSessions].[QualificationKey] = [DimQualifications].[QualificationKey]
INNER JOIN [dbo].[DimTime] ON [FactExamSessions].[CompletionDateKey] = [DimTime].[TimeKey]
WHERE [FactExamSessions].[FinalExamState] <> 10
	AND [DimCentres].[CentreKey] IN (
		SELECT Value
		FROM dbo.fn_ParamsToList(@centres,0)
		)
	AND [DimQualifications].[QualificationKey] IN (
		SELECT Value
		FROM dbo.fn_ParamsToList(@subjects,0)
		)
	AND ([DimTime].[FullDateAlternateKey] + [FactExamSessions].[CompletionTime]) BETWEEN @datefrom
		AND @dateto
ORDER BY [centre!2!key!id]
	,[qualification!3!key!id]
	,[exam!4!key!id]
FOR XML EXPLICIT
OPTION ( MAXRECURSION 0)

