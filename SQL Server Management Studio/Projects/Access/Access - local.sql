use master;

IF NOT EXISTS (
select 1
from sys.syslogins
where name = 'BTL169\AccessTestGroup'
)

CREATE LOGIN [BTL169\AccessTestGroup] FROM WINDOWS WITH DEFAULT_DATABASE=[master]
GO

GRANT CONNECT ANY DATABASE TO [BTL169\AccessTestGroup];
GRANT VIEW ANY DATABASE TO [BTL169\AccessTestGroup];
GRANT VIEW ANY DEFINITION TO [BTL169\AccessTestGroup];
GRANT VIEW SERVER STATE TO [BTL169\AccessTestGroup];
GRANT ALTER TRACE TO [BTL169\AccessTestGroup];

use msdb

exec sp_addrolemember 'SQLAgentOperatorRole','BTL169\AccessTestGroup';
exec sp_addrolemember 'SQLAgentReaderRole','BTL169\AccessTestGroup';
exec sp_addrolemember 'SQLAgentUserRole','BTL169\AccessTestGroup';

exec sp_msforeachdb '

use [?];

if (DB_ID()>4)

BEGIN

	IF NOT EXISTS (
	SELECT 1
	FROM sys.database_principals
	WHERE NAME = ''BTL169\AccessTestGroup''
	)

	CREATE USER [BTL169\AccessTestGroup] FOR LOGIN [BTL169\AccessTestGroup]
	exec sp_addrolemember ''db_datareader'',''BTL169\AccessTestGroup''
	exec sp_addrolemember ''db_datawriter'',''BTL169\AccessTestGroup''

	GRANT VIEW DEFINITION ON SCHEMA::[dbo] TO [BTL169\AccessTestGroup]
	DENY DELETE TO [BTL169\AccessTestGroup]
	DENY INSERT TO [BTL169\AccessTestGroup]

END

'