USE [master]
GO

if exists (
select 1 from sys.server_triggers where name = 'LogonAuditTrigger'
)

DROP TRIGGER [LogonAuditTrigger] ON ALL SERVER
GO

CREATE TRIGGER [LogonAuditTrigger] /* Creates trigger for logons */
ON ALL SERVER 
FOR LOGON
AS
 
BEGIN

DECLARE @LogonTriggerData xml,
@EventTime datetime,
@LoginName varchar(50),
@ClientHost varchar(50),
@LoginType varchar(50),
@HostName varchar(50),
@AppName varchar(500)
 
SET @LogonTriggerData = eventdata()
 
SET @EventTime = @LogonTriggerData.value('(/EVENT_INSTANCE/PostTime)[1]', 'datetime')
SET @LoginName = @LogonTriggerData.value('(/EVENT_INSTANCE/LoginName)[1]', 'varchar(50)')
SET @ClientHost = @LogonTriggerData.value('(/EVENT_INSTANCE/ClientHost)[1]', 'varchar(50)')
SET @HostName = HOST_NAME()
SET @AppName = APP_NAME()

IF(@LoginName = 'AccessTest2')
	BEGIN
		INSERT INTO [LogonAudit].[dbo].[LogonAuditing]
		(
		SessionId,
		LogonTime,
		HostName,
		ProgramName,
		LoginName,
		ClientHost
		)
		SELECT
		@@spid,
		@EventTime,
		@HostName,
		@AppName,
		@LoginName,
		@ClientHost 
	END
END
GO

ENABLE TRIGGER [LogonAuditTrigger] ON ALL SERVER
GO



--SELECT *
--FROM [LogonAudit].[dbo].[LogonAuditing]

