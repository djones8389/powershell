--Drop

exec sp_msforeachdb '

use [?];

if (DB_ID()>4)

BEGIN

	IF EXISTS (
	SELECT 1
	FROM sys.database_principals
	WHERE NAME = ''pre-prod-sql-1\Live Services''
	)

	DROP USER [pre-prod-sql-1\Live Services]

END

'


--Add

use msdb

exec sp_addrolemember 'SQLAgentOperatorRole','pre-prod-sql-1\Live Services'
exec sp_addrolemember 'SQLAgentReaderRole','pre-prod-sql-1\Live Services'
exec sp_addrolemember 'SQLAgentUserRole','pre-prod-sql-1\Live Services'

use master;

exec sp_msforeachdb '

use [?];

if (DB_ID()>4)

BEGIN

	IF NOT EXISTS (
	SELECT 1
	FROM sys.database_principals
	WHERE NAME = ''pre-prod-sql-1\Live Services''
	)

	CREATE USER [pre-prod-sql-1\Live Services] FOR LOGIN [pre-prod-sql-1\Live Services]
	exec sp_addrolemember ''db_datareader'',''pre-prod-sql-1\Live Services''
	exec sp_addrolemember ''db_datawriter'',''pre-prod-sql-1\Live Services''

	GRANT VIEW DEFINITION ON SCHEMA::[dbo] TO [pre-prod-sql-1\Live Services]
	DENY DELETE TO [pre-prod-sql-1\Live Services]
	DENY INSERT TO [pre-prod-sql-1\Live Services]

END

'

--See etls

