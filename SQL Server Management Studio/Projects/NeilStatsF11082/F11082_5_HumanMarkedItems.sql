IF OBJECT_ID('tempdb..##HumanMarked') IS NOT NULL DROP TABLE ##HumanMarked;

CREATE TABLE ##HumanMarked (
	client nvarchar(1000)
	, HumanMarked int
);

DECLARE @dynamic nvarchar(MAX)='';

select @dynamic +=CHAR(13)+ '

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

;with items as (
select est.id
	, esirt.ItemID
from '+name+'_SecureAssess.dbo.ExamSessionTable est
cross apply structurexml.nodes(''assessmentDetails/assessment/section/item'') a(b)

inner join '+name+'_SecureAssess.dbo.ExamSessionItemResponseTable esirt
on est.id = esirt.examsessionid
	and esirt.ItemID = a.b.value(''data(@id)[1]'',''nvarchar(10)'')

where ItemResponseData.exist(''p[@ua=1]'') = 1
	and a.b.value(''data(@markingType)[1]'',''tinyint'') = 1
	and examState != 13

union

select est.ExamSessionID
	, esirt.ItemID
from '+name+'_SecureAssess.dbo.warehouse_ExamSessionTable est
cross apply structurexml.nodes(''assessmentDetails/assessment/section/item'') a(b)

inner join '+name+'_SecureAssess.dbo.warehouse_ExamSessionItemResponseTable esirt
on est.id = esirt.warehouseexamsessionid
	and esirt.ItemID = a.b.value(''data(@id)[1]'',''nvarchar(10)'')

where est.id in (
	select SA.ID
	from '+name+'_SecureAssess.dbo.Warehouse_ExamSessionTable SA
	left join '+name+'_SurpassDataWarehouse.dbo.FactExamSessions FES
	on FES.ExamSessionKey = SA.ID
	where FES.ExamSessionKey IS NULL
)
	and ItemResponseData.exist(''p[@ua=1]'') = 1
	and a.b.value(''data(@markingType)[1]'',''tinyint'') = 1
	
union

SELECT  FCR.ExamSessionKey  
	,  fcr.CPID collate SQL_Latin1_General_CP1_CI_AS
FROM '+name+'_SurpassDataWarehouse.dbo.FactComponentResponses FCR
INNER JOIN '+name+'_SurpassDataWarehouse.dbo.[DimQuestions] DQ
on DQ.CPID = FCR.CPID
	and DQ.CPVersion = FCR.CPVersion
where DQ.MarkingTypeKey = 1 
)

INSERT ##HumanMarked
	SELECT '''+name+'''
	, count(*)
FROM Items

'
from (
	select distinct B.name
	FROM (
		select SUBSTRING(name, 0, charindex('_',name)) [Name]
			, ROW_NUMBER() OVER(PARTITION BY SUBSTRING(name, 0, charindex('_',name)) ORDER BY SUBSTRING(name, 0, charindex('_',name)) ) R
		from sys.databases
		where state_desc = 'ONLINE'
			and [Name] like '%/_%' ESCAPE '/'
			and name not like 'test%'
		) B
	where R > 3
) a

exec(@dynamic)


SELECT *
FROM ##HumanMarked
