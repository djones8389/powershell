SELECT distinct db_name()
	, dc.Country
FROM FactExamSessions FES
INNER JOIN FactExamSessionsStateAudit FEST
ON FEST.[ExamSessionKey] = FES.ExamSessionKey
INNER JOIN DimCentres DCE 
ON DCE.CentreKey = FES.CentreKey
INNER JOIN DimCountry DC
on DC.Country = DCE.Country
where FEST.ExamStateId = 6
	