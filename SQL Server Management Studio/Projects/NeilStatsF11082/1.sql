use Certify_SecureAssess

;with SA as (
SELECT ExamSessionID
FROM ExamStateChangeAuditTable
where newstate = 6 

UNION

SELECT west.ExamSessionID
FROM WAREHOUSE_ExamSessionTable west
inner join WAREHOUSE_ExamSessionTable_Shreded wests on west.id = wests.examSessionId
where started != '1900-01-01 00:00:00.000'
)

SELECT DISTINCT db_name()
	, FES.[ExamSessionKey]
FROM Certify_SurpassDataWarehouse..FactExamSessions FES
INNER JOIN Certify_SurpassDataWarehouse..FactExamSessionsStateAudit FEST
ON FEST.[ExamSessionKey] = FES.ExamSessionKey
where FEST.ExamStateId = 6

UNION

SELECT DISTINCT db_name()
	, WEST.ID
FROM WAREHOUSE_ExamSessionTable WEST
INNER JOIN SA SA
on SA.ExamSessionID = WEST.ExamSessionID
LEFT JOIN Certify_SurpassDataWarehouse..FactExamSessions FES
on FES.ExamSessionKey = WEST.ID
where FES.ExamSessionKey IS NULL

