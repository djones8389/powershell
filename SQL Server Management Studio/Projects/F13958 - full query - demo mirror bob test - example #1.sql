exec sp_executesql N'
--------------------------------------------------------
--If, for whatever reasons, the latest item version 
--is transferred from SecureAssess, not Content Author, 
--the item will not be displayed in the report
--------------------------------------------------------

--Added since Analytics cannot handle NULL parameters
IF @folder = -2 SET @folder = NULL
IF @status = -2 SET @status = NULL


IF OBJECT_ID(''tempdb..#Items'') IS NOT NULL DROP TABLE #Items
CREATE TABLE #Items(
	CPID nvarchar(50)
	,CPVersion int
	--,ProjectKey int
	--,QualificationKey int
	,ItemsetId int
	,ItemsetName nvarchar(100)
	--,PRIMARY KEY CLUSTERED (CPID, CPVersion)
)

;WITH folders AS (
	SELECT ExternalId
	FROM DimQuestions  DQ
	WHERE DQ.ExternalId = @folder
	
	UNION ALL
	
	SELECT DQ.ExternalId
	FROM DimQuestions  DQ
		JOIN folders F ON F.ExternalId = DQ.ItemParentKey
	WHERE 
		(DQ.CAQuestionTypeKey=1 OR DQ.CAQuestionTypeKey=26)
		AND DQ.Source=2
),Items AS (
	SELECT 
		DQ.CPID
		,DQ.CPVersion
		--,DQ.ProjectKey
		--,DP.QualificationKey
		,CASE WHEN DQP.CAQuestionTypeKey = 26 THEN DQP.ItemKey ELSE NULL END ItemsetId
		,CASE WHEN DQP.CAQuestionTypeKey = 26 THEN DQP.QuestionName ELSE NULL END ItemsetName
	FROM 
		DimQuestions  DQ
		JOIN DimProjects DP ON DP.ProjectKey = DQ.ProjectKey
		JOIN DimWorkflowStatuses DWS ON DQ.WorkflowStatusKey = DWS.WorkflowStatusKey
		LEFT JOIN DimQuestions DQP ON DQ.ItemParentKey = DQP.ItemKey AND DQP.CPVersion = DQP.LatestVersion AND DQP.ProjectKey = DQ.ProjectKey
	WHERE
		(DP.QualificationKey = @subject)
		AND (DQ.CAQuestionTypeKey<>1) AND (DQ.CAQuestionTypeKey<>26)
		AND DQ.Deleted<>1
		AND (DWS.ExternalId = @status OR @status IS NULL)
		AND DQ.CPVersion = DQ.LatestVersion
		AND DQ.LastModifiedDate BETWEEN @datefrom AND @dateto
		AND DQ.Source=2
		AND (
			EXISTS (SELECT * FROM folders F WHERE F.ExternalId = DQ.ItemParentKey) --subfolders
			OR @folder IS NULL -- all items
			OR (DQ.ItemParentKey = 0 AND @folder = 0) --not in folder
		)
)
INSERT INTO #Items
SELECT * FROM Items


;WITH ExamSessions AS(
	SELECT 
		ROW_NUMBER() OVER(PARTITION BY FQR.CPID ORDER BY FES.CompletionDateKey DESC, FES.CompletionTime DESC) N
		,I.CPID
		,FES.CompletionDateKey
		,FES.CompletionTime 
	FROM #Items I
		JOIN FactQuestionResponses FQR ON I.CPID = FQR.CPID --AND I.CPVersion = FQR.CPVersion --getting last used time regardless of item`s version
		JOIN FactExamSessions FES ON FQR.ExamSessionKey = FES.ExamSessionKey
	WHERE
		FES.ExcludeFromReporting <> 1
), LastUsed AS( 
	SELECT 
		CPID
		,DT.FullDateAlternateKey + ES.CompletionTime LastUsedDateTime
	FROM ExamSessions ES
	JOIN DimTime DT ON DT.TimeKey = ES.CompletionDateKey
	WHERE N=1
), Comments AS (
	SELECT I.CPID, DCU.FirstName, DCU.LastName, ActionDate, Comment, EventType
	FROM 
		DimQuestionHistory DQH
		JOIN #Items I ON I.CPID = DQH.CPID
		JOIN DimContentUsers DCU ON DQH.UserKey = DCU.UserKey
	WHERE Comment IS NOT NULL
), SourceMaterials AS (
	SELECT 
		I.CPID
		,DMI.MediaType
		,ISNULL(SMcr.name, DMI.Name) Name
		,DMI.Size
	FROM 
		#Items I 
		JOIN DimItemsSourceMaterialsCrossRef SMcr 
			ON I.CPID = SMcr.CPID 
			AND I.CPVersion = SMcr.CPVersion --Show source materials only for latest item version
		JOIN DimMediaItems DMI
			ON DMI.MediaItemKey = SMcr.MediaItemKey
), Metadata AS (
	SELECT 
		DQM.CPID
		,attrib
		,attribType
		,attribval
		,DQM.externalId
		,deleted
	FROM 
		#Items I
		JOIN DimQuestionMetaData DQM 
			ON I.CPID = DQM.CPID 
			AND I.CPVersion = DQM.CPVersion --Show tags only for latest item version
	WHERE 
		attribType IS NOT NULL
), Answers AS (
	SELECT 
		I.CPID
		,ContentType
		,ShortText
		,[text]
		,IsCorrect 
		,OrderNumber
		,PlaceholderId
		,PlaceholderOrderNumber
		,WeightedMark
	FROM
		#Items I 
		JOIN DimOptions DO
			ON DO.CPID = I.CPID AND DO.CPVersion = I.CPVersion
)
, Combinations AS (
	SELECT 
		I.CPID
		,DC.AnswerText [text]
		,DC.Mark
	FROM
		#Items I 
		JOIN DimCombinations DC
			ON DC.CPID = I.CPID AND DC.CPVersion = I.CPVersion
), Itemsets AS (
	SELECT DISTINCT 
		ItemsetId
		,ItemsetName
	FROM #Items
	WHERE ItemsetId IS NOT NULL
)
------------------------Header------------------------
SELECT  
	1 AS [Tag]
	,NULL AS [Parent]
	,NULL AS [report!1]
	,NULL AS [itemsets!10]
	,NULL AS [itemset!11!Id]
	,NULL AS [itemset!11!Name]
	,NULL AS [item!12!CPID]
	,NULL AS [item!12!Name]
	,NULL AS [item!12!Position]
	,NULL AS [items!19]
	,NULL AS [item!20!Id]
	,NULL AS [item!20!CAID]
	,NULL AS [item!20!Version]
	,NULL AS [item!20!QualificationName]
	,NULL AS [item!20!QuestionName]
	,NULL AS [item!20!QuestionStem]
	,NULL AS [item!20!QuestionTypeId]
	,NULL AS [item!20!Status]
	,NULL AS [item!20!StatusName]
	,NULL AS [item!20!CreatorFirstName]
	,NULL AS [item!20!CreatorLastName]
	,NULL AS [item!20!CreationDate]
	,NULL AS [item!20!ModifierFirstName]
	,NULL AS [item!20!ModifierLastName]
	,NULL AS [item!20!LastModifiedDate]
	,NULL AS [item!20!LastUsedDateTime]
	,NULL AS [item!20!MediaName]
	,NULL AS [item!20!MediaType]
	,NULL AS [item!20!MediaSize]
	,NULL AS [item!20!QuestionPath]
	,NULL AS [item!20!TotalMark]
	,NULL AS [item!20!MarkingType]
	,NULL AS [item!20!UsageCount]
	,NULL AS [item!20!FacilityValue]
	,NULL AS [item!20!SeedUsageCount]
	,NULL AS [item!20!SeedPValue]
	,NULL AS [item!20!MarkType]
	,NULL AS [item!20!ItemsetId]
	,NULL AS [sourceMaterials!30]
	,NULL AS [sourceMaterial!40!MediaType]
	,NULL AS [sourceMaterial!40!Name]
	,NULL AS [sourceMaterial!40!Size]
	,NULL AS [answers!50]
	,NULL AS [answer!60!contentType]
	,NULL AS [answer!60!character]
	,NULL AS [answer!60!text]
	,NULL AS [answer!60!isCorrect]
	,NULL AS [answer!60!orderNumber]
	,NULL AS [answer!60!placeholderId]
	,NULL AS [answer!60!placeholderOrderNumber]
	,NULL AS [answer!60!mark]
	,NULL AS [metadata!70]
	,NULL AS [tag!80!name]	
	,NULL AS [tag!80!type]
	,NULL AS [tag!80!id]
	,NULL AS [tag!80!deleted]
	,NULL AS [value!90]
	,NULL AS [comments!100]
	,NULL AS [comment!110!date]
	,NULL AS [comment!110!EventType]
	,NULL AS [comment!110!FirstName]
	,NULL AS [comment!110!LastName]	
	,NULL AS [comment!110!text]	
	,NULL AS [combinations!120]
	,NULL AS [combination!130!text]
	,NULL AS [combination!130!mark]
	,NULL AS [enemies!140]
	,NULL AS [enemyItem!150!CPID]
	,NULL AS [enemyItem!150!Name]
	,NULL AS [enemyItem!150!IsSectionEnemy]

UNION ALL
------------------------Itemsets root------------------------
SELECT 
	10
	,1
	,NULL AS [report!1]
	,NULL AS [itemsets!10]
	,0 AS [itemset!11!id]
	,NULL AS [itemset!11!name]
	,NULL AS [item!12!CPID]
	,NULL AS [item!12!name]
	,NULL AS [item!12!Position]
	,NULL AS [items!19]
	,NULL AS [item!20!Id]
	,NULL AS [item!20!CAID]
	,NULL AS [item!20!Version]
	,NULL AS [item!20!QualificationName]
	,NULL AS [item!20!QuestionName]
	,NULL AS [item!20!QuestionStem]
	,NULL AS [item!20!QuestionTypeId]
	,NULL AS [item!20!Status]
	,NULL AS [item!20!StatusName]
	,NULL AS [item!20!CreatorFirstName]
	,NULL AS [item!20!CreatorLastName]
	,NULL AS [item!20!CreationDate]
	,NULL AS [item!20!ModifierFirstName]
	,NULL AS [item!20!ModifierLastName]
	,NULL AS [item!20!LastModifiedDate]
	,NULL AS [item!20!LastUsedDateTime]
	,NULL AS [item!20!MediaName]
	,NULL AS [item!20!MediaType]
	,NULL AS [item!20!MediaSize]
	,NULL AS [item!20!QuestionPath]
	,NULL AS [item!20!TotalMark]
	,NULL AS [item!20!MarkingType]
	,NULL AS [item!20!UsageCount]
	,NULL AS [item!20!FacilityValue]
	,NULL AS [item!20!SeedUsageCount]
	,NULL AS [item!20!SeedPValue]
	,NULL AS [item!20!MarkType]
	,NULL AS [item!20!ItemsetId]
	,NULL AS [sourceMaterials!30]
	,NULL AS [sourceMaterial!40!MediaType]
	,NULL AS [sourceMaterial!40!Name]
	,NULL AS [sourceMaterial!40!Size]
	,NULL AS [answers!50]
	,NULL AS [answer!60!contentType]
	,NULL AS [answer!60!character]
	,NULL AS [answer!60!text]
	,NULL AS [answer!60!isCorrect]
	,NULL AS [answer!60!orderNumber]
	,NULL AS [answer!60!placeholderId]
	,NULL AS [answer!60!placeholderOrderNumber]
	,NULL AS [answer!60!mark]
	,NULL AS [metadata!70]
	,NULL AS [tag!80!name]	
	,NULL AS [tag!80!type]
	,NULL AS [tag!80!id]
	,NULL AS [tag!80!deleted]
	,NULL AS [value!90]
	,NULL AS [comments!100]
	,NULL AS [comment!110!date]
	,NULL AS [comment!110!EventType]
	,NULL AS [comment!110!FirstName]
	,NULL AS [comment!110!LastName]	
	,NULL AS [comment!110!text]	
	,NULL AS [combinations!120]
	,NULL AS [combination!130!text]
	,NULL AS [combination!130!mark]
	,NULL AS [enemies!140]
	,NULL AS [enemyItem!150!CPID]
	,NULL AS [enemyItem!150!name]
	,NULL AS [enemyItem!150!IsSectionEnemy]

UNION ALL
------------------------Itemsets------------------------
SELECT 
	11
	,10
	,NULL AS [report!1]
	,NULL AS [itemsets!10]
	,ItemsetId AS [itemset!11!id]
	,ItemsetName AS [itemset!11!name]
	,NULL AS [item!12!CPID]
	,NULL AS [item!12!name]
	,NULL AS [item!12!Position]
	,NULL AS [items!19]
	,NULL AS [item!20!Id]
	,NULL AS [item!20!CAID]
	,NULL AS [item!20!Version]
	,NULL AS [item!20!QualificationName]
	,NULL AS [item!20!QuestionName]
	,NULL AS [item!20!QuestionStem]
	,NULL AS [item!20!QuestionTypeId]
	,NULL AS [item!20!Status]
	,NULL AS [item!20!StatusName]
	,NULL AS [item!20!CreatorFirstName]
	,NULL AS [item!20!CreatorLastName]
	,NULL AS [item!20!CreationDate]
	,NULL AS [item!20!ModifierFirstName]
	,NULL AS [item!20!ModifierLastName]
	,NULL AS [item!20!LastModifiedDate]
	,NULL AS [item!20!LastUsedDateTime]
	,NULL AS [item!20!MediaName]
	,NULL AS [item!20!MediaType]
	,NULL AS [item!20!MediaSize]
	,NULL AS [item!20!QuestionPath]
	,NULL AS [item!20!TotalMark]
	,NULL AS [item!20!MarkingType]
	,NULL AS [item!20!UsageCount]
	,NULL AS [item!20!FacilityValue]
	,NULL AS [item!20!SeedUsageCount]
	,NULL AS [item!20!SeedPValue]
	,NULL AS [item!20!MarkType]
	,NULL AS [item!20!ItemsetId]
	,NULL AS [sourceMaterials!30]
	,NULL AS [sourceMaterial!40!MediaType]
	,NULL AS [sourceMaterial!40!Name]
	,NULL AS [sourceMaterial!40!Size]
	,NULL AS [answers!50]
	,NULL AS [answer!60!contentType]
	,NULL AS [answer!60!character]
	,NULL AS [answer!60!text]
	,NULL AS [answer!60!isCorrect]
	,NULL AS [answer!60!orderNumber]
	,NULL AS [answer!60!placeholderId]
	,NULL AS [answer!60!placeholderOrderNumber]
	,NULL AS [answer!60!mark]
	,NULL AS [metadata!70]
	,NULL AS [tag!80!name]	
	,NULL AS [tag!80!type]
	,NULL AS [tag!80!id]
	,NULL AS [tag!80!deleted]
	,NULL AS [value!90]
	,NULL AS [comments!100]
	,NULL AS [comment!110!date]
	,NULL AS [comment!110!EventType]
	,NULL AS [comment!110!FirstName]
	,NULL AS [comment!110!LastName]	
	,NULL AS [comment!110!text]	
	,NULL AS [combinations!120]
	,NULL AS [combination!130!text]
	,NULL AS [combination!130!mark]
	,NULL AS [enemies!140]
	,NULL AS [enemyItem!150!CPID]
	,NULL AS [enemyItem!150!name]
	,NULL AS [enemyItem!150!IsSectionEnemy]
FROM Itemsets

UNION ALL
------------------------Itemsets content------------------------
SELECT DISTINCT 
	12
	,11
	,NULL AS [report!1]
	,NULL AS [itemsets!10]
	,ItemsetId AS [itemset!11!id]
	,NULL AS [itemset!11!name]
	,DQ.CPID AS [item!12!CPID]
	,DQ.QuestionName AS [item!12!name]
	,DQ.Position AS [item!12!Position]
	,NULL AS [items!19]
	,NULL AS [item!20!Id]
	,NULL AS [item!20!CAID]
	,NULL AS [item!20!Version]
	,NULL AS [item!20!QualificationName]
	,NULL AS [item!20!QuestionName]
	,NULL AS [item!20!QuestionStem]
	,NULL AS [item!20!QuestionTypeId]
	,NULL AS [item!20!Status]
	,NULL AS [item!20!StatusName]
	,NULL AS [item!20!CreatorFirstName]
	,NULL AS [item!20!CreatorLastName]
	,NULL AS [item!20!CreationDate]
	,NULL AS [item!20!ModifierFirstName]
	,NULL AS [item!20!ModifierLastName]
	,NULL AS [item!20!LastModifiedDate]
	,NULL AS [item!20!LastUsedDateTime]
	,NULL AS [item!20!MediaName]
	,NULL AS [item!20!MediaType]
	,NULL AS [item!20!MediaSize]
	,NULL AS [item!20!QuestionPath]
	,NULL AS [item!20!TotalMark]
	,NULL AS [item!20!MarkingType]
	,NULL AS [item!20!UsageCount]
	,NULL AS [item!20!FacilityValue]
	,NULL AS [item!20!SeedUsageCount]
	,NULL AS [item!20!SeedPValue]
	,NULL AS [item!20!MarkType]
	,NULL AS [item!20!ItemsetId]
	,NULL AS [sourceMaterials!30]
	,NULL AS [sourceMaterial!40!MediaType]
	,NULL AS [sourceMaterial!40!Name]
	,NULL AS [sourceMaterial!40!Size]
	,NULL AS [answers!50]
	,NULL AS [answer!60!contentType]
	,NULL AS [answer!60!character]
	,NULL AS [answer!60!text]
	,NULL AS [answer!60!isCorrect]
	,NULL AS [answer!60!orderNumber]
	,NULL AS [answer!60!placeholderId]
	,NULL AS [answer!60!placeholderOrderNumber]
	,NULL AS [answer!60!mark]
	,NULL AS [metadata!70]
	,NULL AS [tag!80!name]	
	,NULL AS [tag!80!type]
	,NULL AS [tag!80!id]
	,NULL AS [tag!80!deleted]
	,NULL AS [value!90]
	,NULL AS [comments!100]
	,NULL AS [comment!110!date]
	,NULL AS [comment!110!EventType]
	,NULL AS [comment!110!FirstName]
	,NULL AS [comment!110!LastName]	
	,NULL AS [comment!110!text]	
	,NULL AS [combinations!120]
	,NULL AS [combination!130!text]
	,NULL AS [combination!130!mark]
	,NULL AS [enemies!140]
	,NULL AS [enemyItem!150!CPID]
	,NULL AS [enemyItem!150!name]
	,NULL AS [enemyItem!150!IsSectionEnemy]
FROM 
	Itemsets I
	JOIN DimQuestions DQ ON I.ItemsetId = DQ.ItemParentKey AND DQ.CPVersion = LatestVersion

UNION ALL

------------------------Items------------------------
SELECT 
	20
	,1
	,NULL
	,NULL AS [itemsets!10]
	,NULL AS [itemset!11!id]
	,NULL AS [itemset!11!name]
	,NULL AS [item!12!CPID]
	,NULL AS [item!12!name]
	,NULL AS [item!12!Position]
	,NULL AS [items!19]
	,DQ.CPID
	,FirstVersion.ExternalId
	,DQ.CPVersion
	,DQua.QualificationName
	,DQ.QuestionName
	,DQ.QuestionStem
	,DQ.CAQuestionTypeKey
	,DWS.ExternalId [Status]
	,DWS.Name AS [item!20!StatusName]
	,CU.FirstName
	,CU.LastName
	,DQ.CreationDate
	,MU.FirstName
	,MU.LastName
	,DQ.LastModifiedDate
	,LU.LastUsedDateTime
	,DMI.Name
	,DMI.MediaType
	,DMI.Size	
	,DQ.QuestionPath
	,TotalMark
	,MarkingTypeKey
	,UsageCount
	,FacilityValue
	,SeedUsageCount
	,SeedPValue
	,MarkType
	,ItemsetId AS [item!20!ItemsetId]
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
FROM 
	#Items I
	JOIN DimQuestions DQ ON DQ.CPID = I.CPID AND DQ.CPVersion = I.CPVersion
	JOIN DimProjects DP ON DP.ProjectKey = DQ.ProjectKey
	JOIN DimQualifications DQua ON DQua.QualificationKey = DP.QualificationKey
	JOIN DimContentUsers CU ON CU.UserKey = DQ.CreatedByUserKey
	JOIN DimWorkflowStatuses DWS ON DQ.WorkflowStatusKey = DWS.WorkflowStatusKey
	LEFT JOIN DimContentUsers MU ON MU.UserKey = DQ.ModifiedByUserKey
	LEFT JOIN LastUsed LU ON I.CPID = LU.CPID
	OUTER APPLY (
		SELECT TOP 1 DMI.*
		FROM 
			DimItemMediaItems DIMI 
			JOIN DimMediaItems DMI ON DIMI.MediaItemKey = DMI.MediaItemKey
		WHERE 
			DQ.CPID = DIMI.CPID 
			AND DQ.CPVersion = DIMI.CPVersion 
			AND [Order] = 1
	) DMI
	OUTER APPLY (SELECT TOP 1 ExternalId FROM DimQuestions DQ2 WHERE DQ2.CPID = DQ.CPID ORDER BY CPVersion ASC) FirstVersion
UNION ALL
-----------------Source materials root----------------
SELECT  
	30 AS [Tag]
	,20 AS [Parent]
	,NULL AS [report!1]
	,NULL AS [itemsets!10]
	,NULL AS [itemset!11!id]
	,NULL AS [itemset!11!name]
	,NULL AS [item!12!CPID]
	,NULL AS [item!12!name]
	,NULL AS [item!12!Position]
	,NULL AS [items!19]
	,CPID AS [item!20!Id]
	,NULL AS [item!20!CAID]
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL AS [item!20!ItemsetId]
	,''''
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
FROM #Items

UNION ALL
-------------------Source materials-------------------
SELECT  
	40
	,30
	,NULL
	,NULL AS [itemsets!10]
	,NULL AS [itemset!11!id]
	,NULL AS [itemset!11!name]
	,NULL AS [item!12!CPID]
	,NULL AS [item!12!name]
	,NULL AS [item!12!Position]
	,NULL AS [items!19]
	,CPID AS [item!20!Id]
	,NULL AS [item!20!CAID]
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL AS [item!20!ItemsetId]
	,''''
	,MediaType AS [sourceMaterial!40!MediaType]
	,Name AS [sourceMaterial!40!Name]
	,Size AS [sourceMaterial!40!Size]
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
FROM 
	SourceMaterials

UNION ALL
---------------------Answers root----------------------
SELECT  
	50 AS [Tag]
	,20 AS [Parent]
	,NULL AS [report!1]
	,NULL AS [itemsets!10]
	,NULL AS [itemset!11!id]
	,NULL AS [itemset!11!name]
	,NULL AS [item!12!CPID]
	,NULL AS [item!12!name]
	,NULL AS [item!12!Position]
	,NULL AS [items!19]
	,CPID AS [item!20!Id]
	,NULL AS [item!20!CAID]
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL AS [item!20!ItemsetId]
	,NULL
	,NULL
	,NULL
	,NULL
	,''''
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
FROM #Items

UNION ALL

------------------------Answers------------------------
SELECT  
	60 AS [Tag]
	,50 AS [Parent]
	,NULL AS [report!1]
	,NULL AS [itemsets!10]
	,NULL AS [itemset!11!id]
	,NULL AS [itemset!11!name]
	,NULL AS [item!12!CPID]
	,NULL AS [item!12!name]
	,NULL AS [item!12!Position]
	,NULL AS [items!19]
	,CPID AS [item!20!Id]
	,NULL AS [item!20!CAID]
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL AS [item!20!ItemsetId]
	,NULL
	,NULL
	,NULL
	,NULL
	,''''
	,ContentType
	,ShortText
	,[text]
	,IsCorrect
	,OrderNumber AS [answer!60!orderNumber]
	,PlaceholderId AS [answer!60!placeholderId]
	,PlaceholderOrderNumber AS [answer!60!placeholderOrderNumber]
	,WeightedMark
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
FROM Answers

UNION ALL
---------------------Metadata root---------------------
SELECT  
	70 AS [Tag]
	,20 AS [Parent]
	,NULL AS [report!1]
	,NULL AS [itemsets!10]
	,NULL AS [itemset!11!id]
	,NULL AS [itemset!11!name]
	,NULL AS [item!12!CPID]
	,NULL AS [item!12!Name]
	,NULL AS [item!12!Position]
	,NULL AS [items!19]
	,CPID AS [item!20!Id]
	,NULL AS [item!20!CAID]
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,''''
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
FROM #Items

UNION ALL
-------------------------Tags--------------------------
SELECT DISTINCT
	80 AS [Tag]
	,70 AS [Parent]
	,NULL AS [report!1]
	,NULL AS [itemsets!10]
	,NULL AS [itemset!11!id]
	,NULL AS [itemset!11!name]
	,NULL AS [item!12!CPID]
	,NULL AS [item!12!name]
	,NULL AS [item!12!Position]
	,NULL AS [items!19]
	,CPID AS [item!20!Id]
	,NULL AS [item!20!CAID]
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,''''
	,attrib AS [tag!80!name]
	,attribType AS [tag!80!type]
	,externalId AS [tag!80!id]
	,deleted AS [tag!80!deleted]
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
FROM Metadata

UNION ALL

SELECT 
	90 AS [Tag]
	,80 AS [Parent]
	,NULL AS [report!1]
	,NULL AS [itemsets!10]
	,NULL AS [itemset!11!id]
	,NULL AS [itemset!11!name]
	,NULL AS [item!12!CPID]
	,NULL AS [item!12!name]
	,NULL AS [item!12!Position]
	,NULL AS [items!19]
	,CPID AS [item!20!Id]
	,NULL AS [item!20!CAID]
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,''''
	,attrib AS [tag!80!name]
	,attribType AS [tag!80!type]
	,externalId AS [tag!80!id]
	,deleted AS [tag!80!deleted]
	,attribval AS [tag!80!value]
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
FROM Metadata

UNION ALL
---------------------Comments root---------------------
SELECT  
	100 AS [Tag]
	,20 AS [Parent]
	,NULL AS [report!1]
	,NULL AS [itemsets!10]
	,NULL AS [itemset!11!id]
	,NULL AS [itemset!11!name]
	,NULL AS [item!12!CPID]
	,NULL AS [item!12!name]
	,NULL AS [item!12!Position]
	,NULL AS [items!19]
	,CPID AS [item!20!Id]
	,NULL AS [item!20!CAID]
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,''''
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
FROM #Items
UNION ALL
-----------------------Comments-----------------------
SELECT  
	110
	,100
	,NULL
	,NULL AS [items!19]
	,NULL AS [itemsets!10]
	,NULL AS [itemset!11!id]
	,NULL AS [itemset!11!name]
	,NULL AS [item!12!CPID]
	,NULL AS [item!12!name]
	,NULL AS [item!12!Position]
	,CPID AS [item!20!Id]
	,NULL AS [item!20!CAID]
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,''''
	,ActionDate AS [comment!3!date]
	,EventType AS [comment!110!EventType]
	,FirstName AS [comment!110!FirstName]
	,LastName AS [comment!110!LastName]	
	,Comment AS [comment!3!text]
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
FROM 
	Comments

UNION ALL
---------------------Combinations root---------------------
SELECT  
	120 AS [Tag]
	,20 AS [Parent]
	,NULL AS [report!1]
	,NULL AS [itemsets!10]
	,NULL AS [itemset!11!id]
	,NULL AS [itemset!11!name]
	,NULL AS [item!12!CPID]
	,NULL AS [item!12!name]
	,NULL AS [item!12!Position]
	,NULL AS [items!19]
	,CPID AS [item!20!Id]
	,NULL AS [item!20!CAID]
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,''''
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
FROM #Items

UNION ALL
---------------------Combinations---------------------
SELECT  
	130 AS [Tag]
	,120 AS [Parent]
	,NULL AS [report!1]
	,NULL AS [itemsets!10]
	,NULL AS [itemset!11!id]
	,NULL AS [itemset!11!name]
	,NULL AS [item!12!CPID]
	,NULL AS [item!12!name]
	,NULL AS [item!12!Position]
	,NULL AS [items!19]
	,CPID AS [item!20!Id]
	,NULL AS [item!20!CAID]
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,''''
	,[text]
	,Mark
	,NULL
	,NULL
	,NULL
	,NULL
FROM Combinations

UNION ALL
---------------------Enemies root---------------------
SELECT  
	140 AS [Tag]
	,20 AS [Parent]
	,NULL AS [report!1]
	,NULL AS [itemsets!10]
	,NULL AS [itemset!11!id]
	,NULL AS [itemset!11!name]
	,NULL AS [item!12!CPID]
	,NULL AS [item!12!name]
	,NULL AS [item!12!Position]
	,NULL AS [items!19]
	,CPID AS [item!20!Id]
	,NULL AS [item!20!CAID]
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,''''
	,NULL
	,NULL
	,NULL
FROM #Items

UNION ALL
---------------------Enemies---------------------
SELECT  
	150 AS [Tag]
	,140 AS [Parent]
	,NULL AS [report!1]
	,NULL AS [itemsets!10]
	,NULL AS [itemset!11!id]
	,NULL AS [itemset!11!name]
	,NULL AS [item!12!CPID]
	,NULL AS [item!12!name]
	,NULL AS [item!12!Position]
	,NULL AS [items!19]
	,I.CPID AS [item!20!Id]
	,NULL AS [item!20!CAID]
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,''''
	,DEQ.EnemyCPID
	,DQ.QuestionName
	,DEQ.IsSectionEnemy
	
FROM #Items I
	JOIN DimEnemyQuestions DEQ ON I.CPID = DEQ.CPID AND I.CPVersion = DEQ.CurrentVersion
	JOIN DimQuestions DQ ON DEQ.EnemyCPID = DQ.CPID AND DEQ.EnemyCurrentVersion = DQ.CPVersion
	
UNION ALL
SELECT  
	150 AS [Tag]
	,140 AS [Parent]
	,NULL AS [report!1]
	,NULL AS [itemsets!10]
	,NULL AS [itemset!11!id]
	,NULL AS [itemset!11!name]
	,NULL AS [item!12!CPID]
	,NULL AS [item!12!name]
	,NULL AS [item!12!Position]
	,NULL AS [items!19]
	,I.CPID AS [item!20!Id]
	,NULL AS [item!20!CAID]
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,''''
	,DEQ.CPID
	,DQ.QuestionName
	,DEQ.IsSectionEnemy

FROM #Items I
	JOIN DimEnemyQuestions DEQ ON I.CPID = DEQ.EnemyCPID AND I.CPVersion = DEQ.EnemyCurrentVersion
	JOIN DimQuestions DQ ON DEQ.CPID = DQ.CPID AND DEQ.CurrentVersion = DQ.CPVersion
	
ORDER BY 
	 [item!20!Id]
	, [enemies!140]
	, [combinations!120]
	, [comments!100]
	, [tag!80!name]
	, [metadata!70]
	, [answers!50]
	, [sourceMaterials!30]
	, [itemset!11!id]
	, [item!12!Position]
	, Tag
FOR XML Explicit
OPTION (RECOMPILE) --Added to avoid parameter sniffing which produces ineffective plans


IF OBJECT_ID(''tempdb..#Items'') IS NOT NULL DROP TABLE #Items
',N'@subject nvarchar(3),@datefrom nvarchar(19),@dateto nvarchar(19),@folder nvarchar(2),@status nvarchar(2)',@subject=N'347',@datefrom=N'1998-03-29 00:00:00',@dateto=N'2018-03-31 00:00:00',@folder=N'-2',@status=N'-2'