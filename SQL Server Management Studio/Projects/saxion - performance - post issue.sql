SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

select top 1000 *
from ExamStateChangeAuditTable NOLOCK
where StateChangeDate > '2018-02-14 12:00:00'



select EST.id, clientinformation,downloadinformation
from examsessiontable est 
inner join ExamStateChangeAuditTable escat 
on est.id = escat.examsessionid
where downloadinformation is not null
	AND StateChangeDate > '2018-02-14 12:00:00'

select id
from warehouse_examsessiontable west
where warehousetime  > '2018-02-14 12:00:00' 

select distinct est.examsessionid
from ExamSectionsTable est 
inner join ExamStateChangeAuditTable escat 
on est.examsessionid = escat.examsessionid
where   StateChangeDate > '2018-02-14 12:00:00'

/****** Script for SelectTopNRows command from SSMS  ******/
SELECT  distinct
      [examInstanceID]
      ,[IPAddress]
      ,[methodCalled]
	--  , b.statechangedate
  FROM [Saxion_SecureAssess].[dbo].[ExamSessionAuditTrail] a
  inner join ExamStateChangeAuditTable b
  on a.examinstanceid = b.examsessionid
  where StateChangeDate > '2018-02-14 12:00:00'
  order by statechangedate desc;