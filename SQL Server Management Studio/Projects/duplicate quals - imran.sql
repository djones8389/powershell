IF OBJECT_ID('tempdb..##DATALENGTH') IS NOT NULL DROP TABLE ##DATALENGTH;

CREATE TABLE ##DATALENGTH (
	client nvarchar(1000)
	, qualName nvarchar(200)
);

DECLARE @dynamic nvarchar(MAX)='';

select @dynamic +=CHAR(13)+ '
use '+QUOTENAME([NAME])+'

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

INSERT ##DATALENGTH
select db_name()
	, QualificationName
from IB3QualificationLookup 
group by QualificationName
having count(QualificationName) > 1
'
from sys.databases
where state_desc = 'ONLINE'
	and name like '%[_]SecureAssess';

exec(@dynamic);



select SUBSTRING(Client, 0, charindex('_',Client)) [Client]
	 , qualname [QualificationName]
from ##DATALENGTH
order by 1






