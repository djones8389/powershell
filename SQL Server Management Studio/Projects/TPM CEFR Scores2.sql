SELECT b.ScheduledExamRef
	, b.Name
	, cast(round(b.Score,0) as float) Score
	, eg.Code [CEFR]
	,DefaultScaleValue
FROM (
	SELECT a.*
	FROM (
	select 
		ScheduledExamRef
		, Pe.Name
		, Score
		, egd.ExamGradeId
		, DefaultScaleValue
		, ROW_NUMBER () OVER (partition by ScheduledExamRef order by (Score-Value) )R
	from [dbo].ScheduledPackageCandidateExams as SPCE

		inner join ScheduledPackageCandidates as SPC
		on SPC.ScheduledPackageCandidateId =  SPCE.Candidate_ScheduledPackageCandidateId
		inner join [dbo].[PackageExams] pe
		on pe.[PackageExamId] = SPCE.[PackageExamId]
		inner join [dbo].[Packages] p
		on p.PackageID = pe.PackageId
				
		left join PackageExamScoreBoundaries pes
		on pes.PackageExamId = pe.PackageexamId
			and pes.ExamGradeId = ExamGradeId
		
		--on pes.PackageExamId = pe.PackageId
		--	 and pes.ExamGradeId = ExamGradeId
		left join ExamGradeDescriptions egd
		on pes.examgradeid = egd.[ExamGradeId]
			and pe.ExamTypeId = egd.ExamTypeId
			and egd.DefaultMinScore = pes.Value
		--		left join [dbo].[ExamGrades] EG
		--		on EG.Id in (egd.ExamGradeId)
		
			where ScheduledExamRef In  ('NY9XYM01','5B48C501','YRDJLN01','ENCUVW01')
			--where Keycode In  ('246EW901','TTY2ZZ01')
				--and (Score-Value) > 0
	) a
	where R = 1
) B 
INNER JOIN ExamGrades EG
on EG.Id = ExamGradeId