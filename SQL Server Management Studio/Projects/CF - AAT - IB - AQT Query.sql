SELECT IB.[ProjectID] 
	, IB.ItemRef
	, MAX(IB.[Version]) version
FROM dbo.ItemTable IB 
INNER JOIN [430327-AAT-SQL2\SQL2].[AAT_CPProjectAdmin].DBO.[AQTs] SA
ON SA.ID = CAST(IB.[ProjectID] AS VARCHAR(10)) + 'P' + CAST(IB.ItemRef AS VARCHAR(10))

GROUP BY  
	IB.[ProjectID] 
	, IB.ItemRef



/*
CREATE VIEW  [AQTs]
AS
SELECT DISTINCT
	SUBSTRING(id, 0, CHARINDEX('S',ID)) ID
FROM dbo.ItemCustomQuestionTable NOLOCK
*/

SELECT TOP 100 *
FROM dbo.ItemTable nolock