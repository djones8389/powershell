USE [msdb]
GO

/****** Object:  Job [Surpass Suite DBA Maintenance]    Script Date: 05/08/2016 09:09:06 ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [[Uncategorized (Local)]]    Script Date: 05/08/2016 09:09:07 ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'Surpass Suite DBA Maintenance', 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=3, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'No description available.', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=N'andyb', 
		@notify_email_operator_name=N'DBAdmin', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Shink DBs + Logs]    Script Date: 05/08/2016 09:09:07 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Shink DBs + Logs', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=3, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'SET NOCOUNT ON;

DECLARE @Kb float
DECLARE @PageSize float
DECLARE @SQL varchar(2000)
--DECLARE @Dynamic nvarchar(MAX)='''';

SELECT @Kb = 1024.0
SELECT @PageSize=v.low/@Kb FROM master..spt_values v WHERE v.number=1 AND v.type=''E''

IF OBJECT_ID(''tempdb.dbo.#FileSize'') IS NOT NULL DROP TABLE #FileSize
IF OBJECT_ID(''tempdb.dbo.#FileStats'') IS NOT NULL  DROP TABLE #FileStats
IF OBJECT_ID(''tempdb.dbo.#LogSpace'') IS NOT NULL DROP TABLE #LogSpace

CREATE TABLE #FileSize (
      DatabaseName sysname,
      FileName sysname,
      FileSize int,
      FileGroupName sysname,
      LogicalName sysname
)

CREATE TABLE #FileStats (
      FileID int,
      FileGroup int,
      TotalExtents int,
      UsedExtents int,
      LogicalName sysname,
      FileName nchar(520)
)

CREATE TABLE #LogSpace (
      DatabaseName sysname,
      LogSize float,
      SpaceUsedPercent float,
      Status bit
)

INSERT #LogSpace EXEC (''DBCC sqlperf(logspace)'')

DECLARE @DatabaseName sysname

DECLARE cur_Databases CURSOR FAST_FORWARD FOR
      SELECT DatabaseName = [name] FROM sys.databases where state_desc = ''ONLINE'' ORDER BY DatabaseName
OPEN cur_Databases
FETCH NEXT FROM cur_Databases INTO @DatabaseName
WHILE @@FETCH_STATUS = 0
  BEGIN
      SET @SQL = ''
                        USE ['' + @DatabaseName + ''];
                        DBCC showfilestats;
                        INSERT #FileSize (DatabaseName, FileName, FileSize, FileGroupName, LogicalName)
                        SELECT '''''' +@DatabaseName + '''''', filename, size, ISNULL(FILEGROUP_NAME(groupid),''''LOG''''), [name]
                        FROM dbo.sysfiles sf;
                        ''

      INSERT #FileStats EXECUTE (@SQL)
      FETCH NEXT FROM cur_Databases INTO @DatabaseName
  END

CLOSE cur_Databases
DEALLOCATE cur_Databases
SET NOCOUNT ON



   /*   DatabaseName = fsi.DatabaseName,
      --FileGroupName = fsi.FileGroupName,
      LogicalName = RTRIM(fsi.LogicalName),
      FileName = RTRIM(fsi.FileName),
      FileSize = CAST(fsi.FileSize*@PageSize/@Kb as decimal(15,2)),
      UsedSpace = CAST(ISNULL((fs.UsedExtents*@PageSize*8.0/@Kb), fsi.FileSize*@PageSize/@Kb * ls.SpaceUsedPercent/100.0) as decimal(15,2)),
       CASE WHEN RTRIM(fsi.FileName) like ''%.mdf%'' THEN	
		  CONVERT(Nvarchar(MAX), CONVERT(DECIMAL(7,0), ROUND(CAST(ISNULL((fs.UsedExtents*@PageSize*8.0/@Kb), fsi.FileSize*@PageSize/@Kb * ls.SpaceUsedPercent/100.0) as decimal(15)) *1.05, 0)))
			ELSE  CONVERT(Nvarchar(MAX), CONVERT(DECIMAL(7,0), ROUND(CAST(ISNULL((fs.UsedExtents*@PageSize*8.0/@Kb), fsi.FileSize*@PageSize/@Kb * ls.SpaceUsedPercent/100.0) as decimal(15)) +100, 0)))		
					END AS 	''NewUsedSpace-5%'',
      FreeSpace = CAST(ISNULL(((fsi.FileSize - UsedExtents*8.0)*@PageSize/@Kb), (100.0-ls.SpaceUsedPercent)/100.0 * fsi.FileSize*@PageSize/@Kb) as decimal(15,2)),
      [FreeSpace %] = CAST(ISNULL(((fsi.FileSize - UsedExtents*8.0) / fsi.FileSize * 100.0), 100-ls.SpaceUsedPercent) as decimal(15,2)),
	  CAST(fsi.FileSize*@PageSize/@Kb as decimal(15,2)) - CONVERT(Nvarchar(MAX), CONVERT(DECIMAL(7,0), ROUND(CAST(ISNULL((fs.UsedExtents*@PageSize*8.0/@Kb), fsi.FileSize*@PageSize/@Kb * ls.SpaceUsedPercent/100.0) as decimal(15)) +100, 0))) --LDF Space we''re freeing up
	*/	
	
--SELECT @Dynamic +=CHAR(13) +

DECLARE @Dynamic TABLE (

	DBCCSTATEMENT NVARCHAR(MAX)
)

INSERT @Dynamic
SELECT      
		CASE WHEN RTRIM(fsi.FileName) like ''%.ldf%'' 
		THEN
		''USE '' + QUOTENAME(fsi.DatabaseName) + '';  DBCC SHRINKFILE ('''''' + RTRIM(fsi.LogicalName) + + '''''''' + '','' + CONVERT(Nvarchar(MAX), CONVERT(DECIMAL(7,0), ROUND(CAST(ISNULL((fs.UsedExtents*@PageSize*8.0/@Kb), fsi.FileSize*@PageSize/@Kb * ls.SpaceUsedPercent/100.0) as decimal(15))+ 100, 0))) + '');''
		ELSE 
		''USE '' + QUOTENAME(fsi.DatabaseName) + '';  DBCC SHRINKFILE ('''''' + RTRIM(fsi.LogicalName) + + '''''''' + '','' +  CONVERT(Nvarchar(MAX), CONVERT(DECIMAL(7,0), ROUND(CAST(ISNULL((fs.UsedExtents*@PageSize*8.0/@Kb), fsi.FileSize*@PageSize/@Kb * ls.SpaceUsedPercent/100.0) as decimal(15))*1.05, 0))) + '');''
		END AS 	DBCCStatement
	
FROM #FileSize fsi
LEFT JOIN #FileStats fs
      ON fs.FileName = fsi.FileName
LEFT JOIN #LogSpace ls
      ON ls.DatabaseName = fsi.DatabaseName

where 
	(
	RTRIM(fsi.FileName) like ''%.mdf%''
	AND CONVERT(Nvarchar(MAX), CONVERT(DECIMAL(7,0), ROUND(CAST((fs.UsedExtents*@PageSize*8.0/@Kb) as decimal(15))*1.05, 0))) <  CAST(fsi.FileSize*@PageSize/@Kb as decimal(15,2))
	and fsi.DatabaseName not in (''tempdb'',''model'',''msdb'', ''master'')
	AND CAST((fsi.FileSize - UsedExtents*8.0)*@PageSize/@Kb as decimal(15,2)) > 500  --Only do >500MB to make it worthwhile
	)
	      
    OR 
	(
	RTRIM(fsi.FileName) like ''%.ldf%''
	AND CAST(ISNULL((fs.UsedExtents*@PageSize*8.0/@Kb), fsi.FileSize*@PageSize/@Kb * ls.SpaceUsedPercent/100.0) as decimal(15,2)) <  CAST(fsi.FileSize*@PageSize/@Kb as decimal(15,2))
	and fsi.DatabaseName not in (''tempdb'',''model'',''msdb'', ''master'')
	AND CAST(fsi.FileSize*@PageSize/@Kb as decimal(15,2)) > 100 --Where it''s using more than 100MB of space on disk
	--AND DIFFERENCE(CAST(fsi.FileSize*@PageSize/@Kb as decimal(15,2)), CONVERT(Nvarchar(MAX), CONVERT(DECIMAL(7,0), ROUND(CAST(ISNULL((fs.UsedExtents*@PageSize*8.0/@Kb), fsi.FileSize*@PageSize/@Kb * ls.SpaceUsedPercent/100.0) as decimal(15)) +100, 0)))) > 10		
	AND (CAST(fsi.FileSize*@PageSize/@Kb as decimal(15,2)) - CONVERT(Nvarchar(MAX), CONVERT(DECIMAL(7,0), ROUND(CAST(ISNULL((fs.UsedExtents*@PageSize*8.0/@Kb), fsi.FileSize*@PageSize/@Kb * ls.SpaceUsedPercent/100.0) as decimal(15)) +100, 0)))) > 250
	)


DECLARE Shrink Cursor FOR
SELECT DBCCSTATEMENT
FROM @Dynamic

DECLARE @SQLToRun NVARCHAR(MAX);

OPEN Shrink

FETCH NEXT FROM Shrink INTO @SQLToRun

WHILE @@FETCH_STATUS = 0
BEGIN

EXEC(@SQLToRun);

FETCH NEXT FROM Shrink INTO @SQLToRun

END
CLOSE Shrink;
DEALLOCATE Shrink;', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Index stats before maintenance]    Script Date: 05/08/2016 09:09:07 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Index stats before maintenance', 
		@step_id=2, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=3, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'use msdb

DECLARE @To NVARCHAR(100),
		@body NVARCHAR(MAX), 
		@profile_name SYSNAME,
		@query nvarchar(MAX)

/* Get email address */
	SELECT @To = email_address FROM msdb.dbo.sysoperators WHERE name = ''Dave Jones'';

	SET @body = ERROR_MESSAGE();

	SELECT TOP(1) @profile_name = name FROM msdb.dbo.sysmail_profile


SELECT @query = ''

if OBJECT_ID(''''tempdb..#IndexResults'''') IS NOT NULL DROP TABLE #IndexResults;

CREATE TABLE #IndexResults (
	[DB_Name] sysname
	, TableName nvarchar(200)
	, IndexName nvarchar(200)
	, Avg_fragmentation_in_percent decimal(15,13)
	, page_count bigint
)

INSERT #IndexResults
EXECUTE sp_MSforeachdb N''''
		SET QUOTED_IDENTIFIER ON;
		USE [?];
		IF DB_ID() > 4
		BEGIN
			SELECT	db_NAME() [DB]
					,OBJECT_NAME(ix.object_id)
					,QUOTENAME(ix.name) [Index]
					, avg_fragmentation_in_percent 
					, ix_phy.page_count
				FROM	 sys.dm_db_index_physical_stats(DB_ID(''''''''?''''''''), NULL, NULL, NULL, NULL) ix_phy
				INNER JOIN sys.indexes ix
						ON ix_phy.object_id = ix.object_id
					AND ix_phy.index_id = ix.index_id
			WHERE	 avg_fragmentation_in_percent > 5
			 AND	 ix.index_id <> 0
			
		END'''';


SELECT *
FROM #IndexResults
ORDER BY 1,2,3,4,5
''



EXECUTE	 msdb.dbo.sp_send_dbmail
		@profile_name = @profile_name,
		@recipients = @To,
		@subject = N''Index - Before Maintenance'',
		@body = @body,
		@execute_query_database = N''master'',
		@query = @query,
		@attach_query_result_as_file = 1,
		@query_attachment_filename = N''Index Before.csv'',
		@query_result_separator = N'','',
		@query_result_no_padding = 1,
		@body_format = ''HTML'';', 
		@database_name=N'msdb', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Index Maintenance]    Script Date: 05/08/2016 09:09:07 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Index Maintenance', 
		@step_id=3, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'BEGIN TRY
	/* Reorganise any index over 5% and Rebuild any index over 30% fragmented */
	EXECUTE sp_MSforeachdb N''
		SET QUOTED_IDENTIFIER ON;
		USE [?];
		IF DB_ID() > 4
		BEGIN
		DECLARE IndexMaintenance CURSOR FOR
			SELECT	 ''''ALTER INDEX '''' + QUOTENAME(ix.name) + '''' ON '''' + QUOTENAME(DB_NAME(ix_phy.database_id)) + ''''.''''+ QUOTENAME(OBJECT_SCHEMA_NAME(ix.object_id, ix_phy.database_id)) +''''.'''' + QUOTENAME(OBJECT_NAME(ix.object_id)) + '''' '''' +
					 CASE
						WHEN avg_fragmentation_in_percent BETWEEN 5 AND 30 THEN ''''REORGANIZE''''
						ELSE ''''REBUILD''''
					 END + '''';''''
				FROM	 sys.dm_db_index_physical_stats(DB_ID(''''?''''), NULL, NULL, NULL, NULL) ix_phy
				INNER JOIN sys.indexes ix
						ON ix_phy.object_id = ix.object_id
					AND ix_phy.index_id = ix.index_id
			WHERE	 avg_fragmentation_in_percent > 5
			 AND	 ix.index_id <> 0
			ORDER BY database_id;
		DECLARE @SQL NVARCHAR(1000);
		OPEN IndexMaintenance;
		FETCH NEXT FROM IndexMaintenance INTO @SQL;
		WHILE @@FETCH_STATUS = 0
		BEGIN
			EXECUTE(@SQL);
			FETCH NEXT FROM IndexMaintenance INTO @SQL;
		END
		CLOSE IndexMaintenance;
		DEALLOCATE IndexMaintenance;
		END'';
END TRY
BEGIN CATCH
	/* BANG! - Just sent the raw error message */
	DECLARE @To NVARCHAR(100),
		@body NVARCHAR(MAX), 
		@profile_name SYSNAME;

	/* Get email address */
	SELECT @To = email_address FROM msdb.dbo.sysoperators WHERE name = ''DBAdmin'';

	SET @body = ERROR_MESSAGE();

	SELECT TOP(1) @profile_name = name FROM msdb.dbo.sysmail_profile

	EXECUTE	 msdb.dbo.sp_send_dbmail
			 @profile_name = @profile_name,
			 @recipients = @To,
			 @subject = N''Index Maitenance Error'',
			 @body = @body,
			 @body_format = ''HTML'';
END CATCH', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Index stats after maintenance]    Script Date: 05/08/2016 09:09:07 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Index stats after maintenance', 
		@step_id=4, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=3, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'use msdb

DECLARE @To NVARCHAR(100),
		@body NVARCHAR(MAX), 
		@profile_name SYSNAME,
		@query nvarchar(MAX)

/* Get email address */
	SELECT @To = email_address FROM msdb.dbo.sysoperators WHERE name = ''Dave Jones'';

	SET @body = ERROR_MESSAGE();

	SELECT TOP(1) @profile_name = name FROM msdb.dbo.sysmail_profile


SELECT @query = ''

if OBJECT_ID(''''tempdb..#IndexResults'''') IS NOT NULL DROP TABLE #IndexResults;

CREATE TABLE #IndexResults (
	[DB_Name] sysname
	, TableName nvarchar(200)
	, IndexName nvarchar(200)
	, Avg_fragmentation_in_percent decimal(15,13)
	, page_count bigint
)

INSERT #IndexResults
EXECUTE sp_MSforeachdb N''''
		SET QUOTED_IDENTIFIER ON;
		USE [?];
		IF DB_ID() > 4
		BEGIN
			SELECT	db_NAME() [DB]
					,OBJECT_NAME(ix.object_id)
					,QUOTENAME(ix.name) [Index]
					, avg_fragmentation_in_percent 
					, ix_phy.page_count
				FROM	 sys.dm_db_index_physical_stats(DB_ID(''''''''?''''''''), NULL, NULL, NULL, NULL) ix_phy
				INNER JOIN sys.indexes ix
						ON ix_phy.object_id = ix.object_id
					AND ix_phy.index_id = ix.index_id
			WHERE	 avg_fragmentation_in_percent > 5
			 AND	 ix.index_id <> 0
			
		END'''';


SELECT *
FROM #IndexResults
ORDER BY 1,2,3,4,5
''



EXECUTE	 msdb.dbo.sp_send_dbmail
		@profile_name = @profile_name,
		@recipients = @To,
		@subject = N''Index - After Maintenance'',
		@body = @body,
		@execute_query_database = N''master'',
		@query = @query,
		@attach_query_result_as_file = 1,
		@query_attachment_filename = N''Index After.csv'',
		@query_result_separator = N'','',
		@query_result_no_padding = 1,
		@body_format = ''HTML'';
', 
		@database_name=N'msdb', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Update Statistics]    Script Date: 05/08/2016 09:09:07 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Update Statistics', 
		@step_id=5, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'BEGIN TRY
	/* Does what it says on the tin */
	EXECUTE sp_MSforeachdb N''USE [?]; EXECUTE sp_updatestats;'';
END TRY
BEGIN CATCH
	/* BANG! */
	DECLARE @To NVARCHAR(100),
		@body NVARCHAR(MAX), 
		@profile_name SYSNAME;

	/* Get email address */
	SELECT @To = email_address FROM msdb.dbo.sysoperators WHERE name = ''DBAdmin'';

	SET @body = ERROR_MESSAGE();

	SELECT TOP(1) @profile_name = name FROM msdb.dbo.sysmail_profile;

	EXECUTE	 msdb.dbo.sp_send_dbmail
			 @profile_name = @profile_name,
			 @recipients = @To,
			 @subject = N''Statistics Maintenance Error'',
			 @body = @body,
			 @body_format = ''HTML'';
END CATCH', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Purge History]    Script Date: 05/08/2016 09:09:07 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Purge History', 
		@step_id=6, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'/* Cleanup data over 1 month old */
DECLARE @dt DATETIME;
SET @dt = DATEADD(MONTH, -1, GETDATE());
EXECUTE msdb.dbo.sp_delete_backuphistory @dt;
EXECUTE msdb.dbo.sp_purge_jobhistory  @oldest_date = @dt;
EXECUTE msdb..sp_maintplan_delete_log NULL, NULL, @dt;', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'9PM', 
		@enabled=1, 
		@freq_type=8, 
		@freq_interval=111, 
		@freq_subday_type=1, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=1, 
		@active_start_date=20150824, 
		@active_end_date=99991231, 
		@active_start_time=3000, 
		@active_end_time=235959, 
		@schedule_uid=N'53c099f6-ae22-4bef-89c2-907b350e7504'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:

GO


