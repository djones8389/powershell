DECLARE BACKUPDB CURSOR FOR
SELECT 
	d.name AS [Database Name]

FROM sys.databases as d
where d.recovery_model_desc = 'FULL'
	and d.name <> 'model'
GO

DECLARE @DBName nvarchar(150);


OPEN BACKUPDB;

FETCH NEXT FROM BACKUPDB INTO @DBName

WHILE @@FETCH_STATUS = 0


BEGIN

DECLARE @BackupFilePath nvarchar(1000) = 'D:\DJMaintenancePlan\'
DECLARE @BackupDBFileName nvarchar(1000) = (SELECT @BackupFilePath + @DBName + '_' + REPLACE(convert(nvarchar(20),GetDate(),120),':','.') + '.bak')
DECLARE @BackupLogFileName nvarchar(1000) = (SELECT @BackupFilePath + @DBName + '_' + REPLACE(convert(nvarchar(20),GetDate(),120),':','.') + '.trn')
DECLARE @DBErrorMessage nvarchar(1000) = N'Database backup has failed for' + @DBName
DECLARE @LogErrorMessage nvarchar(1000) = N'Log file backup has failed for' + @DBName
DECLARE @TwoDayClearupDB  datetime  = (SELECT GetDate()-2)



--Task 1:  Check DB integrity,  backup DB, clear-up old DB's

		/*Check DB Integrity*/

		DBCC CHECKDB(@DBName) WITH NO_INFOMSGS

		
		/*Backup DB*/		 
		BACKUP DATABASE @DBName TO  DISK = @BackupDBFileName WITH NOFORMAT, NOINIT,  NAME = @BackupDBFileName, SKIP, REWIND, NOUNLOAD, NO_COMPRESSION,  STATS = 10;
		declare @DBbackupSetId as int
		select @DBbackupSetId = position from msdb..backupset where database_name= @DBName and backup_set_id=(select max(backup_set_id) 
						from msdb..backupset where database_name= @DBName )

		/*If backup fails..*/

		if @DBbackupSetId is null 
			begin 
				 EXECUTE msdb.dbo.sp_notify_operator @name=N'Dave Jones',@subject=N'Database backup failure',@body=@DBErrorMessage
			end

		RESTORE VERIFYONLY FROM  DISK = @BackupDBFileName WITH FILE = @DBbackupSetId,  NOUNLOAD,  NOREWIND

		/*Cleanup Task*/

		EXECUTE master.dbo.xp_delete_file 0,@BackupDBFileName,N'bak',@TwoDayClearupDB,0;
			
--Task 2: Backup Transaction log,  clear-up old transaction log(s)			

		/*Backup Log*/		
		BACKUP LOG @DBName TO  DISK = @BackupLogFileName WITH NOFORMAT, NOINIT,  NAME = @BackupLogFileName, SKIP, REWIND, NOUNLOAD,  STATS = 10;	
		declare @LogbackupSetId as int
		select @LogbackupSetId = position from msdb..backupset where database_name=@DBName and backup_set_id=(select max(backup_set_id) 
						from msdb..backupset where database_name= @DBName )
		
		/*If backup fails..*/

		if @LogbackupSetId is null 
		
			begin 
				  EXECUTE msdb.dbo.sp_notify_operator @name=N'Dave Jones',@subject=N'Log backup failure',@body=@LogErrorMessage
			end
		
		RESTORE VERIFYONLY FROM  DISK = @BackupLogFileName WITH FILE = @LogbackupSetId,  NOUNLOAD,  NOREWIND

		/*Cleanup Task*/

		EXECUTE master.dbo.xp_delete_file 0,@BackupLogFileName,N'trn',@TwoDayClearupDB,0;



		--Task 3: Reorganize/Rebuild Indexes
		
		EXEC('USE ' + @DBName + ' 

		SET NOCOUNT ON;
		DECLARE @objectid int;
		DECLARE @indexid int;
		DECLARE @partitioncount bigint;
		DECLARE @schemaname nvarchar(130); 
		DECLARE @objectname nvarchar(130); 
		DECLARE @indexname nvarchar(130); 
		DECLARE @partitionnum bigint;
		DECLARE @partitions bigint;
		DECLARE @frag float;
		DECLARE @command nvarchar(4000); 
		-- Conditionally select tables and indexes from the sys.dm_db_index_physical_stats function 
		-- and convert object and index IDs to names.
		SELECT
			object_id AS objectid,
			index_id AS indexid,
			partition_number AS partitionnum,
			avg_fragmentation_in_percent AS frag
		INTO #work_to_do
		FROM sys.dm_db_index_physical_stats (DB_ID(), NULL, NULL , NULL, ''LIMITED'')
		WHERE avg_fragmentation_in_percent > 10.0 AND index_id > 0;

		-- Declare the cursor for the list of partitions to be processed.
		DECLARE partitions CURSOR FOR SELECT * FROM #work_to_do;

		-- Open the cursor.
		OPEN partitions;

		-- Loop through the partitions.
		WHILE (1=1)
			BEGIN;
				FETCH NEXT
				   FROM partitions
				   INTO @objectid, @indexid, @partitionnum, @frag;
				IF @@FETCH_STATUS < 0 BREAK;
				SELECT @objectname = QUOTENAME(o.name), @schemaname = QUOTENAME(s.name)
				FROM sys.objects AS o
				JOIN sys.schemas as s ON s.schema_id = o.schema_id
				WHERE o.object_id = @objectid;
				SELECT @indexname = QUOTENAME(name)
				FROM sys.indexes
				WHERE  object_id = @objectid AND index_id = @indexid;
				SELECT @partitioncount = count (*)
				FROM sys.partitions
				WHERE object_id = @objectid AND index_id = @indexid;

		-- 30 is an arbitrary decision point at which to switch between reorganizing and rebuilding.
				IF @frag < 30.0
					SET @command = N''ALTER INDEX '' + @indexname + N'' ON '' + @schemaname + N''.'' + @objectname + N'' REORGANIZE'';
				IF @frag >= 30.0
					SET @command = N''ALTER INDEX '' + @indexname + N'' ON '' + @schemaname + N''.'' + @objectname + N'' REBUILD'';
				IF @partitioncount > 1
					SET @command = @command + N'' PARTITION='' + CAST(@partitionnum AS nvarchar(10));
				EXEC (@command);
				PRINT N''Executed: '' + @command;
			END;

		-- Close and deallocate the cursor.
		CLOSE partitions;
		DEALLOCATE partitions;

		-- Drop the temporary table.
		DROP TABLE #work_to_do;')


		
FETCH NEXT FROM BACKUPDB INTO @DBName


END
CLOSE BACKUPDB;
DEALLOCATE  BACKUPDB;

