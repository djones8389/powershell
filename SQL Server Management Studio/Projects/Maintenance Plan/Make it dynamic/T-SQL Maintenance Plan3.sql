DECLARE BACKUPDB CURSOR FOR
SELECT 
	d.name AS [Database Name]

FROM sys.databases as d
where d.recovery_model_desc = 'FULL'

GO

DECLARE @DBName nvarchar(150);


OPEN BACKUPDB;

FETCH NEXT FROM BACKUPDB INTO @DBName

WHILE @@FETCH_STATUS = 0


BEGIN

DECLARE @BackupFilePath nvarchar(1000) = 'D:\DJMaintenancePlan\'
DECLARE @BackupDBFileName nvarchar(1000) = (SELECT @BackupFilePath + @DBName + '_' + REPLACE(convert(nvarchar(20),GetDate(),120),':','.') + '.bak')
DECLARE @BackupLogFileName nvarchar(1000) = (SELECT @BackupFilePath + @DBName + '_' + REPLACE(convert(nvarchar(20),GetDate(),120),':','.') + '.trn')
DECLARE @DBErrorMessage nvarchar(1000) = N'Database backup has failed for' + @DBName
DECLARE @LogErrorMessage nvarchar(1000) = N'Log file backup has failed for' + @DBName
DECLARE @TwoDayClearupDB  datetime  = (SELECT GetDate()-2)
DECLARE @Command_ONE nvarchar(max) = 'ALTER INDEX ALL ON ' + @DBName + '.? ' +'REORGANIZE'
DECLARE @Command_TWO nvarchar(max) = 'Reorganising Indexes for: ' + @DBName

--select @Command_ONE
--select @Command_TWO

--EXEC sp_MSforeachtable @command1="ALTER INDEX ALL ON AQA_SALocal.? REORGANIZE"


--Task 1:  Check DB integrity,  backup DB, clear-up old DB's

		/*Check DB Integrity*/



--'ALTER INDEX ALL ON AQA_SALOCAL.? REORGANIZE'


--		DBCC CHECKDB(@DBName) WITH NO_INFOMSGS

		
--		/*Backup DB*/		 
--		BACKUP DATABASE @DBName TO  DISK = @BackupDBFileName WITH NOFORMAT, NOINIT,  NAME = @BackupDBFileName, SKIP, REWIND, NOUNLOAD, NO_COMPRESSION,  STATS = 10;
--		declare @DBbackupSetId as int
--		select @DBbackupSetId = position from msdb..backupset where database_name= @DBName and backup_set_id=(select max(backup_set_id) 
--						from msdb..backupset where database_name= @DBName )

--		/*If backup fails..*/

--		if @DBbackupSetId is null 
--			begin 
--				 EXECUTE msdb.dbo.sp_notify_operator @name=N'Dave Jones',@subject=N'Database backup failure',@body=@DBErrorMessage
--			end

--		RESTORE VERIFYONLY FROM  DISK = @BackupDBFileName WITH FILE = @DBbackupSetId,  NOUNLOAD,  NOREWIND

--		/*Cleanup Task*/

--		EXECUTE master.dbo.xp_delete_file 0,@BackupDBFileName,N'bak',@TwoDayClearupDB,0;
			
----Task 2: Backup Transaction log,  clear-up old transaction log(s)			

--		/*Backup Log*/		
--		BACKUP LOG @DBName TO  DISK = @BackupLogFileName WITH NOFORMAT, NOINIT,  NAME = @BackupLogFileName, SKIP, REWIND, NOUNLOAD,  STATS = 10;	
--		declare @LogbackupSetId as int
--		select @LogbackupSetId = position from msdb..backupset where database_name=@DBName and backup_set_id=(select max(backup_set_id) 
--						from msdb..backupset where database_name= @DBName )
		
--		/*If backup fails..*/

--		if @LogbackupSetId is null 
		
--			begin 
--				  EXECUTE msdb.dbo.sp_notify_operator @name=N'Dave Jones',@subject=N'Log backup failure',@body=@LogErrorMessage
--			end
		
--		RESTORE VERIFYONLY FROM  DISK = @BackupLogFileName WITH FILE = @LogbackupSetId,  NOUNLOAD,  NOREWIND

--		/*Cleanup Task*/

--		EXECUTE master.dbo.xp_delete_file 0,@BackupLogFileName,N'trn',@TwoDayClearupDB,0;

--Task 3: Reorganize Indexes
		
		--Need an if statement to notify operator if it fails--

		--EXEC sp_MSforeachtable @command1= @Command_ONE, @command2 = @Command_TWO
		--EXEC sp_MSforeachtable @command1="ALTER INDEX ALL ON ? REORGANIZE", @command2 = "print 'Reorganising Indexes for:' + '?'";



		--Doesn't let me pass in 'WITH FULLSCAN',  like the UPDATE STATISTICS command does
		--EXEC sp_updatestats;  


--EXEC sp_MSforeachtable @command1='select * from AQA_SALOCAL.?'
--EXEC sp_MSforeachtable @command1='ALTER INDEX ALL ON AQA_SALOCAL.? REORGANIZE'
 EXEC sp_MSforeachtable @command1= @Command_ONE, @command2 = @Command_TWO




FETCH NEXT FROM BACKUPDB INTO @DBName

END
CLOSE BACKUPDB;
DEALLOCATE  BACKUPDB;





/*

--Task 1:  Check DB integrity,  backup DB, clear-up old DB's

		/*Check DB Integrity*/

		DBCC CHECKDB(@DBName) WITH NO_INFOMSGS

		
		/*Backup DB*/		 
		BACKUP DATABASE @DBName TO  DISK = @BackupDBFileName WITH NOFORMAT, NOINIT,  NAME = @BackupDBFileName, SKIP, REWIND, NOUNLOAD, NO_COMPRESSION,  STATS = 10;
		declare @DBbackupSetId as int
		select @DBbackupSetId = position from msdb..backupset where database_name= @DBName and backup_set_id=(select max(backup_set_id) 
						from msdb..backupset where database_name= @DBName )

		/*If backup fails..*/

		if @DBbackupSetId is null 
			begin 
				 EXECUTE msdb.dbo.sp_notify_operator @name=N'Dave Jones',@subject=N'Database backup failure',@body=@DBErrorMessage
			end

		RESTORE VERIFYONLY FROM  DISK = @BackupDBFileName WITH FILE = @DBbackupSetId,  NOUNLOAD,  NOREWIND

		/*Cleanup Task*/

		EXECUTE master.dbo.xp_delete_file 0,@BackupDBFileName,N'bak',@TwoDayClearupDB,0;
			
--Task 2: Backup Transaction log,  clear-up old transaction log(s)			

		/*Backup Log*/		
		BACKUP LOG @DBName TO  DISK = @BackupLogFileName WITH NOFORMAT, NOINIT,  NAME = @BackupLogFileName, SKIP, REWIND, NOUNLOAD,  STATS = 10;	
		declare @LogbackupSetId as int
		select @LogbackupSetId = position from msdb..backupset where database_name=@DBName and backup_set_id=(select max(backup_set_id) 
						from msdb..backupset where database_name= @DBName )
		
		/*If backup fails..*/

		if @LogbackupSetId is null 
		
			begin 
				  EXECUTE msdb.dbo.sp_notify_operator @name=N'Dave Jones',@subject=N'Log backup failure',@body=@LogErrorMessage
			end
		
		RESTORE VERIFYONLY FROM  DISK = @BackupLogFileName WITH FILE = @LogbackupSetId,  NOUNLOAD,  NOREWIND

		/*Cleanup Task*/

		EXECUTE master.dbo.xp_delete_file 0,@BackupLogFileName,N'trn',@TwoDayClearupDB,0;

--Task 3: Reorganize Indexes

		--Need an if statement to notify operator if it fails--
		EXEC sp_MSforeachtable @command1="ALTER INDEX ALL ON ? REORGANIZE", @command2 = "print 'Reorganising Indexes for:' + '?'";


		--Doesn't let me pass in 'WITH FULLSCAN',  like the UPDATE STATISTICS command does
		EXEC sp_updatestats;  

*/
EXEC sp_MSforeachtable @command1="ALTER INDEX ALL ON AQA_SALocal.? REORGANIZE"

/*
USE AdventureWorks2012; 
GO
-- Reorganize all indexes on the HumanResources.Employee table.
ALTER INDEX ALL ON HumanResources.Employee
REORGANIZE ; 
GO
*/
