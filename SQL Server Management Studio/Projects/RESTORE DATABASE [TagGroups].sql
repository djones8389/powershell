USE [master]
RESTORE DATABASE [TagGroups] 
FROM  DISK = N'E:\Backup\perf2.bak' 
WITH  FILE = 1,  MOVE N'Performance2_ContentAuthor_2016-04-05_084720' TO N'E:\Data\TagGroups.mdf',  
				MOVE N'Performance2_ContentAuthor_2016-04-05_084720_log' TO N'E:\Log\TagGroups.ldf'
, RECOVERY, NOUNLOAD, NOREWIND,  STATS = 5

GO


