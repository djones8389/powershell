select distinct 
	@@SERVERNAME [Server]
	,DB_NAME() [Database]
	,SCHEMA_NAME(schema_id) [Schema]
	,object_name(st.object_id) [Table]
	, st.row_count [Rows]	
from sys.dm_db_partition_stats st
inner join sys.objects o
on o.object_id = st.object_id
where type = 'U'
order by 1;



exec sp_spaceused 'JBMTest'

--JBMTest	1000000    	121576 KB	121224 KB	296 KB	56 KB