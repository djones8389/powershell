USE [SANDBOX_Abcawards_SecureAssess]

select --distinct
	SA.ID
	, a.b.value('@id','nvarchar(20)')
	, a.b.value('@name','nvarchar(100)')
	, a.b.value('@unit','nvarchar(100)')
	, a.b.value('@LO','nvarchar(100)')
	, i.SubjectId
	, i.Id
	, SubjectTagValues.[text]
	, MultipleValuesAllowed
	, substring(a.b.value('@id','nvarchar(20)'), CHARINDEX('P',a.b.value('@id','nvarchar(20)'))+1, LEN(a.b.value('@id','nvarchar(20)')))
from [SANDBOX_Abcawards_SecureAssess]..WAREHOUSE_ExamSessionTable SA
cross apply structurexml.nodes('assessmentDetails/assessment/section/item') a(b)

inner join [SANDBOX_Abcawards_ContentAuthor]..Subjects s
on s.Title = StructureXML.value('data(assessmentDetails/qualificationName)[1]','nvarchar(255)')

inner join [SANDBOX_Abcawards_ContentAuthor]..Items I
on I.id = substring(a.b.value('@id','nvarchar(20)'), CHARINDEX('P',a.b.value('@id','nvarchar(20)'))+1, LEN(a.b.value('@id','nvarchar(20)')))
	and i.SubjectId = s.Id
	
inner join [SANDBOX_Abcawards_ContentAuthor]..SubjectTagValueItems on SubjectTagValueItems.Item_Id = I.Id
inner join [SANDBOX_Abcawards_ContentAuthor]..SubjectTagValues on SubjectTagValues.id = SubjectTagValueItems.SubjectTagValue_Id
inner join [SANDBOX_Abcawards_ContentAuthor]..SubjectTagTypes on SubjectTagTypes.Id = SubjectTagValues.SubjectTagTypeId
inner join [SANDBOX_Abcawards_ContentAuthor]..Subjects on subjects.Id = I.SubjectId

where a.b.value('@LO','nvarchar(max)') = ''
	and a.b.value('@unit','nvarchar(max)') != ''
	--and SubjectTagValues.[text] != a.b.value('@unit','nvarchar(max)')
	and TagTypeKey = 1
	--and a.b.value('@name','nvarchar(100)') = 'MOTQ113'
	
--SELECT
--	ID
--	, structureXML
--	, resultDataFull
--INTO #ResultHolder