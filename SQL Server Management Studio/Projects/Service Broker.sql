USE [master];
GO

if exists(select 1 from sys.databases where [Name] = 'SurpassEventNotification')
	DROP DATABASE SurpassEventNotification;
GO
CREATE DATABASE SurpassEventNotification
GO
if exists(select 1 from sys.databases where [Name] = 'Surpass_SecureAssess')
	DROP DATABASE Surpass_SecureAssess;
GO
CREATE DATABASE [Surpass_SecureAssess] 
GO


ALTER DATABASE SurpassEventNotification SET NEW_BROKER WITH ROLLBACK IMMEDIATE;
GO

SELECT service_broker_guid FROM sys.databases WHERE [name] = N'SurpassEventNotification';
GO

ALTER DATABASE Surpass_SecureAssess SET NEW_BROKER WITH ROLLBACK IMMEDIATE;
GO

USE [SurpassEventNotification];
GO
/*

IF EXISTS(SELECT TOP(1) 1 FROM sys.services WHERE [name] = N'SA_TrackingNotificationService')
 DROP SERVICE [SA_TrackingNotificationService];

IF EXISTS(SELECT TOP(1) 1 FROM sys.service_queues WHERE [name] = N'SA_TrackingNotificationQueue')
 DROP QUEUE [SA_TrackingNotificationQueue];

*/
CREATE QUEUE [SA_TrackingNotificationQueue] 
WITH
 STATUS = ON,
 RETENTION = OFF;

CREATE SERVICE [SA_TrackingNotificationService] ON QUEUE [SA_TrackingNotificationQueue] 
 ([http://schemas.microsoft.com/SQL/Notifications/PostEventNotification]);


/*

IF EXISTS(SELECT TOP(1) 1 FROM sys.event_notifications WHERE [name] = N'TrackingEventNotification')
 DROP EVENT NOTIFICATION [TrackingEventNotification] ON QUEUE [TrackingRequestQueue];

IF EXISTS(SELECT TOP(1) 1 FROM sys.services WHERE [name] = N'TrackingInitiatorService')
 DROP SERVICE [TrackingInitiatorService];

IF EXISTS(SELECT TOP(1) 1 FROM sys.services WHERE [name] = N'TrackingTargetService')
 DROP SERVICE [TrackingTargetService];

IF EXISTS(SELECT TOP(1) 1 FROM sys.service_queues WHERE [name] = N'TrackingRequestQueue')
 DROP QUEUE [TrackingRequestQueue];

IF EXISTS(SELECT TOP(1) 1 FROM sys.service_queues WHERE [name] = N'TrackingResponseQueue')
 DROP QUEUE [TrackingResponseQueue];

IF EXISTS(SELECT TOP(1) 1 FROM sys.service_contracts WHERE [name] = N'TrackingContract')
 DROP CONTRACT [TrackingContract];

IF EXISTS(SELECT TOP(1) 1 FROM sys.service_message_types WHERE [name] = N'TrackingRequest')
 DROP MESSAGE TYPE [TrackingRequest];

IF EXISTS(SELECT TOP(1) 1 FROM sys.service_message_types WHERE [name] = N'TrackingResponse')
 DROP MESSAGE TYPE [TrackingResponse];

*/
CREATE MESSAGE TYPE [TrackingRequest] VALIDATION = WELL_FORMED_XML;
CREATE MESSAGE TYPE [TrackingResponse] VALIDATION = NONE;

CREATE CONTRACT [TrackingContract] ([TrackingRequest] SENT BY INITIATOR, [TrackingResponse] SENT BY TARGET);

CREATE QUEUE [TrackingRequestQueue] WITH
 STATUS = ON,
 RETENTION = OFF,
 POISON_MESSAGE_HANDLING (STATUS = OFF);

CREATE QUEUE [TrackingResponseQueue] WITH 
 STATUS = ON,
 RETENTION = OFF;

CREATE SERVICE [TrackingInitiatorService] ON QUEUE [TrackingResponseQueue] ([TrackingContract]);
CREATE SERVICE [TrackingTargetService] ON QUEUE [TrackingRequestQueue] ([TrackingContract]);

CREATE EVENT NOTIFICATION [TrackingEventNotification]
 ON QUEUE [TrackingRequestQueue]
 FOR QUEUE_ACTIVATION
 TO SERVICE 'SA_TrackingNotificationService', '0F722911-CD69-4B8B-8347-B76DECB68263'; --SurpassEventNotification Database
GO

/*
DECLARE @handle AS UNIQUEIDENTIFIER;

BEGIN DIALOG CONVERSATION @handle  
 FROM SERVICE [TrackingInitiatorService]
 TO SERVICE 'TrackingTargetService'
 ON CONTRACT [TrackingContract]
 WITH ENCRYPTION = OFF;

DECLARE @data XML = N'<root><msg>Message</msg></root>';

SEND ON CONVERSATION @handle
MESSAGE TYPE [TrackingRequest] (@data);
GO

SELECT * FROM [dbo].[TrackingRequestQueue];
SELECT * FROM [dbo].[TrackingResponseQueue];
--SELECT CAST(CAST(message_body AS nvarchar(max)) AS xml), * FROM [dbo].[SA_TrackingNotificationQueue];
SELECT CAST(CAST(message_body AS nvarchar(max)) AS xml), * FROM [SurpassEventNotification].[dbo].[SA_TrackingNotificationQueue];

RECEIVE TOP(1) *
FROM [SurpassEventNotification].[dbo].[SA_TrackingNotificationQueue];
GO

RECEIVE TOP(1) *
FROM [dbo].[TrackingRequestQueue];
GO

DECLARE @handle AS UNIQUEIDENTIFIER;

RECEIVE TOP(1) @handle = conversation_handle
FROM [dbo].[SA_TrackingNotificationQueue];

END CONVERSATION @handle WITH CLEANUP;
GO

DECLARE @handle AS UNIQUEIDENTIFIER;

RECEIVE TOP(1) @handle = conversation_handle
FROM [dbo].[TrackingRequestQueue];

END CONVERSATION @handle WITH CLEANUP;
GO

SELECT * FROM sys.dm_broker_activated_tasks;
SELECT * FROM sys.dm_broker_connections;
SELECT * FROM sys.dm_broker_forwarded_messages;
SELECT * FROM sys.dm_broker_queue_monitors;
SELECT * FROM sys.transmission_queue;
SELECT * FROM sys.conversation_endpoints;
SELECT * FROM sys.conversation_groups;
SELECT * FROM sys.service_message_types;
SELECT * FROM sys.service_queue_usages;
SELECT * FROM sys.services;
*/