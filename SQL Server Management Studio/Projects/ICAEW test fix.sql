use ICAEW_SecureAssess

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;


select forename, surname, keycode, examstate,cast(StructureXML as xml) StructureXML, resultData, resultDataFull
--into #original
from examsessiontable est
inner join UserTable ut
on ut.id = est.userid
where keycode = 'HRV9LCF6'
			
--368P936	131


select *
from ExamSessionItemResponseTable
where examsessionid = 5794
	and itemid = '368P936'

UPDATE ExamSessionItemResponseTable
set ItemResponseData = '<p cs="1" ua="1" um="0" id="368P936">
  <s ua="1" um="0" id="1">
    <c typ="4" id="8">
      <i id="1" cc="3">null</i>
    </c>
    <c wei="1" ua="1" um="0" ie="1" typ="20" id="6">
      <i id="1">
        <t bd="1001" bc="0x0" bgc="16777215" fnt="Arial">
          <r h="20">
            <c w="380" algn="ml">
              <text>7.1%29</text>
            </c>
            <c w="380" />
          </r>
          <r h="130">
            <c typ="4">
              <text>%3CP%20ALIGN%3D%22LEFT%22%3E%3CFONT%20FACE%3D%22Arial%22%20SIZE%3D%2212%22%20COLOR%3D%22%23000000%22%20LETTERSPACING%3D%220%22%20KERNING%3D%220%22%3EThey%20should%20have%20got%20the%20permission%20of%20the%20firm%20to%20contact%20the%20previous%20auditors%2C%20and%20if%20no%20permission%20was%20given%2C%20query%20as%20to%20why.%20If%20the%20previous%20auditor%20refused%20to%20tender%20due%20to%20a%20reason%20that%20could%20affect%20this%20years%20audit%20you%20need%20to%20no.%20If%20the%20client%20refused%20to%20give%20permission%20you%20should%20have%20refused%20the%20audit.%20The%20fee%20should%20have%20been%20checked%20to%20make%20sure%20it%20did%26apos%3Bt%20breach%2015%25%20of%20fee%20income%2C%20which%20is%20the%20lmit%20for%20a%20non%20listed%20company.%20As%20the%20fees%20are%204%25%20this%20audit%20should%20be%20fine%20to%20accept%20on%20that%20basis%20with%20no%20additional%20steps%20needed%20to%20address%20the%20ethics%20partner.%20The%20independence%20of%20the%20audit%20firm%20to%20THB%20should%20have%20also%20been%20confirmed%2C%20to%20make%20sure%20the%20interity%20of%20the%20audit%20wouldn%3Bt%20be%20affected.%3C/FONT%3E%3C/P%3E%3CP%20ALIGN%3D%22LEFT%22%3E%3CFONT%20FACE%3D%22Arial%22%20SIZE%3D%2212%22%20COLOR%3D%22%23000000%22%20LETTERSPACING%3D%220%22%20KERNING%3D%220%22%3E%3C/FONT%3E%3C/P%3E%3CP%20ALIGN%3D%22LEFT%22%3E%3CFONT%20FACE%3D%22Arial%22%20SIZE%3D%2212%22%20COLOR%3D%22%23000000%22%20LETTERSPACING%3D%220%22%20KERNING%3D%220%22%3E%3C/FONT%3E%3C/P%3E</text>
            </c>
            <c />
          </r>
          <r h="20">
            <c />
            <c />
          </r>
          <r h="20">
            <c>
              <text>7.2%29</text>
            </c>
            <c />
          </r>
          <r h="24">
            <c bd="0220" bld="1">
              <text>Justification</text>
            </c>
            <c bd="0022" bld="1">
              <text>Procedures</text>
            </c>
          </r>
          <r h="73">
            <c bd="2200">
              <text>Puchases%3A%20As%20the%20company%20buys%20its%20products%20from%20Asia%20invoiced%20in%20lcoal%20currency%2C%20fx%20transaltion%20errors%20could%20lead%20to%20under%20or%20overstatement</text>
            </c>
            <c bd="2002">
              <text>Retreive%20a%20sample%20of%20foreign%20invoices%20and%20perform%20the%20fx%20transdlation%20again%20and%20reconcile%20it%20to%20the%20figure%20in%20the%20trial%20balance.%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20You%20should%20also%20find%20a%20reliable%20source%20for%20fx%20rates%20and%20compare%20them%20to%20what%20was%20used%20to%20make%20sure%20no%20material%20discrepancy%20arises.</text>
            </c>
          </r>
          <r h="102">
            <c bd="0200">
              <text>Purchases%3A%20May%20understate%20pruchases%20to%20increase%20revenue%20and%20amke%20the%20accuonts%20look%20good%20for%20the%20application%20for%20the%20bank%20loan.</text>
            </c>
            <c bd="0002">
              <text>Look%20at%20the%20goods%20recevied%20note%20and%20compare%20reconile%20to%20items%20in%20the%20warehouse</text>
            </c>
          </r>
          <r h="70">
            <c bd="0200">
              <text>Trade%20payables%3A%20Payable%20days%20have%20gone%20down%20fro%2029.4%20days%20to%2025.3%20days%20%28using%20purchases%29%2C%20highlgihting%20that%20they%27re%20paying%20their%20suppliers%20back%20quicker.%20May%20have%20been%20understated%20to%20look%20good%20for%20the%20bank%20loan%20as%20it%27s%20below%20their%2030%20day%20credit%20term.</text>
            </c>
            <c bd="0002">
              <text>Acquire%20a%20samnple%20of%20supplier%20statements%20and%20reconcile%20them%20to%20the%20amounts%20sitting%20in%20the%20payables%20ledger.%20Use%20data%20analytics%20to%20create%20a%20payables%20ageing%20list%20to%20see%20if%20there%20are%20any%20that%20standout.%20Get%20direct%20confirmation%20from%20the%20suppliers%20of%20the%20amounts%20they%27re%20owed%20and%20reconcile%20it%20back%20to%20the%20amounts%20in%20the%20ledger.</text>
            </c>
          </r>
          <r h="50">
            <c bd="0200">
              <text>Purchases%3A%20Gross%20profit%20has%20incerased%20from%2045.6%25%20to%2048%25%2C%20may%20indicate%20that%20they%27re%20understating%20their%20cost%20of%20sales</text>
            </c>
            <c bd="0002" />
          </r>
          <r h="50">
            <c bd="0200">
              <text>Payables%3A%20Payables%20have%20decreased%206%25%20yet%20inventory%20has%20increased%2048%25%2C%20this%20may%20indicate%20that%20the%20invoices%20paid%20on%20returned%20goods%20is%20having%20a%20substantial%20effect</text>
            </c>
            <c bd="0002">
              <text>Look%20at%20the%20amounts%20that%20have%20been%20reutrned%20and%20ensure%20that%20they%20match%20what%20has%20been%20paid%20out</text>
            </c>
          </r>
          <r h="50">
            <c bd="0200" />
            <c bd="0002" />
          </r>
          <r h="50">
            <c bd="0200" />
            <c bd="0002" />
          </r>
          <r h="50">
            <c bd="0200" />
            <c bd="0002" />
          </r>
          <r h="50">
            <c bd="0200" />
            <c bd="0002" />
          </r>
          <r h="50">
            <c bd="0200" />
            <c bd="0002" />
          </r>
          <r h="50">
            <c bd="0200" />
            <c bd="0002" />
          </r>
          <r h="50">
            <c bd="0200" />
            <c bd="0002" />
          </r>
          <r h="50">
            <c bd="0200" />
            <c bd="0002" />
          </r>
          <r h="50">
            <c bd="0200" />
            <c bd="0002" />
          </r>
          <r h="230">
            <c bd="0200">
              <text>Inventory%3A%20Inventory%20has%20jumped%20up%2048%25%2C%20whereas%20revenue%20has%20only%20nicreased%209.3%25.%20With%20inventory%20days%20also%20increasing%20from%2050.5%20days%20to%2071.8%20days%2C%20it%20highlgihts%20there%20may%20be%20obsolete%20inventory%20sitting%20in%20warehouses%20or%20inventory%20ahs%20been%20over%20valued</text>
            </c>
            <c bd="0002">
              <text>%253CP%2520ALIGN%253D%2522LEFT%2522%253E%253CFONT%2520FACE%253D%2522Arial%2522%2520SIZE%253D%252212%2522%2520COLOR%253D%2522%2523000000%2522%2520LETTERSPACING%253D%25220%2522%2520KERNING%253D%25220%2522%253EAtten%2520the%2520inventory%2520account%2520that%2520Whitechapel%2520LLP%2520are%2520undertaking%2520to%2520ensure%2520that%2520they%2520have%2520controls%2520in%2520place%2520to%2520deal%2520with%2520any%2520oboslete%2520inventory%252C%2520and%2520to%2520see%2520how%2520they%2520get%2520dealt%2520with.%2520Do%2520some%2520sheet%2520to%2520floor%2520testing%2520to%2520test%2520existence%2520%2528overstatement%2529%252C%2520and%2520some%2520floor%2520to%2520sheet%2520testing%2520for%2520compelteness%2520%2528understatement%2529%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520We%2520should%2520also%2520assess%2520the%2520competency%2520of%2520Whitechapel%2520LLP%2520before%2520we%2520decide%2520to%2520sue%2520any%2520of%2520their%2520work.%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520%2520Inventory%2520aging%2520analysis%2520using%2520%253CB%253Edata%2520analytics%253C/B%253E%2520will%2520also%2520be%2520useful%2520here%2520as%2520you%2520could%2520do%2520an%2520aging%2520analysis%2520of%2520inventory%252C%2520which%2520will%2520highlight%2520any%2520old%2520inventory%2520that%2520may%2520need%2520to%2520be%2520written%2520off.%253C/FONT%253E%253C/P%253E</text>
            </c>
          </r>
          <r h="103">
            <c bd="0200">
              <text>Inventory%3A%20May%20be%20stated%20incorrectly%20due%20to%20misappropriation%2C%20as%20the%20goods%20they%20deal%20with%20%28shoes%20and%20bags%29%20are%20small%20and%20easy%20to%20steal.%20This%20seems%20to%20have%20been%20the%20case%20once%20before</text>
            </c>
            <c bd="0002">
              <text>Assess%20the%20controls%20they%20have%20in%20place%20to%20mitigate%20this%20and%20discuss%20with%20management%20how%20much%20of%20a%20problem%20it%20is%20and%20what%20they%27re%20doing%20to%20try%20and%20stop%20it.</text>
            </c>
          </r>
          <r h="182">
            <c bd="0200">
              <text>Inventory%3A%20Quality%20of%20products%20they%27re%20receeiving%20from%20their%20main%20supplier%20may%20indicate%20that%20the%20net%20rpesent%20value%20of%20the%20goods%20in%20inventory%20are%20less%20than%20the%20cost.%20If%20so%2C%20inventory%20mey%20be%20overstate%20and%20they%20need%20to%20be%20changed</text>
            </c>
            <c bd="0002">
              <text>%253CP%2520ALIGN%253D%2522LEFT%2522%253E%253CFONT%2520FACE%253D%2522Arial%2522%2520SIZE%253D%252212%2522%2520COLOR%253D%2522%2523000000%2522%2520LETTERSPACING%253D%25220%2522%2520KERNING%253D%25220%2522%253ECompare%2520cost%2520to%2520year%2520end%2520selling%2520price.%2520%253CB%253EData%2520analytics%253C/B%253E%2520can%2520be%2520used%2520here%2520to%2520compare%2520large%2520sets%2520of%2520data%2520and%2520see%2520any%2520discrepancies%2520between%2520selling%2520price%2520and%2520the%2520cost.%253C/FONT%253E%253C/P%253E</text>
            </c>
          </r>
          <r h="66">
            <c bd="0200">
              <text>Inventory%3A%20A%20perpetual%20inventory%20system%20in%20place%20means%20that%20there%20is%20potential%20the%20system%20may%20not%20be%20worjing%20properly</text>
            </c>
            <c bd="0002">
              <text>-%20Test%20the%20controls%20in%20place%20for%20the%20perpetual%20inventory%20system%20and%20ensure%20that%20they%27re%20reocrding%20the%20inventory%20correctly.%20Reconile%20the%20amounts%20to%20the%20year%20end%20stock%20count%20information</text>
            </c>
          </r>
          <r h="50">
            <c bd="0200">
              <text>Inventory%20-%20may%20be%20overstated%20as%20the%20reutrns%20made%20to%20their%20largest%20supplier%20haven%3Bt%20been%20accounted%20for%20yet/propely.</text>
            </c>
            <c bd="0002">
              <text>-%20Look%20at%20the%20inventory%20that%20has%20been%20reutrned%20and%20make%20sure%20none%20still%20sit%20in%20their%20warehouse%20at%20year%20end</text>
            </c>
          </r>
          <r h="50">
            <c bd="0200" />
            <c bd="0002" />
          </r>
          <r h="50">
            <c bd="0200" />
            <c bd="0002" />
          </r>
          <r h="50">
            <c bd="0200" />
            <c bd="0002" />
          </r>
          <r h="50">
            <c bd="0200" />
            <c bd="0002" />
          </r>
          <r h="50">
            <c bd="0200" />
            <c bd="0002" />
          </r>
          <r h="50">
            <c bd="0200" />
            <c bd="0002" />
          </r>
          <r h="50">
            <c bd="0200" />
            <c bd="0002" />
          </r>
          <r h="50">
            <c bd="0200" />
            <c bd="0002" />
          </r>
          <r h="50">
            <c bd="0200" />
            <c bd="0002" />
          </r>
          <r h="50">
            <c bd="0200" />
            <c bd="0002" />
          </r>
          <r h="50">
            <c bd="0200" />
            <c bd="0002" />
          </r>
          <r h="50">
            <c bd="0200" />
            <c bd="0002" />
          </r>
          <r h="50">
            <c bd="0200" />
            <c bd="0002" />
          </r>
          <r h="50">
            <c bd="0200" />
            <c bd="0002" />
          </r>
          <r h="50">
            <c bd="0200" />
            <c bd="0002" />
          </r>
          <r h="50">
            <c bd="0200" />
            <c bd="0002" />
          </r>
          <r h="50">
            <c bd="0200" />
            <c bd="0002" />
          </r>
          <r h="50">
            <c bd="0200" />
            <c bd="0002" />
          </r>
          <r h="50">
            <c bd="0200" />
            <c bd="0002" />
          </r>
          <r h="50">
            <c bd="0200" />
            <c bd="0002" />
          </r>
          <r h="50">
            <c bd="0200" />
            <c bd="0002" />
          </r>
          <r h="50">
            <c bd="0200" />
            <c bd="0002" />
          </r>
          <r h="50">
            <c bd="0200" />
            <c bd="0002" />
          </r>
          <r h="50">
            <c bd="0200" />
            <c bd="0002" />
          </r>
          <r h="20">
            <c />
            <c />
          </r>
          <r h="20">
            <c>
              <text>7.3%29</text>
            </c>
            <c />
          </r>
          <r h="196">
            <c typ="4">
              <text>%3CP%20ALIGN%3D%22LEFT%22%3E%3CFONT%20FACE%3D%22Arial%22%20SIZE%3D%2212%22%20COLOR%3D%22%23000000%22%20LETTERSPACING%3D%220%22%20KERNING%3D%220%22%3EAssisting%20with%20preparation%20-%20The%20threat%20here%20is%20to%20objectivity%20as%20you%26apos%3Bll%20potentiall%20be%20looking%20over%20work%20during%20the%20audit%20that%20ahs%20been%20been%20compelted%20by%20someone%20at%20your%20own%20firm.%20There%26apos%3Bd%20be%20a%20self-review%20threat%20as%20you%26apos%3Bd%20be%20unlikley%20to%20challenge%20it%20as%20much%20if%20it%20was%20someone%20else%20as%20you%20may%20trust%20the%20person%20who%20did%20it%20and%20therefore%20take%20over%20relaincce%20on%20their%20work.%20To%20tackle%20this%2C%20the%20firm%20ened%20to%20make%20sure%20that%20the%20person%20that%20helps%2C%20is%20not%20cnonected%20to%20the%20audit%20team%20that%26apos%3Bs%20going%20to%20work%20on%20the%20audit%20and%20also%2C%20the%20roles%20that%20they%20do%20are%20of%20a%20mechanicala%20nd%20striaghforward%20nature%20with%20no%20maangement%20decisions%2C%20which%20may%20bring%20into%20play%20maangement%20risk.%20So%20we%20need%20to%20ensure%20we%20have%20informed%20management%20%3C/FONT%3E%3C/P%3E%3CP%20ALIGN%3D%22LEFT%22%3E%3CFONT%20FACE%3D%22Arial%22%20SIZE%3D%2212%22%20COLOR%3D%22%23000000%22%20LETTERSPACING%3D%220%22%20KERNING%3D%220%22%3E%3C/FONT%3E%3C/P%3E%3CP%20ALIGN%3D%22LEFT%22%3E%3CFONT%20FACE%3D%22Arial%22%20SIZE%3D%2212%22%20COLOR%3D%22%23000000%22%20LETTERSPACING%3D%220%22%20KERNING%3D%220%22%3EForensic%20specialist%20-%20Valuation%20is%20a%20service%20that%20cannot%20be%20provided%20as%20it%20is%20a%20management%20decision%2C%20so%20the%20threat%20of%20maangement%20risk%20is%20too%20high.%20There%26apos%3Bs%20aslo%20a%20self-revew%20threat%20again%20as%20if%20you%20did%20value%20something%20as%20an%20auditor%2C%20you%26apos%3Bd%20be%20less%20inclined%20to%20audit%20the%20number%20as%20much%20as%20you%20would%20have%20done%20if%20it%20was%20someone%20else.%20By%20asking%20to%20act%20as%20an%20expert%20witness%2C%20there%26apos%3Bs%20also%20an%20advocay%20threat%2C%20as%20we%26apos%3Bd%20be%20supporting%20a%20position%20taken%20by%20management%20in%20an%20adversarial%20or%20promotional%20context.%20This%20means%20that%20in%20the%20future%20it%20may%20be%20hard%20to%20be%20impartial%20regarding%20this%20area%20of%20the%20audit%2C%20risking%20objectivity%20and%20independence.%3C/FONT%3E%3C/P%3E%3CP%20ALIGN%3D%22LEFT%22%3E%3CFONT%20FACE%3D%22Arial%22%20SIZE%3D%2212%22%20COLOR%3D%22%23000000%22%20LETTERSPACING%3D%220%22%20KERNING%3D%220%22%3E%3C/FONT%3E%3C/P%3E</text>
            </c>
            <c />
          </r>
          <r h="20">
            <c />
            <c />
          </r>
          <r h="20">
            <c>
              <text>7.4%29</text>
            </c>
            <c />
          </r>
          <r h="103">
            <c typ="4">
              <text>%3CP%20ALIGN%3D%22LEFT%22%3E%3CFONT%20FACE%3D%22Arial%22%20SIZE%3D%2212%22%20COLOR%3D%22%23000000%22%20LETTERSPACING%3D%220%22%20KERNING%3D%220%22%3EThere%20is%20a%20self%20interest%20threat%20here%20as%20the%20audit%20assisstant%20has%20clearly%20used%20the%20nformation%20for%20her%20beenfit%20and%20may%20as%20a%20result%20be%20less%20inquisitive%20of%20the%20client%20as%20she%20wants%20the%20treats%20to%20continue.%20This%20self%20itnerest%20threat%20will%20hit%20the%20itnegrity%20of%20the%20audit%20and%20the%20firm%20should%20be%20made%20aware%20of%20it.%3C/FONT%3E%3C/P%3E%3CP%20ALIGN%3D%22LEFT%22%3E%3CFONT%20FACE%3D%22Arial%22%20SIZE%3D%2212%22%20COLOR%3D%22%23000000%22%20LETTERSPACING%3D%220%22%20KERNING%3D%220%22%3E%3C/FONT%3E%3C/P%3E%3CP%20ALIGN%3D%22LEFT%22%3E%3CFONT%20FACE%3D%22Arial%22%20SIZE%3D%2212%22%20COLOR%3D%22%23000000%22%20LETTERSPACING%3D%220%22%20KERNING%3D%220%22%3E%3C/FONT%3E%3C/P%3E%3CP%20ALIGN%3D%22LEFT%22%3E%3CFONT%20FACE%3D%22Arial%22%20SIZE%3D%2212%22%20COLOR%3D%22%23000000%22%20LETTERSPACING%3D%220%22%20KERNING%3D%220%22%3E%3C/FONT%3E%3C/P%3E</text>
            </c>
            <c />
          </r>
          <r h="20">
            <c />
            <c />
          </r>
          <r h="20">
            <c />
            <c />
          </r>
          <r h="20">
            <c />
            <c />
          </r>
          <r h="20">
            <c />
            <c />
          </r>
          <r h="20">
            <c />
            <c />
          </r>
          <r h="20">
            <c />
            <c />
          </r>
          <r h="20">
            <c />
            <c />
          </r>
          <r h="20">
            <c />
            <c />
          </r>
          <r h="20">
            <c />
            <c />
          </r>
          <r h="20">
            <c />
            <c />
          </r>
          <r h="20">
            <c />
            <c />
          </r>
          <r h="20">
            <c />
            <c />
          </r>
          <r h="20">
            <c />
            <c />
          </r>
          <r h="20">
            <c />
            <c />
          </r>
          <r h="20">
            <c />
            <c />
          </r>
          <r h="20">
            <c />
            <c />
          </r>
          <r h="20">
            <c />
            <c />
          </r>
          <r h="20">
            <c />
            <c />
          </r>
          <r h="20">
            <c />
            <c />
          </r>
          <r h="20">
            <c />
            <c />
          </r>
          <r h="20">
            <c />
            <c />
          </r>
          <r h="20">
            <c />
            <c />
          </r>
          <r h="20">
            <c />
            <c />
          </r>
          <r h="20">
            <c />
            <c />
          </r>
          <r h="20">
            <c />
            <c />
          </r>
          <r h="20">
            <c />
            <c />
          </r>
          <r h="20">
            <c />
            <c />
          </r>
          <r h="20">
            <c />
            <c />
          </r>
          <merge>
            <m id="0" cols="0,1" rows="1" />
            <m id="1" cols="0,1" rows="50" />
            <m id="2" cols="0,1" rows="53" />
          </merge>
        </t>
      </i>
    </c>
    <c typ="1" id="9">
      <i id="1" />
    </c>
  </s>
</p>'
where examsessionid = 5794
	and itemid = '368P936'


UPDATE ExamSessionItemResponseTable
set ItemResponseData = --replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(cast(ItemResponseData as nvarchar(MAX)), '%2520','%20'),'%253CP%','%3CP%'),'%3CP%20ALIGN%253D%','%3CP%20ALIGN%3D%'),'%253E%','%3E%'), '%2C','%20'), '%26apos%3Bt','%20'),'15%25','%20'),'4%25','%20'), '%3CP%20ALIGN%3D%22LEFT%22%3E%3CFONT%20FACE%3D%22Arial%22%20SIZE%3D%2212%22%20COLOR%3D%22%23000000%22%20LETTERSPACING%3D%220%22%20KERNING%3D%220%22%3E',''), '%3CP%20ALIGN%3D%2522LEFT%2522%3E%253CFONT%20FACE%253D%2522Arial%2522%20SIZE%253D%252212%2522%20COLOR%253D%2522%2523000000%2522%20LETTERSPACING%253D%25220%2522%20KERNING%253D%25220%2522%253E',''), '%3C/FONT%3E%3C/P%3E%3C/FONT%3E%3C/P%3E%3C/FONT%3E%3C/P%3E',''), '%253C/FONT%3E%253C/P%253E',''), '%3C/FONT%3E%3C/P%3E%3C/FONT%3E%3C/P%3E','')
	replace(cast(ItemResponseData as nvarchar(MAX)),'%25','%')
where examsessionid = 5794
	and itemid = '368P936'


SELECT
	--cast(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(cast(ItemResponseData as nvarchar(MAX)), '%2520','%20'),'%253CP%','%3CP%'),'%3CP%20ALIGN%253D%','%3CP%20ALIGN%3D%'),'%253E%','%3E%'), '%2C','%20'), '%26apos%3Bt','%20'),'15%25','%20'),'4%25','%20'), '%3CP%20ALIGN%3D%22LEFT%22%3E%3CFONT%20FACE%3D%22Arial%22%20SIZE%3D%2212%22%20COLOR%3D%22%23000000%22%20LETTERSPACING%3D%220%22%20KERNING%3D%220%22%3E',''), '%3CP%20ALIGN%3D%2522LEFT%2522%3E%253CFONT%20FACE%253D%2522Arial%2522%20SIZE%253D%252212%2522%20COLOR%253D%2522%2523000000%2522%20LETTERSPACING%253D%25220%2522%20KERNING%253D%25220%2522%253E',''), '%3C/FONT%3E%3C/P%3E%3C/FONT%3E%3C/P%3E%3C/FONT%3E%3C/P%3E',''), '%253C/FONT%3E%253C/P%253E','') as xml).query('p/s/c/i/t/r/c/text')
	ItemResponseData --.query('p/s/c/i/t/r/c/text')
from ExamSessionItemResponseTable
where examsessionid = 5794
	and itemid = '368P936'

--broken - bobs..
select ItemResponseData,
  --cast(replace(replace(cast(ItemResponseData as nvarchar(MAX)), '%2520','%20'),'%253CP%','%3CP%') as xml)
  ItemResponseData.query('p/s/c/i/t/r/c/text')
from ExamSessionItemResponseTable
where examsessionid = 5794
	and itemid = '368P936'

--works
select ItemResponseData,
  --cast(replace(replace(cast(ItemResponseData as nvarchar(MAX)), '%2520','%20'),'%253CP%','%3CP%') as xml)
  ItemResponseData.query('p/s/c/i/t/r/c/text')
from warehouse_ExamSessionItemResponseTable wesirt
inner join WAREHOUSE_ExamSessionTable west
on west.id = wesirt.WAREHOUSEExamSessionID
where keycode = '7F4QVKF6' 
	and itemid = '368P938'





--select	qualificationname
--	, examname
--	, examversionref
--	, examversionname
--	, keycode
--from WAREHOUSE_ExamSessionTable_Shreded wests
--inner join WAREHOUSE_ExamSessionItemResponseTable wesirt
--on wests.examsessionid = wesirt.WAREHOUSEExamSessionID
--inner join warehouse_examsessiontable west
--on west.id = wests.examsessionid

--where ItemVersion = 131
--	and itemid in ('368P937','368P936')
