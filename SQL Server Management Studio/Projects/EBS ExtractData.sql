DECLARE @myXML XML = '<exam passMark="0" passType="1" originalPassMark="0" originalPassType="1" totalMark="160" userMark="50" userPercentage="31.25" passValue="1" originalPassValue="1" totalTimeSpent="9342074" closeBoundaryType="0" closeBoundaryValue="0" grade="Pass" originalGrade="Pass" closeValue="0">
  <gradeBoundaryData>
    <gradeBoundaries passLevelType="1" adjustedGradeBoundaries="0">
      <grade modifier="lt" value="0" description="Fail" />
      <grade modifier="gt" value="0" description="Pass" />
    </gradeBoundaries>
  </gradeBoundaryData>
  <section id="1" passMark="0" passType="1" name="Section I" totalMark="60" userMark="50" userPercentage="83.3333333333333" passValue="1" totalTimeSpent="3343516" itemsToMark="0">
    <item id="5016P5068" name="EC D2017 - mcq1" totalMark="2" version="2" markingType="0" markingState="0" userMark="1" markerUserMark="" userAttempted="1" viewingTime="116988" displayName="" type="1" quT="10" actualUserMark="2">
    </item>
    <item id="5016P5069" name="EC D2017 - mcq2" totalMark="2" version="3" markingType="0" markingState="0" userMark="1" markerUserMark="" userAttempted="1" viewingTime="129337" displayName="" type="1" quT="10" actualUserMark="2">
    </item>
    <item id="5016P5070" name="EC D2017 - mcq3" totalMark="2" version="2" markingType="0" markingState="0" userMark="1" markerUserMark="" userAttempted="1" viewingTime="64603" displayName="" type="1" quT="10" actualUserMark="2">
    </item>
    <item id="5016P5071" name="EC D2017 - mcq4" totalMark="2" version="4" markingType="0" markingState="0" userMark="1" markerUserMark="" userAttempted="1" viewingTime="174600" displayName="" type="1" quT="10" actualUserMark="2">
    </item>
    <item id="5016P5100" name="EC D2017 - mcq5" totalMark="2" version="12" markingType="0" markingState="0" userMark="1" markerUserMark="" userAttempted="1" viewingTime="55916" displayName="" type="1" quT="10" actualUserMark="2">
    </item>
    <item id="5016P5109" name="EC D2017 - mcq6" totalMark="2" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="1" viewingTime="156426" displayName="" type="1" quT="10" actualUserMark="0">
    </item>
    <item id="5016P5117" name="EC D2017 - mcq7" totalMark="2" version="10" markingType="0" markingState="0" userMark="1" markerUserMark="" userAttempted="1" viewingTime="159604" displayName="" type="1" quT="10" actualUserMark="2">
    </item>
    <item id="5016P5127" name="EC D2017 - mcq8" totalMark="2" version="4" markingType="0" markingState="0" userMark="1" markerUserMark="" userAttempted="1" viewingTime="30079" displayName="" type="1" quT="10" actualUserMark="2">
    </item>
    <item id="5016P5130" name="EC D2017 - mcq9" totalMark="2" version="8" markingType="0" markingState="0" userMark="1" markerUserMark="" userAttempted="1" viewingTime="73300" displayName="" type="1" quT="10" actualUserMark="2">
    </item>
    <item id="5016P5136" name="EC D2017 - mcq10" totalMark="2" version="5" markingType="0" markingState="0" userMark="1" markerUserMark="" userAttempted="1" viewingTime="41132" displayName="" type="1" quT="10" actualUserMark="2">
    </item>
    <item id="5016P5143" name="EC D2017 - mcq11" totalMark="2" version="3" markingType="0" markingState="0" userMark="1" markerUserMark="" userAttempted="1" viewingTime="39267" displayName="" type="1" quT="10" actualUserMark="2">
    </item>
    <item id="5016P5145" name="EC D2017 - mcq12" totalMark="2" version="3" markingType="0" markingState="0" userMark="1" markerUserMark="" userAttempted="1" viewingTime="120020" displayName="" type="1" quT="10" actualUserMark="2">
    </item>
    <item id="5016P5153" name="EC D2017 - mcq13" totalMark="2" version="2" markingType="0" markingState="0" userMark="1" markerUserMark="" userAttempted="1" viewingTime="205096" displayName="" type="1" quT="10" actualUserMark="2">
    </item>
    <item id="5016P5155" name="EC D2017 - mcq14" totalMark="2" version="2" markingType="0" markingState="0" userMark="1" markerUserMark="" userAttempted="1" viewingTime="155485" displayName="" type="1" quT="10" actualUserMark="2">
    </item>
    <item id="5016P5157" name="EC D2017 - mcq15" totalMark="2" version="2" markingType="0" markingState="0" userMark="1" markerUserMark="" userAttempted="1" viewingTime="45394" displayName="" type="1" quT="10" actualUserMark="2">
    </item>
    <item id="5016P5159" name="EC D2017 - mcq16" totalMark="2" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="1" viewingTime="201286" displayName="" type="1" quT="10" actualUserMark="0">
    </item>
    <item id="5016P5162" name="EC D2017 - mcq17" totalMark="2" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="1" viewingTime="73960" displayName="" type="1" quT="10" actualUserMark="0">
    </item>
    <item id="5016P5165" name="EC D2017 - mcq18" totalMark="2" version="5" markingType="0" markingState="0" userMark="1" markerUserMark="" userAttempted="1" viewingTime="140696" displayName="" type="1" quT="10" actualUserMark="2">
    </item>
    <item id="5016P5170" name="EC D2017 - mcq19" totalMark="2" version="4" markingType="0" markingState="0" userMark="1" markerUserMark="" userAttempted="1" viewingTime="113732" displayName="" type="1" quT="10" actualUserMark="2">
    </item>
    <item id="5016P5174" name="EC D2017 - mcq20" totalMark="2" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="1" viewingTime="105855" displayName="" type="1" quT="10" actualUserMark="0">
    </item>
    <item id="5016P5177" name="EC D2017 - mcq 21" totalMark="2" version="2" markingType="0" markingState="0" userMark="1" markerUserMark="" userAttempted="1" viewingTime="175055" displayName="" type="1" quT="10" actualUserMark="2">
    </item>
    <item id="5016P5179" name="EC D2017 - mcq22" totalMark="2" version="2" markingType="0" markingState="0" userMark="1" markerUserMark="" userAttempted="1" viewingTime="294142" displayName="" type="1" quT="10" actualUserMark="2">
    </item>
    <item id="5016P5181" name="EC D2017 - mcq23" totalMark="2" version="2" markingType="0" markingState="0" userMark="1" markerUserMark="" userAttempted="1" viewingTime="53755" displayName="" type="1" quT="10" actualUserMark="2">
    </item>
    <item id="5016P5183" name="EC D2017 - mcq24" totalMark="2" version="3" markingType="0" markingState="0" userMark="1" markerUserMark="" userAttempted="1" viewingTime="119924" displayName="" type="1" quT="10" actualUserMark="2">
    </item>
    <item id="5016P5186" name="EC D2017 - mcq25" totalMark="2" version="2" markingType="0" markingState="0" userMark="1" markerUserMark="" userAttempted="1" viewingTime="63180" displayName="" type="1" quT="10" actualUserMark="2">
    </item>
    <item id="5016P5188" name="EC D2017 - mcq26" totalMark="2" version="2" markingType="0" markingState="0" userMark="1" markerUserMark="" userAttempted="1" viewingTime="65830" displayName="" type="1" quT="10" actualUserMark="2">
    </item>
    <item id="5016P5190" name="EC D2017 - mcq27" totalMark="2" version="2" markingType="0" markingState="0" userMark="1" markerUserMark="" userAttempted="1" viewingTime="58444" displayName="" type="1" quT="10" actualUserMark="2">
    </item>
    <item id="5016P5192" name="EC D2017 - mcq28" totalMark="2" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="1" viewingTime="107238" displayName="" type="1" quT="10" actualUserMark="0">
    </item>
    <item id="5016P5194" name="EC D2017 - mcq29" totalMark="2" version="2" markingType="0" markingState="0" userMark="1" markerUserMark="" userAttempted="1" viewingTime="104918" displayName="" type="1" quT="10" actualUserMark="2">
    </item>
    <item id="5016P5072" name="EC D2017 - mcq30" totalMark="2" version="3" markingType="0" markingState="0" userMark="1" markerUserMark="" userAttempted="1" viewingTime="98254" displayName="" type="1" quT="10" actualUserMark="2">
    </item>
  </section>
  <section id="2" passMark="0" passType="1" name="Section II" totalMark="20" userMark="0" userPercentage="0" passValue="1" totalTimeSpent="1775806" itemsToMark="0">
    <item id="5016P5073" name="EC D2017 - Case Study" totalMark="0" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="1775806" displayName="" type="3" quT="0" surpassGroupID="1" surpassGroupName="Group 1" actualUserMark="0">
    </item>
    <item id="5016P5074" name="EC D2017 - Case Study Questions" totalMark="20" version="4" markingType="1" markingState="0" userMark="0" markerUserMark="" userAttempted="1" viewingTime="0" displayName="" type="1" quT="11" surpassGroupID="1" surpassGroupName="Group 1" actualUserMark="0">
    </item>
    <item id="5016P5084" name="EC D2017 - Case Study Table" totalMark="0" version="6" markingType="1" markingState="0" userMark="0" markerUserMark="" userAttempted="1" viewingTime="0" displayName="" type="1" quT="20" surpassGroupID="1" surpassGroupName="Group 1" actualUserMark="0">
    </item>
  </section>
  <section id="3" passMark="0" passType="1" name="Section III" totalMark="80" userMark="0" userPercentage="0" passValue="1" totalTimeSpent="4222752" itemsToMark="0">
    <item id="5016P5075" name="EC D2017 - Essay 1" totalMark="40" version="3" markingType="1" markingState="0" userMark="0" markerUserMark="" userAttempted="1" viewingTime="2817583" displayName="" type="1" quT="11" surpassGroupID="3" surpassGroupName="Group 2" actualUserMark="0">
    </item>
    <item id="5016P5615" name="EC D2017 - Essay 1 Equation" totalMark="0" version="4" markingType="1" markingState="0" userMark="0" markerUserMark="" userAttempted="1" viewingTime="0" displayName="" type="1" quT="20" surpassGroupID="3" surpassGroupName="Group 2" actualUserMark="0">
    </item>
    <item id="5016P5076" name="EC D2017 - Essay 2" totalMark="40" version="2" markingType="1" markingState="0" userMark="0" markerUserMark="" userAttempted="1" viewingTime="1405169" displayName="" type="1" quT="11" surpassGroupID="4" surpassGroupName="Group 3" actualUserMark="0">
    </item>
    <item id="5016P5639" name="EC D2017 - Essay 2 Equation" totalMark="0" version="3" markingType="1" markingState="0" userMark="0" markerUserMark="" userAttempted="1" viewingTime="0" displayName="" type="1" quT="20" surpassGroupID="4" surpassGroupName="Group 3" actualUserMark="0">
    </item>
  </section>
</exam>'

--userMark
--totalMark
SELECT c.d.value('../@id','tinyint') Section
	, c.d.value('@id','nvarchar(20)') ItemID
	, c.d.value('@name','nvarchar(200)') ItemName
	, c.d.value('@userMark','tinyint') * c.d.value('@totalMark','tinyint') [Mark]
	, CASE c.d.value('@markingType','bit') 
		WHEN 0 THEN 'ComputerMarked'
		ELSE 'HumanMarked'
	END AS MarkingType
FROM (VALUES (@myXML)) a(b)
CROSS APPLY a.b.nodes('exam/section/item') c(d)
WHERE c.d.value('@totalMark','tinyint') > 0
