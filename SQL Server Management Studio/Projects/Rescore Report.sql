
DECLARE @ExpandVersions int

IF @CPID = '' SET @CPID = NULL
IF @CPID IS NOT NULL SET @ExpandVersions = 1 ELSE SET @ExpandVersions = 0

IF OBJECT_ID('tempdb..#ES') IS NOT NULL DROP TABLE #ES
CREATE TABLE #ES(
	N int
	,ExamSessionKey int
	,CentreKey int
	,QualificationKey int
	,ExamKey int
	,ExamVersionKey int
	,UserMarks float
	,TotalMarks float
	,Percentage float
	,CompletionDateTime datetime
	,Pass bit
	,TimeTaken int
	,Duration float
	,ExamVersionVersion int
	,isRescored bit
)
;WITH Preview AS (
	SELECT FESA.*
	FROM 
		FactExamSessionAudits FESA
		JOIN RescoringHistory REH ON FESA.RescoringHistoryId = REH.Id
		JOIN RescoringEvents RE ON REH.RescoringEventId = RE.Id
	WHERE 
		REH.RescoringEventId = @RescoringEventId
		AND FESA.IsTemp = 1
)
INSERT INTO #ES
SELECT 
	ROW_NUMBER() OVER (ORDER BY ISNULL(P.UserMarks, ES.UserMarks) ASC, ES.ExamSessionKey ASC) N
	,ES.ExamSessionKey
	,ES.CentreKey
	,ES.QualificationKey
	,ES.ExamKey
	,ES.ExamVersionKey
	,ISNULL(P.UserMarks, ES.UserMarks)
	,ISNULL(P.TotalMarksAvailable, ES.TotalMarksAvailable)
	,CASE WHEN ISNULL(P.TotalMarksAvailable, ES.TotalMarksAvailable) >0 THEN ISNULL(P.UserMarks, ES.UserMarks)/ISNULL(P.TotalMarksAvailable, ES.TotalMarksAvailable) ELSE ISNULL(P.UserMarks, ES.UserMarks) END
	,T.FullDateAlternateKey + ES.CompletionTime  CompletionDateTime
	,ISNULL(P.Pass,ES.Pass)
	,TimeTaken
	,ES.Duration
	,ES.ExamVersionVersion
	,CASE WHEN EXISTS (SELECT * FROM FactExamSessionAudits FESA WHERE FESA.ExamSessionKey = ES.ExamSessionKey AND FESA.IsTemp = 0) THEN 1 ELSE 0 END isRescored
FROM	 
	dbo.FactExamSessions AS ES
	INNER JOIN dbo.DimTime AS T
		 ON ES.CompletionDateKey = T.TimeKey
	LEFT JOIN Preview P ON P.ExamSessionKey = ES.ExamSessionKey
WHERE
	(T.FullDateAlternateKey + ES.CompletionTime) BETWEEN @dateStartRange AND @dateEndRange
	AND ES.FinalExamState <> 10
	AND ES.ExcludeFromReporting <> 1
	--AND ES.CentreKey IN (SELECT Value FROM dbo.fn_ParamsToList(@centres,0))
	AND ES.QualificationKey IN (SELECT Value FROM dbo.fn_ParamsToList(@subjects,0))
	AND ES.ExamVersionKey = @examVersion
OPTION (MAXRECURSION 0)


IF OBJECT_ID('tempdb..#Items') IS NOT NULL DROP TABLE #Items
CREATE TABLE #Items(SessionCount int, Inx int, ExamSessionKey int, ExamVersionKey int, ExamVersionVersion int, CPID nvarchar(50), CPVersion int, OriginalVersion int, UserMarks float, MarkerMark float, FQRMark float, FQRAMark float, QuestionMarks float, QuestionTotalMarks float, OriginalTotalMarks float, QuestionName nvarchar(100), QuestionTypeKey int, CAQuestionTypeKey int, ViewingTime int, Attempted float, TestOrder int, CAId int, CompletionDateTime datetime, ModifiedBy int, Percentage float, IsDichotomous bit, Scored int, Source int, MarkingTypeKey int, isRescored bit, PRIMARY KEY CLUSTERED (ExamSessionKey, CPID))

;WITH 
--MarkerResponses AS (
--	SELECT 
--		MR.*
--		,ROW_NUMBER() OVER(PARTITION BY MR.ExamSessionKey, MR.CPID ORDER BY SEQNO DESC) N
--	FROM #ES ES
--		JOIN dbo.FactMarkerResponse MR ON MR.ExamSessionKey = ES.ExamSessionKey
--),
items AS (
	SELECT	
		NULL SessionCount
		,NULL Inx --NTILE(3) OVER(PARTITION BY ES.ExamVersionKey, QR.CPID, Q.CPVersion ORDER BY UserMarks DESC, QR.ExamSessionKey ASC) Inx
		,ES.ExamSessionKey
		,ES.ExamVersionKey
		,ES.ExamVersionVersion
		,QR.CPID
		,QR.CPVersion CPVersion --IF @ExpandVersions=0 it will be overwritten below
		,QR.CPVersion OriginalVersion
		,ES.UserMarks
		,(SELECT TOP 1 AssignedMark FROM dbo.FactMarkerResponse MR WHERE MR.ExamSessionKey = QR.ExamSessionKey AND MR.CPID = QR.CPID ORDER BY SEQNO DESC) AssM
		--,MR.assignedMark assM --assuming the number of human-marked items is small compared to computer-marked, the seeks to FMR will be faster than joins with CTE
		,QR.Mark FQRMark
		,FQRA.Mark FQRAMark
		,NULL AssM2
		,NULL QuestionTotalMarks
		,NULL OriginalTotalMarks
		,NULL QuestionName
		,NULL QuestionTypeKey
		,NULL CAQuestionTypeKey
		,NULL CAId
		,QR.ViewingTime
		,QR.Attempted
		,QR.ItemPresentationOrder
		,ES.CompletionDateTime
		,NULL ModifiedByUserKey
		,Percentage
		,NULL IsDichotomous
		,ISNULL(FQRA.Scored, QR.Scored) Scored
		,isRescored
	FROM	
	#ES ES
	INNER JOIN	FactQuestionResponses QR
		 ON ES.ExamSessionKey = QR.ExamSessionKey
		 AND (@CPID IS NULL OR QR.CPID = @CPID)
	LEFT JOIN FactQuestionResponseAudits FQRA
		ON FQRA.ExamSessionKey = QR.ExamSessionKey
		AND FQRA.CPID = QR.CPID
		AND FQRA.IsTemp = 1
	--LEFT JOIN MarkerResponses MR
	--	ON MR.ExamSessionKey = QR.ExamSessionKey
	--	AND MR.CPID = QR.CPID
	--	AND MR.N = 1
)
INSERT INTO #Items 
SELECT 
	SessionCount
	,Inx
	,ExamSessionKey
	,ExamVersionKey
	,ExamVersionVersion
	,CPID
	,CPVersion
	,OriginalVersion
	,UserMarks
	,AssM
	,FQRMark
	,FQRAMark
	,NULL QuestionMarks
	,QuestionTotalMarks
	,OriginalTotalMarks
	,QuestionName
	,QuestionTypeKey
	,CAQuestionTypeKey
	,ViewingTime
	,Attempted	
	,ItemPresentationOrder
	,CAId
	,CompletionDateTime
	,ModifiedByUserKey
	,Percentage
	,IsDichotomous
	,Scored
	,NULL Source
	,NULL MarkingTypeKey
	,isRescored
FROM items


IF @ExpandVersions = 0 BEGIN
	;WITH Versions AS (
		SELECT CPID, MAX(CPVersion) MaxVersion
		FROM #Items
		GROUP BY CPID
	)
	UPDATE #Items 
	SET 
		CPVersion = V.MaxVersion
	FROM 
		#Items I 
		JOIN Versions V ON I.CPID = V.CPID
END

;WITH items2 AS (
	SELECT 
		I.ExamSessionKey
		,I.CPID
		,ROW_NUMBER() OVER(PARTITION BY ExamVersionKey, I.CPID, I.CPVersion ORDER BY Percentage DESC, ExamSessionKey ASC) Inx
		,Q.TotalMark * I.FQRMark FQRMark_abs
		,Q.TotalMark * I.FQRAMark FQRAMark_abs
		,Q.TotalMark QuestionTotalMarks
		,OQ.TotalMark OriginalTotalMarks
		,Q.QuestionName
		,Q.QuestionTypeKey
		,Q.CAQuestionTypeKey
		,CASE WHEN Q.CAQuestionTypeKey IS NOT NULL THEN Q.ExternalId ELSE NULL END CAId
		,Q.ModifiedByUserKey
		,CASE 
			WHEN 
				Q.CAQuestionTypeKey IN (2,4,5,7,9,18)	--MCQ, Either/Or, Numerical Entry, Short Answer, Select from a list, Hotspot
				AND Q.MarkingTypeKey=0				--ComputerMarked
				AND I.MarkerMark IS NULL			--Hadn`t been overriden
				THEN 1 
			ELSE 0 
		 END IsDichotomous
		 ,Q.Source
		 ,Q.MarkingTypeKey
	FROM 
		#Items I
		JOIN DimQuestions Q 
			ON I.CPID = Q.CPID
			AND I.CPVersion = Q.CPVersion
		JOIN DimQuestions OQ 
			ON I.CPID = OQ.CPID
			AND I.OriginalVersion = OQ.CPVersion
)
UPDATE I
SET
	 Inx = I2.Inx
	,QuestionMarks = 
		CASE WHEN isRescored = 1 
			THEN ISNULL(I2.FQRAMark_abs, I2.FQRMark_abs)
			ELSE COALESCE(I2.FQRAMark_abs, I.MarkerMark, I2.FQRMark_abs)
		END
	,QuestionTotalMarks = I2.QuestionTotalMarks
	,OriginalTotalMarks = I2.OriginalTotalMarks
	,QuestionName = I2.QuestionName
	,QuestionTypeKey = I2.QuestionTypeKey
	,CAQuestionTypeKey = I2.CAQuestionTypeKey
	,CAId = I2.CAId
	,ModifiedBy = I2.ModifiedByUserKey
	,IsDichotomous = I2.IsDichotomous
	,Source = I2.Source
	,MarkingTypeKey = I2.MarkingTypeKey
FROM 
	#Items I 
	JOIN items2 I2 
		ON I.CPID = I2.CPID
		AND I.ExamSessionKey = I2.ExamSessionKey

DELETE FROM #Items WHERE QuestionTotalMarks=0

;WITH SessionCounts AS (
	SELECT 
		CPID
		, CPVersion
		, COUNT(DISTINCT ExamSessionKey) SessionCount 
	FROM #Items
	GROUP BY CPID, CPVersion
)
UPDATE #Items 
SET 
	SessionCount = SC.SessionCount
FROM 
	#Items I 
	JOIN SessionCounts SC ON 
		I.CPID = SC.CPID
		AND I.CPVersion = SC.CPVersion

DECLARE @ESCount int,  @SD float, @SDb float, @passRate float, @LastSat datetime, @MeanTime float, @Duration bigint, @MeanScore float, @MaxScore float, @QuestionCount int, @MeanAttempted float

IF @ExpandVersions = 0 --to not show exam-level data based on particular item stats
BEGIN
	SELECT 
		@SD = STDEV(UserMarks)
		,@SDb = STDEVP(UserMarks)
		,@EScount = COUNT(ExamSessionKey)
		,@passRate = CAST(ROUND((SUM(CAST(pass AS INT)* 1.)/COUNT(examVersionKey)*1.) * 100,0) AS INT)
		,@LastSat = MAX(CompletionDateTime)
		,@MeanTime = AVG(CAST(TimeTaken AS BIGINT))
		,@Duration=AVG(CAST(ROUND(Duration*60*1000,0) AS BIGINT))
		,@MeanScore = AVG(UserMarks)
		,@MaxScore = AVG(TotalMarks)
	FROM #ES

	SELECT 
		@QuestionCount = COUNT(DISTINCT CPID)
		,@MeanAttempted = SUM(Attempted)/COUNT(CPID)
	FROM #Items
END

;WITH 
/*
ES_indices AS (
	SELECT CEILING(MAX(N)/2.+0.5) ceil, FLOOR(MAX(N)/2.+0.5) flr
	FROM #ES ES
), 
medians AS (
	SELECT AVG(UserMarks) median
	FROM #ES ES
	JOIN ES_indices ESI ON (ES.N = ESI.ceil OR ES.N = ESI.flr)
),
modes_raw AS (
	SELECT 
		ROW_NUMBER() OVER(ORDER BY COUNT(ExamSessionKey) DESC, UserMarks) N
		,ROW_NUMBER() OVER(ORDER BY COUNT(ExamSessionKey) DESC, UserMarks DESC) N2
		,UserMarks mode
		,COUNT(ExamSessionKey) cnt 
	FROM #ES ES
	GROUP BY UserMarks
)
,Vars AS (
	SELECT 
		CPID
		,CASE WHEN QuestionMarks > 0 THEN 1 ELSE 0 END Mp
		,UserMarks
	FROM #Items
), 
Numbers AS (
	SELECT 
		V.CPID
		,CAST(SUM(V.Mp*V.UserMarks) AS numeric) M1_raw
		,CAST(SUM((1-V.Mp)*V.UserMarks) AS numeric) M0_raw
		,CAST(SUM(V.Mp) AS numeric) n1
		,CAST(SUM(1-V.Mp) AS numeric) n0
		,CAST(COUNT(V.Mp) AS numeric) n
		,@SDb SDb
	FROM Vars V
	GROUP BY 
		V.CPID
)*/
ComponentVariance AS (
	SELECT 
		CPID
		,VARP(QuestionMarks) Variance
	FROM #Items
	WHERE Scored=1
	GROUP BY CPID
),
Cronbach AS (
	SELECT 
		CASE 
			WHEN 
				COUNT(C.CPID) = 1 
				OR SUM(C.Variance)=0
				OR @SDb=0
			THEN NULL 
			ELSE COUNT(C.CPID)/(COUNT(C.CPID)-1.) * (1-SUM(C.Variance)/(@SDb*@SDb)) 
		 END CronbachA
	FROM ComponentVariance C 
),
FV_Top AS (
	SELECT 
		CPID
		,CPVersion
		,AVG(QuestionMarks/QuestionTotalMarks) FV
	FROM 
		#Items 
	WHERE 
		Inx<=ROUND(SessionCount/3., 0)
	GROUP BY 
		CPID
		,CPVersion
), FV_Bottom AS (
	SELECT 
		CPID
		,CPVersion
		,AVG(QuestionMarks/QuestionTotalMarks) FV
	FROM 
		#Items 
	WHERE 
		Inx>=SessionCount-ROUND(SessionCount/3., 0)+1
	GROUP BY 
		CPID
		,CPVersion
), PearsonsR_raw AS (
	SELECT 
		CPID
		,CPVersion
		,(UserMarks - AVG(UserMarks) OVER(PARTITION BY CPID,CPVersion)) dx
		,(QuestionMarks - AVG(QuestionMarks) OVER(PARTITION BY CPID,CPVersion)) dy
	FROM #Items
), PearsonsR AS (
	SELECT 
		CPID
		,CPVersion
		,SUM(dx*dy)/SQRT(SUM(dx*dx)*SUM(dy*dy)) R
	FROM PearsonsR_raw
	GROUP BY CPID,CPVersion
	HAVING SQRT(SUM(dx*dx)*SUM(dy*dy))<>0
) 

SELECT 
	1 AS [Tag]
	,NULL AS [Parent]
	,NULL AS [Report!1] -- root element
	,NULL AS [ExamVersion!3]
	,NULL AS [ExamVersion!3!ExamKey]
	,NULL AS [ExamVersion!3!ExamName]
	,NULL AS [ExamVersion!3!ExamReference]
	,NULL AS [ExamVersion!3!ExamVersionKey]
	,NULL AS [ExamVersion!3!ExamVersionName]
	,NULL AS [ExamVersion!3!ExamVersionReference]
	,NULL AS [ExamVersion!3!LastSat]
	,NULL AS [ExamVersion!3!PassRate]
	,NULL AS [ExamVersion!3!MeanTime]
	,NULL AS [ExamVersion!3!Duration]
	,NULL AS [ExamVersion!3!MeanScore]
	,NULL AS [ExamVersion!3!MaxScore]
	,NULL AS [ExamVersion!3!MeanAttempted]
    ,NULL AS [ExamVersion!3!CronbachA]
    ,NULL AS [ExamVersion!3!TestsDelivered]
    ,NULL AS [ExamVersion!3!ItemsDelivered]
	,NULL AS [ExamVersionVersions!4]
	,NULL AS [ExamVersionVersion!5!Version]
	--,NULL AS [ExamVersionVersion!5!TotalMark]
	,NULL AS [ExamVersionVersion!5!ScaleScoreId]
	,NULL AS [ExamVersionVersion!5!GradeBoundaryId]
	,NULL AS [ExamVersionVersion!5!LastSat]
	,NULL AS [ExamVersionVersion!5!PassRate]
	,NULL AS [ExamVersionVersion!5!MeanTime]
	,NULL AS [ExamVersionVersion!5!Duration]
	,NULL AS [ExamVersionVersion!5!MeanScore]
	,NULL AS [ExamVersionVersion!5!MaxScore]
	,NULL AS [ExamVersionVersion!5!MeanAttempted]
    ,NULL AS [ExamVersionVersion!5!TestsDelivered]
    ,NULL AS [ExamVersionVersion!5!ItemsDelivered]
	,NULL AS [Items!6]
	,NULL AS [Item!7!CPID]
	,NULL AS [Item!7!CPVersion]
	,NULL AS [Item!7!TotalMark]
	,NULL AS [Items!9]
	,NULL AS [Item!10!CPID]
	,NULL AS [Item!10!CPVersion]
	,NULL AS [Item!10!VersionCount]
	,NULL AS [Item!10!CAID]
	,NULL AS [Item!10!Name]
	,NULL AS [Item!10!Type]
	,NULL AS [Item!10!QuestionTypeKey]
	,NULL AS [Item!10!AuthoringQuestionTypeKey]
	,NULL AS [Item!10!TotalMark]
	,NULL AS [Item!10!AverageMark]
	,NULL AS [Item!10!PercentUnAnswered]
	,NULL AS [Item!10!FacilityValue]
	,NULL AS [Item!10!DI]
	,NULL AS [Item!10!AttemptedCount]
	,NULL AS [Item!10!ViewedUnattemptedCount]
	,NULL AS [Item!10!NotViewedCount]
	,NULL AS [Item!10!AverageViewingTime]
	,NULL AS [Item!10!TestOrder]
	,NULL AS [Item!10!TestOrderIsFixed]
	,NULL AS [Item!10!LastModifierUserId]
	,NULL AS [Item!10!LastModifierFirstName]
	,NULL AS [Item!10!LastModifierLastName]
	,NULL AS [Item!10!PearsonsR]
	,NULL AS [Item!10!IsDichotomous]
	,NULL AS [Item!10!Scored]
	,NULL AS [Item!10!ScoredChanged]
	,NULL AS [Item!10!isContentAvailable]
	,NULL AS [Item!10!isHumanMarked]

UNION ALL

SELECT 
	3 AS [Tag]
	,1 AS [Parent]
	,NULL AS [Report!1] -- root element
	,NULL AS [ExamVersion!3]
	,DE.ExamKey AS [ExamVersion!3!ExamKey]
	,DE.ExamName AS [ExamVersion!3!ExamName]
	,DE.ExamReference AS [ExamVersion!3!ExamReference]
	,DEV.ExamVersionKey AS [ExamVersion!3!ExamVersionKey]
	,DEV.ExamVersionName AS [ExamVersion!3!ExamVersionName]
	,DEV.ExamVersionReference AS [ExamVersion!3!ExamVersionReference]
	,@LastSat AS [ExamVersion!3!LastSat]
	,@passRate AS [ExamVersion!3!PassRate]
	,@MeanTime AS [ExamVersion!3!MeanTime]
	,@Duration AS [ExamVersion!3!Duration]
	,@MeanScore AS [ExamVersion!3!MeanScore]
	,@MaxScore AS [ExamVersion!3!MaxScore]
	,@MeanAttempted AS [ExamVersion!3!MeanAttempted]
    ,(SELECT CronbachA FROM Cronbach) AS [ExamVersion!3!CronbachA]
    ,@ESCount AS [ExamVersion!3!TestsDelivered]
    ,@QuestionCount AS [ExamVersion!3!ItemsDelivered]
	,NULL AS [ExamVersionVersions!4]
	,NULL AS [ExamVersionVersion!5!Version]
	--,NULL AS [ExamVersionVersion!5!TotalMark]
	,NULL AS [ExamVersionVersion!5!ScaleScoreId]
	,NULL AS [ExamVersionVersion!5!GradeBoundaryId]
	,NULL AS [ExamVersionVersion!5!LastSat]
	,NULL AS [ExamVersionVersion!5!PassRate]
	,NULL AS [ExamVersionVersion!5!MeanTime]
	,NULL AS [ExamVersionVersion!5!Duration]
	,NULL AS [ExamVersionVersion!5!MeanScore]
	,NULL AS [ExamVersionVersion!5!MaxScore]
	,NULL AS [ExamVersionVersion!5!MeanAttempted]
    ,NULL AS [ExamVersionVersion!5!TestsDelivered]
    ,NULL AS [ExamVersionVersion!5!ItemsDelivered]
	,NULL AS [Items!6]
	,NULL AS [Item!7!CPID]
	,NULL AS [Item!7!CPVersion]
	,NULL AS [Item!7!TotalMark]
	,NULL AS [Items!9]
	,NULL AS [Item!10!CPID]
	,NULL AS [Item!10!CPVersion]
	,NULL AS [Item!10!VersionCount]
	,NULL AS [Item!10!CAID]
	,NULL AS [Item!10!Name]
	,NULL AS [Item!10!Type]
	,NULL AS [Item!10!QuestionTypeKey]
	,NULL AS [Item!10!AuthoringQuestionTypeKey]
	,NULL AS [Item!10!TotalMark]
	,NULL AS [Item!10!AverageMark]
	,NULL AS [Item!10!PercentUnAnswered]
	,NULL AS [Item!10!FacilityValue]
	,NULL AS [Item!10!DI]
	,NULL AS [Item!10!AttemptedCount]
	,NULL AS [Item!10!ViewedUnattemptedCount]
	,NULL AS [Item!10!NotViewedCount]
	,NULL AS [Item!10!AverageViewingTime]
	,NULL AS [Item!10!TestOrder]
	,NULL AS [Item!10!TestOrderIsFixed]
	,NULL AS [Item!10!LastModifierUserId]
	,NULL AS [Item!10!LastModifierFirstName]
	,NULL AS [Item!10!LastModifierLastName]
	,NULL AS [Item!10!PearsonsR]
	,NULL AS [Item!10!IsDichotomous]
	,NULL AS [Item!10!Scored]
	,NULL AS [Item!10!ScoredChanged]
	,NULL AS [Item!10!isContentAvailable]
	,NULL AS [Item!10!isHumanMarked]
FROM
	DimExamVersions DEV
	JOIN DimExams DE ON DEV.ExamKey = DE.ExamKey
WHERE 
	DEV.ExamVersionKey = @examVersion

UNION ALL

SELECT 
	4 AS [Tag]
	,3 AS [Parent]
	,NULL AS [Report!1] -- root element
	,NULL AS [ExamVersion!3]
	,NULL AS [ExamVersion!3!ExamKey]
	,NULL AS [ExamVersion!3!ExamName]
	,NULL AS [ExamVersion!3!ExamReference]
	,NULL AS [ExamVersion!3!ExamVersionKey]
	,NULL AS [ExamVersion!3!ExamVersionName]
	,NULL AS [ExamVersion!3!ExamVersionReference]
	,NULL AS [ExamVersion!3!LastSat]
	,NULL AS [ExamVersion!3!PassRate]
	,NULL AS [ExamVersion!3!MeanTime]
	,NULL AS [ExamVersion!3!Duration]
	,NULL AS [ExamVersion!3!MeanScore]
	,NULL AS [ExamVersion!3!MaxScore]
	,NULL AS [ExamVersion!3!MeanAttempted]
    ,NULL AS [ExamVersion!3!CronbachA]
    ,NULL AS [ExamVersion!3!TestsDelivered]
    ,NULL AS [ExamVersion!3!ItemsDelivered]
	,NULL AS [ExamVersionVersions!4]
	,0 AS [ExamVersionVersion!5!Version]
	--,NULL AS [ExamVersionVersion!5!TotalMark]
	,NULL AS [ExamVersionVersion!5!ScaleScoreId]
	,NULL AS [ExamVersionVersion!5!GradeBoundaryId]
	,NULL AS [ExamVersionVersion!5!LastSat]
	,NULL AS [ExamVersionVersion!5!PassRate]
	,NULL AS [ExamVersionVersion!5!MeanTime]
	,NULL AS [ExamVersionVersion!5!Duration]
	,NULL AS [ExamVersionVersion!5!MeanScore]
	,NULL AS [ExamVersionVersion!5!MaxScore]
	,NULL AS [ExamVersionVersion!5!MeanAttempted]
    ,NULL AS [ExamVersionVersion!5!TestsDelivered]
    ,NULL AS [ExamVersionVersion!5!ItemsDelivered]
	,NULL AS [Items!6]
	,NULL AS [Item!7!CPID]
	,NULL AS [Item!7!CPVersion]
	,NULL AS [Item!7!TotalMark]
	,NULL AS [Items!9] -- root element
	,NULL AS [Item!10!CPID]
	,NULL AS [Item!10!CPVersion]
	,NULL AS [Item!10!VersionCount]
	,NULL AS [Item!10!CAID]
	,NULL AS [Item!10!Name]
	,NULL AS [Item!10!Type]
	,NULL AS [Item!10!QuestionTypeKey]
	,NULL AS [Item!10!AuthoringQuestionTypeKey]
	,NULL AS [Item!10!TotalMark]
	,NULL AS [Item!10!AverageMark]
	,NULL AS [Item!10!PercentUnAnswered]
	,NULL AS [Item!10!FacilityValue]
	,NULL AS [Item!10!DI]
	,NULL AS [Item!10!AttemptedCount]
	,NULL AS [Item!10!ViewedUnattemptedCount]
	,NULL AS [Item!10!NotViewedCount]
	,NULL AS [Item!10!AverageViewingTime]
	,NULL AS [Item!10!TestOrder]
	,NULL AS [Item!10!TestOrderIsFixed]
	,NULL AS [Item!10!LastModifierUserId]
	,NULL AS [Item!10!LastModifierFirstName]
	,NULL AS [Item!10!LastModifierLastName]
	,NULL AS [Item!10!PearsonsR]
	,NULL AS [Item!10!IsDichotomous]
	,NULL AS [Item!10!Scored]
	,NULL AS [Item!10!ScoredChanged]
	,NULL AS [Item!10!isContentAvailable]
	,NULL AS [Item!10!isHumanMarked]

UNION ALL

SELECT
	5 AS [Tag]
	,4 AS [Parent]
	,NULL AS [Report!1] -- root element
	,NULL AS [ExamVersion!3]
	,NULL AS [ExamVersion!3!ExamKey]
	,NULL AS [ExamVersion!3!ExamName]
	,NULL AS [ExamVersion!3!ExamReference]
	,NULL AS [ExamVersion!3!ExamVersionKey]
	,NULL AS [ExamVersion!3!ExamVersionName]
	,NULL AS [ExamVersion!3!ExamVersionReference]
	,NULL AS [ExamVersion!3!LastSat]
	,NULL AS [ExamVersion!3!PassRate]
	,NULL AS [ExamVersion!3!MeanTime]
	,NULL AS [ExamVersion!3!Duration]
	,NULL AS [ExamVersion!3!MeanScore]
	,NULL AS [ExamVersion!3!MaxScore]
	,NULL AS [ExamVersion!3!MeanAttempted]
    ,NULL AS [ExamVersion!3!CronbachA]
    ,NULL AS [ExamVersion!3!TestsDelivered]
    ,NULL AS [ExamVersion!3!ItemsDelivered]
	,NULL AS [ExamVersionVersions!4]
	,EVV.ExamVersionVersion AS [ExamVersionVersion!5!Version]
	--,DEVV.TotalMark AS [ExamVersionVersion!5!TotalMark]
	,COALESCE(RESS.TempScaleScoreId, RESS.ScaleScoreId, DEVV.ScaleScoreId) AS [ExamVersionVersion!5!ScaleScoreId]
	,COALESCE(RESS.TempGradeBoundaryId, RESS.GradeBoundaryId, DEVV.GradeBoundaryId) AS [ExamVersionVersion!5!GradeBoundaryId]
	,LastSat AS [ExamVersionVersion!5!LastSat]
	,PassRate AS [ExamVersionVersion!5!PassRate]
	,MeanTime AS [ExamVersionVersion!5!MeanTime]
	,Duration AS [ExamVersionVersion!5!Duration]
	,MeanScore AS [ExamVersionVersion!5!MeanScore]
	,MaxScore AS [ExamVersionVersion!5!MaxScore]
	,MeanAttempted AS [ExamVersionVersion!5!MeanAttempted]
	,TestsDelivered AS [ExamVersionVersion!5!TestsDelivered]
	,ItemsDelivered AS [ExamVersionVersion!5!ItemsDelivered]
	,NULL AS [Items!6]
	,NULL AS [Item!7!CPID]
	,NULL AS [Item!7!CPVersion]
	,NULL AS [Item!7!TotalMark]
	,NULL AS [Items!9] -- root element
	,NULL AS [Item!10!CPID]
	,NULL AS [Item!10!CPVersion]
	,NULL AS [Item!10!VersionCount]
	,NULL AS [Item!10!CAID]
	,NULL AS [Item!10!Name]
	,NULL AS [Item!10!Type]
	,NULL AS [Item!10!QuestionTypeKey]
	,NULL AS [Item!10!AuthoringQuestionTypeKey]
	,NULL AS [Item!10!TotalMark]
	,NULL AS [Item!10!AverageMark]
	,NULL AS [Item!10!PercentUnAnswered]
	,NULL AS [Item!10!FacilityValue]
	,NULL AS [Item!10!DI]
	,NULL AS [Item!10!AttemptedCount]
	,NULL AS [Item!10!ViewedUnattemptedCount]
	,NULL AS [Item!10!NotViewedCount]
	,NULL AS [Item!10!AverageViewingTime]
	,NULL AS [Item!10!TestOrder]
	,NULL AS [Item!10!TestOrderIsFixed]
	,NULL AS [Item!10!LastModifierUserId]
	,NULL AS [Item!10!LastModifierFirstName]
	,NULL AS [Item!10!LastModifierLastName]
	,NULL AS [Item!10!PearsonsR]
	,NULL AS [Item!10!IsDichotomous]
	,NULL AS [Item!10!Scored]
	,NULL AS [Item!10!ScoredChanged]
	,NULL AS [Item!10!isContentAvailable]
	,NULL AS [Item!10!isHumanMarked]
FROM 
	(
		SELECT 
			ExamVersionVersion 
			,MAX(CompletionDateTime) AS LastSat
			,AVG(CAST(Pass AS float))*100. AS PassRate
			,AVG(TimeTaken) AS MeanTime
			,AVG(Duration) AS Duration
			,AVG(UserMarks) AS MeanScore
			,AVG(TotalMarks) AS MaxScore
			,COUNT(ExamSessionKey) AS TestsDelivered
		FROM 
			#ES ES
		GROUP BY 
			ExamVersionVersion
	) EVV
	LEFT JOIN 
	(
		SELECT
			ExamVersionVersion
			,COUNT(DISTINCT CPID) ItemsDelivered
			,AVG(Attempted) MeanAttempted
		FROM 
			#Items
		GROUP BY 
			ExamVersionVersion
	) I ON I.ExamVersionVersion = EVV.ExamVersionVersion
	JOIN DimExamVersionVersions DEVV 
		ON EVV.ExamVersionVersion = DEVV.ExamVersionVersion
		AND DEVV.ExamVersionKey  = @ExamVersion
	LEFT JOIN RescoringEventToScaleScores RESS
		ON RESS.RescoringEventId = @RescoringEventId
		AND RESS.ExamVersionVersion = EVV.ExamVersionVersion

UNION ALL

SELECT
	6 AS [Tag]
	,5 AS [Parent]
	,NULL AS [Report!1] -- root element
	,NULL AS [ExamVersion!3]
	,NULL AS [ExamVersion!3!ExamKey]
	,NULL AS [ExamVersion!3!ExamName]
	,NULL AS [ExamVersion!3!ExamReference]
	,NULL AS [ExamVersion!3!ExamVersionKey]
	,NULL AS [ExamVersion!3!ExamVersionName]
	,NULL AS [ExamVersion!3!ExamVersionReference]
	,NULL AS [ExamVersion!3!LastSat]
	,NULL AS [ExamVersion!3!PassRate]
	,NULL AS [ExamVersion!3!MeanTime]
	,NULL AS [ExamVersion!3!Duration]
	,NULL AS [ExamVersion!3!MeanScore]
	,NULL AS [ExamVersion!3!MaxScore]
	,NULL AS [ExamVersion!3!MeanAttempted]
    ,NULL AS [ExamVersion!3!CronbachA]
    ,NULL AS [ExamVersion!3!TestsDelivered]
    ,NULL AS [ExamVersion!3!ItemsDelivered]
	,NULL AS [ExamVersionVersions!4]
	,DEVV.ExamVersionVersion AS [ExamVersionVersion!5!Version]
	--,NULL AS [ExamVersionVersion!5!TotalMark]
	,NULL AS [ExamVersionVersion!5!ScaleScoreId]
	,NULL AS [ExamVersionVersion!5!GradeBoundaryId]
	,NULL AS [ExamVersionVersion!5!LastSat]
	,NULL AS [ExamVersionVersion!5!PassRate]
	,NULL AS [ExamVersionVersion!5!MeanTime]
	,NULL AS [ExamVersionVersion!5!Duration]
	,NULL AS [ExamVersionVersion!5!MeanScore]
	,NULL AS [ExamVersionVersion!5!MaxScore]
	,NULL AS [ExamVersionVersion!5!MeanAttempted]
    ,NULL AS [ExamVersionVersion!5!TestsDelivered]
    ,NULL AS [ExamVersionVersion!5!ItemsDelivered]
	,NULL AS [Items!6]
	,NULL AS [Item!7!CPID]
	,NULL AS [Item!7!CPVersion]
	,NULL AS [Item!7!TotalMark]
	,NULL AS [Items!9] -- root element
	,NULL AS [Item!10!CPID]
	,NULL AS [Item!10!CPVersion]
	,NULL AS [Item!10!VersionCount]
	,NULL AS [Item!10!CAID]
	,NULL AS [Item!10!Name]
	,NULL AS [Item!10!Type]
	,NULL AS [Item!10!QuestionTypeKey]
	,NULL AS [Item!10!AuthoringQuestionTypeKey]
	,NULL AS [Item!10!TotalMark]
	,NULL AS [Item!10!AverageMark]
	,NULL AS [Item!10!PercentUnAnswered]
	,NULL AS [Item!10!FacilityValue]
	,NULL AS [Item!10!DI]
	,NULL AS [Item!10!AttemptedCount]
	,NULL AS [Item!10!ViewedUnattemptedCount]
	,NULL AS [Item!10!NotViewedCount]
	,NULL AS [Item!10!AverageViewingTime]
	,NULL AS [Item!10!TestOrder]
	,NULL AS [Item!10!TestOrderIsFixed]
	,NULL AS [Item!10!LastModifierUserId]
	,NULL AS [Item!10!LastModifierFirstName]
	,NULL AS [Item!10!LastModifierLastName]
	,NULL AS [Item!10!PearsonsR]
	,NULL AS [Item!10!IsDichotomous]
	,NULL AS [Item!10!Scored]
	,NULL AS [Item!10!ScoredChanged]
	,NULL AS [Item!10!isContentAvailable]
	,NULL AS [Item!10!isHumanMarked]
FROM 
	DimExamVersionVersions DEVV
WHERE 
	EXISTS (
		SELECT * FROM #ES ES 
		WHERE ES.ExamVersionVersion = DEVV.ExamVersionVersion
			AND DEVV.ExamVersionKey  = @ExamVersion
	)

UNION ALL

SELECT 
	7 AS [Tag]
	,6 AS [Parent]
	,NULL AS [Report!1] -- root element
	,NULL AS [ExamVersion!3]
	,NULL AS [ExamVersion!3!ExamKey]
	,NULL AS [ExamVersion!3!ExamName]
	,NULL AS [ExamVersion!3!ExamReference]
	,NULL AS [ExamVersion!3!ExamVersionKey]
	,NULL AS [ExamVersion!3!ExamVersionName]
	,NULL AS [ExamVersion!3!ExamVersionReference]
	,NULL AS [ExamVersion!3!LastSat]
	,NULL AS [ExamVersion!3!PassRate]
	,NULL AS [ExamVersion!3!MeanTime]
	,NULL AS [ExamVersion!3!Duration]
	,NULL AS [ExamVersion!3!MeanScore]
	,NULL AS [ExamVersion!3!MaxScore]
	,NULL AS [ExamVersion!3!MeanAttempted]
    ,NULL AS [ExamVersion!3!CronbachA]
    ,NULL AS [ExamVersion!3!TestsDelivered]
    ,NULL AS [ExamVersion!3!ItemsDelivered]
	,NULL AS [ExamVersionVersions!4]
	,ExamVersionVersion AS [ExamVersionVersion!5!Version]
	--,NULL AS [ExamVersionVersion!5!TotalMark]
	,NULL AS [ExamVersionVersion!5!ScaleScoreId]
	,NULL AS [ExamVersionVersion!5!GradeBoundaryId]
	,NULL AS [ExamVersionVersion!5!LastSat]
	,NULL AS [ExamVersionVersion!5!PassRate]
	,NULL AS [ExamVersionVersion!5!MeanTime]
	,NULL AS [ExamVersionVersion!5!Duration]
	,NULL AS [ExamVersionVersion!5!MeanScore]
	,NULL AS [ExamVersionVersion!5!MaxScore]
	,NULL AS [ExamVersionVersion!5!MeanAttempted]
    ,NULL AS [ExamVersionVersion!5!TestsDelivered]
    ,NULL AS [ExamVersionVersion!5!ItemsDelivered]
	,NULL AS [Items!6]
	,CPID AS [Item!7!CPID]
	,OriginalVersion AS [Item!7!CPVersion]
	,AVG(OriginalTotalMarks) AS [Item!7!TotalMark]
	,NULL AS [Items!9] -- root element
	,NULL AS [Item!10!CPID]
	,NULL AS [Item!10!CPVersion]
	,NULL AS [Item!10!VersionCount]
	,NULL AS [Item!10!CAID]
	,NULL AS [Item!10!Name]
	,NULL AS [Item!10!Type]
	,NULL AS [Item!10!QuestionTypeKey]
	,NULL AS [Item!10!AuthoringQuestionTypeKey]
	,NULL AS [Item!10!TotalMark]
	,NULL AS [Item!10!AverageMark]
	,NULL AS [Item!10!PercentUnAnswered]
	,NULL AS [Item!10!FacilityValue]
	,NULL AS [Item!10!DI]
	,NULL AS [Item!10!AttemptedCount]
	,NULL AS [Item!10!ViewedUnattemptedCount]
	,NULL AS [Item!10!NotViewedCount]
	,NULL AS [Item!10!AverageViewingTime]
	,NULL AS [Item!10!TestOrder]
	,NULL AS [Item!10!TestOrderIsFixed]
	,NULL AS [Item!10!LastModifierUserId]
	,NULL AS [Item!10!LastModifierFirstName]
	,NULL AS [Item!10!LastModifierLastName]
	,NULL AS [Item!10!PearsonsR]
	,NULL AS [Item!10!IsDichotomous]
	,NULL AS [Item!10!Scored]
	,NULL AS [Item!10!ScoredChanged]
	,NULL AS [Item!10!isContentAvailable]
	,NULL AS [Item!10!isHumanMarked]
FROM 
	#Items
WHERE 
	ExamVersionVersion IS NOT NULL
GROUP BY 
	ExamVersionVersion
	,CPID
	,OriginalVersion

UNION ALL

SELECT 
	9 AS [Tag]
	,3 AS [Parent]
	,NULL AS [Report!1] -- root element
	,NULL AS [ExamVersion!3]
	,NULL AS [ExamVersion!3!ExamKey]
	,NULL AS [ExamVersion!3!ExamName]
	,NULL AS [ExamVersion!3!ExamReference]
	,NULL AS [ExamVersion!3!ExamVersionKey]
	,NULL AS [ExamVersion!3!ExamVersionName]
	,NULL AS [ExamVersion!3!ExamVersionReference]
	,NULL AS [ExamVersion!3!LastSat]
	,NULL AS [ExamVersion!3!PassRate]
	,NULL AS [ExamVersion!3!MeanTime]
	,NULL AS [ExamVersion!3!Duration]
	,NULL AS [ExamVersion!3!MeanScore]
	,NULL AS [ExamVersion!3!MaxScore]
	,NULL AS [ExamVersion!3!MeanAttempted]
    ,NULL AS [ExamVersion!3!CronbachA]
    ,NULL AS [ExamVersion!3!TestsDelivered]
    ,NULL AS [ExamVersion!3!ItemsDelivered]
	,NULL AS [ExamVersionVersions!4]
	,NULL AS [ExamVersionVersion!5!Version]
	--,NULL AS [ExamVersionVersion!5!TotalMark]
	,NULL AS [ExamVersionVersion!5!ScaleScoreId]
	,NULL AS [ExamVersionVersion!5!GradeBoundaryId]
	,NULL AS [ExamVersionVersion!5!LastSat]
	,NULL AS [ExamVersionVersion!5!PassRate]
	,NULL AS [ExamVersionVersion!5!MeanTime]
	,NULL AS [ExamVersionVersion!5!Duration]
	,NULL AS [ExamVersionVersion!5!MeanScore]
	,NULL AS [ExamVersionVersion!5!MaxScore]
	,NULL AS [ExamVersionVersion!5!MeanAttempted]
    ,NULL AS [ExamVersionVersion!5!TestsDelivered]
    ,NULL AS [ExamVersionVersion!5!ItemsDelivered]
	,NULL AS [Items!6]
	,NULL AS [Item!7!CPID]
	,NULL AS [Item!7!CPVersion]
	,NULL AS [Item!7!TotalMark]
	,NULL AS [Items!9] -- root element
	,NULL AS [Item!10!CPID]
	,NULL AS [Item!10!CPVersion]
	,NULL AS [Item!10!VersionCount]
	,NULL AS [Item!10!CAID]
	,NULL AS [Item!10!Name]
	,NULL AS [Item!10!Type]
	,NULL AS [Item!10!QuestionTypeKey]
	,NULL AS [Item!10!AuthoringQuestionTypeKey]
	,NULL AS [Item!10!TotalMark]
	,NULL AS [Item!10!AverageMark]
	,NULL AS [Item!10!PercentUnAnswered]
	,NULL AS [Item!10!FacilityValue]
	,NULL AS [Item!10!DI]
	,NULL AS [Item!10!AttemptedCount]
	,NULL AS [Item!10!ViewedUnattemptedCount]
	,NULL AS [Item!10!NotViewedCount]
	,NULL AS [Item!10!AverageViewingTime]
	,NULL AS [Item!10!TestOrder]
	,NULL AS [Item!10!TestOrderIsFixed]
	,NULL AS [Item!10!LastModifierUserId]
	,NULL AS [Item!10!LastModifierFirstName]
	,NULL AS [Item!10!LastModifierLastName]
	,NULL AS [Item!10!PearsonsR]
	,NULL AS [Item!10!IsDichotomous]
	,NULL AS [Item!10!Scored]
	,NULL AS [Item!10!ScoredChanged]
	,NULL AS [Item!10!isContentAvailable]
	,NULL AS [Item!10!isHumanMarked]

UNION ALL

SELECT 
	10
	,9
	,NULL AS [Report!1] -- root element
	,NULL AS [ExamVersion!3]
	,NULL AS [ExamVersion!3!ExamKey]
	,NULL AS [ExamVersion!3!ExamName]
	,NULL AS [ExamVersion!3!ExamReference]
	,NULL AS [ExamVersion!3!ExamVersionKey]
	,NULL AS [ExamVersion!3!ExamVersionName]
	,NULL AS [ExamVersion!3!ExamVersionReference]
	,NULL AS [ExamVersion!3!LastSat]
	,NULL AS [ExamVersion!3!PassRate]
	,NULL AS [ExamVersion!3!MeanTime]
	,NULL AS [ExamVersion!3!Duration]
	,NULL AS [ExamVersion!3!MeanScore]
	,NULL AS [ExamVersion!3!MaxScore]
	,NULL AS [ExamVersion!3!MeanAttempted]
    ,NULL AS [ExamVersion!3!CronbachA]
    ,NULL AS [ExamVersion!3!TestsDelivered]
    ,NULL AS [ExamVersion!3!ItemsDelivered]
	,NULL AS [ExamVersionVersions!4]
	,NULL AS [ExamVersionVersion!5!Version]
	--,NULL AS [ExamVersionVersion!5!TotalMark]
	,NULL AS [ExamVersionVersion!5!ScaleScoreId]
	,NULL AS [ExamVersionVersion!5!GradeBoundaryId]
	,NULL AS [ExamVersionVersion!5!LastSat]
	,NULL AS [ExamVersionVersion!5!PassRate]
	,NULL AS [ExamVersionVersion!5!MeanTime]
	,NULL AS [ExamVersionVersion!5!Duration]
	,NULL AS [ExamVersionVersion!5!MeanScore]
	,NULL AS [ExamVersionVersion!5!MaxScore]
	,NULL AS [ExamVersionVersion!5!MeanAttempted]
    ,NULL AS [ExamVersionVersion!5!TestsDelivered]
    ,NULL AS [ExamVersionVersion!5!ItemsDelivered]
	,NULL AS [Items!6]
	,NULL AS [Item!7!CPID]
	,NULL AS [Item!7!CPVersion]
	,NULL AS [Item!7!TotalMark]
	,NULL
	,I.CPID
	,I.CPVersion
	,COUNT(DISTINCT I.OriginalVersion) VersionCount
	,I.CAId
	,I.QuestionName 
	,DQT.QuestionType 
	,I.QuestionTypeKey
	,I.CAQuestionTypeKey
	,AVG(I.QuestionTotalMarks)
	,AVG(I.QuestionMarks) 
	,(1 - (SUM(CAST(I.Attempted AS FLOAT)) / COUNT(I.CPID)))
	,AVG(I.QuestionMarks/I.QuestionTotalMarks) 
	,(FV1.FV - FV3.FV)
	,CAST(SUM(I.Attempted) AS int) 
	,SUM(CASE WHEN I.Attempted = 0 AND I.ViewingTime>0 THEN 1 ELSE 0 END) 
	,SUM(CASE WHEN I.Attempted = 0 AND I.ViewingTime=0 THEN 1 ELSE 0 END) 
	,AVG(I.ViewingTime)
	,MIN(I.TestOrder)
	,CASE WHEN VAR(I.TestOrder)<>0 THEN 0 ELSE 1 END OrderIsFixed
	,I.ModifiedBy 
	,DCU.FirstName 
	,DCU.LastName 
	,R.R 
	,MIN(IsDichotomous+0) 
	,CASE WHEN ISNULL(VAR(Scored),0)=0 THEN MIN(Scored) ELSE NULL END 
	,CASE WHEN ISNULL(VAR(Scored),0)=0 THEN 0 ELSE 1 END 
	,CASE WHEN Source<>2 THEN 0 ELSE 1 END [Item!10!isContentAvailable]
	,CASE WHEN SUM(I.MarkerMark) IS NOT NULL OR SUM(I.MarkingTypeKey) > 0 THEN 1 ELSE 0 END AS [Item!10!isHumanMarked]
FROM #Items I
	JOIN DimQuestionTypes DQT 
		ON DQT.QuestionTypeKey = I.QuestionTypeKey
	LEFT JOIN FV_Top FV1 
		ON I.CPID = FV1.CPID
		AND I.CPVersion = FV1.CPVersion
	LEFT JOIN FV_Bottom FV3 
		ON I.CPID = FV3.CPID
		AND I.CPVersion = FV3.CPVersion
	LEFT JOIN DimContentUsers DCU
		ON DCU.UserKey = I.ModifiedBy
	LEFT JOIN PearsonsR R
		ON R.CPID = I.CPID 
		AND R.CPVersion = I.CPVersion
GROUP BY 
	I.CPID
	,I.CPVersion
	,I.CAId
	,I.QuestionName
	,DQT.QuestionType
	,I.QuestionTypeKey
	,(FV1.FV - FV3.FV)
	,I.CAQuestionTypeKey
	,I.ModifiedBy 
	,DCU.FirstName
	,DCU.LastName
	,R.R
	,I.Source
ORDER BY [ExamVersionVersion!5!Version], Tag
FOR XML EXPLICIT

IF OBJECT_ID('tempdb..#ES') IS NOT NULL DROP TABLE #ES
IF OBJECT_ID('tempdb..#Items') IS NOT NULL DROP TABLE #Items
