USE [master]
RESTORE DATABASE [PPD_AAT_SecureAssess] 
FROM  DISK = N'T:\FTP\AAT_SecureAssess.2017.11.16.bak' 
	, DISK = N'T:\FTP\AAT_SecureAssess.2017.11.16_1.bak' 
	, DISK = N'T:\FTP\AAT_SecureAssess.2017.11.16_2.bak' 
	, DISK = N'T:\FTP\AAT_SecureAssess.2017.11.16_3.bak' 
	, DISK = N'T:\FTP\AAT_SecureAssess.2017.11.16_4.bak' 
	, DISK = N'T:\FTP\AAT_SecureAssess.2017.11.16_5.bak' 
	, DISK = N'T:\FTP\AAT_SecureAssess.2017.11.16_6.bak' 
	, DISK = N'T:\FTP\AAT_SecureAssess.2017.11.16_7.bak' 
	, DISK = N'T:\FTP\AAT_SecureAssess.2017.11.16_8.bak' 
	, DISK = N'T:\FTP\AAT_SecureAssess.2017.11.16_9.bak' 
WITH  FILE = 1
	,  MOVE N'Data' TO N'S:\Data\PPD_AAT_SecureAssess.mdf',  
	   MOVE N'Log' TO N'L:\Logs\PPD_AAT_SecureAssess.ldf'
, RECOVERY, NOUNLOAD, NOREWIND,  STATS = 5, MEDIAPASSWORD = '9c~MYZ230;M3O1=u=&ylww%n,U0Lp1WQ'

GO


