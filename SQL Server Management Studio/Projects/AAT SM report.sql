USE PPD_AAT_SecureMarker

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

DECLARE @markers TABLE (
      pos int
      ,pos2 int
      , itemID nvarchar(100)
      , itemName nvarchar(100)
      , qualName  nvarchar(100)
      , examName  nvarchar(100)
      , examVersionName nvarchar(100)
      , DateSubmitted datetime
      , [Date Marked] datetime
      , markerName nvarchar(100)
      , keycode nvarchar(10)
)

INSERT @markers
select a.R
      , a.R + 1
      , a.[Item ID]
      , a.[Item Name]
      , a.[Qualification Name]
      , a.[Assessment Name]
      , a.[ExamVersion Name]
      , a.[Assessment Date]
      , a.[Date Marked]
      , a.[Marker Name]
      , a.keycode
FROM (
SELECT ROW_NUMBER() OVER(ORDER BY agm.userID, agm.timestamp) R
      , ExternalItemID [Item ID]
      , ExternalItemName [Item Name]
      , Q.Name [Qualification Name]
      , E.Name [Assessment Name]
      , EV.Name [ExamVersion Name]
      , CEV.DateSubmitted [Assessment Date]
      , Timestamp [Date Marked]
      , U.Forename + ' ' + U.Surname [Marker Name]
      , keycode
FROM CandidateExamVersions as CEV
INNER JOIN CandidateResponses as CR ON CR.CandidateExamVersionID = CEV.ID
INNER JOIN UniqueResponses as UR ON UR.ID = CR.UniqueResponseID 
INNER JOIN UniqueGroupResponseLinks as UGRL on UGRL.UniqueResponseId = UR.id
INNER JOIN UniqueGroupResponses as UGR on UGR.ID = UGRL.UniqueGroupResponseID
INNER JOIN Items as I on I.ID = UR.itemId
INNER JOIN ExamVersions EV on EV.ID = CEV.ExamVersionID
INNER JOIN Exams E on E.ID = EV.ExamID
INNER JOIN Qualifications Q on Q.ID = E.QualificationID
INNER JOIN GroupDefinitions GD on GD.ExamID = E.ID and GD.name = ExternalItemName
INNER JOIN AssignedGroupMarks AGM on AGM.GroupDefinitionID = GD.ID and AGM.UniqueGroupResponseId = ugr.ID 
INNER JOIN Users U on U.ID = AGM.UserId
where userid = 28
      and MarkingMethodId in (4,5,6,11)
) A

SELECT *
FROM @markers
ORDER BY [Date Marked]

SELECT	a.pos
        ,a.itemID
        ,a.itemName
        ,a.qualName
        ,a.examName
        ,a.examVersionName
        ,a.DateSubmitted
        ,a.[Date Marked]
        ,a.markerName
        ,DATEDIFF(s, b.[Date Marked], a.[Date Marked]) timeDiff     
        ,a.keycode
		
FROM @markers A
INNER JOIN @markers B ON A.pos = B.pos2
where A.markerName = B.markerName
	and a.markerName = 'Tim Bates'
order by a.pos, markerName


--28
select *
from AssignedGroupMarks
where UserId = 28
		order by Timestamp

select top 1 * from UniqueGroupResponses