DECLARE @DBName nvarchar(100) =  
(
	SELECT NAME  +  CONVERT(VARCHAR(10), GETDATE(), 102) + '.bak'
	from sys.databases
	where name = '21BrokenExams'

)

DECLARE @CurrentDate nvarchar(50) = GETDATE();
DECLARE @Dynamic NVARCHAR(MAX) = '';

SELECT @Dynamic += CHAR(13) 
 +  'BACKUP DATABASE [BRITISHCOUNCIL_SecureAssess] TO  DISK = N''D:\Backup\' + @DBName + '''
	 WITH NOFORMAT, INIT,  NAME = N''BRITISHCOUNCIL_SecureAssess_backup' + @CurrentDate +''', SKIP, REWIND, NOUNLOAD, COMPRESSION,  STATS = 10
GO
declare @backupSetId as int
select @backupSetId = position from msdb..backupset where database_name=N''BRITISHCOUNCIL_SecureAssess'' and backup_set_id=
	(select max(backup_set_id) from msdb..backupset where database_name=N''BRITISHCOUNCIL_SecureAssess'' 
	)
if @backupSetId is null begin raiserror(N''Verify failed. Backup information for database ''''BRITISHCOUNCIL_SecureAssess'''' not found.'', 16, 1) end

RESTORE VERIFYONLY FROM ['+ @DBName +'] WITH  FILE = @backupSetId,  NOUNLOAD,  NOREWIND'

PRINT(@DYNAMIC)



