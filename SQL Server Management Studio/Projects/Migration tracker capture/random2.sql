INSERT [_MigrationScripts]
VALUES (1, 'abc')


SELECT * 
FROM _MigrationMetrics

dbcc sqlperf('logspace')


SELECT (
			SELECT CONVERT(DECIMAL(18, 2), SUM(CAST(df.size AS FLOAT)) * 8 / 1024.0)
				FROM sys.database_files AS df
				WHERE df.type IN (0,2,4)
				) AS [DbSize]
			, CONVERT(DECIMAL(18, 2), SUM(a.total_pages) * 8 / 1024.0)  AS [SpaceUsed]
			,(
				SELECT CONVERT(DECIMAL(18, 2), SUM(CAST(df.size AS FLOAT)) * 8 / 1024.0)
				FROM sys.database_files AS df
				WHERE df.type IN (1,3)
				) AS [LogSize]
FROM sys.partitions p
INNER JOIN sys.allocation_units a ON p.partition_id = a.container_id


INSERT [dbo].[_MigrationMetrics] ([LogFileSize MB])
values('1024.00')


declare @test table (
	tester decimal(10,2)
)

INSERT @test
values('1024.00')

select * from @test

/*
SELECT (
		SELECT CONVERT(DECIMAL(18, 2), SUM(CAST(df.size AS FLOAT)) * 8 / 1024.0)
		FROM sys.database_files AS df
		WHERE df.type IN (0,2,4)
		) AS [DbSize]
	, CONVERT(DECIMAL(18, 2), SUM(a.total_pages) * 8 / 1024.0)  AS [SpaceUsed]
	,(
		SELECT CONVERT(DECIMAL(18, 2), SUM(CAST(df.size AS FLOAT)) * 8 / 1024.0)
		FROM sys.database_files AS df
		WHERE df.type IN (1,3)
		) AS [LogSize]
FROM sys.partitions p
INNER JOIN sys.allocation_units a ON p.partition_id = a.container_id;
*/


declare @i int = 1
declare @max int = 500000

--while (1=1)

while @i < @max

begin

insert DJTable(testing)
values(@i)

set @i  = @i+1

end



select 
	(
				SELECT CONVERT(DECIMAL(18, 2), SUM(CAST(df.size AS FLOAT)) * 8 / 1024.0)
				FROM sys.database_files AS df
				WHERE df.type IN (0,2,4)
				) AS [DbSize]
			, CONVERT(DECIMAL(18, 2), SUM(a.total_pages) * 8 / 1024.0)  AS [SpaceUsed]
			,(
				SELECT CONVERT(DECIMAL(18, 2), SUM(CAST(df.size AS FLOAT)) * 8 / 1024.0)
				FROM sys.database_files AS df
				WHERE df.type IN (1,3)
				) AS [LogSize]
FROM sys.partitions p
INNER JOIN sys.allocation_units a ON p.partition_id = a.container_id