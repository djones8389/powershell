IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES where TABLE_NAME = '_MigrationScripts')
	DROP TABLE [_MigrationScripts];

--CREATE TABLE [dbo].[_MigrationScripts](
--	[ID] [int] NOT NULL,
--	[FileName] [nvarchar](500) NOT NULL
--) 

CREATE TABLE [dbo].[_MigrationScripts](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[BatchId] [nvarchar](36) NOT NULL,
	[FileName] [nvarchar](500) NOT NULL,
	[Checksum] [nchar](32) NOT NULL,
	[StartDateTime] [datetime] NOT NULL,
	[EndDateTime] [datetime] NOT NULL,
	[WindowsUserName] [nvarchar](128) NOT NULL,
	[MachineName] [nvarchar](128) NOT NULL,
	[TimeTaken]  AS (datediff(second,[StartDateTime],[EndDateTime]))
)

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES where TABLE_NAME = '_MigrationDatabaseMetrics')
	DROP TABLE [dbo].[_MigrationDatabaseMetrics];

CREATE TABLE [dbo].[_MigrationDatabaseMetrics] (
	MigrationID int
	,  [DatabaseSizeOnDisk MB] decimal(10,2)
	,  [SpaceUsedInDB MB] decimal(10,2)
	,  [LogFileSize MB] decimal(10,2)
)


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES where TABLE_NAME = '_MigrationTableMetrics')
	DROP TABLE [dbo].[_MigrationTableMetrics];


CREATE TABLE [dbo].[_MigrationTableMetrics] (
	table_name nvarchar(100)
	, rows int
	, reserved_kb nvarchar(20)
	, data_kb nvarchar(20)
	, index_size nvarchar(20)
	, unused_kb nvarchar(20)
	, fileID INT
)

IF EXISTS (SELECT 1 FROM sys.objects where type_desc = 'SQL_TRIGGER' and name = 'MigrationTracker_Trigger')
	DROP TRIGGER [MigrationTracker_Trigger]


/*
CREATE TRIGGER [dbo].[MigrationTracker_Trigger]
   ON  [dbo].[_MigrationScripts]
   AFTER INSERT
AS 
BEGIN

	DECLARE @id tinyint;

	select  
		@id = [ID]
	from inserted

	INSERT [dbo].[_MigrationDatabaseMetrics] ([MigrationID], [DatabaseSizeOnDisk MB], [SpaceUsedInDB MB], [LogFileSize MB])
	select @id
		,  
			 (
					SELECT CONVERT(DECIMAL(18, 2), SUM(CAST(df.size AS FLOAT)) * 8 / 1024.0)
					FROM sys.database_files AS df
					WHERE df.type IN (0,2,4)
					) AS [DbSize]
				, CONVERT(DECIMAL(18, 2), SUM(a.total_pages) * 8 / 1024.0)  AS [SpaceUsed]
				,(
					SELECT CONVERT(DECIMAL(18, 2), SUM(CAST(df.size AS FLOAT)) * 8 / 1024.0)
					FROM sys.database_files AS df
					WHERE df.type IN (1,3)
					) AS [LogSize]
	FROM sys.partitions p
	INNER JOIN sys.allocation_units a ON p.partition_id = a.container_id;

	declare @dynamic nvarchar(MAX) = '';

	select @dynamic +=CHAR(13) +
		'exec sp_spaceused @objname = '''+s.name+'.'+t.name + ''' '
	from sys.tables T
	inner join sys.schemas S
	on s.schema_id = t.schema_id
	where type = 'u'
		and t.name not in ('_MigrationDatabaseMetrics','_MigrationScripts','_MigrationTableMetrics' )
	order by t.name;


	INSERT [dbo].[_MigrationTableMetrics] (table_name, [rows], reserved_kb, data_kb, index_size, unused_kb)
	EXEC(@dynamic);

	UPDATE [_MigrationTableMetrics]
	SET fileID = @id
	where fileID is null

END


select * 
from [_MigrationTableMetrics]
order by 1

exec sp_spaceused @objname = 'ConditionalJoinExample' 



truncate table [_MigrationTableMetrics]
truncate table [dbo].[_MigrationDatabaseMetrics]
truncate table[dbo].[_MigrationScripts]










	declare @dynamic nvarchar(MAX) = '';

	select @dynamic +=CHAR(13) +
		 'exec sp_spaceused @objname = '''+ name + ''' '
	from sys.tables
	where type = 'u'
		and name not in ('_MigrationDatabaseMetrics','_MigrationScripts','_MigrationTableMetrics' )
	order by name;

	print(@dynamic)

*/