SELECT
    DB_NAME(db.database_id) DatabaseName,
    (CAST(mfrows.RowSize AS FLOAT)*8)/1024 RowSizeMB,
    (CAST(mflog.LogSize AS FLOAT)*8)/1024 LogSizeMB
FROM sys.databases db
    LEFT JOIN (SELECT database_id, SUM(size) RowSize FROM sys.master_files WHERE type = 0 GROUP BY database_id, type) mfrows ON mfrows.database_id = db.database_id
    LEFT JOIN (SELECT database_id, SUM(size) LogSize FROM sys.master_files WHERE type = 1 GROUP BY database_id, type) mflog ON mflog.database_id = db.database_id
    where DB_NAME(db.database_id) = 'PPD_BCIntegration_SecureAssess'



if OBJECT_ID('tempdb..#FileStats') is not null drop table #FileStats;

CREATE TABLE #FileStats (
      FileID int,
      FileGroup int,
      TotalExtents int,
      UsedExtents int,
      LogicalName sysname,
      FileName nchar(520)
);


declare @sql nvarchar(MAX);
select @sql = 'DBCC showfilestats;'

INSERT #FileStats 
exec(@sql)

select ((sum(TotalExtents)) * 64)/1024  [Data: Total MB]
	, ((sum(UsedExtents)) * 64)/1024 [Data: Used MB]
	, ((sum(TotalExtents)) * 64)/1024 - ((sum(UsedExtents)) * 64)/1024  [Data: Unused MB]
from #FileStats

exec sp_spaceused

SELECT CAST(((SUM(total_pages) * 8192) / 1048576.0) AS FLOAT) FROM sys.allocation_units




SELECT (
		SELECT CONVERT(DECIMAL(18, 2), SUM(CAST(df.size AS FLOAT)) * 8 / 1024.0)
		FROM sys.database_files AS df
		WHERE df.type IN (0,2,4)
		) AS [DbSize]
	, CONVERT(DECIMAL(18, 2), SUM(a.total_pages) * 8 / 1024.0)  AS [SpaceUsed]
	,(
		SELECT CONVERT(DECIMAL(18, 2), SUM(CAST(df.size AS FLOAT)) * 8 / 1024.0)
		FROM sys.database_files AS df
		WHERE df.type IN (1,3)
		) AS [LogSize]
FROM sys.partitions p
INNER JOIN sys.allocation_units a ON p.partition_id = a.container_id;



--select *
--FROM sys.dm_db_index_physical_stats(DB_ID(),NULL,NULL,NULL,NULL) A


--select *
--from sys.partitions D
--INNER JOIN sys.objects B

--INNER JOIN sys.dm_db_index_physical_stats(DB_ID(),NULL,NULL,NULL,NULL) A