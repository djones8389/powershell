/****** Script for SelectTopNRows command from SSMS  ******/
SELECT [table_name]
      ,[rows]
      ,[reserved_kb]
      ,[data_kb]
      ,[index_size]
      ,[unused_kb]
      ,[fileID]
  FROM [21BrokenExams].[dbo].[_MigrationTableMetrics]




SELECT FileID	
	 , sum(cast(((replace(Data_kb, 'kb' ,'')) ) as int)) [TotalDataKB]
	 , sum(cast(((replace(Index_size, 'kb' ,'')) ) as int)) [TotalIndexKB]
FROM [21BrokenExams].[dbo].[_MigrationTableMetrics]
group by FILEID
