IF OBJECT_ID('tempdb..##SubjectCount') IS NOT NULL DROP TABLE ##SubjectCount;

CREATE TABLE ##SubjectCount (
	client sysname
	, SubjectCount int
);

DECLARE @dynamic nvarchar(MAX)='';

select @dynamic +=CHAR(13)+ '
use '+QUOTENAME([NAME])+'

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

INSERT ##SubjectCount
select db_name() [client]
	, count(ID) [SubjectCount]
from Subjects

'
from sys.databases
where state_desc = 'ONLINE'
	and name like '%[_]SurpassManagement';

exec(@dynamic);

select substring(client, 0, charindex('_',client)) [Client]
	, SubjectCount
from ##SubjectCount
order by 1 asc;






