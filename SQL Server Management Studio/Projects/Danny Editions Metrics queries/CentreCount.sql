IF OBJECT_ID('tempdb..##Centres') IS NOT NULL DROP TABLE ##Centres;

CREATE TABLE ##Centres (
	client sysname
	, CentreCount smallint
);

DECLARE @dynamic nvarchar(MAX)='';

select @dynamic +=CHAR(13)+ '
use '+QUOTENAME([NAME])+'

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

INSERT ##Centres
select db_name() [client]
	, count(ID) [CentreCount]
from Centres
'
from sys.databases
where state_desc = 'ONLINE'
	and name like '%[_]SurpassManagement';

exec(@dynamic);

select substring(client, 0, charindex('_',client)) [Client]
	, CentreCount
from ##Centres
order by 1 asc;






