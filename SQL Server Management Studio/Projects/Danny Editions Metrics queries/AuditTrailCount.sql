IF OBJECT_ID('tempdb..##AuditCount') IS NOT NULL DROP TABLE ##AuditCount;

CREATE TABLE ##AuditCount (
	client sysname
	, [Count] int
);

DECLARE @dynamic nvarchar(MAX)='';

select @dynamic +=CHAR(13)+ '
use '+QUOTENAME([NAME])+'

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

INSERT ##AuditCount
select  
	db_name() [client]
	,count([Id]) [Count]
FROM  [dbo].[AuditTrails];

'
from sys.databases
where state_desc = 'ONLINE'
	and name like '%[_]ContentAuthor';

exec(@dynamic);

select substring(client, 0, charindex('_',client)) [Client]
	, [Count]
from ##AuditCount
order by 1 asc;






