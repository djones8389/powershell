USE [PRV_BRITISHCOUNCIL_SecureAssess];

ALTER DATABASE [PRV_BRITISHCOUNCIL_SecureAssess]
	ADD FILEGROUP [BC_WHShrededItems]
GO


ALTER DATABASE [PRV_BRITISHCOUNCIL_SecureAssess]
ADD FILE (

	NAME = BC_WHShrededItems
	, FILENAME = 'E:\BC_WHShrededItems\BC_WHShrededItems.ndf'
    , SIZE = 2000MB
    , MAXSIZE = UNLIMITED
    , FILEGROWTH = 1000MB
)
TO FILEGROUP [BC_WHShrededItems];
GO

CREATE TABLE [dbo].[WAREHOUSE_ExamSessionTable_ShrededItems_new] (
	[examSessionId] [int] NOT NULL,
	[ItemRef] [varchar](15) NULL,
	[ItemName] [nvarchar](200) NULL,
	[userMark] [decimal](6, 3) NULL,
	[markerUserMark] [nvarchar](max) NULL,
	[examPercentage] [decimal](6, 3) NULL,
	[candidateId] [int] IDENTITY(1,1) NOT NULL,
	[ItemVersion] [int] NULL,
	[responsexml] [xml] NULL,
	[OptionsChosen] [varchar](200) NULL,
	[selectedCount] [int] NULL,
	[correctAnswerCount] [int] NULL,
	[TotalMark] [decimal](6, 3) NOT NULL,
	[userAttempted] [bit] NULL,
	[markingIgnored] [tinyint] NULL,
	[markedMetadataCount] [int] NULL,
	[ItemType] [int] NULL,
	[FriendItems] [nvarchar](max) NULL
) ON [BC_WHShrededItems] TEXTIMAGE_ON [BC_WHShrededItems]
GO

set identity_insert [WAREHOUSE_ExamSessionTable_ShrededItems_new] on

INSERT [WAREHOUSE_ExamSessionTable_ShrededItems_new] (examSessionId, ItemRef, ItemName, userMark, markerUserMark, examPercentage, candidateId, ItemVersion, responsexml, OptionsChosen, selectedCount, correctAnswerCount, TotalMark, userAttempted, markingIgnored, markedMetadataCount, ItemType, FriendItems)
SELECT examSessionId, ItemRef, ItemName, userMark, markerUserMark, examPercentage, candidateId, ItemVersion, responsexml, OptionsChosen, selectedCount, correctAnswerCount, TotalMark, userAttempted, markingIgnored, markedMetadataCount, ItemType, FriendItems
FROM [WAREHOUSE_ExamSessionTable_ShrededItems];

set identity_insert [WAREHOUSE_ExamSessionTable_ShrededItems_new] off

PRINT 'Data inserted into new table :'   --36 min, 47 secs
PRINT  getDATE();

IF (select COUNT(*) FROM [WAREHOUSE_ExamSessionTable_ShrededItems]) = (select COUNT(*) FROM [WAREHOUSE_ExamSessionTable_ShrededItems_new])

BEGIN
	
	ALTER TABLE [dbo].[WAREHOUSE_ExamSessionTable_ShrededItems] DROP CONSTRAINT [PK_WAREHOUSE_ExamSessionTable_ShrededItems]
	ALTER TABLE  [dbo].[WAREHOUSE_ExamSessionTable_ShrededItems] DROP CONSTRAINT  [FK_WAREHOUSE_ExamSessionTable_ShrededItems_WAREHOUSE_ExamSessionTable_Shreded]
	ALTER TABLE  [dbo].[WAREHOUSE_ExamSessionTable_ShrededItems] DROP CONSTRAINT [DF__WAREHOUSE__Total__28A2FA0E]
	ALTER TABLE [dbo].[WAREHOUSE_ExamSessionTable_ShreddedItems_Mark]  DROP CONSTRAINT [FK_WAREHOUSE_ExamSessionTable_ShreddedItems_Mark_WAREHOUSE_ExamSessionTable_ShrededItems] 

	DROP TABLE [dbo].[WAREHOUSE_ExamSessionTable_ShrededItems];

	PRINT 'DROP TABLE [dbo].[WAREHOUSE_ExamSessionTable_ShrededItems]'

	EXEC sp_rename 'WAREHOUSE_ExamSessionTable_ShrededItems_new','WAREHOUSE_ExamSessionTable_ShrededItems';

	PRINT 'Dropped and renamed table :'  --v quick
	PRINT  getDATE();

END

	ALTER TABLE [dbo].[WAREHOUSE_ExamSessionTable_ShrededItems]
		ADD CONSTRAINT [PK_WAREHOUSE_ExamSessionTable_ShrededItems] PRIMARY KEY CLUSTERED  (
		[examSessionId] ASC,
		[candidateId] ASC
	)

	ALTER TABLE [dbo].[WAREHOUSE_ExamSessionTable_ShrededItems] ADD  CONSTRAINT [DF__WAREHOUSE__Total__28A2FA0E]  DEFAULT ((0)) FOR [TotalMark]
	GO

	ALTER TABLE [dbo].[WAREHOUSE_ExamSessionTable_ShrededItems]  WITH CHECK ADD  CONSTRAINT [FK_WAREHOUSE_ExamSessionTable_ShrededItems_WAREHOUSE_ExamSessionTable_Shreded] 
	FOREIGN KEY([examSessionId])
	REFERENCES [dbo].[WAREHOUSE_ExamSessionTable_Shreded] ([examSessionId])
	ON DELETE CASCADE
	GO

	ALTER TABLE [dbo].[WAREHOUSE_ExamSessionTable_ShrededItems] CHECK CONSTRAINT [FK_WAREHOUSE_ExamSessionTable_ShrededItems_WAREHOUSE_ExamSessionTable_Shreded]
	GO



	ALTER TABLE [dbo].[WAREHOUSE_ExamSessionTable_ShreddedItems_Mark]  WITH CHECK ADD  CONSTRAINT [FK_WAREHOUSE_ExamSessionTable_ShreddedItems_Mark_WAREHOUSE_ExamSessionTable_ShrededItems] FOREIGN KEY([ExamSessionID], [ItemID])
	REFERENCES [dbo].[WAREHOUSE_ExamSessionTable_ShrededItems] ([examSessionId], [ItemRef])
	GO

	ALTER TABLE [dbo].[WAREHOUSE_ExamSessionTable_ShreddedItems_Mark] CHECK CONSTRAINT [FK_WAREHOUSE_ExamSessionTable_ShreddedItems_Mark_WAREHOUSE_ExamSessionTable_ShrededItems]
	GO



	PRINT 'Added constraints :'
	PRINT  getDATE();

	CREATE UNIQUE NONCLUSTERED INDEX [IX_WAREHOUSE_ExamSessionTable_ShrededItems] ON [dbo].[WAREHOUSE_ExamSessionTable_ShrededItems]
	(
		[examSessionId] ASC,
		[ItemRef] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	GO

	CREATE NONCLUSTERED INDEX [IX_ExamSessionID] ON [dbo].[WAREHOUSE_ExamSessionTable_ShrededItems]
	(
		[examSessionId] ASC
	)
	INCLUDE ( 	[examPercentage],
		[ItemName],
		[ItemRef],
		[userMark]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	GO


	PRINT 'Added Indexes :'
	PRINT  getDATE();