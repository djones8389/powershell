USE CONNECT_SecureAssess

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

--------1------------

	--;with Uploaded as (
	--select distinct examsessionid
	--from examstatechangeaudittable
	--where newstate = 9
	--	and statechangedate between '2018-05-17 00:00:00' and '2018-05-17 23:59:59'

	--union

	--select west.examsessionid
	--from WAREHOUSE_ExamSessionTable_Shreded wests
	--inner join WAREHOUSE_ExamSessionTable west
	--on west.id = wests.examSessionID
	--where submitted between '2018-05-17 00:00:00' and '2018-05-17 23:59:59'
	--	and examstateChangeAuditxml.exist('//stateChange[newStateID=9]') =1
	--)

	--select count(1) [Uploaded]
	--from Uploaded;

------------2------------

	--;with Submitted as (
	--select distinct examsessionid
	--from examstatechangeaudittable
	--where newstate = 6
	--	and statechangedate between '2018-05-16 00:00:00' and '2018-05-16 23:59:59'

	--union

	--select west.examsessionid
	--from WAREHOUSE_ExamSessionTable_Shreded wests
	--inner join WAREHOUSE_ExamSessionTable west
	--on west.id = wests.examSessionID
	--where [started] between '2018-05-16 00:00:00' and '2018-05-16 23:59:59'	
	--	and examstateChangeAuditxml.exist('//stateChange[newStateID=6]') = 1
	--)

	--select count(1) [Submitted]
	--from Submitted;



--	;with Uploaded as (
--	select distinct examsessionid
--		, statechangedate
--	from examstatechangeaudittable
--	where newstate = 9
--		and statechangedate between '2018-05-17 00:00:00' and '2018-05-17 23:59:59'

--	union

--	select distinct west.examsessionid
--		, examstateChangeAuditxml.value('data(exam/stateChange[newStateID=9]/changeDate)[1]','datetime') statechangedate
--	from WAREHOUSE_ExamSessionTable_Shreded wests
--	inner join WAREHOUSE_ExamSessionTable west
--	on west.id = wests.examSessionID
--	where submitted between '2018-05-17 00:00:00' and '2018-05-17 23:59:59'
--		and examstateChangeAuditxml.exist('//stateChange[newStateID=9]') =1
--		and examstateChangeAuditxml.value('data(exam/stateChange[newStateID=9]/changeDate)[1]','datetime') between '2018-05-17 00:00:00' and '2018-05-17 23:59:59'	
--	)

------------3------------

--	Select *
--	from Uploaded
--	order by statechangedate;

----------4------------

--	select count(1) [Count]
--		, b.[Hour]
--	from (
--	select  DATEPART(HOUR, statechangedate) [Hour]
--	from Uploaded
--	) b
--	group by [Hour]


----------5------------


	;with sixteenth as (
	select examSessionID
		,  datalength(document) DL
	from examsessiondocumenttable (READUNCOMMITTED)
	where uploaddate between '2018-05-16 00:00:00' and '2018-05-16 23:59:59'

	union

	select wesdt.warehouseexamsessionid
		,  datalength(document) DL
	from warehouse_Examsessiondocumenttable wesdt (READUNCOMMITTED)
	where uploaddate between '2018-05-16 00:00:00' and '2018-05-16 23:59:59'
	)

	select SUM(cast(DL as bigint))/1024/1024/1024
		--, examSessionID
	from sixteenth
	--group by examSessionID


	;with seventeenth as (
	select examSessionID
		,  datalength(document) DL
	from examsessiondocumenttable (READUNCOMMITTED)
	where uploaddate between '2018-05-17 00:00:00' and '2018-05-17 23:59:59'

	union

	select wesdt.warehouseexamsessionid
		,  datalength(document) DL
	from warehouse_Examsessiondocumenttable wesdt (READUNCOMMITTED)
	where uploaddate between '2018-05-17 00:00:00' and '2018-05-17 23:59:59'
	)

	select SUM(cast(DL as bigint))/1024/1024/1024
		--, examSessionID
	from seventeenth
	--group by examSessionID





----------6------------

	;with sixteenth as (
	select examSessionID
			, count(id) [id]
	from examsessiondocumenttable (READUNCOMMITTED)
	where uploaddate between '2018-05-16 00:00:00' and '2018-05-16 23:59:59'
	group by examSessionID

	union

	select  wesdt.warehouseexamsessionid as examSessionID
			, count(wesdt.id) [id]
	from warehouse_Examsessiondocumenttable wesdt (READUNCOMMITTED)
	where uploaddate between '2018-05-16 00:00:00' and '2018-05-16 23:59:59'
	group by  wesdt.warehouseexamsessionid
	)
	
	select SUM([id]) [16th]
	from sixteenth


	;with seventeenth as (
	select examSessionID
			, count(id) [id]
	from examsessiondocumenttable (READUNCOMMITTED)
	where uploaddate between '2018-05-17 00:00:00' and '2018-05-17 23:59:59'
	group by examSessionID
	union

	select  wesdt.warehouseexamsessionid as examSessionID
			, count(wesdt.id) [id]
	from warehouse_Examsessiondocumenttable wesdt (READUNCOMMITTED)
	where uploaddate between '2018-05-17 00:00:00' and '2018-05-17 23:59:59'
	group by  wesdt.warehouseexamsessionid
	)
	
	select SUM([id]) [17th]
	from seventeenth