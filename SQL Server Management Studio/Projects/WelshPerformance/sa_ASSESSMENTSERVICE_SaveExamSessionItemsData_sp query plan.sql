--declare @p1 dbo.IntList
--insert into @p1 values(156078)

--declare @p2 dbo.ItemDetailsList
--insert into @p2 values(N'5004P5193',10,1,N'62iiE5',N'en',0,11,1,1)
--insert into @p2 values(N'5004P5195',12,0,N'62iiW5',N'we',0,11,1,1)

--exec sa_ASSESSMENTSERVICE_SaveExamSessionItemsData_sp @ExamSessions=@p1,@ItemsList=@p2

SELECT plan_handle, query_plan, objtype, *   
FROM sys.dm_exec_cached_plans   
CROSS APPLY sys.dm_exec_query_plan(plan_handle)   
WHERE cast(query_plan as nvarchar(MAX)) like '%sa_ASSESSMENTSERVICE_SaveExamSessionItemsData_sp%'
GO  