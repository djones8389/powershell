SELECT NEWID() [id]
	  ,@@ServerName [Instance]
	  ,DB_NAME(database_id) AS DBName
      ,OBJECT_SCHEMA_NAME(object_id,database_id) AS [SCHEMA_NAME] 
      ,OBJECT_NAME(object_id,database_id) AS [OBJECT_NAME]
      ,cached_time
      ,last_execution_time
      ,execution_count
      ,total_worker_time CPU
      ,total_elapsed_time ELAPSED
	  , total_elapsed_time/execution_count [avg]
      ,total_logical_reads LOGICAL_READS
      ,total_logical_writes LOGICAL_WRITES
      ,total_physical_reads PHYSICAL_READS
FROM sys.dm_exec_procedure_stats 

where database_id > 4
	and OBJECT_NAME(object_id,database_id) = 'sa_ASSESSMENTSERVICE_SaveExamSessionItemsData_sp'
	and database_id != 32767


WAITFOR DELAY '00:05:00'

SELECT NEWID() [id]
	  ,@@ServerName [Instance]
	  ,DB_NAME(database_id) AS DBName
      ,OBJECT_SCHEMA_NAME(object_id,database_id) AS [SCHEMA_NAME] 
      ,OBJECT_NAME(object_id,database_id) AS [OBJECT_NAME]
      ,cached_time
      ,last_execution_time
      ,execution_count
      ,total_worker_time CPU
      ,total_elapsed_time ELAPSED
      ,total_logical_reads LOGICAL_READS
      ,total_logical_writes LOGICAL_WRITES
      ,total_physical_reads PHYSICAL_READS
FROM sys.dm_exec_procedure_stats 

where database_id > 4
	and OBJECT_NAME(object_id,database_id) = 'sa_ASSESSMENTSERVICE_SaveExamSessionItemsData_sp'
	and database_id != 32767

	/*
		2018-05-31 12:03:55.323  4443
		2018-05-31 12:04:11.667	 4533
		

		110 times every 16 seconds

	*/






select count(1)
from examsessionitemresponsetable
where id = 156078

select id, examstate, structurexml
from examsessiontable
where id = 156078

select *
from examstatechangeaudittable 
where examsessionid = 156078

select *
from examstatechangeaudittable (NOLOCK)
where NewState in (1,4)
	and examsessionid = 155998

select a.examsessionid	
	, DATEDIFF(second,statechangedatemin,statechangedatemax) Diff
from (
select ExamSessionID
	, min(statechangedate) statechangedatemin
	, max(statechangeDate) statechangedatemax
from examstatechangeaudittable (NOLOCK)
where NewState in (1,4)
group by ExamSessionID
) a
order by 2


select examsessionid	
	, DATEDIFF(second,statechangedatemin,statechangedatemax) DiffS
	, DATEDIFF(Minute,statechangedatemin,statechangedatemax) DiffM
from (
select ExamSessionID
	, min(statechangedate) statechangedatemin
	, max(statechangeDate) statechangedatemax
from examstatechangeaudittable (NOLOCK)
where ExamSessionID IN (
	SELECT [1].ExamSessionID
	FROM (
		select ExamSessionID
			,statechangedate
		from examstatechangeaudittable 
		where NewState in (1)
	) [1]
	inner join (
		select ExamSessionID
			,statechangedate
		from examstatechangeaudittable 
		where NewState in (4)
	) [4]
	ON [1].ExamSessionID = [4].ExamSessionID
	group by [1].ExamSessionID
)
	and newstate in (1,4)
group by ExamSessionID
) a

order by 2

examsessionid	Diff
77891		13
155625		28439


select *
from examStatechangeaudittable
where examsessionid = 155625



select AVG(DATEDIFF(MINUTE,statechangedatemin,statechangedatemax)) Diff
from (
select ExamSessionID
	, min(statechangedate) statechangedatemin
	, max(statechangeDate) statechangedatemax
from examstatechangeaudittable (NOLOCK)
where ExamSessionID IN (
	SELECT [1].ExamSessionID
	FROM (
		select ExamSessionID
			,statechangedate
		from examstatechangeaudittable 
		where NewState in (1)
	) [1]
	inner join (
		select ExamSessionID
			,statechangedate
		from examstatechangeaudittable 
		where NewState in (4)
	) [4]
	ON [1].ExamSessionID = [4].ExamSessionID
	group by [1].ExamSessionID
)
group by ExamSessionID
) a




