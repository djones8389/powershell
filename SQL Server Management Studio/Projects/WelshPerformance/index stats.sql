--USE [Asesu_SecureAssess]
--GO

--/****** Object:  Index [IX_ExamSessionItemsTable_ExamSessionID_ItemID_Version]    Script Date: 31/05/2018 11:58:18 ******/
--CREATE UNIQUE NONCLUSTERED INDEX [IX_ExamSessionItemsTable_ExamSessionID_ItemID_Version] ON [dbo].[ExamSessionItemsTable]
--(
--	[ExamSessionID] ASC,
--	[ItemID] ASC,
--	[Version] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
--GO


SELECT	newID() as [ID],
			@@SERVERNAME as [Instance],
			 DB_NAME() AS [database],
			 OBJECT_NAME(s.OBJECT_ID) AS [table],
			 i.name AS [index],
			 s.user_updates,
			 [last_user_update] ,
			 s.user_seeks , 
			 s.user_scans, 
			 s.user_lookups
			 ,  [last_user_seek]
			 ,  [last_user_scan]
			 ,  [last_user_lookup]
	 FROM		sys.dm_db_index_usage_stats AS s WITH (NOLOCK)
	 INNER JOIN sys.indexes AS i WITH (NOLOCK)
			 ON s.[object_id] = i.[object_id]
			AND i.index_id = s.index_id
	WHERE	 OBJECTPROPERTY(s.[OBJECT_ID],'IsUserTable') = 1
	 AND	 i.index_id > 1
 	 and     s.database_id = DB_ID()
	 and     OBJECT_NAME(s.OBJECT_ID)  = 'ExamSessionItemsTable'
	ORDER BY 1,2
	OPTION	 (RECOMPILE);
