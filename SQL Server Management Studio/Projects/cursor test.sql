SET NOCOUNT ON;
USE tempdb;

--IF OBJECT_ID(N'dbo.Arrays', N'U') IS NOT NULL DROP TABLE dbo.Arrays;

--CREATE TABLE dbo.Arrays
--(
--  id  VARCHAR(10)   NOT NULL PRIMARY KEY,
--  arr VARCHAR(8000) NOT NULL
--);
--GO

--INSERT INTO dbo.Arrays VALUES('A', '20,223,2544,25567,14');
--INSERT INTO dbo.Arrays VALUES('B', '30,-23433,28');
--INSERT INTO dbo.Arrays VALUES('C', '12,10,8099,12,1200,13,12,14,10,9');
--INSERT INTO dbo.Arrays VALUES('D', '-4,-6,-45678,-2');

declare mycursor cursor for

SELECT top 1 id
	, arr
FROM Arrays

declare @id char(1), @arr nvarchar(100)

open mycursor

fetch next from mycursor into @id,@arr

while @@FETCH_STATUS = 0
	begin

	while(@arr like '%,%')
	begin
		declare @counter tinyint = (select  LEN(@arr) - LEN(REPLACE(@arr,',','')))
			
		--4
		while @counter > 0
			begin
				declare @@pos int 

				--declare @start tinyint = 0	
				if (@counter=1)
						begin 

							select @@pos = (select CHARINDEX(',', @arr))  --set start pos to 3;						
							select SUBSTRING(@arr,0, CHARINDEX(',', @arr))					
						end	
						
				else
					print 'hi'
					
				--	select SUBSTRING(@Arr, @pos, CHARINDEX(',', @arr))
					
					--set @start = @pos
					--select @pos int = (select CHARINDEX(',', @arr))
					--select SUBSTRING(@arr,(select CHARINDEX(',', @arr)), CHARINDEX(',', @arr))
			
			select @counter = @@pos - 1

			end

	end


	fetch next from mycursor into @id,@arr
	end
close mycursor
deallocate  mycursor


--declare @arr varchar(100) = '20,223,2544,25567,14'
--(select CHARINDEX(',', @arr))


--select CHARINDEX

--select  LEN(@arr) - LEN(REPLACE(@arr,',',''))