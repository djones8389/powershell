SELECT EST.ID, EST.KeyCode, pinNumber, EST.examState, UT.Forename, UT.Surname, CT.CentreName, IB.QualificationName, SCET.examName	
  FROM ExamSessionTable as EST (NOLOCK)

  Inner Join ScheduledExamsTable as SCET (NOLOCK)
  on SCET.ID = EST.ScheduledExamID
   
  Inner Join UserTable as UT (NOLOCK)
  on UT.ID = EST.UserID

  Inner Join CentreTable as CT (NOLOCK)
  on CT.ID = SCET.CentreID
  
  Inner join IB3QualificationLookup as IB (NOLOCK)
  on IB.ID = SCET.qualificationId

WHERE KeyCode = 'C3HDMD7K'