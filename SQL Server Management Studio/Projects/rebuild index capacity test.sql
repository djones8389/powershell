use SecureAssess_Populated

--dbcc showfilestats

--dbcc shrinkfile ('SecureAssess',2411)

/*

Fileid	FileGroup	TotalExtents	UsedExtents	Name			FileName
1		1			38640			38638		SecureAssess	D:\Data\Develop-MI-SA.mdf

					45059	38717

					--396.98 MB (9%) free space.


					342.1484375
*/

DECLARE IndexMaintenance CURSOR FOR
			SELECT	distinct 'ALTER INDEX ' + quotename(ix.name) +  ' ON ' + quotename(schema_name(schema_id)) + '.' + quotename(B.name) + ' REBUILD;'
				FROM	 sys.dm_db_index_physical_stats(DB_ID(5), NULL, NULL, NULL, NULL) ix_phy
				INNER JOIN sys.indexes ix
				ON ix_phy.object_id = ix.object_id
					AND ix_phy.index_id = ix.index_id
				INNER JOIN sys.objects B
				ON ix_phy.object_id = B.object_id
			WHERE ix.index_id > 0
				and b.type = 'u'
		
		DECLARE @SQL NVARCHAR(1000);
		OPEN IndexMaintenance;
		FETCH NEXT FROM IndexMaintenance INTO @SQL;
		WHILE @@FETCH_STATUS = 0
		BEGIN
			PRINT(@SQL);
			FETCH NEXT FROM IndexMaintenance INTO @SQL;
		END
		CLOSE IndexMaintenance;
		DEALLOCATE IndexMaintenance;