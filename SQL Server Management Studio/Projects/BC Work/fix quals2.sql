

UPDATE CandidateExams
set QualificationId = q.QualificationID
FROM CandidateExams C
inner join [Qualifications] Q
on Q.QualificationName = c.QualificationName
	and q.QualificationRef = c.QualificationRef
GO

--ALTER TABLE CandidateExams
--	drop column QualificationName;

--ALTER TABLE CandidateExams
--	drop column QualificationRef;


--UPDATE TestPackages
--set QualificationID = ce.QualificationID
--from TestPackages TP
--inner join [CandidateSchedules] cs
--on TP.ScheduledPackageID = CS.ScheduledPackageId
--inner join [CandidateExams] ce
--on  cs.CandidateScheduleID = ce.CandidateScheduleID;
--GO

--select 
--	Keycode
--	, q.*
--	,tp.QualificationID
--from [CandidateExams] ce
--inner join [CandidateSchedules] cs on cs.CandidateScheduleID = ce.CandidateScheduleID
--inner join TestPackages TP on TP.ScheduledPackageID = CS.ScheduledPackageId
--inner join Qualifications q on q.QualificationID = tp.QualificationID
--where   keycode = '3VKR4H99'
