/****** Script for SelectTopNRows command from SSMS  ******/
SELECT p.[PackageId]
      ,p.[Name]
      ,p.[Description]
      ,p.[SurpassQualificationId]
      ,p.[QualificationID]
	  ,q.*
	  , q1.*
  FROM [BC_TPM_Extract].[dbo].[Packages] p
  inner join [BC_TPM_Extract_05082017].[dbo].[xxQualifications] Q
  on q.[QualificationID] = p.SurpassQualificationId
  inner join [BC_TPM_Extract].[dbo].[Qualifications] q1
  on q1.QualificationName = q.QualificationName
	and q1.QualificationRef = q.QualificationRef

  where p.[QualificationID] is null
  order by [PackageId]


begin tran

select *
from [Packages]
where QualificationID is null
order by 1

UPDATE [Packages]
set QualificationID = q1.QualificationID
FROM [BC_TPM_Extract].[dbo].[Packages] p
inner join [BC_TPM_Extract_05082017].[dbo].[xxQualifications] Q
on q.[QualificationID] = p.SurpassQualificationId
inner join [BC_TPM_Extract].[dbo].[Qualifications] q1
	on q1.QualificationName = q.QualificationName
	and q1.QualificationRef = q.QualificationRef
where p.[QualificationID] is null

select *
from [Packages]
where QualificationID is null
order by 1

rollback

--select *
--into [Packages_backup_08052017]
--from [Packages]

--UPDATE [Packages]
--set QualificationID = 


select *
from [Packages] p
inner join [dbo].[IB3Lookup] ib
on ib.id = p.SurpassQualificationId
where p.QualificationID is null
	order by 1

--update [Packages]
--set QualificationID = 146
--where SurpassQualificationId = 146

select ib.*
from [IB3Lookup] ib
left join Qualifications q
on q.QualificationName = ib.QualificationName
	and q.QualificationRef = ib.QualificationRef
where q.QualificationID is null

drop TABLE [dbo].[Qualifications_new]
go
CREATE TABLE [dbo].[Qualifications_new](
	[QualificationID]  [bigint] identity(1,1),
	[QualificationRef] [nvarchar](4000) NULL,
	[QualificationName] [nvarchar](4000) NULL,
	[AptisQualID] int null
) ON [PRIMARY]
GO

set identity_insert [Qualifications_new] on 

insert [Qualifications_new](QualificationID, QualificationRef, QualificationName)
select ib.QualificationID, ib.QualificationRef, ib.QualificationName
from Qualifications ib

insert [Qualifications_new](QualificationID, QualificationRef, QualificationName)
select  ib.[qualificationid], ib.QualificationRef, ib.QualificationName
from [SA_ShrededQuals] ib
left join Qualifications q
on q.QualificationName = ib.QualificationName
	and q.QualificationRef = ib.QualificationRef
where q.QualificationID is null


set identity_insert [Qualifications_new] off


update [Qualifications_new]
set AptisQualID = ib.qualificationid
from [Qualifications_new] q
inner join [dbo].[SA_ShrededQuals] ib
on q.QualificationName = ib.QualificationName
	and q.QualificationRef = ib.QualificationRef
where AptisQualID in (172,235)

select *
from [Qualifications_new]
--where AptisQualID is null
order by QualificationName

select *
from Qualifications_new
where AptisQualID in (172,235)


/****** Script for SelectTopNRows command from SSMS  ******/
SELECT TOP (1000) [QualificationID]
      ,[QualificationRef]
      ,[QualificationName]
      ,[AptisQualID]
  FROM [BC_TPM_Extract_05082017].[dbo].[Qualifications_import]
  where QualificationName in ('Aptis Commercial Listening','Aptis Commercial Reading Writing Listening Speaking','Aptis Commercial Reading Listening Speaking','Aptis Commercial Reading Writing Listening','Aptis Commercial Reading Writing Speaking','Aptis Commercial Listening Speaking','Aptis Commercial Writing Listening','Aptis Commercial Reading Speaking','Aptis Commercial Reading Writing','test aug 21','Aptis Adv P+P Test','Aptis Malaysia Advanced Package 1','Aptis Malaysia Advanced Package 2','Aptis General KSA','Uzbekistan Teachers')