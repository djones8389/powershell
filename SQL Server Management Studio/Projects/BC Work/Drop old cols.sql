/*
alter table Centres
	drop column [CentreID]
GO
exec sp_rename 'Centres.NewCentreID','CentreID','COLUMN'
GO
alter table Candidates
	drop column [CandidateID]
GO
exec sp_rename 'Candidates.[NewCandidateID]','CandidateID','COLUMN'
GO
alter table [dbo].[TestPackages]
	drop column [CentreID]
GO
exec sp_rename 'TestPackages.Newcentreid','CentreID','COLUMN'
GO
exec sp_rename 'CandidateSchedules.NewCandidateID','CandidateID','COLUMN'
GO
*/


use BC_TPM_Extract

select  *
from CandidateSchedules CS

inner join CandidateExams CE on CE.CandidateScheduleID = CS.ScheduledPackageCandidateId
inner join TestPackages TP on TP.ScheduledPackageID = CS.ScheduledPackage_ScheduledPackageId
inner join Centres C on C.CentreID = TP.CentreID
inner join Packages P on P.PackageID = TP.PackageID
inner join PackageExams PE ON PE.PackageExamID = CE.PackageExamId and PE.PackageID = P.PackageID
INNER JOIN Qualifications Q on Q.QualificationID =  tp.QualificationID
INNER JOIN Candidates CA on CA.CandidateID = CS.CandidateID

where keycode IN ('XR9PNB99','JYYBGH99','DXCHY999','7LBHKB99','G7VWKJ99')

where tp.centreid = 205