SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

select ScheduledExamRef
	, SurpassCandidateRef
	, FirstName
	, LastName
	, CenterName
	, spc.PackageScore
	, spce.Score
	, QualificationId
	, QualificationName
	, QualificationRef
	--, SurpassExamId
	, SurpassExamRef
	, pack.Name
	, PackageDate
from ScheduledPackageCandidateExams as SPCE

	inner join ScheduledPackageCandidates as SPC
	on SPC.ScheduledPackageCandidateId =  SPCE.Candidate_ScheduledPackageCandidateId
	inner join ScheduledPackages as SP
     on SP.ScheduledPackageId =  SPC.ScheduledPackage_ScheduledPackageId
    inner join [dbo].[PackageExams] pe
    on pe.[PackageExamId] = SPCE.[PackageExamId]
    inner join [dbo].[Packages] pack
    on pack.[PackageId] = pe.[PackageId]
where ScheduledExamRef in ('ZD5ATN01','VZ3UC301','AELCVK01','QVA84B01','RQPK2S01','GCVH2F01','ZP5G3N01','VD2W9801','3LUJYK01','YAT4AG01','JLUDZ401','QBP7W901','5FRDTU01','MLURDT01','HAEVL601','QDPMEF01','G3V68601','GQER8A01','75C3TB01','M8Z56A01','38NYX801','UMUMKA01','SULB4701','AHJBWZ01','XNDC7K01','M4HJCN01','TGGM8D01','3VGUD801','PHG3FY01','VDCD3C01','73QNHB01','NVNYZ501','3QABFJ01','RCP2GC01','W6KE8P01','4S9XN701','QCGUAY01','7SEVX601','7JD3G301','CYCWZM01','QF6WQ601','BBSSSG01','39H85501','82URLB01','TCPD3R01','RM92SH01','2FZFG401','CD5SWF01','AWMUH201','34R4QD01','U6LA9801','ZJFR8R01','PU83G601','YKKFHM01','J3FHFG01','KNN4VJ01','KFKG6H01','AXD3X801','KWTCP401','LYDGS201','CN9QHB01','SUB4DB01','W29FW601','975YX201','QMDPKB01','NB656N01','79EXXK01','EDFHRE01','EM2PP701','UR4LUD01','UXNADF01','SQXGXK01','6LWKWU01','3MBFD901','7YV7E201','DYTFV801','AF7REY01','YS8Y4301','TZTDA401','KD38F301','7LPRAX01','G5L74901','DRR73H01','QY4LYE01','YDJZ3601','5BJNRE01','YPUZ9301','FEAYGQ01','NZW4AG01','RXJSU401','622PEX01','E3K96Y01','M63DZX01','J7Q4SF01','ENGD4201','Z7DFPL01','64ZNP501','VVK7KQ01','DNUKB401','APS32Z01')
	order by ScheduledExamRef