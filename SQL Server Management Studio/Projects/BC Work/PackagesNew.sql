SELECT distinct
	p.*
	, q.QualificationID
--into [PackagesNew]
FROM [BC_TPM_Extract].[dbo].[TestPackageInfo] TPI
left join Packages P
on p.PackageId = tpi.packageid
left join CandidateSchedules cs
on cs.ScheduledPackageId = tpi.packageid
left join Qualifications q
on q.QualificationName = tpi.[QualificationName]
	and q.QualificationRef = tpi.[Qualificationref]
	order by 1
where TPI.[PackageId] = 106

--create nonclustered index [ix_qualnamequalref] on [TestPackageInfo] ([QualificationName], [QualificationRef])

SELECT ScheduledPackageID
	, tp.QualificationID
	, PackageID
	, q.*
FROM [BC_TPM_Extract].[dbo].[TestPackages] TP
inner join Qualifications q
on q.QualificationID = tp.QualificationID
where TP.[PackageId] = 106

  --create clustered index [ix_ScheduledPackageCandidateId] on [TestPackageInfo] ([ScheduledPackageCandidateId])