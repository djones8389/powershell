use BC_TPM_Extract
--USE [CG_TPM_Extract]
DELETE A
--SELECT A.*
FROM (
SELECT *
	, ROW_NUMBER () OVER (PARTITION BY ID, forename, surname, middlename, dob, gender, addressline1, addressline2, cast(town as nvarchar(MAX)), cast(country as nvarchar(MAX)), postcode, telephone, cast(email as nvarchar(MAX)), AccountCreationDate,AccountExpiryDate ORDER BY ID) R
FROM Candidates
) A
where R > 1
--ORDER BY 1;


--SELECT *
--INTO [Candidates_With_Duplicates]
--FROM Candidates


UPDATE Candidates
set AddressLine1 = REPLACE(AddressLine1, ',','')
	, AddressLine2 = REPLACE(AddressLine2, ',','')
	, CandidateRef = REPLACE(CandidateRef, ',','')
	, [Forename] = REPLACE([Forename], ',','')
	, MiddleName = REPLACE(MiddleName, ',','')
	, Surname = REPLACE(Surname, ',','')

UPDATE Candidates
set AddressLine1 = REPLACE(AddressLine1, '"','')
	, AddressLine2 = REPLACE(AddressLine2, '"','')
	, CandidateRef = REPLACE(CandidateRef, '"','')
	, [Forename] = REPLACE([Forename], '"','')
	, MiddleName = REPLACE(MiddleName, '"','')
	, Surname = REPLACE(Surname, '"','')

SELECT A.*
FROM (
SELECT ID
	, COUNTRY
	, County
	--, ROW_NUMBER() OVER(PARTITION BY ID , COUNTRY , County order by County DESC) R
	, ROW_NUMBER () OVER (PARTITION BY ID, forename, surname, middlename, dob, gender, addressline1, addressline2, cast(town as nvarchar(MAX)),  postcode, telephone, cast(email as nvarchar(MAX)), AccountCreationDate,AccountExpiryDate ORDER BY ID) R 
FROM Candidates
) A
where R > 1
order by 1


--cast(country as nvarchar(MAX))

--begin tran
	UPDATE Candidates
	Set Country = cast(D.ID as nvarchar(MAX))
	FROM Candidates C
	INNER JOIN CountryLookUpTable D
	On cast(D.Country as nvarchar(MAX)) = cast(C.Country as nvarchar(MAX))
	Where ISNUMERIC(cast(C.Country as nvarchar(max))) = 0

	UPDATE Candidates
	Set County =cast(D.ID as nvarchar(MAX))
	FROM Candidates C
	INNER JOIN CountyLookUpTable D
	On cast(D.County as nvarchar(MAX)) = cast(C.County as nvarchar(MAX))
	Where ISNUMERIC(cast(C.County as nvarchar(max))) = 0
--rollback


SELECT *
FROM Candidates
where ISNUMERIC(cast(Country as nvarchar(max))) = 0
	or ISNUMERIC(cast(County as nvarchar(max))) = 0


SELECT log_reuse_wait_desc
FROM SYS.Databases
where name = db_NAME()
