SELECT *
FROM Candidates
where AddressLine1 like '%"%'
	or AddressLine2 like '%"%'
	or CandidateRef like '%"%'
	or [Forename] like '%"%'
	or Surname like '%"%'
	or MiddleName like '%"%'



UPDATE Candidates
set AddressLine1 = REPLACE(AddressLine1, ',','')
	, AddressLine2 = REPLACE(AddressLine2, ',','')
	, CandidateRef = REPLACE(CandidateRef, ',','')
	, [Forename] = REPLACE([Forename], ',','')
	, MiddleName = REPLACE(MiddleName, ',','')
	, Surname = REPLACE(Surname, ',','')

UPDATE Candidates
set AddressLine1 = REPLACE(AddressLine1, '"','')
	, AddressLine2 = REPLACE(AddressLine2, '"','')
	, CandidateRef = REPLACE(CandidateRef, '"','')
	, [Forename] = REPLACE([Forename], '"','')
	, MiddleName = REPLACE(MiddleName, '"','')
	, Surname = REPLACE(Surname, '"','')