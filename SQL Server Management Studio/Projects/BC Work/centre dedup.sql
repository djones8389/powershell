DELETE A
FROM (
SELECT  [ID]
      ,[CentreName]
      ,[CentreCode]
      ,[AddressLine1]
      ,[AddressLine2]
      ,[Town]
      ,[County]
      ,[Country]
      ,[PostCode]
	  , ROW_NUMBER() OVER (
			PARTITION BY [ID]
			,[CentreName]
			,[CentreCode]
			--,[AddressLine1]
			--,[AddressLine2]
			--,CAST(Town AS NVARCHAR(MAX))
			--,CAST(Country AS NVARCHAR(MAX))
		   -- ,[PostCode] 
		  
		  ORDER BY CAST(County AS NVARCHAR(MAX)) ) R
  FROM [BCCG_TPM_Extract].[dbo].[Centres]

) A
WHERE R > 1

--UPDATE Centres
--Set Country = D.ID
--FROM Centres C
--INNER JOIN CountryLookUpTable D
--On cast(D.Country as nvarchar(MAX)) = cast(C.Country as nvarchar(MAX))
--Where ISNUMERIC(cast(C.Country as nvarchar(MAX))) = 0;

--UPDATE Centres
--Set County = D.ID
--FROM Centres C
--INNER JOIN CountyLookUpTable D
--On cast(D.County as nvarchar(MAX)) = cast(C.County as nvarchar(MAX)) 
--Where ISNUMERIC(C.County) = 0;

--SELECT * FROM CENTRES
--SELECT *
--INTO [Centres_Backup]
--FROM  [BCCG_TPM_Extract].[dbo].[Centres]