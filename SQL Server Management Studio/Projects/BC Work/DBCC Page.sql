/*
	View wait information using activity monitor or sp_whoisactive
	This should tell you where the locking is:  (5, 1, 1701581100)
	DB_Name, FileGroup, ObjectID

	Take note of the SPID
*/


USE Surpass_SecureAssess

DECLARE @Spid TINYINT = 54;


/*Find which table has contention*/

DBCC traceon (3604)
go
dbcc page (Surpass_SecureAssess, 1,  5141002, 1)

/*
	The output shows the objectId : m_objId = 978102525 
	
	SELECT OBJECT_NAME(978102525)
	--Returns:   ExamSessionTable  --	
*/

--Store it:
DECLARE @TableName sysname = (SELECT OBJECT_NAME(978102525))

	

/*Currently running*/

SELECT a.session_id
	, b.text
	, DB_NAME(b.dbid) [DBName]
	, b.objectid
	, 'use ' + QUOTENAME(DB_NAME(b.dbid)) + ' SELECT [Name],  [type_desc] FROM sys.objects WHERE OBJECT_ID =' + CAST(b.objectid AS NVARCHAR(10))
	, a.most_recent_sql_handle
FROM sys.dm_exec_connections a
CROSS APPLY sys.dm_exec_sql_text(a.most_recent_sql_handle) b
	WHERE DB_NAME(b.dbid) = DB_NAME()
		and a.session_id = @Spid
		--and b.text LIKE '%ExamSessionTable%'

--sa_CANDIDATEEXAMSTATEMANAGEMENTSERVICE_GetExternallyDeliveredExams_sp	SQL_STORED_PROCEDURE


/*Checked cached plans*/

SELECT  TOP 1  *,
	 DB_NAME(b.dbid)
	, OBJECT_NAME(b.objectid)
	 ,b.objectid
	, b.query_plan
	, 'use ' + QUOTENAME(DB_NAME(b.dbid)) + ' SELECT [Name],  [type_desc] FROM sys.objects WHERE OBJECT_ID =' + CAST(b.objectid AS NVARCHAR(10))
FROM sys.dm_exec_cached_plans a
CROSS APPLY sys.dm_exec_query_plan(a.plan_handle) b
WHERE DB_NAME(b.dbid) = DB_NAME()
	AND OBJECT_NAME(b.objectid) = @TableName

