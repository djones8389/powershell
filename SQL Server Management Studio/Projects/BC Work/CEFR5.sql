SELECT A.*
FROM (
	select distinct
		Keycode
		, Pe.Name
		, Score
		, DefaultMinScore
		, EG.Code
		, (Score-DefaultMinScore) [Diff]
		,ROW_NUMBER () OVER (PARTITION BY Keycode ORDER BY (Score-DefaultMinScore) ASC) R
	from [dbo].[CandidateExams] as SPCE

		inner join CandidateSchedules as SPC
		on SPC.[CandidateScheduleID] =  SPCE.[CandidateScheduleID]

		inner join [dbo].[PackageExams] pe
		on pe.[PackageExamId] = SPCE.[PackageExamId]
			
		inner join [dbo].[Packages] p
		on p.PackageID = pe.PackageId
		
		left join ExamTypes ET
		on ET.ExamTypeId = PE.ExamTypeId
		
		left join [ExamGradeDescriptions] EGD
		on EGD.ExamTypeId = ET.ExamTypeId		

		left join PackageExamScoreBoundaries pes
		on pes.ExamGradeId = egd.ExamGradeId
			and pes.[PackageExamId] = pe.[PackageExamId] 
		
		left join [ExamGrades] EG
		on EG.Id = pes.[ExamGradeId] 

		where Keycode in ('47VUFP01')
		--where Keycode In  ('NY9XYM01','5B48C501','YRDJLN01','ENCUVW01')
		--where Keycode in ('C8JQDK01','246EW901','TTY2ZZ01','VB7UC801','AEEW3301','LCPXRQ01')
			AND (Score-DefaultMinScore) > 0
	) A
	where R = 1


	select * from CandidateExams where Keycode in ('47VUFP01')