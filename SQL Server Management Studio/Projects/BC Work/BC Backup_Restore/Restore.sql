USE [master];

if not exists (select 1 from sys.databases where name = 'BC_TPM_Extract_Import')
	CREATE DATABASE [BC_TPM_Extract_Import];
if not exists (select 1 from sys.databases where name = 'CG_TPM_Extract_Import')
	CREATE DATABASE [CG_TPM_Extract_Import];


RESTORE DATABASE [BC_TPM_Extract] 
FROM  DISK = N'T:\FTP\BC\DataPull_18thAug\BC_TPM_Extract.bak' 
WITH  FILE = 1,  MOVE N'Data' TO N'E:\Data\BC_TPM_Extract1.mdf',  
				 MOVE N'Log' TO N'L:\Logs\BC_TPM_Extract1.ldf'
, RECOVERY, NOUNLOAD, NOREWIND,  REPLACE, STATS = 5, MEDIAPASSWORD = 's_!85WrenukEtrut+4f5a#-a&ac*aKUs'


RESTORE DATABASE [CG_TPM_Extract] 
FROM  DISK = N'T:\FTP\BC\DataPull_18thAug\CG_TPM_Extract.bak' 
WITH  FILE = 1,  MOVE N'Data' TO N'E:\Data\CG_TPM_Extract1.mdf',  
				 MOVE N'Log' TO N'L:\Logs\CG_TPM_Extract1.ldf'
, RECOVERY, NOUNLOAD, NOREWIND,  REPLACE,  STATS = 5, MEDIAPASSWORD = 's_!85WrenukEtrut+4f5a#-a&ac*aKUs'

