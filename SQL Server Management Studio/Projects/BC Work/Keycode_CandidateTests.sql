SELECT [ScheduledPackageCandidateExamID]
      ,[ScheduledPackageCandidateID]
      ,[PackageExamID]
      ,[ExamVersionRef]
      ,[IsCompleted]
      ,[DateCompleted]
      ,[Score]
      ,[IsVoided]
      ,[IsRescheduled]
      ,[ExamCompletionDate]
  FROM [BC_TPM_Extract].[dbo].[CandidateTests] 
  where [ScheduledPackageCandidateID] = 68

   ALTER TABLE [CandidateTests]
	ALTER COLUMN [Keycode] nvarchar(20)

 -- ALTER TABLE [CandidateTests]
	--ADD [Keycode] nvarchar(10)

UPDATE [CandidateTests]
set Keycode = b.[ScheduledExamRef]
from [CandidateTests] a
inner join [dbo].[ScheduledPackageCandidateExams] b
on a.ScheduledPackageCandidateExamID = b.ScheduledPackageCandidateExamID

/*

select *
from [CandidateTests] a
inner join [dbo].[ScheduledPackageCandidateExams] b
on a.ScheduledPackageCandidateExamID = a.ScheduledPackageCandidateExamID


select top 100 *
from [CandidateTests] a
where ScheduledPackageCandidateID = 68

select top 100 *
from  [ScheduledPackageCandidateExams]
where [Candidate_ScheduledPackageCandidateId] = 68


*/