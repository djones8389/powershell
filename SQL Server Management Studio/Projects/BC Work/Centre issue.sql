USE BC_TPM_Extract

SELECT *
FROM CandidateSchedules
WHERE CandidateID = 577858
GO

SELECT *
FROM Qualifications
WHERE QualificationID IN (
		SELECT TOP 1 QualificationID
		FROM TestPackages
		WHERE packageid IN (
				SELECT PackageID
				FROM PackageExams
				WHERE PackageExamId IN (
						SELECT PackageExamId
						FROM CandidateExams
						WHERE CandidateScheduleID IN (
								SELECT CandidateScheduleID
								FROM CandidateSchedules
								WHERE CandidateID = 577858
								)
						)
				)
		)
GO

SELECT *
FROM Packages
WHERE PackageID IN (
		SELECT PackageID
		FROM PackageExams
		WHERE PackageExamId IN (
				SELECT PackageExamId
				FROM CandidateExams
				WHERE CandidateScheduleID IN (
						SELECT CandidateScheduleID
						FROM CandidateSchedules
						WHERE CandidateID = 577858
						)
				)
		)
GO

SELECT *
FROM CandidateExams
WHERE CandidateScheduleID IN (
		SELECT CandidateScheduleID
		FROM CandidateSchedules
		WHERE CandidateID = 577858
		)
GO

SELECT *
FROM PackageExams
WHERE PackageExamId IN (
		SELECT PackageExamId
		FROM CandidateExams
		WHERE CandidateScheduleID IN (
				SELECT CandidateScheduleID
				FROM CandidateSchedules
				WHERE CandidateID = 577858
				)
		)
GO

SELECT *
FROM CEFR
WHERE keycode IN (
		SELECT keycode
		FROM CandidateExams
		WHERE CandidateScheduleID IN (
				SELECT CandidateScheduleID
				FROM CandidateSchedules
				WHERE CandidateID = 577858
				)
		)
GO

SELECT *
FROM TestPackages
WHERE ScheduledPackageID = 83549
GO

