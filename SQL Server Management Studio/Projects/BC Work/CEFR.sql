
--ExamGradeDescriptions from PackageExamScoreBoundaries inner joined with PackageExam.


  select *
	--egd.*
	--,Keycode
	--, Ce.score
	--, eg.*
  from [dbo].[PackageExams_Full] pe
  left join PackageExamScoreBoundaries pes
  on pes.[PackageExamId] = pe.[PackageExamId]
	and pe.examtypeid = pes.examgradeid
  inner join ExamGradeDescriptions egd
  on egd.[ExamGradeId] = pes.[ExamGradeId]
	and pes.examgradeid = egd.[ExamTypeId]
  inner join [dbo].[CandidateExams] CE
  on CE.PackageExamId = pe.PackageExamId
 inner join [dbo].[ExamGrades] EG
 on EG.Id = egd.ExamGradeId
	 where PE.PackageID = 26
	and PE.PackageExamId in (87,49,50,48)
   and Keycode in ('NY9XYM01','5B48C501','YRDJLN01','ENCUVW01')


select * from [dbo].[PackageGrades] where PackageId = 26

select Keycode
	,pe.*

from [CandidateExams] ce
inner join [CandidateSchedules] cs on cs.CandidateScheduleID = ce.CandidateScheduleID
inner join TestPackages TP on TP.ScheduledPackageID = CS.ScheduledPackageId
inner join Candidates ca on ca.CandidateID = cs.CandidateID
inner join Centres C on C.CentreID = TP.CentreID
inner join Packages P on P.PackageID = TP.PackageID
inner join PackageExams PE ON PE.PackageExamID = CE.PackageExamId and PE.PackageID = P.PackageID
INNER JOIN Qualifications Q ON Q.QualificationID = tp.QualificationID
  inner join [PackageGrades] PG
  on PE.PackageID = PG.PackageID
	and PE.[ExamTypeId] = PG.ExamGradeId
INNER JOIN [dbo].[ExamTypes] ET
on ET.ExamTypeId = PG.[ExamGradeId]
where Keycode in ('NY9XYM01','5B48C501','YRDJLN01','ENCUVW01')




/*select *	
	--, case  [IsAdjustComponentScoreBoundariesActivated]
	--	when 1 then e
  from [dbo].[PackageExams_full] PE
  inner join [PackageGrades] PG
  on PE.PackageID = PG.PackageID
	and PE.[ExamTypeId] = PG.ExamGradeId
  left join [PackageExamScoreBoundaries] PES
	on PES.packageexamid = pe.PackageExamId
		and pes.examGradeID = pg.ExamGradeId
  where PE.PackageID = 26
	and PE.PackageExamId in (87,49,50,48)
	order by 1

*/