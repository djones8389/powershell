/****** Script for SelectTopNRows command from SSMS  ******/
SELECT p.*
	, q.QualificationID
  FROM [BC_TPM_Extract].[dbo].[TestPackageInfo] tpi
  left join Qualifications q
  on q.QualificationName = tpi.QualificationName
	and q.QualificationRef = tpi.QualificationRef
  left join Packages p 
  on p.PackageId = tpi.packageid
	and p.SurpassQualificationId = tpi.[SurpassQualID]
	order by 1

	--4qC7S1f6u0DsToeufuGPpjvQ6bhx65S4

/****** Script for SelectTopNRows command from SSMS  ******/
SELECT distinct p.*
	, q.QualificationID
into [Packages_new]
  FROM [BC_TPM_Extract].[dbo].Packages p 
left join [TestPackageInfo] tpi
  on p.PackageId = tpi.packageid
	and p.SurpassQualificationId = tpi.[SurpassQualID]
left join Qualifications q
  on q.QualificationName = tpi.QualificationName
	and q.QualificationRef = tpi.QualificationRef
order by 1
  
  /*
 tpi.[ScheduledPackageCandidateId]
      ,tpi.[Keycode]
      ,tpi.[SurpassQualID] [AptisQualID]
	  ,q.QualificationID
      ,tpi.[PackageId]
      ,tpi.[QualificationName]
      ,tpi.[QualificationRef]
      ,tpi.[PackageName]
	  , 
	  */