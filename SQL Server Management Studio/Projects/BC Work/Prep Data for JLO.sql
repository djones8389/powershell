use master

create database [TPM_ToSend]
go

USE [BC_TPM_Extract]

select 'select * INTO [TPM_ToSend].[dbo].' + quotename(NAME) + ' FROM ' + quotename(NAME) +';'
from sys.tables
where name not like 'xx%'
order by 1


select * 
INTO [TPM_ToSend].[dbo].[CandidateExams] 
FROM [CandidateExams];
select * INTO [TPM_ToSend].[dbo].[Candidates] FROM [Candidates];
select * INTO [TPM_ToSend].[dbo].[CandidateSchedules] FROM [CandidateSchedules];
select * INTO [TPM_ToSend].[dbo].[CentreQualLookup] FROM [CentreQualLookup];
select * INTO [TPM_ToSend].[dbo].[Centres] FROM [Centres];
select * INTO [TPM_ToSend].[dbo].[ExamGradeDescriptions] FROM [ExamGradeDescriptions];
select * INTO [TPM_ToSend].[dbo].[ExamGrades] FROM [ExamGrades];
select * INTO [TPM_ToSend].[dbo].[ExamTypes] FROM [ExamTypes];
select * INTO [TPM_ToSend].[dbo].[ExamVersionScalings] FROM [ExamVersionScalings];
select * INTO [TPM_ToSend].[dbo].[PackageExams] FROM [PackageExams];
select * INTO [TPM_ToSend].[dbo].[PackageExamScoreBoundaries] FROM [PackageExamScoreBoundaries];
select * INTO [TPM_ToSend].[dbo].[PackageGrades] FROM [PackageGrades];
select * INTO [TPM_ToSend].[dbo].[Packages] FROM [Packages];
select * INTO [TPM_ToSend].[dbo].[Qualifications] FROM [Qualifications];
select * INTO [TPM_ToSend].[dbo].[TestPackages] FROM [TestPackages];