select n.*
	, o.id [AptisCentreID]
from [BC_TPM_Extract]..Centres N
inner join BC_TPM_Extract..xxSACentres O
on n.CentreName = o.centrename
	and n.CentreCode = o.centrecode