--select *
--from CandidateSchedules
--where CandidateID = 29161

use [BC_TPM_Extract];

select *
into #missingids
from CandidateSchedules
where CandidateID NOT IN (

	select N.CandidateID
	from Candidates N 
	INNER JOIN (
		select id	
			, CandidateRef
			, AddressLine1
			, AddressLine2
			, Forename
			, Middlename
			, Surname
			, DOB
		from [BC_TPM_Extract].[dbo].[xxFullUsers]
	) O
	on  isnull(N.FirstName,0) =  isnull(O.Forename,0)
		and isnull(N.LastName,0) =  isnull(o.Surname,0)
		and isnull(N.DOB,0) =  isnull(O.DOB,0)
		and isnull(N.Middlename,0) = isnull(O.Middlename,0)
		and isnull(N.CandidateRef,0) = isnull(O.CandidateRef,0)
		and isnull(n.AddressLine1,0) = isnull(o.AddressLine1,0)    
		and isnull(n.AddressLine2,0) = isnull(o.AddressLine2,0) 
 ) 


--where CandidateID = 29161




sp_whoisactive




--where Forename = 'Monika' and Surname = 'Zarat'

--UNION

--select UserId
--	, CandidateRef	
--	,AddressLine1
--	,AddressLine2
--	, Forename
--	, Middlename
--	, Surname
--	, DOB
--from [BC_TPM_Extract_old].[dbo].[xxWAREHOUSE_UserTable]
--where Forename = 'Monika' and Surname = 'Zarat'