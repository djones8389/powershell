select N.*
	, O.QualificationID [AptisQualID]
from [BC_TPM_Extract]..Qualifications N
INNER JOIN [BC_TPM_Extract_old]..Qualifications O
on N.QualificationName = O.QualificationName
	and N.QualificationRef = O.QualificationRef