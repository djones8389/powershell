--TRUNCATE TABLE TPM_Centres
--INSERT TPM_Centres([ID],CentreID, CentreName, CentreCode, AddressLine1, AddressLine2, County, Country, PostCode, [version], Town)
--SELECT [ID]
--	, [CentreID]
--      ,[CentreName]
--      ,[CentreCode]
--      ,[AddressLine1]
--      ,[AddressLine2]
--      ,[County]
--      ,[Country]
--      ,[PostCode]
--      ,[version]
--      --,[OriginatorID]
--      ,[Town]
--INTO TPM_Centres
--  FROM [BC_TPM_Extract].[dbo].[WAREHOUSE_CentreTable]  

--  DROP TABLE TPM_Centres

SELECT *
from TPM_Centres
order by centreid

DELETE
FROM TPM_Centres
where ID IN (
select a.[ID]
FROM (
select [ID]
	  ,[CentreID]
      ,[CentreName]
      ,[CentreCode]
      ,[AddressLine1]
      ,[AddressLine2]
      ,[County]
      ,[Country]
      ,[PostCode]
      ,[version]
	  ,[Town]
	  , ROW_NUMBER() OVER (partition by centreid,centrename order by centreid) R
FROM TPM_Centres
) a
where R <> 1
)
--where centreid = 41
 -- ALTER TABLE TPM_Centres
 -- drop column [OriginatorID]

 -- BEGIN TRAN

 -- DELETE
 -- FROM TPM_Centres
 -- where TPM_Centres.CentreID NOT IN (
	--SELECT [CentreID]
	--FROM [WAREHOUSE_CentreTable]
 -- )

 -- ROLLBACK