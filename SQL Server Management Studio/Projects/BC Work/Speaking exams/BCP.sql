USE [SANDBOX_BRITISHCOUNCIL_SecureAssess]

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

IF OBJECT_ID('tempdb..#test') IS NOT NULL DROP TABLE #test;

SELECT --TOP (8000)
	DISTINCT
	 wesdt.ID [DocID]
	, wests.Country
    , wests.Section
	, wests.examVersionName		
	, wesdt.itemId
	, wesirt.ItemMark
	, tpm.CEFR	
	, wests.examSessionId
	
INTO #test
	--SUM(CAST(DATALENGTH(wesdt.Document) AS BIGINT))/1024/1024 [MB]
FROM (

	SELECT
		examSessionId
		,examVersionName
		,KeyCode
		,country
		, userMark		
		, section.item.value('@id','nvarchar(20)') Item
		, exam.section.value('@id','tinyint') Section
	FROM WAREHOUSE_ExamSessionTable_Shreded 

		cross apply resultData.nodes('exam/section') exam(section)
		cross apply exam.section.nodes('item') section(item)
		
	WHERE qualificationName = 'International Comparisons Research'
		AND examName = 'Speaking'
		and previousExamState != 10
 ) wests
INNER JOIN [WAREHOUSE_ExamSessionDocumentTable] wesdt
ON wesdt.warehouseExamSessionID = wests.examSessionId
	and substring(wesdt.itemId, 0, charindex('S',wesdt.itemId)) = wests.Item
LEFT JOIN dbo.WAREHOUSE_ExamSessionItemResponseTable wesirt
ON wesirt.WAREHOUSEExamSessionID = wesdt.warehouseExamSessionID
	AND wesirt.ItemID = SUBSTRING(wesdt.itemId, 0, CHARINDEX('S',wesdt.itemId))
LEFT JOIN SANDBOX_BritishCouncil_TestPackage..[CEFR] TPM
on TPM.Keycode = wests.KeyCode collate SQL_Latin1_General_CP1_CI_AS

--SELECT SUM(cast(DATALENGTH(b.document) as bigint))/1024/1024/1024 [GB]
--from #test a
--inner join WAREHOUSE_ExamSessionDocumentTable b (READUNCOMMITTED)
--on a.DocID = b.ID
--3524P3710S3C1T1
SELECT a.DocID
	, a.Country
	, a.Section
	, a.examVersionName
	, substring(a.itemId, 0, CHARINDEX('C1T1',a.itemId))
	, a.ItemMark
	, a.CEFR
	, a.examSessionId
	, candidateRef
FROM #test a
inner join WAREHOUSE_ExamSessionTable_Shreded wests
on wests.examSessionId = a.examSessionId
order by a.examSessionId


DECLARE @FilePath NVARCHAR(200) = '\\430091-bc-file\contentshare\';
DECLARE @Servername sysname = (SELECT @@SERVERNAME)

SELECT --TOP 100
	'bcp "select document from [britishcouncil_secureassess].dbo.[WAREHOUSE_ExamSessionDocumentTable] where id = ' +CAST([DocID] AS NVARCHAR(8)) +'" queryout "' + @FilePath 
	+ 'International Comparisons Research - Speaking\' + a.Country + '_T' +
		 CAST(Section AS NVARCHAR(8)) + '_' + a.examVersionName +  '_' + CAST(ItemMark AS NVARCHAR(8)) + '_' + CAST(CEFR AS NVARCHAR(8)) + '_' + CAST([DocID] AS NVARCHAR(8))
			+ '.mp3" -T -c -S ' + @Servername collate SQL_Latin1_General_CP1_CI_AS
			,ItemMark
FROM #test a
--inner join WAREHOUSE_ExamSessionTable_Shreded b
--on a.examSessionId = b.examSessionId
--WHERE DocID is not null
--where examSessionId = 768381
--a.Country != 'VI'
	
	order by examSessionId asc;
--SELECT DISTINCT country FROM #test

/*Dup documents*/

--bcp "select document from [WAREHOUSE_ExamSessionDocumentTable] where id = 556801" queryout \\430091-bc-file\contentshare\International Comparisons Research - Speaking\Egypt_T2_ICR Version 1_0_A0_963576.mp3 -T -c -S 704276-PREVSTA1
--bcp "select document from [WAREHOUSE_ExamSessionDocumentTable] where id = 556795" queryout \\430091-bc-file\contentshare\International Comparisons Research - Speaking\Egypt_T2_ICR Version 1_0_A0_963576.mp3 -T -c -S 704276-PREVSTA1



/*
SELECT resultData
	, substring(wesdt.itemId, 0, charindex('S',wesdt.itemId))
FROM WAREHOUSE_ExamSessionTable west
inner join [WAREHOUSE_ExamSessionDocumentTable] wesdt
on west.id = wesdt.warehouseExamSessionID
where west.id = 1030357
	order by 2

SELECT
		candidateRef
		, KeyCode
		--, ExternalReference
		--, PreviousExamState
	    , exam.section.value('@id','tinyint') Section
		, section.item.value('@id','nvarchar(20)') Item
		--, item.mark.value('@mark','tinyint') mark
		--, item.mark.value('@maxMark','tinyint') maxMark
		,DENSE_RANK() OVER (PARTITION BY examSessionID, exam.section.value('@id','tinyint') ORDER BY exam.section.value('@id','tinyint'), section.item.value('@id','nvarchar(20)')) AS [Question No.]
	FROM WAREHOUSE_ExamSessionTable_Shreded
	cross apply resultData.nodes('exam/section') exam(section)
	cross apply exam.section.nodes('item') section(item)
	--cross apply section.item.nodes('mark') item(mark)
	WHERE WAREHOUSE_ExamSessionTable_Shreded.examSessionId = 1030357



SELECT TOP 10
	'bcp "select document from [WAREHOUSE_ExamSessionDocumentTable] where id = ' +CAST([DocID] AS NVARCHAR(8)) +'" queryout ' + @FilePath 
	+ 'International Comparisons Research - Speaking\' + a.Country +
		 CAST(itemId AS NVARCHAR(8)) + '\' + '_' + CAST(itemId AS NVARCHAR(8)) + '_' + CAST(ItemMark AS NVARCHAR(8)) + '_' + CAST(a.userMark AS NVARCHAR(8))	
			+ '-T -c -S' + @Servername
			, itemId
			, a.examSessionId
			, resultData
FROM #test a
inner join WAREHOUSE_ExamSessionTable_Shreded b
on a.examSessionId = b.examSessionId
WHERE a.Country != 'VI'

*/