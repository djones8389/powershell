USE [BC_TPM_Extract]
GO

/****** Object:  Table [dbo].[Candidates]    Script Date: 31/03/2017 08:55:28 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
--drop TABLE [dbo].[Candidates2]
CREATE TABLE [dbo].[Candidates2](
	[CandidateID] [bigint] identity(1,1)  ,
	[CandidateRef] [nvarchar](max) NULL,
	[uln] [nvarchar](max) NULL,
	[FirstName] [nvarchar](max) NULL,
	[MiddleName] [nvarchar](max) NULL,
	[LastName] [nvarchar](max) NULL,
	[Gender] [char](1) NULL,
	[AddressLine1] [nvarchar](max) NULL,
	[AddressLine2] [nvarchar](max) NULL,
	[Town] [nvarchar](max) NULL,
	[County] [nvarchar](max) NULL,
	[Country] [nvarchar](max) NULL,
	[PostCode] [nvarchar](max) NULL,
	[email] [nvarchar](max) NULL,
	[Telephone] [nvarchar](max) NULL,
	[Retired] [int]  NULL,
	[DOB] [smalldatetime] NULL,
	[AccountExpiryDate] [smalldatetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO


set identity_insert [Candidates2] on 

insert [Candidates2] (CandidateID, CandidateRef, uln, FirstName, MiddleName, LastName, Gender, AddressLine1, AddressLine2, Town, County, Country, PostCode, email, Telephone, Retired, DOB, AccountExpiryDate )
select CandidateID, CandidateRef, uln, FirstName, MiddleName, LastName, Gender, AddressLine1, AddressLine2, Town, County, Country, PostCode, email, Telephone, Retired, DOB, AccountExpiryDate 
from [Candidates] order by 1

set identity_insert [Candidates2] off

--create clustered index [IX_CandidateID] on [Candidates1] (CandidateID)