use BC_TPM_Extract

select  
	cs.*
from CandidateSchedules CS
inner join CandidateExams CE on CE.CandidateScheduleID = CS.ScheduledPackageCandidateId
inner join [dbo].[ScheduledPackageCentreLookup] l on l.scheduledpackageid = CS.[ScheduledPackage_ScheduledPackageId]
inner join TestPackages_New tp on tp.ScheduledPackageID = CE.CandidateScheduleID
inner join New_Centres nc on nc.NewCentreID = tp.NewCentreID
--inner join [New_Centres] C on c.[NewCentreID] = tp.[NewCentreID]
WHERE  [ScheduledExamRef] in ('26M7LL01','DWT8UN01','3VFEH301','4YQBMB01','NR5NSK01','55FHP501','KEPCHM01','DWT8UN01','BG69C401','XAN43M01','RPRDZT01','735JS701','QQ8JRK01','BYX4AR01','HQSQC901','7FLSRZ01','2P8ZGZ01','NGH4VR01','32DWV601','JVNZTC01','P7TEW501','W6EAVD01','HG8BH401','J943YN01','PWP5ZE01','C8K7CK01','D9URYL01','6HFYAH01','FCMLTU01','VRCH8301','72FVFC01','HGXXB401','C65GDC01','26M7LL01','QJRPAW01','BUYWG301','6TDJPP01')

--select * from New_Centres where Centrename = 'British Council - Warsaw'
--select * from [ScheduledPackageCentreLookup] l where centername = 'British Council - Warsaw'
--select * from TestPackages_New a inner join [ScheduledPackageCentreLookup] l on l.scheduledpackageid = a.scheduledpackageid where newcentreid = 34

select * from New_Centres where Centrename = 'British Council - Warsaw'
select * from [ScheduledPackageCentreLookup] l where centername = 'British Council - Warsaw'
select * 
from TestPackages_New
where newcentreid = 4


/*
UPDATE New_Centres
set

select *
from  New_Centres nc
inner join TestPackages_New tp
on nc.NewCentreID = tp.NewCentreID
inner join  [dbo].[ScheduledPackageCentreLookup] l 
on l.scheduledpackageid = tp.scheduledpackageid


begin tran

update TestPackages_New
set newcentreid = nc.newcentreid
from TestPackages_New a
inner join [ScheduledPackageCentreLookup] l
on l.[ScheduledPackageId] = a.[ScheduledPackageID]
inner join New_Centres nc
on nc.centreid = l.centerid
--where Centrename = 'British Council - Warsaw'


rollback




--select *
--from [ScheduledPackageCentreLookup] l
--inner join New_Centres nc 
--on nc.centrename = l.centername
--	and nc.centrecode = l.centerref
--where l.centerid = 34


--begin tran

--select * from New_Centres

--UPDATE New_Centres
--set centrename = centername
--	, centrecode = centerref
--FROM New_Centres NC
--inner join [ScheduledPackageCentreLookup] l
--on nc.centrename = l.centername
--	and nc.centrecode = l.centerref

--select * from New_Centres

--rollback

begin tran
UPDATE TestPackages_New
set [NewCentreID] = nc.newcentreid
FROM TestPackages_New TP
inner join CandidateExams CE
on tp.ScheduledPackageID = CE.CandidateScheduleID
inner join CandidateSchedules CS
on CE.CandidateScheduleID = CS.ScheduledPackageCandidateId
inner join [dbo].[ScheduledPackageCentreLookup] l 
on l.scheduledpackageid = CS.[ScheduledPackage_ScheduledPackageId]
inner join New_Centres nc on nc.CentreName = l.centername
	and nc.centrecode = l.centerref


rollback

*/