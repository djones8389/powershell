UPDATE [TPM_Import].[dbo].[Candidates26042017_Amend]
set country = o.Country

--SELECT  n.firstname,  n.LastName, n.CandidateRef, n.country
--	, o.firstname, o.LastName, o.CandidateRef, o.Country
FROM [TPM_Import].[dbo].[Candidates26042017_Amend] n
INNER JOIN [BC_TPM_Extract]..Candidates o
on o.CandidateRef = n.CandidateRef
	and o.FirstName = n.firstname
	and o.LastName = n.LastName
	and o.MiddleName = n.MiddleName
where  n.Country = '-1'
	and o.Country ! = '-1'

	and n.FirstName = 'Fami'
	and [AptisCandidateID]  in (484273,484275,484276,484277)
	

SELECT count(*)
FROM [TPM_Import].[dbo].[Candidates26042017_Amend]
where Country = '-1'

SELECT count(*)
FROM [BC_TPM_Extract]..Candidates 
where Country = '-1'




SELECT distinct country
FROM [TPM_Import].[dbo].[Candidates26042017_Amend]
SELECT distinct county
FROM [TPM_Import].[dbo].[Candidates26042017_Amend]


begin tran

SELECT candidateid, country, county
FROM [BC_TPM_Extract].[dbo].[Candidates_2] A
where ISNUMERIC(A.Country) = 0
	or ISNUMERIC (A.County) = 0

UPDATE [BC_TPM_Extract].[dbo].[Candidates_2]
set county = COUNTY.ID
	, country = COUNTRY.ID
FROM [BC_TPM_Extract].[dbo].[Candidates_2] A
inner join [SecureAssess].dbo.CountyLookupTable COUNTY
on COUNTY.COUNTY = A.county
INNER JOIN [SecureAssess].dbo.[CountryLookupTable] COUNTRY
on COUNTRY.COUNTRY = A.COUNTRY
where ISNUMERIC(A.Country) = 0
	and ISNUMERIC (A.County) = 0

SELECT candidateid, country, county
FROM [BC_TPM_Extract].[dbo].[Candidates_2] A
where ISNUMERIC(A.Country) = 0
	or ISNUMERIC (A.County) = 0

rollback



begin tran
update [BC_TPM_Extract].[dbo].[Candidates_2] 
set country = '185'
where country = 'Sri Lanka'

SELECT candidateid, country, county
FROM [BC_TPM_Extract].[dbo].[Candidates_2] A
where ISNUMERIC(A.Country) = 0
	or ISNUMERIC (A.County) = 0
rollback

--select *
--into  [BC_TPM_Extract].[dbo].[Candidates_270417_beforeCountryCounty]
--FROM [BC_TPM_Extract].[dbo].[Candidates_2]
/*

SELECT *
FROM [BC_TPM_Extract].[dbo].[Candidates_2] A

where ISNUMERIC(A.Country) = 0
	or ISNUMERIC (A.County) = 0

	*/