USE BC_TPM_Extract

SELECT b.Keycode
	, b.Name
	, cast(round(b.Score,0) as float) Score
	, eg.Code [CEFR]
	,DefaultScaleValue
FROM (
	SELECT a.*
	FROM (
	select 
		Keycode
		, Pe.Name
		, Score
		,EGD.*
	from [dbo].[CandidateExams] as SPCE

		inner join CandidateSchedules as SPC
		on SPC.[CandidateScheduleID] =  SPCE.[CandidateScheduleID]

		inner join [dbo].[PackageExams] pe
		on pe.[PackageExamId] = SPCE.[PackageExamId]
			
		inner join [dbo].[Packages] p
		on p.PackageID = pe.PackageId
		
		inner join ExamTypes ET
		on ET.ExamTypeId = PE.ExamTypeId
		
		left join [ExamGradeDescriptions] EGD
		on EGD.ExamTypeId = ET.ExamTypeId		


		left join PackageExamScoreBoundaries pes
		--on pes.ExamGradeId = ExamGradeId
			on pes.[PackageExamId] = pe.[PackageExamId] 
		
		inner join [ExamGrades] EG
		on EG.Id = pes.[ExamGradeId] 
		----on pes.PackageExamId = pe.PackageId
		----	 and pes.ExamGradeId = ExamGradeId
		--left join ExamGradeDescriptions egd
		--on pes.examgradeid = egd.[ExamGradeId]
		--	and pe.ExamTypeId = egd.ExamTypeId
		--	and egd.DefaultMinScore = pes.Value
		--		left join [dbo].[ExamGrades] EG
		--		on EG.Id in (egd.ExamGradeId)
		
			where Keycode In  ('NY9XYM01','5B48C501','YRDJLN01','ENCUVW01')
			--where Keycode In  ('246EW901','TTY2ZZ01')
				--and (Score-Value) > 0
	) a
	where R = 1
) B 
INNER JOIN ExamGrades EG
on EG.Id = ExamGradeId
 

SELECT *
FROM ExamGrades