/****** Script for SelectTopNRows command from SSMS  ******/
SELECT distinct *
  FROM [BC_TPM_Extract].[dbo].[BigCentres] BC
  inner join [Centres_ID] c
  on bc.centrename = c.centrename
	and bc.centrecode = c.centrecode

select * 
from [Centres_ID] c
left join [BigCentres] bc
  on bc.centrename = c.centrename
	and bc.centrecode = c.centrecode

where c.county is null
	order by 1


--begin tran

--  UPDATE [Centres_ID]
--  set County = bc.County
--	 , country = bc.country
--	 , town = bc.town
--	 , retired = bc.retired
--	 , centreadminuser = bc.primarycontactid
--	 , postcode = bc.postcode
--  FROM [dbo].[Centres_ID] C
--  INNER JOIN [BigCentres] BC
--  on bc.centrename = c.centrename
--	and bc.centrecode = c.centrecode
--where  c.County is null

--rollback