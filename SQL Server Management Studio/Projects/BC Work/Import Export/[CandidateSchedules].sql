/****** Script for SelectTopNRows command from SSMS  ******/
SELECT 
      [CandidateScheduleID]
	  ,[CandidateID]
      --,[SurpassCandidateId]
      --,[CandidateRef]
      --,[FirstName]
      --,[LastName]
      --,[MiddleName]
      --,[BirthDate]
      ,[IsCompleted]
      ,[DateCompleted]
      ,[PackageScore]
      ,[ScheduledPackageId]
      ,[IsVoided]
      ,[IsLocalScan]
      ,cast([TestPackageCompletionDate] as datetime) [TestPackageCompletionDate]
  FROM [BC_TPM_Extract].[dbo].[CandidateSchedules]
  order by 1;