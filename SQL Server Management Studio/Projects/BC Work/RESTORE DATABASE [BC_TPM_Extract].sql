use master

RESTORE DATABASE [BC_TPM_Extract] FROM DISK = N'T:\FTP\BC\BC_TPM_Extract.bak' 
	WITH FILE = 1, NOUNLOAD, NOREWIND, REPLACE, MEDIAPASSWORD = 's_!85WrenukEtrut+4f5a#-a&ac*aKUs'
	, STATS = 10
	, MOVE 'Data' TO N'E:\DATA\BC_TPM_Extract.mdf', MOVE 'Log' TO N'L:\LOGS\BC_TPM_Extract.ldf'; 
--1 GB of storage needed
