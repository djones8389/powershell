USE STG_SANDBOX_SQA_CPProjectAdmin


IF OBJECT_ID('tempdb..#Contracts') IS NOT NULL DROP TABLE #Contracts

CREATE TABLE #Contracts (
	ProjectName nvarchar(200)
	, projectID INT
	, forenameSurname nvarchar(200)
	, username nvarchar(100)
	, pageID nvarchar(15)
	, ID nvarchar(20)
	, Description nvarchar(20)	
	, randomthing2 INT NULL
	, ContractID  nvarchar(20)
	, SpecificationID int
	, Status nvarchar(1000)
)

BULK INSERT #Contracts
from  'C:\Users\977496-davej\Desktop\f3730 SQAcontacts_updated1.csv'
with (fieldterminator = ',', rowterminator = '\n')

DELETE FROM #Contracts where projectName IS NULL;
ALTER TABLE #Contracts DROP COLUMN [randomthing2];


SELECT distinct B.*
FROM 
(
SELECT a.projectname
	, a.projectid
	, a.forenameSurname
	, a.username
	--, a.pageid
	--, a.id
	, a.description
	, a.contractid 
	, a.specificationid
	, a.status [SQAWantItToBe]
	--, c.completionDate
	, case when c.completionDate IS NULL then 'Open' ELSE 'Closed' end as [StatusInCP]
	--,ISL.SpecificationName as SpecificationID
FROM #Contracts A
INNER JOIN Contract C
on C.ContractID = A.ContractID
	and C.ID = A.ID 
INNER JOIN ItemSpecificationLookup AS ISL 
ON ISL.ID = C.ItemSpecificationID
--INNER JOIN ContractTypeLookup AS CTL 
--ON CTL.ID = C.Type
--	and CTL.Description = A.Description
--where a.projectid = 1141
--	and a.username = '118518'

) B
where [SQAWantItToBe] <> [StatusInCP]

order by 1 desc