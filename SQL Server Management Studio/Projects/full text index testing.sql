/*
USE [master]
GO
RESTORE DATABASE [FULL_TEXT_TEST] FROM  DISK = N'E:\Backup\FULL_TEXT_TEST.bak' WITH  FILE = 1
,  MOVE N'FULL_TEXT_TEST' TO N'E:\DATA\FULL_TEXT_TEST.mdf'
,  MOVE N'FULL_TEXT_TEST_log' TO N'E:\Log\FULL_TEXT_TEST_log.ldf',  NOUNLOAD,  STATS = 5
, REPLACE


USE [FULL_TEXT_TEST];

INSERT questions (question_text)
select top 3280 question_text
from questions;

INSERT questions (question_text)
select question_text
from questions

INSERT questions (question_text)
select question_text
from questions

INSERT questions (question_text)
select question_text
from questions

INSERT questions (question_text)
select top 100000 question_text
from questions


select count(ID)
from questions;

*/


DROP  FULLTEXT INDEX 
	ON [dbo].[questions]

/*
CREATE FULLTEXT INDEX ON [dbo].[questions]
(
	[question_text]  Language 1033
)
KEY INDEX PK_questions
	on questions
	with CHANGE_TRACKING OFF, NO POPULATION
*/

use [FULL_TEXT_TEST]

SELECT c.fulltext_catalog_id
	,c.NAME AS fulltext_catalog_name
	,i.change_tracking_state
	--,i.object_id
	,OBJECT_SCHEMA_NAME(i.object_id) + '.' + OBJECT_NAME(i.object_id) AS [object_name]
	,f.num_fragments
	,f.fulltext_mb
	,f.largest_fragment_mb
	,100.0 * (f.fulltext_mb - f.largest_fragment_mb) / NULLIF(f.fulltext_mb, 0) AS fulltext_fragmentation_in_percent
FROM sys.fulltext_catalogs c
INNER JOIN sys.fulltext_indexes i ON i.fulltext_catalog_id = c.fulltext_catalog_id
INNER JOIN (
	-- Compute fragment data for each table with a full-text index
	SELECT table_id
		,COUNT(*) AS num_fragments
		,CONVERT(DECIMAL(9, 2), SUM(data_size / (1024. * 1024.))) AS fulltext_mb
		,CONVERT(DECIMAL(9, 2), MAX(data_size / (1024. * 1024.))) AS largest_fragment_mb
	FROM sys.fulltext_index_fragments
	GROUP BY table_id
	) f ON f.table_id = i.object_id
--where (100.0 * (f.fulltext_mb - f.largest_fragment_mb) / NULLIF(f.fulltext_mb, 0)) >= 10
--	and f.fulltext_mb >= 1

--sp_spaceused 'questions'

--15.8671875
--16248 KB

--alter  FULLTEXT catalog [questions] rebuild


--INSERT questions (question_text)
--select top 3280 question_text
--from questions

SELECT 
    avg(len([question_text]))
	  ,avg(DATALENGTH([question_text]))
	  ,SUM(DATALENGTH([question_text]))
  FROM [FULL_TEXT_TEST].[dbo].[questions]