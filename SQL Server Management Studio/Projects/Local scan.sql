USE [PRV_BritishCouncil_LocalScan]
GO

set statistics io on

select EntryID
	,CandidateID
	,ProductID
	,CentreID
	,CandidateNumber
	,PackingID
	,Candidates.DisplayName
	,AssessmentID
	,ComponentID
	,AvailabilityID
	,Assessment_DisplayName
	,Component_DisplayName
	,Availability_DisplayName
	,TFFormID
	,Centre_Reference
	,Centre_DisplayName
	,Candidates.DateOfBirth
	,A.Reference
	,ComponentVersionsName
	,Candidates.Reference
FROM (

SELECT dbo.Entries.EntryID
	,dbo.Entries.CandidateID
	,dbo.Entries.ProductID
	,dbo.Entries.CentreID
	,dbo.Entries.CandidateNumber
	,dbo.Entries.PackingID
	,dbo.Candidates.DisplayName
	,dbo.Products.AssessmentID
	,dbo.Products.ComponentID
	,dbo.Products.AvailabilityID
	,dbo.Assessments.DisplayName AS Assessment_DisplayName
	,dbo.Components.DisplayName AS Component_DisplayName
	,dbo.Availabilities.DisplayName AS Availability_DisplayName
	,dbo.Tests.TFFormID
	,dbo.Centres.Reference AS Centre_Reference
	,dbo.Centres.DisplayName AS Centre_DisplayName
	,dbo.Candidates.DateOfBirth
	,dbo.Entries.Reference
	,dbo.ComponentVersions.NAME AS ComponentVersionsName
	,dbo.Candidates.Reference AS Candidate_Reference
FROM dbo.Entries
INNER JOIN dbo.Candidates ON dbo.Entries.CandidateID = dbo.Candidates.ID
INNER JOIN dbo.Centres ON dbo.Entries.CentreID = dbo.Centres.CentreID
INNER JOIN dbo.Products ON dbo.Entries.ProductID = dbo.Products.ProductID
INNER JOIN dbo.Tests ON dbo.Products.ProductID = dbo.Tests.ProductID
INNER JOIN dbo.Assessments ON dbo.Products.AssessmentID = dbo.Assessments.AssessmentID
INNER JOIN dbo.Availabilities ON dbo.Products.AvailabilityID = dbo.Availabilities.AvailabilityID
INNER JOIN dbo.Components ON dbo.Products.ComponentID = dbo.Components.ComponentID
LEFT JOIN dbo.ComponentVersions ON dbo.Entries.ComponentVersionID = dbo.ComponentVersions.ComponentVersionID
	AND dbo.Components.ComponentID = dbo.ComponentVersions.ComponentID

LEFT JOIN dbo.Prints p
on p.EntryID = dbo.Entries.EntryID
	and p.TFFormID = dbo.tests.TFFormID
	and p.ProductID = dbo.Entries.ProductID
where p.EntryID IS NULL

) A
INNER JOIN Candidates 
ON Candidates.ID = a.CandidateID


--WHERE (
--		(
--			SELECT TOP (1) EntryID
--			FROM dbo.Prints AS p
--			WHERE (EntryID = dbo.Entries.EntryID)
--				AND (TFFormID = dbo.Tests.TFFormID)
--				AND (ProductID = dbo.Entries.ProductID)
--			) IS NULL
--		)


--CREATE NONCLUSTERED INDEX [IX_DJTEST] ON [PRV_BritishCouncil_LocalScan]..Candidates (
--	ID,
--	DisplayName,
--	DateOfBirth,
--	Reference
--)
--INCLUDE (
--	
--)

--DROP INDEX [IX_DJTEST] ON [PRV_BritishCouncil_LocalScan]..Candidates