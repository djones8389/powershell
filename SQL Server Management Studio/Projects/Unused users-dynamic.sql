DECLARE @Dynamic NVARCHAR(MAX) = '';

SELECT  @Dynamic += CHAR(13) + 
 'USE [' + Name + '];  

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
SELECT @@Servername [ServerName]
	, DB_Name() [DBName]
	, COUNT(ID) [Count]
FROM WAREHOUSE_UserTable
LEFT JOIN (

	SELECT WAREHOUSEUserID FROM WAREHOUSE_ExamSessionTable
	UNION
	' + CASE 
   WHEN EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME IN (N'WAREHOUSE_ExamSessionTable') AND COLUMN_NAME = N'WAREHOUSEUserID') THEN N'SELECT WAREHOUSEUserID FROM WAREHOUSE_ExamSessionTable UNION'
   ELSE N''
   END  
	+ CASE 
   WHEN EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME IN (N'WAREHOUSE_ExamSessionTable') AND COLUMN_NAME = N'AssignedModeratorUserID') THEN N'SELECT AssignedModeratorUserID FROM WAREHOUSE_ExamSessionTable'
   ELSE N''
  END + 
  
  + CASE 
   WHEN EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME IN (N'WAREHOUSE_ExamSessionTable') AND COLUMN_NAME = N'AssignedMarkerUserID') THEN N'SELECT AssignedMarkerUserID FROM WAREHOUSE_ExamSessionTable UNION'
   ELSE N''
   END  
	+ CASE 
   WHEN EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME IN (N'WAREHOUSE_ScheduledExamsTable') AND COLUMN_NAME = N'WAREHOUSECreatedBy') THEN N'SELECT WAREHOUSECreatedBy FROM WAREHOUSE_ScheduledExamsTable'
   ELSE N''
  END 
    + CASE 
   WHEN EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME IN (N'WAREHOUSE_ExamSessionDocumentInfoTable') AND COLUMN_NAME = N'Warehoused_UploadingUserID') THEN N'SELECT Warehoused_UploadingUserID FROM WAREHOUSE_ExamSessionDocumentInfoTable UNION'
   ELSE N''
   END  
	+ CASE 
   WHEN EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME IN (N'WAREHOUSE_ExamSessionDurationAuditTable') AND COLUMN_NAME = N'Warehoused_UserID') THEN N'SELECT Warehoused_UserID FROM WAREHOUSE_ExamSessionDurationAuditTable'
   ELSE N''
  END 
  + CASE 
   WHEN EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME IN (N'WAREHOUSE_ExamSessionItemResponseTable') AND COLUMN_NAME = N'MarkerResponseData') THEN N'SELECT 
	  y.value(''userId[1]'', ''int'') AS UserId
	FROM WAREHOUSE_ExamSessionItemResponseTable
	CROSS APPLY MarkerResponseData.nodes(''entries/entry[not(userId/text()=-1)]'') x(y)'
   ELSE N''
  END + '

	SELECT 
	  y.value(''userId[1]'', ''int'') AS UserId
	FROM WAREHOUSE_ExamSessionItemResponseTable
	CROSS APPLY MarkerResponseData.nodes(''entries/entry[not(userId/text()=-1)]'') x(y)

) A
 on A.WAREHOUSEUserID = WAREHOUSE_UserTable.id'

FROM SYS.Databases
where name = 'STG_EAL_SecureAssess'
	and state_desc = 'ONLINE';
	
PRINT(@Dynamic)
