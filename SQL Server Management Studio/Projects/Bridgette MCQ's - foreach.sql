DECLARE @ResultHolder TABLE (
	Instance sysname
	, DBName sysname
	, ProjectName nvarchar(100)
	, PageID nvarchar(30)
	, ComponentCount tinyint
);

SELECT @@ServerName

DECLARE @Dynamic nvarchar(MAX) = '';

SELECT @Dynamic +=CHAR(13) +
'SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

USE ' + quotename(name) + '

SELECT @@ServerName [Instance] 
	, DB_NAME() [DatabaseName]
	, Name [ProjectName]
	, PageID
	, [ComponentCount]
FROM Projectlisttable PLT
INNER JOIN (
		SELECT A.PageID	
			, COUNT(cast(C as int)) [ComponentCount]
		FROM (
		SELECT [ID]
			, SUBSTRING([ID],0,CHARINDEX(''S'',[ID])) [PageID]
			, SUBSTRING(ID, CHARINDEX(''S'', ID) + 1, CHARINDEX(''C'',ID) - CHARINDEX(''S'', ID) - 2 + Len(''C'')) [S]
			, SUBSTRING(ID, CHARINDEX(''C'', ID) + 1, CHARINDEX(''I'',ID) - CHARINDEX(''C'', ID) - 2 + Len(''I'')) [C]
		FROM ['+name+'].[dbo].[ItemMultipleChoiceTable] MCQ
		INNER JOIN (
			select 
				cast(ID as nvarchar(10)) + ''P'' + cast(project.page.value(''@ID'',''nvarchar(12)'') as nvarchar(10)) PageID
			from Projectlisttable
			cross apply ProjectStructureXml.nodes(''Pro//Pag'') project(page)
			where project.page.value(''@sta'',''int'') Between PublishSettingsXML.value(''(/PublishConfig/StatusLevelFrom)[1]'',''int'') and PublishSettingsXML.value(''(/PublishConfig/StatusLevelTo)[1]'',''int'')

				EXCEPT

			select 
				cast(ID as nvarchar(10)) + ''P'' + cast(project.page.value(''@ID'',''nvarchar(12)'') as nvarchar(10)) PageID
			from Projectlisttable
			cross apply ProjectStructureXml.nodes(''Pro/Rec//Pag'') project(page)
			where project.page.value(''@sta'',''int'') Between PublishSettingsXML.value(''(/PublishConfig/StatusLevelFrom)[1]'',''int'') and PublishSettingsXML.value(''(/PublishConfig/StatusLevelTo)[1]'',''int'')
		) P
		   on P.PageID = SUBSTRING([ID],0,CHARINDEX(''S'',[ID]))
	) A
	GROUP BY PAGEID
) MCQ
on PLT.ID = SUBSTRING(MCQ.PageID, 0, CHARINDEX(''P'',MCQ.PageID))
'
from sys.databases
where name like '%cpproject%' or name like '%content%' 

INSERT @ResultHolder
EXEC(@Dynamic);


SELECT *
FROM @ResultHolder
ORDER BY 1;