USE [SurpassTC];

--Grab Comms Key

DECLARE @CommsKey nvarchar(20) = (
       SELECT [CommunicationKey] 
       FROM [Prometric_SurpassLocal].[dbo].[LocalSettingsTable]
);

--Choose where you want files to be saved

DECLARE @SaveLocation nvarchar(30) = 'D:\F13087\'

--Produce commands for CMD

       SELECT
              'LocalDecryptor.exe --commskey='+@CommsKey+' --encryptedtext=' + [ItemResponseData].value('data(/p)[1]','nvarchar(MAX)')
                     + ' > '+ @SaveLocation + cast([ExamSessionID] as nvarchar(10))+'_'+Keycode+'_'+cast([ItemID] as nvarchar(12)) 
                           + '_V' + cast([ItemVersion]as nvarchar(5))+'.xml' [CmdLine]
       FROM [dbo].[ExamSessionItemResponseTable] ESIRT
       INNER JOIN ExamSessionTable EST ON EST.ID = ESIRT.ExamSessionID 
	   where KeyCode in ('Y4MJ6RK8','LFR3PNK8','GTRJ9GK8','JB66XDK8','MNRT7BK8','3LKY8RK8','3X7HLXK8','7KWF4JK8','LB6M3KK8')

       --ALTER TABLE [SurpassTC].[dbo].[ExamSessionItemResponseTable]
       --       ADD [Decrypted] XML;

SELECT examsessionid, itemid, itemversion, MarkerResponseData, decrypted [ItemResponseData]
from [ExamSessionItemResponseTable]
where Decrypted is not null