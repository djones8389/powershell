USE [Develop-MI-IB]
GO

--create function dbo.CalculateTotalDurationForAssessment(
--	@assessmentId int
--)
--returns @totalDuration table
--	( TotalDuration int not null )
--as
--begin


DECLARE @assessmentId int = 1

declare @durationMode int = 2
declare @assessmentDuration int = 60
declare @assessmentRules xml

declare @totalDuration table ( 
	TotalDuration int not null 
	)

select
	@durationMode = DurationMode,
	@assessmentDuration = AssessmentDuration,
	@assessmentRules = AssessmentRules
from AssessmentTable
where ID = @assessmentId

--if (@durationMode != 2 )
--begin
--	insert into @totalDuration( TotalDuration ) values ( @assessmentDuration )
--	return
--end;

;with RawData as (
	select
		a.value('@ID', 'int' ) SectionId,
		a.value('@Duration', 'int' ) Duration,
		a.value('@SectionType', 'varchar(50)') SectionType,
		a.value('@RequiredSections', 'int' ) RequiredSections,
		a.value('../@ID', 'varchar(50)') ParentId
	from @assessmentRules.nodes('//Section') a(a)
),
NonSelectorSectionDuration as (
	select
		sum(Duration) TotalDuration
	from RawData
	where ParentId is null
	and (SectionType = 'Section' or SectionType is null)
),
SectionSelectors as (
	select
		SectionId,
		RequiredSections
	from RawData
	where SectionType = 'Selector'
),
SectionSelectorSectionDuration as (
	select
		ss.SectionId,
		MIN(Duration) as Duration
	from SectionSelectors as ss
	join RawData as rd
		on rd.ParentId = ss.SectionId
	group by
		ss.SectionId
),
SectionSelectorTotalDuration as (
select
	sum( ss.RequiredSections * sssd.Duration ) as TotalDuration
from SectionSelectors as ss
join SectionSelectorSectionDuration as sssd
	on ss.SectionId = sssd.SectionId
)
insert into @totalDuration( TotalDuration ) 
select
	(NonSelectorSectionDuration.TotalDuration + SectionSelectorTotalDuration.TotalDuration ) as TotalDuration
from NonSelectorSectionDuration
cross join SectionSelectorTotalDuration

--return
--end;

select * from @totalDuration