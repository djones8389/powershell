use master

DECLARE @AutoGrow TABLE
(
	Name nvarChar(100)
	, fileID tinyint
	, filename nvarchar(1000)
	, filegroup nvarchar(100)
	, size nvarchar(100)
	, maxSize nvarchar(100)
	, growth  nvarchar(20)
	, usage nvarchar(30)

)
INSERT @AutoGrow
exec sp_MSforeachdb '

use [?];

IF (db_ID() > 4)
EXEC sp_helpfile  

'

SELECT C.name
	, a.size
	, b.size
	, max_size
	, maxSize
	, a.growth
	--, b.growth
	, b.type_desc
FROM @AutoGrow A
INNER JOIN sys.master_files B
ON A.filename = B.physical_name
inner join sys.databases c
on c.database_id = b.database_id
where (c.name not like '%content%' and c.name not like '%cppro%'
	and c.name not like '%secureassess%' and c.name not like '%itembank%'
	and c.name not like '%securemarker%' and c.name not like '%surpassdata%'
	and c.name not like '%testpack%'
	)

--exec sp_renamedb 'PRV_SQA_SurpassAnalytics', 'PRV_SQA_SurpassDataWarehouse'