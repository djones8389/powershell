select server_name [Instance]
	, database_name [DatabaseName]
	, backup_start_date	
from msdb.dbo.backupset
where name != 'CommVault Galaxy Backup'
	and backup_start_date > DATEADD(WEEK, -2, GETDATE())



select @@ServerName [Instance]
	, destination_database_name [DatabaseName]	
	, user_name
	--, case when user_name like '%cms%' then 'System'
	--	else 'Other'
	--	end as [Restore_Type]
from msdb.dbo.restorehistory
where restore_date > DATEADD(WEEK, -2, GETDATE())