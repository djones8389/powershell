use master

IF OBJECT_ID ('tempdb..#MBofDatainUse') IS NOT NULL DROP TABLE #MBofDatainUse;

CREATE TABLE #MBofDatainUse (	
	Instance sysname
	, DatabaseName sysname
	, UsedSpace_MB float
);

INSERT #MBofDatainUse
EXEC sp_msforeachdb '

use [?];

if(db_id() > 4)

BEGIN

	SELECT @@ServerName
		, db_name()
		, CAST(((SUM(total_pages) * 8192) / 1048576.0) AS FLOAT) FROM sys.allocation_units
END

'

SELECT *
FROM #MBofDatainUse
ORDER BY 1,2;

DROP TABLE #MBofDatainUse;