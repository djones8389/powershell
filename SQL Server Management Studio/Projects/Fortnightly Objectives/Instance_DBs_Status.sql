SELECT @@SERVERNAME [Instance]
	, COUNT(database_ID) [DatabaseCount]
	, state_desc [Status]
FROM sys.databases
where database_id > 4
group by state_desc