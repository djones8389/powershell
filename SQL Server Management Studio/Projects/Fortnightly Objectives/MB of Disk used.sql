SELECT @@ServerName [Instance]
    ,db.name AS [Database]
	,mf.size/128 as Size_MB
FROM sys.master_files mf
INNER JOIN sys.databases db 
ON db.database_id = mf.database_id
where type_desc = 'ROWS'
ORDER BY 1,2
