use master

IF OBJECT_ID('tempdb..##ClientTableMetrics') IS NOT NULL DROP TABLE ##ClientTableMetrics;

CREATE TABLE ##ClientTableMetrics (
	
	ID uniqueIdentifier
	,server_name nvarchar(100)
	, database_name nvarchar(100)
	, database_id int
	, table_name nvarchar(100)
	, rows int
	, reserved_kb nvarchar(20)
	, data_kb nvarchar(20)
	, index_size nvarchar(20)
	, unused_kb nvarchar(20)
);

exec sp_MSforeachdb '

USE [?];

if (''?'' in (select name from sys.databases where state_desc = ''ONLINE'' and database_id > 4))

BEGIN

	declare @dynamic nvarchar(MAX) = '''';

	select @dynamic +=CHAR(13) +
		''exec sp_spaceused @objname = ''''''+s.name+''.''+t.name + '''''' ''
	from sys.tables T
	inner join sys.schemas S
	on s.schema_id = t.schema_id
	where type = ''u''
	order by t.name;

	INSERT ##ClientTableMetrics (table_name, [rows], reserved_kb, data_kb, index_size, unused_kb)
	EXEC(@dynamic);

	UPDATE ##ClientTableMetrics
	SET database_id = db_id()
	where database_id IS NULL;

	UPDATE ##ClientTableMetrics
	SET server_name = @@SERVERNAME
		, database_name = DB_Name()
	where db_id() = database_id;
END
'


SELECT server_name
	, database_name
	, table_name	
	, cast(replace(reserved_kb,'KB','') as float) reserved_kb
	, cast(replace(data_kb,'KB','') as float) data_kb
	, cast(replace(index_size,'KB','') as float) index_size
	, cast(replace(unused_kb,'KB','') as float) unused_kb
FROM ##ClientTableMetrics
order by 1,2

