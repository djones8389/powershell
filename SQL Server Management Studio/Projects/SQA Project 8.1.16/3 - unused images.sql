SELECT B.Name [ProjectName]
	, A.Name [SharedLibraryName]
	, a.id [SharedLibraryID]
FROM ##Pages  A
INNER JOIN ProjectListTable B (NOLOCK)
on A.ProjectID = B.ID
where itemID IS NULL 
	and delcol = 0
	order by 1,3;



SELECT DISTINCT 
	'UPDATE ProjectManifestTable set delcol = 1 where projectID = ' + CONVERT(nvarchar(10),A.ProjectID) + ' and ID = '  +  CONVERT(nvarchar(10),A.ID) + ';' [UpdateScript]
,  'UPDATE ProjectManifestTable set delcol = 0 where projectID = ' + CONVERT(nvarchar(10),A.ProjectID) + ' and ID = '  +  CONVERT(nvarchar(10),A.ID) + ';' [RollbackScript]
FROM ProjectManifestTable A (NOLOCK)
INNER JOIN ##Pages B
on A.projectID = B.projectID
	and A.ID = B.ID

where itemID IS NULL 
	and A.delcol = 0;