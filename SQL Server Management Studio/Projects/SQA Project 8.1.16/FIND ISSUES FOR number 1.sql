USE [STG_SANDBOX_SQA_CPProjectAdmin];

SELECT A.*
FROM
(
      Select i.id, i.ver, i.moD, it.aLI
      from ItemGraphicTable it 
            INNER JOIN ComponentTable c ON c.ID = it.ParentID 
            INNER JOIN SceneTable s ON s.ID = c.ParentID 
            INNER JOIN PageTable i ON i.id = S.ParentID
      UNION
      Select i.id, i.ver, i.moD, it.aLI  
      from ItemVideoTable it 
            INNER JOIN ComponentTable c ON c.ID = it.ParentID 
            INNER JOIN SceneTable s ON s.ID = c.ParentID 
            INNER JOIN PageTable i ON i.id = S.ParentID
      UNION
      Select i.id, i.ver, i.moD, it.aLI  
            from ItemHotSpotTable it 
            INNER JOIN ComponentTable c ON c.ID = it.ParentID 
            INNER JOIN SceneTable s ON s.ID = c.ParentID 
            INNER JOIN PageTable i ON i.id = S.ParentID
      UNION
      Select i.id, i.ver, i.moD, it.aLI 
      from ItemCustomQuestionTable it 
            INNER JOIN ComponentTable c ON c.ID = it.ParentID 
            INNER JOIN SceneTable s ON s.ID = c.ParentID 
            INNER JOIN PageTable i ON i.id = S.ParentID
) A
where ISNUMERIC(ali) = 0
	and ali != ''
	and ali !='placeholder'
	ORDER BY 1
	
/*
id	ver	moD	aLI

id	ver	moD	aLI
842P993	3	26/10/2009 14:48:36	h2008q8_a2feed
842P993	3	26/10/2009 14:48:36	h2008q8_d2
842P993	3	26/10/2009 14:48:36	h2008q8_a2
842P993	3	26/10/2009 14:48:36	hint
842P993	3	26/10/2009 14:48:36	h2008q8_2
842P995	2	05/10/2009 16:59:56	h2008_q4hint
842P995	2	05/10/2009 16:59:56	h2008_q4correct1
842P995	2	05/10/2009 16:59:56	hint
842P995	2	05/10/2009 16:59:56	h2008_q4correct2
842P996	3	26/10/2009 15:36:47	hint
*/

--SELECT * FROM ProjectManifestTable where NAME LIKE '%h200%' and ProjectId = 842
--select * from ProjectListTable where id  = 842

--id	ver	moD	aLI
--891P907	2	09/04/2010 11:03:49	mediastar_news