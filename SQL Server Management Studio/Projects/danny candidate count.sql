IF OBJECT_ID('tempdb..##DATALENGTH') IS NOT NULL DROP TABLE ##DATALENGTH;

CREATE TABLE ##DATALENGTH (
	client nvarchar(1000)
	, [Count] int
);

DECLARE @dynamic nvarchar(MAX)='';

select @dynamic +=CHAR(13)+ '
use '+QUOTENAME([NAME])+'

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

INSERT ##DATALENGTH
select  db_name() Client
	,  COUNT(ID) [Count]
from UserTable
where IsCandidate = 1
'
from sys.databases
where state_desc = 'ONLINE'
	and name like '%[_]SecureAssess';

exec(@dynamic);



select *
from ##DATALENGTH
order by 2 desc;






