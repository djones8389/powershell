select c.Name [ClientName]
	, url [Alias]
	, s.Suffix
from [dbo].[Clients] C
inner join Suffixes s
on c.suffixid = s.id
where Suffix like '%o%'
	or Suffix like '%i%'
	or Suffix like '%a%'
	or Suffix like '%e%'
	or Suffix like '%u%'
	or Suffix like '%1%'
	or Suffix like '%2%'
	or Suffix like '%5%'

order by 1;