SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

--;with Live as (
DECLARE @LiveIDs TABLE (ID INT)
INSERT @LiveIDs
select esdt.ID--sum(DATALENGTH(esdt.Document))/1024/1024 [MB]
from ScheduledExamsTable scet
inner join ExamSessionTable est
on est.ScheduledExamID = scet.id
inner join ExamSessionDocumentTable esdt
on esdt.examSessionId = est.ID
inner join IB3QualificationLookup ib
on ib.ID = scet.qualificationId
where ExportToSecureMarker = 1
	--and DATEADD(MONTH, -6, getdate()) < scet.ScheduledStartDateTime 
	and (qualificationName != 'International Comparisons Research') -- AND examName != 'Speaking')

--)

--select DATEADD(MONTH, -6, getdate())
DECLARE @WHIDs TABLE (ID INT)
INSERT @WHIDs
select esdt.ID --sum(cast(DATALENGTH(esdt.Document) as bigint))/1024/1024 [MB]
from Warehouse_ScheduledExamsTable scet
inner join Warehouse_ExamSessionTable est
on est.WAREHOUSEScheduledExamID = scet.id
inner join Warehouse_ExamSessionDocumentTable esdt
on esdt.warehouseExamSessionID = est.ID
where ExportToSecureMarker = 1
	--and DATEADD(MONTH, -6, getdate()) < scet.ScheduledStartDateTime 
	and (qualificationName != 'International Comparisons Research')

--100 gb

Print 'Collected ESIDS into tables'
print getDATE()


BEGIN TRANSACTION

DELETE A
FROM ExamSessionDocumentTable A 
INNER JOIN @LiveIDs B
on A.ID = b.ID

PRINT 'Deleted Live'
print getDATE()

DELETE C
FROM Warehouse_ExamSessionDocumentTable C
INNER JOIN @WHIDs D
on D.ID = c.ID

PRINT 'Deleted Warehouse'
print getDATE()


ROLLBACK TRANSACTION