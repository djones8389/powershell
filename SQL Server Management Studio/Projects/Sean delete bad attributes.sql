DECLARE @XMLHolder TABLE (

	myXML XML
)

INSERT @XMLHolder
VALUES(
'<assessmentDetails>
        <assessmentID>1762</assessmentID>
        <qualificationID>82</qualificationID>
        <qualificationName>Accounts preparation (AQ2013)</qualificationName>
        <qualificationReference>AATQCF</qualificationReference>
        <assessmentGroupName>ACPR (AQ2013)</assessmentGroupName>
        <assessmentGroupID>1659</assessmentGroupID>
        <assessmentGroupReference>CAACPR</assessmentGroupReference>
        <assessmentName>Accounts preperation (AQ2013)</assessmentName>
        <validFromDate>01 Oct 2013</validFromDate>
        <expiryDate>30 Apr 2018</expiryDate>
        <startTime>00:00:00</startTime>
        <endTime>23:59:59</endTime>
        <duration>120</duration>
        <defaultDuration>120</defaultDuration>
        <scheduledDuration>
          <value>120</value>
          <reason />
        </scheduledDuration>
        <sRBonus>0</sRBonus>
        <sRBonusMaximum>40</sRBonusMaximum>
        <externalReference>CBTAAT</externalReference>
        <passLevelValue>70</passLevelValue>
        <passLevelType>1</passLevelType>
        <status>2</status>
        <testFeedbackType>
          <passFail>0</passFail>
          <percentageMark>0</percentageMark>
          <allowSummaryFeedback>0</allowSummaryFeedback>
          <summaryFeedbackType>1</summaryFeedbackType>
          <itemSummary>0</itemSummary>
          <itemReview>0</itemReview>
          <itemReviewCorrectAnswer>0</itemReviewCorrectAnswer>
          <itemFeedback>0</itemFeedback>
          <printableSummary>0</printableSummary>
          <candidateDetails>0</candidateDetails>
          <feedbackByReference>0</feedbackByReference>
          <allowFeedbackDuringExam>0</allowFeedbackDuringExam>
          <allowFeedbackDuringSummary>0</allowFeedbackDuringSummary>
        </testFeedbackType>
        <itemFeedback>0</itemFeedback>
        <allowCalculator>0</allowCalculator>
        <lastInUseDate>14 Sep 2013 15:40:59</lastInUseDate>
        <completedScriptReview>0</completedScriptReview>
        <assessment currentSection="1" totalTime="0" currentTime="0">
          <intro id="0" name="Introduction" currentItem="" duration="0" overrideLockdown="0">
            <item id="908P1131" name="Introduction" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" unit="Accounts Preparation" />
          </intro>
          <outro duration="0" overrideLockdown="0" id="0" name="Finish" currentItem="" />
          <tools id="0" name="Tools" currentItem="" />
          <section id="1" name="1" passLevelValue="70" passLevelType="1" itemsToMark="0" currentItem="908P989" fixed="0" duration="0" overrideLockdown="0" scheduledDuration="0">
            <item id="908P989" name="1 ACPR RE 002" totalMark="18" version="41" markingType="0" markingState="0" userMark="0.142857142857143" markerUserMark="" userAttempted="1" viewingTime="39549" flagged="0" LO="1. Non-current assets register" unit="Accounts Preparation" quT="11,12,20" dirty="2">
              <userData>
                <p um="0.142857142857143" cs="1" ua="1" id="908P989">
                  <s ua="1" um="0.14" id="3">
                    <c wei="15" maS="0" ie="1" typ="20" rv="0" mv="0" ua="0" um="0" id="13">
                      <i um="0" id="1">&lt;table extensionName="protean"&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="0" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="2" cor="171.00"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="2" cor="513.00�684-[c4r6]~o"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="0" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="0" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="0" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="2" cor="251.40"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="2" cor="754.20�1005.60-[c4r10]~o"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="0" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="0" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="0" cor=""&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="5000"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="2" cor="9675"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="0" cor=""&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="2" cor="1935�0.2*[c3r18]~o"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="2" cor="7740�[c3r18]-[c4r19]~o"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="0" cor=""&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;/table&gt;</i>
                    </c>
                    <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="0" um="0" id="7">
                      <i ca="30/09/X6�30.09.X6�30/9/X6�30.9.X6" id="1" />
                    </c>
                    <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="0" um="0" id="8">
                      <i ca="30/09/X6�30/09/x6�30.09.X6�30.09.x6�30/9/X6�30/9/x6�30.9.X6�30.9.x6" id="1" />
                    </c>
                    <c wei="1" maS="0" ie="1" typ="12" rv="0" mv="0" ua="1" um="0" id="10">
                      <i sl="3" id="1" />
                    </c>
                    <c wei="1" maS="0" ie="1" typ="12" rv="0" mv="0" ua="1" um="1" id="11">
                      <i sl="2" id="1" />
                    </c>
                    <c wei="1" maS="0" ie="1" typ="12" rv="0" mv="0" ua="1" um="1" id="12">
                      <i sl="5" id="1" />
                    </c>
                    <c wei="1" maS="0" ie="1" typ="12" rv="0" mv="0" ua="1" um="1" id="9">
                      <i sl="3" id="1" />
                    </c>
                  </s>
                </p>
              </userData>
            </item>
            <item id="908P978" name="2 ACPR RE 001" totalMark="17" version="36" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" LO="2. Ledger accounts for non-current assets" unit="Accounts Preparation" quT="10,11,20" />
            <item id="908P1096" name="3 ACPR AL005" totalMark="16" version="21" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" LO="3. Accounting for accruals and prepayments of income and expenses" unit="Accounts Preparation" quT="11,12,20" />
            <item id="908P1098" name="4 ACPR AL 004" totalMark="19" version="23" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" LO="4. Prepare a trial balance and reconciliations" unit="Accounts Preparation" quT="20" />
            <item id="908P1085" name="5 ACPR AL 004" totalMark="20" version="18" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" LO="5. Accounting adjustments in extended trial balance (ETB) or journals" unit="Accounts Preparation" quT="20" />
            <item id="908P1074" name="6 ACPR AL002" totalMark="20" version="23" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" LO="6. Extend the trial balance and show knowledge of accounting framework, accounting equation, records, and standards" unit="Accounts Preparation" quT="10,12,20" />
          </section>
        </assessment>
        <isValid>1</isValid>
        <isPrintable>0</isPrintable>
        <qualityReview>0</qualityReview>
        <numberOfGenerations>1</numberOfGenerations>
        <requiresInvigilation>1</requiresInvigilation>
        <advanceContentDownloadTimespan>60</advanceContentDownloadTimespan>
        <automaticVerification>1</automaticVerification>
        <offlineMode>0</offlineMode>
        <requiresValidation>0</requiresValidation>
        <requiresSecureClient>0</requiresSecureClient>
        <examType>0</examType>
        <advanceDownload>0</advanceDownload>
        <gradeBoundaryData>
          <gradeBoundaries passLevelType="1" adjustedGradeBoundaries="0">
            <grade modifier="lt" value="70" description="Fail" />
            <grade modifier="gt" value="70" description="Pass" />
          </gradeBoundaries>
        </gradeBoundaryData>
        <autoViewExam>0</autoViewExam>
        <autoProgressItems>0</autoProgressItems>
        <confirmationText>
          <confirmationText>Tick this box to confirm the above details are correct, the CBA submission is your own unaided work and you will not distribute, reproduce or circulate AAT assessment material.</confirmationText>
        </confirmationText>
        <requiresConfirmationCheck>1</requiresConfirmationCheck>
        <allowCloseWithoutSubmit>0</allowCloseWithoutSubmit>
        <useSecureMarker>0</useSecureMarker>
        <annotationVersion>0</annotationVersion>
        <allowPackagingDelivery>1</allowPackagingDelivery>
        <scoreBoundaryData>
          <scoreBoundaries showScoreBoundaryColumn="1">
            <score modifier="lt" value="36" description="Significantly below" higherBoundarySet="1" />
            <score modifier="lt" value="36" description="Significantly below" higherBoundarySet="0" />
            <score modifier="gt" value="36" description="Below requirement" higherBoundarySet="1" />
            <score modifier="gt" value="60" description="Borderline" higherBoundarySet="1" />
            <score modifier="gt" value="70" description="Met" higherBoundarySet="0" />
            <score modifier="gt" value="70" description="Met" higherBoundarySet="1" />
            <score modifier="gt" value="36" description="Below requirement" higherBoundarySet="0" userCreated="1" />
            <score modifier="gt" value="60" description="Borderline" higherBoundarySet="0" userCreated="1" />
            <score modifier="gt" value="85" description="Exceeded" higherBoundarySet="1" userCreated="1" />
          </scoreBoundaries>
        </scoreBoundaryData>
        <showCandidateReportResultColumn>0</showCandidateReportResultColumn>
        <showCandidateReportPercentageColumn>1</showCandidateReportPercentageColumn>
        <certifiedAccessible>0</certifiedAccessible>
        <markingProgressVisible>1</markingProgressVisible>
        <markingProgressMode>0</markingProgressMode>
        <secureClientOperationMode>1</secureClientOperationMode>
        <examDeliverySystemStyleType>delivery</examDeliverySystemStyleType>
        <markingAutoVoidPeriod>365</markingAutoVoidPeriod>
        <showPageRequiresScrollingAlert>1</showPageRequiresScrollingAlert>
        <project ID="908" />
      </assessmentDetails>
')

UPDATE @XMLHolder
SET myXML.modify('delete (/assessmentDetails/assessment//@*[local-name()=("duration", "overrideLockdown")])');

UPDATE @XMLHolder
SET myXML.modify('delete (/assessmentDetails/assessment/section/@scheduledDuration)');

	

SELECT *
FROM @XMLHolder