use [FULL_TEXT_TEST]

--truncate table IX_Metrics

--create table IX_Metrics (

--	fulltext_catalog_id tinyint
--	,fulltext_catalog_name varchar(20)
--	,change_tracking_state char(1)
--	, [object_name] varchar(20)
--	, num_fragments tinyint
--	, fulltext_mb decimal(8,2)
--	, largest_fragment_mb decimal(8,2)
--	, fulltext_fragmentation_in_percent decimal(8,2)
--	,[timeStamp] datetime
--)

CREATE FULLTEXT INDEX ON [dbo].[questions]
(
	[question_text]  Language 1033
)
KEY INDEX PK_questions
	on questions


declare @num_fragments smallint;

SELECT @num_fragments =  COUNT(*) 
FROM sys.fulltext_index_fragments

while(@num_fragments <> 1)

BEGIN

	INSERT IX_Metrics(fulltext_catalog_id, fulltext_catalog_name, change_tracking_state, object_name, num_fragments, fulltext_mb, largest_fragment_mb, fulltext_fragmentation_in_percent,[timeStamp])
	SELECT c.fulltext_catalog_id
		,c.NAME AS fulltext_catalog_name
		,i.change_tracking_state
		--,i.object_id
		,OBJECT_SCHEMA_NAME(i.object_id) + '.' + OBJECT_NAME(i.object_id) AS [object_name]
		,f.num_fragments
		,f.fulltext_mb
		,f.largest_fragment_mb
		,100.0 * (f.fulltext_mb - f.largest_fragment_mb) / NULLIF(f.fulltext_mb, 0) AS fulltext_fragmentation_in_percent
		, getDATE()
	FROM sys.fulltext_catalogs c
	INNER JOIN sys.fulltext_indexes i ON i.fulltext_catalog_id = c.fulltext_catalog_id
	INNER JOIN (
		-- Compute fragment data for each table with a full-text index
		SELECT table_id
			,COUNT(*) AS num_fragments
			,CONVERT(DECIMAL(9, 2), SUM(data_size / (1024. * 1024.))) AS fulltext_mb
			,CONVERT(DECIMAL(9, 2), MAX(data_size / (1024. * 1024.))) AS largest_fragment_mb
		FROM sys.fulltext_index_fragments
		GROUP BY table_id
		) f ON f.table_id = i.object_id

END


select * from IX_Metrics

--delete from IX_Metrics where [timeStamp] between  '2017-03-07 14:48:57.267' and '2017-03-07 14:48:57.593'

declare @start datetime = '2017-03-07 14:48:55.593'
declare @end datetime = '2017-03-07 14:50:02.250'

select DATEDIFF(SECOND, @start, @end)

--67 seconds