use Demo_ItemBank;

SELECT G.[ID]
       , AGT.[Name]
       , AT.[AssessmentName]
       , AT.[AssessmentDuration]
       , G.[AssessmentDuration Should Be]
FROM (
SELECT ID
       ,  a.b.value('data(@RequiredSections)[1]','int') *  a.b.value('data(@Duration)[1]','int') 
			+ c.d.value('data(@Duration)[1]','int') * a.b.value('data(@RequiredSections)[1]','int') [AssessmentDuration Should Be]
from AssessmentTable 
CROSS APPLY AssessmentRules.nodes('PaperRules/Section[@Name="Section Selector"]') a(b)
CROSS APPLY a.b.nodes('Section') c(d)
UNION
SELECT ID
		, sum(a.b.value('data(@Duration)[1]','int'))
from AssessmentTable 
CROSS APPLY AssessmentRules.nodes('PaperRules/Section[@Name="Section"]') a(b)
group by ID

) G
INNER JOIN  AssessmentTable AT
on AT.ID = G.ID

INNER JOIN AssessmentGroupTable AGT
ON AT.AssessmentGroupID = AGT.ID

WHERE AT.AssessmentDuration != G.[AssessmentDuration Should Be]
	and  G.[AssessmentDuration Should Be] > 0

GROUP BY G.[ID]
       , AGT.[Name]
       , AT.[AssessmentName]
       , AT.[AssessmentDuration]
       , G.[AssessmentDuration Should Be];



--SELECT ID
--		--, sum(a.b.value('data(@Duration)[1]','int'))
--		,AssessmentRules
--from AssessmentTable 
--CROSS APPLY AssessmentRules.nodes('PaperRules/Section[@Name="Section"]') a(b)
--where ID = 995
--group by ID

--select *
--from AssessmentTable
--where AssessmentTable.AssessmentName = 'timed sections1'