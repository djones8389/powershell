SELECT A.*
FROM (
select CQT.*
	, CT.CentreName
	, IB.QualificationName
	, IB.QualificationStatus
	, ROW_NUMBER() OVER (PARTITION BY CQT.CentreID,  IB.QualificationStatus ORDER BY CQT.CentreID) R
from CentreQualificationsTable CQT
INNER JOIN IB3QualificationLookup IB
on IB.ID = CQT.QualificationID
INNER JOIN CentreTable CT
on CT.ID = CQT.CentreID
LEFT JOIN PRV_WJEC_ItemBank..QualificationTable QT
on QT.ID = CQT.QualificationID
where QT.ID IS NULL
	and IB.QualificationStatus = 0
	and CT.Retired = 0
) A
WHERE R  = 1
	order by CentreName