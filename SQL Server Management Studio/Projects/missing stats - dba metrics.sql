select a.*
from (
SELECT Instance
	, database_name
	, max(cast(startdate as date)) [Date]
  FROM [PSCollector].[dbo].[DBGrowthMetrics]
  where Instance -- not in ('524778-SQL','553092-PRDSQL','704276-PREVSTA1','425732-OCR-SQL\SQL1','430325-OCR-SQL2\SQL2','430088-BC-SQL\SQL1','430326-BC-SQL2\SQL2')
	in ('welsh-sql-1')
  group by Instance
	, database_name
) a
where [Date] <> '2018-03-05'
 order by 1,2,3