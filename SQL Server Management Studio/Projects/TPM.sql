SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

select ScheduledExamRef
	, SurpassCandidateRef
	, Forename
	, Surname
	, CenterName
	, QualificationId
	, QualificationName
	, QualificationRef
	, SurpassExamId
	, SurpassExamRef
	, pack.Name
	, PackageDate
	, spc.ScheduledPackage_ScheduledPackageId
    , spc.ScheduledPackageCandidateId
	, sp.ScheduledPackageId
from ScheduledPackageCandidateExams as SPCE

	inner join ScheduledPackageCandidates as SPC
	on SPC.ScheduledPackageCandidateId =  SPCE.Candidate_ScheduledPackageCandidateId
	inner join ScheduledPackages as SP
     on SP.ScheduledPackageId =  SPC.ScheduledPackage_ScheduledPackageId
    inner join [dbo].[PackageExams] pe
    on pe.[PackageExamId] = SPCE.[PackageExamId]
    inner join [dbo].[Packages] pack
    on pack.[PackageId] = pe.[PackageId]
	inner join PPD_BCIntegration_SecureAssess.dbo.usertable ut
	on ut.CandidateRef collate SQL_Latin1_General_CP1_CI_AS = SurpassCandidateRef
where CenterName = 'aaaDeleteCheck'

/*
H36BDD99
RG4FVQ99
GYV63899
9XMJQP99
*/


select FK.name
	, fk.object_id
	, fk.parent_object_id
	, fk.referenced_object_id
	, o.name
	, p.name
	, fk.delete_referential_action_desc
from sys.foreign_keys FK
inner join sys.objects O
on O.object_id = FK.referenced_object_id

inner join sys.objects P
on P.object_id = FK.parent_object_id
