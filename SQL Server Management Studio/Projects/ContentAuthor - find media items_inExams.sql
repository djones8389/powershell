SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

USE Surpass_ContentAuthor

DECLARE @ItemIds TABLE (Id INT);
DECLARE @MediaIds TABLE (
	mediaId INT
	,itemId INT
)
DECLARE @AssistiveMediaIds TABLE (
	assistiveMediaId INT
	,itemId INT
);

DECLARE @SubjectName NVARCHAR(256);

INSERT INTO @ItemIds
SELECT Id
FROM Items

--MediaItems
INSERT INTO @MediaIds
--hotSpot media item
SELECT HotSpotMediaItemId
	,itemIds.Id
FROM HotSpotItems
INNER JOIN @ItemIds itemIds ON HotSpotItems.Id = itemIds.Id

UNION

--Answer options media both columns
SELECT MediaItemId3
	,itemIds.Id
FROM AnswerOptions
UNPIVOT(MediaItemId3 FOR MediaItemId2 IN (
			MediaItemId
			,AdditionalMediaItemId
			)) AS P
INNER JOIN @ItemIds itemIds ON P.ItemId = itemIds.Id

UNION

--stem components media
SELECT MediaItemId
	,itemIds.Id
FROM StemComponents
INNER JOIN @ItemIds itemIds ON StemComponents.ItemId = itemIds.Id

UNION

--candidate feedback media
SELECT MediaItemId
	,itemIds.Id
FROM CandidateFeedbacks
INNER JOIN @ItemIds itemIds ON CandidateFeedbacks.ItemId = itemIds.Id

UNION

--source material media
SELECT MediaItemId
	,itemIds.Id
FROM SourceMaterials
INNER JOIN @ItemIds itemIds ON SourceMaterials.ItemId = itemIds.Id

--Assistive media items
INSERT INTO @AssistiveMediaIds
--assistiveMedia on Item
SELECT AssistiveMediaId
	,itemIds.Id
FROM Items
INNER JOIN @ItemIds itemIds ON Items.Id = itemIds.Id

UNION

--assistiveMedia on answer option 
SELECT AssistiveMediaId
	,itemIds.Id
FROM AnswerOptions
INNER JOIN @ItemIds itemIds ON AnswerOptions.ItemId = itemIds.Id

--Summary
SELECT S.Title SubjectName
	,s.ProjectId
	,subjectid
	,mediaIds.itemId
	,mediaIds.mediaId
	,MediaItems.Id
	,MediaItems.FileId
	,MediaItems.Name [FileName]
	,QualificationName
	,AssessmentName
	,ExternalReference
FROM @MediaIds mediaIds
INNER JOIN MediaItems ON mediaIds.mediaId = MediaItems.Id
INNER JOIN Subjects S ON S.ID = SubjectId
INNER JOIN (
	
	SELECT QT.QualificationName
		, AssessmentName
		, ExternalReference
		, REPLACE(REPLACE(cast(a.b.query('.') as NVARCHAR(MAX)), '<XPath>@ID="',''), '"</XPath>','') ItemID
	FROM Surpass_ItemBank.dbo.AssessmentTable AT  (READUNCOMMITTED)
	INNER JOIN  Surpass_ItemBank.dbo.AssessmentGroupTable AGT (READUNCOMMITTED) ON AGT.ID = AT.AssessmentGroupID
	INNER JOIN  Surpass_ItemBank.dbo.QualificationTable QT (READUNCOMMITTED) ON QT.ID = AGT.QualificationID
	CROSS APPLY AssessmentRules.nodes('PaperRules//XPath') a(b)

	where REPLACE(REPLACE(cast(a.b.query('.') as NVARCHAR(MAX)), '<XPath>@ID="',''), '"</XPath>','') not like '%xpath%'
) ItemBank
ON SUBSTRING(ItemBank.ItemID, 0, CHARINDEX('P',ItemBank.ItemID)) = s.ProjectId
	and  SUBSTRING(ItemBank.ItemID, CHARINDEX('P',ItemBank.ItemID)+1, (LEN(ItemBank.ItemID)-CHARINDEX('P',ItemBank.ItemID))) = mediaIds.itemId

WHERE MediaItems.Type = 5;

/*


        None = -1,
        Image = 0,
        Audio = 1,
        Video = 2,
        Swf = 3,
        Flv = 4,
        Pdf = 5,
        Group = 6,
        Html = 7
*/

--May return records if used--

SELECT @SubjectName SubjectName,
              assistiveMediaIds.itemId,
              assistiveMediaIds.assistiveMediaId,
              AssistiveMediaItems.*
FROM @AssistiveMediaIds assistiveMediaIds
JOIN AssistiveMediaItems ON assistiveMediaIds.assistiveMediaId=AssistiveMediaItems.Id
WHERE AssistiveMediaItems.Type=5
ORDER BY ItemId

