SELECT DBName,
	b.[OBJECT_NAME]
	,[execution_count]
	,([CPU]  / [execution_count])	/ 1000000 		[average CPU-s]
	,([ELAPSED] / [execution_count])/ 1000000		[average elapsed time-s]
	,[PHYSICAL_READS] / [execution_count]			[average PHYSICAL_READS]
	,[LOGICAL_READS] / [execution_count]			[average LOGICAL_READS]		
	,[LOGICAL_WRITES] / [execution_count]			[average LOGICAL_WRITES]
FROM (
	SELECT A.*
	FROM (
	SELECT  DBName--substring(DBName, CHARINDEX('_',DBName)+1, LEN(DBName)-CHARINDEX('_',DBName)) DBName
			,[OBJECT_NAME]
		  ,sum([execution_count]) [execution_count]
		  ,sum([CPU]) [CPU]
		  ,sum([ELAPSED]) [ELAPSED]
		  ,sum([LOGICAL_READS]) [LOGICAL_READS]
		  ,sum([LOGICAL_WRITES]) [LOGICAL_WRITES]
		  ,sum([PHYSICAL_READS]) [PHYSICAL_READS]
	  FROM [PSCollector].[dbo].[SprocMetrics]
	  where dbname not like '%SurpassDataWarehouse%'
		 and [OBJECT_NAME] = 'sa_REPORTINGSERVICE_GetExamInstancesForReporting_sp'
	  group by DBName,
		[OBJECT_NAME]
	) A
	where [execution_count] > 10
) B
order by [average elapsed time-s] desc








/*
SELECT *
FROM [PSCollector].[dbo].[SprocMetrics]
where dbname in ('BRITISHCOUNCIL_SecureAssess','AAT_SecureAssess')
	and [OBJECT_NAME] = 'sa_REPORTINGSERVICE_GetExamInstancesForReporting_sp'
	order by 1



SELECT DBName
	--,b.[OBJECT_NAME]
	,[execution_count]
	,([CPU]  / [execution_count])	/ 1000000 		[average CPU-s]
	,([ELAPSED] / [execution_count])/ 1000000		[average elapsed time-s]
	,[PHYSICAL_READS] / [execution_count]			[average PHYSICAL_READS]
	,[LOGICAL_READS] / [execution_count]			[average LOGICAL_READS]		
	,[LOGICAL_WRITES] / [execution_count]			[average LOGICAL_WRITES]
	FROM (
	SELECT  DBName
			,[OBJECT_NAME]
		  ,sum([execution_count]) [execution_count]
		  ,sum([CPU]) [CPU]
		  ,sum([ELAPSED]) [ELAPSED]
		  ,sum([LOGICAL_READS]) [LOGICAL_READS]
		  ,sum([LOGICAL_WRITES]) [LOGICAL_WRITES]
		  ,sum([PHYSICAL_READS]) [PHYSICAL_READS]
	  FROM [PSCollector].[dbo].[SprocMetrics]
	  where dbname in ('BRITISHCOUNCIL_SecureAssess','AAT_SecureAssess')
		 and [OBJECT_NAME] = 'sa_REPORTINGSERVICE_GetExamInstancesForReporting_sp'
	  group by DBName,
		[OBJECT_NAME]
	) A
*/