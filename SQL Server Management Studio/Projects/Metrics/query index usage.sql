select Instance
	, [database]
	, [table]
	, [index]
	, ([user_seeks] + [user_scans] + [user_lookups]) usage
	, user_updates
	, last_user_seek, last_user_scan, last_user_lookup
from [dbo].[IndexMetrics]
where --[table] = 'examsessiontable'
	 [StartDate] > '2017-10-01'
	 and [database] like '%[_]ContentAuthor'
	 and Instance not in ('704276-PREVSTA1','524778-SQL')
	 and [index] in ('IX_AssistiveMediaId','IX_UserId','IX_AssistiveMediaId','IX_NotificationType','IX_KeyCode','UK_dbo.TestLinks_KeyCode','UQ_Tools_ToolItemId','IX_FacultyId','IX_UserId','IX_UserId')
	 and [table] in ('Items','AnswerOptions')
order by usage -- [table], [index], usage desc;
	
--ID, Instance, database, table, index, user_updates, last_user_uID, Instance, database, table, index, user_updates, last_user_update, user_seeks
--, user_scans, user_lookups, last_user_seek, last_user_scan, last_user_lookup, collectionDate, StartDate, EndDatepdate, user_seeks, user_scans, user_lookups, last_user_seek
