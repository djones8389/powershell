use master

IF OBJECT_ID('tempdb..##GetUsers') IS NOT NULL DROP TABLE ##GetUsers;

CREATE TABLE ##GetUsers (
	client nvarchar(1000)
	, centreID int
	, subjectID int
	, userCount int
);

DECLARE @dynamic nvarchar(MAX)='';

select @dynamic +=CHAR(13)+ '

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

USE '+QUOTENAME(NAME)+'

;WITH CTE_Roles AS
(
	SELECT	U.Id AS ''UserId''
	,	U.UserName
	,	r.[Name] AS ''Role''
	,	ucsr.SubjectId
	,	rt.[Name] AS ''RoleType''
	,	ucsr.CentreId
	FROM Users U
		INNER JOIN UserCentreSubjectRoles ucsr
			ON U.Id = ucsr.UserId
		INNER JOIN Roles r
			ON ucsr.RoleId = r.Id
		LEFT OUTER JOIN RoleTypes rt 
			ON r.RoleTypeId = rt.Id
	WHERE (rt.[Name] = ''TestCreation'' OR r.[Name] IN (''Site Administrator'',''Centre Administrator'',''Subject Owner''))
)
--SELECT * FROM CTE_Roles;
, CTE_SiteAdmins AS
(
	SELECT DISTINCT UserId
	FROM CTE_Roles
	WHERE [Role] = ''Site Administrator''
)
--SELECT * FROM CTE_SiteAdmins
, CTE_CentreAdmins AS
(
	SELECT DISTINCT UserId, CentreId
	FROM CTE_Roles
	WHERE [Role] = ''Centre Administrator''
	EXCEPT
	SELECT UserId, CentreId
	FROM CTE_Roles r
	WHERE EXISTS (SELECT 1 FROM CTE_SiteAdmins WHERE UserId = r.UserId)
)
--SELECT * FROM CTE_CentreAdmins
, CTE_SubjectOwners AS
(
	SELECT DISTINCT UserId, CentreId, SubjectId
	FROM CTE_Roles
	WHERE [Role] = ''Subject Owner''
	EXCEPT
	SELECT UserId, CentreId, SubjectId
	FROM CTE_Roles r
	WHERE EXISTS (SELECT 1 FROM CTE_SiteAdmins WHERE UserId = r.UserId)
		OR EXISTS (SELECT 1 FROM CTE_CentreAdmins WHERE UserId = r.UserId)
)
--SELECT * FROM CTE_SubjectOwners
, CTE_NonAdmins AS 
(
	SELECT DISTINCT UserId, CentreId, SubjectId
	FROM CTE_Roles
	WHERE UserId NOT IN 
	(
		SELECT UserId FROM CTE_SiteAdmins
		UNION
		SELECT UserId FROM CTE_CentreAdmins
		UNION 
		SELECT UserId FROM CTE_SubjectOwners
	)
)
--SELECT * FROM CTE_NonAdmins;

-- Phew... final query - count of all users that have permission for each Centre/Subject combo...
INSERT ##GetUsers
SELECT db_name()
	,  a.*
from (
SELECT sc.[CentreId]
	,	sc.[SubjectId]
	  ,  (SELECT COUNT(1) FROM CTE_SiteAdmins)
		  + (SELECT COUNT(1) FROM CTE_CentreAdmins WHERE CentreId = sc.CentreId)
		  + (SELECT COUNT(1) FROM CTE_SubjectOwners WHERE SubjectId = sc.SubjectId)
		  + COUNT(DISTINCT users.UserId)
		AS UserCount
FROM [dbo].[SubjectCentres] sc
	LEFT OUTER JOIN CTE_NonAdmins users
		ON sc.CentreId = users.CentreId
		AND sc.SubjectId = users.SubjectId
GROUP BY sc.[SubjectId]
      ,sc.[CentreId]
) a
ORDER BY UserCount DESC;
'

from sys.databases
where state_desc = 'ONLINE'
	and name like '%[_]SurpassManagement';

exec(@dynamic);

select Client [ClientDBName]
	, SUBSTRING(client, 0, charindex('_',client)) Client
	, centreID
	, subjectID
	, userCount
from ##GetUsers
order by 1




