USE STG_SANDBOX_SQA_CPProjectAdmin

IF OBJECT_ID('tempdb..#ProjectIDs') IS NOT NULL DROP TABLE #ProjectIDs

CREATE TABLE #ProjectIDs (
	projectID int
);

INSERT #ProjectIDs
select ID
from ProjectListTable
where ID IN (1190,1154,1326,1272,1317,1345,1321,1255,1341,1274,1233,1279,1208,1305,1232,1191,1342,1193,1256,1177,1207,1181,1209,1231,1278,1347,1257,1179,1320,1192,1313,1194,1344,1195,1196,1197,1258,1180,1155,1210);

select FK.name
	, fk.object_id
	, fk.parent_object_id
	, fk.referenced_object_id
	, o.name
	, p.name
	, fk.delete_referential_action_desc
	, 'DELETE FROM ' + QUOTENAME (p.name) + ' where projectID in (select projectID from #ProjectIDs)'
from sys.foreign_keys FK
inner join sys.objects O
on O.object_id = FK.referenced_object_id
inner join sys.objects P
on P.object_id = FK.parent_object_id
where o.name = 'ProjectListTable'
	and delete_referential_action_desc = 'NO_ACTION';

CREATE CLUSTERED INDEX [ix_test] ON #ProjectIDs (
	projectID
	);


BEGIN TRY
  BEGIN TRANSACTION
	DELETE FROM [Contract] where projectID in (select projectID from #ProjectIDs);
	DELETE FROM [ProjectPaperTemplateTable] where projectID in (select projectID from #ProjectIDs);
	DELETE FROM [ReferenceLibraryDetails] where projectID in (select projectID from #ProjectIDs);
	DELETE FROM ProjectListTable where ID in (select projectID from #ProjectIDs);
  COMMIT TRANSACTION
END TRY
BEGIN CATCH
    SELECT 
        ERROR_NUMBER() AS ErrorNumber
        ,ERROR_SEVERITY() AS ErrorSeverity
        ,ERROR_STATE() AS ErrorState
        ,ERROR_PROCEDURE() AS ErrorProcedure
        ,ERROR_LINE() AS ErrorLine
        ,ERROR_MESSAGE() AS ErrorMessage;

    IF @@TRANCOUNT > 0
    ROLLBACK TRANSACTION;
END CATCH

IF @@TRANCOUNT > 0
    COMMIT TRANSACTION;
GO


