use AAT_SecureAssess

SELECT
	qualificationName
	, examVersionName
	, examVersionRef
	, KeyCode
	, candidateRef [Membership No.]
	, foreName + ' ' + surName [CandidateName]
	, centreName
	, centreCode
	, originalGrade
	, WESTS.userMark
	, userPercentage
	, ItemRef
	, ItemVersion
	, cast((WESTI.userMark*100)/WESTI.TotalMark as float) [MarkAwarded]
	, warehouseTime
FROM WAREHOUSE_ExamSessionTable_ShrededItems WESTI WITH (READUNCOMMITTED)

INNER JOIN WAREHOUSE_ExamSessionTable_Shreded WESTS WITH (READUNCOMMITTED)
on WESTS.examSessionID = WESTI.examSessionID

where ItemRef in ('974P1252', '974P1225','974P1180','974P1169','974P1132','974P1064')
	and previousExamState != 10
	and ExternalReference not like '%PRACTICE%'
	and warehouseTime > '2016-'
order by ItemRef, warehouseTime;



