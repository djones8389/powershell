DECLARE @DBName nvarchar(100) = 'AAT_SecureAssess';

SELECT 
		 AVG(CONVERT(DECIMAL(15,2),compressed_backup_size))/1000/1024/1024 'AvBackup-GB'
		, AVG(CONVERT(DECIMAL(15,2),compressed_backup_size))/1000/1024 'AvBackup-MB'
		, AVG(CONVERT(DECIMAL(15,2),Pages)) 'AvPages'
		, AVG(Duration) 'Duration'
		
	FROM (

SELECT 
		ROW_NUMBER() OVER(PARTITION BY ServerName,DBName, name ORDER BY ServerName,DBName, name desc) R   
	 ,CASE 
	   WHEN ServerName = '343553-WEB5' then 'AQA'
   	   WHEN ServerName = '408302-AQAREPOR' then 'AQA'
	   WHEN ServerName = '430069-AAT-SQL\SQL1' then 'AAT'
	   WHEN ServerName = '430327-AAT-SQL2\SQL2' then 'AAT'
	   WHEN ServerName = '403252-ACTVARIE' then 'Actuaries'
	   WHEN ServerName = '430088-BC-SQL\SQL1' then 'BritishCouncil'
	   WHEN ServerName = '430326-BC-SQL2\SQL2' then 'BritishCouncil'
	   WHEN ServerName = '420304-EAL2-LIV' then 'EAL'
	   WHEN ServerName = '553092-PRDSQL' then 'Editions Live'
	   WHEN ServerName = '524778-SQL' then 'Editios Mirror'
	   WHEN ServerName = '407710-WEB3' then 'NCFE'
	   WHEN ServerName = '425732-OCR-SQL\SQL1' then 'OCR'
	   WHEN ServerName = '430325-OCR-SQL2\SQL2' then 'OCR'
	   WHEN ServerName = '704276-PREVSTA1' then 'PRP-PRV'
	   WHEN ServerName = '338301-WEB4' then 'Skillsfirst'
	   WHEN ServerName = '335657-APP1' then 'SQA'
	   WHEN ServerName = '335658-APP2' then 'SQA'
	   WHEN ServerName = '311619-WEB3' then 'WJEC'
		    ELSE 'ServerName'
		END AS 'ServerName'
		, DBName
		, Pages
		, compressed_backup_size
	 , DATEDIFF(SECOND, backup_start_date, backup_finish_date) as [Duration]
	 , backup_start_date
	 , backup_finish_date
	 , CASE WHEN name = 'CommVault Galaxy Backup' THEN 'Rackspace Backup' ELSE 'Maintenance Backup' END AS [Backup Type]
	 , is_compressed
 FROM [PSCollector].[dbo].[Backup Metrics_Raw]
	
) A
	where DBName = @DBName
		--and A.[Backup Type] =  'Maintenance Backup'
		and [Duration] > 0

		--order by 7 DESC;
