SELECT A.*
, B.*
FROM(
SELECT 
	row_NUMBER()OVER(PARTITION BY [database_name] ORDER BY [database_name],[RunDate] DESC) R
      , [database_name]
      , CASE 
	   WHEN server = '343553-WEB5' then 'AQA'
   	   WHEN server = '408302-AQAREPOR' then 'AQA'
	   WHEN server = '430069-AAT-SQL\SQL1' then 'AAT'
	   WHEN server = '430327-AAT-SQL2\SQL2' then 'AAT'
	   WHEN server = '403252-ACTVARIE' then 'Actuaries'
	   WHEN server = '430088-BC-SQL\SQL1' then 'BritishCouncil'
	   WHEN server = '430326-BC-SQL2\SQL2' then 'BritishCouncil'
	   WHEN server = '420304-EAL2-LIV' then 'EAL'
	   WHEN server = '553092-PRDSQL' then 'Editions Live'
	   WHEN server = '524778-SQL' then 'Editios Mirror'
	   WHEN server = '407710-WEB3' then 'NCFE'
	   WHEN server = '425732-OCR-SQL\SQL1' then 'OCR'
	   WHEN server = '430325-OCR-SQL2\SQL2' then 'OCR'
	   WHEN server = '704276-PREVSTA1' then 'PRP-PRV'
	   WHEN server = '338301-WEB4' then 'Skillsfirst'
	   WHEN server = '335657-APP1' then 'SQA'
	   WHEN server = '335658-APP2' then 'SQA'
	   WHEN server = '311619-WEB3' then 'WJEC'
		    ELSE 'ServerName'
		END AS 'ServerName'
      ,[run_duration]
      ,[CompressedBackupSize_MB]
      ,[CompressedBackupSize_GB]
  FROM [PSCollector].[dbo].[Backup Metrics]
  where Server NOT IN('553092-PRDSQL', '338301-WEB4')
  )A
INNER JOIN [SQLEdition] B
on B.ServerName = A.ServerName

WHERE R = 1


