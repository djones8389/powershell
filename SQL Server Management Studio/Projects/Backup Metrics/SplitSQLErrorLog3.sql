DECLARE @Backups TABLE(logDate datetime, ProcessInfo nvarchar(20), text nvarchar(2000))
INSERT @Backups
EXEC sp_readerrorlog 0, 1, 'Back'

DECLARE @T TABLE(logDate datetime,Metrics varchar(1000));

INSERT INTO @T 
SELECT logDate
	,REPLACE(SUBSTRING(text, 0, CHARINDEX(', first',text))	, ',', '-')

FROM @Backups
where processInfo = 'Backup'
	

SELECT distinct A.*
--, REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(first_lsn, '0',':'), '::::::',':'), ':::::',':'), '::::',':'), ':::',':'), '::',':')
FROM (

SELECT  CASE WHEN @@Servername = '704276-PREVSTA1' THEN '704276-PRP-PRV' END AS [ServerName]
        ,SUBSTRING(SUBSTRING(PARSENAME(REPLACE(Metrics,'-','.'),LEN(Metrics)-LEN(REPLACE(Metrics,'-',''))+1), CHARINDEX(':', PARSENAME(REPLACE(Metrics,'-','.'),LEN(Metrics)-LEN(REPLACE(Metrics,'-',''))+1)), 100), 3, 100) [DBName]
        ,logDate
        ,REPLACE(REPLACE(SUBSTRING(PARSENAME(REPLACE(Metrics,'-','.'),LEN(Metrics)-LEN(REPLACE(Metrics,'-',''))), 33,LEN(PARSENAME(REPLACE(Metrics,'-','.'),LEN(Metrics)-LEN(REPLACE(Metrics,'-',''))))),'(', ''), ')', '') [Duration]
        ,SUBSTRING(PARSENAME(REPLACE(Metrics,'-','.'),LEN(Metrics)-LEN(REPLACE(Metrics,'-',''))-1), CHARINDEX(':', PARSENAME(REPLACE(Metrics,'-','.'),LEN(Metrics)-LEN(REPLACE(Metrics,'-',''))-1))+2, 100) [Pages]
		,metrics as [FullDetails]
FROM @T 
) A


INNER JOIN msdb.dbo.backupset B
ON A.ServerName = B.machine_name

where dbname = 'STG_SQA2_ContentProducer'
	AND A.DBName = B.database_name
	--and backup_finish_date = LogDate


--SELECT b.backup_finish_date, first_lsn
--FROM   msdb.dbo.backupmediafamily mf
--INNER JOIN msdb.dbo.backupset b ON mf.media_set_id = b.media_set_id 
--where first_lsn = '249316:26569:196'

where database_name = 'STG_BC2_SecureAssess'