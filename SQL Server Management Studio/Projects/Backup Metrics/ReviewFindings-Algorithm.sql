DECLARE @SizeOfDB bigint = 53687091200;  --GB
DECLARE @IsCompressed bit = 1;   --0 or 1


SELECT X
	,  y
	, x / y
FROM 
(

SELECT 
	x / AvDuration [y]
	, x
FROM
(

SELECT
	AvBackup/AvPages [x]
	, AvDuration
FROM
(

SELECT 
	
	CASE WHEN @IsCompressed = 1 THEN AVG(CONVERT(DECIMAL(15,2),compressed_backup_size)) END AS 'AvBackup'
	, CASE WHEN @IsCompressed = 1 THEN AVG(CONVERT(DECIMAL(15,2),Pages)) END AS 'AvPages'
	, CASE WHEN @IsCompressed = 1 THEN AVG(Duration) END AS 'AvDuration'
	
FROM (

SELECT 
	ROW_NUMBER() OVER(PARTITION BY DBName ORDER BY backup_start_date desc) R 
      ,[DBName]
	  --, [x] =  CONVERT(DECIMAL(15,2),compressed_backup_size) / CONVERT(DECIMAL(15,2),Pages)	  
	  --, [y] = (CONVERT(DECIMAL(15,2),compressed_backup_size) / CONVERT(DECIMAL(15,2),Pages))/[Duration]
      ,[Pages]
	  ,compressed_backup_size
      ,[Duration] --as 'Duration in seconds'
      ,[backup_start_date]
      ,[backup_finish_date]
      ,[Backup Type]
      ,[is_compressed]
  FROM [PSCollector].[dbo].[Backup Metrics]
	where [Backup Type] = 'Maintenance Backup'
) A

where R = 1
	and [Duration] > 0

 ) B
 
 ) C
 ) D





	--and DBName in ('EAL_SecureAssess','SkillsFirst_SecureAssess')







	
	--R
	--, DBName
	--, Pages
	--, x
	--, y
	--, x/y
	--, compressed_backup_size
	--, [Duration in seconds]
 --   , [backup_start_date]
 --   , [backup_finish_date]
 --   , [Backup Type]
 --   , [is_compressed]





	--cast(compressed_backup_size as bigint)/1024/1024 / [Duration in seconds] 'Time it takes to backup, in seconds, per megabyte'
	--,cast(compressed_backup_size as bigint)/1000/1024 'Megaybytes'
	--,Pages/[Duration in seconds] 'How many seconds per page to backup'
	--,Pages
	--,[Duration in seconds]
