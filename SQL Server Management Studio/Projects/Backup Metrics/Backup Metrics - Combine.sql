use msdb

declare @tmp_sp_help_jobhistory table
(
    instance_id int null, 
    job_id uniqueidentifier null, 
    job_name sysname null, 
    step_id int null, 
    step_name sysname null, 
    sql_message_id int null, 
    sql_severity int null, 
    message nvarchar(4000) null, 
    run_status int null, 
    run_date int null, 
    run_time int null, 
    run_duration int null, 
    operator_emailed sysname null, 
    operator_netsent sysname null, 
    operator_paged sysname null, 
    retries_attempted int null, 
    server sysname null  
)

insert into @tmp_sp_help_jobhistory 
exec msdb.dbo.sp_help_jobhistory 
    @job_name = 'Pre-Migration - Backup ContentProducer',
    @mode='FULL' 
        
SELECT

    tshj.server AS [Server],
    a.database_name,
    tshj.job_name AS [JobName],
    tshj.step_name AS [StepName],
    
    CASE tshj.run_date WHEN 0 THEN NULL 
	ELSE
	CONVERT(datetime,LEFT(
    convert(datetime, 
            stuff(stuff(cast(tshj.run_date as nchar(8)), 7, 0, '-'), 5, 0, '-') + N' ' + 
            stuff(stuff(substring(cast(1000000 + tshj.run_time as nchar(7)), 2, 6), 5, 0, ':'), 3, 0, ':'), 
            120), 20)) END AS [RunDate]
	, run_duration
	, backup_type
	, a.CompressedBackupSize_MB
	, A.CompressedBackupSize_GB
	, physical_device_name
FROM @tmp_sp_help_jobhistory as tshj
INNER JOIN (
	SELECT  
	ROW_NUMBER() OVER(PARTITION BY 
		CONVERT(CHAR(100), SERVERPROPERTY('Servername'))
		,b.database_name
		,b.type
		,device_type
	 ORDER BY	 
		CONVERT(CHAR(100), SERVERPROPERTY('Servername'))
		,b.database_name
		,b.type
		,device_type
	 ) R ,
	b.database_name,  
	CONVERT(datetime,LEFT(b.backup_start_date, 20)) as backup_start_date,   
	b.backup_finish_date, 
	CASE b.type
	   WHEN 'D' THEN 'Database'  
	   WHEN 'I' then 'Differential Database'
	   WHEN 'L' THEN 'Log'
	   WHEN 'F' THEN 'File of filegroup'
	   when 'G' then 'Differential file'
	   when 'P' then 'Partial'
	   when 'Q' then 'Differential partial'
	END AS backup_type,  
	b.compressed_backup_size / 1000 / 1024 AS CompressedBackupSize_MB,
	b.compressed_backup_size / 1000 / 1024 / 1024 AS CompressedBackupSize_GB,
	mf.device_type,
	mf.physical_device_name
FROM   msdb.dbo.backupmediafamily mf
INNER JOIN msdb.dbo.backupset b ON mf.media_set_id = b.media_set_id 

where backup_start_date > DATEADD(DAY, -3, CONVERT(nvarchar(10),GETDATE(),112))
	and database_name like '%SecureAssess%'
	 
) A

ON A.backup_start_date = CONVERT(datetime,LEFT(
    convert(datetime, 
            stuff(stuff(cast(tshj.run_date as nchar(8)), 7, 0, '-'), 5, 0, '-') + N' ' + 
            stuff(stuff(substring(cast(1000000 + tshj.run_time as nchar(7)), 2, 6), 5, 0, ':'), 3, 0, ':'), 
            120), 20))

where run_status = 1
	and step_name != '(Job outcome)'
	and backup_type = 'Database'