/****** Script for SelectTopNRows command from SSMS  ******/
SELECT TOP 1000 [R]
      ,[Server]
      ,[database_name]
      ,[backup_start_date]
      ,[backup_finish_date]
	  ,[backup_type]
      ,[CompressedBackupSize_MB]
	  ,[CompressedBackupSize_GB]
	  ,DATEDIFF(SECOND, [backup_start_date],[backup_finish_date])/ROUND([CompressedBackupSize_GB], 0) 'SecondsPerGigabyte'      
      ,[device_type]
      ,[physical_device_name]
  FROM [PSCollector].[dbo].[Backup Metrics]
  where [backup_type] != 'Differential Database'
	order by 3;