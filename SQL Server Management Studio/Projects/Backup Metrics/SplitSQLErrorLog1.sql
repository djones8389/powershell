DECLARE @Backups TABLE(logDate datetime, ProcessInfo nvarchar(20), text nvarchar(2000))
INSERT @Backups
EXEC sp_readerrorlog 0, 1, 'Back'

DECLARE @T TABLE(Metrics varchar(1000));

INSERT INTO @T 
SELECT REPLACE(SUBSTRING(text, 0, CHARINDEX(', first',text))	, ',', '-')
FROM @Backups
where processInfo = 'Backup'


SELECT  @@Servername [ServerName],
        PARSENAME(REPLACE(Metrics,'-','.'),LEN(Metrics)-LEN(REPLACE(Metrics,'-',''))+1),
        SUBSTRING(PARSENAME(REPLACE(Metrics,'-','.'), LEN(Metrics)-LEN(REPLACE(Metrics,'-',''))+1), CHARINDEX(':', PARSENAME(REPLACE(Metrics,'-','.'),LEN(Metrics)-LEN(REPLACE(Metrics,'-',''))+1)),LEN(PARSENAME(REPLACE(Metrics,'-','.'),LEN(Metrics)-LEN(REPLACE(Metrics,'-',''))+1))),       
        PARSENAME(REPLACE(Metrics,'-','.'),LEN(Metrics)-LEN(REPLACE(Metrics,'-',''))),
        PARSENAME(REPLACE(Metrics,'-','.'),LEN(Metrics)-LEN(REPLACE(Metrics,'-',''))-1)
        

FROM @T

--LEN(PARSENAME(REPLACE(Metrics,'-','.'),LEN(Metrics)-LEN(REPLACE(Metrics,'-',''))+1))