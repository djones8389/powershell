DECLARE @Backups TABLE(logDate datetime, ProcessInfo nvarchar(20), text nvarchar(2000))
INSERT @Backups
EXEC sp_readerrorlog 0, 1, 'Back'

DECLARE @T TABLE(logDate datetime,Metrics varchar(1000));

INSERT INTO @T 
SELECT logDate
	, SUBSTRING(text, 0, CHARINDEX(', last',text))

FROM @Backups
where processInfo = 'Backup'
	AND text not like '%Differential%'

SELECT DISTINCT
	A.*
	, compressed_backup_size
	, CASE WHEN name = 'CommVault Galaxy Backup' THEN 'Rackspace Backup' ELSE 'Maintenance Backup' END AS [Backup Type]
FROM
(
SELECT  
 SUBSTRING(Metrics, CHARINDEX(':', Metrics)+2, CHARINDEX(',', Metrics)-CHARINDEX(':', Metrics)-2) [DBName]
, SUBSTRING(Metrics, CHARINDEX(',', Metrics)+34, 8) [Duration]
, SUBSTRING(Metrics, CHARINDEX('umped:', Metrics)+6,  CHARINDEX(', first', Metrics)-CHARINDEX('umped:', Metrics)-6) [Pages]
, REPLACE(SUBSTRING(Metrics, CHARINDEX('LSN', Metrics)+5, 30),':','') [First LSN]
--, SUBSTRING(Metrics, CHARINDEX('LSN', Metrics)+5, 30) [First LSN]

FROM @T 
) A

INNER JOIN msdb.dbo.backupset b 
	ON b.database_name = A.DBName			--mf.media_set_id = b.media_set_id 
	--AND REPLACE(b.first_lsn,'0','')  = A.[First LSN]

INNER JOIN msdb.dbo.backupmediafamily c
	ON c.media_set_id = b.media_set_id 

Where database_name not in ('model','master','tempdb','msdb')

ORDER BY 1;
--where database_name = 'PRV_BritishCouncil_LocalScan'






SELECT DISTINCT
	b.backup_finish_date
	, b.backup_start_date
	, first_lsn
	, REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(first_lsn, '0',':'), '00',':'), '000',':'), '0000',':'), '00000',':'), ':','')
    , name
	, database_name
	, compressed_backup_size
FROM   msdb.dbo.backupmediafamily mf
INNER JOIN msdb.dbo.backupset b ON mf.media_set_id = b.media_set_id 
where database_name = 'PRV_BritishCouncil_LocalScan'



/*


--LEFT JOIN msdb.dbo.backupmediafamily mf
--INNER JOIN msdb.dbo.backupset b ON mf.media_set_id = b.media_set_id 


SELECT b.backup_finish_date
	, first_lsn
	, REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(first_lsn, '0',':'), '00',':'), '000',':'), '0000',':'), '00000',':'), ':','')
    , *

FROM   msdb.dbo.backupmediafamily mf
INNER JOIN msdb.dbo.backupset b ON mf.media_set_id = b.media_set_id 
where database_name = 'PRV_BritishCouncil_LocalScan'
	and first_lsn = '232787000000581500258'

--232787:5815:258
--232787000000581500258
--2327875815258
--2327875815258


2327875815258
2327875815258

*/