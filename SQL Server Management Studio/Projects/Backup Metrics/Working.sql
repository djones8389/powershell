DECLARE @Backups TABLE(logDate datetime, ProcessInfo nvarchar(20), text nvarchar(2000))
INSERT @Backups
EXEC sp_readerrorlog 0, 1, 'Back'

DECLARE @T TABLE(logDate datetime,Metrics varchar(1000));

INSERT INTO @T 
SELECT logDate
	, SUBSTRING(text, 0, CHARINDEX(', last',text))

FROM @Backups
where processInfo = 'Backup'
	AND text not like '%Differential%'
	AND text not like '%Log%'

SELECT DISTINCT
	A.DBName
	, a.Duration
	, A.Pages
	, compressed_backup_size
	, backup_start_date
	, compressed_backup_size/1024 BackupSizeInKB
	, compressed_backup_size/1024/1024 BackupSizeInMB
	, CASE WHEN name = 'CommVault Galaxy Backup' THEN 'Rackspace Backup' ELSE 'Maintenance Backup' END AS [Backup Type]
FROM
(
SELECT  
 SUBSTRING(Metrics, CHARINDEX(':', Metrics)+2, CHARINDEX(',', Metrics)-CHARINDEX(':', Metrics)-2) [DBName]
, SUBSTRING(Metrics, CHARINDEX(',', Metrics)+34, 8) [Duration]
, SUBSTRING(Metrics, CHARINDEX('umped:', Metrics)+6,  CHARINDEX(', first', Metrics)-CHARINDEX('umped:', Metrics)-6) [Pages]
, REPLACE(SUBSTRING(Metrics, CHARINDEX('LSN', Metrics)+5, 30),':','') [First LSN]

FROM @T 
) A

INNER JOIN msdb.dbo.backupset b 
	ON b.database_name = A.DBName				
	AND REPLACE(a.[First LSN], '0', '') =  REPLACE(b.first_lsn, '0', '')
INNER JOIN msdb.dbo.backupmediafamily c
	ON c.media_set_id = b.media_set_id 

Where database_name not in ('model','master','tempdb','msdb')

ORDER BY 1;

