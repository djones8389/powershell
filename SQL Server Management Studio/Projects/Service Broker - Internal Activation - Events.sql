use [master];

ALTER DATABASE [msdb] SET ENABLE_BROKER WITH NO_WAIT;

--select 'kill ' + cast(spid as varchar(5)) + ';'
--	, loginame
--	 , db_name(dbid) [databasename]
--from sys.sysprocesses a
--where db_name(dbid) = 'msdb'
--order by 3

use [msdb];

DROP EVENT NOTIFICATION [DBANotification] ON SERVER

IF EXISTS(select 1 from sys.objects where name = 'ProcessDBAQueue' and type = 'P')
 DROP PROCEDURE ProcessDBAQueue

IF EXISTS(SELECT TOP(1) 1 FROM sys.services WHERE [name] = N'DBAService')
 DROP SERVICE [DBAService];

IF EXISTS(SELECT TOP(1) 1 FROM sys.service_contracts WHERE [name] = N'SendMail')
 DROP CONTRACT [SendMail];

IF EXISTS(SELECT TOP(1) 1 FROM sys.service_message_types WHERE [name] = N'ReplyMessage')
 DROP MESSAGE TYPE [ReplyMessage];

IF EXISTS(SELECT TOP(1) 1 FROM sys.service_message_types WHERE [name] = N'RequestMessage')
 DROP MESSAGE TYPE [RequestMessage];
 
IF EXISTS(SELECT TOP(1) 1 FROM sys.service_queues WHERE [name] = N'DBAQueue')
 DROP QUEUE [DBAQueue];

IF EXISTS(SELECT TOP(1) 1 FROM sys.routes WHERE [name] = N'DBARoute')
 DROP ROUTE [DBARoute];

IF EXISTS(SELECT TOP(1) 1 FROM sys.event_notifications WHERE [name] = N'DBANotification')
 DROP EVENT NOTIFICATION [DBANotification] ON QUEUE [DBAQueue];
 
CREATE MESSAGE TYPE [RequestMessage] validation = WELL_FORMED_XML;
CREATE MESSAGE TYPE [ReplyMessage] validation = WELL_FORMED_XML;

CREATE CONTRACT [SendMail] (
	[RequestMessage] SENT BY INITIATOR,
	[ReplyMessage] SENT BY TARGET
);

CREATE QUEUE DBAQueue;

CREATE SERVICE [DBAService]  ON QUEUE [dbo].[DBAQueue] ([http://schemas.microsoft.com/SQL/Notifications/PostEventNotification])
GO

CREATE ROUTE [DBARoute] WITH SERVICE_NAME = N'DBAService' 
	,  ADDRESS  = N'LOCAL' 
GO

ALTER SERVICE [DBAService] (
	ADD CONTRACT [SendMail]
);
GO

DECLARE @Event NVARCHAR(MAX)='';
SELECT @Event +=CHAR(13) + '
CREATE EVENT NOTIFICATION [DBANotification]  
ON SERVER  
FOR DDL_USER_EVENTS,DDL_ROLE_EVENTS,DDL_TABLE_EVENTS,DEADLOCK_GRAPH 
TO SERVICE ''DBAService'', '''+cast(service_broker_guid as nvarchar(MAX))+'''
'
FROM (SELECT service_broker_guid 
FROM sys.databases 
where name = 'msdb'
) A
EXEC(@Event);
GO


IF EXISTS (select 1 from sys.tables where name = 'Auditing')
	DROP TABLE Auditing;
			
CREATE TABLE Auditing(
	[EventTime] datetime DEFAULT(GETDATE())
	,[EventAction] xml);
GO

CREATE PROCEDURE [dbo].[ProcessDBAQueue]
AS
BEGIN
SET NOCOUNT ON;

DECLARE @conversation_handle UNIQUEIDENTIFIER,
		@message_type_name NVARCHAR(256),
		@message_body XML;

WHILE (1 = 1)
BEGIN
	BEGIN TRY
		
		BEGIN TRANSACTION;
		
		WAITFOR (
			RECEIVE TOP(1) @conversation_handle = [conversation_handle]
				,@message_type_name = message_type_name
				,@message_body = CAST(message_body AS XML)
			FROM msdb..DBAQueue
			), TIMEOUT 500; --Milliseconds

		IF(@@ROWCOUNT = 0)
		BEGIN
			--No messages
			ROLLBACK TRANSACTION;
			BREAK;
		END

		--Only process post notifications
		--IF(@message_type_name = N'http://schemas.microsoft.com/SQL/Notifications/EventNotification')
		BEGIN
			--Get message details into friendly HTML email format

			--Extract event information
			DECLARE @EventType NVARCHAR(50) = @message_body.value('(/EVENT_INSTANCE/EventType)[1]', 'nvarchar(50)')
				,@ServerName SYSNAME = @message_body.value('(/EVENT_INSTANCE/ServerName)[1]', 'sysname');

			--Create holding table for email body
			DECLARE @EmailBody TABLE
				(Title NVARCHAR(100), Value NVARCHAR(MAX));

			--Bod event generic details into holding table
			INSERT INTO @EmailBody(Title, Value)
			VALUES(N'Event', @EventType)
				,(N'Date', @message_body.value('(/EVENT_INSTANCE/PostTime)[1]', 'nvarchar(50)'))
				,(N'Server', @ServerName)
				,(N'Login Name', @message_body.value('(/EVENT_INSTANCE/LoginName)[1]', 'nvarchar(50)'));

			--CREATE/DROP_DATABASE
			IF @EventType LIKE N'%[_]DATABASE'
			BEGIN
				--Bob event specific details into holding table
				INSERT INTO @EmailBody(Title, Value)
					VALUES(N'Database', @message_body.value('(/EVENT_INSTANCE/DatabaseName)[1]', 'nvarchar(50)'))
						,(N'Command', @message_body.value('(/EVENT_INSTANCE/TSQLCommand/CommandText)[1]', 'nvarchar(max)'));
				
				--Skip to sending the email
				GOTO SENDEMAIL;
			END

			--CREATE/DROP_LOGIN
			IF @EventType LIKE N'%[_]LOGIN'
			BEGIN
				--Bob event specific details into holding table
				INSERT INTO @EmailBody(Title, Value)
					VALUES(N'Object Name', @message_body.value('(/EVENT_INSTANCE/ObjectName)[1]', 'sysname'))
						,(N'Object Type', @message_body.value('(/EVENT_INSTANCE/ObjectType)[1]', 'sysname'))
						,(N'Default Database', @message_body.value('(/EVENT_INSTANCE/DefaultDatabase)[1]', 'sysname'))
						,(N'Login Type', @message_body.value('(/EVENT_INSTANCE/LoginType)[1]', 'nvarchar(50)'))
						,(N'SID', @message_body.value('(/EVENT_INSTANCE/SID)[1]', 'nvarchar(max)'))
						,(N'Command', @message_body.value('(/EVENT_INSTANCE/TSQLCommand/CommandText)[1]', 'nvarchar(max)'));
				
				--Skip to sending the email
				GOTO SENDEMAIL;
			END

			--CREATE/DROP_USER
			IF @EventType LIKE N'%[_]USER'
			BEGIN
				--Bob event specific details into holding table
				INSERT INTO @EmailBody(Title, Value)
					VALUES(N'User Name', @message_body.value('(/EVENT_INSTANCE/UserName)[1]', 'sysname'))
						,(N'Database', @message_body.value('(/EVENT_INSTANCE/DatabaseName)[1]', 'sysname'))
						,(N'Object Name', @message_body.value('(/EVENT_INSTANCE/ObjectName)[1]', 'sysname'))
						,(N'Object Type', @message_body.value('(/EVENT_INSTANCE/ObjectType)[1]', 'sysname'))
						,(N'Default Schema', @message_body.value('(/EVENT_INSTANCE/DefaultSchema)[1]', 'sysname'))
						,(N'SID', @message_body.value('(/EVENT_INSTANCE/SID)[1]', 'nvarchar(max)'))
						,(N'Command', @message_body.value('(/EVENT_INSTANCE/TSQLCommand/CommandText)[1]', 'nvarchar(max)'));
				
				--Skip to sending the email
				GOTO SENDEMAIL;
			END

			--ADD{_SERVER}_ROLE_MENBER
			IF @EventType LIKE N'%[_]ROLE[_]MEMBER'
			BEGIN
				IF @EventType LIKE N'%[_]SERVER[_]ROLE[_]MEMBER'
				BEGIN
					--Bob event specific details into holding table
					INSERT INTO @EmailBody(Title, Value)
						VALUES(N'Object Name', @message_body.value('(/EVENT_INSTANCE/ObjectName)[1]', 'sysname'))
							,(N'Object Type', @message_body.value('(/EVENT_INSTANCE/ObjectType)[1]', 'sysname'))
							,(N'Default Database', @message_body.value('(/EVENT_INSTANCE/DefaultDatabase)[1]', 'sysname'))
							,(N'Login Type', @message_body.value('(/EVENT_INSTANCE/LoginType)[1]', 'nvarchar(50)'))
							,(N'SID', @message_body.value('(/EVENT_INSTANCE/SID)[1]', 'nvarchar(max)'))
							,(N'Role Name', @message_body.value('(/EVENT_INSTANCE/RoleName)[1]', 'sysname'))
							,(N'Role SID', @message_body.value('(/EVENT_INSTANCE/RoleSID)[1]', 'nvarchar(50)'))
							,(N'Command', @message_body.value('(/EVENT_INSTANCE/TSQLCommand/CommandText)[1]', 'nvarchar(max)'));
					
					--Skip to sending the email
					GOTO SENDEMAIL;
				END
				ELSE
					--Bob event specific details into holding table
					INSERT INTO @EmailBody(Title, Value)
						VALUES(N'User Name', @message_body.value('(/EVENT_INSTANCE/UserName)[1]', 'sysname'))
							,(N'Database', @message_body.value('(/EVENT_INSTANCE/DatabaseName)[1]', 'sysname'))
							,(N'Object Name', @message_body.value('(/EVENT_INSTANCE/ObjectName)[1]', 'sysname'))
							,(N'Object Type', @message_body.value('(/EVENT_INSTANCE/ObjectType)[1]', 'sysname'))
							,(N'Default Schema', @message_body.value('(/EVENT_INSTANCE/DefaultSchema)[1]', 'sysname'))
							,(N'SID', @message_body.value('(/EVENT_INSTANCE/SID)[1]', 'nvarchar(max)'))
							,(N'Role Name', @message_body.value('(/EVENT_INSTANCE/RoleName)[1]', 'sysname'))
							,(N'Command', @message_body.value('(/EVENT_INSTANCE/TSQLCommand/CommandText)[1]', 'nvarchar(max)'));
					
					--Skip to sending the email
					GOTO SENDEMAIL;
			END

			--DEADLOCK_GRAPH
			IF @EventType = N'DEADLOCK_GRAPH'
			BEGIN
				--Bob event specific details into holding table
				INSERT INTO @EmailBody(Title, Value)
					VALUES(N'Deadlock', CAST(@message_body.query('/EVENT_INSTANCE/TextData') AS NVARCHAR(MAX)));
				
				--Skip to sending the email
				GOTO SENDEMAIL;
			END

			--Send an email
			SENDEMAIL:
			


			INSERT Auditing ([EventAction])
			VALUES(@message_body);
			
			/*

			--Get details for the email
			DECLARE @Recipients VARCHAR(MAX) = N'DBAdmin@btl.com'
				,@Subject NVARCHAR(255) = (@EventType+N' - '+@ServerName)
				,@Body NVARCHAR(MAX)
				,@BodyFormat VARCHAR(20) = N'HTML'
				,@Importance VARCHAR(6) = N'High';

			--Build the body dynamically from holding table
			SET @Body = CAST(
				(
				SELECT (
					SELECT td = Title, N''
						,td = Value, N''
					FROM @EmailBody
					FOR XML PATH('tr'), TYPE
					)
				FOR XML PATH('table'), TYPE
				)
			AS NVARCHAR(MAX));
			
			DELETE FROM @EmailBody;

			--Get dbmail profile name
			DECLARE @profile_name SYSNAME;
			SELECT TOP(1) @profile_name = name FROM msdb.dbo.sysmail_profile

			--Actually send the email
			EXECUTE	msdb.dbo.sp_send_dbmail
					@profile_name = @profile_name,
					@recipients = @Recipients,
					@subject = @Subject,
					@importance = @Importance,
					@body_format = @BodyFormat,
					@body = @Body,
					@exclude_query_output = 1;

			*/

		END

		COMMIT TRANSACTION;

	END TRY
	BEGIN CATCH
		--Cleanup
		ROLLBACK TRANSACTION;
		BREAK;
	END CATCH
END
END

ALTER QUEUE DBAQueue
	WITH STATUS = ON
		,ACTIVATION (
			PROCEDURE_NAME = ProcessDBAQueue
			,EXECUTE AS SELF
				,STATUS = ON
				,MAX_QUEUE_READERS = 1
			);


--SELECT * FROM sys.dm_broker_activated_tasks;
--SELECT * FROM sys.dm_broker_connections;
--SELECT * FROM sys.dm_broker_forwarded_messages;
--SELECT * FROM sys.dm_broker_queue_monitors;
--SELECT * FROM sys.transmission_queue;
--SELECT * FROM sys.conversation_endpoints;
--SELECT * FROM sys.conversation_groups;
--SELECT * FROM sys.service_message_types;
--SELECT * FROM sys.service_queue_usages;
--SELECT * FROM sys.services;

--truncate table Auditing

select * 
from Auditing