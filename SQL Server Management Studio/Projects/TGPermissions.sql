exec sp_msforeachdb '

use [?];

if (''?'' = ''PRV_SADB'')

Select   
	CASE 
	   WHEN @@servername = ''343553-WEB5'' then ''AQA''
   	   WHEN @@servername = ''408302-AQAREPOR'' then ''AQA''
	   WHEN @@servername = ''430069-AAT-SQL\SQL1'' then ''AAT''
	   WHEN @@servername = ''430327-AAT-SQL2\SQL2'' then ''AAT''
	   WHEN @@servername = ''403252-ACTVARIE'' then ''Actuaries''
	   WHEN @@servername = ''430088-BC-SQL\SQL1'' then ''BritishCouncil''
	   WHEN @@servername = ''430326-BC-SQL2\SQL2'' then ''BritishCouncil''
	   WHEN @@servername = ''420304-EAL2-LIV'' then ''EAL''
	   WHEN @@servername = ''553092-PRDSQL'' then ''Editions Live''
	   WHEN @@servername = ''524778-SQL'' then ''Editios Mirror''
	   WHEN @@servername = ''407710-WEB3'' then ''NCFE''
	   WHEN @@servername = ''425732-OCR-SQL\SQL1'' then ''OCR''
	   WHEN @@servername = ''430325-OCR-SQL2\SQL2'' then ''OCR''
	   WHEN @@servername = ''704276-PREVSTA1'' then ''PRP-PRV''
	   WHEN @@servername = ''338301-WEB4'' then ''Skillsfirst''
	   WHEN @@servername = ''335657-APP1'' then ''SQA''
	   WHEN @@servername = ''335658-APP2'' then ''SQA''
	   WHEN @@servername = ''311619-WEB3'' then ''WJEC''
		    ELSE ''ServerName''
		END AS ''ServerName''
  -- , ''?'' as ''DatabaseName'' 	
	    ,ID
		, Name
		, GlobalCentre
		, count(UserID) as NumUserCentreAssociationsWithOneQualLevelButNotTheOther	
FROM (
	SELECT A.ID
		, A.Name
		, A.UserID
		, case when A.CentreId = 1 then 1 else 0 end as GlobalCentre
		, case when A.hasquallevel1 <> A.hasquallevel0 then 1 else 0 end as hasOneLevelButNotTheOther
	FROM (
	  select 
			PermissionsTable.ID, 
			PermissionsTable.Name, 
			UserID, 
			CentreId, 
			CASE WHEN MAX(QualificationLevel) = 1 THEN 1 ELSE 0 END HasQualLevel1, 
			CASE WHEN MIN(QualificationLevel) = 0 THEN 1 ELSE 0 END HasQualLevel0
	  from PermissionsTable
	  left join RolePermissionsTable
	  on RolePermissionsTable.PermissionID = PermissionsTable.ID
	  inner join AssignedUserRolesTable
	  on AssignedUserRolesTable.RoleID = RolePermissionsTable.RoleID
	  where UsesQualLevel = 1
	  group by PermissionsTable.ID, PermissionsTable.Name, UserID, CentreId
	  )   A
  
) B

where hasOneLevelButNotTheOther = 1
group by ID, Name, GlobalCentre
order by ID


'