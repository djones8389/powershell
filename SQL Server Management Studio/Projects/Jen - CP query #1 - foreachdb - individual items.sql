use master

IF OBJECT_ID('tempdb..##Global') IS NOT NULL DROP TABLE ##Global;

CREATE TABLE ##Global (

	Servername sysname
    , DatabaseName sysname
	, ProjectID int
	, ProjectName nvarchar(200)
	, ItemID nvarchar(20)
	, ext nvarchar(200)
	--, NumPages int
	, TotalCount bigint
);

DECLARE @DYNAMIC NVARCHAR(MAX) = ''

select @DYNAMIC +=CHAR(13) +
'use ' +quotename(name)+ ';

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

BEGIN

	if OBJECT_ID(''tempdb..[#'+name+']'') is not null drop table [#'+name+'];

	CREATE TABLE [#'+name+'] (
		ItemID NVARCHAR(20)
	);

	INSERT [#'+name+']
	select 
		cast(Projectlisttable.id as varchar(5))+ ''P'' + project.page.value(''@ID'',''nvarchar(12)'') as PageID
	from '+quotename(name)+'.dbo.Projectlisttable

	cross apply ProjectStructureXml.nodes(''Pro//Pag'') project(page)

	INNER JOIN '+quotename(name)+'.dbo.PageTable 
	on PageTable.ParentID = Projectlisttable.id
		AND PageTable.ID = cast(Projectlisttable.ID as nvarchar(10)) + ''P'' + cast(project.page.value(''@ID'',''nvarchar(12)'') as nvarchar(10))

	where project.page.value(''@sta'',''int'') Between PublishSettingsXML.value(''(/PublishConfig/StatusLevelFrom)[1]'',''int'') and PublishSettingsXML.value(''(/PublishConfig/StatusLevelTo)[1]'',''int'')

	EXCEPT

	select 
		cast(Projectlisttable.id as varchar(5))+ ''P'' + project.page.value(''@ID'',''nvarchar(12)'') as PageID
	from '+quotename(name)+'.dbo.Projectlisttable

	cross apply ProjectStructureXml.nodes(''Pro/Rec//Pag'') project(page)

	INNER JOIN '+quotename(name)+'.dbo.PageTable 
	on PageTable.ParentID = Projectlisttable.id
		AND PageTable.ID = cast(Projectlisttable.ID as nvarchar(10)) + ''P'' + cast(project.page.value(''@ID'',''nvarchar(12)'') as nvarchar(10))

	where project.page.value(''@sta'',''int'') Between PublishSettingsXML.value(''(/PublishConfig/StatusLevelFrom)[1]'',''int'') and PublishSettingsXML.value(''(/PublishConfig/StatusLevelTo)[1]'',''int'')

	
	SELECT DISTINCT @@ServerName ServerName
		,db_name() DBName
		, PLT.ID ProjectID
		, PLT.Name ProjectName
		, ItemID
		, A.ext
		, (SELECT COUNT(1) FROM [#'+name+']) TotalCount
	FROM (
	select  p.ID ItemID
		   , ext
	from '+quotename(name)+'.dbo.ItemCustomQuestionTable i
	INNER JOIN '+quotename(name)+'.dbo.ComponentTable c on i.ParentID = c.ID
	INNER JOIN '+quotename(name)+'.dbo.SceneTable s on s.ID = c.ParentID
	INNER JOIN '+quotename(name)+'.dbo.PageTable p on p.id = s.Parentid
	INNER JOIN [#'+name+'] a on a.ItemID = p.id
	INNER JOIN '+quotename(name)+'.dbo.ProjectListTable pr on pr.ID = p.ParentID
	where ext in (''barchart'', ''piechart'', ''scattergraph'', ''selfDraw_lineGraph'', ''proof_reading'')

	UNION ALL

	select SUBSTRING(ID, 0, CHARINDEX(''S'',ID)) ItemID
		, ''TextSelector''
	from '+quotename(name)+'.dbo.ItemTextSelectorTable
	where SUBSTRING(ID, 0, CHARINDEX(''S'',ID)) in (select itemid from [#'+name+'])

	UNION ALL

	select SUBSTRING(ID, 0, CHARINDEX(''S'',ID)) ItemID
		, ''DragAndDrop''
	from '+quotename(name)+'.dbo.[ItemDragAndDropTable]
	where SUBSTRING(ID, 0, CHARINDEX(''S'',ID)) in (select itemid from [#'+name+'])
	) A
	INNER JOIN '+quotename(name)+'.dbo.ProjectListTable PLT
	on PLT.ID = SUBSTRING(A.ItemID, 0, CHARINDEX(''P'',A.ItemID))
	order by 1;
END
'
from sys.databases
where name like '%CPProjectAdmin%' 	or name like '%ContentProducer%' and name != 'SHA_QEYADAH_ContentProducer'

INSERT ##Global
exec(@DYNAMIC);

SELECT
	Servername 
    , DatabaseName 
	, ProjectID 
	, ProjectName
	, ItemID 
	, ext 
	, TotalCount 
FROM ##Global