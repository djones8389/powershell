use SANDBOX_Abcawards_ContentAuthor;


--SELECT *
--FROM SubjectTagValueItems STVI
--INNER JOIN SubjectTagValues STV
--ON STVI.SubjectTagValue_Id = STV.Id
--WHERE Item_Id = 5222

SELECT STV.Id
	, STV.SubjectTagTypeId
	, stv.Text
	, stv.Deleted
	, STVI.Item_Id 
	, ROW_NUMBER() OVER(PARTITION BY SubjectTagTypeId, [text], STV.deleted ORDER BY SubjectTagTypeId) R
FROM SubjectTagValues STV
LEFT JOIN SubjectTagValueItems STVI
ON STV.Id = STVI.SubjectTagValue_Id
INNER JOIN SubjectTagTypes STT
ON STV.SubjectTagTypeId = STT.Id
WHERE TEXT = 'Understand the requirements relating to health, safety and good housekeeping in a vehicle test centre'
AND SubjectTagTypeId = 256
AND STT.TagTypeKey = 1




--SELECT STV.Id
--	, STV.SubjectTagTypeId
--	, stv.Text
--	, stv.Deleted
--	, STVI.Item_Id 
--	, ROW_NUMBER() OVER(PARTITION BY SubjectTagTypeId, [text] ORDER BY SubjectTagTypeId) R
--FROM SubjectTagValues STV
--LEFT JOIN SubjectTagValueItems STVI
--ON STV.Id = STVI.SubjectTagValue_Id
--where stv.id = 426


select A.*
FROM (
select  STV.[ID]
		,SubjectTagTypeId
		,[text]
		, STV.Deleted
		, ROW_NUMBER() OVER(PARTITION BY STV.[text], STV.Deleted ORDER BY STV.id) R
from SubjectTagValues STV
INNER JOIN SubjectTagTypes STT
ON STV.SubjectTagTypeId = STT.Id
where [text] = 'Be able to work safely when carrying out vehicle test (App C)'
AND STT.TagTypeKey = 1
) A

