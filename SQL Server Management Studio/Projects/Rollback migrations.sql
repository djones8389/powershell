USE msdb;
GO

DECLARE @myjobname nvarchar(200) = 'Rollback-Migration - Rollback PRV_BCGuilds_SecureAssess';

EXEC dbo.sp_add_job
    @job_name = @myjobname ;
--GO


DECLARE @RestoreSA nvarchar(MAX) = '';

SELECT @RestoreSA = '

RESTORE DATABASE [PRV_BCGuilds_SecureAssess]
FROM DISK = N''T:\BACKUP\PRV.BCGuilds.SA.2016.03.18.bak''
WITH FILE = 1
	,NOUNLOAD
	,NOREWIND
	,REPLACE
	,STATS = 10
	,MOVE ''UAT_BCGuilds_SecureAssess_11_7_223_0_IP'' TO N''S:\DATA\PRV_BCGuilds_SecureAssess.mdf''
	,MOVE ''UAT_BCGuilds_SecureAssess_11_7_223_0_IP_log'' TO N''L:\LOGS\PRV_BCGuilds_SecureAssess.ldf'';
'

EXEC sp_add_jobstep
    @job_name = @myjobname,
    @step_name = N'Rollback SecureAssess',
    @subsystem = N'TSQL',
    @database_name = N'msdb',
    @on_success_action = 1,
    @on_fail_action = 2,
    @command = @RestoreSA , 
    @retry_attempts = 0,
    @retry_interval = 0;
--GO

EXEC dbo.sp_add_jobserver
    @job_name = @myjobname
--GO