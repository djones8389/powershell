SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

IF OBJECT_ID('tempdb..#BadEntries') IS NOT NULL DROP TABLE #BadEntries;
IF OBJECT_ID('tempdb..#BadXMLs') IS NOT NULL DROP TABLE #BadXMLs;

select  
      PLT.id as ProjectID
	  , pro.pag.value('@ID','nvarchar(100)') as PageID      
      , pro.pag.value('@markingSchemeFor','nvarchar(100)')  as markingSchemeFor
INTO #BadEntries
from ProjectListTable PLT

cross apply ProjectStructureXml.nodes('Pro//Pag') pro(pag)

left join PageTable PT
on PT.ID = convert(nvarchar(5), PLT.ID) + 'P' +  convert(nvarchar(5),pro.pag.value('@markingSchemeFor','nvarchar(100)'))

where 	
	pro.pag.value('@markingSchemeFor','nvarchar(100)') != '-1'
	and 
	pro.pag.value('@markingSchemeFor','nvarchar(100)') IS NOT NULL
	and 
	pt.id IS NULL;

SELECT ID
	, ProjectStructureXml
INTO #BadXMLs
FROM ProjectListTable
WHERE ID IN (select ProjectID from #BadEntries)

--Original XMLs--
select ID
,	 ProjectStructureXml 
from #BadXMLs;


DECLARE RemoveNodes CURSOR FOR 
SELECT ProjectID
	, PageID
FROM #BadEntries


DECLARE @ProjectID int
	, @PageID int

OPEN RemoveNodes

FETCH NEXT FROM RemoveNodes INTO @ProjectID, @PageID

WHILE @@FETCH_STATUS = 0

BEGIN

UPDATE #BadXMLs
set ProjectStructureXml.modify('delete(/Pro//Pag[@ID=sql:variable("@PageID")])')
where ID = @ProjectID

FETCH NEXT FROM RemoveNodes INTO @ProjectID, @PageID

END
CLOSE RemoveNodes
DEALLOCATE RemoveNodes

--New XMLs--
select ID
,	 ProjectStructureXml 
from #BadXMLs;

/*
UPDATE ProjectListTable
set ProjectStructureXml = B.ProjectStructureXml
FROM ProjectListTable A
INNER JOIN #BadXMLs B
on A.ID = B.ID;
*/