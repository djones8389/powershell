use Windesheim_SecureAssess


select 1
from ExamSessionTable est 
inner join usertable ut on ut.id = est.userid
where candidateref = 'S1099885'

SELECT WEST.ID, WEST.KeyCode, WUT.Forename, WUT.Surname, WUT.CandidateRef, WCT.CentreName, WSCET.examName, WEST.completionDate
	, WESTS.ExternalReference, examstatechangeauditxml, west.WarehouseExamState
	, StructureXML, usermark, userPercentage
FROM WAREHOUSE_ExamSessionTable as WEST (NOLOCK)

  Inner Join WAREHOUSE_ScheduledExamsTable as WSCET (NOLOCK)
  on WSCET.ID = WEST.WAREHOUSEScheduledExamID
  
  Inner Join WAREHOUSE_UserTable as WUT (NOLOCK)
  on WUT.ID = WEST.WAREHOUSEUserID

  Inner Join WAREHOUSE_CentreTable as WCT (NOLOCK)
  on WCT.ID = WSCET.WAREHOUSECentreID

  Inner Join WAREHOUSE_ExamSessionTable_Shreded as WESTS (NOLOCK)
  on WESTS.examSessionId = WEST.ID

where candidateref = 'S1099885'
	--and WESTS.ExternalReference = '01 VPP01DS120334 D1M7G'
	order by 1 desc;
