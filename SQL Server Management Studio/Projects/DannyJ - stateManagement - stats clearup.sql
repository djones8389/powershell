SELECT DISTINCT [stateid] FROM [StateManagement_DJ]  order by 1

SELECT TOP 2 B.[Count]	
	, b.Stateid
	, b.TS
FROM (
	SELECT A.*
		, ROW_NUMBER() OVER(PARTITION BY [Count] ORDER BY TS DESC) R
		--,RANK() OVER (ORDER BY ts desc) AS Rank 
	FROM (
		SELECT [count]
			, stateid
			, CONVERT(VARCHAR(10), [timestamp], 103) + ' '  + convert(VARCHAR(5), [timestamp], 14) TS
		FROM [StateManagement_DJ] 
		where stateid = 1	
	) A
) B
where R = 1
order by ts desc



/*

SELECT A.*
	, ROW_NUMBER() OVER(PARTITION BY [Count] ORDER BY TS DESC) R
	,RANK() OVER (ORDER BY ts desc) AS Rank 
FROM (
	SELECT  count, CONVERT(VARCHAR(10), [timestamp], 103) + ' '  + convert(VARCHAR(5), [timestamp], 14) TS
	FROM [StateManagement_DJ] 
	where stateid = 15	
) A

order by ts desc

*/



















/*SELECT *
	, ROW_NUMBER() over (partition by stateid, [count] order by timestamp) R
FROM [StateManagement_DJ] 
order by R --timestamp desc, stateid asc




SELECT *
	--, ROW_NUMBER() over (partition by stateid, [count] order by timestamp desc) R
FROM [StateManagement_DJ] 
where stateid = 15
	order by ID


SELECT B.*
FROM (
	SELECT A.*
		, ROW_NUMBER() OVER (PARTITION BY Count, stateid ORDER BY TS desc) R
	FROM (
	SELECT [Count]
		, stateid
		--, cast(timestamp as datetime) TS
		, CONVERT(VARCHAR(10), [timestamp], 103) + ' '  + convert(VARCHAR(5), [timestamp], 14) TS
		--, count(ID)
	FROM [StateManagement_DJ] 
	--where stateid = 15
		group by [Count],stateid,CONVERT(VARCHAR(10), [timestamp], 103) + ' '  + convert(VARCHAR(5), [timestamp], 14)
	
	) A
	order by 2
) B
WHERE R = 1



if object_id('tempdb..#maxDates') is not null drop table #maxDates;

create table #maxDates (
	[state] tinyint
	, [timestamp] datetime
);

insert #maxDates
SELECT  stateid	
	,MAX([timestamp])
FROM [StateManagement_DJ] 
group by stateid

create clustered index [ix_stateid] on #maxDates (state)

select * 
from #maxDates
order by 2

select MAX([timestamp])
from #maxDates

SELECT *
FROM [StateManagement_DJ] A
where timestamp > (SELECT MAX([timestamp]) from #maxDates)



*/