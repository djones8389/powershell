DECLARE @Keycode nvarchar(10) = 'J3V9XHA6'

SELECT a.*
	, a.Data.value('data(data)[1]','nvarchar(MAX)') [Data]
	, b.Description
FROM WAREHOUSE_ExamSessionCandidateInteractionLogsTable a
inner join ExamSessionCandidateInteractionEventsLookupTable b 
on a.EventType = b.EventType
where ExamSessionID in (

	SELECT ID
	FROM WAREHOUSE_ExamSessionTable
	where Keycode = @Keycode 
)


