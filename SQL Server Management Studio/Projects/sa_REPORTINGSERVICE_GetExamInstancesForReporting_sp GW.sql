/****** Script for SelectTopNRows command from SSMS  ******/
SELECT [Instance]
      ,[DBName]
      ,[cached_time]
      ,[last_execution_time]
      ,[execution_count]
      ,[CPU]
	  ,cast(([CPU]/[execution_count]) as float)/1000000 [AVG_CPU (S)]
      ,[ELAPSED]
	  ,cast(([ELAPSED]/[execution_count]) as float)/1000000 [AVG_ElapsedTime (S)]
      ,[LOGICAL_READS]
      ,[LOGICAL_WRITES]
      ,[PHYSICAL_READS]
      ,[StartDate] [CollectionDate]
  FROM [PSCollector].[dbo].[Sproc Metrics]
  where [OBJECT_NAME] = 'sa_REPORTINGSERVICE_GetExamInstancesForReporting_sp'
	and StartDate  > '2018-02-03'
	order by ([CPU]/[execution_count]) desc;