use Saxion_SecureAssess

DECLARE @examInstanceId   int  = 262893

SET STATISTICS IO ON 
--BEGIN  
  
DECLARE @errorReturnString  nvarchar(max)  
DECLARE @errorNum  nvarchar(100)  
DECLARE @errorMess  nvarchar(max)  
DECLARE @myExamState int
DECLARE @structureXml xml
DECLARE @defaultDuration INT
DECLARE @computedTotalDuration INT
DECLARE @durationMode INT
DECLARE @enableCandidateBreak TINYINT
DECLARE @unscheduledBreakMinutes INT
DECLARE @unscheduledBreakMinutesUsed INT
DECLARE @candidateBreakStyle INT
DECLARE @scheduledBreakType INT
DECLARE @breakPooledTime INT
DECLARE @maximumNumberOfBreaksPerSection INT
DECLARE @totalBreakTime INT
DECLARE @totalBreakTimeUsed INT
DECLARE @isUnlimitedBreaks TINYINT
DECLARE @totalSectionBreakTime INT
DECLARE @candidateBreakExtraDurationType INT
DECLARE @candidateBreakExtraDuration INT
DECLARE @numberOfExtraBreaksPerSection INT
DECLARE @candidateBreakExtraBreakReasonType INT
DECLARE @candidateBreakExtraDurationReasonText nvarchar(100)
DECLARE @candidateBreakExtraDurationReasonOther nvarchar(250)
DECLARE @originalBreakPooledTime INT
DECLARE @originalMaximumNumberOfBreaksPerSection INT
DECLARE @originalTotalBreakTime INT		
DECLARE @extendedXml XML
DECLARE @customRoundingEnabled INT
DECLARE @customRoundingDigits INT
DECLARE @rowId INT
DECLARE @assessmentSectionType INT
DECLARE @sectionId INT
DECLARE @duration INT
DECLARE @overrideLockdown INT
DECLARE @hasSectionBreak TINYINT
DECLARE @breakDuration INT
DECLARE @originalBreakDuration INT
DECLARE @cancellableBreak TINYINT
DECLARE @numberOfBreaksUsed SMALLINT
DECLARE @scheduledBreakStarted TINYINT
DECLARE @sectionSelectorID INT,
		@description NVARCHAR(50),
		@sectionSelected TINYINT,
		@forwardOnly TINYINT,
		@requiredResponse TINYINT,
		@isBranchingSection TINYINT,
		@adaptiveEnabled BIT,
		@adaptiveType  NVARCHAR(50),
		@noReturnToSection TINYINT,
		@forwardOnlyAllowItemReview TINYINT,
		@forwardOnlyShowBreadcrumbs TINYINT

DECLARE @TEMP_EXAM_SECTIONS TABLE (
	[RowID] INT IDENTITY(1, 1) NOT NULL
	,[ExamSessionId] INT
	,[SectionID] INT
	,[ExamSectionType] INT
	,[Duration] INT
	,[OverrideLockdown] BIT
	,[HasSectionBreak] BIT
	,[BreakDuration] INT
	,[OriginalBreakDuration] INT
	,[CancellableBreak] BIT
	,[NumberOfBreaksUsed] SMALLINT
	,[ScheduledBreakStarted] BIT
	,[Description] NVARCHAR(50)
	,[SectionSelectorID] INT
	,SectionSelected BIT
	,ForwardOnly BIT
	,RequiredResponse BIT
	,IsBranchingSection BIT
	,[AdaptiveEnabled] BIT
	,[AdaptiveType] NVARCHAR(50)
	,[NoReturnToSection] BIT
	,[ForwardOnlyAllowItemReview] BIT
	,[ForwardOnlyShowBreadcrumbs] BIT
	)


DECLARE @TEMP_SCALES_SCORES TABLE (
	[RowID] INT IDENTITY(1, 1) NOT NULL
	,[RawScore] DECIMAL(18, 4)
	,[ScaleScore] DECIMAL(18, 4)
	)


  --IF (EXISTS(SELECT id FROM [dbo].[ExamSessionTable] WHERE [ID] = @examInstanceId))  
  -- BEGIN  
    SELECT @myExamState=[examState], 
		   @defaultDuration = [DefaultDuration] , 
		   @computedTotalDuration = [ComputedTotalDuration]
    FROM [dbo].[ExamSessionTable] WHERE [ID] = @examInstanceId    

   
    SELECT @totalSectionBreakTime = SUM(ISNULL([BreakDuration], 0))
                                       FROM [dbo].[ExamSectionsTable]
                                       Where ExamSessionID = @examInstanceId
                                       Group by ExamSessionID    

	SELECT @structureXml = [StructureXML].query('assessmentDetails/assessment'), 
			@durationMode = DurationMode,
			@enableCandidateBreak = enableCandidateBreak,
			@candidateBreakStyle = candidateBreakStyle,  -- Scheduled / Unscheduled
			@scheduledBreakType = scheduledBreakType,    -- Fixed/Pooled
			@breakPooledTime = breakPooledTime,
			@originalBreakPooledTime = breakPooledTime,
			@maximumNumberOfBreaksPerSection = maximumNumberOfBreaksPerSection,
			@originalMaximumNumberOfBreaksPerSection = maximumNumberOfBreaksPerSection,						
			@totalBreakTime = totalBreakTime,
			@originalTotalBreakTime = totalBreakTime,
			@totalBreakTimeUsed = totalBreakTimeUsed,
			@isUnlimitedBreaks = isUnlimitedBreaks,
			@unscheduledBreakMinutes = UnscheduledBreakMin,
			@unscheduledBreakMinutesUsed = UnscheduledBreakMinUsed,
			@candidateBreakExtraDurationType = ISNULL(CandidateBreakExtraDurationType, 0),
            @candidateBreakExtraDuration = ISNULL(CandidateBreakExtraDuration, 0),
            @numberOfExtraBreaksPerSection = ISNULL(NumberOfExtraBreaksPerSection, 0),
            @candidateBreakExtraBreakReasonType = ISNULL(CandidateBreakExtraBreakReasonType, 0),
            @candidateBreakExtraDurationReasonText = ISNULL(CandidateBreakExtraDurationReasonText, 0),
            @candidateBreakExtraDurationReasonOther = ISNULL(CandidateBreakExtraDurationReasonOther, 0),
		    @extendedXml	= [ExtendedXml],
		    @customRoundingEnabled = CustomRoundingEnabled,
		    @customRoundingDigits = CustomRoundingDigits
	FROM [dbo].[ExamSessionTable] WHERE [ID] = @examInstanceId  

INSERT INTO @TEMP_EXAM_SECTIONS ([ExamSessionId]
					,[SectionID]
					,[ExamSectionType]
					,[Duration]
					,[OverrideLockdown]
					,[HasSectionBreak]
					,[BreakDuration]
					,[OriginalBreakDuration]
					,[CancellableBreak]
					,[NumberOfBreaksUsed]
					,[ScheduledBreakStarted]
					,[Description]
					,[SectionSelectorID]
					,[SectionSelected]
					,[ForwardOnly]
					,[RequiredResponse]
					,[IsBranchingSection]
					,[AdaptiveEnabled]
					,[AdaptiveType]
					,[NoReturnToSection]
					,[ForwardOnlyAllowItemReview]
					,[ForwardOnlyShowBreadcrumbs])
	SELECT			 [ExamSessionId]
					,[SectionID]
					,[ExamSectionType]
					,[Duration]
					,[OverrideLockdown]
					,[HasSectionBreak]
					,CEILING((CASE					    
								WHEN @enableCandidateBreak = 1 AND @candidateBreakStyle = 0 AND @scheduledBreakType = 0 AND @candidateBreakExtraDurationType = 0  THEN ([BreakDuration] + (CONVERT(decimal, [BreakDuration])/CONVERT(decimal, @totalSectionBreakTime) * @candidateBreakExtraDuration))
								WHEN @enableCandidateBreak = 1 AND @candidateBreakStyle = 0 AND @scheduledBreakType = 0 AND @candidateBreakExtraDurationType = 1  Then ([BreakDuration] + (CONVERT(decimal, @candidateBreakExtraDuration)/100 * [BreakDuration]))
								ELSE  [BreakDuration]					   
							  END))
                    ,[BreakDuration]							  
					,[CancellableBreak]
					,[NumberOfBreaksUsed]
					,[ScheduledBreakStarted]
					,[Description]
					,[SectionSelectorID]
					,[SectionSelected]
					,[ForwardOnly]
					,[RequiredResponse]
					,CASE WHEN BranchingResultSet.r.value('@sectionID', 'INT') = 0 THEN 1
					      WHEN BranchingResultSet.r.value('@sectionID', 'INT') IS NULL THEN 0
						  ELSE BranchingResultSet.r.value('@sectionID', 'INT') END AS [BranchingSection]
					,[AdaptiveEnabled]
					,[AdaptiveType]
					,[NoReturnToSection]
					,[ForwardOnlyAllowItemReview]
					,[ForwardOnlyShowBreadcrumbs]
			FROM [dbo].[ExamSectionsTable]                                           
			LEFT JOIN @extendedXml.nodes('/extendedXml/branching/fileContent//group') AS BranchingResultSet(r) ON  BranchingResultSet.r.value('@sectionID', 'INT') = SectionID	
			WHERE [ExamSessionId] = @examInstanceId
			ORDER BY [ExamSectionType]

DECLARE SectionCursor CURSOR FAST_FORWARD FOR
		SELECT 	    [ExamSectionType]
					,[SectionID]
					,[Duration]
					,[OverrideLockdown]
					,[HasSectionBreak]
					,[BreakDuration]
					,[CancellableBreak]
					,[NumberOfBreaksUsed]
					,[ScheduledBreakStarted]
					,[OriginalBreakDuration]
					,[SectionSelectorID]
					,[Description]
					,SectionSelected
					,ForwardOnly
					,RequiredResponse
					,[IsBranchingSection]
					,[AdaptiveEnabled]
					,[AdaptiveType]
					,[NoReturnToSection]
					,[ForwardOnlyAllowItemReview]
					,[ForwardOnlyShowBreadcrumbs]					
		FROM @TEMP_EXAM_SECTIONS
		ORDER BY [ExamSectionType];
	
	OPEN SectionCursor;
	FETCH NEXT FROM SectionCursor 
	INTO
	    @assessmentSectionType
	,	@sectionId
	,	@duration
	,	@overrideLockdown
	,	@hasSectionBreak
	,	@breakDuration
	,	@cancellableBreak
	,	@numberOfBreaksUsed
	,	@scheduledBreakStarted
	,	@originalBreakDuration
	,	@sectionSelectorID
	,	@description
	,	@sectionSelected
	,	@forwardOnly
	,	@requiredResponse
	,	@isBranchingSection
	,	@adaptiveEnabled
	,	@adaptiveType
	,	@noReturnToSection
	,	@forwardOnlyAllowItemReview
	,	@forwardOnlyShowBreadcrumbs
		;
	
	DECLARE @sectionXml xml;
	WHILE (@@FETCH_STATUS = 0)
	BEGIN

			-- alter the assessment sections xml to include Duration and OverrideLockdown as attribute to Intro, Outro and Section nodes

				-- check if section requires extra time due to special requirements
				DECLARE @timeExtensionRatio FLOAT 
				DECLARE @sectionScheduledDuration FLOAT		
				SET @timeExtensionRatio = 
					CASE 
						WHEN @defaultDuration > 0 
							AND @computedTotalDuration > @defaultDuration --this also removes all NULL combinations
							THEN CAST(@computedTotalDuration AS FLOAT) / CAST(@defaultDuration AS FLOAT)
						ELSE 
							-- if no special requirement bonus time, just set ratio to 1
							1
					END
				SET @sectionScheduledDuration = ROUND((@duration * @timeExtensionRatio),4)

				-- Intro
				IF @assessmentSectionType = 1
					BEGIN
						--Intro and Outro don't support sectionScheduledDuration atribute so have to modify duration
						SET @structureXml.modify('insert attribute duration {sql:variable("@sectionScheduledDuration")} into (/assessmentDetails/assessment/intro)[1]')
						SET @structureXml.modify('insert attribute overrideLockdown {sql:variable("@overrideLockdown")} into (/assessmentDetails/assessment/intro)[1]')
					END
				-- Outro
				ELSE IF @assessmentSectionType = 2
					BEGIN
						--Intro and Outro don't support sectionScheduledDuration atribute so have to modify duration
						SET @structureXml.modify('insert attribute duration {sql:variable("@sectionScheduledDuration")} into (/assessmentDetails/assessment/outro)[1]')
						SET @structureXml.modify('insert attribute overrideLockdown {sql:variable("@overrideLockdown")} into (/assessmentDetails/assessment/outro)[1]')
					END
				-- Section (there may be several therefore search by id)
				ELSE IF @assessmentSectionType = 4
					BEGIN
						SET @sectionXml = @structureXml.query('/assessmentDetails/assessment/section[@id = sql:variable("@sectionId")][1]');
						
						SET @sectionXml.modify('insert attribute duration {sql:variable("@duration")} into (/section)[1]')
						SET @sectionXml.modify('insert attribute overrideLockdown {sql:variable("@overrideLockdown")} into (/section)[1]')
						SET @sectionXml.modify('insert attribute scheduledDuration {sql:variable("@sectionScheduledDuration")} into (/section)[1]')

						-- Candidate Breaks
						SET @sectionXml.modify('insert attribute hasSectionBreak {sql:variable("@hasSectionBreak")} into (/section)[1]')
						SET @sectionXml.modify('insert attribute breakDuration {sql:variable("@breakDuration")} into (/section)[1]')
						SET @sectionXml.modify('insert attribute cancellableBreak {sql:variable("@cancellableBreak")} into (/section)[1]')
						SET @sectionXml.modify('insert attribute numberOfBreaksUsed {sql:variable("@numberOfBreaksUsed")} into (/section)[1]')
						SET @sectionXml.modify('insert attribute scheduledBreakStarted {sql:variable("@scheduledBreakStarted")} into (/section)[1]')
						SET @sectionXml.modify('insert attribute originalBreakDuration {sql:variable("@originalBreakDuration")} into (/section)[1]')

						--Section Selector
						SET @sectionXml.modify('insert attribute description {sql:variable("@description")} into (/section)[1]')

						IF @sectionSelectorID IS NOT NULL
						BEGIN
							SET @sectionXml.modify('insert attribute sectionSelectorId {sql:variable("@sectionSelectorID")} into (/section)[1]')
							SET @sectionXml.modify('insert attribute selected {sql:variable("@sectionSelected")} into (/section)[1]')							
						END

						--Forward Only Section, Required Response Section
						SET @sectionXml.modify('insert attribute forwardOnly {sql:variable("@forwardOnly")} into (/section)[1]')
						SET @sectionXml.modify('insert attribute requiredResponse {sql:variable("@requiredResponse")} into (/section)[1]')



						-- No Return to section, Allow item review, show breadcrumbs
						SET @sectionXml.modify('insert attribute noReturnToSection {sql:variable("@noReturnToSection")} into (/section)[1]')
						SET @sectionXml.modify('insert attribute forwardOnlyAllowItemReview {sql:variable("@forwardOnlyAllowItemReview")} into (/section)[1]')
						SET @sectionXml.modify('insert attribute forwardOnlyShowBreadcrumbs {sql:variable("@forwardOnlyShowBreadcrumbs")} into (/section)[1]')
						
						--Extended XML -> Branching XML -> Extended XML column located in the ExamSession table, is an extensible column which could be used to add additional XML information required within the
						                                -- ExamSession instead of creating a new column.
						SET @sectionXml.modify('insert attribute isBranchingSection {sql:variable("@isBranchingSection")} into (/section)[1]')

						--AdaptiveEnabled and AdaptiveType
						IF @adaptiveEnabled = 1
						BEGIN
							SET @sectionXml.modify('insert attribute adaptiveEnabled {"1"} into (/section)[1]')
							SET @sectionXml.modify('insert attribute adaptiveType {sql:variable("@adaptiveType")} into (/section)[1]')
						END
						
						SET @structureXml.modify('delete /assessmentDetails/assessment/section[@id = sql:variable("@sectionId")][1]');
						SET @structureXml.modify('insert sql:variable("@sectionXml") into (/assessmentDetails/assessment)[1]');						

					END
					
		FETCH NEXT FROM SectionCursor 
		INTO
			@assessmentSectionType
		,	@sectionId
		,	@duration
		,	@overrideLockdown
		,	@hasSectionBreak
		,	@breakDuration
		,	@cancellableBreak
		,	@numberOfBreaksUsed
		,	@scheduledBreakStarted
		,	@originalBreakDuration
		,	@sectionSelectorID
		,	@description
		,	@sectionSelected
		,	@forwardOnly
		,	@requiredResponse
		,	@isBranchingSection
		,	@adaptiveEnabled
		,	@adaptiveType
		,	@noReturnToSection
		,	@forwardOnlyAllowItemReview
		,	@forwardOnlyShowBreadcrumbs
			;					

	END
	CLOSE SectionCursor;
	DEALLOCATE SectionCursor;


	SELECT @structureXml