SELECT '0' AS '@errorCode',(
								SELECT @PageNumber AS '@pageIndex',
										@PageSize AS '@pageSize',
										@TotalCount AS '@totalCount',
										@TotalResultCount AS '@totalRecords',(
SELECT qualificationID AS 'qualificationId',
										 qualificationName,
										 qualificationLevel,
										 examName,
										 startDate,
										 endDate,
										 scheduledExamEndDate,
										 startTime,
										 endTime,
										 totalCandidates,
										 dateCreated,
										 examId,
										 centreName,
										 centreId,
										 createdById,
										 createdByForename,
										 createdBySurname,
										 scheduledExamId,
										 groupState,
										 examVersionId,
										 Invigilated,
										 ScheduledForInvigilate AS 'scheduledForInvigilate',
										 StrictControlOnDDA, firstName, surname, candidateRef, candidateID, examSessionID, candidateUln, candidateDateOfBirth  
								  FROM
								  (            
									  SELECT  ROW_NUMBER() OVER (ORDER BY qualificationName ASC, examName ASC, dateCreated DESC, surname ASC, candidateRef ASC) as RowNumber,
											  qualificationID,
											  qualificationName,
											  qualificationLevel,
											  examName,
											  startDate,
											  endDate,
											  scheduledExamEndDate,
											  startTime,
											  endTime,
											  totalCandidates,
											  dateCreated,
											  examId,
											  centreName,
											  centreId,
											  createdById,
											  createdByForename,
											  createdBySurname,
											  scheduledExamId,
											  groupState,
											  examVersionId,
											  Invigilated,
											  ScheduledForInvigilate,
											  StrictControlOnDDA, firstName, surname, candidateRef, candidateID, examSessionID, candidateUln, candidateDateOfBirth  
									  FROM             
									  (
									      SELECT dbo.IB3QualificationLookup.id						AS qualificationID,
												 dbo.IB3QualificationLookup.[QualificationName]	    AS qualificationName,
												 dbo.IB3QualificationLookup.[QualificationLevel]	AS qualificationLevel,
												 dbo.ScheduledExamsTable.[examName]					AS examName,
												 dbo.ScheduledExamsTable.[ScheduledStartDateTime]	AS startDate,CASE WHEN dbo.ExamSessionTable.IsProjectBased = 1
										  THEN
											 CASE WHEN dbo.ExamSessionTable.StructureXML IS NULL
											 THEN
												 -- no strucutreXML so use durationXML
												 DATEADD(MINUTE, ExamSessionTable.ScheduledDurationXml.value('(/duration/value)[1]', 'int')-1440, dbo.ScheduledExamsTable.ScheduledStartDateTime)
											 ELSE
												 -- use structureXML
												 DATEADD(MINUTE, ExamSessionTable.StructureXML.value('(/assessmentDetails/scheduledDuration/value)[1]', 'int')-1440, dbo.ScheduledExamsTable.ScheduledStartDateTime)
											 END
										  ELSE
											 -- use scheduledExam end date
											 dbo.ScheduledExamsTable.ScheduledEndDateTime
										  END 								AS endDate,
												 dbo.ScheduledExamsTable.[ScheduledEndDateTime]		AS scheduledExamEndDate,
												 dbo.ScheduledExamsTable.[ActiveStartTime]			AS startTime,
												 dbo.ScheduledExamsTable.[ActiveEndTime]			AS endTime, (SELECT count(*) FROM [dbo].[ExamSessionTable] where ScheduledExamID = dbo.ScheduledExamsTable.[ID]) as totalCandidates,
												 dbo.ScheduledExamsTable.[CreatedDateTime]			AS dateCreated,
												 dbo.ScheduledExamsTable.[ExamID]					AS examId,
												 dbo.CentreTable.CentreName							AS centreName,
												 dbo.ScheduledExamsTable.CentreID					AS centreId,
												 dbo.ScheduledExamsTable.CreatedBy					AS createdById,
												 dbo.UserTable.forename								AS createdByForename,
												 dbo.UserTable.surname								AS createdBySurname,
												 dbo.ScheduledExamsTable.[ID]						AS scheduledExamId,
												 dbo.ScheduledExamsTable.[groupState]				AS groupState,
												 dbo.ScheduledExamsTable.[examVersionId]			AS examVersionId,
												 dbo.ScheduledExamsTable.[Invigilated]				AS Invigilated,
												 dbo.ScheduledExamsTable.[ScheduledForInvigilate]	AS ScheduledForInvigilate, 
												 dbo.ScheduledExamsTable.StrictControlOnDDA			AS StrictControlOnDDA, (SELECT Forename FROM UserTable WHERE UserTable.ID = ExamSessionTable.UserID) AS firstName,
													   (SELECT Surname FROM UserTable WHERE UserTable.ID = ExamSessionTable.UserID) AS surname,
													   (SELECT CandidateRef FROM UserTable WHERE UserTable.ID = ExamSessionTable.UserID) AS candidateRef,
													   (SELECT isNULL(ULN, '') FROM UserTable WHERE UserTable.ID = ExamSessionTable.UserID) AS candidateUln,
													   (SELECT DOB FROM UserTable WHERE UserTable.ID = ExamSessionTable.UserID) AS candidateDateOfBirth,
														UserID AS candidateID,
														ExamSessionTable.ID AS examSessionID FROM [dbo].[ExamSessionTable]
															INNER JOIN
																[dbo].[ScheduledExamsTable] ON [dbo].[ScheduledExamsTable].ID = [dbo].[ExamSessionTable].ScheduledExamID
															INNER JOIN
																CentreTable on dbo.ScheduledExamsTable.CentreID = dbo.CentreTable.ID
															INNER JOIN
																UserTable on dbo.ScheduledExamsTable.CreatedBy = dbo.UserTable.ID
															INNER JOIN
																IB3QualificationLookup on IB3QualificationLookup.id = dbo.ScheduledExamsTable.qualificationId
 WHERE  (dbo.UserTable.surname LIKE '%' OR dbo.UserTable.forename LIKE '%') AND dbo.ScheduledExamsTable.[examName] LIKE '%'  AND (qualificationID IN (41,141,147,82,150,33,23,225,233,231,159,22,30,31,19,80,175,100,148,138,235,262,252,234,261,251,27,85,255,246,144,25,96,250,24,88,170,164,154,184,160,156,167,139,142,163,91,103,99,21,84,89,297,32,185,186,162,226,227,187,29,90,249,238,258,28,87,43,86,243,229,230,44,97,240,259,224,146,165,176,168,172,256,257,135,78,104,228,239,244,188,189,95,247,145,26,190,83,151,191,79,102,98,232,20,173,174,153,158,143,166,171,149,155,140,161,152,157,196,169,236,260,253,81,177,101,70,192)) AND dbo.ScheduledExamsTable.[CreatedDateTime] >= Convert(smalldatetime, '10 Oct 2016')  AND dbo.ScheduledExamsTable.[CreatedDateTime] <= Convert(smalldatetime, '09 Nov 2016')  AND (dbo.ScheduledExamsTable.groupState = 1 OR dbo.ScheduledExamsTable.groupState = 2)
												AND dbo.ScheduledExamsTable.CreatedBy  <> -1
												 AND dbo.ScheduledExamsTable.qualityReview=0 AND (SELECT Surname FROM UserTable WHERE UserTable.ID = ExamSessionTable.UserID) LIKE '%'
													AND (SELECT CandidateRef FROM UserTable WHERE UserTable.ID = ExamSessionTable.UserID) LIKE '%'
													AND (SELECT isNULL(ULN, '') FROM UserTable WHERE UserTable.ID = ExamSessionTable.UserID) LIKE '%'
												AND EXISTS(
															SELECT TOP (1) ID FROM #TEMP_CentreQualificationLevels_TABLE
															INNER JOIN IB3QualificationLookup
																ON IB3QualificationLookup.QualificationLevel = #TEMP_CentreQualificationLevels_TABLE.qualLevel
															WHERE ID = dbo.ScheduledExamsTable.qualificationId
															AND (centreId = 1 OR centreId = dbo.CentreTable.ID)
														   )
													)as ExamDetails) as examSearch
 WHERE RowNumber BETWEEN @RowStart AND @RowEnd ORDER BY qualificationName ASC, examName ASC, dateCreated DESC, surname ASC, candidateRef ASC
 FOR XML PATH('exam'), TYPE) FOR XML PATH('return'), TYPE) FOR XML PATH('result')