IF EXISTS(SELECT 1 FROM sys.tables)
BEGIN	
	DROP TABLE Membership  
END

CREATE TABLE Membership  
  (MemberID int IDENTITY PRIMARY KEY,  
   FirstName varchar(100) MASKED WITH (FUNCTION = 'partial(1,"XXXXXXX",0)') NULL,  
   LastName varchar(100) NOT NULL,  
   Phone# varchar(12) MASKED WITH (FUNCTION = 'default()') NULL,  
   Email varchar(100) MASKED WITH (FUNCTION = 'email()') NULL
);  

INSERT Membership (FirstName, LastName, Phone#, Email) 
VALUES  ('Roberto', 'Tamburello', '555.123.4567', 'RTamburello@contoso.com'),  
	('Janice', 'Galvin', '555.123.4568', 'JGalvin@contoso.com.co'),  
	('Zheng', 'Mu', '555.123.4569', 'ZMu@contoso.net');  

SELECT * 
FROM Membership;  

ALTER TABLE Membership  
ALTER COLUMN LastName ADD MASKED WITH (FUNCTION = 'partial(2,"XXX",0)');  

SELECT * 
FROM Membership;  


DECLARE @DDM TABLE (
	DB sysname
	, table_name sysname	
	, table_schema sysname
	, column_name sysname
)

INSERT @DDM
EXEC sp_MSforeachdb '

use [?];

if(''?'' like ''%Demo%'')

begin
	
	SELECT ''?''
		, A.table_name
		, A.table_schema
		, A.column_name 
	FROM INFORMATION_SCHEMA.COLUMNS A
	INNER JOIN INFORMATION_SCHEMA.TABLES B
	ON A.TABLE_NAME = B.TABLE_NAME
	WHERE COLUMN_name in (''FirstName'',''Forename'',''LastName'',''Surname'', ''email'',''documentName'')
		and B.TABLE_TYPE = ''BASE TABLE''
	order by 1,2,3,4
end

'
SELECT	
	'use '+QUOTENAME(DB)+'; ALTER TABLE '+QUOTENAME(table_schema) + '.' + QUOTENAME(table_name) + ' ALTER COLUMN '+QUOTENAME(column_name) + ' ADD MASKED WITH (FUNCTION = ''partial(2,"XXX",0)'');  '
	, *
FROM @DDM;



