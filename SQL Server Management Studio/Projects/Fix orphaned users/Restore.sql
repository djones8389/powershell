--use master
--RESTORE DATABASE [Bpec_ContentAuthor] FROM DISK = N'D:\Backup\Bpec.All.2016.12.01.bak' WITH FILE = 1, NOUNLOAD, NOREWIND, REPLACE, STATS = 10, MOVE 'Bpec_ContentAuthor' TO N'D:\Data\Bpec_ContentAuthor.mdf', MOVE 'Bpec_ContentAuthor_log' TO N'D:\Log\Bpec_ContentAuthor.ldf'; 
--RESTORE DATABASE [Bpec_ItemBank] FROM DISK = N'D:\Backup\Bpec.All.2016.12.01.bak' WITH FILE = 2, NOUNLOAD, NOREWIND, REPLACE, STATS = 10, MOVE 'Bpec_ItemBank' TO N'D:\Data\Bpec_ItemBank.mdf', MOVE 'Bpec_ItemBank_log' TO N'D:\Log\Bpec_ItemBank.ldf'; 	
--RESTORE DATABASE [Bpec_SecureAssess] FROM DISK = N'D:\Backup\Bpec.All.2016.12.01.bak' WITH FILE = 3, NOUNLOAD, NOREWIND, REPLACE, STATS = 10, MOVE 'Bpec_SecureAssess' TO N'D:\Data\Bpec_SecureAssess.mdf', MOVE 'Bpec_SecureAssess_log' TO N'D:\Log\Bpec_SecureAssess.ldf'; 
--RESTORE DATABASE [Bpec_SurpassManagement] FROM DISK = N'D:\Backup\Bpec.All.2016.12.01.bak' WITH FILE = 4, NOUNLOAD, NOREWIND, REPLACE, STATS = 10, MOVE 'Bpec_SurpassManagement' TO N'D:\Data\Bpec_SurpassManagement.mdf', MOVE 'Bpec_SurpassManagement_log' TO N'D:\Log\Bpec_SurpassManagement.ldf'; 


if OBJECT_ID('tempdb..##orphanHolder') IS NOT NULL DROP TABLE ##orphanHolder;

create table ##orphanHolder (
	[db] sysname null
	, [username] nvarchar(1000)
	, [sID] varbinary(max)
)

exec sp_MSforeachdb '

use [?];

if(db_ID() > 4)

BEGIN
	
	INSERT ##orphanHolder ([username], [sID])		
	exec sp_change_users_login ''report''

	UPDATE ##orphanHolder set [db] = ''?'' where [db] is null;
		
END

'

DECLARE @Dynamic nvarchar(MAX)='';

select @Dynamic += Char(13) + 'use ' +QUOTENAME(db)+ ' exec sp_change_users_login ' + '''auto_fix'',''' + username + ''';'
from ##orphanHolder

exec(@Dynamic)

