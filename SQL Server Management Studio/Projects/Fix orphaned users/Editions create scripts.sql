create database [Bpec_SurpassManagement]
create database [Bpec_ContentAuthor]
create database [Bpec_ItemBank]
create database [Bpec_SecureAssess]

create login [Bpec_ContentAuthor_User] with password = '123456789'

use [Bpec_ContentAuthor]  create user [Bpec_ContentAuthor_User] for login  [Bpec_ContentAuthor_User]
exec sp_addrolemember 'db_owner','Bpec_ContentAuthor_User'

create login [Bpec_ItemBank_User] with password = '123456789'

use [Bpec_ItemBank]  create user [Bpec_ItemBank_User] for login  [Bpec_ItemBank_User]
exec sp_addrolemember 'db_owner','Bpec_ItemBank_User'

create login [Bpec_SecureAssess_User] with password = '123456789'

use [Bpec_SecureAssess]  create user [Bpec_SecureAssess_User] for login  [Bpec_SecureAssess_User]
exec sp_addrolemember 'db_owner','Bpec_SecureAssess_User'



create login [Bpec_SurpassManagement_User] with password = '123456789'

use [Bpec_SurpassManagement]  create user [Bpec_SurpassManagement_User] for login  [Bpec_SurpassManagement_User]
exec sp_addrolemember 'db_owner','Bpec_SurpassManagement_User'



create login [Bpec_ETL_User] with password = '123456789'


use [Bpec_SecureAssess] Create user [Bpec_ETL_User] for login [Bpec_ETL_User]
exec sp_addrolemember 'db_datareader','Bpec_ETL_User'

use [Bpec_ItemBank] Create user [Bpec_ETL_User] for login [Bpec_ETL_User]
exec sp_addrolemember 'db_datawriter','Bpec_ETL_User'

use [Bpec_ContentAuthor] Create user [Bpec_ETL_User] for login [Bpec_ETL_User]
exec sp_addrolemember 'db_owner','Bpec_ETL_User'
