
--RESTORE DATABASE [Testdeploy_AnalyticsManagement]
--FROM DISK = N'T:\Backup\Saxion.06102017.bak'
--WITH FILE = 1
--	,NOUNLOAD
--	,NOREWIND
--	,REPLACE
--	,STATS = 10
--	,MOVE 'Data' TO N'S:\DATA\Testdeploy_AnalyticsManagement.mdf'
--	,MOVE 'Log' TO N'L:\LOGS\Testdeploy_AnalyticsManagement.ldf';

--RESTORE DATABASE [Testdeploy_ContentAuthor]
--FROM DISK = N'T:\Backup\Saxion.06102017.bak'
--WITH FILE = 2
--	,NOUNLOAD
--	,NOREWIND
--	,REPLACE
--	,STATS = 10
--	,MOVE 'Data' TO N'S:\DATA\Testdeploy_ContentAuthor.mdf'
--	,MOVE 'Log' TO N'L:\LOGS\Testdeploy_ContentAuthor.ldf';
	
--RESTORE DATABASE [Testdeploy_ItemBank]
--FROM DISK = N'T:\Backup\Saxion.06102017.bak'
--WITH FILE = 3
--	,NOUNLOAD
--	,NOREWIND
--	,REPLACE
--	,STATS = 10
--	,MOVE 'Data' TO N'S:\DATA\Testdeploy_ItemBank.mdf'
--	,MOVE 'Log' TO N'L:\LOGS\Testdeploy_ItemBank.ldf';

RESTORE DATABASE [Testdeploy_SecureAssess]
FROM DISK = N'T:\Backup\Saxion.06102017.bak'
WITH FILE = 4
	,NOUNLOAD
	,NOREWIND
	,REPLACE
	,STATS = 10
	,MOVE 'Data' TO N'S:\DATA\Testdeploy_SecureAssess.mdf'
	,MOVE 'Log' TO N'L:\LOGS\Testdeploy_SecureAssess.ldf';

RESTORE DATABASE [Testdeploy_SurpassDataWarehouse]
FROM DISK = N'T:\Backup\Saxion.06102017.bak'
WITH FILE = 6
	,NOUNLOAD
	,NOREWIND
	,REPLACE
	,STATS = 10
	,MOVE 'Data' TO N'S:\DATA\Testdeploy_SurpassDataWarehouse.mdf'
	,MOVE 'Log' TO N'L:\LOGS\Testdeploy_SurpassDataWarehouse.ldf';

RESTORE DATABASE [Testdeploy_SurpassManagement]
FROM DISK = N'T:\Backup\Saxion.06102017.bak'
WITH FILE = 7
	,NOUNLOAD
	,NOREWIND
	,REPLACE
	,STATS = 10
	,MOVE 'Data' TO N'S:\DATA\Testdeploy_SurpassManagement.mdf'
	,MOVE 'Log' TO N'L:\LOGS\Testdeploy_SurpassManagement.ldf';


