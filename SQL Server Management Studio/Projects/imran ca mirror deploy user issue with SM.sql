IF OBJECT_ID('tempdb..##DATALENGTH') IS NOT NULL DROP TABLE ##DATALENGTH;

CREATE TABLE ##DATALENGTH (
	client nvarchar(1000)
	, ItemAuthoringUserId int
);

DECLARE @dynamic nvarchar(MAX)='';

select @dynamic +=CHAR(13)+ '
use ['+name+'_ContentAuthor];

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;


DECLARE @'+name+' as TABLE(UserName nvarchar(300), Id bigint, RowN int)

INSERT INTO @'+name+'
SELECT u.UserName, u.Id, ROW_NUMBER() OVER(PARTITION BY u.UserName ORDER BY u.UserName ASC, u.Id ASC) AS RowN  
FROM ['+name+'_ContentAuthor].[dbo].[Users] u
JOIN (
	SELECT UserName 
	FROM ['+name+'_ContentAuthor].[dbo].[Users] 
	GROUP BY UserName 
	HAVING COUNT(UserName) > 1
) dup ON dup.UserName = u.UserName

INSERT ##DATALENGTH
select DB_NAME(), users.ItemAuthoringUserId
from  '+name+'_SurpassManagement.[dbo].[Users]
where ItemAuthoringUserId in (
select cau.Id
FROM '+name+'_SurpassManagement.[dbo].[Users] u
JOIN ['+name+'_ContentAuthor].[dbo].[Users] cau ON cau.UserName = u.UserName
LEFT JOIN @'+name+' dupl ON dupl.Id = cau.Id
WHERE u.ItemAuthoringUserId <> cau.Id AND (dupl.UserName IS NULL OR dupl.RowN = 1)
)
'
from (
	select distinct B.name
	FROM (
		select SUBSTRING(name, 0, charindex('_',name)) [Name]
			, ROW_NUMBER() OVER(PARTITION BY SUBSTRING(name, 0, charindex('_',name)) ORDER BY SUBSTRING(name, 0, charindex('_',name)) ) R
		from sys.databases
		where state_desc = 'ONLINE'
			and [Name] like '%/_%' ESCAPE '/'
			and name not like 'test%'
			and name not like 'damian%'
		) B
	where R > 3
) C
exec(@dynamic);

select *
from ##DATALENGTH
order by 1
