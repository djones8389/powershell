USE MASTER 

RESTORE DATABASE [WHItemResponses] FROM DISK = N'T:\FTP\BC\WHItemResponses.2017.04.25.bak' WITH FILE = 1, NOUNLOAD, NOREWIND, REPLACE
	, MEDIAPASSWORD = 's_!85WrenukEtrut+4f5a#-a&ac*aKUs', STATS = 10, MOVE 'WHItemResponses'
	 TO N'S:\DATA\WHItemResponses.mdf', MOVE 'WHItemResponses_log' TO N'L:\LOGS\WHItemResponses.ldf'; 


DECLARE @BackupFile NVARCHAR(255), 
            @Suffix NVARCHAR(10),
            @Prefix NVARCHAR(10),
			@MediaPassword NVARCHAR(100),
			@DefaultDataLoc NVARCHAR(512),
            @DefaultLogLoc NVARCHAR(512),
			@SQL NVARCHAR(4000),
			@Password nvarchar(35);

SET @Password = 's_!85WrenukEtrut+4f5a#-a&ac*aKUs';
SET @BackupFile = 'T:\FTP\BC\WHItemResponses.2017.04.25.bak';
SET @Suffix = N'';
SET @Prefix = N'';

BEGIN TRY
	RESTORE FILELISTONLY FROM DISK = @BackupFile 
	SET @MediaPassword = ''
END TRY
BEGIN CATCH
	SET @MediaPassword = 'MEDIAPASSWORD = '''+ @Password+ ''''
END CATCH


EXECUTE xp_instance_regread N'HKEY_LOCAL_MACHINE',N'Software\Microsoft\MSSQLServer\MSSQLServer',N'DefaultData', @DefaultDataLoc OUTPUT;
EXECUTE xp_instance_regread N'HKEY_LOCAL_MACHINE',N'Software\Microsoft\MSSQLServer\MSSQLServer',N'DefaultLog', @DefaultLogLoc OUTPUT;

IF (@DefaultDataLoc IS NULL OR @DefaultLogLoc IS NULL) Select 'Default Locations need defining!!!'
	--set @DefaultDataLoc= 'S:\Data'
	--set @DefaultLogLoc = 'L:\Log'

DECLARE @Header TABLE (
      BackupName NVARCHAR(128)
      ,BackupDescription NVARCHAR(255)
      ,BackupType SMALLINT
      ,ExpirationDate DATETIME
      ,Compressed BIT
      ,Position SMALLINT
      ,DeviceType TINYINT
      ,UserName NVARCHAR(128)
      ,ServerName NVARCHAR(128)
      ,DatabaseName NVARCHAR(128)
      ,DatabaseVersion INT
      ,DatabaseCreationDate DATETIME
      ,BackupSize NUMERIC(20, 0)
      ,FirstLSN NUMERIC(25, 0)
      ,LastLSN NUMERIC(25, 0)
      ,CheckpointLSN NUMERIC(25, 0)
      ,DatabaseBackupLSN NUMERIC(25, 0)
      ,BackupStartDate DATETIME
      ,BackupFinishDate DATETIME
      ,SortOrder SMALLINT
      ,CodePage SMALLINT
      ,UnicodeLocaleId INT
      ,UnicodeComparisonStyle INT
      ,CompatibilityLevel TINYINT
      ,SoftwareVendorId INT
      ,SoftwareVersionMajor INT
      ,SoftwareVersionMinor INT
      ,SoftwareVersionBuild INT
      ,MachineName NVARCHAR(128)
      ,Flags INT
      ,BindingID UNIQUEIDENTIFIER
      ,RecoveryForkID UNIQUEIDENTIFIER
      ,Collation NVARCHAR(128)
      ,FamilyGUID UNIQUEIDENTIFIER
      ,HasBulkLoggedData BIT
      ,IsSnapshot BIT
      ,IsReadOnly BIT
      ,IsSingleUser BIT
      ,HasBackupChecksums BIT
      ,IsDamaged BIT
      ,BeginsLogChain BIT
      ,HasIncompleteMetaData BIT
      ,IsForceOffline BIT
      ,IsCopyOnly BIT
      ,FirstRecoveryForkID UNIQUEIDENTIFIER
      ,ForkPointLSN NUMERIC(25, 0) NULL
      ,RecoveryModel NVARCHAR(60)
      ,DifferentialBaseLSN NUMERIC(25, 0) NULL
      ,DifferentialBaseGUID UNIQUEIDENTIFIER
      ,BackupTypeDescription NVARCHAR(60)
      ,BackupSetGUID UNIQUEIDENTIFIER NULL
      ,CompressedBackupSize NUMERIC(20, 0)
      );

IF @MediaPassword = '' 
	INSERT INTO @Header
	EXECUTE (N'RESTORE HEADERONLY FROM DISK = N''' + @BackupFile + ''' ');
ELSE 
	INSERT INTO @Header
	EXECUTE (N'RESTORE HEADERONLY FROM DISK = N''' + @BackupFile + ''' WITH ' +@MediaPassword+'');



DECLARE Header CURSOR
FOR
SELECT Position
      ,DatabaseName
FROM @Header;

DECLARE @File NVARCHAR(22),
      @DatabaseName NVARCHAR(128),
      @RestoreSize BIGINT = 0;

OPEN Header;

FETCH NEXT
FROM Header
INTO @File
      ,@DatabaseName;

WHILE @@FETCH_STATUS = 0
BEGIN
      DECLARE @FileList TABLE (
            LogicalName NVARCHAR(128)
            ,PhysicalName NVARCHAR(260)
            ,[Type] CHAR(1)
            ,FileGroupName NVARCHAR(128)
            ,Size NUMERIC(20, 0)
            ,MaxSize NUMERIC(20, 0)
            ,FileID BIGINT
            ,CreateLSN NUMERIC(25, 0)
            ,DropLSN NUMERIC(25, 0) NULL
            ,UniqueID UNIQUEIDENTIFIER
            ,ReadOnlyLSN NUMERIC(25, 0) NULL
            ,ReadWriteLSN NUMERIC(25, 0) NULL
            ,BackupSizeInBytes BIGINT
            ,SourceBlockSize INT
            ,FileGroupID INT
            ,LogGroupGUID UNIQUEIDENTIFIER NULL
            ,DifferentialBaseLSN NUMERIC(25, 0) NULL
            ,DifferentialBaseGUID UNIQUEIDENTIFIER
            ,IsReadOnly BIT
            ,IsPresent BIT
            ,TDEThumbPrint VARBINARY(32)
            );

IF @MediaPassword = '' 
      INSERT INTO @FileList
      EXECUTE (N'RESTORE FILELISTONLY FROM DISK = N''' + @BackupFile + ''' WITH FILE =  ' + @File +  ';');
ELSE
	  INSERT INTO @FileList
      EXECUTE (N'RESTORE FILELISTONLY FROM DISK = N''' + @BackupFile + ''' WITH FILE =  ' + @File + ','+ @MediaPassword +';');	

      DECLARE @DataFileName NVARCHAR(128)
            ,@LogFileName NVARCHAR(128);

IF @MediaPassword = ''       
      SET @SQL = N'RESTORE DATABASE [' + @Prefix + @DatabaseName + @Suffix +N'] FROM DISK = N''' + @BackupFile + ''' WITH FILE = ' + @File + ', NOUNLOAD, NOREWIND, REPLACE, STATS = 10,';
ELSE
      SET @SQL = N'RESTORE DATABASE [' + @Prefix + @DatabaseName + @Suffix +N'] FROM DISK = N''' + @BackupFile + ''' WITH FILE = ' + @File + ', NOUNLOAD, NOREWIND, REPLACE, ' + @MediaPassword + ', STATS = 10,';


      DECLARE DataFiles CURSOR FOR
            SELECT LogicalName
            FROM @FileList
            WHERE [Type] = N'D';

      OPEN DataFiles;
      
      FETCH NEXT FROM DataFiles INTO @DataFileName;
      WHILE @@FETCH_STATUS = 0
      BEGIN
            SET @SQL = @SQL + N' MOVE '''+@DataFileName+''' TO N'''+@DefaultDataLoc + '\' +@Prefix+@DatabaseName+@Suffix+'.mdf'',';
            FETCH NEXT FROM DataFiles INTO @DataFileName;
      END
      
      CLOSE DataFiles;
      DEALLOCATE DataFiles;

      DECLARE LogFiles CURSOR FOR
           SELECT LogicalName
            FROM @FileList
            WHERE [Type] = N'L';
      
      OPEN LogFiles;
      
      FETCH NEXT FROM LogFiles INTO @LogFileName;
      WHILE @@FETCH_STATUS = 0
      BEGIN
            SET @SQL = @SQL + N' MOVE '''+@LogFileName+''' TO N'''+@DefaultLogLoc + '\' + @Prefix+@DatabaseName+@Suffix+'.ldf''; 

IF NOT EXISTS (SELECT 1 FROM Sys.sql_logins where name =''' + @Prefix + @DatabaseName + @Suffix + '_Login''' + ') 
	CREATE LOGIN [' + @Prefix + @DatabaseName + @Suffix + '_Login]'  + ' WITH PASSWORD = ''Password'';  
	USE [' + @Prefix + @DatabaseName + @Suffix + '];  	
	IF EXISTS (
		SELECT 1
		FROM sys.database_principals
		WHERE NAME ='''+@Prefix + @DatabaseName + @Suffix + '_User'+'''
		)
	DROP USER '+QUOTENAME(@Prefix + @DatabaseName + @Suffix + '_User')+';
	
	CREATE USER ' + QUOTENAME(@Prefix + @DatabaseName + @Suffix + '_User') + ' FOR LOGIN ' + QUOTENAME(@Prefix + @DatabaseName + @Suffix + '_Login') + '; EXEC sp_addrolemember ''db_owner'','''+@Prefix + @DatabaseName + @Suffix + '_User''
	
	'
            FETCH NEXT FROM LogFiles INTO @LogFileName;
      END
            
      CLOSE LogFiles;
      DEALLOCATE LogFiles;

      SELECT @RestoreSize = @RestoreSize + SUM(Size)
      FROM @FileList;

      --SET @SQL = LEFT(@SQL, (LEN(@SQL) - 1)) + ';';

      PRINT @SQL;

      DELETE
      FROM @FileList;

      FETCH NEXT
      FROM Header
      INTO @File
            ,@DatabaseName;
END

CLOSE Header;

DEALLOCATE Header;

PRINT '--' + CONVERT(NVARCHAR(10),(((@RestoreSize / 1000) / 1000) / 1000))	+N' GB of storage needed';








