use Ocr_SurpassDataWarehouse

SELECT FQR.*
FROM FactExamSessions FES
INNER JOIN FactQuestionResponses FQR
on FES.ExamSessionKey = FQR.ExamSessionKey
where KeyCode = 'VG4FGD4J'	
	and cpid in ('4208P5359','4208P5366','4208P5383','4208P5384','4208P5386','4208P5387','4208P5388')
order by cpid

use Ocr_SecureAssess

select ItemID	
	, ItemVersion
	,  wesirt.ItemResponseData
from WAREHOUSE_ExamSessionTable west
inner join WAREHOUSE_ExamSessionItemResponseTable wesirt
on west.id = warehouseexamsessionid
where KeyCode = 'VG4FGD4J'	
	and ItemID in ('4208P5359','4208P5366','4208P5383','4208P5384','4208P5386','4208P5387','4208P5388')
order by ItemID
