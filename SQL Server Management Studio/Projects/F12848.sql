DECLARE @ESID INT = (
	select ID
	from warehouse_examsessiontable
	where    keycode = 'CRR9WD4C'
)

/*

--Make all items human marked--

	update Warehouse_ExamSessionTable
	set structurexml = replace(cast(structurexml as nvarchar(MAX)),'markingType="0"','markingType="1"')
		, resultData = replace(cast(resultData as nvarchar(MAX)),'markingType="0"','markingType="1"')
		, resultDataFull =  replace(cast(resultDataFull as nvarchar(MAX)),'markingType="0"','markingType="1"')
	where id = @ESID;

	update Warehouse_ExamSessionTable_shreded
	set resultData = replace(cast(resultData as nvarchar(MAX)),'markingType="0"','markingType="1"')
	where examSessionID = 9705;

--Make all items "attempted" so that they are markable

	update Warehouse_ExamSessionTable
	set structurexml = replace(cast(structurexml as nvarchar(MAX)),'userAttempted="0"','userAttempted="1"')
		, resultData = replace(cast(resultData as nvarchar(MAX)),'userAttempted="0"','userAttempted="1"')
		, resultDataFull =  replace(cast(resultDataFull as nvarchar(MAX)),'userAttempted="0"','userAttempted="1"')
	where id = @ESID;

	update Warehouse_ExamSessionTable_shreded
	set resultData = replace(cast(resultData as nvarchar(MAX)),'userAttempted="0"','userAttempted="1"')
	where examSessionID = @ESID;


--Following remarking, revert the markingType back to computer-marked 

	update Warehouse_ExamSessionTable
	set structurexml = replace(cast(structurexml as nvarchar(MAX)),'markingType="1"','markingType="0"')
		, resultData = replace(cast(resultData as nvarchar(MAX)),'markingType="1"','markingType="0"')
		, resultDataFull =  replace(cast(resultDataFull as nvarchar(MAX)),'markingType="1"','markingType="0"')
	where id = @ESID;

	update Warehouse_ExamSessionTable_shreded
	set resultData = replace(cast(resultData as nvarchar(MAX)),'markingType="1"','markingType="0"')
	where examSessionID = @ESID;

--Change original grade to Pass if applicable, so it looks OK in Audit

	update Warehouse_ExamSessionTable
	set resultData = replace(cast(resultData as nvarchar(MAX)),'originalGrade="Fail"','originalGrade="Pass"')
	where id = 9705;

	update Warehouse_ExamSessionTable_shreded
	set resultData = replace(cast(resultData as nvarchar(MAX)),'originalGrade="Fail"','originalGrade="Pass"')
	where examSessionID = 9705;


*/