--26 mins 20 seconds
--est 1 hr on Live.

use [PRV_BritishCouncil_SecureAssess]

ALTER DATABASE [PRV_BritishCouncil_SecureAssess]
	ADD FILEGROUP [WHAItemsWHItemResponses]
GO


ALTER DATABASE [PRV_BritishCouncil_SecureAssess]
ADD FILE (

	NAME = WHAItemsWHItemResponses
	, FILENAME = 'S:\WHAItemsWHItemResponses.ndf'
    , SIZE = 80000MB
    , MAXSIZE = UNLIMITED
    , FILEGROWTH = 500MB
)
TO FILEGROUP [WHAItemsWHItemResponses];
GO

PRINT 'Created Filegroups'

CREATE TABLE [dbo].[WAREHOUSE_ExamSessionItemResponseTable_new] (
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[WAREHOUSEExamSessionID] [int] NOT NULL,
	[ItemID] [nvarchar](15) NOT NULL,
	[ItemVersion] [int] NOT NULL,
	[ItemResponseData] [xml] NULL,
	[MarkerResponseData] [xml] NULL,
	[ItemMark] [int] NULL,
	[MarkingIgnored] [tinyint] NULL,
	[DerivedResponse] [nvarchar](max) NULL,
	[ShortDerivedResponse] [nvarchar](500) NULL
 ) ON [WHAItemsWHItemResponses] TEXTIMAGE_ON [WHAItemsWHItemResponses]
GO

PRINT 'Created [WAREHOUSE_ExamSessionItemResponseTable_new]'

SET IDENTITY_INSERT [WAREHOUSE_ExamSessionItemResponseTable_new] ON
GO

BEGIN TRANSACTION

declare @rc int

INSERT [WAREHOUSE_ExamSessionItemResponseTable_new](ID, WAREHOUSEExamSessionID, ItemID, ItemVersion, ItemResponseData, MarkerResponseData, ItemMark, MarkingIgnored, DerivedResponse, ShortDerivedResponse)
SELECT ID, WAREHOUSEExamSessionID, ItemID, ItemVersion, ItemResponseData, MarkerResponseData, ItemMark, MarkingIgnored, DerivedResponse, ShortDerivedResponse
FROM [WAREHOUSE_ExamSessionItemResponseTable]

select @rc = @@ROWCOUNT

COMMIT TRANSACTION

--SELECT @rc

PRINT 'INSERT [WAREHOUSE_ExamSessionItemResponseTable_new] COMPLETED'

SET IDENTITY_INSERT [WAREHOUSE_ExamSessionItemResponseTable_new] OFF
--GO

--14:40

if (@rc > 0)
DROP TABLE [dbo].[WAREHOUSE_ExamSessionItemResponseTable];
GO

PRINT 'DROP TABLE [dbo].[WAREHOUSE_ExamSessionItemResponseTable]'

EXEC sp_rename 'WAREHOUSE_ExamSessionItemResponseTable_new','WAREHOUSE_ExamSessionItemResponseTable';
GO


/*Constraints*/


ALTER TABLE [dbo].[WAREHOUSE_ExamSessionItemResponseTable]
	ADD CONSTRAINT [PK_WAREHOUSEExamSessionItemResponseTable_1] PRIMARY KEY CLUSTERED (ID)
GO

PRINT 'ADD CONSTRAINT [PK_WAREHOUSEExamSessionItemResponseTable_1] PRIMARY KEY CLUSTERED (ID)'

ALTER TABLE [dbo].[WAREHOUSE_ExamSessionItemResponseTable] ADD  CONSTRAINT [DF_WAREHOUSE_ExamSessionItemResponseTable_MarkingIgnored]  DEFAULT ((0)) FOR [MarkingIgnored]
GO

PRINT 'ADD  CONSTRAINT [DF_WAREHOUSE_ExamSessionItemResponseTable_MarkingIgnored]'

ALTER TABLE [dbo].[WAREHOUSE_ExamSessionItemResponseTable]  WITH CHECK 
ADD  CONSTRAINT [FK_WAREHOUSE_ExamSessionItemResponseTable_WAREHOUSE_ExamSessionTable] FOREIGN KEY([WAREHOUSEExamSessionID])
REFERENCES [dbo].[WAREHOUSE_ExamSessionTable] ([ID])
ON DELETE CASCADE
GO

PRINT 'ADD  CONSTRAINT [FK_WAREHOUSE_ExamSessionItemResponseTable_WAREHOUSE_ExamSessionTable]'

ALTER TABLE [dbo].[WAREHOUSE_ExamSessionItemResponseTable] CHECK CONSTRAINT [FK_WAREHOUSE_ExamSessionItemResponseTable_WAREHOUSE_ExamSessionTable]
GO



/*Indexes*/

PRINT 'START INDEXES'

CREATE NONCLUSTERED INDEX [IX_WAREHOUSE_ExamSessionItemResponseTable_R11_3PT_script3] ON [dbo].[WAREHOUSE_ExamSessionItemResponseTable]
(
	[WAREHOUSEExamSessionID] ASC
)
INCLUDE ( 	[ItemID],
	[ItemResponseData]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [WHAItemsWHItemResponses]
GO


CREATE NONCLUSTERED INDEX [idx_NC_WAREHOUSE_ExamSessionItemResponseTable_WAREHOUSEExamSessionID_ItemID_ItemVersion] ON [dbo].[WAREHOUSE_ExamSessionItemResponseTable]
(
	[WAREHOUSEExamSessionID] ASC,
	[ItemID] ASC,
	[ItemVersion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [WHAItemsWHItemResponses]
GO


CREATE NONCLUSTERED INDEX [examSessionId_NCI] ON [dbo].[WAREHOUSE_ExamSessionItemResponseTable]
(
	[WAREHOUSEExamSessionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [WHAItemsWHItemResponses]
GO


PRINT '[WAREHOUSE_ExamSessionItemResponseTable] DONE!!!!'

--


CREATE TABLE [dbo].[WAREHOUSE_ExamSessionAvailableItemsTable_new] (
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ExamSessionID] [int] NOT NULL,
	[ItemID] [nvarchar](50) NOT NULL,
	[ItemVersion] [int] NOT NULL,
	[ItemXML] [xml] NOT NULL
 ) ON [WHAItemsWHItemResponses] TEXTIMAGE_ON [WHAItemsWHItemResponses]
GO

PRINT 'Created [WAREHOUSE_ExamSessionAvailableItemsTable_new]'

SET IDENTITY_INSERT [WAREHOUSE_ExamSessionAvailableItemsTable_new] ON
GO

BEGIN TRANSACTION

declare @rc int

INSERT [WAREHOUSE_ExamSessionAvailableItemsTable_new] (ID, ExamSessionID, ItemID, ItemVersion, ItemXML)
SELECT ID, ExamSessionID, ItemID, ItemVersion, ItemXML
FROM [WAREHOUSE_ExamSessionAvailableItemsTable]

select @rc = @@ROWCOUNT

COMMIT TRANSACTION


PRINT 'INSERT [WAREHOUSE_ExamSessionAvailableItemsTable_new]  COMPLETE'

SET IDENTITY_INSERT [WAREHOUSE_ExamSessionAvailableItemsTable_new] OFF
--GO

if(@rc > 0)
DROP TABLE [dbo].[WAREHOUSE_ExamSessionAvailableItemsTable];
GO

PRINT 'DROP TABLE [dbo].[WAREHOUSE_ExamSessionAvailableItemsTable]'

EXEC sp_rename 'WAREHOUSE_ExamSessionAvailableItemsTable_new','WAREHOUSE_ExamSessionAvailableItemsTable';
GO

/*Constraints*/

ALTER TABLE [dbo].[WAREHOUSE_ExamSessionAvailableItemsTable]
	ADD CONSTRAINT [PK_WAREHOUSE_ExamSessionAvailableItemsTable] PRIMARY KEY CLUSTERED (ID)
GO

PRINT 'ADD CONSTRAINT [PK_WAREHOUSE_ExamSessionAvailableItemsTable] PRIMARY KEY CLUSTERED (ID)'

ALTER TABLE [dbo].[WAREHOUSE_ExamSessionAvailableItemsTable]  WITH CHECK 
ADD  CONSTRAINT [FK_WAREHOUSE_ExamSessionAvailableItemsTable_WAREHOUSE_ExamSessionTable] FOREIGN KEY([ExamSessionID])
REFERENCES [dbo].[WAREHOUSE_ExamSessionTable] ([ID])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[WAREHOUSE_ExamSessionAvailableItemsTable] CHECK CONSTRAINT [FK_WAREHOUSE_ExamSessionAvailableItemsTable_WAREHOUSE_ExamSessionTable]
GO


/*Indexes*/

CREATE NONCLUSTERED INDEX [FK_ExamSessionID] ON [dbo].[WAREHOUSE_ExamSessionAvailableItemsTable]
(
	[ExamSessionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [WHAItemsWHItemResponses]
GO

PRINT '[WAREHOUSE_ExamSessionAvailableItemsTable] DONE !!!!'



--select *
--into [WAREHOUSE_ExamSessionAvailableItemsTable_DJBackup]
--from [WAREHOUSE_ExamSessionAvailableItemsTable]