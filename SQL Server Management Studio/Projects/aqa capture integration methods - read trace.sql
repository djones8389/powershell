select ObjectName
	, TextData
	, StartTime
	, EndTime
from ::fn_trace_gettable('E:\Savetrace29112017.trc',default)
--from ::fn_trace_gettable('E:\Savetrace29112017.trc',default)

--Savetrace2
where ObjectName in ('sa_SECUREASSESSINTEGRATION_getCandidateResults_sp')--, 'sa_SECUREASSESSINTEGRATION_UpdateExportedToIntegration_sp')

--exec sa_SECUREASSESSINTEGRATION_getCandidateResults_sp @startDate=N'26/07/2017 23:33:36',@endDate=N'28/11/2017 11:06:31',@examReference=N'',@centreReference=N'',@useSubmissionTime=0,@pageNumber=1,@userID=1,@warehouseStates=N'1'
--exec sa_SECUREASSESSINTEGRATION_getCandidateResults_sp @startDate=N'28/11/2017 11:21:57',@endDate=N'29/11/2017 08:06:31',@examReference=N'',@centreReference=N'',@useSubmissionTime=0,@pageNumber=1,@userID=1,@warehouseStates=N'1'

--update  WAREHOUSE_ExamSessionTable_Shreded
--set WarehouseExamState = 99
--where examSessionId in (54325,54326,54327,54328,54329,54330,54345,54346)


--update  WAREHOUSE_ExamSessionTable
--set WarehouseExamState = 99
--where ID in (54325,54326,54327,54328,54329,54330,54345,54346)

/*
     SELECT    
      ROW_NUMBER() OVER(ORDER BY [WAREHOUSE_ExamSessionTable_Shreded].[warehouseTime]) AS 'RowNumber',
      [WAREHOUSE_ExamSessionTable_Shreded].[examSessionid] AS 'ID',
      [WAREHOUSE_ScheduledExamsTable].[ExamID] AS 'ExamID',
      [WAREHOUSE_ExamSessionTable_Shreded].[warehouseTime] AS 'WarehousedTime',
      [WAREHOUSE_ExamSessionTable_Shreded].[examRef] AS 'ExamRef',
      [WAREHOUSE_ExamSessionTable_Shreded].[examVersionRef] AS 'ExamVersionRef',
      [WAREHOUSE_ScheduledExamsTable].[examVersionId] AS 'ExamVersionID',
      [WAREHOUSE_ExamSessionTable_Shreded].[qualificationRef] AS 'qualificationReference',
      [WAREHOUSE_ExamSessionTable_Shreded].[validFromDate] AS 'start',
      [WAREHOUSE_ExamSessionTable_Shreded].[expiryDate] AS 'end',
      [WAREHOUSE_ExamSessionTable_Shreded].[totalTimeSpent] AS 'ActualDuration',
      Convert(nvarchar(max),[WAREHOUSE_ExamSessionTable_Shreded].[scheduledDurationValue]) AS 'allowedDuration',
      [WAREHOUSE_ExamSessionTable_Shreded].[defaultDuration] AS 'defaultDuration',
      [WAREHOUSE_ExamSessionTable_Shreded].[scheduledDurationReason] AS 'reason',
      [WAREHOUSE_CentreTable].[CentreID] AS 'CentreId',
      [WAREHOUSE_CentreTable].[CentreName] AS 'CentreName',
      [WAREHOUSE_UserTable].[Userid] AS 'CandidateID',
      [WAREHOUSE_UserTable].[Gender] AS 'CandidateGender',
      [WAREHOUSE_UserTable].[DOB] AS 'CandidateDOB',
      [WAREHOUSE_UserTable].[EthnicOrigin] AS 'CandidateEthnicOriginID',
      [WAREHOUSE_UserTable].[CandidateRef] AS 'CandidateRef',
      [WAREHOUSE_UserTable].[Surname] AS 'CandidateSurname',
      [WAREHOUSE_UserTable].[Forename] AS 'CandidateForename',
      [WAREHOUSE_ExamSessionTable_Shreded].[userMark] AS 'CandidateMark',
      [WAREHOUSE_ExamSessionTable_Shreded].[passMark] AS 'PassMark',
      [WAREHOUSE_ExamSessionTable_Shreded].[totalMark] AS 'TotalMark',
      [WAREHOUSE_ExamSessionTable_Shreded].[previousExamState] As 'PreviousExamState',
      [VoidJustificationLookupTable].[name] AS 'VoidReason',
      Convert(nvarchar(max),[WAREHOUSE_ExamSessionTable_Shreded].[passValue]) AS 'PassValue',
      [WAREHOUSE_ExamSessionTable_Shreded].[automaticVerification] AS 'AutoVerify',
      [WAREHOUSE_ExamSessionTable_Shreded].[resultSampled] AS 'resultSampled',
      SUBSTRING(CONVERT(nvarchar(max),[WAREHOUSE_ExamSessionTable_Shreded].[started],113),0,24) AS 'started',
      [WAREHOUSE_ExamSessionTable_Shreded].[submitted] AS 'submitted',
      Convert(xml,Convert(nvarchar(max),'<sectionDataRaw>' + Convert(nvarchar(max),[WAREHOUSE_ExamSessionTable_Shreded].[resultData].query('//section')) + '</sectionDataRaw>')) AS 'sectionDataRaw' ,    
      dbo.fn_getMarkers([WAREHOUSE_ExamSessionTable_Shreded].[examSessionId]) AS 'Marker',
      [WAREHOUSE_ScheduledExamsTable].[ScheduledStartDateTime] AS 'WindowsStartTime',
	  [WAREHOUSE_ExamSessionTable_Shreded].[TakenThroughLocalScan] AS 'TakenThroughLocalScan',
      [WAREHOUSE_CentreTable].[CentreCode] AS 'CentreRef',
      CASE WHEN [WAREHOUSE_ExamSessionTable_Shreded].[adjustedGrade] = '' THEN [WAREHOUSE_ExamSessionTable_Shreded].[originalGrade] ELSE
		ISNULL([WAREHOUSE_ExamSessionTable_Shreded].[adjustedGrade], [WAREHOUSE_ExamSessionTable_Shreded].[originalGrade]) END AS 'Grade',
	  [WAREHOUSE_ExamSessionTable_Shreded].[KeyCode] AS 'keyCode', 
      [WAREHOUSE_UserTable].[ULN] AS 'ULN',
      [WAREHOUSE_ExamSessionTable_Shreded].[reMarkStatus] AS 'ReMarkStatus',
      [WAREHOUSE_ExamSessionTable_Shreded].[CQN] as 'CQN',
      [WAREHOUSE_ExamSessionTable_Shreded].[passType] AS 'PassType',
      [WAREHOUSE_ExamSessionTable_Shreded].[WarehouseExamState] AS 'WarehouseExamState',
	  [WAREHOUSE_ExamSessionTable_Shreded].[examName]  AS 'ExamName',
	  [WAREHOUSE_ExamSessionTable_Shreded].[examVersionName] AS 'ExamVersionName',
	  [WAREHOUSE_ExamSessionTable_Shreded].[itemDataFull] AS 'itemDataFull'
      FROM [dbo].[WAREHOUSE_ExamSessionTable_Shreded]
      INNER JOIN [dbo].[WAREHOUSE_ScheduledExamsTable]
       ON [dbo].[WAREHOUSE_ScheduledExamsTable].[ID] = [WAREHOUSE_ExamSessionTable_Shreded].[WAREHOUSEScheduledExamID]
      INNER JOIN [dbo].[WAREHOUSE_CentreTable]
       ON [dbo].[WAREHOUSE_CentreTable].[ID] = [WAREHOUSE_ScheduledExamsTable].[WAREHOUSECentreID]
      INNER JOIN [dbo].[WAREHOUSE_UserTable]
       ON [dbo].[WAREHOUSE_UserTable].[ID] = [WAREHOUSE_ExamSessionTable_Shreded].[WAREHOUSEUserID]
      LEFT JOIN [dbo].[VoidJustificationLookupTable]
       ON [dbo].[WAREHOUSE_ExamSessionTable_Shreded].[voidJustificationLookupTableId] = [VoidJustificationLookupTable].[ID]
        where keycode = '4KMFJKA7'

*/