set identity_insert PRV_BritishCouncil_SecureAssess..WAREHOUSE_ExamSessionItemResponseTable on

INSERT PRV_BritishCouncil_SecureAssess..WAREHOUSE_ExamSessionItemResponseTable (ID, WAREHOUSEExamSessionID, ItemID, ItemVersion, ItemResponseData, MarkerResponseData, ItemMark, MarkingIgnored, DerivedResponse, ShortDerivedResponse)
select ID, WAREHOUSEExamSessionID, ItemID, ItemVersion, ItemResponseData, MarkerResponseData, ItemMark, MarkingIgnored, DerivedResponse, ShortDerivedResponse
from [WHItemResponses].[dbo].[WAREHOUSE_ExamSessionItemResponseTable] 
where WAREHOUSEExamSessionID in (Select ID from PRV_BritishCouncil_SecureAssess.dbo.WAREHOUSE_ExamSessionTable)

set identity_insert PRV_BritishCouncil_SecureAssess..WAREHOUSE_ExamSessionItemResponseTable off

select count(ID)
FROM PRV_BritishCouncil_SecureAssess..WAREHOUSE_ExamSessionItemResponseTable