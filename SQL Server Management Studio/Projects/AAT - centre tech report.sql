/*
SELECT COUNT(CentreID)
	, CentreID
FROM ExamSessionTable EST
INNER JOIN ScheduledExamsTable SCET
on SCET.ID = EST.ScheduledExamID
WHERE ClientInformation IS NOT NULL
group by CentreID
order by 1 desc


SELECT clientInformation
FROM ExamSessionTable
where Keycode = 'FMH4Y6A6'

*/

;WITH CTE AS 
(
	SELECT ROW_NUMBER() OVER(PARTITION BY [MAC Address] ORDER BY [started] DESC) AS [N], *
	FROM (
		SELECT E.ID
			,E.clientInformation.value('(/clientInformation/systemConfiguration[1]/identification/computerName)[1]', 'nvarchar(max)') AS [Computer Name]
			,ESCAT.stateChangeDate [started]
			,E.clientInformation.value('(/clientInformation/systemConfiguration[1]/environment/operatingSystem/version)[1]', 'nvarchar(max)') AS [Operating System]
			,E.clientInformation.value('(/clientInformation/systemConfiguration[1]/environment/operatingSystem/servicePack)[1]', 'nvarchar(max)') AS [Service Pack]
			,SUBSTRING((
					SELECT N', '
						,net.ver.value('.', 'nvarchar(max)')
					FROM E.clientInformation.nodes('/clientInformation/systemConfiguration/dotNet[1]/version') net(ver)
					FOR XML PATH('')
					), 3, 1000) AS [.NET Framework(s)]
			--,E.clientInformation.value('(/clientInformation/systemConfiguration[1]/flashPlayer/name)[1]', 'nvarchar(max)') AS [Flash Player]
			,E.clientInformation.value('(/clientInformation/systemConfiguration[1]/flashPlayer/version)[1]', 'nvarchar(max)') AS [Flash Player]
			,E.clientInformation.value('(/clientInformation/systemConfiguration[1]/secureClient/CurrentVersion)[1]', 'bigint') [SecureClientVersion]
			,SUBSTRING((
					SELECT N', '
						,(disc.drive.query('.').value('(drive/name)[1]', 'nvarchar(max)')) + N' ' + (disc.drive.query('.').value('(drive/totalSize)[1]', 'nvarchar(max)')) + N' (' + (disc.drive.query('.').value('(drive/freeSpace)[1]', 'nvarchar(max)')) + N' Free)'
					FROM E.clientInformation.nodes('/clientInformation/systemConfiguration[1]/environment/hardDisk/drives/drive') disc(drive)
					FOR XML PATH('')
					), 3, 1000) AS [Hard Disk(s)]
			,E.clientInformation.value('(/clientInformation/systemConfiguration[1]/environment/memory/installedPhysicalMemory)[1]', 'nvarchar(max)') AS [Memory]
			,E.clientInformation.value('(/clientInformation/systemConfiguration[1]/environment/processor/name)[1]', 'nvarchar(max)') AS [CPU]
			,E.clientInformation.value('(/clientInformation/systemConfiguration[1]/environment/video/resolution)[1]', 'nvarchar(max)') AS [Display Resolution]
			,CASE E.clientInformation.exist('/clientInformation/systemConfiguration[1]/environment/soundCard/name')
				WHEN 1
					THEN N'Yes'
				ELSE N'No'
				END AS [Audio Available]
			,E.clientInformation.value('(/clientInformation/systemConfiguration[1]/identification/macAddress)[1]', 'nvarchar(max)') AS [MAC Address]
			,E.clientInformation.value('(/clientInformation/systemConfiguration[1]/identification/ipAddress)[1]', 'nvarchar(max)') AS [IP Address]
			,E.KeyCode
		FROM dbo.ExamSessionTable E WITH (NOLOCK)
		INNER JOIN (
			SELECT examSessionID
				, MAX(stateChangeDate) stateChangeDate
			FROM ExamStateChangeAuditTable  WITH (NOLOCK)
			WHERE newState = 6
			group by examSessionID
		) ESCAT 
			on E.ID = ESCAT.ExamSessionID
		INNER JOIN ScheduledExamsTable SCET  WITH (NOLOCK)
		ON SCET.ID = ScheduledExamID
		INNER JOIN CentreTable CT  WITH (NOLOCK)
		on CT.ID = SCET.CentreID	

		WHERE Keycode = '4lyh9fa6'

		--UNION

		--SELECT E.ExamSessionID
		--	,E.clientInformation.value('(/clientInformation/systemConfiguration[1]/identification/computerName)[1]', 'nvarchar(max)') AS [Computer Name]
		--	,S.[started]
		--	,E.clientInformation.value('(/clientInformation/systemConfiguration[1]/environment/operatingSystem/version)[1]', 'nvarchar(max)') AS [Operating System]
		--	,E.clientInformation.value('(/clientInformation/systemConfiguration[1]/environment/operatingSystem/servicePack)[1]', 'nvarchar(max)') AS [Service Pack]
		--	,SUBSTRING((
		--			SELECT N', '
		--				,net.ver.value('.', 'nvarchar(max)')
		--			FROM E.clientInformation.nodes('/clientInformation/systemConfiguration/dotNet[1]/version') net(ver)
		--			FOR XML PATH('')
		--			), 3, 1000) AS [.NET Framework(s)]
		--	,E.clientInformation.value('(/clientInformation/systemConfiguration[1]/flashPlayer/name)[1]', 'nvarchar(max)') AS [Flash Player]
		--	,E.clientInformation.value('(/clientInformation/systemConfiguration[1]/flashPlayer/version)[1]', 'nvarchar(max)') AS [Flash Player Version]
		--	,SUBSTRING((
		--			SELECT N', '
		--				,(disc.drive.query('.').value('(drive/name)[1]', 'nvarchar(max)')) + N' ' + (disc.drive.query('.').value('(drive/totalSize)[1]', 'nvarchar(max)')) + N' (' + (disc.drive.query('.').value('(drive/freeSpace)[1]', 'nvarchar(max)')) + N' Free)'
		--			FROM E.clientInformation.nodes('/clientInformation/systemConfiguration[1]/environment/hardDisk/drives/drive') disc(drive)
		--			FOR XML PATH('')
		--			), 3, 1000) AS [Hard Disk(s)]
		--	,E.clientInformation.value('(/clientInformation/systemConfiguration[1]/environment/memory/installedPhysicalMemory)[1]', 'nvarchar(max)') AS [Memory]
		--	,E.clientInformation.value('(/clientInformation/systemConfiguration[1]/environment/processor/name)[1]', 'nvarchar(max)') AS [CPU]
		--	,E.clientInformation.value('(/clientInformation/systemConfiguration[1]/environment/video/resolution)[1]', 'nvarchar(max)') AS [Display Resolution]
		--	,CASE E.clientInformation.exist('/clientInformation/systemConfiguration[1]/environment/soundCard/name')
		--		WHEN 1
		--			THEN N'Yes'
		--		ELSE N'No'
		--		END AS [Audio Available]
		--	,E.clientInformation.value('(/clientInformation/systemConfiguration[1]/identification/macAddress)[1]', 'nvarchar(max)') AS [MAC Address]
		--	,E.clientInformation.value('(/clientInformation/systemConfiguration[1]/identification/ipAddress)[1]', 'nvarchar(max)') AS [IP Address]
		--	,E.KeyCode
		--FROM dbo.WAREHOUSE_ExamSessionTable E WITH (NOLOCK)
		--INNER JOIN dbo.WAREHOUSE_ExamSessionTable_Shreded S WITH (NOLOCK) ON E.ID = S.examSessionId
		--WHERE ClientInformation IS NOT NULL
		--	AND CentreID = @Centreid
	) A
)

SELECT [Computer Name]
	,[IP Address]
	,[MAC Address]
	,[Operating System]
	,[Service Pack]
	,[.NET Framework(s)]
	,[Flash Player]
	,[SecureClientVersion]
	,[Hard Disk(s)]
	,[Memory]
	,[Display Resolution]
	,[Audio Available]
	,[KeyCode]
	,[Started]
FROM CTE
WHERE N = 1