USE AAT_SecureAssess

--C:\Users\977496-davej2\Desktop\AATBulkAssign\1_Centres_AssignQuals - CentreQualTable.csv
--C:\Users\977496-davej2\Desktop\AATBulkAssign\2_Users_AssessorsIV_QualsandCentres  -AllActiveCentresAndAssignedUserRolesTable.csv
--C:\Users\977496-davej2\Desktop\AATBulkAssign\3_Users_CentreAdmin_QualsandCentres.csv


if OBJECT_ID('tempdb..#1_Centres_AssignQuals') is not null drop table #1_Centres_AssignQuals;

create table #1_Centres_AssignQuals (
	centrename nvarchar(200)
	,centrecode nvarchar(200)
	,qual1 nvarchar(200)
	,qual2 nvarchar(200)
	,qual3 nvarchar(200)
	,qual4 nvarchar(200)
);
go

bulk insert #1_Centres_AssignQuals
from 'C:\Users\977496-davej2\Desktop\AATBulkAssign\1_Centres_AssignQuals - CentreQualTable.csv'
with (fieldterminator = ',', rowterminator='\n')
go

DECLARE @CentreQualInsert TABLE (
	CentreID int
	,QualificationID int
);

INSERT @CentreQualInsert
SELECT CT.ID	
	, B.ID
FROM #1_Centres_AssignQuals A
INNER JOIN CentreTable CT
on CT.CentreName = A.centrename
	and CT.CentreCode = A.centrecode
INNER JOIN IB3QualificationLookup B
on B.QualificationName in (a.qual1, a.qual2, a.qual3, a.qual4);
