SELECT 'DROP DATABASE ' + QUOTENAME(Name) +';'
	,'ALTER DATABASE ' + QUOTENAME(Name) + ' SET OFFLINE;'
from sys.databases
where database_id > 4
	and name like 'Professional%'
	and name not like '%deleted%'


select 'kill ' + convert(varchar(5),spid)
	,db_name(dbid)
from sys.sysprocesses s
where dbid > 4
	--and  db_name(dbid) = 'SaxionDefect_ItemBank'
order by 2;


kill 278
kill 232
kill 277
kill 93
kill 184
kill 92
kill 124
kill 310
kill 117
kill 356

DROP DATABASE [Professional_AnalyticsManagement];
DROP DATABASE [Professional_ContentAuthor];
DROP DATABASE [Professional_ItemBank];
DROP DATABASE [Professional_SecureAssess];
DROP DATABASE [Professional_SecureMarker];
DROP DATABASE [Professional_SurpassDataWarehouse];
DROP DATABASE [Professional_SurpassManagement];


ALTER DATABASE [Professional_AnalyticsManagement_deleted_2017-08-21_151517] SET ONLINE;
ALTER DATABASE [Professional_ContentAuthor_deleted_2017-08-21_151407] SET ONLINE;
ALTER DATABASE [Professional_ItemBank_deleted_2017-08-21_151322] SET ONLINE;
ALTER DATABASE [Professional_SecureAssess_deleted_2017-08-21_151222] SET ONLINE;
ALTER DATABASE [Professional_SecureMarker_deleted_2017-08-21_151604] SET ONLINE;
ALTER DATABASE [Professional_SurpassDataWarehouse_deleted_2017-08-21_151519] SET ONLINE;
ALTER DATABASE [Professional_SurpassManagement_deleted_2017-08-21_151448] SET ONLINE;

exec sp_renamedb 'Professional_AnalyticsManagement_deleted_2017-08-21_151517','Professional_AnalyticsManagement'
exec sp_renamedb 'Professional_ContentAuthor_deleted_2017-08-21_151407','Professional_ContentAuthor'
exec sp_renamedb 'Professional_ItemBank_deleted_2017-08-21_151322','Professional_ItemBank'
exec sp_renamedb 'Professional_SecureAssess_deleted_2017-08-21_151222','Professional_SecureAssess'
exec sp_renamedb 'Professional_SecureMarker_deleted_2017-08-21_151604','Professional_SecureMarker'
exec sp_renamedb 'Professional_SurpassDataWarehouse_deleted_2017-08-21_151519','Professional_SurpassDataWarehouse'
exec sp_renamedb 'Professional_SurpassManagement_deleted_2017-08-21_151448','Professional_SurpassManagement'


USE [Professional_SecureAssess]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'Professional_SecureAssessUser') CREATE USER [Professional_SecureAssessUser]FOR LOGIN [Professional_SecureAssessUser]; exec sp_addrolemember'db_owner', 'Professional_SecureAssessUser' 
USE [Professional_SecureAssess]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'Professional_ETLUser') CREATE USER [Professional_ETLUser]FOR LOGIN [Professional_ETLUser]; exec sp_addrolemember'db_owner', 'Professional_ETLUser' 
USE [Professional_ItemBank]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'Professional_ETLUser') CREATE USER [Professional_ETLUser]FOR LOGIN [Professional_ETLUser]; exec sp_addrolemember'ETLRole', 'Professional_ETLUser' 
USE [Professional_ItemBank]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'Professional_ItemBankUser') CREATE USER [Professional_ItemBankUser]FOR LOGIN [Professional_ItemBankUser]; exec sp_addrolemember'db_owner', 'Professional_ItemBankUser' 
USE [Professional_ItemBank]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'Professional_ETLUser') CREATE USER [Professional_ETLUser]FOR LOGIN [Professional_ETLUser]; exec sp_addrolemember'db_datareader', 'Professional_ETLUser' 
USE [Professional_ItemBank]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'Professional_ETLUser') CREATE USER [Professional_ETLUser]FOR LOGIN [Professional_ETLUser]; exec sp_addrolemember'db_datawriter', 'Professional_ETLUser' 
USE [Professional_ContentAuthor]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'Professional_ContentAuthorUser') CREATE USER [Professional_ContentAuthorUser]FOR LOGIN [Professional_ContentAuthorUser]; exec sp_addrolemember'db_owner', 'Professional_ContentAuthorUser' 
USE [Professional_ContentAuthor]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'Professional_ETLUser') CREATE USER [Professional_ETLUser]FOR LOGIN [Professional_ETLUser]; exec sp_addrolemember'db_datareader', 'Professional_ETLUser' 
USE [Professional_SurpassManagement]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'Professional_SurpassManagementUser') CREATE USER [Professional_SurpassManagementUser]FOR LOGIN [Professional_SurpassManagementUser]; exec sp_addrolemember'db_owner', 'Professional_SurpassManagementUser' 
USE [Professional_SurpassManagement]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'Professional_ETLUser') CREATE USER [Professional_ETLUser]FOR LOGIN [Professional_ETLUser]; exec sp_addrolemember'db_datareader', 'Professional_ETLUser' 
USE [Professional_AnalyticsManagement]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'Professional_AnalyticsManagementUser') CREATE USER [Professional_AnalyticsManagementUser]FOR LOGIN [Professional_AnalyticsManagementUser]; exec sp_addrolemember'db_owner', 'Professional_AnalyticsManagementUser' 
USE [Professional_SurpassDataWarehouse]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'Professional_ETLUser') CREATE USER [Professional_ETLUser]FOR LOGIN [Professional_ETLUser]; exec sp_addrolemember'db_owner', 'Professional_ETLUser' 
USE [Professional_SecureMarker]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'Professional_SecureMarkerUser') CREATE USER [Professional_SecureMarkerUser]FOR LOGIN [Professional_SecureMarkerUser]; exec sp_addrolemember'db_owner', 'Professional_SecureMarkerUser' 