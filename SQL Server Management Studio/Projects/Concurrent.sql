SELECT COUNT(A.ID)
	, [year]
	, [month]
	, [day]
	,[Time They Started]
FROM (
SELECT 
	EST.ID
	, YEAR(MIN(StateChangeDate)) [Year]
	, MONTH(MIN(StateChangeDate)) [Month]
	, DAY(MIN(StateChangeDate)) [Day]
	, CONVERT(time, MIN(StateChangeDate)) [Time They Started]
	, DATEDIFF(MINUTE, MIN(StateChangeDate), MAX(StateChangeDate)) [TimeSpentInExam_Minutes]
FROM ExamSessionTable EST

INNER JOIN (
	select  ExamSessionID
	from ExamStateChangeAuditTable 
	where
		NewState IN (6,9)
	group by ExamSessionID
	having count(*) > 1
) MultiStates
	ON MultiStates.ExamSessionID = EST.ID

INNER JOIN ExamStateChangeAuditTable ESCAT
on EST.ID = ESCAT.ExamSessionID
where  NewState IN (6,9)
GROUP BY EST.ID

) A

GROUP BY [year]
	, [month]
	, [day]
	,[Time They Started]
ORDER BY 1 DESC

--ORDER BY [Year],[Month],[Day],[Time They Started],[TimeSpentInExam_Minutes]
/*

DECLARE @test datetime = '2010-05-26 14:00:00.000'
SELECT CONVERT(time, @test)
SELECT SUBSTRING(CONVERT(time, @test), 0, 10)

*/


--WITH CTE AS (

--select ROW_NUMBER() OVER (
--	PARTITION BY ExamSessionID
--	ORDER BY ExamSessionID
--			--,	NewState 
--		) R
--	,  ExamSessionID
--	--,  NewState 
--	--,  StateChangeDate
--from ExamStateChangeAuditTable 
--where examsessionid = 271--23711
--	and NewState IN (6,9)
--group by ExamSessionID
--having count(*) > 1

--)
--SELECT * FROM CTE WHERE R > 1

	select 
	  ExamSessionID
	from ExamStateChangeAuditTable 
	where
		NewState IN (6,9)
		and examsessionid = 23711
	group by ExamSessionID
	having count(*) > 1


--select * from ExamStateChangeAuditTable where examsessionid = 22170
--SELECT TOP 100 *
--FROM WAREHOUSE_ExamSessionTable
--WHERE EXISTS (SELECT WarehouseExamSessionID FROM WAREHOUSE_ExamStateAuditTable WHERE WarehouseExamSessionID = WAREHOUSE_ExamSessionTable.ID AND ExamState = 1)
--AND NOT EXISTS (SELECT WarehouseExamSessionID FROM WAREHOUSE_ExamStateAuditTable WHERE WarehouseExamSessionID = WAREHOUSE_ExamSessionTable.ID AND ExamState = 4)
--AND EXISTS (SELECT WarehouseExamSessionID FROM WAREHOUSE_ExamStateAuditTable WHERE WarehouseExamSessionID = WAREHOUSE_ExamSessionTable.ID AND ExamState = 2)
--AND ExportToSecureMarker = 1
