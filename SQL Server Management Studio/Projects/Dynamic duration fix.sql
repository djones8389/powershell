DECLARE @Dynamic nvarchar(max) = '';

SELECT @Dynamic += CHAR(13) +
	'
	DECLARE @'+cast(ID as nvarchar(10))+' xml = (SELECT [StructureXML] FROM [dbo].[ExamSessionTable] WHERE ID = '+cast(ID as nvarchar(10))+')
	
	SET @'+cast(ID as nvarchar(10))+'.modify(''replace value of (/assessmentDetails/scheduledDuration/value/text())[1] with ("'+cast([defaultDuration] as varchar(4))+'")'')
	
	UPDATE ExamSessionTable
	SET [ScheduledDurationXml].modify(''replace value of (/duration/value/text())[1] with ("'+cast([defaultDuration] as varchar(4))+'")'')
		, ScheduledDuration = [defaultDuration]
		, StructureXML = @'+cast(ID as nvarchar(10))+'
	WHERE ID = '''+cast(ID as nvarchar(10))+'''
'
FROM [dbo].[ExamSessionTable]
WHERE examState != 13
	 and DurationMode <> 0
	 and scheduledduration <> [defaultDuration];

PRINT(@Dynamic);










--select 
--	Pinnumber
--	, KeyCode
--	, scheduledduration	
--	,[DurationMode]
--	,[ScheduledDurationXml].value('data(duration/value)[1]','tinyint') [ScheduledDurationXml]
--	,[defaultDuration]
--from ExamSessionTable (READUNCOMMITTED)
--where KeyCode = 'CHJXFW4Z'
