--USE STG_SANDBOX_SQA_CPProjectAdmin

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

SELECT PLT.NAME as ProjectName
	,PLT.ID as ProjectID
	,Forename + ' ' + Surname as ForeName_And_Surname
	,UT.Username
	,CP.*
	,ISL.SpecificationName as SpecificationID	
	, cTL.Description
	, c.ID	
	, C.ContractID
	, CompletionDate
FROM Contract AS C
INNER JOIN ProjectListTable AS PLT ON PLT.ID = C.ProjectID
INNER JOIN UserTable AS UT ON UT.UserID = C.UserID
LEFT JOIN ContractPage AS CP ON CP.ContractID = C.ID
LEFT JOIN ContractTypeLookup AS CTL ON CTL.ID = C.Type
LEFT JOIN ItemSpecificationLookup AS ISL ON ISL.ID = C.ItemSpecificationID

where 
	--plt.id = 1140
	--and ut.userid = 1881
	--and description = 'Writer'
	completionDate is null

	ORDER  BY 1 ASC;

SELECT *
FROM Contract C (NOLOCK)
INNER JOIN UserTable U (NOLOCK) on C.UseriD = U.UserID
LEFT JOIN ContractPage AS CP (NOLOCK)  ON CP.ContractID = C.ID
WHERE C.ContractID = 2492
	
--DELETE FROM ContractPage --where ContractID = 3605;
--DELETE FROM Contract-- where ID = 3605 and Type = 1;


--SELECT * FROM Contract where ID = 3343

--UPDATE Contract 
--set CompletionDate = '01/01/2011 09:00:00'
--where ID = 3346