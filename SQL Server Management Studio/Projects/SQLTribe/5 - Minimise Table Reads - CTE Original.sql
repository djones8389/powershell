USE [STG_BC2_SecureMarker]

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

SET STATISTICS IO ON

DECLARE
    @groupId INT = 929
	,@getAllExaminers bit = 1
--AS 
    BEGIN
        SET NOCOUNT ON;
        
        DECLARE @examId INT=(
            SELECT ExamId
            FROM   dbo.GroupDefinitions
            WHERE  ID = @groupID
        )
        
	;WITH RealMarkedResponses AS (	
		SELECT U.ID AS ID,
			   UGR.GroupDefinitionID AS GroupId
		FROM   dbo.Users U
			   INNER JOIN dbo.UniqueGroupResponses UGR ON  UGR.GroupDefinitionID = @groupId
			   INNER JOIN dbo.AssignedGroupMarks AGM ON AGM.UniqueGroupResponseId = UGR.ID  AND AGM.UserId = U.ID
		WHERE  UGR.CI=0
			   AND AGM.IsConfirmedMark=1
			   AND AGM.UserId = U.ID
		GROUP BY U.ID,
				 UGR.GroupDefinitionID
				 
		),
	MarkedCIs AS (
		SELECT U.ID AS ID,
			   UGR.GroupDefinitionID AS GroupId
		FROM   dbo.Users U
			   LEFT JOIN dbo.UniqueGroupResponses UGR ON  UGR.GroupDefinitionID = @groupId
			   LEFT JOIN dbo.AssignedGroupMarks AGM ON  AGM.UniqueGroupResponseId = UGR.ID AND AGM.UserId = U.ID
		WHERE  UGR.CI_Review=1
			   AND (AGM.MarkingMethodId=2 OR AGM.MarkingMethodId=3)
		GROUP BY U.ID,
				 UGR.GroupDefinitionID
	)
	SELECT U.ID										AS ID
			  ,U.Forename							AS Forename
			  ,U.Surname							AS Surname
			  ,ISNULL(QM.AssignedQuota ,0)			AS Quota
			  ,ISNULL(QM.NumberMarked ,0)			AS NumberMarked
			  ,ISNULL(QM.Within1CIofSuspension ,0)	AS Within1CIOfSuspension
			  ,ISNULL(QM.NumItemsParked ,0)			AS NumItemsParked
			  ,ISNULL(QM.MarkingSuspended ,0)		AS Suspended
			  ,ISNULL(QM.NumberSuspended ,0)		AS NumberSuspended
			  ,CAST(CASE WHEN RealMarkedResponses.ID IS NULL THEN 0 ELSE 1 END AS BIT) AS HasRealMarkedResponses
			  ,CAST(CASE WHEN MarkedCIs.ID IS NULL THEN 0 ELSE 1 END  AS BIT) AS HasMarkedCIs
		FROM   dbo.Users U
			   LEFT JOIN dbo.QuotaManagement QM ON (QM.UserId = U.ID AND QM.GroupId = @groupId)
			   LEFT JOIN RealMarkedResponses ON RealMarkedResponses.ID = U.ID AND RealMarkedResponses.GroupId = @groupId
			   LEFT JOIN MarkedCIs ON MarkedCIs.ID = U.ID AND MarkedCIs.GroupId = QM.GroupId
		WHERE  U.ID IN (SELECT AR.UserID
						FROM   dbo.AssignedUserRoles AR
							   INNER JOIN dbo.RolePermissions RP
									ON  RP.RoleID = AR.RoleID
						WHERE  RP.PermissionID IN (12 ,13 ,14)
							   AND (AR.ExamID=@examId OR AR.ExamID=0))
			   AND U.Retired = 0
			   AND (U.InternalUser=0 OR U.Username='superuser')
			   AND (ISNULL(QM.NumberMarked ,0) > 0 AND @getAllExaminers = 0 OR @getAllExaminers = 1)
			   ORDER BY 1
		OPTION(OPTIMIZE FOR (@groupId UNKNOWN))
	
	
    END


GO


