use PPD_AAT_SecureAssess

--SELECT '0' AS '@errorCode',
--       (

    SELECT [WAREHOUSE_ExamSessionTable].ID AS 'examInstanceId',
           [WAREHOUSE_ScheduledExamsTable].ExamID AS 'examId',
           [WAREHOUSE_ScheduledExamsTable].examName AS 'examName',
           [WAREHOUSE_ExamSessionTable].[WAREHOUSEUserID] AS 'candidateId',
           REPLACE([WAREHOUSE_ExamSessionTable_Shreded].Forename, '&apos;', '''') AS 'candidateForename',
           REPLACE([WAREHOUSE_ExamSessionTable_Shreded].Surname, '&apos;', '''') AS 'candidateSurname',
           [WAREHOUSE_ExamSessionTable].StructureXML.query ('assessmentDetails/*') AS 'assessmentDetails',
           [WAREHOUSE_ExamSessionTable].MarkerData AS 'markerData',
           [WAREHOUSE_UserTable].SpecialRequirements AS 'specialRequirements',
           [WAREHOUSE_UserTable].MiddleName AS 'candidateMiddleName',
           [WAREHOUSE_UserTable].DOB AS 'candidateDOB',
           [WAREHOUSE_ExamSessionTable_Shreded].Gender AS 'gender',
           [WAREHOUSE_ExamSessionTable_Shreded].AddressLine1 AS 'candidateAddressLine1',
           [WAREHOUSE_ExamSessionTable_Shreded].AddressLine2 AS 'candidateAddressLine2',
           [WAREHOUSE_ExamSessionTable_Shreded].Town AS 'candidateTown',
           [WAREHOUSE_ExamSessionTable_Shreded].PostCode AS 'candidatePostcode',
           [WAREHOUSE_UserTable].email AS 'candidateEmail',
           [WAREHOUSE_UserTable].Telephone AS 'candidateTelephone',
           [WAREHOUSE_ExamSessionTable_Shreded].County AS 'candidateCounty',
           [WAREHOUSE_ExamSessionTable_Shreded].Country AS 'candidateCountry',
           [WAREHOUSE_UserTable].EthnicOrigin AS 'candidateEthnicOrigin',
           CAST([WAREHOUSE_ExamSessionTable_Shreded].StructureXML AS XML).query ('requiresValidation//*') AS 'requiredResultsRelease',
           [WAREHOUSE_ExamSessionTable].ExamStateChangeAuditXML.query ('exam/*') AS 'stateChanges',           
           
           dbo.sp_wareHouse_GetExamSessionAudit ([WAREHOUSE_ExamSessionTable].[ID]) .query ('ExamDurationAudit/*') AS 'ExamDurationAuditORIGINAL',
           
           [WAREHOUSE_ExamSessionTable_Shreded].Forename + ' ' + [WAREHOUSE_ExamSessionTable_Shreded].Surname AS 'ExamDurationAudit/ExamDurationChange/user',
		   [WAREHOUSE_ExamSessionTable].IsProjectBased AS 'ExamDurationAudit/ExamDurationChange/projectBased',    
		   [ChangeDateSubmitted]			AS 'ExamDurationAudit/ExamDurationChange/dateChanged',
		   [ScheduledStartDateTime]			AS 'ExamDurationAudit/ExamDurationChange/startDate',
		   [OriginalExamDuration]			AS 'ExamDurationAudit/ExamDurationChange/originalDuration',
		   [NewExamDuration]				AS 'ExamDurationAudit/ExamDurationChange/newDuration',
		   [Reason]							AS 'ExamDurationAudit/ExamDurationChange/Reason',         

           dbo.fn_getMarkersasxml ([WAREHOUSE_ExamSessionTable].[ID]) .query ('//marker') AS 'markers',
           dbo.fn_getResultsReleaseUser ([WAREHOUSE_ExamSessionTable].[ID]) .query ('//user/*') AS 'resultsReleaseUser',
           [WAREHOUSE_ExamSessionTable].EnableOverrideMarking AS 'EnableOverrideMarking',
           [WAREHOUSE_ExamSessionTable].IsProjectBased AS 'isProjectBased',
           [WAREHOUSE_ExamSessionDurationAuditTable].*
           ,[WAREHOUSE_ScheduledExamsTable].ScheduledStartDateTime
    FROM [WAREHOUSE_ExamSessionTable]
    INNER JOIN [WAREHOUSE_ScheduledExamsTable]
    ON [WAREHOUSE_ScheduledExamsTable].ID = [WAREHOUSE_ExamSessionTable].[WAREHOUSEScheduledExamID]
    INNER JOIN [WAREHOUSE_ExamSessionTable_Shreded]
    ON [WAREHOUSE_ExamSessionTable_Shreded].ExamsessionID = [WAREHOUSE_ExamSessionTable].ID
    INNER JOIN [WAREHOUSE_UserTable]
    ON [WAREHOUSE_ExamSessionTable].[WAREHOUSEUserID] = [WAREHOUSE_UserTable].ID
    LEFT JOIN [WAREHOUSE_ExamSessionDurationAuditTable]
    ON [WAREHOUSE_ExamSessionDurationAuditTable].[Warehoused_ExamSessionID] = [WAREHOUSE_ExamSessionTable].ID    
    WHERE [WAREHOUSE_ExamSessionTable].ID = 1559085
      AND [WAREHOUSE_ScheduledExamsTable].[OriginatorId] = 1
    
--) A    
   
   FOR XML PATH ('examScript') , ROOT ('return') , TYPE
    
 --       ) 
	
	--FOR XML PATH ('result') ;

--select dbo.sp_wareHouse_GetExamSessionAudit (1559085)