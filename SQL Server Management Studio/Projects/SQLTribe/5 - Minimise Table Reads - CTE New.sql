USE [STG_BC2_SecureMarker]

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

SET STATISTICS IO ON

DECLARE
    @groupId INT = 929
	,@getAllExaminers bit = 1

    BEGIN
        SET NOCOUNT ON;
        
        DECLARE @examId INT= (
            SELECT ExamId
            FROM   dbo.GroupDefinitions
            WHERE  ID = @groupID
        )

	SELECT  U.ID									AS ID
			  ,U.Forename							AS Forename
			  ,U.Surname							AS Surname
			  ,ISNULL(QM.AssignedQuota ,0)			AS Quota
			  ,ISNULL(QM.NumberMarked ,0)			AS NumberMarked
			  ,ISNULL(QM.Within1CIofSuspension ,0)	AS Within1CIOfSuspension
			  ,ISNULL(QM.NumItemsParked ,0)			AS NumItemsParked
			  ,ISNULL(QM.MarkingSuspended ,0)		AS Suspended
			  ,ISNULL(QM.NumberSuspended ,0)		AS NumberSuspended
			  ,ISNULL(MAX(HasRealMarkedResponses), 0)  AS HasRealMarkedResponses
			  ,ISNULL(MAX(HasMarkedCIs),0)			   AS HasMarkedCIs
		FROM   dbo.Users U
			   LEFT JOIN dbo.QuotaManagement QM ON (QM.UserId = U.ID AND QM.GroupId = @groupId)
			   LEFT JOIN   (
					SELECT 
						UserId
					 ,   GroupDefinitionID
					 ,	 CASE WHEN CI=0
						   AND IsConfirmedMark=1				  
							THEN '1'
						ELSE '0'
						END as 'HasRealMarkedResponses'
					,  CASE WHEN CI_Review=1
						AND (MarkingMethodId=2 OR MarkingMethodId=3)
						THEN '1'
						ELSE '0'
						END AS 'HasMarkedCIs'
   					FROM 
					(
					SELECT AGM.UserId
						  ,UGR.GroupDefinitionID
						  ,UGR.CI
						  ,AGM.IsConfirmedMark		  
						  ,UGR.CI_Review
						  ,AGM.MarkingMethodId
					FROM  dbo.UniqueGroupResponses UGR
					LEFT JOIN dbo.AssignedGroupMarks AGM ON  AGM.UniqueGroupResponseId = UGR.ID
						where UGR.GroupDefinitionID = @groupId	
 				 ) B
			) A

			ON A.UserId = U.id
				AND GroupDefinitionID = @groupId
			
			INNER JOIN AssignedUserRoles AR ON AR.UserID = U.ID
			INNER JOIN dbo.RolePermissions RP ON  RP.RoleID = AR.RoleID


		WHERE  U.Retired = 0
			   AND (U.InternalUser=0 OR U.Username='superuser')
			   AND (ISNULL(QM.NumberMarked ,0) > 0 AND @getAllExaminers = 0 OR @getAllExaminers = 1)
			   AND  RP.PermissionID IN (12 ,13 ,14)
			   AND (AR.ExamID=@examId OR AR.ExamID=0)


	GROUP BY ID
		, Forename
		, Surname
		, ISNULL(QM.AssignedQuota ,0)	
		, NumberMarked
		, Within1CIOfSuspension
		, NumItemsParked
	    , ISNULL(QM.MarkingSuspended ,0)
		, ISNULL(QM.NumberSuspended ,0)		

		OPTION(OPTIMIZE FOR (@groupId UNKNOWN))
	
	
    END


GO
