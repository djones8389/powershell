USE PPD_AAT_SecureAssess

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

--SELECT * FROM [PPD_AAT_ItemBank].dbo.AssessmentGroupTable WHERE ID = 2160
--SELECT * FROM [PPD_AAT_ItemBank].dbo.AssessmentTable WHERE AssessmentGroupID = 2160
--SELECT * FROM dbo.ScheduledExamsTable WHERE id = 2230501

--SELECT * FROM [PPD_AAT_ItemBank].dbo.AssessmentTable WHERE AssessmentGroupID = 2160
SELECT est.keycode
	,se.ScheduledStartDateTime
	,ct.CentreCode
	,ib3.QualificationRef
	,at.ExternalReference
FROM ExamSessionTable est
INNER JOIN ScheduledExamsTable se ON est.ScheduledExamID = se.id
INNER JOIN CentreTable ct ON se.CentreID = ct.ID
INNER JOIN IB3QualificationLookup ib3 ON se.qualificationId = ib3.id
INNER JOIN [PPD_AAT_ItemBank].dbo.AssessmentGroupTable agt ON agt.QualificationID = se.qualificationId
	AND agt.id = se.ExamID
INNER JOIN [PPD_AAT_ItemBank].dbo.AssessmentTable at ON at.AssessmentGroupID = agt.ID
	AND at.id = se.examVersionID
--WHERE ScheduledStartDateTime >= '5/18/2017'



use PPD_AAT_SecureAssess
select  
	 est.keycode
	,se.ScheduledStartDateTime
	,ct.CentreCode
	,ib3.QualificationRef
	,at.ExternalReference

from ExamSessionTable est
inner join ScheduledExamsTable se on est.ScheduledExamID=se.id
inner join CentreTable ct on se.CentreID=ct.ID
inner join IB3QualificationLookup ib3 on se.qualificationId = ib3.id 
inner join [PPD_AAT_ItemBank].dbo.AssessmentTable at on at.ID=se.examVersionID
--where ScheduledStartDateTime >='5/18/2017'


	





	/*

USE [E2E_EAL_SecureAssess]

select
	est.StructureXML
	,est.StructureXML.value('data(assessmentDetails/qualificationName)[1]','nvarchar(100)') qualificationName
	,est.StructureXML.value('data(assessmentDetails/assessmentGroupName)[1]','nvarchar(1000)') assessmentGroupName
	,est.StructureXML.value('data(assessmentDetails/assessmentName)[1]','nvarchar(100)') assessmentName
	, ib3.QualificationName
	, se.examName
	, at.ExternalReference
from ExamSessionTable est
inner join ScheduledExamsTable se on est.ScheduledExamID=se.id
inner join CentreTable ct on se.CentreID=ct.ID
inner join IB3QualificationLookup ib3 on se.qualificationId = ib3.id 
INNER JOIN E2E_EAL_ItemBank.dbo.QualificationTable qt ON qt.id = ib3.ID
INNER JOIN E2E_EAL_ItemBank.dbo.AssessmentGroupTable agt ON agt.QualificationID = qt.id
	AND agt.Name = se.examName
INNER JOIN E2E_EAL_ItemBank.dbo.AssessmentTable at ON at.AssessmentGroupID = agt.ID

	AND at.ExternalReference = se.examVersionName
	--AND at.ExternalReference = se.examVersionVerionRef
	
--WHERE se.id = 679972
	WHERE est.StructureXML.value('data(assessmentDetails/qualificationName)[1]','nvarchar(100)') != ib3.qualificationname
			OR 
			est.StructureXML.value('data(assessmentDetails/assessmentGroupName)[1]','nvarchar(1000)') != se.examName
			OR
			est.StructureXML.value('data(assessmentDetails/assessmentName)[1]','nvarchar(100)') != at.ExternalReference



SELECT *
FROM E2E_EAL_ItemBank.dbo.AssessmentTable at
INNER JOIN E2E_EAL_ItemBank.dbo.AssessmentGroupTable agt ON agt.id = at.AssessmentGroupID
INNER JOIN E2E_EAL_ItemBank.dbo.QualificationTable qt ON qt.id = agt.QualificationID
WHERE agt.QualificationID = 583



*/