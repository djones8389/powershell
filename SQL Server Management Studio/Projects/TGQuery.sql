IF OBJECT_ID('tempdb..##Target') IS NOT NULL DROP TABLE ##Target;

SELECT  
	ROW_NUMBER() OVER(PARTITION BY SUBSTRING(Name, 0, CHARINDEX('_', Name)) ORDER BY SUBSTRING(Name, 0, CHARINDEX('_', Name)), create_date) R
	, Name as DBName
INTO ##Target
FROM SYS.Databases
where name like '%SADB%'
	and state_desc = 'ONLINE';


exec sp_msforeachdb '

use [?];

if (''?'' in (SELECT DBName 
			FROM ##Target 
			WHERE R = 1
			)
   )
Select  @@ServerName as ''ServerName''
		,''?'' as ''DatabaseName'' 	
	    ,  ID
		, Name
		, GlobalCentre
		, count(UserID) as NumUserCentreAssociationsWithOneQualLevelButNotTheOther	
FROM (
	SELECT A.ID
		, A.Name
		, A.UserID
		, case when A.CentreId = 1 then 1 else 0 end as GlobalCentre
		, case when A.hasquallevel1 <> A.hasquallevel0 then 1 else 0 end as hasOneLevelButNotTheOther
	FROM (
	  select 
			PermissionsTable.ID, 
			PermissionsTable.Name, 
			UserID, 
			CentreId, 
			CASE WHEN MAX(QualificationLevel) = 1 THEN 1 ELSE 0 END HasQualLevel1, 
			CASE WHEN MIN(QualificationLevel) = 0 THEN 1 ELSE 0 END HasQualLevel0
	  from PermissionsTable
	  left join RolePermissionsTable
	  on RolePermissionsTable.PermissionID = PermissionsTable.ID
	  inner join AssignedUserRolesTable
	  on AssignedUserRolesTable.RoleID = RolePermissionsTable.RoleID
	  where UsesQualLevel = 1
	  group by PermissionsTable.ID, PermissionsTable.Name, UserID, CentreId
	  )   A
  
) B

where hasOneLevelButNotTheOther = 1
group by ID, Name, GlobalCentre
order by ID

'