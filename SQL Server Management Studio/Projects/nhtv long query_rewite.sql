SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

USE NHTV_SurpassDataWarehouse

DECLARE @p__linq__0 datetime2(7) ='2017-06-09 00:00:00'
	,@p__linq__1 datetime2(7) ='2017-06-13 23:59:59'
	,@p__linq__2 int = 68

SELECT 
    [FactExamSessions].[ExamSessionKey] AS [ExamSessionKey], 
    --[FactExamSessions].[ExamSessionKey1] AS [ExamSessionKey1], 
    [OriginatorKey] AS [OriginatorKey], 
    [WarehouseTime] AS [WarehouseTime], 
    [CompletionDateKey] AS [CompletionDateKey], 
    [CompletionTime] AS [CompletionTime], 
    [KeyCode] AS [KeyCode], 
    [ExamKey] AS [ExamKey], 
    [QualificationKey] AS [QualificationKey], 
    [CentreKey] AS [CentreKey], 
    [CandidateKey] AS [CandidateKey], 
    [GradeKey] AS [GradeKey], 
    [Pass] AS [Pass], 
    [TimeTaken] AS [TimeTaken], 
    [Duration] AS [Duration], 
    [UserMarks] AS [UserMarks], 
    [TotalMarksAvailable] AS [TotalMarksAvailable], 
    [PassMark] AS [PassMark], 
    [Invigilated] AS [Invigilated], 
    [ClientMachineKey] AS [ClientMachineKey], 
    [FinalExamState] AS [FinalExamState], 
    [VoidReasonKey] AS [VoidReasonKey], 
    [VoidReason] AS [VoidReason], 
    [ExamVersionKey] AS [ExamVersionKey], 
    [IsMysteryShopper] AS [IsMysteryShopper], 
    [ExamStatus] AS [ExamStatus], 
    [AdjustedGradeKey] AS [AdjustedGradeKey], 
    [ScaleScoreKey] AS [ScaleScoreKey], 
    [ScaledScore] AS [ScaledScore], 
    [ExamVersionVersion] AS [ExamVersionVersion], 
    [startedDate] AS [startedDate], 
    [resultsReleased] AS [resultsReleased], 
    [resultsSampled] AS [resultsSampled], 
    [SubmittedDate] AS [SubmittedDate], 
    [ScaleScoreId] AS [ScaleScoreId], 
    [GradeBoundaryId] AS [GradeBoundaryId], 
    [ScoreBoundaryId] AS [ScoreBoundaryId], 
    [ExcludeFromReporting] AS [ExcludeFromReporting], 
    [ResultDataFull] AS [ResultDataFull], 
    [LOResults] AS [LOResults], 
    --[C2] AS [C1], 
    --[ExamSessionKey2] AS [ExamSessionKey2], 
    [CPID] AS [CPID], 
    [CPVersion] AS [CPVersion], 
    [Section] AS [Section], 
    [Mark] AS [Mark], 
    [ViewingTime] AS [ViewingTime], 
    [RawResponse] AS [RawResponse], 
    [DerivedResponse] AS [DerivedResponse], 
    [ShortDerivedResponse] AS [ShortDerivedResponse], 
    [Attempted] AS [Attempted], 
    [ItemPresentationOrder] AS [ItemPresentationOrder], 
    [Scored] AS [Scored], 
    --[CPID1] AS [CPID1], 
    --[CPVersion1] AS [CPVersion1], 
    [ItemXML] AS [ItemXML], 
    --[C1] AS [C2], 
    [Id] AS [Id], 
    --[ExamSessionKey3] AS [ExamSessionKey3], 
    --[CPID2] AS [CPID2], 
    [RescoringHistoryId] AS [RescoringHistoryId], 
    [FactExamSessionAuditId] AS [FactExamSessionAuditId], 
    --[Mark1] AS [Mark1], 
    [IsTemp] AS [IsTemp], 
    --[Scored1] AS [Scored1], 
    [IsOriginal] AS [IsOriginal]--, 
    --[RawResponse1] AS [RawResponse1]
FROM [dbo].[FactExamSessions] 
LEFT OUTER JOIN [dbo].[FactExamSessionXML]
ON [FactExamSessionXML].[ExamSessionKey] = [FactExamSessions].[ExamSessionKey]
LEFT OUTER JOIN  (
			SELECT [Extent3].[ExamSessionKey] AS [ExamSessionKey3]
							,[Extent3].[CPID] AS [CPID]
							,[Extent3].[CPVersion] AS [CPVersion]
							,[Extent3].[Section] AS [Section]
							,[Extent3].[Mark] AS [Mark]
							,[Extent3].[ViewingTime] AS [ViewingTime]
							,[Extent3].[RawResponse] AS [RawResponse]
							,[Extent3].[DerivedResponse] AS [DerivedResponse]
							,[Extent3].[ShortDerivedResponse] AS [ShortDerivedResponse]
							,[Extent3].[Attempted] AS [Attempted]
							,[Extent3].[ItemPresentationOrder] AS [ItemPresentationOrder]
							,[Extent3].[Scored] AS [Scored]
							--,[Extent4].[CPID] AS [CPID2]
							,[Extent4].[CPVersion] AS [CPVersion2]
							,[Extent4].[ItemXML] AS [ItemXML]
							,[Extent5].[Id] AS [Id]
							--,[Extent5].[ExamSessionKey] AS [ExamSessionKey4]
							--,[Extent5].[CPID] AS [CPID3]
							,[Extent5].[RescoringHistoryId] AS [RescoringHistoryId]
							,[Extent5].[FactExamSessionAuditId] AS [FactExamSessionAuditId]
							,[Extent5].[Mark] AS [Mark2]
							,[Extent5].[IsTemp] AS [IsTemp]
							--,[Extent5].[Scored] AS [Scored2]
							,[Extent5].[IsOriginal] AS [IsOriginal]
							--,[Extent5].[RawResponse] AS [RawResponse2]
			FROM [dbo].[FactQuestionResponses] AS [Extent3]
            INNER JOIN [dbo].[DimQuestionXML] AS [Extent4] 
			ON ([Extent3].[CPVersion] = [Extent4].[CPVersion]) AND ([Extent3].[CPID] = [Extent4].[CPID])
            LEFT OUTER JOIN [dbo].[FactQuestionResponseAudits] AS [Extent5] 
			ON ([Extent3].[CPID] = [Extent5].[CPID]) AND ([Extent3].[ExamSessionKey] = [Extent5].[ExamSessionKey]) 

			) AS [Join3] 
			ON [FactExamSessions].[ExamSessionKey] = [Join3].[ExamSessionKey3]

WHERE [FinalExamState] != 10 
	AND [FinalExamState] IS NOT NULL
	AND [ExcludeFromReporting] <> 1
	AND [SubmittedDate] >= @p__linq__0 
		AND [SubmittedDate] <= @p__linq__1 
		AND [ExamVersionKey] = @p__linq__2


	--ORDER BY [ExamSessionKey] DESC, [ExamSessionKey1] ASC, [C2] ASC, [ExamSessionKey2] ASC, [CPID] ASC, [CPID1] ASC, [CPVersion1] ASC, [C1] ASC
				
--CREATE NONCLUSTERED INDEX [IX_RescoreReport] ON dbo.FactExamSessions (
	
--	[FinalExamState]
--	,[ExcludeFromReporting]
--	,[SubmittedDate]
--	,[ExamVersionKey]
--)				