DECLARE @UpdateAndInsert TABLE (

	ExamSessionKey INT
	, FKIssue nvarchar(200)
);

INSERT @UpdateAndInsert
select ExamSessionKey
	, 	CASE 
		WHEN do.OriginatorKey IS  NULL  then 'ETL.stg_FactExamSessions.OriginatorKey-' + convert(nvarchar(100),sfes.OriginatorKey) 
		 WHEN de.ExamKey IS  NULL then 'ETL.stg_FactExamSessions.ExamKey-' + convert(nvarchar(100),sfes.ExamKey)  
		 WHEN dq.qualificationKey IS  NULL then 'ETL.stg_FactExamSessions.QualificationKey-' + convert(nvarchar(100),sfes.qualificationId)  
		 WHEN dc.CentreKey IS  NULL then 'ETL.stg_FactExamSessions.CentreKey-' + convert(nvarchar(100),sfes.CentreKey)  
		 WHEN des.ExamStateKey IS  NULL then 'ETL.stg_FactExamSessions.FinalExamState-' + convert(nvarchar(100),FinalExamState)  
		 WHEN dev.ExamVersionKey IS  NULL then 'ETL.stg_FactExamSessions.ExamVersionKey-' + convert(nvarchar(100),sfes.ExamVersionKey)
		 WHEN dca.CandidateKey IS  NULL then 'ETL.stg_FactExamSessions.CandidateKey-' + convert(nvarchar(100),sfes.CandidateKey) 
    END AS 'INSERT Statement'
FROM ETL.stg_FactExamSessions sfes
           LEFT JOIN dbo.DimOriginators do ON do.OriginatorKey = sfes.OriginatorKey
           LEFT JOIN dbo.DimExams de ON de.ExamKey = sfes.ExamKey
           LEFT JOIN dbo.DimQualifications dq ON dq.QualificationKey = sfes.qualificationId
           LEFT JOIN dbo.DimCentres dc ON dc.CentreKey = sfes.CentreKey
           LEFT JOIN dbo.DimExamState des ON des.ExamStateKey = sfes.FinalExamState
           LEFT JOIN dbo.DimExamVersions dev ON dev.ExamVersionKey = sfes.ExamVersionKey
           LEFT JOIN dbo.DimCandidate dca ON dca.CandidateKey = sfes.CandidateKey
WHERE  sfes.UseForMerge = 1
	 AND (
			  do.OriginatorKey IS NULL
			   OR de.ExamKey IS NULL
			   OR dq.QualificationKey IS NULL
			   OR dc.CentreKey IS NULL
			   OR des.ExamStateKey IS NULL
			   OR dev.ExamVersionKey IS NULL
			   OR dca.CandidateKey IS NULL
		  );

UPDATE ETL.stg_FactExamSessions
set UseForMerge = 0
FROM ETL.stg_FactExamSessions A
INNER JOIN @UpdateAndInsert B
on A.ExamSessionKey = B.ExamSessionKey;

INSERT ETL.FKError (FKColumn, FKValue)
SELECT SUBSTRING(A.FKIssue, 0, CHARINDEX('-',A.FKIssue))
	, SUBSTRING(A.FKIssue, CHARINDEX('-',A.FKIssue)+1, LEN(A.FKIssue))
FROM (

SELECT 
	FKIssue
FROM @UpdateAndInsert

) A;

	

