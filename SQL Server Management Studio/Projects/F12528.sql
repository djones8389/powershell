SELECT CASE WHEN EST.examState = 13 THEN 'Warehoused'
	ELSE 'Live'
	END AS 'Location'
	, CT.CentreName
	, IB.QualificationName
	, SCET.examName
	, UT.CandidateRef
	, ut.forename + ' ' + UT.Surname [CandidateName]
	, EST.examState
	, CONVERT(VARCHAR,ut.DOB,103) DOB
	, CONVERT(VARCHAR,scet.ScheduledStartDateTime,103) ScheduledStartDate
	, CONVERT(VARCHAR,scet.ScheduledEndDateTime,103) ScheduledEndDate
	, CONVERT(VARCHAR,scet.CreatedDateTime,103) CreatedDate
	, ut2.Forename + ' ' + ut2.Surname [Createdby]
  FROM ExamSessionTable as EST (NOLOCK)

  INNER JOIN ScheduledExamsTable as SCET (NOLOCK)  on SCET.ID = EST.ScheduledExamID   
  INNER JOIN UserTable as UT (NOLOCK)  on UT.ID = EST.UserID
  INNER JOIN CentreTable as CT (NOLOCK)  on CT.ID = SCET.CentreID
  INNER JOIN IB3QualificationLookup as IB (NOLOCK) on IB.ID = scet.qualificationId
  INNER JOIN dbo.UserTable UT2 ON ut2.ID = SCET.CreatedBy

