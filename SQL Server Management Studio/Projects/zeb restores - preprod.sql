select  [client]
	, max(r) r
from (
select destination_database_name
	, restore_date
	, SUBSTRING(destination_database_name, 0, charindex('_',destination_database_name)) [client]
	, ROW_NUMBER() OVER (Partition by SUBSTRING(destination_database_name, 0, charindex('_',destination_database_name)) order by SUBSTRING(destination_database_name, 0, charindex('_',destination_database_name))) r
from msdb.dbo.restorehistory
where restore_date > '2018-04-16'
--order by 1
) a
--where  r <> 6
group by  [client]
order by 2


--sp_whoisactive