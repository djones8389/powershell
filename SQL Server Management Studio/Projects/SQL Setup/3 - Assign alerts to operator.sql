USE [msdb]
GO
EXEC msdb.dbo.sp_update_operator @name=N'DBAdmin', 
		@enabled=1, 
		@pager_days=0, 
		@email_address=N'DBAdmin@btl.com', 
		@pager_address=N'', 
		@netsend_address=N''
GO
EXEC msdb.dbo.sp_add_notification @alert_name=N'Notify DBA on Database Backup', @operator_name=N'DBAdmin', @notification_method = 1
GO
EXEC msdb.dbo.sp_add_notification @alert_name=N'Notify DBA on Database Change', @operator_name=N'DBAdmin', @notification_method = 1
GO
EXEC msdb.dbo.sp_add_notification @alert_name=N'Notify DBA on Database Startup', @operator_name=N'DBAdmin', @notification_method = 1
GO
EXEC msdb.dbo.sp_add_notification @alert_name=N'Notify DBA on Deadlock', @operator_name=N'DBAdmin', @notification_method = 1
GO
EXEC msdb.dbo.sp_add_notification @alert_name=N'Notify DBA on Error 823', @operator_name=N'DBAdmin', @notification_method = 1
GO
EXEC msdb.dbo.sp_add_notification @alert_name=N'Notify DBA on Error 824', @operator_name=N'DBAdmin', @notification_method = 1
GO
EXEC msdb.dbo.sp_add_notification @alert_name=N'Notify DBA on Error 825', @operator_name=N'DBAdmin', @notification_method = 1
GO
EXEC msdb.dbo.sp_add_notification @alert_name=N'Notify DBA on Fatal Error', @operator_name=N'DBAdmin', @notification_method = 1
GO
EXEC msdb.dbo.sp_add_notification @alert_name=N'Notify DBA on Fatal Error in Current Process', @operator_name=N'DBAdmin', @notification_method = 1
GO
EXEC msdb.dbo.sp_add_notification @alert_name=N'Notify DBA on Fatal Error in Database Process', @operator_name=N'DBAdmin', @notification_method = 1
GO
EXEC msdb.dbo.sp_add_notification @alert_name=N'Notify DBA on Fatal Error in Resource', @operator_name=N'DBAdmin', @notification_method = 1
GO
EXEC msdb.dbo.sp_add_notification @alert_name=N'Notify DBA on Fatal Error: Database Integrity Suspect', @operator_name=N'DBAdmin', @notification_method = 1
GO
EXEC msdb.dbo.sp_add_notification @alert_name=N'Notify DBA on Fatal Error: Hardware Error', @operator_name=N'DBAdmin', @notification_method = 1
GO
EXEC msdb.dbo.sp_add_notification @alert_name=N'Notify DBA on Fatal Error: Table Integrity Suspect', @operator_name=N'DBAdmin', @notification_method = 1
GO
EXEC msdb.dbo.sp_add_notification @alert_name=N'Notify DBA on Insufficent Resources', @operator_name=N'DBAdmin', @notification_method = 1
GO
EXEC msdb.dbo.sp_add_notification @alert_name=N'Notify DBA on Nonfatal Internal Error', @operator_name=N'DBAdmin', @notification_method = 1
GO
