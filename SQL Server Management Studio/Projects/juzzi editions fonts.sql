use master

IF OBJECT_ID('tempdb..#Fonts') IS NOT NULL DROP TABLE #Fonts;

CREATE TABLE #Fonts (
	client sysname
	, SubjectName nvarchar(1000)
	, FontFamily tinyint
	, FontSize tinyint
);

DECLARE @dynamic nvarchar(MAX)= '';

select @dynamic +=CHAR(13)+ '

	use '+QUOTENAME([NAME])+'

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

		if (
			SELECT count(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME =''Subjects'' AND COLUMN_NAME IN (''FontFamily'',''FontSize'')
		) = 2
	 
	 BEGIN
			INSERT #Fonts
			select  db_name() Client
				, Title
				, FontFamily
				, FontSize
			from [dbo].[Subjects]
			where FontSize != 16
				and FontFamily != 0
	 END

	'
from sys.databases
where state_desc = 'ONLINE'
	and name like '%[_]ContentAuthor'
	and name != 'Nbme_ContentAuthor';

exec(@dynamic);

select *
from #Fonts
order by 1;

drop table #Fonts;

/*

select 'USE ' + QUOTENAME(NAME) + ' SELECT db_name(), count(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME =''Subjects'' AND COLUMN_NAME IN (''FontFamily'',''FontSize'')'
from sys.databases
where state_desc = 'ONLINE'
	and name like '%[_]ContentAuthor';


USE [Abcawards_ContentAuthor] SELECT db_name(), count(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME ='Subjects' AND COLUMN_NAME IN ('FontFamily','FontSize')
USE [Academic_ContentAuthor] SELECT db_name(), count(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME ='Subjects' AND COLUMN_NAME IN ('FontFamily','FontSize')
USE [Acceptancewindesheim_ContentAuthor] SELECT db_name(), count(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME ='Subjects' AND COLUMN_NAME IN ('FontFamily','FontSize')
USE [Acumina_ContentAuthor] SELECT db_name(), count(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME ='Subjects' AND COLUMN_NAME IN ('FontFamily','FontSize')
USE [Aqa_ContentAuthor] SELECT db_name(), count(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME ='Subjects' AND COLUMN_NAME IN ('FontFamily','FontSize')
USE [Bpec_ContentAuthor] SELECT db_name(), count(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME ='Subjects' AND COLUMN_NAME IN ('FontFamily','FontSize')
USE [Cache_ContentAuthor] SELECT db_name(), count(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME ='Subjects' AND COLUMN_NAME IN ('FontFamily','FontSize')
USE [Candgdemo_ContentAuthor] SELECT db_name(), count(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME ='Subjects' AND COLUMN_NAME IN ('FontFamily','FontSize')
USE [Certify_ContentAuthor] SELECT db_name(), count(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME ='Subjects' AND COLUMN_NAME IN ('FontFamily','FontSize')
USE [Cilex_ContentAuthor] SELECT db_name(), count(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME ='Subjects' AND COLUMN_NAME IN ('FontFamily','FontSize')
USE [Cityandguilds_ContentAuthor] SELECT db_name(), count(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME ='Subjects' AND COLUMN_NAME IN ('FontFamily','FontSize')
USE [Clonetest_ContentAuthor] SELECT db_name(), count(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME ='Subjects' AND COLUMN_NAME IN ('FontFamily','FontSize')
USE [CompexJTLtd_ContentAuthor] SELECT db_name(), count(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME ='Subjects' AND COLUMN_NAME IN ('FontFamily','FontSize')
USE [Connect_ContentAuthor] SELECT db_name(), count(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME ='Subjects' AND COLUMN_NAME IN ('FontFamily','FontSize')
USE [Creatio_ContentAuthor] SELECT db_name(), count(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME ='Subjects' AND COLUMN_NAME IN ('FontFamily','FontSize')
USE [Demo_ContentAuthor] SELECT db_name(), count(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME ='Subjects' AND COLUMN_NAME IN ('FontFamily','FontSize')
USE [Eal_ContentAuthor] SELECT db_name(), count(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME ='Subjects' AND COLUMN_NAME IN ('FontFamily','FontSize')
USE [EBS_ContentAuthor] SELECT db_name(), count(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME ='Subjects' AND COLUMN_NAME IN ('FontFamily','FontSize')
USE [Ebstest_ContentAuthor] SELECT db_name(), count(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME ='Subjects' AND COLUMN_NAME IN ('FontFamily','FontSize')
USE [Edutech_ContentAuthor] SELECT db_name(), count(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME ='Subjects' AND COLUMN_NAME IN ('FontFamily','FontSize')
USE [Fontys_ContentAuthor] SELECT db_name(), count(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME ='Subjects' AND COLUMN_NAME IN ('FontFamily','FontSize')
USE [Habc_ContentAuthor] SELECT db_name(), count(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME ='Subjects' AND COLUMN_NAME IN ('FontFamily','FontSize')
USE [Hw_ContentAuthor] SELECT db_name(), count(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME ='Subjects' AND COLUMN_NAME IN ('FontFamily','FontSize')
USE [ICAEW_ContentAuthor] SELECT db_name(), count(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME ='Subjects' AND COLUMN_NAME IN ('FontFamily','FontSize')
USE [Icaewmock_ContentAuthor] SELECT db_name(), count(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME ='Subjects' AND COLUMN_NAME IN ('FontFamily','FontSize')
USE [Icas_ContentAuthor] SELECT db_name(), count(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME ='Subjects' AND COLUMN_NAME IN ('FontFamily','FontSize')
USE [Icasmock_ContentAuthor] SELECT db_name(), count(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME ='Subjects' AND COLUMN_NAME IN ('FontFamily','FontSize')
USE [Itts_ContentAuthor] SELECT db_name(), count(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME ='Subjects' AND COLUMN_NAME IN ('FontFamily','FontSize')
USE [Nbme_ContentAuthor] SELECT db_name(), count(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME ='Subjects' AND COLUMN_NAME IN ('FontFamily','FontSize')
USE [Nebosh_ContentAuthor] SELECT db_name(), count(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME ='Subjects' AND COLUMN_NAME IN ('FontFamily','FontSize')
USE [NHTV_ContentAuthor] SELECT db_name(), count(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME ='Subjects' AND COLUMN_NAME IN ('FontFamily','FontSize')
USE [Ocr_ContentAuthor] SELECT db_name(), count(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME ='Subjects' AND COLUMN_NAME IN ('FontFamily','FontSize')
USE [Opengroup_ContentAuthor] SELECT db_name(), count(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME ='Subjects' AND COLUMN_NAME IN ('FontFamily','FontSize')
USE [Professional_ContentAuthor] SELECT db_name(), count(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME ='Subjects' AND COLUMN_NAME IN ('FontFamily','FontSize')
USE [Prometric_ContentAuthor] SELECT db_name(), count(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME ='Subjects' AND COLUMN_NAME IN ('FontFamily','FontSize')
USE [Rcpch_ContentAuthor] SELECT db_name(), count(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME ='Subjects' AND COLUMN_NAME IN ('FontFamily','FontSize')
USE [Rgu_ContentAuthor] SELECT db_name(), count(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME ='Subjects' AND COLUMN_NAME IN ('FontFamily','FontSize')
USE [Rm_ContentAuthor] SELECT db_name(), count(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME ='Subjects' AND COLUMN_NAME IN ('FontFamily','FontSize')
USE [Saxion_ContentAuthor] SELECT db_name(), count(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME ='Subjects' AND COLUMN_NAME IN ('FontFamily','FontSize')
USE [Sharedsandbox_ContentAuthor] SELECT db_name(), count(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME ='Subjects' AND COLUMN_NAME IN ('FontFamily','FontSize')
USE [Sqa_ContentAuthor] SELECT db_name(), count(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME ='Subjects' AND COLUMN_NAME IN ('FontFamily','FontSize')
USE [TCNTechnicalAudit_ContentAuthor] SELECT db_name(), count(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME ='Subjects' AND COLUMN_NAME IN ('FontFamily','FontSize')
USE [Telc_ContentAuthor] SELECT db_name(), count(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME ='Subjects' AND COLUMN_NAME IN ('FontFamily','FontSize')
USE [Templateempty_ContentAuthor] SELECT db_name(), count(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME ='Subjects' AND COLUMN_NAME IN ('FontFamily','FontSize')
USE [Training_ContentAuthor] SELECT db_name(), count(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME ='Subjects' AND COLUMN_NAME IN ('FontFamily','FontSize')
USE [Trinitycollege_ContentAuthor] SELECT db_name(), count(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME ='Subjects' AND COLUMN_NAME IN ('FontFamily','FontSize')
USE [Twi_ContentAuthor] SELECT db_name(), count(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME ='Subjects' AND COLUMN_NAME IN ('FontFamily','FontSize')
USE [Utwente_ContentAuthor] SELECT db_name(), count(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME ='Subjects' AND COLUMN_NAME IN ('FontFamily','FontSize')
USE [Windesheim_ContentAuthor] SELECT db_name(), count(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME ='Subjects' AND COLUMN_NAME IN ('FontFamily','FontSize')

*/