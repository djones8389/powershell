USE [master]



RESTORE DATABASE [AdventureWorks2016] 
FROM  DISK = N'E:\Backup\AdventureWorks2016CTP3.bak' WITH  FILE = 1
,  MOVE N'AdventureWorks2016CTP3_Data' TO N'E:\Data\AdventureWorks2016CTP3_Data.mdf'
,  MOVE N'AdventureWorks2016CTP3_Log' TO N'E:\Log\AdventureWorks2016CTP3_Log.ldf'
,  MOVE N'AdventureWorks2016CTP3_mod' TO N'E:\Data\AdventureWorks2016CTP3_mod'
,  NOUNLOAD,  REPLACE,  STATS = 5

GO

