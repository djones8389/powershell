USE AdventureWorks2016

/*

Temporal tables

--https://msdn.microsoft.com/en-us/library/dn935015.aspx

New to SQL 2016: Available in Standard, Enterprise, Web and Express

When configured, this can tell you what your data was, at given a point in time - like an Audit trail.
You configure this on a table-by-table basis
For each table you configure, you'll have your actual table,  and a corresponding 'history' table
You can tell where this has been applied by the (System-Versioned) suffix and icon change in Solution Explorer

*/

/***********Create temporal table*********/

if exists (select 1 from sys.tables where name = 'Employee' and SCHEMA_NAME(schema_id)='dbo')
	BEGIN
		ALTER TABLE dbo.Employee SET (SYSTEM_VERSIONING = OFF);
		DROP TABLE dbo.Employee;  
		DROP TABLE dbo.EmployeeHistory;  
	END
	 
CREATE TABLE dbo.Employee   
(    
  [EmployeeID] int NOT NULL PRIMARY KEY CLUSTERED			--Must have a PK
  , [Name] nvarchar(100) NOT NULL  
  , [Position] varchar(100) NOT NULL   
  , [Department] varchar(100) NOT NULL  
  , [Address] nvarchar(1024) NOT NULL 
  , testXML xml null 
  , [AnnualSalary] decimal (10,2) NOT NULL  
  , [ValidFrom] datetime2 (2) GENERATED ALWAYS AS ROW START  --Mandatory
  , [ValidTo] datetime2 (2) GENERATED ALWAYS AS ROW END      --Mandatory
  , PERIOD FOR SYSTEM_TIME (ValidFrom, ValidTo)				 --Mandatory
 )    
 WITH (SYSTEM_VERSIONING = ON (HISTORY_TABLE = dbo.EmployeeHistory, DATA_CONSISTENCY_CHECK = ON) );  
 --WITH (SYSTEM_VERSIONING = ON)   ** Not specifying a TableName means SQL auto-generates a messy name for you, prefixed MSSQL_TemporalHistoryFor_[TableName]_GUID **
 

INSERT Employee(EmployeeID, [Name], Position, Department, [Address], testXML, AnnualSalary)--, ValidFrom, ValidTo
VALUES(1,'DaveJ','Tester','Testing','123 fake street', '<tag>Text</tag>','1000.00')

--Notice the ValidFrom and ValidTo dates have updated automatically
--ValidFrom is the start time of the INSERT Transaction
--ValidTo is set like this because this row is marked as "Open"

Select * from Employee	for system_time	ALL	WHERE EmployeeID = 1

--Nothing in here at the moment, because nothing has changed	
	
Select * from EmployeeHistory		

	UPDATE Employee
	set Position = 'Head Tester'
		, testXML = '<tag><tag1>Text</tag1></tag>'
	where EmployeeID = 1;

--Now that the data has changed, we have an entry
--ValidFrom is the original INSERT Date
--ValidTo is the UPDATE time,  which we just actioned

--Select * from EmployeeHistory	
	
DELETE from Employee where EmployeeID = 1;

--As you'd expect, my deleted records appears, as it was

Select *
from EmployeeHistory	
where EmployeeID = 1;

/**********Analysis**********/

SELECT * FROM Employee   
FOR SYSTEM_TIME 
AS OF '2016-11-16 07:27:36.86'
--BETWEEN '' and ''
--FROM '' and ''
--CONTAINED IN ('','')

SELECT * FROM Employee   
    FOR SYSTEM_TIME ALL 
	WHERE EmployeeID = 1 ORDER BY ValidFrom;  


/*********To apply to existing tables**********/


ALTER TABLE Person.[Address]
   ADD SysStartTime datetime2(0) GENERATED ALWAYS AS ROW START HIDDEN    
           CONSTRAINT DF_SysStart DEFAULT SYSUTCDATETIME()  
      , SysEndTime datetime2(0) GENERATED ALWAYS AS ROW END HIDDEN    
           CONSTRAINT DF_SysEnd DEFAULT CONVERT(datetime2 (0), '9999-12-31 23:59:59')
	  , PERIOD FOR SYSTEM_TIME (SysStartTime, SysEndTime);   
GO   
ALTER TABLE Person.[Address]    
   SET (SYSTEM_VERSIONING = ON (HISTORY_TABLE = Person.AddressHistory))   
;  


/**********Further Info in Word Doc**********/

*/



