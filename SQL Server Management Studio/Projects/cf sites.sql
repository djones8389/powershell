/****** Script for SelectTopNRows command from SSMS  ******/
SELECT uc.*
	,	 ca.*
FROM [Demo_SurpassManagement].[dbo].[Users] u
inner join UserCentreSubjectRoles uc
on uc.userid = u.id
inner join roles r
on r.id = uc.roleid
inner join rolepermissions rp
on rp.roleid = r.id
inner join [permissions] p
on p.id = rp.PermissionId
left join demo_contentauthor..users ca
on ca.id = itemauthoringuserid

where u.username in ('admin_test', 'subject_test')

--itemauthoring

  select *
  from UserCentreSubjectRoles
  where userid = 483