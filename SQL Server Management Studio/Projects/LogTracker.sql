USE [AdventureWorks2008R2]
GO

--CREATE TABLE [dbo].[LogGrowth](
--	[DatabaseName] [sysname] NOT NULL,
--	[logSpace] [real] NULL,
--	[logSpaceUsed] [real] NULL,
--	[status] [bit] NULL,
--	[timeStamp] [datetime] NULL
--)

DECLARE @Statement nvarchar(MAX) = 'DBCC SQLPERF(LogSpace)'
INSERT [LogGrowth]([DatabaseName], [logSpace], [logSpaceUsed], [status])
exec sp_executesql @Statement;

UPDATE [LogGrowth]
set timeStamp = getDATE()
where timeStamp IS NULL;

select *
from [LogGrowth]
where [DatabaseName] = 'AdventureWorks2008R2'


ALTER TABLE [dbo].[JBMTest]
ADD [DJTest3] nvarchar(20)

UPDATE [dbo].[JBMTest]
set [DJTest3] = '1234567890'