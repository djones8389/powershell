use STG_NCFE_SecureAssess

select userid, id, examState,KeyCode, StructureXML
from ExamSessionTable
where keycode = 'QG6QRLA8'

SELECT *
FROM [dbo].[UserExamHistoryVersionTrackerTable]
where userid in (
	select userid
	from ExamSessionTable
	where keycode = 'QG6QRLA8'
		
)
and examSessionID = 359847

SELECT *
FROM [dbo].UserExamHistoryCompletionDateTrackerTable
where userid in (
	select userid	
	from ExamSessionTable
	where keycode = 'QG6QRLA8'
)

--1605149	37251	153	588	1605149


SELECT distinct so.name
FROM sys.sysobjects so
inner join sys.syscomments sc
on sc.id =so.id
where sc.text like '%UserExamHistoryVersionTrackerTable%'