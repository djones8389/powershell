SET NOCOUNT ON;

IF EXISTS (
SELECT 1 from sys.configurations where Name = 'xp_cmdshell' and value_in_use = 0
)
	PRINT 'You need to enable CMDShell, or copy files manually!!'

IF (SELECT CONVERT(SYSNAME, SERVERPROPERTY('instancedefaultdatapath'))) IS NULL
	PRINT 'Default Data Path isn''t defined, set this in the variable manually'
	
IF (SELECT CONVERT(SYSNAME, SERVERPROPERTY('instancedefaultlogpath'))) IS NULL
	PRINT 'Default Log Path isn''t defined, set this in the variable manually'

DECLARE @SQL NVARCHAR(MAX) = ''
	  , @OnlineOffline NVARCHAR(MAX) = '';

DECLARE @DefaultDataLoc NVARCHAR(1000) = CONVERT(SYSNAME, SERVERPROPERTY('instancedefaultdatapath'))
	  ,  @DefaultLogLoc NVARCHAR(1000) = CONVERT(SYSNAME, SERVERPROPERTY('instancedefaultlogpath'));

SELECT @DefaultDataLoc = 'E:\SQL2012MasterDBs\'
SELECT @DefaultLogLoc = 'E:\SQL2012MasterDBs\'


DECLARE @Tables TABLE(databaseName nvarchar(100), [LogicalName] nvarchar(250), [CurrentLocation] nvarchar(1000), [NewLocation] nvarchar(1000))
INSERT @Tables
SELECT D.[Name] [DatabaseName]
	  , F.Name  [LogicalName]
	  , F.physical_name [CurrentLocation]
	  ,  case f.type_desc when 'ROWS' then   @DefaultDataLoc + SUBSTRING(F.physical_name,LEN(REVERSE(SUBSTRING(REVERSE(F.physical_name), CHARINDEX('\',REVERSE(F.physical_name)), LEN(F.physical_name))))+1,LEN(F.physical_name))
					 when 'LOG' then   @DefaultLogLoc + SUBSTRING(F.physical_name,LEN(REVERSE(SUBSTRING(REVERSE(F.physical_name), CHARINDEX('\',REVERSE(F.physical_name)), LEN(F.physical_name))))+1,LEN(F.physical_name))
	END as [NewLocation]
FROM sys.master_files as F
inner join (
	SELECT database_id, [Name]
	FROM sys.databases
	) as D
on D.database_id = F.database_id 
--where D.[Name] in  ('Bpec_ContentAuthor','Bpec_ItemBank','Bpec_SecureAssess','Bpec_SurpassManagement','RCPCH_AnalyticsManagement','RCPCH_ContentAuthor','SAXION_ItemBank','SAXION_SecureAssess');

--DECLARE myCursor CURSOR FOR
Select [DatabaseName]
	, [LogicalName]
	, [CurrentLocation]
	, [NewLocation]
	, case 
		when RIGHT([CurrentLocation],3)  in ('mdf','ndf')
		then  'ALTER DATABASE ' + QUOTENAME (databaseName) + ' SET OFFLINE; ALTER DATABASE ' + QUOTENAME (databaseName) + ' MODIFY FILE (NAME = ''' + [LogicalName] + ''',' + 'Filename = ''' + NewLocation + ''');'
				+ ' EXECUTE xp_cmdshell ''' + 'move /Y ' + CurrentLocation + '  ' +  NewLocation + ''''
	    when RIGHT([CurrentLocation],3)  in ('ldf')
				then 'ALTER DATABASE ' + QUOTENAME (databaseName) + ' MODIFY FILE (NAME = ''' + [LogicalName] + ''',' + 'Filename = ''' + NewLocation + ''');'
				+ ' EXECUTE xp_cmdshell ''' + 'move /Y ' + CurrentLocation + '  ' +  NewLocation + '''' +  'ALTER DATABASE ' + QUOTENAME (databaseName) + ' SET ONLINE;'
		end as [statement]
from @Tables
order by [DatabaseName], RIGHT([CurrentLocation],3) desc;


--select *
--from sys.configurations
--where name = 'xp_cmdshell'
--EXEC sp_configure 'show advanced options', 1; 
--reconfigure;
--exec sp_configure 'xp_cmdshell',1;
--reconfigure;

ALTER DATABASE [model] MODIFY FILE (NAME = 'modeldev',Filename = 'E:\SQL2012MasterDBs\model.mdf'); EXECUTE xp_cmdshell 'move /Y D:\Microsoft SQL Server2012\MSSQL11.DAVEJSQL2012\MSSQL\DATA\model.mdf  E:\SQL2012MasterDBs\model.mdf'
ALTER DATABASE [model] MODIFY FILE (NAME = 'modellog',Filename = 'E:\SQL2012MasterDBs\modellog.ldf'); EXECUTE xp_cmdshell 'move /Y D:\Microsoft SQL Server2012\MSSQL11.DAVEJSQL2012\MSSQL\DATA\modellog.ldf  E:\SQL2012MasterDBs\modellog.ldf'
ALTER DATABASE [msdb] MODIFY FILE (NAME = 'MSDBData',Filename = 'E:\SQL2012MasterDBs\MSDBData.mdf'); EXECUTE xp_cmdshell 'move /Y D:\Microsoft SQL Server2012\MSSQL11.DAVEJSQL2012\MSSQL\DATA\MSDBData.mdf  E:\SQL2012MasterDBs\MSDBData.mdf'
ALTER DATABASE [msdb] MODIFY FILE (NAME = 'MSDBLog',Filename = 'E:\SQL2012MasterDBs\MSDBLog.ldf'); EXECUTE xp_cmdshell 'move /Y D:\Microsoft SQL Server2012\MSSQL11.DAVEJSQL2012\MSSQL\DATA\MSDBLog.ldf  E:\SQL2012MasterDBs\MSDBLog.ldf'
ALTER DATABASE [ReportServer] SET OFFLINE; ALTER DATABASE [ReportServer] MODIFY FILE (NAME = 'ReportServer',Filename = 'E:\SQL2012MasterDBs\ReportServer.mdf'); EXECUTE xp_cmdshell 'move /Y D:\Microsoft SQL Server2012\MSSQL11.DAVEJSQL2012\MSSQL\DATA\ReportServer.mdf  E:\SQL2012MasterDBs\ReportServer.mdf'
ALTER DATABASE [ReportServer] MODIFY FILE (NAME = 'ReportServer_log',Filename = 'E:\SQL2012MasterDBs\ReportServer_log.ldf'); EXECUTE xp_cmdshell 'move /Y D:\Microsoft SQL Server2012\MSSQL11.DAVEJSQL2012\MSSQL\DATA\ReportServer_log.ldf  E:\SQL2012MasterDBs\ReportServer_log.ldf'ALTER DATABASE [ReportServer] SET ONLINE;
ALTER DATABASE [ReportServerTempDB] SET OFFLINE; ALTER DATABASE [ReportServerTempDB] MODIFY FILE (NAME = 'ReportServerTempDB',Filename = 'E:\SQL2012MasterDBs\ReportServerTempDB.mdf'); EXECUTE xp_cmdshell 'move /Y D:\Microsoft SQL Server2012\MSSQL11.DAVEJSQL2012\MSSQL\DATA\ReportServerTempDB.mdf  E:\SQL2012MasterDBs\ReportServerTempDB.mdf'
ALTER DATABASE [ReportServerTempDB] MODIFY FILE (NAME = 'ReportServerTempDB_log',Filename = 'E:\SQL2012MasterDBs\ReportServerTempDB_log.ldf'); EXECUTE xp_cmdshell 'move /Y D:\Microsoft SQL Server2012\MSSQL11.DAVEJSQL2012\MSSQL\DATA\ReportServerTempDB_log.ldf  E:\SQL2012MasterDBs\ReportServerTempDB_log.ldf'ALTER DATABASE [ReportServerTempDB] SET ONLINE;
ALTER DATABASE [tempdb] MODIFY FILE (NAME = 'tempdev',Filename = 'E:\SQL2012MasterDBs\tempdb.mdf'); EXECUTE xp_cmdshell 'move /Y D:\Microsoft SQL Server2012\MSSQL11.DAVEJSQL2012\MSSQL\DATA\tempdb.mdf  E:\SQL2012MasterDBs\tempdb.mdf'
ALTER DATABASE [tempdb] MODIFY FILE (NAME = 'templog',Filename = 'E:\SQL2012MasterDBs\templog.ldf'); EXECUTE xp_cmdshell 'move /Y D:\Microsoft SQL Server2012\MSSQL11.DAVEJSQL2012\MSSQL\DATA\templog.ldf  E:\SQL2012MasterDBs\templog.ldf'