		SELECT	WAREHOUSE_ExamSessionTable_Shreded.examsessionid
			, WAREHOUSE_ExamSessionTable_Shreded.previousExamState 
			,WAREHOUSE_ExamSessionTable_Shreded.passValue
			, WAREHOUSE_ExamSessionTable_Shreded.WarehouseExamState
			, WAREHOUSE_ExamSessionTable.WarehouseExamState
		FROM
			WAREHOUSE_ExamSessionTable_Shreded
		LEFT OUTER JOIN
			WAREHOUSE_ExamSessionTable on WAREHOUSE_ExamSessionTable.id = WAREHOUSE_ExamSessionTable_Shreded.examsessionid
		INNER JOIN WAREHOUSE_ScheduledExamsTable
			ON WAREHOUSE_ExamSessionTable.WAREHOUSEScheduledExamID = WAREHOUSE_ScheduledExamsTable.ID
		INNER JOIN WAREHOUSE_CentreTable
			ON WAREHOUSE_ScheduledExamsTable.WAREHOUSECentreID = WAREHOUSE_CentreTable.ID
		INNER JOIN WAREHOUSE_UserTable AS [Candidate]
			ON [Candidate].Id = WAREHOUSE_ExamSessionTable.WAREHOUSEUserId
		LEFT OUTER JOIN
			WAREHOUSE_UserTable AS WAREHOUSE_Users_Moderator ON WAREHOUSE_ExamSessionTable.AssignedModeratorUserID = WAREHOUSE_Users_Moderator.ID 
		LEFT OUTER JOIN
			WAREHOUSE_UserTable AS WAREHOUSE_Users_Marker ON WAREHOUSE_ExamSessionTable.AssignedMarkerUserID = WAREHOUSE_Users_Marker.ID
		LEFT JOIN dbo.voidJustificationLookupTable VoidTable ON VoidTable.ID =  voidJustificationLookupTableId
 WHERE  WAREHOUSE_ExamSessionTable.KeyCode LIKE '%3MWRG35S%'AND (WAREHOUSE_ExamSessionTable_Shreded.originatorId = '1') AND WAREHOUSE_ExamSessionTable_Shreded.qualificationLevel IN (103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121) 	
	AND WAREHOUSE_ExamSessionTable.reMarkStatus IN (0,1,2)          
			AND WAREHOUSE_ExamSessionTable_Shreded.WarehouseExamState IN (1,2,3,4,5,6,99)                    
			AND (
					(
						(1 =  1) 
						AND	(
								WAREHOUSE_ExamSessionTable_Shreded.previousExamState = 12 
								OR (
										WAREHOUSE_ExamSessionTable_Shreded.previousExamState = 9 
										AND (
												WAREHOUSE_ExamSessionTable.ExportToSecureMarker = 1 OR WAREHOUSE_ExamSessionTable_Shreded.WarehouseExamState = 6
											)
									)
							) 
					)                          
				)          

begin tran

	begin 

	UPDATE WAREHOUSE_ExamSessionTable
	SET PreviousExamState = 12
	OUTPUT inserted.ID, deleted.WarehouseExamState, getDATE()
		INTO BTL_ExamStateUpdate
	WHERE PreviousExamState = 9
		and ExportToSecureMarker = 0

	end

	begin

	UPDATE WAREHOUSE_ExamSessionTable_Shreded
	SET PreviousExamState = 12
	OUTPUT inserted.examSessionId, deleted.WarehouseExamState, getDATE()
		INTO BTL_ExamStateUpdate
	FROM WAREHOUSE_ExamSessionTable_Shreded a
	inner join WAREHOUSE_ExamSessionTable b
	on a.examSessionId = b.ID

	WHERE a.PreviousExamState = 9
		and b.ExportToSecureMarker = 0

	end


rollback
			