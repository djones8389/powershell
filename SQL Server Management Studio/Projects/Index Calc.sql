SELECT  
	[TableName]
	,[index_size-kb] = cast(LTRIM (STR ((CASE WHEN usedpages > pages THEN (usedpages - pages) ELSE 0 END) * 8, 15, 0)) as int)
	, name [index name]	
FROM (

	SELECT 
		OBJECT_NAME(s.object_id) [TableName]
		, (used_page_count) [usedpages]
		,  (
			CASE
				WHEN (s.index_id < 2) THEN (in_row_data_page_count + lob_used_page_count + row_overflow_used_page_count)
				ELSE lob_used_page_count + row_overflow_used_page_count
			END
		) [pages]
		, i.name
	FROM sys.dm_db_partition_stats AS s
		INNER JOIN sys.indexes AS i
			ON s.[object_id] = i.[object_id]
			AND s.[index_id] = i.[index_id]
		INNER JOIN sys.tables t
		on t.object_id = s.object_id
		
		--where OBJECT_NAME(s.object_id)  = 'EmailAddress'
) A
	order by [index_size-kb] DESC

--ALTER INDEX [IX_WAREHOUSE_ExamSessionItemResponseTable_R11_3PT_script3] ON WAREHOUSE_ExamSessionItemResponseTable rebuild;

--413.56 MB (0%) after shrink

--10212 MB biggest index
--17702 MB (10%) free space
--17629.63 MB (10%) after rebuild all > 5%




