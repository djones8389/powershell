use Saxion_SurpassDataWarehouse

DECLARE @ExpandVersions int
,@centres nvarchar(65),@subjects nvarchar(4),@dateStartRange nvarchar(19),@dateEndRange nvarchar(19),@examVersion nvarchar(4),@CPID nvarchar(4000),@showScored nvarchar(1),@showNonScored nvarchar(1),@showUntagged nvarchar(4000),@tagGroups nvarchar(4000),@tagValues nvarchar(4000)
set @centres=N'40,50,34,49,36,35,44,41,37,30,39,47,42,45,54,38,48,53,46,43,52,33' set @subjects=N'1135'set @dateStartRange=N'2017-09-10 00:00:00' set @dateEndRange=N'2018-03-08 23:59:59'
set @examVersion=N'8916' set @CPID=NULL set @showScored=N'1'set @showNonScored=N'1'set @showUntagged=NULL set @tagGroups=NULL set @tagValues=NULL

IF @CPID = '' SET @CPID = NULL
IF @CPID IS NOT NULL SET @ExpandVersions = 1 ELSE SET @ExpandVersions = 0

IF OBJECT_ID('tempdb..#ES') IS NOT NULL DROP TABLE #ES
CREATE TABLE #ES(ExamSessionKey int
		,CentreKey int
		,QualificationKey int
		,ExamKey int
		,ExamVersionKey int
		,UserMarks float
		,Percentage float
		,CompletionDateTime datetime
		,isRescored bit)
INSERT INTO #ES
SELECT 
	ES.ExamSessionKey
	,ES.CentreKey
	,ES.QualificationKey
	,ES.ExamKey
	,ES.ExamVersionKey
	,ES.UserMarks
	,CASE WHEN ES.TotalMarksAvailable>0 THEN ES.UserMarks/ES.TotalMarksAvailable ELSE ES.UserMarks END
	,T.FullDateAlternateKey + ES.CompletionTime  CompletionDateTime
	,CASE WHEN EXISTS (SELECT * FROM FactExamSessionAudits FESA WHERE FESA.ExamSessionKey = ES.ExamSessionKey AND FESA.IsTemp = 0) THEN 1 ELSE 0 END isRescored
FROM	 
	dbo.FactExamSessions AS ES
	INNER JOIN dbo.DimTime AS T
		 ON ES.CompletionDateKey = T.TimeKey
WHERE
	(T.FullDateAlternateKey + ES.CompletionTime) BETWEEN @dateStartRange AND @dateEndRange
	AND ES.FinalExamState <> 10
	AND ES.ExcludeFromReporting <> 1
	AND ES.CentreKey IN (SELECT Value FROM dbo.fn_ParamsToList(@centres,0))
	AND ES.QualificationKey IN (SELECT Value FROM dbo.fn_ParamsToList(@subjects,0))
	AND ES.ExamVersionKey = @examVersion
	AND (
		(@showUntagged IS NULL AND @tagGroups IS NULL AND @tagValues IS NULL) -- no filter on Candidate Tags
		OR (
			(@showUntagged = 1 AND NOT EXISTS (SELECT * FROM CandidateTagValues CTV WHERE ES.CandidateKey = CTV.CandidateKey)) --untagged
			OR EXISTS(
				SELECT * 
				FROM 
					CandidateTagValues CTV 
					JOIN TagValues TV ON CTV.TagValueKey = TV.TagValueKey
				WHERE 
					ES.CandidateKey = CTV.CandidateKey
					AND (
						TV.TagTypeKey IN (SELECT Value FROM dbo.fn_ParamsToList(@tagGroups,0))
						OR CTV.TagValueKey IN (SELECT Value FROM dbo.fn_ParamsToList(@tagValues,0))
					)
			)
		)
	)
OPTION (MAXRECURSION 0)

IF OBJECT_ID('tempdb..#Items') IS NOT NULL DROP TABLE #Items
CREATE TABLE #Items(SessionCount int, Inx int, Inx5 int, ExamSessionKey int, ExamVersionKey int, CPID nvarchar(50), CPVersion int, OriginalVersion int, UserMarks float, MarkerMark float, FQRMark float, QuestionMarks float, QuestionTotalMarks float, QuestionName nvarchar(100), QuestionTypeKey int, CAQuestionTypeKey int, ViewingTime int, Attempted float, TestOrder int, CAId int, CompletionDateTime datetime, ModifiedBy int, Percentage float, IsDichotomous bit, Scored int, isRescored bit  PRIMARY KEY CLUSTERED (ExamSessionKey, CPID))

;WITH items AS (
-------------Query for NULL @unit and @LO -----------
	SELECT	
		NULL SessionCount
		,NULL Inx --NTILE(3) OVER(PARTITION BY ES.ExamVersionKey, QR.CPID, Q.CPVersion ORDER BY UserMarks DESC, QR.ExamSessionKey ASC) Inx
		,NULL Inx5 --,NTILE(5) OVER(PARTITION BY ES.ExamVersionKey, QR.CPID, Q.CPVersion ORDER BY UserMarks DESC, QR.ExamSessionKey ASC) Inx5
		,ES.ExamSessionKey
		,ES.ExamVersionKey
		,QR.CPID
		,QR.CPVersion CPVersion --IF @ExpandVersions=0 it will be overwritten below
		,QR.CPVersion OriginalVersion
		,ES.UserMarks
		,(SELECT TOP 1 AssignedMark FROM dbo.FactMarkerResponse MR WHERE MR.ExamSessionKey = QR.ExamSessionKey AND MR.CPID = QR.CPID ORDER BY SEQNO DESC) AssM
		,QR.Mark FQRMark
		,NULL AssM2
		,NULL QuestionTotalMarks
		,NULL QuestionName
		,NULL QuestionTypeKey
		,NULL CAQuestionTypeKey
		,NULL CAId
		,QR.ViewingTime
		,QR.Attempted
		,QR.ItemPresentationOrder
		,ES.CompletionDateTime
		,NULL ModifiedByUserKey
		,Percentage
		,NULL IsDichotomous
		,QR.Scored
		,isRescored
	FROM	
	#ES ES
	INNER JOIN	FactQuestionResponses QR
		 ON ES.ExamSessionKey = QR.ExamSessionKey
		 AND (@CPID IS NULL OR QR.CPID = @CPID)
		 AND( 
			(@showScored = 1 AND @showNonScored = 1)
			OR (@showNonScored = 1 AND QR.Scored = 0)
			OR (@showScored = 1 AND QR.Scored = 1)
		 )
) 
INSERT INTO #Items 
SELECT 
	SessionCount
	,Inx
	,Inx5
	,ExamSessionKey
	,ExamVersionKey
	,CPID
	,CPVersion
	,OriginalVersion
	,UserMarks
	,AssM
	,FQRMark
	,NULL QuestionMarks
	,QuestionTotalMarks
	,QuestionName
	,QuestionTypeKey
	,CAQuestionTypeKey
	,ViewingTime
	,Attempted	
	,ItemPresentationOrder
	,CAId
	,CompletionDateTime
	,ModifiedByUserKey
	,Percentage
	,IsDichotomous
	,Scored
	,isRescored
FROM items


IF @ExpandVersions = 0 BEGIN
	;WITH Versions AS (
		SELECT CPID, MAX(CPVersion) MaxVersion
		FROM #Items
		GROUP BY CPID
	)
	UPDATE #Items 
	SET 
		CPVersion = V.MaxVersion
	FROM 
		#Items I 
		JOIN Versions V ON I.CPID = V.CPID
END


;WITH items2 AS (
	SELECT 
		I.ExamSessionKey
		,I.CPID
		,i.CPVersion
		,ROW_NUMBER() OVER(PARTITION BY ExamVersionKey, I.CPID, I.CPVersion ORDER BY Percentage DESC, ExamSessionKey ASC) Inx
		,NTILE(5) OVER(PARTITION BY ExamVersionKey, I.CPID, I.CPVersion ORDER BY UserMarks DESC, ExamSessionKey ASC) Inx5
		,Q.TotalMark * I.FQRMark AssM2
		,Q.TotalMark QuestionTotalMarks
		,Q.QuestionName
		,Q.QuestionTypeKey
		,Q.CAQuestionTypeKey
		,CASE WHEN Q.CAQuestionTypeKey IS NOT NULL THEN FirstVersion.ExternalId ELSE NULL END CAId
		,Q.ModifiedByUserKey
		,CASE 
			WHEN 
				Q.CAQuestionTypeKey IN (2,4,5,7,9,18)	--MCQ, Either/Or, Numerical Entry, Short Answer, Select from a list, Hotspot
				AND Q.MarkingTypeKey=0				--ComputerMarked
				AND I.MarkerMark IS NULL			--Hadn't been overriden
				THEN 1 
			ELSE 0 
		 END IsDichotomous
	FROM 
		#Items I
		JOIN DimQuestions Q 
			ON I.CPID = Q.CPID
			AND I.CPVersion = Q.CPVersion
		OUTER APPLY (SELECT TOP 1 ExternalId FROM DimQuestions DQ2 WHERE DQ2.CPID = I.CPID ORDER BY CPVersion ASC) FirstVersion

)   

UPDATE I
SET
	 Inx = I2.Inx
	,Inx5 = I2.Inx5
	,QuestionMarks = 
		CASE WHEN isRescored = 1 
			THEN I2.AssM2
			ELSE ISNULL(I.MarkerMark, I2.AssM2)
		END
	,QuestionTotalMarks = I2.QuestionTotalMarks
	,QuestionName = I2.QuestionName
	,QuestionTypeKey = I2.QuestionTypeKey
	,CAQuestionTypeKey = I2.CAQuestionTypeKey
	,CAId = I2.CAId
	,ModifiedBy = I2.ModifiedByUserKey
	,IsDichotomous = I2.IsDichotomous
FROM 
	#Items I 
	JOIN items2 I2 
		ON I.CPID = I2.CPID
		and i.CPVersion = i2.cpversion
		AND I.ExamSessionKey = I2.ExamSessionKey

select distinct cpid, cpversion, QuestionTotalMarks
from #Items
where cpid in ('5909P442622','5909P442628','5909P442630','5909P442632','5909P442634','5909P442636','5909P442640','5909P442641','5909P442647','5909P442651','5909P442652','5909P442654','5909P442656','5909P442657','5909P442658','5909P442659','5909P442660','5909P442661','5909P442662','5909P442724','5909P442727','5909P442729','5909P442730','5909P442731','5909P442732','5909P442733','5909P442734','5909P442736','5909P442739','5909P442741')

select *
from DimQuestions
where cpid = '5909P442660'

--select distinct A.CPID
--		, A.CPVersion [SDW]
--		--, PackagePriority --CA
--		, a.QuestionTotalMarks [SDWMark]
--		, i.MajorVersion [CA]
--		, i.Mark [CAMark]
--FROM #Items A
--inner join Saxion_ContentAuthor..Items I
--on I.id = substring(cpid, charindex('P',cpid)+1, 10) 
--inner join DimQuestions dq 
--on dq.CPID = a.CPID	
--	and dq.CPVersion = A.CPVersion
--WHERE QuestionTotalMarks=0


--IF OBJECT_ID('tempdb..#ES') IS NOT NULL DROP TABLE #ES
--IF OBJECT_ID('tempdb..#Items') IS NOT NULL DROP TABLE #Items

--select i.id, MajorVersion
--from Saxion_ContentAuthor..Items I (NOLOCK)


use saxion_contentauthor

select  title
from Subjects
where ProjectId = 5909