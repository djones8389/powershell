DECLARE @DateStart datetime
	,@DateEnd datetime

SET @DateStart='22/03/2016 00:00:00' 
SET @DateEnd='22/04/2016 00:00:00'

DECLARE @DateKeyStart int, @DateKeyEnd int, @DateCompletedKey int
SELECT @DateKeyStart = (
	CASE 
		WHEN @DateStart<(SELECT FullDateAlternateKey FROM DimTime WHERE TimeKey = 1) THEN 1 
		ELSE (SELECT TimeKey FROM DimTime WHERE FullDateAlternateKey = CAST(@DateStart AS date)) 
	END)
	,@DateKeyEnd = (
	CASE 
		WHEN @DateStart>(SELECT FullDateAlternateKey FROM DimTime WHERE TimeKey = IDENT_CURRENT('DimTime')) 
			THEN IDENT_CURRENT('DimTime') 
		ELSE (SELECT TimeKey FROM DimTime WHERE FullDateAlternateKey = CAST(@DateEnd AS date)) 
	END)
	--,@DateCompletedKey = (SELECT TimeKey FROM DimTime WHERE FullDateAlternateKey = CAST(@DateCompleted AS date)) 

--SELECT 	@DateKeyStart, @DateKeyEnd
----------------------------------------  Exam sessions  ----------------------------------------
--IF OBJECT_ID('tempdb..#ES') IS NOT NULL DROP TABLE #ES
--CREATE TABLE #ES(
--	ExamSessionKey int PRIMARY KEY CLUSTERED
--	,SeqN int
--	,CentreKey int	
--	,ExamVersionKey int
--	, keycode nvarchar(10)
--	,DateCompleted datetime
--	,ExamUserMark decimal(9,4)
--	,ExamTotalMark decimal(9,4)
--	,ExamScaledMark decimal(9,4)
--	,FinalExamState int
--	--,ItemNumber int
--)

--INSERT INTO #ES
SELECT
	FES.ExamSessionKey
	,NULL --ROW_NUMBER() OVER (PARTITION BY FES.ExamVersionKey, FES.CentreKey ORDER BY FES.UserMarks ASC, FES.ExamSessionKey ASC) SeqN
	,CentreKey
	,ExamVersionKey
	,keycode
	,DT.FullDateAlternateKey +  FES.CompletionTime DateCompleted
	,FES.UserMarks 
	,FES.TotalMarksAvailable
	,NULL --FES.UserMarks/FES.TotalMarksAvailable*50
	,FES.FinalExamState
	, fes.candidatekey
	--,COUNT(CID) 
FROM 
	FactExamSessions FES
	JOIN DimTime DT ON FES.CompletionDateKey = DT.TimeKey

WHERE 
	FES.FinalExamState<>10
	AND FES.CompletionDateKey BETWEEN @DateKeyStart AND @DateKeyEnd
	AND (FES.CompletionDateKey = @DateCompletedKey OR @DateCompletedKey IS NULL)
	AND FES.CentreKey IN (784,749,796,113,442,347,138,564,39,73,30,785,786,350,797,794,769,783)
	AND FES.ExamVersionKey IN (144,99,96,192,1008,97,193,252,253,594,595,722,723,1007)
	AND FES.TotalMarksAvailable>0

---------------------------------------  Item(task) level - to obtain TPM marks ---------------------------------------
IF OBJECT_ID('tempdb..#Items_raw') IS NOT NULL DROP TABLE #Items_raw
CREATE TABLE #Items_raw (
	CentreKey int 
	,ExamVersionKey int 
	,ExamSessionKey int 
	,SeqN int
	,N int
	,SDp float
	,ExamUserMark decimal(9,4)
	--,ExamTotalMark float
	,CPID nvarchar(50)
	,CPVersion int
--	,MarkerMark float
--	,UserMark float
	,ItemUserMark decimal(9,4)
	,ItemTotalMark decimal(9,4)
	,FinalExamState int
	,AutoMarked int
	,TPMScaled bit
	,PRIMARY KEY CLUSTERED (
		CentreKey ASC
		,ExamVersionKey ASC 
		,ExamSessionKey ASC
		,CPID ASC
	)
)

;WITH 
MarkerMarks AS(
      SELECT 
            ROW_NUMBER() OVER(PARTITION BY FMR.ExamSessionKey, FMR.CPID ORDER BY SeqNo DESC) N
            ,FMR.ExamSessionKey
            ,FMR.CPID
            ,FMR.assignedMark
      FROM 
            #ES ES
            JOIN FactMarkerResponse FMR
                  ON FMR.ExamSessionKey = ES.ExamSessionKey
), Items_raw AS (
	SELECT 
		ES.CentreKey
		,ES.ExamVersionKey
		,FQR.ExamSessionKey
		,NULL SeqN
		,NULL N
		,NULL SDp
		,NULL ExamUserMark
		--,ES.ExamTotalMark
		,FQR.CPID
		,FQR.CPVersion
		,ROUND(COALESCE(TPM.Mark, MM.assignedMark, FQR.Mark*DQ.TotalMark),4) ItemUserMark
		--,MM.assignedMark MarkerMark
		--,FQR.Mark*DQ.TotalMark UserMark
		,ISNULL(TPM.TotalMark, DQ.TotalMark) ItemTotalMark
		,FinalExamState
		,CASE WHEN MM.assignedMark IS NULL THEN 1 ELSE 0 END AutoMarked
		,CASE WHEN TPM.Mark IS NULL THEN 0 ELSE 1 END TPMScaled
	FROM 
		#ES ES
		--JOIN #EV EV ON ES.ExamVersionKey = EV.ExamVersionKey AND ES.CentreKey = EV.CentreKey
		JOIN FactQuestionResponses FQR ON FQR.ExamSessionKey = ES.ExamSessionKey
		JOIN DimQuestions DQ ON DQ.CPID = FQR.CPID AND DQ.CPVersion = FQR.CPVersion
		LEFT JOIN MarkerMarks MM ON MM.CPID = FQR.CPID AND MM.ExamSessionKey = FQR.ExamSessionKey AND MM.N = 1 
		LEFT JOIN FactTestPackageMarks TPM ON TPM.CPID = FQR.CPID AND TPM.ExamSessionKey = FQR.ExamSessionKey
)
INSERT INTO #Items_raw
SELECT * FROM Items_raw 


;WITH TPMMarks AS (
	SELECT
		ExamSessionKey
		,SUM(ItemUserMark) ExamUserMark
		,SUM(ItemTotalMark) ExamTotalMark
	FROM #Items_raw
	GROUP BY
		ExamSessionKey
)
UPDATE ES
SET
	ExamUserMark = ISNULL(TPM.ExamUserMark, ES.ExamUserMark) 
	,ExamTotalMark = ISNULL(TPM.ExamTotalMark, ES.ExamTotalMark)  
FROM #ES ES
	JOIN TPMMarks TPM 
	ON ES.ExamSessionKey = TPM.ExamSessionKey
	
;WITH Seq AS (
	SELECT 
		ExamSessionKey
		,ROW_NUMBER() OVER (PARTITION BY ExamVersionKey, CentreKey ORDER BY ExamUserMark ASC, ExamSessionKey ASC) SeqN
		,ExamUserMark/ExamTotalMark*50 ExamScaledMark
	FROM #ES
)
UPDATE ES 
SET 
	SeqN = S.SeqN
	,ExamScaledMark = S.ExamScaledMark
FROM #ES ES JOIN Seq S ON ES.ExamSessionKey = S.ExamSessionKey
	
--SELECT * FROM #ES

--------------------------------------  Exam version level  --------------------------------------
IF OBJECT_ID('tempdb..#EV') IS NOT NULL DROP TABLE #EV
CREATE TABLE #EV (
	CentreKey int
	,ExamVersionKey int
	,N int
	,SDp float

)
INSERT INTO #EV

SELECT 
	CentreKey
	,ExamVersionKey
	, keycode
	,COUNT(ExamSessionKey) N
	,STDEVP(ExamUserMark) SDp
FROM #ES ES
GROUP BY 
	CentreKey
	, ExamVersionKey
	, keycode
---- Back to item(task) level: updating session- and version-level stats, calculate item-level stats ----
UPDATE IR
SET	
	ExamUserMark = ES.ExamUserMark
	,SeqN = ES.SeqN
	,N = EV.N
	,SDp = EV.SDp
FROM #Items_raw IR
	JOIN #ES ES ON IR.ExamSessionKey = ES.ExamSessionKey
	JOIN #EV EV ON IR.ExamVersionKey = EV.ExamVersionKey AND IR.CentreKey = EV.CentreKey
--SELECT * FROM #Items_raw


-------------------------------------  Component(item) level  ------------------------------------
IF OBJECT_ID('tempdb..#Components') IS NOT NULL DROP TABLE #Components
CREATE TABLE #Components (
	CentreKey int
	,ExamVersionKey int
	,CPID nvarchar(50)
	,CID nvarchar(50)
	,OptionKey int
	,FV float
	--,Variance float
	,DI float
	,PBis float
	,PRIMARY KEY CLUSTERED (
		CentreKey ASC
		,ExamVersionKey ASC 
		,CID ASC
		,OptionKey ASC
	)
)
IF OBJECT_ID('tempdb..#Components_raw') IS NOT NULL DROP TABLE #Components_raw
CREATE TABLE #Components_raw (
	CentreKey int 
	,ExamVersionKey int 
	,ExamSessionKey int 
	,SeqN int
	,N int
	,SDp float
	,ExamUserMark decimal(9,4)
	,CPID nvarchar(50)
	,CID nvarchar(50)
	,OptionKey int
	,CPVersion int
	,QuestionType int
	,ComponentUserMark decimal(9,4)
	,ComponentTotalMark decimal(9,4)
	--,HasCPAuditEntry int
	,CalculatePbis bit
	,sel nvarchar(100)
	,PRIMARY KEY CLUSTERED (
		CentreKey ASC
		,ExamVersionKey ASC 
		,ExamSessionKey ASC
		,CID ASC
		,OptionKey ASC
	)
)

;WITH Components_raw AS (	
	SELECT 
		I.CentreKey
		,I.ExamVersionKey
		,I.ExamSessionKey
		,I.SeqN
		,I.N
		,I.SDp
		,I.ExamUserMark
		,I.CPID
		,DC.CID
		,-1 OptionKey
		,I.CPVersion
		,DC.QuestionType
		, ISNULL(
			ROUND(
				CASE 
					WHEN I.AutoMarked = 1 --MCQ
						AND I.FinalExamState = 12 --W/O SecureMarker
						THEN FCR.Mark*DC.TotalMark
					ELSE I.ItemUserMark*DC.Weight
				END
			,4)
		  , 0
		) ComponentUserMark
		,DC.TotalMark ComponentTotalMark
		--,DC.HasCPAuditEntry
		,CASE 
			WHEN QuestionType = 10 AND HasCPAuditEntry = 1 AND DC.TotalMark = 1 AND DC.ItemXML.value('count(/C/I[@cor=1])','int')=1 THEN 1
			WHEN QuestionType = 12 THEN 1
			ELSE NULL
		END CalculatePbis
		,CASE 
			WHEN QuestionType = 10 THEN FCR.ShortDerivedResponse
			ELSE NULL
		END sel

	FROM #Items_raw I
		JOIN DimComponents DC
			ON DC.CPID = I.CPID
			AND DC.CPVersion = I.CPVersion
			AND DC.Weight>0
			AND DC.TotalMark>0
		LEFT JOIN FactComponentResponses FCR 
			ON DC.CID = FCR.CID
			AND I.CPVersion = FCR.CPVersion
			AND I.ExamSessionKey = FCR.ExamSessionKey
	WHERE 
		DC.QuestionType<>16
		AND DC.QuestionType<>13
		AND (
			DC.QuestionType<>12
			OR DC.ItemXML.value('count(C/I)[1]', 'integer') = 1
		)
		OR DC.MarkingMethod=1
		OR I.AutoMarked=0
)
INSERT INTO #Components_raw
SELECT * FROM Components_raw

;WITH Components_raw2 AS (	
	SELECT 
		I.CentreKey
		,I.ExamVersionKey
		,I.ExamSessionKey
		,I.SeqN
		,I.N
		,I.SDp
		,I.ExamUserMark
		,I.CPID
		,DO.CID
		,DO.OptionKey
		,I.CPVersion
		,DO.OptionType
		,ISNULL(FR.Mark,
			CASE WHEN 
				FR.ExamSessionKey IS NULL AND DO.CorrectAnswersCount>0 --If a drop zone has correct answer but a candidate hasn't placed any item there, he gets 0.
			THEN 0 ELSE NULL END
		) ComponentUserMark
		,1 ComponentTotalMark
		,CASE WHEN DO.OptionType = 17 AND DO.CorrectAnswersCount <> 1 THEN 0 ELSE 1 END CalculatePbis --For re-ordering and grouped picklists we always have only 1 correct answer
		,NULL sel
	FROM #Items_raw I
		JOIN DimOptions DO
			ON DO.CPID = I.CPID
			AND DO.CPVersion = I.CPVersion
			AND (
				(DO.OptionType=17 AND DO.CorrectAnswersCount IS NOT NULL) --exclude distractor drop zones 
				OR DO.OptionType=13
				OR DO.OptionType=12
			)
			AND DO.ComponentMarkingMethod = 0
			AND I.AutoMarked=1
		LEFT JOIN FactOptionResponses FR
			ON DO.OID = FR.OID
			AND DO.CPVersion = FR.CPVersion
			AND I.ExamSessionKey = FR.ExamSessionKey
	--WHERE DO.CorrectAnswersCount=1
)
INSERT INTO #Components_raw
SELECT * FROM Components_raw2

--SELECT * FROM #Components_raw


;WITH ComponentStats AS (
	SELECT 
		CentreKey
		,ExamVersionKey
		,CPID
		,CID
		,OptionKey
		,AVG(ComponentUserMark/ComponentTotalMark) FV
		--,VAR(ComponentUserMark) Variance
	FROM #Components_raw
	GROUP BY ExamVersionKey, CentreKey, CID, CPID, OptionKey
), DI_lo AS (
	SELECT 
		CentreKey
		, ExamVersionKey
		, CID
		, OptionKey
		, AVG(ComponentUserMark/ComponentTotalMark) FV
	FROM #Components_raw
	WHERE 
		--Round .5 to the nearest even number (R-3/SAS-2 type from http://en.wikipedia.org/wiki/Quantile)
		SeqN<=CASE WHEN CEILING(0.27*N) - 0.27*N = 0.27*N - FLOOR(0.27*N) AND ROUND(0.27*N, 0)%2<>0 THEN FLOOR(0.27*N) ELSE ROUND(0.27*N,0) END
	GROUP BY CID, ExamVersionKey, CentreKey, OptionKey
), DI_hi AS (
	SELECT 
		CentreKey
		, ExamVersionKey
		, CID
		, OptionKey
		, AVG(ComponentUserMark/ComponentTotalMark) FV
	FROM #Components_raw
	WHERE 
		--Round .5 to the nearest even number (R-3/SAS-2 type from http://en.wikipedia.org/wiki/Quantile)
		SeqN>=CASE WHEN CEILING(0.73*N) - 0.73*N = 0.73*N - FLOOR(0.73*N) AND ROUND(0.73*N, 0)%2<>0 THEN FLOOR(0.73*N) ELSE ROUND(0.73*N,0) END
	GROUP BY CID, ExamVersionKey, CentreKey, OptionKey
), DIs AS (
	SELECT 
		DI_lo.ExamVersionKey
		, DI_lo.CentreKey
		, DI_lo.CID
		, DI_lo.OptionKey
		, DI_hi.FV - DI_lo.FV DI
	FROM  
		DI_lo JOIN DI_hi 
			ON DI_lo.CID = DI_hi.CID 
			AND DI_lo.ExamVersionKey = DI_hi.ExamVersionKey 
			AND DI_lo.CentreKey = DI_hi.CentreKey
			AND DI_lo.OptionKey = DI_hi.OptionKey
			
), Components AS (
	SELECT 
		CS.*
		,DIs.DI
		,NULL PBis
	FROM 
		ComponentStats CS
		LEFT JOIN DIs ON CS.ExamVersionKey = DIs.ExamVersionKey AND CS.CentreKey = DIs.CentreKey AND CS.CID = DIs.CID AND CS.OptionKey = DIs.OptionKey
)
INSERT INTO #Components
SELECT * FROM Components

--SELECT * FROM #Components

;WITH ComponentsMCQ AS (
	SELECT 
		C.*
	FROM #Components_raw C
	WHERE 
		C.CalculatePbis = 1 --implies CorrectAnswerCount=1
		AND ComponentTotalMark=1
), ComponentsPBis_raw AS (
	SELECT 
		CentreKey
		, ExamVersionKey
		, CID
		, OptionKey
		, SDp Sx 
		, SUM(ComponentUserMark*ExamUserMark)/SUM(ComponentUserMark) Xp
		, SUM((1-ComponentUserMark)*ExamUserMark)/SUM(1-ComponentUserMark) Xq
		, CAST(SUM(ComponentUserMark) as float)/COUNT(ComponentUserMark) p
	FROM  ComponentsMCQ

	GROUP BY ExamVersionKey, CentreKey, CID, SDp, OptionKey
	HAVING 
			SUM(ComponentUserMark)>0
			AND SUM(1-ComponentUserMark)>0
), ComponentsPBis AS (
	SELECT 
		CentreKey
		,ExamVersionKey
		,CID
		,OptionKey
		,CASE WHEN Sx=0 THEN NULL ELSE (Xp-Xq)/Sx*SQRT(p*(1-p)) END PBis
	FROM ComponentsPBis_raw
)

UPDATE C
	SET PBis = CPB.PBis
--SELECT *
FROM
	#Components C
	JOIN ComponentsPBis CPB ON C.ExamVersionKey = CPB.ExamVersionKey AND C.CentreKey = CPB.CentreKey AND C.CID = CPB.CID AND C.OptionKey = CPB.OptionKey
	
--SELECT * FROM #Components

----------------------------------------  Options level  ----------------------------------------
IF OBJECT_ID('tempdb..#Options') IS NOT NULL DROP TABLE #Options

CREATE TABLE #Options (
	CentreKey int
	,ExamVersionKey int
	,CID nvarchar(50)
	,Chr nvarchar(10)
	,p float
	,PBis float
	,PRIMARY KEY CLUSTERED (
		CentreKey ASC
		,ExamVersionKey ASC 
		,CID ASC
		,Chr ASC
	)
)

;WITH Options_raw AS (
	SELECT 
		CR.CentreKey
		,CR.ExamVersionKey
		,CR.CID
		,CR.SDp
		,CR.ExamUserMark
		,CR.sel
		--,ops.op.value('data(@ID)', 'nvarchar(100)') OID
		,ops.op.value('data(@asC)', 'nvarchar(100)') Chr
		--,ops.op.value('data(@cor)', 'nvarchar(100)') isCorrect
	FROM 
		#Components_raw CR
		JOIN DimComponents DC ON DC.CID = CR.CID AND DC.CPVersion = CR.CPVersion
		CROSS APPLY DC.ItemXML.nodes('/C/I') ops(op)
	WHERE
		CR.CalculatePbis = 1 --Implies CorrectAnswerCount=1
		AND CR.ComponentTotalMark=1
		AND sel IS NOT NULL --Implies QuestionType=10 
), Options_raw2 AS (
	SELECT 
		* 
		,CASE WHEN sel = Chr THEN 1 ELSE 0 END p
	FROM Options_raw
), Options_raw3 AS (
	SELECT 
		CentreKey
		, ExamVersionKey
		, CID
		--, OID
		, Chr
		--, isCorrect
		, SDp Sx 
		--, STDEVP(ExamUserMark) Sx --under assumption that we have equal number of options for an item in different sessions
		, CASE WHEN SUM(p)>0 THEN SUM(p*ExamUserMark)/SUM(p) ELSE NULL END Xp
		, CASE WHEN SUM(1-p)>0 THEN SUM((1-p)*ExamUserMark)/SUM(1-p) ELSE NULL END Xq
		, CAST(SUM(p) as float)/COUNT(p) p
	FROM Options_raw2
	GROUP BY ExamVersionKey, CentreKey, CID, Chr
		--, isCorrect
		, SDp
), Options AS (
	SELECT 
		CentreKey
		,ExamVersionKey
		,CID
		,Chr
		,p
		,CASE WHEN Sx=0 THEN NULL ELSE (Xp-Xq)/Sx*SQRT(p*(1-p)) END pbis
	FROM Options_raw3
)
INSERT INTO #Options
SELECT * FROM Options


---------------------- Quintiles & final query ----------------------
;WITH QU_raw AS (
	SELECT 
		EV.ExamVersionKey
		,EV.CentreKey
		,CEILING(CAST(SeqN as float)/N*5) QU
		,COUNT(ExamSessionKey) QU_cnt
	FROM #ES ES
		JOIN #EV EV
			ON ES.ExamVersionKey = EV.ExamVersionKey 
			AND ES.CentreKey = EV.CentreKey
	GROUP BY 
		EV.ExamVersionKey
		,EV.CentreKey
		, CEILING(CAST(SeqN as float)/N*5)
), QU AS (
	SELECT *
	FROM QU_raw
	PIVOT (
		SUM(QU_cnt)
		FOR QU IN ([1],[2],[3],[4],[5])
	) AS p
)
SELECT 
	DC.CentreName
	,ev.CentreKey
	,ev.ExamVersionKey
	,ev.N
	,C.CID + ISNULL('T'+CAST(NULLIF(C.OptionKey, -1) AS nvarchar(10)),'') CID
	,C.FV
	,C.DI
	,C.PBis ComponentPBis
	,O.Chr
	,O.p
	,O.PBis OptionPBis
	,QU.[1] qu1,QU.[2] qu2,QU.[3] qu3,QU.[4] qu4,QU.[5] qu5
FROM 
	#EV EV 
	JOIN DimCentres DC ON DC.CentreKey = EV.CentreKey
	JOIN QU ON QU.ExamVersionKey = EV.ExamVersionKey AND QU.CentreKey = EV.CentreKey
	LEFT JOIN #Components C ON EV.CentreKey = C.CentreKey AND EV.ExamVersionKey = C.ExamVersionKey
	LEFT JOIN #Options O ON O.CentreKey = C.CentreKey AND O.ExamVersionKey = C.ExamVersionKey AND O.CID = C.CID

IF OBJECT_ID('tempdb..#ES') IS NOT NULL DROP TABLE #ES
IF OBJECT_ID('tempdb..#Items_raw') IS NOT NULL DROP TABLE #Items_raw
IF OBJECT_ID('tempdb..#EV') IS NOT NULL DROP TABLE #EV
IF OBJECT_ID('tempdb..#Items') IS NOT NULL DROP TABLE #Items
IF OBJECT_ID('tempdb..#Components') IS NOT NULL DROP TABLE #Components
IF OBJECT_ID('tempdb..#Components_raw') IS NOT NULL DROP TABLE #Components_raw
IF OBJECT_ID('tempdb..#Options') IS NOT NULL DROP TABLE #Options