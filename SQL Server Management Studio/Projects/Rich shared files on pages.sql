--/****** Script for SelectTopNRows command from SSMS  ******/
--SELECT AT.ItemId
--	, ServerDateTime
	
--  FROM [Demo_ContentAuthor].[dbo].[AuditTrails] AT
--  INNER JOIN Items I
--  on I.ID = AT.ItemId
--  INNER JOIN Subjects S
--  on S.ID = I.SubjectId

--  where S.id = 125
--	and i.id = 18030
--  order by AT.ItemId--, CreateDate

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

  select A.ItemId
	, A.LastCheckIn
	, A.MediaItemName
	, A.SubjectID
	,[URL]
  FROM (
	  SELECT AT.ItemId
			, ServerDateTime [LastCheckIn]
			, I.Version [ItemVersion]
			, m.Name [MediaItemName]
			, s.id	[SubjectID]
			, 'https://'+substring(DB_NAME(),0,charindex('_',DB_NAME()))+'.btlsurpass.com/#ItemAuthoring/Subject/'+cast(S.id as varchar(10))+'/Item/Edit/'+cast(i.id  as varchar(10))+ '' [URL]
			, ROW_NUMBER() OVER (Partition by AT.ItemId order by ServerDateTime desc) R
	  FROM [Demo_ContentAuthor].[dbo].[AuditTrails] AT
	  INNER JOIN [Demo_ContentAuthor].[dbo].[Items]  I
	  on I.ID = AT.ItemId
	  INNER JOIN [Demo_ContentAuthor].[dbo].[Subjects]  S
	  on S.id = I.SubjectId
	  INNER JOIN [MediaItems] M
	  on M.SubjectId = S.Id
	
	  LEFT JOIN [Demo_ContentAuthor].[dbo].[SourceMaterials] SM
	  on   sm.ItemId = I.ID
	  --LEFT JOIN [Demo_ContentAuthor].[dbo].[Tools] T
	  --on T.MediaItemId = M.Id
	  where ServerDateTime > '2017-04-13 11:10:07.070'
		--and s.id = 125
			and (M.Type = 5 or m.RequireTool = 1)
    ) A
	where R = 1

--where m.SubjectId = 125
--	and (M.Type = 5 or m.RequireTool = 1)

select *
from SourceMaterials
where ItemId = 18388



SELECT distinct m.name
  FROM [Demo_ContentAuthor].[dbo].[MediaItems] M
  left JOIN [Demo_ContentAuthor].[dbo].[SourceMaterials] SM
  on SM.MediaItemId = M.Id
  LEFT JOIN [Demo_ContentAuthor].[dbo].[Items] I
  on I.Id = Sm.ItemId
	 and I.SubjectId = M.SubjectId
  LEFT JOIN [Demo_ContentAuthor].[dbo].[Tools] T
  on T.MediaItemId = M.Id
where m.SubjectId = 125
	and (M.Type = 5 or m.RequireTool = 1)



--    left JOIN [Demo_ContentAuthor].[dbo].[SourceMaterials] SM
--  on SM.MediaItemId = M.Id
--  LEFT JOIN [Demo_ContentAuthor].[dbo].[Items] I
--  on I.Id = Sm.ItemId
--	 and I.SubjectId = M.SubjectId
--  LEFT JOIN [Demo_ContentAuthor].[dbo].[Tools] T
--  on T.MediaItemId = M.Id
--where m.SubjectId = 125
--	and (M.Type = 5 or m.RequireTool = 1)