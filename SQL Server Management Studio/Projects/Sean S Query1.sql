--SELECT wut.UserId
--    ,wut.Forename
--    ,wut.Surname
--from WAREHOUSE_UserTable WUT
--LEFT JOIN WAREHOUSE_ExamSessionTable WEST
--ON WEST.WarehouseUserID = WUT.ID
--LEFT JOIN WAREHOUSE_ExamSessionTable_Shreded WESTS
--on WESTS.examSessionId = WEST.ID
--where wut.userid  in (20,569,31)
--group by wut.userid, wut.Forename, wut.Surname


--       SELECT ROW_NUMBER() OVER (
--                     PARTITION BY userid
--                     ,Forename
--                     ,Surname 
--                     ORDER BY userid
--                     ) R
--              ,userid
--              ,Forename
--              ,Surname
--       FROM WAREHOUSE_UserTable wut (NOLOCK)
--	   where wut.userid  in (20,569,31)


--SELECT A.*
--FROM (

--SELECT ROW_NUMBER() OVER (
--                     PARTITION BY userid					 
--                     ,Forename
--                     ,Surname 
--					 --,centrename
--					 --,centrecode
--                     ORDER BY userid
--                     ) R
--	          ,UserId
--              ,Forename
--              ,Surname
--from WAREHOUSE_UserTable
--where userid  in (569,20)
--group by userid, Forename, Surname

--) A



--select a.*
--,	ROW_NUMBER() OVER (
--                     PARTITION BY WAREHOUSE_UserTable.userid					 
--                     ,WAREHOUSE_UserTable.Forename
--                     ,WAREHOUSE_UserTable.Surname 
--					 --,centrename
--					 --,centrecode
--                     ORDER BY WAREHOUSE_UserTable.userid
--                     ) R
--FROM (

--select UserId, Forename, Surname
--from WAREHOUSE_UserTable
--where WAREHOUSE_UserTable.userid  in (20,569)
--group by UserId, Forename, Surname
--) A
--INNER JOIN WAREHOUSE_UserTable
--on WAREHOUSE_UserTable.UserId = A.UserId

SET STATISTICS IO ON


SELECT C.userid
	,c.Forename
	,c.Surname
	,c.centreName
	,c.centreCode
FROM (
	SELECT DISTINCT WUT.UserId
		,WUT.foreName
		,WUT.surName
		,ROW_NUMBER() OVER (
			PARTITION BY WUT.UserId
			,WUT.foreName
			,WUT.surName ORDER BY wut.id
			) M
		,centreName
		,centreCode
	FROM WAREHOUSE_UserTable WUT
	LEFT JOIN WAREHOUSE_ExamSessionTable WEST ON WEST.WAREHOUSEUserID = WUT.ID
	LEFT JOIN WAREHOUSE_ExamSessionTable_Shreded WESTS ON WESTS.examSessionId = west.ID
	WHERE WUT.UserID IN (
			SELECT B.UserId
			FROM (
				SELECT A.UserId
					,sum(R) [SUM]
				FROM (
					SELECT UserId
						,Forename
						,Surname
						,ROW_NUMBER() OVER (
							PARTITION BY UserId ORDER BY UserId
							) r
					FROM WAREHOUSE_UserTable
					--WHERE WAREHOUSE_UserTable.userid IN (
					--		20
					--		,569
					--		,128603
					--		)
					GROUP BY UserId
						,Forename
						,Surname
					) A
				GROUP BY UserId
				) B
			WHERE [SUM] > 1
			)
	) C
WHERE M = 1

