SELECT '<AssessmentList>' + (
SELECT 
CASE
    WHEN [AssessmentRules].exist('data(//Section/@Fixed="0")') = 1 THEN 0
    WHEN [AssessmentRules].exist('data(//Section/@Fixed="1")') = 1 THEN 1
    ELSE 0
    END AS 'Assessment/@VersionLockingEnabled',                  
    AssessmentTable.UseAsTemplate                                     AS 'Assessment/@UseAsTemplate',
    QualificationName                                                 AS 'Assessment/QualificationName',
    QualificationID                                                   AS 'Assessment/QualificationID',
    AssessmentName                                                    AS 'Assessment/AssessmentName',
    AssessmentTable.ID                                                AS 'Assessment/AssessmentID',
    AssessmentGroupTable.Name                                         AS 'Assessment/AssessmentGroupName',
    AssessmentGroupID                                                 AS 'Assessment/AssessmentGroupID',
    ExternalReference                                                 AS 'Assessment/ExternalReference',
    AssessmentStatus                                                  AS 'Assessment/Status',
    CONVERT(varchar(11), AssessmentGroupTable.ValidFromDate, 106)     AS 'Assessment/ValidFromDate',
    CONVERT(varchar(11), AssessmentGroupTable.ExpiryDate, 106)        AS 'Assessment/ExpiryDate',
    CONVERT(varchar(8), StartTime, 114)                               AS 'Assessment/StartTime', 
    CONVERT(varchar(8), EndTime, 114)                                 AS 'Assessment/EndTime',
    CONVERT(varchar(20), LastInUseDate, 113)                          AS 'Assessment/LastInUseDate',
    AssessmentTable.IsValid                                           AS 'Assessment/IsValid',
    AssessmentGroupTable.OutputType                                   AS 'Assessment/OutputType',
    ISNULL(CONVERT(varchar(11), AssessmentTable.ValidFrom , 106),'')  AS 'Assessment/VersionValidFrom',
    ISNULL(CONVERT(varchar(11), AssessmentTable.ExpiryDate, 106),'')  AS 'Assessment/VersionExpiryDate',
    ISNULL(RequiresVersionAvailability,0)                             AS 'Assessment/IsRequiresVersionAvailability',
    ISNULL(IsCheckedOut,0)                                            AS 'Assessment/IsCheckedOut',
    CheckedOutBy                                                      AS 'Assessment/CheckedOutBy',
    LastCheckedInBy                                                   AS 'Assessment/LastCheckedInBy',
    Locked  AS 'Assessment/Locked'--,
		--AssessmentGroupTable.Name AS 'AssessmentGroup/Name',
  --      AssessmentGroupTable.ID   AS 'AssessmentGroup/ID',
		--QualificationTable.QualificationName  AS 'Qualification/Name',
  --      QualificationTable.ID                 AS 'Qualification/ID',
  --      AssessmentGroupTable.OutputType       AS 'Qualification/OutputType'		
	--CONVERT(varchar(11), MIN(ValidFromDate), 106) AS 'EarliestDate/*',
	--CONVERT(varchar(11), MAX(AssessmentGroupTable.ExpiryDate), 106) AS 'LatestDate/*'
FROM AssessmentTable WITH (NOLOCK)
INNER JOIN AssessmentGroupTable WITH (NOLOCK)
ON AssessmentGroupTable.ID = AssessmentTable.AssessmentGroupID
INNER JOIN QualificationTable WITH (NOLOCK)
ON QualificationID = QualificationTable.ID
WHERE QualificationID IN (103)
ORDER BY AssessmentGroupTable.Name
FOR XML PATH('')
) +  '<AssessmentList>'

--FOR XML PATH('Assessment'), TYPE

SELECT DISTINCT	
	   AssessmentGroupTable.Name AS 'AssessmentGroup/Name',
        AssessmentGroupTable.ID   AS 'AssessmentGroup/ID',
		QualificationTable.QualificationName  AS 'Qualification/Name',
        QualificationTable.ID                 AS 'Qualification/ID',
        AssessmentGroupTable.OutputType       AS 'Qualification/OutputType',
		CONVERT(varchar(11), MIN(ValidFromDate), 106) AS 'EarliestDate/*',
		CONVERT(varchar(11), MAX(AssessmentGroupTable.ExpiryDate), 106) AS 'LatestDate/*'
FROM AssessmentTable WITH (NOLOCK)
INNER JOIN AssessmentGroupTable WITH (NOLOCK)
ON AssessmentGroupTable.ID = AssessmentTable.AssessmentGroupID
INNER JOIN QualificationTable WITH (NOLOCK)
ON QualificationID = QualificationTable.ID
WHERE QualificationID IN (103)
group by
		AssessmentGroupTable.Name
		,AssessmentGroupTable.ID
		,QualificationTable.QualificationName
		, QualificationTable.ID 
		,AssessmentGroupTable.OutputType
ORDER BY AssessmentGroupTable.Name
FOR XML PATH('')