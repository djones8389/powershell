/* Q. Make the ID's in #myTable unique using a single update statement */

SELECT A.*
FROM (
SELECT *
	, ROW_NUMBER() OVER(PARTITION BY id	ORDER BY id)  R
FROM #myTable
) A
WHERE R > 1




update #myTable
set ID = (
	case R when 2 then -2
		   when 3 then -3
		   when 4 then -4
		   when 5 then -5
		   when 6 then -6
		   when 7 then -7
		   when 8 then -8
	end 
	)
select  *
from #myTable A
inner join (
	
	SELECT A.ID
		 , a.val
		 , a.R
	FROM (
	SELECT ID [id]
		, val [val]
		, ROW_NUMBER() OVER(PARTITION BY id	ORDER BY id)  R
	FROM #myTable
	) A
	WHERE R > 1

) B
on A.id = b.id
	and b.val = a.val

select *
from #myTable



	SELECT A.ID
		 , a.val
		 , a.R
	FROM (
	SELECT ID [id]
		, val [val]
		, ROW_NUMBER() OVER(PARTITION BY id	ORDER BY id)  R
	FROM #myTable
	) A
	WHERE R > 1


UPDATE #myTable
set id = R
FROM (
	select A.*
	from (
		SELECT ID [id]
		, val [val]
		, ROW_NUMBER() OVER(PARTITION BY id	ORDER BY id)  R
	FROM #myTable		
	) A
	where R > 1
)




UPDATE A
set id = A.r*-1
FROM (
	select *
	from (
		SELECT ID [id]
		, val [val]
		, ROW_NUMBER() OVER(PARTITION BY id	ORDER BY id)  R
	FROM #myTable		
	) A
	where R > 1
 ) A

 --select * from  #myTable order by ID



/*
DROP TABLE #myTable;

CREATE TABLE #myTable (ID int, Val char(3));

INSERT INTO #myTable(ID, Val) VALUES
    (1, N'ABC'),
    (2, N'ABC'),
    (3, N'ABC'),
    (-1, N'ABC'),
    (4, N'ABC'),
    (5, N'ABC'),
    (-1, N'ABC'),
    (-1, N'ABC'),
    (6, N'ABC'),
    (7, N'ABC'),
    (-1, N'ABC'),
    (-1, N'ABC'),
    (-1, N'ABC'),
    (-1, N'ABC'),
    (-1, N'ABC'),
    (8, N'ABC'),
    (9, N'ABC'),
    (10, N'ABC');

*/


/*

select *
FROM (
select  ID [ID]
	, r * ID [NewVal]
	,ABS(CHECKSUM(newID())) % 100 [newID]
	, r
FROM (
SELECT ROW_NUMBER() OVER(PARTITION BY ID ORDER BY ID) R, *
FROM #myTable
 ) A
 where R > 1
 ) b

where [newID] not in (select id from #myTable)

*/