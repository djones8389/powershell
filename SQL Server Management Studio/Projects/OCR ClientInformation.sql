USE [PRV_OCR_SecureAssess]

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

SELECT centreName [CentreName]
	,E.clientInformation.value('(/clientInformation/systemConfiguration[1]/environment/operatingSystem/version)[1]', 'nvarchar(max)') AS [OS Version]
		,E.clientInformation.value('(/clientInformation/systemConfiguration[1]/environment/operatingSystem/servicePack)[1]', 'nvarchar(max)') AS [OSServicePack]
		,E.clientInformation.value('(/clientInformation/systemConfiguration[1]/identification/computerName)[1]', 'nvarchar(max)') AS [ComputerName]
		,E.clientInformation.value('(/clientInformation/systemConfiguration[1]/identification/macAddress)[1]', 'nvarchar(max)') AS [MACAddress]
		,E.clientInformation.value('(/clientInformation/systemConfiguration[1]/environment/processor/name)[1]', 'nvarchar(max)') AS [CPU]
		,E.clientInformation.value('(/clientInformation/systemConfiguration[1]/environment/processor/cores)[1]', 'tinyint') AS [CPUCores]
		,E.clientInformation.value('(/clientInformation/systemConfiguration[1]/environment/memory/installedPhysicalMemory)[1]', 'nvarchar(max)') AS [Memory]
		,SUBSTRING((
                    SELECT N', '
                            ,(disc.drive.query('.').value('(drive/name)[1]', 'nvarchar(max)')) + N' ' + (disc.drive.query('.').value('(drive/totalSize)[1]', 'nvarchar(max)')) + N' (' + (disc.drive.query('.').value('(drive/freeSpace)[1]', 'nvarchar(max)')) + N' Free)'
                    FROM E.clientInformation.nodes('/clientInformation/systemConfiguration[1]/environment/hardDisk/drives/drive') disc(drive)
                    FOR XML PATH('')
                    ), 3, 1000) AS [Disk(s)]
		,E.clientInformation.value('(/clientInformation/systemConfiguration[1]/environment/video/deviceName)[1]', 'nvarchar(max)') AS [GPU]
		,E.clientInformation.value('(/clientInformation/systemConfiguration[1]/environment/video/memory)[1]', 'nvarchar(max)') AS [GPUMemory]
        ,SUBSTRING((
                    SELECT N', '
                            ,net.ver.value('.', 'nvarchar(max)')
                    FROM E.clientInformation.nodes('/clientInformation/systemConfiguration/dotNet[1]/version') net(ver)
                    FOR XML PATH('')
                    ), 3, 1000) AS [.NET]
FROM dbo.WAREHOUSE_ExamSessionTable E WITH (NOLOCK)
INNER JOIN dbo.WAREHOUSE_ExamSessionTable_Shreded S WITH (NOLOCK) ON E.ID = S.examSessionId
WHERE ClientInformation IS NOT NULL
	AND S.warehouseTime > DATEADD(MONTH, -1, getDate());
              
	