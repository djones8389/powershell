UPDATE     ETL.stg_FactExamSessions
SET        UseForMerge = 0
FROM ETL.stg_FactExamSessions sfes
           LEFT JOIN dbo.DimOriginators do ON do.OriginatorKey = sfes.OriginatorKey
           LEFT JOIN dbo.DimExams de ON de.ExamKey = sfes.ExamKey
           LEFT JOIN dbo.DimQualifications dq ON dq.QualificationKey = sfes.qualificationId
           LEFT JOIN dbo.DimCentres dc ON dc.CentreKey = sfes.CentreKey
           LEFT JOIN dbo.DimExamState des ON des.ExamStateKey = sfes.FinalExamState
           LEFT JOIN dbo.DimExamVersions dev ON dev.ExamVersionKey = sfes.ExamVersionKey
           LEFT JOIN dbo.DimCandidate dca ON dca.CandidateKey = sfes.CandidateKey
WHERE do.OriginatorKey IS NULL
           OR de.ExamKey IS NULL
           OR dq.QualificationKey IS NULL
           OR dc.CentreKey IS NULL
           OR des.ExamStateKey IS NULL
           OR dev.ExamVersionKey IS NULL
           OR dca.CandidateKey IS NULL
           AND sfes.UseForMerge = 1;

INSERT     INTO ETL.FKError
          ( FKColumn,
             FKValue
           )
-- Originator --
           SELECT     'ETL.stg_FactExamSessions.OriginatorKey',
                     sfes.OriginatorKey
           FROM ETL.stg_FactExamSessions sfes
                     LEFT JOIN DimOriginators do ON do.OriginatorKey = sfes.OriginatorKey
           WHERE do.OriginatorKey IS NULL
                     
-- Exam --
           UNION
           SELECT     'ETL.stg_FactExamSessions.ExamKey',
                     sfes.ExamKey
           FROM ETL.stg_FactExamSessions sfes
                     LEFT JOIN DimExams de ON de.ExamKey = sfes.ExamKey
           WHERE de.ExamKey IS NULL
-- Qualification --
           UNION
           SELECT     'ETL.stg_FactExamSessions.QualificationId',
                     sfes.qualificationId
           FROM ETL.stg_FactExamSessions sfes
                     LEFT JOIN dbo.DimQualifications dq ON dq.QualificationKey = sfes.qualificationId
           WHERE dq.QualificationKey IS NULL
-- Centre --
           UNION
           SELECT     'ETL.stg_FactExamSessions.CentreKey',
                     sfes.CentreKey
           FROM ETL.stg_FactExamSessions sfes
                     LEFT JOIN dbo.DimCentres dc ON dc.CentreKey = sfes.CentreKey
           WHERE dc.CentreKey IS NULL
-- Exam State --
           UNION
           SELECT     'ETL.stg_FactExamSessions.FinalExamState',
                     sfes.FinalExamState
           FROM ETL.stg_FactExamSessions sfes
                     LEFT JOIN dbo.DimExamState des ON des.ExamStateKey = sfes.FinalExamState
           WHERE des.ExamStateKey IS NULL
-- Exam Version --
           UNION
           SELECT     'ETL.stg_FactExamSessions.ExamVersionKey',
                     sfes.ExamVersionKey
           FROM ETL.stg_FactExamSessions sfes
                     LEFT JOIN dbo.DimExamVersions dev ON dev.ExamVersionKey = sfes.ExamVersionKey
           WHERE dev.ExamVersionKey IS NULL
-- Candidate --
           UNION
           SELECT     'ETL.stg_FactExamSessions.CandidateKey',
                     sfes.ExamVersionKey
           FROM ETL.stg_FactExamSessions sfes
                     LEFT JOIN dbo.DimCandidate dca ON dca.CandidateKey = sfes.CandidateKey
           WHERE dca.CandidateKey IS NULL;
