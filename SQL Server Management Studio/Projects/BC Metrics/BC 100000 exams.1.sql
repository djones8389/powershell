--SELECT *
--FROM
--(


--select
--	MONTH(uploadDate) MonthOfUpload
--	,YEAR(uploadDate) YearOfUpload
--	,count(ID) CountOfDocuments
--	, warehouseExamSessionID
--from WAREHOUSE_ExamSessionDocumentTable
--group by MONTH(uploadDate),YEAR(uploadDate), warehouseExamSessionID
	

--	) A

--order by 2,1,3




SELECT
	MonthOfUpload
	,YearOfUpload
	,CountOfDocuments
	, CONVERT(BIGINT,MIN([DataLength])) [Min.DocSizePerExam]
	, CONVERT(BIGINT,MAX([DataLength])) [Max.DocSizePerExam]
	, CONVERT(BIGINT,AVG([DataLength])) [Avg.DocSizePerExam]
FROM (

select
	CONVERT(BIGINT,SUM(DATALENGTH(Document)))[DataLength]
	, warehouseExamSessionID
from WAREHOUSE_ExamSessionDocumentTable
group by
	 warehouseExamSessionID
) A

INNER JOIN (


select
	MONTH(uploadDate) MonthOfUpload
	,YEAR(uploadDate) YearOfUpload
	,count(ID) CountOfDocuments
	, warehouseExamSessionID
from WAREHOUSE_ExamSessionDocumentTable
group by MONTH(uploadDate),YEAR(uploadDate), warehouseExamSessionID
	
) B	

ON A.warehouseExamSessionID = B.warehouseExamSessionID

GROUP BY 	MonthOfUpload
			,YearOfUpload
			,CountOfDocuments
ORDER BY YearOfUpload
	, MonthOfUpload
	, CountOfDocuments
	
	