DECLARE @ESIDS TABLE (ESID INT)

INSERT @ESIDS 
select examSessionId
from WAREHOUSE_ExamSessionTable_Shreded
	where examName like '%Speaking%'
		and previousexamstate != 10



SELECT SUM(convert(decimal(8,2),[DL-KB]))/1024 [DL-MB]
	, SUM(convert(decimal(8,2),[DL-KB]))/1024/1024 [DL-GB]
FROM (
select top 20000 [distinct warehouseExamSessionID]
	, SUM(DATALENGTH([Document]))/1024 [DL-KB]
FROM [dbo].[WAREHOUSE_ExamSessionDocumentTable] A
inner join @ESIDS B
on a.[warehouseExamSessionID] = b.esid
 group by [warehouseExamSessionID]
	order by  [warehouseExamSessionID] desc
 ) A