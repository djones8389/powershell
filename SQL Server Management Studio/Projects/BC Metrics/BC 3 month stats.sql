IF OBJECT_ID('tempdb..#BCStats') IS NOT NULL DROP TABLE #BCStats;


CREATE TABLE #BCStats (
	server_name nvarchar(100)
	,database_name nvarchar(100)
	,schema_name nvarchar(100)
	,table_name nvarchar(100)
	,row_count nvarchar(100)
	,reserved_kb int
	,data_kb int
	,index_kb int
	,unused_kb int
	,collection_date datetime
) 

BULK INSERT #BCStats
from 'D:\Users\DaveJ\Documents\SQL Server Management Studio\Projects\BC Metrics\BCData.csv'
with (fieldterminator = ',', rowterminator = '\n')
go


select 
	Date
	,SUM(totalkb)/1024/1024 GB
from (
SELECT 

	SUBSTRING(convert(nvarchar(100),collection_date), 0, 7) as [Date]
	,SUM(data_kb) + SUM(index_kb) [Totalkb]
	, collection_date
FROM #BCStats 
where database_name in (

--'BRITISHCOUNCIL_CPProjectAdmin',
'BritishCouncil_ItemBank',
'BRITISHCOUNCIL_SecureAssess',
--'BRITISHCOUNCIL_SecureMarker',
--'BRITISHCOUNCIL_SurpassDataWarehouse',
'BritishCouncil_TestPackage'
)
 group by collection_date
	
	) A
	group by Date
	
/*58 gb*/

