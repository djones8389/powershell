DECLARE @Orphaned TABLE (
	DBName nvarchar(200)
	, userName nvarchar(100)
)

INSERT @Orphaned
exec sp_MSforeachdb '

use [?];

if (''?'' in (select name from sys.databases where database_id > 4 and STATE_DESC = ''ONLINE''))

select DB_NAME()
	,name
from sysusers
    where issqluser = 1 
    and   (sid is not null and sid <> 0x0)
    and   (len(sid) <= 16)
    and   suser_sname(sid) is null
	and name != ''dbo''
    order by name

'
SELECT 'USE '  + QUOTENAME(DBNAME) + 'DROP USER ' + QUOTENAME(userName) + ';'
FROM @Orphaned 
ORDER BY 1;
