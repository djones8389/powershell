select db_name()
, (
select a.id
		FROM WAREHOUSE_ExamSessionTable AS a
		JOIN WAREHOUSE_ScheduledExamsTable AS b ON a.WAREHOUSEScheduledExamID = b.ID
		JOIN WAREHOUSE_CentreTable AS c ON b.WAREHOUSECentreID = c.ID
		JOIN WAREHOUSE_ExamSessionTable_Shreded AS d ON a.ID = d.examSessionId
		WHERE c.AddressLine1 != d.AddressLine1
		) 
, (
SELECT a.id
		 FROM WAREHOUSE_ExamSessionTable AS a
		JOIN WAREHOUSE_ScheduledExamsTable AS b ON a.WAREHOUSEScheduledExamID = b.ID
		JOIN WAREHOUSE_CentreTable AS c ON b.WAREHOUSECentreID = c.ID
		JOIN WAREHOUSE_ExamSessionTable_Shreded AS d ON a.ID = d.examSessionId
		WHERE c.AddressLine2 != d.AddressLine2
) AddressLine2
,
(
SELECT  a.id
		 FROM WAREHOUSE_ExamSessionTable AS a
		JOIN WAREHOUSE_ScheduledExamsTable AS b ON a.WAREHOUSEScheduledExamID = b.ID
		JOIN WAREHOUSE_CentreTable AS c ON b.WAREHOUSECentreID = c.ID
		JOIN WAREHOUSE_ExamSessionTable_Shreded AS d ON a.ID = d.examSessionId
		WHERE c.County != d.County
) county
, (
SELECT  a.id
		 FROM WAREHOUSE_ExamSessionTable AS a
		JOIN WAREHOUSE_ScheduledExamsTable AS b ON a.WAREHOUSEScheduledExamID = b.ID
		JOIN WAREHOUSE_CentreTable AS c ON b.WAREHOUSECentreID = c.ID
		JOIN WAREHOUSE_ExamSessionTable_Shreded AS d ON a.ID = d.examSessionId
		WHERE c.Country != d.Country
) country
,(
SELECT  a.id
		 FROM WAREHOUSE_ExamSessionTable AS a
		JOIN WAREHOUSE_ScheduledExamsTable AS b ON a.WAREHOUSEScheduledExamID = b.ID
		JOIN WAREHOUSE_CentreTable AS c ON b.WAREHOUSECentreID = c.ID
		JOIN WAREHOUSE_ExamSessionTable_Shreded AS d ON a.ID = d.examSessionId
		WHERE c.Town != d.Town
)Town
,(
SELECT a.id
		 FROM WAREHOUSE_ExamSessionTable AS a
		JOIN WAREHOUSE_ScheduledExamsTable AS b ON a.WAREHOUSEScheduledExamID = b.ID
		JOIN WAREHOUSE_CentreTable AS c ON b.WAREHOUSECentreID = c.ID
		JOIN WAREHOUSE_ExamSessionTable_Shreded AS d ON a.ID = d.examSessionId
		WHERE c.Postcode != d.Postcode
	) Postcode 
,(
SELECT a.id
		 FROM WAREHOUSE_ExamSessionTable AS a
		JOIN WAREHOUSE_ScheduledExamsTable AS b ON a.WAREHOUSEScheduledExamID = b.ID
		JOIN WAREHOUSE_CentreTable AS c ON b.WAREHOUSECentreID = c.ID
		JOIN WAREHOUSE_ExamSessionTable_Shreded AS d ON a.ID = d.examSessionId
		WHERE c.CentreCode != d.CentreCode
) CentreCode
,(
SELECT a.examSessionId
		 FROM WAREHOUSE_ExamSessionTable_Shreded AS a
		JOIN WAREHOUSE_UserTable AS b ON a.WAREHOUSEUserID = b.ID
		WHERE a.ULN != b.ULN
) ULN
,(
SELECT a.examSessionId
		 FROM WAREHOUSE_ExamSessionTable_Shreded AS a
		JOIN WAREHOUSE_UserTable AS b ON a.WAREHOUSEUserID = b.ID
		WHERE a.CandidateRef != b.CandidateRef
		)CandidateRef
,(
SELECT a.examSessionId
		 FROM WAREHOUSE_ExamSessionTable_Shreded AS a
		JOIN WAREHOUSE_UserTable AS b ON a.WAREHOUSEUserID = b.ID
		WHERE a.Gender != b.Gender
) Gender

,(
SELECT a.id
		 FROM WAREHOUSE_ExamSessionTable AS a
		JOIN WAREHOUSE_ExamSessionTable_Shreded AS b ON a.ID = b.examSessionId
		WHERE a.AssignedMarkerUserID != b.AssignedMarkerUserID
		) AssignedMarkerUserID
,(
SELECT a.id
		 FROM WAREHOUSE_ExamSessionTable AS a
		JOIN WAREHOUSE_ExamSessionTable_Shreded AS b ON a.ID = b.examSessionId
		WHERE a.AssignedModeratorUserID != b.AssignedModeratorUserID
		) AssignedModeratorUserID
,(
SELECT a.id
		 FROM WAREHOUSE_ExamSessionTable AS a
		JOIN WAREHOUSE_ExamSessionTable_Shreded AS b ON a.ID = b.examSessionId
		WHERE a.AllowPackageDelivery != b.AllowPackageDelivery
		) AllowPackageDelivery
,(
SELECT  a.id
		 FROM WAREHOUSE_ExamSessionTable AS a
		JOIN WAREHOUSE_ExamSessionTable_Shreded AS b ON a.ID = b.examSessionId
		WHERE a.Appeal != b.Appeal
		) Appeal
,(
SELECT  a.id
		 FROM WAREHOUSE_ExamSessionTable AS a
		JOIN WAREHOUSE_ExamSessionTable_Shreded AS b ON a.ID = b.examSessionId
		WHERE a.CertifiedForTablet != b.CertifiedForTablet
		) CertifiedForTablet
,(
SELECT  a.id
		 FROM WAREHOUSE_ExamSessionTable AS a
		JOIN WAREHOUSE_ExamSessionTable_Shreded AS b ON a.ID = b.examSessionId
		WHERE a.ContainsBTLOffice != b.ContainsBTLOffice
		 ) ContainsBTLOffice
,(
SELECT a.id
from WAREHOUSE_ExamSessionTable AS a
		JOIN WAREHOUSE_ExamSessionTable_Shreded AS b ON a.ID = b.examSessionId
		WHERE a.CQN != b.CQN

		) CQN
,(
SELECT  a.id
		 FROM WAREHOUSE_ExamSessionTable AS a
		JOIN WAREHOUSE_ExamSessionTable_Shreded AS b ON a.ID = b.examSessionId
		WHERE a.EnableOverrideMarking != b.EnableOverrideMarking
		) EnableOverrideMarking
,(
SELECT a.id
		 FROM WAREHOUSE_ExamSessionTable AS a
		JOIN WAREHOUSE_ExamSessionTable_Shreded AS b ON a.ID = b.examSessionId
		WHERE a.ExportedToIntegration != b.ExportedToIntegration
		) ExportedToIntegration
,(
SELECT a.id
		 FROM WAREHOUSE_ExamSessionTable AS a
		JOIN WAREHOUSE_ExamSessionTable_Shreded AS b ON a.ID = b.examSessionId
		WHERE a.ExportToSecureMarker != b.ExportToSecureMarker
		) ExportToSecureMarker
,(
SELECT a.id
		 FROM WAREHOUSE_ExamSessionTable AS a
		JOIN WAREHOUSE_ExamSessionTable_Shreded AS b ON a.ID = b.examSessionId
		WHERE a.IsProjectBased != b.IsProjectBased
		 ) IsProjectBased
,(
SELECT a.id
		 FROM WAREHOUSE_ExamSessionTable AS a
		JOIN WAREHOUSE_ExamSessionTable_Shreded AS b ON a.ID = b.examSessionId
		WHERE a.ItemMarksUploaded != b.ItemMarksUploaded
		) ItemMarksUploaded
,(
SELECT a.id
		 FROM WAREHOUSE_ExamSessionTable AS a
		JOIN WAREHOUSE_ExamSessionTable_Shreded AS b ON a.ID = b.examSessionId
		WHERE a.KeyCode != b.KeyCode
		) KeyCode
,(
SELECT a.id
		 FROM WAREHOUSE_ExamSessionTable AS a
		JOIN WAREHOUSE_ExamSessionTable_Shreded AS b ON a.ID = b.examSessionId
		WHERE a.LocalScanDownloadDate != b.LocalScanDownloadDate
		) LocalScanDownloadDate
,(
SELECT a.id
		 FROM WAREHOUSE_ExamSessionTable AS a
		JOIN WAREHOUSE_ExamSessionTable_Shreded AS b ON a.ID = b.examSessionId
		WHERE a.LocalScanNumPages != b.LocalScanNumPages
		) LocalScanNumPages
,(
SELECT a.id
		 FROM WAREHOUSE_ExamSessionTable AS a
		JOIN WAREHOUSE_ExamSessionTable_Shreded AS b ON a.ID = b.examSessionId
		WHERE a.LocalScanUploadDate != b.LocalScanUploadDate
		 ) LocalScanUploadDate
,(
SELECT a.id
		 FROM WAREHOUSE_ExamSessionTable AS a
		JOIN WAREHOUSE_ExamSessionTable_Shreded AS b ON a.ID = b.examSessionId
		WHERE a.reMarkStatus != b.reMarkStatus
		) reMarkStatus
,(
SELECT a.id
		 FROM WAREHOUSE_ExamSessionTable AS a
		JOIN WAREHOUSE_ExamSessionTable_Shreded AS b ON a.ID = b.examSessionId
		WHERE a.TakenThroughLocalScan != b.TakenThroughLocalScan
) TakenThroughLocalScan
