--26 mins 20 seconds
--est 1 hr on Live.

use [BRITISHCOUNCIL_SecureAssess]

ALTER DATABASE [BRITISHCOUNCIL_SecureAssess]
	ADD FILEGROUP [SECONDARY]
GO

ALTER DATABASE [BRITISHCOUNCIL_SecureAssess]
ADD FILE (

	NAME = BritishCouncil_SecureAssess_WAREHOUSE_ExamSessionItemResponseTable
	, FILENAME = 'O:\Data\BritishCouncil_SecureAssess_WAREHOUSE_ExamSessionItemResponseTable.ndf'
    , SIZE = 80000MB
    , MAXSIZE = UNLIMITED
    , FILEGROWTH = 500MB
)
TO FILEGROUP [Secondary];
GO


CREATE TABLE [dbo].[WAREHOUSE_ExamSessionItemResponseTable_new](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[WAREHOUSEExamSessionID] [int] NOT NULL,
	[ItemID] [nvarchar](15) NOT NULL,
	[ItemVersion] [int] NOT NULL,
	[ItemResponseData] [xml] NULL,
	[MarkerResponseData] [xml] NULL,
	[ItemMark] [int] NULL,
	[MarkingIgnored] [tinyint] NULL,
	[DerivedResponse] [nvarchar](max) NULL,
	[ShortDerivedResponse] [nvarchar](500) NULL
 ) ON [SECONDARY] TEXTIMAGE_ON [SECONDARY]
GO

SET IDENTITY_INSERT [WAREHOUSE_ExamSessionItemResponseTable_new] ON
GO

INSERT [WAREHOUSE_ExamSessionItemResponseTable_new](ID, WAREHOUSEExamSessionID, ItemID, ItemVersion, ItemResponseData, MarkerResponseData, ItemMark, MarkingIgnored, DerivedResponse, ShortDerivedResponse)
SELECT ID, WAREHOUSEExamSessionID, ItemID, ItemVersion, ItemResponseData, MarkerResponseData, ItemMark, MarkingIgnored, DerivedResponse, ShortDerivedResponse
FROM [WAREHOUSE_ExamSessionItemResponseTable]
GO

SET IDENTITY_INSERT [WAREHOUSE_ExamSessionItemResponseTable_new] OFF
GO

DROP TABLE [dbo].[WAREHOUSE_ExamSessionItemResponseTable];
GO

PRINT 'DROP TABLE [dbo].[WAREHOUSE_ExamSessionItemResponseTable]'

EXEC sp_rename 'WAREHOUSE_ExamSessionItemResponseTable_new','WAREHOUSE_ExamSessionItemResponseTable';
GO


/*Constraints*/


ALTER TABLE [dbo].[WAREHOUSE_ExamSessionItemResponseTable]
	ADD CONSTRAINT [PK_WAREHOUSEExamSessionItemResponseTable_1] PRIMARY KEY CLUSTERED (ID)
GO

PRINT 'ADD CONSTRAINT [PK_WAREHOUSEExamSessionItemResponseTable_1] PRIMARY KEY CLUSTERED (ID)'

ALTER TABLE [dbo].[WAREHOUSE_ExamSessionItemResponseTable] ADD  CONSTRAINT [DF_WAREHOUSE_ExamSessionItemResponseTable_MarkingIgnored]  DEFAULT ((0)) FOR [MarkingIgnored]
GO

PRINT 'ADD  CONSTRAINT [DF_WAREHOUSE_ExamSessionItemResponseTable_MarkingIgnored]'

ALTER TABLE [dbo].[WAREHOUSE_ExamSessionItemResponseTable]  WITH CHECK 
ADD  CONSTRAINT [FK_WAREHOUSE_ExamSessionItemResponseTable_WAREHOUSE_ExamSessionTable] FOREIGN KEY([WAREHOUSEExamSessionID])
REFERENCES [dbo].[WAREHOUSE_ExamSessionTable] ([ID])
ON DELETE CASCADE
GO

PRINT 'ADD  CONSTRAINT [FK_WAREHOUSE_ExamSessionItemResponseTable_WAREHOUSE_ExamSessionTable]'

ALTER TABLE [dbo].[WAREHOUSE_ExamSessionItemResponseTable] CHECK CONSTRAINT [FK_WAREHOUSE_ExamSessionItemResponseTable_WAREHOUSE_ExamSessionTable]
GO



/*Indexes*/


CREATE NONCLUSTERED INDEX [IX_WAREHOUSE_ExamSessionItemResponseTable_R11_3PT_script3] ON [dbo].[WAREHOUSE_ExamSessionItemResponseTable]
(
	[WAREHOUSEExamSessionID] ASC
)
INCLUDE ( 	[ItemID],
	[ItemResponseData]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [SECONDARY]
GO


CREATE NONCLUSTERED INDEX [idx_NC_WAREHOUSE_ExamSessionItemResponseTable_WAREHOUSEExamSessionID_ItemID_ItemVersion] ON [dbo].[WAREHOUSE_ExamSessionItemResponseTable]
(
	[WAREHOUSEExamSessionID] ASC,
	[ItemID] ASC,
	[ItemVersion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [SECONDARY]
GO


CREATE NONCLUSTERED INDEX [examSessionId_NCI] ON [dbo].[WAREHOUSE_ExamSessionItemResponseTable]
(
	[WAREHOUSEExamSessionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [SECONDARY]
GO



