USE RCPCH_SurpassDataWarehouse;


--select count(1) 
--	, ExcludeFromReporting
--from FactExamSessions
--group by ExcludeFromReporting


DECLARE @subjects nvarchar(3),@centres nvarchar(707),@dateStartRange nvarchar(19),@dateEndRange nvarchar(19),@exam nvarchar(3),@examVersion nvarchar(1)

SET @subjects=N'130' 
SET @centres=N'47,150,149,181,225,242,267,44,113,116,176,84,226,179,107,151,209,234,66,244,41,143,237,38,245,246,56,67,247,117,35,52,57,203,264,71,42,138,53,108,153,210,152,61,75,54,51,248,63,174,217,236,172,216,235,58,180,142,83,48,188,166,167,40,59,228,45,154,227,144,198,222,114,173,205,239,171,204,240,185,155,211,195,265,158,91,250,251,74,249,177,50,60,197,252,253,78,261,79,80,77,254,37,36,196,81,168,169,170,214,223,255,156,147,194,233,262,256,157,189,187,192,212,243,182,199,202,206,215,46,160,213,159,39,230,257,207,49,193,258,97,55,65,259,125,161,190,224,269,112,232,148,221,184,268,162,183,266,163,263,146,164,231,241,101,73,68,200,238,105,186,201,178,43,119,123,120,127,115,208,122,124,165,260,229,145,69,72,76'
SET @dateStartRange=N'2018-05-01 00:00:00'
SET @dateEndRange=N'2018-05-10 23:59:59'
SET @exam=N'309'
SET @examVersion=N'0'

--SET @subjects=N'130'
--SET @centres=N'47,150,149,181,225,242,267,44,113,116,176,84,226,179,107,151,209,234,66,244,41,143,237,38,245,246,56,67,247,117,35,52,57,203,264,71,42,138,53,108,153,210,152,61,75,54,51,248,63,174,217,236,172,216,235,58,180,142,83,48,188,166,167,40,59,228,45,154,227,144,198,222,114,173,205,239,171,204,240,185,155,211,195,265,158,91,250,251,74,249,177,50,60,197,252,253,78,261,79,80,77,254,37,36,196,81,168,169,170,214,223,255,156,147,194,233,262,256,157,189,187,192,212,243,182,199,202,206,215,46,160,213,159,39,230,257,207,49,193,258,97,55,65,259,125,161,190,224,269,112,232,148,221,184,268,162,183,266,163,263,146,164,231,241,101,73,68,200,238,105,186,201,178,43,119,123,120,127,115,208,122,124,165,260,229,145,69,72,76'
--SET @dateStartRange=N'2018-04-14 00:00:00'
--SET @dateEndRange=N'2018-05-13 23:59:59'
--SET @exam=N'310'
--SET @examVersion=N'0'

DECLARE @StartDateKey int = (SELECT TimeKey FROM DimTime WHERE CAST(FullDateAlternateKey AS DATE)=CAST(@dateStartRange AS DATE))
DECLARE @EndDateKey int =	(SELECT TimeKey FROM DimTime WHERE CAST(FullDateAlternateKey AS DATE)=CAST(@dateEndRange AS DATE))
IF @StartDateKey IS NULL BEGIN
	IF @dateStartRange<=(SELECT MIN(FullDateAlternateKey) FROM DimTime) SELECT @StartDateKey = MIN(TimeKey) FROM DimTime
	ELSE SELECT @StartDateKey = MAX(TimeKey)+1 FROM DimTime
END
IF @EndDateKey IS NULL BEGIN
	IF @dateEndRange<=(SELECT MIN(FullDateAlternateKey) FROM DimTime) SELECT @EndDateKey = MIN(TimeKey)-1 FROM DimTime
	ELSE SELECT @EndDateKey = MAX(TimeKey) FROM DimTime
END

DECLARE @ES TABLE(
	ExamSessionKey int PRIMARY KEY CLUSTERED
	,KeyCode varchar(12)
	,CandidateKey int
	,CentreKey int
	,QualificationKey int
	,ExamKey int
	,ExamVersionKey int
	,UserMarks decimal(9,4)
	,TotalMarksAvailable decimal(9,4)
	,AdjustedGradeKey int
	,CompletionDateTime datetime
	,FinalExamState int
)

INSERT INTO @ES
SELECT 
	FES.ExamSessionKey
	,FES.KeyCode
	,FES.CandidateKey
	,FES.CentreKey
	,FES.QualificationKey
	,@exam ExamKey
	,FES.ExamVersionKey
	,FES.UserMarks
	,FES.TotalMarksAvailable
	,FES.AdjustedGradeKey
	,DT.FullDateAlternateKey + FES.CompletionTime
	,FES.FinalExamState
FROM [dbo].[FactExamSessions] FES
	JOIN DimTime DT ON FES.CompletionDateKey = DT.TimeKey
WHERE 
	FES.CompletionDateKey BETWEEN @StartDateKey AND @EndDateKey
	AND FES.ExcludeFromReporting <> 1
	AND FES.ExamKey = @exam 
	AND (FES.ExamVersionKey = @examVersion OR @examVersion = 0)
	AND FES.QualificationKey IN (
		SELECT Value
		FROM [dbo].[fn_ParamsToList](@subjects,0)
		)
	AND FES.CentreKey IN (
		SELECT Value
		FROM [dbo].[fn_ParamsToList](@centres,0)
		)
OPTION(MAXRECURSION 0)

SELECT count(DISTINCT ExamSessionKey) FROM @ES
			
DECLARE @Items TABLE(
	CPID nvarchar(50)
	,CPVersion int
	,PRIMARY KEY CLUSTERED(CPID,CPVersion)
)		
	
INSERT INTO @Items
SELECT DISTINCT 
	CPID
	,CPVersion
FROM @ES ES
INNER JOIN FactQuestionResponses FQR 
ON FQR.ExamSessionKey = ES.ExamSessionKey

select count(1) from @Items

--SELECT 
--		I.CPID
--		,I.CPVersion
--		 , MarkType
--		,dbo.ag_CLR_Concat(ISNULL(DO.ShortText, DC.AnswerText)+','+CAST(ISNULL(DO.WeightedMark, DC.Mark) AS nvarchar(100)),'|')  [Key]
--	FROM @Items I
--		JOIN DimQuestions DQ
--			ON I.CPID = DQ.CPID
--			AND I.CPVersion = DQ.CPVersion
--		LEFT JOIN DimOptions DO 
--			ON DO.CPID = I.CPID 
--			AND DO.CPVersion = I.CPVersion
--			AND DQ.MarkType=1
--			--AND DO.WeightedMark IS NOT NULL
--		LEFT JOIN DimCombinations DC
--			ON DC.CPID = I.CPID 
--			AND DC.CPVersion = I.CPVersion
--			AND DQ.MarkType=2
--	GROUP BY I.CPID, I.CPVersion,MarkType


;WITH Metadata AS (
	SELECT 
		DQM.CPID
		,DQM.CPVersion
		,attrib
		,attribType
		,attribval
		,externalId
		,deleted
	FROM 
		@Items I
		JOIN DimQuestionMetaData DQM 
			ON I.CPID = DQM.CPID 
			AND I.CPVersion = DQM.CPVersion
	WHERE 
		attribType IS NOT NULL
), ComboKeys AS (
	SELECT 
		I.CPID
		,I.CPVersion
		,dbo.ag_CLR_Concat(ISNULL(DO.ShortText, DC.AnswerText)+','+CAST(ISNULL(DO.WeightedMark, DC.Mark) AS nvarchar(100)),'|')  [Key]
	FROM @Items I
		JOIN DimQuestions DQ
			ON I.CPID = DQ.CPID
			AND I.CPVersion = DQ.CPVersion
		LEFT JOIN DimOptions DO 
			ON DO.CPID = I.CPID 
			AND DO.CPVersion = I.CPVersion
			AND DQ.MarkType=1
			--AND DO.WeightedMark IS NOT NULL
		LEFT JOIN DimCombinations DC
			ON DC.CPID = I.CPID 
			AND DC.CPVersion = I.CPVersion
			AND DQ.MarkType=2
	WHERE MarkType IN (1,2)
	GROUP BY I.CPID, I.CPVersion
)

SELECT 
	1 [Tag]
	,NULL [Parent]
	,NULL [report!1]
	,NULL [items!2]
	,NULL [i!3!CPID]
	,NULL [i!3!CPV]
	,NULL [i!3!CAID]
	,NULL [i!3!nam]
	,NULL [i!3!tMk]
	,NULL [i!3!key]
	,NULL [i!3!quT]
	,NULL [i!3!mkT]
	,NULL [md!4]
	,NULL [t!5!nam]	
	,NULL [t!5!typ]
	,NULL [t!5!id]
	,NULL [t!5!del]
	,NULL [v!6]
	,NULL [e!20]
	,NULL [ei!21!CPID]
	,NULL [ei!21!ID]
	,NULL [ei!21!nam]
	,NULL [ei!21!isS]
	,NULL [tags!40]
	,NULL [t!41!id]
	,NULL [t!41!nm]
	,NULL [v!42!id]
	,NULL [v!42!nm]
	,NULL [sessions!7]
	,NULL [s!8!id]
	,NULL [s!8!dtC]
	,NULL [s!8!kCd]
	,NULL [s!8!fnm]
	,NULL [s!8!snm]
	,NULL [s!8!cRf]
	,NULL [s!8!sex]
	,NULL [s!8!DOB]
	,NULL [s!8!sub]
	,NULL [s!8!cnt]
	,NULL [s!8!tst]
	,NULL [s!8!tsF]
	,NULL [s!8!aMk]
	,NULL [s!8!tMk]
	,NULL [s!8!grd]
	,NULL [s!8!iVd]
	,NULL [r!9]
	,NULL [i!10!CPID]
	,NULL [i!10!CPV]
	,NULL [i!10!aMk]
	,NULL [i!10!rsp]
	,NULL [i!10!vTm]
	,NULL [i!10!prO]
	,NULL [i!10!sc]
	,NULL [ct!30]
	,NULL [t!31!id]
	,NULL [t!31!vid]

UNION ALL

SELECT 
	2 [Tag]
	,1 [Parent]
	,NULL [report!1]
	,NULL [items!2]
	,NULL [item!3!CPID]
	,NULL [item!3!CPVersion]
	,NULL [item!3!CAID]
	,NULL [item!3!name]
	,NULL [item!3!totalMark]
	,NULL [item!3!key]
	,NULL [item!3!questionType]
	,NULL [i!3!markType]
	,NULL [metadata!4]
	,NULL [tag!5!name]	
	,NULL [tag!5!type]
	,NULL [tag!5!id]
	,NULL [tag!5!deleted]
	,NULL [value!6]
	,NULL [e!20]
	,NULL [ei!21!CPID]
	,NULL [ei!21!ID]
	,NULL [ei!21!nam]
	,NULL [ei!21!isS]
	,NULL [tags!40]
	,NULL [t!41!id]
	,NULL [t!41!nm]
	,NULL [v!42!id]
	,NULL [v!42!nm]
	,NULL [sessions!7]
	,NULL [examSession!8!id]
	,NULL [examSession!8!dateCompleted]
	,NULL [examSession!8!keyCode]
	,NULL [examSession!8!forename]
	,NULL [examSession!8!surname]
	,NULL [examSession!8!candidateRef]
	,NULL [examSession!8!gender]
	,NULL [examSession!8!DOB]
	,NULL [examSession!8!subject]
	,NULL [examSession!8!centre]
	,NULL [examSession!8!test]
	,NULL [examSession!8!testForm]
	,NULL [examSession!8!actualMark]
	,NULL [examSession!8!totalMark]
	,NULL [examSession!8!grade]
	,NULL [examSession!8!isVoided]
	,NULL [responses!9]
	,NULL [item!10!CPID]
	,NULL [item!10!CPVersion]
	,NULL [item!10!actualMark]
	,NULL [item!10!response]
	,NULL [item!10!viewingTime]
	,NULL [item!10!presentedOrder]
	,NULL [i!10!sc]
	,NULL [ct!30]
	,NULL [t!31]
	,NULL [v!32]

UNION ALL

SELECT 
	40 [Tag]
	,1 [Parent]
	,NULL [report!1]
	,NULL [items!2]
	,'y' [item!3!CPID]
	,NULL [item!3!CPVersion]
	,NULL [item!3!CAID]
	,NULL [item!3!name]
	,NULL [item!3!totalMark]
	,NULL [item!3!key]
	,NULL [item!3!questionType]
	,NULL [i!3!markType]
	,NULL [metadata!4]
	,NULL [tag!5!name]	
	,NULL [tag!5!type]
	,NULL [tag!5!id]
	,NULL [tag!5!deleted]
	,NULL [value!6]
	,NULL [e!20]
	,NULL [ei!21!CPID]
	,NULL [ei!21!ID]
	,NULL [ei!21!nam]
	,NULL [ei!21!isS]
	,NULL [tags!40]
	,NULL [t!41!id]
	,NULL [t!41!nm]
	,NULL [v!42!id]
	,NULL [v!42!nm]
	,NULL [sessions!7]
	,NULL [examSession!8!id]
	,NULL [examSession!8!dateCompleted]
	,NULL [examSession!8!keyCode]
	,NULL [examSession!8!forename]
	,NULL [examSession!8!surname]
	,NULL [examSession!8!candidateRef]
	,NULL [examSession!8!gender]
	,NULL [examSession!8!DOB]
	,NULL [examSession!8!subject]
	,NULL [examSession!8!centre]
	,NULL [examSession!8!test]
	,NULL [examSession!8!testForm]
	,NULL [examSession!8!actualMark]
	,NULL [examSession!8!totalMark]
	,NULL [examSession!8!grade]
	,NULL [examSession!8!isVoided]
	,NULL [responses!9]
	,NULL [item!10!CPID]
	,NULL [item!10!CPVersion]
	,NULL [item!10!actualMark]
	,NULL [item!10!response]
	,NULL [item!10!viewingTime]
	,NULL [item!10!presentedOrder]
	,NULL [i!10!sc]
	,NULL [ct!30]
	,NULL [t!31]
	,NULL [v!32]

UNION ALL

SELECT 
	41 [Tag]
	,40 [Parent]
	,NULL [report!1]
	,NULL [items!2]
	,'y' [item!3!CPID]
	,NULL [item!3!CPVersion]
	,NULL [item!3!CAID]
	,NULL [item!3!name]
	,NULL [item!3!totalMark]
	,NULL [item!3!key]
	,NULL [item!3!questionType]
	,NULL [i!3!markType]
	,NULL [metadata!4]
	,NULL [tag!5!name]	
	,NULL [tag!5!type]
	,NULL [tag!5!id]
	,NULL [tag!5!deleted]
	,NULL [value!6]
	,NULL [e!20]
	,NULL [ei!21!CPID]
	,NULL [ei!21!ID]
	,NULL [ei!21!nam]
	,NULL [ei!21!isS]
	,NULL [tags!40]
	,TT.TagTypeKey [t!41!id]
	,TT.TagTypeName [t!41!nm]
	,NULL [v!42!id]
	,NULL [v!42!nm]
	,NULL [sessions!7]
	,NULL [examSession!8!id]
	,NULL [examSession!8!dateCompleted]
	,NULL [examSession!8!keyCode]
	,NULL [examSession!8!forename]
	,NULL [examSession!8!surname]
	,NULL [examSession!8!candidateRef]
	,NULL [examSession!8!gender]
	,NULL [examSession!8!DOB]
	,NULL [examSession!8!subject]
	,NULL [examSession!8!centre]
	,NULL [examSession!8!test]
	,NULL [examSession!8!testForm]
	,NULL [examSession!8!actualMark]
	,NULL [examSession!8!totalMark]
	,NULL [examSession!8!grade]
	,NULL [examSession!8!isVoided]
	,NULL [responses!9]
	,NULL [item!10!CPID]
	,NULL [item!10!CPVersion]
	,NULL [item!10!actualMark]
	,NULL [item!10!response]
	,NULL [item!10!viewingTime]
	,NULL [item!10!presentedOrder]
	,NULL [i!10!sc]
	,NULL [ct!30]
	,NULL [t!31]
	,NULL [v!32]

FROM 
	TagTypes TT 
WHERE EXISTS(
	SELECT * 
	FROM 
		@ES ES 
		JOIN CandidateTagValues CTV ON CTV.CandidateKey = ES.CandidateKey
		JOIN TagValues TV ON CTV.TagValueKey = TV.TagValueKey
	WHERE 
		TV.TagTypeKey = TT.TagTypeKey
)

UNION ALL

SELECT 
	42 [Tag]
	,41 [Parent]
	,NULL [report!1]
	,NULL [items!2]
	,'y' [item!3!CPID]
	,NULL [item!3!CPVersion]
	,NULL [item!3!CAID]
	,NULL [item!3!name]
	,NULL [item!3!totalMark]
	,NULL [item!3!key]
	,NULL [item!3!questionType]
	,NULL [i!3!markType]
	,NULL [metadata!4]
	,NULL [tag!5!name]	
	,NULL [tag!5!type]
	,NULL [tag!5!id]
	,NULL [tag!5!deleted]
	,NULL [value!6]
	,NULL [e!20]
	,NULL [ei!21!CPID]
	,NULL [ei!21!ID]
	,NULL [ei!21!nam]
	,NULL [ei!21!isS]
	,NULL [tags!40]
	,TV.TagTypeKey [t!41!id]
	,NULL [t!41!nm]
	,TV.TagValueKey [v!42!id]
	,TV.[Text] [v!42!nm]
	,NULL [sessions!7]
	,NULL [examSession!8!id]
	,NULL [examSession!8!dateCompleted]
	,NULL [examSession!8!keyCode]
	,NULL [examSession!8!forename]
	,NULL [examSession!8!surname]
	,NULL [examSession!8!candidateRef]
	,NULL [examSession!8!gender]
	,NULL [examSession!8!DOB]
	,NULL [examSession!8!subject]
	,NULL [examSession!8!centre]
	,NULL [examSession!8!test]
	,NULL [examSession!8!testForm]
	,NULL [examSession!8!actualMark]
	,NULL [examSession!8!totalMark]
	,NULL [examSession!8!grade]
	,NULL [examSession!8!isVoided]
	,NULL [responses!9]
	,NULL [item!10!CPID]
	,NULL [item!10!CPVersion]
	,NULL [item!10!actualMark]
	,NULL [item!10!response]
	,NULL [item!10!viewingTime]
	,NULL [item!10!presentedOrder]
	,NULL [i!10!sc]
	,NULL [ct!30]
	,NULL [t!31]
	,NULL [v!32]

FROM 
	TagValues TV 
WHERE EXISTS(
	SELECT * 
	FROM 
		@ES ES 
		JOIN CandidateTagValues CTV ON CTV.CandidateKey = ES.CandidateKey
	WHERE 
		CTV.TagValueKey = TV.TagValueKey
)

UNION ALL

SELECT 
	3 [Tag]
	,2 [Parent]
	,NULL [report!1]
	,NULL [items!2]
	,I.CPID
	,I.CPVersion
	,CASE WHEN DQ.CAQuestionTypeKey IS NOT NULL THEN DQ.ExternalId ELSE NULL END CAID
	,DQ.QuestionName [Name]
	,CAST(DQ.TotalMark AS decimal(9,4)) [Total Mark]
	,CASE ISNULL(DQ.MarkType, 0)
		WHEN 0 THEN 
			CONVERT(NVARCHAR(4000), ISNULL(
			CASE DQ.QuestionTypeKey
				WHEN 11 THEN dbo.fn_CLR_StripHTML(DQ.DerivedCorrectAnswer)
				ELSE DQ.ShortDerivedCorrectAnswer
			END, ''))
		ELSE ISNULL(CK.[Key],'')
	END [Key]
	,DQ.CAQuestionTypeKey QuestionType 
	,CASE DQ.MarkType
		WHEN 1 THEN 'Weighted'
		WHEN 2 THEN 'Combination'
		ELSE 'Standard'
	END [i!3!markType]
	,NULL [metadata!4]
	,NULL [tag!5!name]	
	,NULL [tag!5!type]
	,NULL [tag!5!id]
	,NULL [tag!5!deleted]
	,NULL [value!6]
	,NULL [e!20]
	,NULL [ei!21!CPID]
	,NULL [ei!21!ID]
	,NULL [ei!21!nam]
	,NULL [ei!21!isS]
	,NULL [tags!40]
	,NULL [t!41!id]
	,NULL [t!41!nm]
	,NULL [v!42!id]
	,NULL [v!42!nm]
	,NULL [sessions!7]
	,NULL [examSession!8!id]
	,NULL [examSession!8!dateCompleted]
	,NULL [examSession!8!keyCode]
	,NULL [examSession!8!forename]
	,NULL [examSession!8!surname]
	,NULL [examSession!8!candidateRef]
	,NULL [examSession!8!gender]
	,NULL [examSession!8!DOB]
	,NULL [examSession!8!subject]
	,NULL [examSession!8!centre]
	,NULL [examSession!8!test]
	,NULL [examSession!8!testForm]
	,NULL [examSession!8!actualMark]
	,NULL [examSession!8!totalMark]
	,NULL [examSession!8!grade]
	,NULL [examSession!8!isVoided]
	,NULL [responses!9]
	,NULL [item!10!CPID]
	,NULL [item!10!CPVersion]
	,NULL [item!10!actualMark]
	,NULL [item!10!response]
	,NULL [item!10!viewingTime]
	,NULL [item!10!presentedOrder]
	,NULL [i!10!sc]
	,NULL [ct!30]
	,NULL [t!31]
	,NULL [v!32]
FROM @Items I
	JOIN DimQuestions DQ ON I.CPVersion = DQ.CPVersion AND I.CPID = DQ.CPID
	LEFT JOIN ComboKeys CK ON I.CPVersion = CK.CPVersion AND I.CPID = CK.CPID

UNION ALL

SELECT 
	4 [Tag]
	,3 [Parent]
	,NULL [report!1]
	,NULL [items!2]
	,CPID [item!3!CPID]
	,CPVersion [item!3!CPV]
	,NULL [item!3!CAID]
	,NULL [item!3!name]
	,NULL [item!3!tMk]
	,NULL [item!3!key]
	,NULL [item!3!quT]
	,NULL [i!3!markType]
	,NULL [metadata!4]
	,NULL [tag!5!name]	
	,NULL [tag!5!type]
	,NULL [tag!5!id]
	,NULL [tag!5!deleted]
	,NULL [value!6]
	,NULL [e!20]
	,NULL [ei!21!CPID]
	,NULL [ei!21!ID]
	,NULL [ei!21!nam]
	,NULL [ei!21!isS]
	,NULL [tags!40]
	,NULL [t!41!id]
	,NULL [t!41!nm]
	,NULL [v!42!id]
	,NULL [v!42!nm]
	,NULL [sessions!7]--7
	,NULL [examSession!8!id]--8
	,NULL [examSession!8!dtC]
	,NULL [examSession!8!kCd]
	,NULL [examSession!8!fnm]
	,NULL [examSession!8!snm]
	,NULL [examSession!8!cRf]
	,NULL [examSession!8!sex]
	,NULL [examSession!8!DOB]
	,NULL [examSession!8!sub]
	,NULL [examSession!8!cnt]
	,NULL [examSession!8!tst]
	,NULL [examSession!8!tsF]
	,NULL [examSession!8!aMk]
	,NULL [examSession!8!tMk]
	,NULL [examSession!8!grd]
	,NULL [examSession!8!iVd]
	,NULL [responses!9]--9
	,NULL [item!10!CPID]--10
	,NULL [item!10!CPV]
	,NULL [item!10!aMk]
	,NULL [item!10!rsp]
	,NULL [item!10!vTm]
	,NULL [item!10!prO]
	,NULL [i!10!sc]
	,NULL [ct!30]
	,NULL [t!31]
	,NULL [v!32]
FROM @Items

UNION ALL

SELECT DISTINCT
	5 [Tag]
	,4 [Parent]
	,NULL [report!1]
	,NULL [items!2]
	,CPID [item!3!CPID]
	,CPVersion [item!3!CPV]
	,NULL [item!3!CAID]
	,NULL [item!3!name]
	,NULL [item!3!tMk]
	,NULL [item!3!key]
	,NULL [item!3!quT]
	,NULL [i!3!markType]
	,NULL [metadata!4]
	,attrib [tag!5!name]
	,attribType [tag!5!type]
	,externalId [tag!5!id]
	,deleted [tag!5!deleted]
	,NULL [value!6]
	,NULL [e!20]
	,NULL [ei!21!CPID]
	,NULL [ei!21!ID]
	,NULL [ei!21!nam]
	,NULL [ei!21!isS]
	,NULL [tags!40]
	,NULL [t!41!id]
	,NULL [t!41!nm]
	,NULL [v!42!id]
	,NULL [v!42!nm]
	,NULL [sessions!7]--7
	,NULL [examSession!8!id]--8
	,NULL [examSession!8!dtC]
	,NULL [examSession!8!kCd]
	,NULL [examSession!8!fnm]
	,NULL [examSession!8!snm]
	,NULL [examSession!8!cRf]
	,NULL [examSession!8!sex]
	,NULL [examSession!8!DOB]
	,NULL [examSession!8!sub]
	,NULL [examSession!8!cnt]
	,NULL [examSession!8!tst]
	,NULL [examSession!8!tsF]
	,NULL [examSession!8!aMk]
	,NULL [examSession!8!tMk]
	,NULL [examSession!8!grd]
	,NULL [examSession!8!iVd]
	,NULL [responses!9]--9
	,NULL [item!10!CPID]--10
	,NULL [item!10!CPV]
	,NULL [item!10!aMk]
	,NULL [item!10!rsp]
	,NULL [item!10!vTm]
	,NULL [item!10!prO]
	,NULL [i!10!sc]
	,NULL [ct!30]
	,NULL [t!31]
	,NULL [v!32]
FROM Metadata

--Enemy items
UNION ALL

SELECT 
	20 [Tag]
	,3 [Parent]
	,NULL [report!1]
	,NULL [items!2]
	,CPID [item!3!CPID]
	,CPVersion [item!3!CPV]
	,NULL [item!3!CAID]
	,NULL [item!3!name]
	,NULL [item!3!tMk]
	,NULL [item!3!key]
	,NULL [item!3!quT]
	,NULL [i!3!markType]
	,NULL [metadata!4]
	,NULL [tag!5!name]	
	,NULL [tag!5!type]
	,NULL [tag!5!id]
	,NULL [tag!5!deleted]
	,NULL [value!6]
	,'' [e!20]
	,NULL [ei!21!CPID]
	,NULL [ei!21!ID]
	,NULL [ei!21!nam]
	,NULL [ei!21!isS]
	,NULL [tags!40]
	,NULL [t!41!id]
	,NULL [t!41!nm]
	,NULL [v!42!id]
	,NULL [v!42!nm]
	,NULL [sessions!7]--7
	,NULL [examSession!8!id]--8
	,NULL [examSession!8!dtC]
	,NULL [examSession!8!kCd]
	,NULL [examSession!8!fnm]
	,NULL [examSession!8!snm]
	,NULL [examSession!8!cRf]
	,NULL [examSession!8!sex]
	,NULL [examSession!8!DOB]
	,NULL [examSession!8!sub]
	,NULL [examSession!8!cnt]
	,NULL [examSession!8!tst]
	,NULL [examSession!8!tsF]
	,NULL [examSession!8!aMk]
	,NULL [examSession!8!tMk]
	,NULL [examSession!8!grd]
	,NULL [examSession!8!iVd]
	,NULL [responses!9]--9
	,NULL [item!10!CPID]--10
	,NULL [item!10!CPV]
	,NULL [item!10!aMk]
	,NULL [item!10!rsp]
	,NULL [item!10!vTm]
	,NULL [item!10!prO]
	,NULL [i!10!sc]
	,NULL [ct!30]
	,NULL [t!31]
	,NULL [v!32]
FROM @Items

UNION ALL

SELECT DISTINCT
	21 [Tag]
	,20 [Parent]
	,NULL [report!1]
	,NULL [items!2]
	,I.CPID [item!3!CPID]
	,I.CPVersion [item!3!CPV]
	,NULL [item!3!CAID]
	,NULL [item!3!name]
	,NULL [item!3!tMk]
	,NULL [item!3!key]
	,NULL [item!3!quT]
	,NULL [i!3!markType]
	,NULL [metadata!4]
	,NULL [tag!5!name]
	,NULL [tag!5!type]
	,NULL [tag!5!id]
	,NULL [tag!5!deleted]
	,NULL [value!6]
	,'' [e!20]
	,DEQ.EnemyCPID [ei!21!CPID]
	,DQ.ItemKey [ei!21!ID]
	,DQ.QuestionName [ei!21!nam]
	,DEQ.IsSectionEnemy [ei!21!isS]
	,NULL [tags!40]
	,NULL [t!41!id]
	,NULL [t!41!nm]
	,NULL [v!42!id]
	,NULL [v!42!nm]
	,NULL [sessions!7]--7
	,NULL [examSession!8!id]--8
	,NULL [examSession!8!dtC]
	,NULL [examSession!8!kCd]
	,NULL [examSession!8!fnm]
	,NULL [examSession!8!snm]
	,NULL [examSession!8!cRf]
	,NULL [examSession!8!sex]
	,NULL [examSession!8!DOB]
	,NULL [examSession!8!sub]
	,NULL [examSession!8!cnt]
	,NULL [examSession!8!tst]
	,NULL [examSession!8!tsF]
	,NULL [examSession!8!aMk]
	,NULL [examSession!8!tMk]
	,NULL [examSession!8!grd]
	,NULL [examSession!8!iVd]
	,NULL [responses!9]--9
	,NULL [item!10!CPID]--10
	,NULL [item!10!CPV]
	,NULL [item!10!aMk]
	,NULL [item!10!rsp]
	,NULL [item!10!vTm]
	,NULL [item!10!prO]
	,NULL [i!10!sc]
	,NULL [ct!30]
	,NULL [t!31]
	,NULL [v!32]
FROM 
	@Items I
	JOIN DimEnemyQuestions DEQ ON I.CPID = DEQ.CPID
	JOIN DimQuestions DQ ON DQ.CPID = DEQ.EnemyCPID AND DQ.CPVersion = DEQ.EnemyCurrentVersion
UNION ALL

SELECT DISTINCT
	21 [Tag]
	,20 [Parent]
	,NULL [report!1]
	,NULL [items!2]
	,I.CPID [item!3!CPID]
	,I.CPVersion [item!3!CPV]
	,NULL [item!3!CAID]
	,NULL [item!3!name]
	,NULL [item!3!tMk]
	,NULL [item!3!key]
	,NULL [item!3!quT]
	,NULL [i!3!markType]
	,NULL [metadata!4]
	,NULL [tag!5!name]
	,NULL [tag!5!type]
	,NULL [tag!5!id]
	,NULL [tag!5!deleted]
	,NULL [value!6]
	,'' [e!20]
	,DEQ.CPID [ei!21!CPID]
	,DQ.ItemKey [ei!21!ID]
	,DQ.QuestionName [ei!21!nam]
	,DEQ.IsSectionEnemy [ei!21!isS]
	,NULL [tags!40]
	,NULL [t!41!id]
	,NULL [t!41!nm]
	,NULL [v!42!id]
	,NULL [v!42!nm]
	,NULL [sessions!7]--7
	,NULL [examSession!8!id]--8
	,NULL [examSession!8!dtC]
	,NULL [examSession!8!kCd]
	,NULL [examSession!8!fnm]
	,NULL [examSession!8!snm]
	,NULL [examSession!8!cRf]
	,NULL [examSession!8!sex]
	,NULL [examSession!8!DOB]
	,NULL [examSession!8!sub]
	,NULL [examSession!8!cnt]
	,NULL [examSession!8!tst]
	,NULL [examSession!8!tsF]
	,NULL [examSession!8!aMk]
	,NULL [examSession!8!tMk]
	,NULL [examSession!8!grd]
	,NULL [examSession!8!iVd]
	,NULL [responses!9]--9
	,NULL [item!10!CPID]--10
	,NULL [item!10!CPV]
	,NULL [item!10!aMk]
	,NULL [item!10!rsp]
	,NULL [item!10!vTm]
	,NULL [item!10!prO]
	,NULL [i!10!sc]
	,NULL [ct!30]
	,NULL [t!31]
	,NULL [v!32]
FROM 
	@Items I
	JOIN DimEnemyQuestions DEQ ON I.CPID = DEQ.EnemyCPID
	JOIN DimQuestions DQ ON DQ.CPID = DEQ.CPID AND DQ.CPVersion = DEQ.CurrentVersion
UNION ALL

SELECT 
	6 [Tag]
	,5 [Parent]
	,NULL [report!1]
	,NULL [items!2]
	,CPID [item!3!CPID]
	,CPVersion [item!3!CPV]
	,NULL [item!3!CAID]
	,NULL [item!3!name]
	,NULL [item!3!tMk]
	,NULL [item!3!key]
	,NULL [item!3!quT]
	,NULL [i!3!markType]
	,NULL [metadata!4]
	,attrib [tag!5!name]
	,NULL [tag!5!type]
	,NULL [tag!5!id]
	,NULL [tag!5!deleted]
	,attribval [value!6]
	,NULL [e!20]
	,NULL [ei!21!CPID]
	,NULL [ei!21!ID]
	,NULL [ei!21!nam]
	,NULL [ei!21!isS]
	,NULL [tags!40]
	,NULL [t!41!id]
	,NULL [t!41!nm]
	,NULL [v!42!id]
	,NULL [v!42!nm]
	,NULL [sessions!7]--7
	,NULL [examSession!8!id]--8
	,NULL [examSession!8!dtC]
	,NULL [examSession!8!kCd]
	,NULL [examSession!8!fnm]
	,NULL [examSession!8!snm]
	,NULL [examSession!8!cRf]
	,NULL [examSession!8!sex]
	,NULL [examSession!8!DOB]
	,NULL [examSession!8!sub]
	,NULL [examSession!8!cnt]
	,NULL [examSession!8!tst]
	,NULL [examSession!8!tsF]
	,NULL [examSession!8!aMk]
	,NULL [examSession!8!tMk]
	,NULL [examSession!8!grd]
	,NULL [examSession!8!iVd]
	,NULL [responses!9]--9
	,NULL [item!10!CPID]--10
	,NULL [item!10!CPV]
	,NULL [item!10!aMk]
	,NULL [item!10!rsp]
	,NULL [item!10!vTm]
	,NULL [item!10!prO]
	,NULL [i!10!sc]
	,NULL [ct!30]
	,NULL [t!31]
	,NULL [v!32]
FROM Metadata

UNION ALL

SELECT 
	7 [Tag]
	,1 [Parent]
	,NULL [report!1]
	,NULL [items!2]
	,'z' [item!3!CPID]
	,NULL [item!3!CPVersion]
	,NULL [item!3!CAID]
	,NULL [item!3!name]
	,NULL [item!3!totalMark]
	,NULL [item!3!key]
	,NULL [item!3!questionType]
	,NULL [i!3!markType]
	,NULL [metadata!4]
	,NULL [tag!5!name]	
	,NULL [tag!5!type]
	,NULL [tag!5!id]
	,NULL [tag!5!deleted]
	,NULL [value!6]
	,NULL [e!20]
	,NULL [ei!21!CPID]
	,NULL [ei!21!ID]
	,NULL [ei!21!nam]
	,NULL [ei!21!isS]
	,NULL [tags!40]
	,NULL [t!41!id]
	,NULL [t!41!nm]
	,NULL [v!42!id]
	,NULL [v!42!nm]
	,NULL [sessions!7]
	,NULL [examSession!8!id]
	,NULL [examSession!8!dateCompleted]
	,NULL [examSession!8!keyCode]
	,NULL [examSession!8!forename]
	,NULL [examSession!8!surname]
	,NULL [examSession!8!candidateRef]
	,NULL [examSession!8!gender]
	,NULL [examSession!8!DOB]
	,NULL [examSession!8!subject]
	,NULL [examSession!8!centre]
	,NULL [examSession!8!test]
	,NULL [examSession!8!testForm]
	,NULL [examSession!8!actualMark]
	,NULL [examSession!8!totalMark]
	,NULL [examSession!8!grade]
	,NULL [examSession!8!isVoided]
	,NULL [responses!9]
	,NULL [item!10!CPID]
	,NULL [item!10!CPVersion]
	,NULL [item!10!actualMark]
	,NULL [item!10!response]
	,NULL [item!10!viewingTime]
	,NULL [item!10!presentedOrder]
	,NULL [i!10!sc]
	,NULL [ct!30]
	,NULL [t!31]
	,NULL [v!32]

UNION ALL

SELECT 
	8 [Tag]
	,7 [Parent]
	,NULL [report!1]
	,NULL [items!2]
	,NULL [item!3!CPID]
	,NULL [item!3!CPVersion]
	,NULL [item!3!CAID]
	,NULL [item!3!name]
	,NULL [item!3!totalMark]
	,NULL [item!3!key]
	,NULL [item!3!questionType]
	,NULL [i!3!markType]
	,NULL [metadata!4]
	,NULL [tag!5!name]	
	,NULL [tag!5!type]
	,NULL [tag!5!id]
	,NULL [tag!5!deleted]
	,NULL [value!6]
	,NULL [e!20]
	,NULL [ei!21!CPID]
	,NULL [ei!21!ID]
	,NULL [ei!21!nam]
	,NULL [ei!21!isS]
	,NULL [tags!40]
	,NULL [t!41!id]
	,NULL [t!41!nm]
	,NULL [v!42!id]
	,NULL [v!42!nm]
	,NULL [sessions!7]
	,FES.ExamSessionKey 
	,FES.CompletionDateTime 
	,FES.KeyCode 
	,DC.Forename 
	,DC.Surname 
	,DC.CandidateRef 
	,DC.Gender 
	,DC.DOB 
	,DQu.QualificationName 
	,DCe.CentreName 
	,DE.ExamName 
	,DEV.ExamVersionName 
	,CAST(FES.UserMarks AS decimal(9,4))
	,CAST(FES.TotalMarksAvailable AS decimal(9,4))
	,DG.Grade
	,CASE FinalExamState WHEN 10 THEN 1 ELSE 0 END [examSession!2!isVoided]
	,NULL [responses!9]
	,NULL [item!10!CPID]
	,NULL [item!10!CPVersion]
	,NULL [item!10!actualMark]
	,NULL [item!10!response]
	,NULL [item!10!viewingTime]
	,NULL [item!10!presentedOrder]
	,NULL [i!10!sc]
	,NULL [ct!30]
	,NULL [t!31]
	,NULL [v!32]
FROM @ES FES
	LEFT JOIN [dbo].[DimCandidate] DC ON DC.CandidateKey = FES.CandidateKey
	LEFT JOIN [dbo].[DimQualifications] DQu ON DQu.QualificationKey = FES.QualificationKey
	LEFT JOIN [dbo].[DimCentres] DCe ON DCe.CentreKey = FES.CentreKey
	LEFT JOIN [dbo].[DimExams] DE ON DE.ExamKey = FES.ExamKey
	LEFT JOIN [dbo].[DimExamVersions] DEV ON DEV.ExamKey = FES.ExamKey
		AND DEV.ExamVersionKey = FES.ExamVersionKey
	LEFT JOIN [dbo].[DimGrades] DG ON DG.GradeKey = FES.AdjustedGradeKey

UNION ALL

SELECT 
	9 [Tag]
	,8 [Parent]
	,NULL [report!1]
	,NULL [items!2]
	,NULL [item!3!CPID]
	,NULL [item!3!CPVersion]
	,NULL [item!3!CAID]
	,NULL [item!3!name]
	,NULL [item!3!totalMark]
	,NULL [item!3!key]
	,NULL [item!3!questionType]
	,NULL [i!3!markType]
	,NULL [metadata!4]
	,NULL [tag!5!name]	
	,NULL [tag!5!type]
	,NULL [tag!5!id]
	,NULL [tag!5!deleted]
	,NULL [value!6]
	,NULL [e!20]
	,NULL [ei!21!CPID]
	,NULL [ei!21!ID]
	,NULL [ei!21!nam]
	,NULL [ei!21!isS]
	,NULL [tags!40]
	,NULL [t!41!id]
	,NULL [t!41!nm]
	,NULL [v!42!id]
	,NULL [v!42!nm]
	,NULL [sessions!7]
	,ExamSessionKey [examSession!8!id]
	,NULL [examSession!8!dateCompleted]
	,NULL [examSession!8!keyCode]
	,NULL [examSession!8!forename]
	,NULL [examSession!8!surname]
	,NULL [examSession!8!candidateRef]
	,NULL [examSession!8!gender]
	,NULL [examSession!8!DOB]
	,NULL [examSession!8!subject]
	,NULL [examSession!8!centre]
	,NULL [examSession!8!test]
	,NULL [examSession!8!testForm]
	,NULL [examSession!8!actualMark]
	,NULL [examSession!8!totalMark]
	,NULL [examSession!8!grade]
	,NULL [examSession!8!isVoided]
	,NULL [responses!9]
	,NULL [item!10!CPID]
	,NULL [item!10!CPVersion]
	,NULL [item!10!actualMark]
	,NULL [item!10!response]
	,NULL [item!10!viewingTime]
	,NULL [item!10!presentedOrder]
	,NULL [i!10!sc]
	,NULL [ct!30]
	,NULL [t!31]
	,NULL [v!32]

FROM @ES FES

UNION ALL

SELECT 
	10 [Tag]
	,9 [Parent]
	,NULL [report!1]
	,NULL [items!2]
	,NULL [item!3!CPID]
	,NULL [item!3!CPVersion]
	,NULL [item!3!CAID]
	,NULL [item!3!name]
	,NULL [item!3!totalMark]
	,NULL [item!3!key]
	,NULL [item!3!questionType]
	,NULL [i!3!markType]
	,NULL [metadata!4]
	,NULL [tag!5!name]	
	,NULL [tag!5!type]
	,NULL [tag!5!id]
	,NULL [tag!5!deleted]
	,NULL [value!6]
	,NULL [e!20]
	,NULL [ei!21!CPID]
	,NULL [ei!21!ID]
	,NULL [ei!21!nam]
	,NULL [ei!21!isS]
	,NULL [tags!40]
	,NULL [t!41!id]
	,NULL [t!41!nm]
	,NULL [v!42!id]
	,NULL [v!42!nm]
	,NULL [sessions!7]
	,ES.ExamSessionKey [examSession!8!id]
	,NULL [examSession!8!dateCompleted]
	,NULL [examSession!8!keyCode]
	,NULL [examSession!8!forename]
	,NULL [examSession!8!surname]
	,NULL [examSession!8!candidateRef]
	,NULL [examSession!8!gender]
	,NULL [examSession!8!DOB]
	,NULL [examSession!8!subject]
	,NULL [examSession!8!centre]
	,NULL [examSession!8!test]
	,NULL [examSession!8!testForm]
	,NULL [examSession!8!actualMark]
	,NULL [examSession!8!totalMark]
	,NULL [examSession!8!grade]
	,NULL [examSession!8!isVoided]
	,NULL [responses!9]
	,FQR.CPID [item!10!CPID]
	,FQR.CPVersion [item!10!CPVersion]
	,CAST(FQR.Mark*DQ.TotalMark AS decimal(9,4)) [item!10!actualMark]
	,ISNULL(CASE QuestionTypeKey
				WHEN 11 THEN REPLACE(dbo.fn_CLR_StripHTML(FQR.DerivedResponse),N'#!#',' ')
				ELSE FQR.ShortDerivedResponse
				END, '') [item!10!response]
	,FQR.ViewingTime/1000 [item!10!viewingTime]
	,FQR.ItemPresentationOrder [item!10!presentedOrder]
	,FQR.Scored [i!10!sc]
	,NULL [ct!30]
	,NULL [t!31]
	,NULL [v!32]
FROM 
	@ES ES
	JOIN FactQuestionResponses FQR ON FQR.ExamSessionKey = ES.ExamSessionKey
	JOIN DimQuestions DQ ON DQ.CPID = FQR.CPID AND DQ.CPVersion = FQR.CPVersion

UNION ALL

SELECT 
	30 [Tag]
	,8 [Parent]
	,NULL [report!1]
	,NULL [items!2]
	,NULL [item!3!CPID]
	,NULL [item!3!CPVersion]
	,NULL [item!3!CAID]
	,NULL [item!3!name]
	,NULL [item!3!totalMark]
	,NULL [item!3!key]
	,NULL [item!3!questionType]
	,NULL [i!3!markType]
	,NULL [metadata!4]
	,NULL [tag!5!name]	
	,NULL [tag!5!type]
	,NULL [tag!5!id]
	,NULL [tag!5!deleted]
	,NULL [value!6]
	,NULL [e!20]
	,NULL [ei!21!CPID]
	,NULL [ei!21!ID]
	,NULL [ei!21!nam]
	,NULL [ei!21!isS]
	,NULL [tags!40]
	,NULL [t!41!id]
	,NULL [t!41!nm]
	,NULL [v!42!id]
	,NULL [v!42!nm]
	,NULL [sessions!7]
	,ExamSessionKey [examSession!8!id]
	,NULL [examSession!8!dateCompleted]
	,NULL [examSession!8!keyCode]
	,NULL [examSession!8!forename]
	,NULL [examSession!8!surname]
	,NULL [examSession!8!candidateRef]
	,NULL [examSession!8!gender]
	,NULL [examSession!8!DOB]
	,NULL [examSession!8!subject]
	,NULL [examSession!8!centre]
	,NULL [examSession!8!test]
	,NULL [examSession!8!testForm]
	,NULL [examSession!8!actualMark]
	,NULL [examSession!8!totalMark]
	,NULL [examSession!8!grade]
	,NULL [examSession!8!isVoided]
	,NULL [responses!9]
	,NULL [item!10!CPID]
	,NULL [item!10!CPVersion]
	,NULL [item!10!actualMark]
	,NULL [item!10!response]
	,NULL [item!10!viewingTime]
	,NULL [item!10!presentedOrder]
	,NULL [i!10!sc]
	,'' [ct!30]
	,NULL [t!31!nm]
	,NULL [t!31!v]

FROM @ES FES

UNION ALL

SELECT 
	31 [Tag]
	,30 [Parent]
	,NULL [report!1]
	,NULL [items!2]
	,NULL [item!3!CPID]
	,NULL [item!3!CPVersion]
	,NULL [item!3!CAID]
	,NULL [item!3!name]
	,NULL [item!3!totalMark]
	,NULL [item!3!key]
	,NULL [item!3!questionType]
	,NULL [i!3!markType]
	,NULL [metadata!4]
	,NULL [tag!5!name]	
	,NULL [tag!5!type]
	,NULL [tag!5!id]
	,NULL [tag!5!deleted]
	,NULL [value!6]
	,NULL [e!20]
	,NULL [ei!21!CPID]
	,NULL [ei!21!ID]
	,NULL [ei!21!nam]
	,NULL [ei!21!isS]
	,NULL [tags!40]
	,NULL [t!41!id]
	,NULL [t!41!nm]
	,NULL [v!42!id]
	,NULL [v!42!nm]
	,NULL [sessions!7]
	,ExamSessionKey [examSession!8!id]
	,NULL [examSession!8!dateCompleted]
	,NULL [examSession!8!keyCode]
	,NULL [examSession!8!forename]
	,NULL [examSession!8!surname]
	,NULL [examSession!8!candidateRef]
	,NULL [examSession!8!gender]
	,NULL [examSession!8!DOB]
	,NULL [examSession!8!subject]
	,NULL [examSession!8!centre]
	,NULL [examSession!8!test]
	,NULL [examSession!8!testForm]
	,NULL [examSession!8!actualMark]
	,NULL [examSession!8!totalMark]
	,NULL [examSession!8!grade]
	,NULL [examSession!8!isVoided]
	,NULL [responses!9]
	,NULL [item!10!CPID]
	,NULL [item!10!CPVersion]
	,NULL [item!10!actualMark]
	,NULL [item!10!response]
	,NULL [item!10!viewingTime]
	,NULL [item!10!presentedOrder]
	,NULL [i!10!sc]
	,'' [ct!30]
	,TV.TagTypeKey [t!31!tid]
	,TV.TagValueKey [t!31!vid]

FROM 
	@ES FES
	JOIN CandidateTagValues CTV ON CTV.CandidateKey = FES.CandidateKey
	JOIN TagValues TV ON TV.TagValueKey = CTV.TagValueKey
ORDER BY  [s!8!id],[i!3!CPID],[t!41!id],[i!3!CPV],[e!20],[t!5!nam], [tag]
FOR XML EXPLICIT


