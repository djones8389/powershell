SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

SELECT A.PageID
	, Version
	, checkIntime
FROM (
SELECT 
	ROW_NUMBER() OVER (PARTITION BY PageID ORDER BY  PageID, Version, checkinTime) R
	, PageID
	, Version
	, checkIntime
FROM (
SELECT convert(nvarchar(3),[ProjectID]) + 'P' + convert(nvarchar(10),[ItemRef]) [PageID]
      ,[Version]
	  ,[ManifestXML].value('data(manifest/checkIntime)[1]','datetime') [checkIntime]
  FROM [PPD_AAT_ItemBank].[dbo].[ItemTable]
  where ProjectID = 930
  ) A

) A 
where R = 1
	order by 1,2,3;