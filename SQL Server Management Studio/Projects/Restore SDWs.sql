RESTORE DATABASE [PRV_BCGuilds_SurpassDataWarehouse] 
	FROM DISK = N'T:\FTP\BCGUILDS\BCGuilds.SDW.2016.02.11.bak' WITH FILE = 1, NOUNLOAD, NOREWIND, REPLACE
		, MEDIAPASSWORD = '', STATS = 10, MOVE 'BCGuilds_SurpassDataWarehouse' TO N'S:\DATA\PRV_BCGuilds_SurpassDataWarehouse.mdf'
		, MOVE 'BCGuilds_SurpassDataWarehouse_log' TO N'L:\LOGS\PRV_BCGuilds_SurpassDataWarehouse.ldf';

RESTORE DATABASE [PRV_BRITISHCOUNCIL_SurpassDataWarehouse] 
	FROM DISK = N'T:\FTP\BC\BC.SDW.2016.02.12.bak' WITH FILE = 1, NOUNLOAD, NOREWIND, REPLACE, MEDIAPASSWORD = ''
		, STATS = 10, MOVE 'BRITISHCOUNCIL_SurpassDataWarehouse' TO N'S:\DATA\PRV_BRITISHCOUNCIL_SurpassDataWarehouse.mdf'
			, MOVE 'BRITISHCOUNCIL_SurpassDataWarehouse_log' TO N'L:\LOGS\PRV_BRITISHCOUNCIL_SurpassDataWarehouse.ldf';

RESTORE DATABASE [PRV_AAT_SurpassDataWarehouse] FROM DISK = N'T:\Backup\PRV.AAT.ALL.2016.02.08.bak'
	 WITH FILE = 4, NOUNLOAD, NOREWIND, REPLACE, MEDIAPASSWORD = '', STATS = 10
		, MOVE 'AAT_SurpassAnalytics' TO N'E:\DATA\PRV_AAT_SurpassDataWarehouse.mdf'
		, MOVE 'AAT_SurpassAnalytics_log' TO N'L:\LOGS\PRV_AAT_SurpassDataWarehouse.ldf';