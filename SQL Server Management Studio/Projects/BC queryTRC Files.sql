USE [SPDurationMetrics]

SELECT 
	ObjectName
	,COUNT(ObjectName) [No. of Executions]
	,AVG(Duration)/1000 [Avg duration (ms)]
	,MIN(Duration)/1000 [Min duration (ms)]
	,MAX(Duration)/1000 [Max duration (ms)]
from [dbo].[BCTraceFiles]
group by ObjectName
order by [Avg duration (ms)] desc
