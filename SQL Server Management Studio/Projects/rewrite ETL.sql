UPDATE [dbo].[DimQuestions]
SET [FacilityValue] = FV.FacilityValue
FROM [dbo].[DimQuestions]
LEFT JOIN (
	SELECT DQ.CPID
		,SUM(COALESCE(Mark, 0)) SumMark
		,SUM(COALESCE(DQ.TotalMark, 0)) SumTotalMark
		,CASE 
			WHEN SUM(COALESCE(DQ.TotalMark, 0)) = 0
				THEN 0
			ELSE SUM(COALESCE(Mark, 0)) / SUM(COALESCE(DQ.TotalMark, 0))
			END FacilityValue
	FROM dbo.DimQuestions DQ
	LEFT JOIN dbo.FactQuestionResponses FQR ON DQ.CPID = FQR.CPID
	GROUP BY DQ.CPID
	) FV ON [dbo].[DimQuestions].CPID = FV.CPID


	--if there's a marker mark then use it.




--	SurpassFacilityValue = ISNULL (fv.FacilityValue,0.5)

SELECT A.CPID


FROM (
	SELECT DQ.CPID
		,SUM(COALESCE(FQR.Mark, 0)) SumMark
		,SUM(COALESCE(DQ.TotalMark, 0)) SumTotalMark
		,CASE 
			WHEN SUM(COALESCE(DQ.TotalMark, 0)) = 0
				THEN 0
			ELSE SUM(COALESCE(FQR.Mark, 0)) / SUM(COALESCE(DQ.TotalMark, 0))
			END FacilityValue
			,dq.TotalMark
	FROM dbo.DimQuestions DQ
	LEFT JOIN dbo.FactQuestionResponses FQR ON DQ.CPID = FQR.CPID
	GROUP BY DQ.CPID
 )  A
 LEFT JOIN FactMarkerResponse FMR
 ON FMR.CPID = A.CPID
 GROUP BY A.CPID



 	--if there's a marker mark then use it.


 SELECT A.CPID
 --, a.FMRmark 
 , SUM(COALESCE(FMRmark, FQRMark)) / SUM(COALESCE(DQ.TotalMark, 0))
 FROM (
 	SELECT DQ.CPID
		, CASE WHEN SUM(COALESCE(DQ.TotalMark, 0)) = 0
				THEN 0
				ELSE SUM(COALESCE(fmr.AssignedMark, 0))/ SUM(COALESCE(DQ.TotalMark, 0))
				END AS FMRmark
	FROM dbo.DimQuestions DQ
	LEFT JOIN FactMarkerResponse FMR
	on fmr.CPID = dq.CPID
	group by  DQ.CPID
) A

LEFT JOIN dbo.FactQuestionResponses FQR
ON FQR.CPID = A.CPID






SELECT
	CPID
	, TotalMark
INTO #DQ
FROM [dbo].[DimQuestions]



--UPDATE [DimQuestions]
--SET SurpassFacilityValue = ISNULL(fv.FacilityValue, 0.5)





--UPDATE dbo.DimQuestions
--SET FacilityValue = ISNULL(FV, 0.5)
SELECT b.*
FROM DimQuestions DC
INNER JOIN (

SELECT COALESCE(FMRmark.CPID, FQRmark.CPID)  CPID
	, COALESCE(FMRmark.FMRmark, FQRmark.FQRmark) FV
FROM (
 	SELECT DQ.CPID
		, CASE WHEN SUM(COALESCE(DQ.TotalMark, 0)) = 0
				THEN 0
				ELSE SUM(COALESCE(FMR.AssignedMark, 0))/ SUM(COALESCE(DQ.TotalMark, 0))
				END AS FMRmark
	FROM #DQ DQ
	LEFT JOIN FactMarkerResponse FMR
	on fmr.CPID = DQ.CPID
	group by  DQ.CPID
) FMRmark
LEFT JOIN (
	--SELECT FQRmark.*
	--FROM (
 		SELECT DQ.CPID
			, CASE WHEN SUM(COALESCE(DQ.TotalMark, 0)) = 0
					THEN 0
					ELSE SUM(COALESCE(FQR.mark, 0))/ SUM(COALESCE(DQ.TotalMark, 0))
					END AS FQRmark
		FROM #DQ DQ
		LEFT JOIN FactQuestionResponses FQR
		on FQR.CPID = DQ.CPID
		group by  DQ.CPID
	--) FQRmark
) FQRmark
on FMRmark.CPID = FQRmark.CPID
) b
--) B 
on B.CPID = DC.CPID