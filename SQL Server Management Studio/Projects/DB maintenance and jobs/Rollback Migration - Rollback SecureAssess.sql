USE msdb;
GO

EXEC dbo.sp_add_job
    @job_name = N'Rollback-Migration - Rollback SecureAssess' ;
GO


DECLARE @RestoreSecureAssess nvarchar(MAX) = '';

SELECT @RestoreSecureAssess = 'DECLARE @RestoreSecureAssess nvarchar(MAX) = '''';

SELECT @RestoreSecureAssess +=CHAR(13) +  ''RESTORE DATABASE [''+ Name +''] FROM [''+ ''SecureAssess'' +''] WITH FILE = 1, REPLACE, NOUNLOAD, NOREWIND; ''
from sys.databases
where name like ''%SecureAssess''
EXEC(@RestoreSecureAssess)

'

EXEC sp_add_jobstep
    @job_name = N'Rollback-Migration - Rollback SecureAssess',
    @step_name = N'Rollback SecureAssess',
    @subsystem = N'TSQL',
    @database_name = N'msdb',
    @on_success_action = 3,
    @on_fail_action = 2,
    @command = @RestoreSecureAssess , 
    @retry_attempts = 0,
    @retry_interval = 0;
GO


DECLARE @SetRecovery nvarchar(MAX);

SELECT @SetRecovery = 'DECLARE @SetRecovery nvarchar(MAX) = '''';

SELECT @SetRecovery +=CHAR(13) +  ''ALTER DATABASE [''+ Name +''] SET RECOVERY FULL WITH NO_WAIT;''
from sys.databases
where name like ''%SecureAssess''
EXEC(@SetRecovery)

'

EXEC sp_add_jobstep
    @job_name = N'Rollback-Migration - Rollback SecureAssess',
    @step_name = N'Set Recovery to FULL',
    @subsystem = N'TSQL',
    @database_name = N'msdb',
    @on_success_action = 3,
    @on_fail_action = 2,
    @command = @SetRecovery, 
    @retry_attempts = 0,
    @retry_interval = 0;
GO





DECLARE @EnableAlerts nvarchar(MAX);
SELECT @EnableAlerts = 'DECLARE @EnableAlerts nvarchar(MAX) = '''';

select @EnableAlerts += CHAR(13) + ''EXECUTE msdb.dbo.sp_update_alert @name = N''''''+ name+'''''',@enabled = 1; ''
from dbo.sysalerts

EXEC(@EnableAlerts)
'

EXEC sp_add_jobstep
    @job_name = N'Rollback-Migration - Rollback SecureAssess',
    @step_name = N'Enable Alerts',
    @subsystem = N'TSQL',
    @database_name = N'msdb',
    @on_success_action = 3,
    @on_fail_action = 2,
    @command = @EnableAlerts, 
    @retry_attempts = 0,
    @retry_interval = 0;
GO

DECLARE @EnableMaintenance nvarchar(MAX);
SELECT @EnableMaintenance = 'DECLARE @EnableMaintenance nvarchar(MAX) = '''';

select @EnableMaintenance+= CHAR(13) + ''EXECUTE msdb.dbo.sp_update_job @job_name = N''''''+ name+'''''',@enabled = 1; ''
from dbo.sysjobs
where name like ''%maintenance%''
	or name like ''%etl%''
EXEC(@EnableMaintenance)
'

EXEC sp_add_jobstep
    @job_name = N'Rollback-Migration - Rollback SecureAssess',
    @step_name = N'Enable Maintenance',
    @subsystem = N'TSQL',
    @database_name = N'msdb',
    @on_success_action = 1,
    @on_fail_action = 2,
    @command = @EnableMaintenance, 
    @retry_attempts = 0,
    @retry_interval = 0;
GO



EXEC dbo.sp_add_jobserver
    @job_name = N'Rollback-Migration - Rollback SecureAssess';
GO