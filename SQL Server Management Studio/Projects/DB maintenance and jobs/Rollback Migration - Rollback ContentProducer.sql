USE msdb;
GO

EXEC dbo.sp_add_job
    @job_name = N'Rollback-Migration - Rollback ContentProducer' ;
GO


DECLARE @RestoreContentProducer nvarchar(MAX) = '';

SELECT @RestoreContentProducer = 'DECLARE @RestoreContentProducer nvarchar(MAX) = '''';

SELECT @RestoreContentProducer +=CHAR(13) +  ''RESTORE DATABASE [''+ Name +''] FROM [''+ ''ContentProducer'' +''] WITH FILE = 1, REPLACE, NOUNLOAD, NOREWIND; ''
from sys.databases
where name like ''%ContentProducer'' or name like ''%CPProjectAdmin''
EXEC(@RestoreContentProducer)

'

EXEC sp_add_jobstep
    @job_name = N'Rollback-Migration - Rollback ContentProducer',
    @step_name = N'Rollback ContentProducer',
    @subsystem = N'TSQL',
    @database_name = N'msdb',
    @on_success_action = 3,
    @on_fail_action = 2,
    @command = @RestoreContentProducer , 
    @retry_attempts = 0,
    @retry_interval = 0;
GO

DECLARE @EnableAlerts nvarchar(MAX);
SELECT @EnableAlerts = 'DECLARE @EnableAlerts nvarchar(MAX) = '''';

select @EnableAlerts += CHAR(13) + ''EXECUTE msdb.dbo.sp_update_alert @name = N''''''+ name+'''''',@enabled = 1; ''
from dbo.sysalerts

EXEC(@EnableAlerts)
'

EXEC sp_add_jobstep
    @job_name = N'Rollback-Migration - Rollback ContentProducer',
    @step_name = N'Enable Alerts',
    @subsystem = N'TSQL',
    @database_name = N'msdb',
    @on_success_action = 3,
    @on_fail_action = 2,
    @command = @EnableAlerts, 
    @retry_attempts = 0,
    @retry_interval = 0;
GO

DECLARE @EnableMaintenance nvarchar(MAX);
SELECT @EnableMaintenance = 'DECLARE @EnableMaintenance nvarchar(MAX) = '''';

select @EnableMaintenance+= CHAR(13) + ''EXECUTE msdb.dbo.sp_update_job @job_name = N''''''+ name+'''''',@enabled = 1; ''
from dbo.sysjobs
where name like ''%maintenance%''
	or name like ''%etl%''
EXEC(@EnableMaintenance)
'

EXEC sp_add_jobstep
    @job_name = N'Rollback-Migration - Rollback ContentProducer',
    @step_name = N'Enable Maintenance',
    @subsystem = N'TSQL',
    @database_name = N'msdb',
    @on_success_action = 1,
    @on_fail_action = 2,
    @command = @EnableMaintenance, 
    @retry_attempts = 0,
    @retry_interval = 0;
GO



EXEC dbo.sp_add_jobserver
    @job_name = N'Rollback-Migration - Rollback ContentProducer';
GO