USE msdb;
GO

EXEC dbo.sp_add_job
    @job_name = N'Post-Migration' ;
GO


DECLARE @SetRecovery nvarchar(MAX);

SELECT @SetRecovery = 'DECLARE @SetRecovery nvarchar(MAX) = '''';

SELECT @SetRecovery +=CHAR(13) +  ''ALTER DATABASE [''+ Name +''] SET RECOVERY FULL WITH NO_WAIT;''
from sys.databases
where name like ''%SecureAssess''

EXEC(@SetRecovery)

'

EXEC sp_add_jobstep
    @job_name = N'Post-Migration',
    @step_name = N'Set Recovery to FULL',
    @subsystem = N'TSQL',
    @database_name = N'master',
    @on_success_action = 3,
    @on_fail_action = 2,
    @command = @SetRecovery, 
    @retry_attempts = 0,
    @retry_interval = 0;
GO


DECLARE @EnableAlerts nvarchar(MAX);
SELECT @EnableAlerts = 'DECLARE @EnableAlerts nvarchar(MAX) = '''';

select @EnableAlerts += CHAR(13) + ''EXECUTE msdb.dbo.sp_update_alert @name = N''''''+ name+'''''',@enabled = 1; ''
from dbo.sysalerts

EXEC(@EnableAlerts)
'

EXEC sp_add_jobstep
    @job_name = N'Post-Migration',
    @step_name = N'Enable Alerts',
    @subsystem = N'TSQL',
    @database_name = N'msdb',
    @on_success_action = 3,
    @on_fail_action = 2,
    @command = @EnableAlerts, 
    @retry_attempts = 0,
    @retry_interval = 0;
GO

DECLARE @EnableMaintenance nvarchar(MAX);
SELECT @EnableMaintenance = 'DECLARE @EnableMaintenance nvarchar(MAX) = '''';

select @EnableMaintenance+= CHAR(13) + ''EXECUTE msdb.dbo.sp_update_job @job_name = N''''''+ name+'''''',@enabled = 1; ''
from dbo.sysjobs
where name like ''%maintenance%''
	or name like ''%etl%''
EXEC(@EnableMaintenance)
'

EXEC sp_add_jobstep
    @job_name = N'Post-Migration',
    @step_name = N'Enable Maintenance',
    @subsystem = N'TSQL',
    @database_name = N'msdb',
    @on_success_action = 3,
    @on_fail_action = 2,
    @command = @EnableMaintenance, 
    @retry_attempts = 0,
    @retry_interval = 0;
GO

DECLARE @Indexes nvarchar (MAX);

SELECT @Indexes = 'exec sp_msforeachdb ''

	SET QUOTED_IDENTIFIER ON;
	USE [?];
	IF DB_ID() <> 4
	BEGIN
	DECLARE IndexMaintenance CURSOR FOR
		SELECT	 ''''ALTER INDEX '''' + QUOTENAME(ix.name) + '''' ON '''' + QUOTENAME(DB_NAME(ix_phy.database_id)) + ''''.''''+ QUOTENAME(OBJECT_SCHEMA_NAME(ix.object_id, ix_phy.database_id)) +''''.'''' + QUOTENAME(OBJECT_NAME(ix.object_id)) + '''' '''' +
				 CASE
					WHEN avg_fragmentation_in_percent BETWEEN 5 AND 30 THEN ''''REORGANIZE''''
					ELSE ''''REBUILD''''
				 END + '''';''''
			FROM	 sys.dm_db_index_physical_stats(DB_ID(''''?''''), NULL, NULL, NULL, NULL) ix_phy
			INNER JOIN sys.indexes ix
					ON ix_phy.object_id = ix.object_id
				AND ix_phy.index_id = ix.index_id
		WHERE	 avg_fragmentation_in_percent > 5
		 AND	 ix.index_id <> 0
		ORDER BY database_id;
	DECLARE @SQL NVARCHAR(1000);
	OPEN IndexMaintenance;
	FETCH NEXT FROM IndexMaintenance INTO @SQL;
	WHILE @@FETCH_STATUS = 0
	BEGIN
		EXECUTE(@SQL);
		FETCH NEXT FROM IndexMaintenance INTO @SQL;
	END
	CLOSE IndexMaintenance;
	DEALLOCATE IndexMaintenance;

''

'

EXEC sp_add_jobstep
    @job_name = N'Post-Migration',
    @step_name = N'Rebuild/Reorganize Indexes',
    @subsystem = N'TSQL',
    @database_name = N'msdb',
    @on_success_action = 3,
    @on_fail_action = 2,
    @command = @Indexes, 
    @retry_attempts = 0,
    @retry_interval = 0;
GO


DECLARE @UpdateStats nvarchar(MAX) = '';

SELECT @UpdateStats = 'EXECUTE sp_MSforeachdb N''USE [?]; EXECUTE sp_updatestats;'';'


EXEC sp_add_jobstep
    @job_name = N'Post-Migration',
    @step_name = N'Update Statistics',
    @subsystem = N'TSQL',
    --@database_user_name = 'sa',
    @database_name = N'master',
    @on_success_action = 1,
    @on_fail_action = 2,
    @command = @UpdateStats, 
    @retry_attempts = 0,
    @retry_interval = 0;
GO


EXEC dbo.sp_add_jobserver
    @job_name = N'Post-Migration';
GO