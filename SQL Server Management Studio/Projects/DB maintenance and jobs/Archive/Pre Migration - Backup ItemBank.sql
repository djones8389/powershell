USE msdb;
GO

EXEC dbo.sp_add_job
    @job_name = N'Pre-Migration - Backup ItemBank' ;
GO

DECLARE @BackupItemBank nvarchar(MAX);
SELECT @BackupItemBank = 'DECLARE @BackupItemBank nvarchar(MAX) = '''';

DECLARE @BackupLocation nvarchar(MAX) = N''C:\Users\DaveJ\Desktop\BackupTest'';

SELECT @BackupItemBank +=CHAR(13) +  ''BACKUP DATABASE [''+ Name +''] TO DISK ='''''' + @BackupLocation + ''\'' + Name + '''' +  CONVERT(VARCHAR(10), GETDATE(), 102) + ''.bak'''' WITH COPY_ONLY, COMPRESSION, NOFORMAT, NOINIT, SKIP, COMPRESSION, NOREWIND, NOUNLOAD; ''
from sys.databases
where name like ''%ItemBank%''

EXEC(@BackupItemBank)

'

EXEC sp_add_jobstep
    @job_name = N'Pre-Migration - Backup ItemBank',
    @step_name = N'Backup ItemBank',
    @subsystem = N'TSQL',
    @database_name = N'msdb',
    @on_success_action = 1,
    @on_fail_action = 2,
    @command = @BackupItemBank, 
    @retry_attempts = 0,
    @retry_interval = 0;
GO

EXEC dbo.sp_add_jobserver
    @job_name = N'Pre-Migration - Backup ItemBank';
GO