USE msdb;
GO

EXEC dbo.sp_add_job
    @job_name = N'Pre-Migration - Backup SurpassDataWarehouse' ;
GO


DECLARE @CreateCPBackupDevice nvarchar(MAX)= '';

SELECT @CreateCPBackupDevice = '

DECLARE @BackupDefault TABLE (Data nvarchar(max))
INSERT @BackupDefault
VALUES (''E:\Backup\Pre-Migration'')

--DECLARE @BackupDefault TABLE (Value nvarchar(30), Data nvarchar(max))
--INSERT @BackupDefault
--EXEC  master.dbo.xp_instance_regread 
--N''HKEY_LOCAL_MACHINE'', N''Software\Microsoft\MSSQLServer\MSSQLServer'',N''BackupDirectory''


DECLARE @BackupToDefaultLoc nvarchar(MAX) = '''';

SELECT @BackupToDefaultLoc += CHAR(13) + ''

IF NOT EXISTS (SELECT 1 FROM sys.backup_devices where Name = N''''SurpassDataWarehouse'''')

EXEC master.dbo.sp_addumpdevice  @devtype = N''''disk'''', @logicalname = N''''SurpassDataWarehouse'''', @physicalname ='''''' + Data + ''\'' + ''Pre-Migration-SurpassDataWarehouse.bak'''''' 
FROM @BackupDefault

EXEC(@BackupToDefaultLoc)


'

EXEC sp_add_jobstep
    @job_name = N'Pre-Migration - Backup SurpassDataWarehouse',
    @step_name = N'Create Backup Device',
    @subsystem = N'TSQL',
    @database_name = N'msdb',
    @on_success_action = 3,
    @on_fail_action = 2,
    @command = @CreateCPBackupDevice, 
    @retry_attempts = 0,
    @retry_interval = 0;
GO


DECLARE @BackupSurpassDataWarehouse nvarchar(MAX) = '';

SELECT @BackupSurpassDataWarehouse = 'DECLARE @BackupSurpassDataWarehouse nvarchar(MAX) = '''';

SELECT @BackupSurpassDataWarehouse +=CHAR(13) +  ''BACKUP DATABASE [''+ Name +''] TO [''+ ''SurpassDataWarehouse'' +''] WITH COPY_ONLY, COMPRESSION, NOFORMAT, NOINIT, SKIP, NOREWIND, NOUNLOAD; ''
from sys.databases
where name like ''%SurpassDataWarehouse%'' or name like ''%Analytics%''
EXEC(@BackupSurpassDataWarehouse)

'

EXEC sp_add_jobstep
    @job_name = N'Pre-Migration - Backup SurpassDataWarehouse',
    @step_name = N'Backup SurpassDataWarehouse to Device',
    @subsystem = N'TSQL',
    @database_name = N'msdb',
    @on_success_action = 1,
    @on_fail_action = 2,
    @command = @BackupSurpassDataWarehouse, 
    @retry_attempts = 0,
    @retry_interval = 0;
GO

EXEC dbo.sp_add_jobserver
    @job_name = N'Pre-Migration - Backup SurpassDataWarehouse';
GO