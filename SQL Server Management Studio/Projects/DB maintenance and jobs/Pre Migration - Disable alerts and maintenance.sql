USE msdb;
GO

EXEC dbo.sp_add_job
    @job_name = N'Pre-Migration' ;
GO

DECLARE @DisableAlerts nvarchar(MAX);
SELECT @DisableAlerts = 'DECLARE @DisableAlerts nvarchar(MAX) = '''';

select @DisableAlerts += CHAR(13) + ''EXECUTE msdb.dbo.sp_update_alert @name = N''''''+ name+'''''',@enabled = 0; ''
from dbo.sysalerts

EXEC(@DisableAlerts)
'

EXEC sp_add_jobstep
    @job_name = N'Pre-Migration',
    @step_name = N'Disable Alerts',
    @subsystem = N'TSQL',
    @database_name = N'msdb',
    @on_success_action = 3,
    @on_fail_action = 2,
    @command = @DisableAlerts, 
    @retry_attempts = 0,
    @retry_interval = 0;
GO

DECLARE @DisableMaintenance nvarchar(MAX);
SELECT @DisableMaintenance = 'DECLARE @DisableMaintenance nvarchar(MAX) = '''';

select @DisableMaintenance+= CHAR(13) + ''EXECUTE msdb.dbo.sp_update_job @job_name = N''''''+ name+'''''',@enabled = 0; ''
from dbo.sysjobs
where name like ''%maintenance%''
	or name like ''%etl%''
EXEC(@DisableMaintenance)
'


EXEC sp_add_jobstep
    @job_name = N'Pre-Migration',
    @step_name = N'Disable Maintenance',
    @subsystem = N'TSQL',
    @database_name = N'msdb',
    @on_success_action = 1,
    @on_fail_action = 2,
    @command = @DisableMaintenance, 
    @retry_attempts = 0,
    @retry_interval = 0;
GO


EXEC dbo.sp_add_jobserver
    @job_name = N'Pre-Migration';
GO

