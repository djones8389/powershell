1)  Compression enabled by default.  Ensure version of SQL can handle it
2)  Set the backup location manually (instead of using server defaults)
3)  Ensure there's only one of each DB - using wildcards at the moment, e.g. %SecureAssess%