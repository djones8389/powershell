use AQA_SecureAssess

select id, warehouseTime,ExportedToIntegration
--into [whtimerevert]
from WAREHOUSE_ExamSessionTable
where completiondate > '2017-11-09'
	and PreviousExamState <> 10

UPDATE WAREHOUSE_ExamSessionTable
set warehouseTime = GETDATE()
	, ExportedToIntegration  = 1
where id in (select id from whtimerevert)

UPDATE WAREHOUSE_ExamSessionTable_Shreded
set warehouseTime = GETDATE()
where examSessionID in (select id from whtimerevert)



UPDATE WAREHOUSE_ExamSessionTable
set warehouseTime = b.warehouseTime
from WAREHOUSE_ExamSessionTable a
inner join whtimerevert b
on a.id = b.id

UPDATE WAREHOUSE_ExamSessionTable_Shreded
set warehouseTime = b.warehouseTime
from WAREHOUSE_ExamSessionTable_Shreded a
inner join whtimerevert b
on a.examSessionId = b.id
