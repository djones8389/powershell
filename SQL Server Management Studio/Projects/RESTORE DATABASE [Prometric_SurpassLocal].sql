RESTORE DATABASE [Prometric_SurpassLocal]
FROM DISK = 'E:\Users\DaveJ\Downloads\TC4212_Surpass_Backup'
WITH FILE = 1
	, MOVE 'SurpassTC' to 'E:\Data\Prometric_SurpassLocal.mdf'
	, MOVE 'SurpassTC_log' to 'E:\Log\Prometric_SurpassLocal.ldf'
, RECOVERY, NOUNLOAD, NOREWIND,  STATS = 5

GO