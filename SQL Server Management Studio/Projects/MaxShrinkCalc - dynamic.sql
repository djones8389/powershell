use master
IF OBJECT_ID('tempdb..##DBs') IS NOT NULL DROP TABLE ##DBs;

CREATE TABLE ##DBs (
	[Database] sysname
	, UsedSize int
	, TotalSize int
	, MaxIndexSize int
	,TotalIndexSize int
);

DECLARE @dynamic nvarchar(MAX)='';

select @dynamic +=CHAR(13)+ '
use '+QUOTENAME([NAME])+'

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

INSERT ##DBs([Database],UsedSize,TotalSize,MaxIndexSize,TotalIndexSize)
select db_name() [Database]
,
(
SELECT cast(CAST(((SUM(total_pages) * 8192) / 1048576.0) AS FLOAT) as int) 
FROM sys.allocation_units
) [UsedSizeMB],
(
select (size*8)/1024
from sys.sysfiles
where name = ''Data''
)  [TotalSizeMB]
,
(
SELECT  TOP 1
	cast(LTRIM (STR ((CASE WHEN usedpages > pages THEN (usedpages - pages) ELSE 0 END) * 8, 15, 0)) as int)/1024 [MaxIndexMB] 
FROM (

	SELECT 
		OBJECT_NAME(s.object_id) [TableName]
		, (used_page_count) [usedpages]
		,  (
			CASE
				WHEN (s.index_id < 2) THEN (in_row_data_page_count + lob_used_page_count + row_overflow_used_page_count)
				ELSE lob_used_page_count + row_overflow_used_page_count
			END
		) [pages]
		, i.name
	FROM sys.dm_db_partition_stats AS s
		INNER JOIN sys.indexes AS i
		ON s.[object_id] = i.[object_id]
			AND s.[index_id] = i.[index_id]
		INNER JOIN sys.tables t
		on t.object_id = s.object_id
) A
	order by [MaxIndexMB] DESC
) [MaxIndexMB]
,(
select sum([index_size-kb])/1024 [mb]
from (
	SELECT  
		[TableName]
		,[index_size-kb] = cast(LTRIM (STR ((CASE WHEN usedpages > pages THEN (usedpages - pages) ELSE 0 END) * 8, 15, 0)) as int)
		, name [index name]	
	FROM (

		SELECT 
			OBJECT_NAME(s.object_id) [TableName]
			, (used_page_count) [usedpages]
			,  (
				CASE
					WHEN (s.index_id < 2) THEN (in_row_data_page_count + lob_used_page_count + row_overflow_used_page_count)
					ELSE lob_used_page_count + row_overflow_used_page_count
				END
			) [pages]
			, i.name
		FROM sys.dm_db_partition_stats AS s
			INNER JOIN sys.indexes AS i
				ON s.[object_id] = i.[object_id]
				AND s.[index_id] = i.[index_id]
			INNER JOIN sys.tables t
			on t.object_id = s.object_id
	) A
) B
)

'
from sys.databases
where state_desc = 'ONLINE'
	--and name like '%[_]SecureAssess';

exec(@dynamic);

select *
	, TotalSize - UsedSize [Diff]
from ##DBs
where [Database] like 'stg%'
order by [Diff] DESC

--sp_whoisactive

/*

SELECT [Database]	
	, UsedSize
	, TotalSize
	, TotalSize - UsedSize [ReservedSpaceMB]
	, MaxIndexSize
	, TotalIndexSize
FROM ##DBs
order by UsedSize

/*
Database				UsedSize	TotalSize	ReservedSpaceMB	MaxIndexSize	TotalIndexSize
PRV_EAL_SecureAssess	228			260			32				24				138
*/

use PRV_EAL_SecureAssess

SELECT --distinct 
	B.name AS TableName
	, C.name AS IndexName
	, C.fill_factor AS IndexFillFactor
	, D.rows AS RowsCount
	, A.avg_fragmentation_in_percent
	, A.page_count
	,'ALTER INDEX ' + C.name +  ' ON ' + SCHEMA_NAME(SCHEMA_ID) + '.' + B.name + ' REBUILD;'
	--, case when (A.avg_fragmentation_in_percent > 30)
	--	then 'ALTER INDEX ' + quotename(C.name) +  ' ON ' + SCHEMA_NAME(SCHEMA_ID) + '.' + quotename(b.name) + ' REBUILD;'
	--	else 'ALTER INDEX ' + quotename(C.name) +  ' ON ' + SCHEMA_NAME(SCHEMA_ID) + '.' + quotename(b.name) + ' REORGANIZE;'
	--	end as 'IndexMaintenance'
FROM sys.dm_db_index_physical_stats(DB_ID(),NULL,NULL,NULL,NULL) A
INNER JOIN sys.objects B
ON A.object_id = B.object_id
INNER JOIN sys.indexes C
ON B.object_id = C.object_id AND A.index_id = C.index_id
INNER JOIN sys.partitions D
ON B.object_id = D.object_id AND A.index_id = D.index_id
WHERE C.index_id > 0
	--and A.avg_fragmentation_in_percent > 5


*/