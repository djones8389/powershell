USE master;

IF OBJECT_ID('tempdb..#Users') IS NOT NULL DROP TABLE #Users;

CREATE TABLE #Users (
	dbname sysname
	, username sysname
	, [sid] VARBINARY(max)
);

DECLARE @Command NVARCHAR(MAX) =  '

use [?];
if db_id() >  4
BEGIN
	SELECT ''?''
		,[Name]
		, sid
	FROM sysusers 
	WHERE issqlrole = 0
		AND uid > 4
		AND [Name] NOT IN (''LON\3168686-Admins'',''Powershell'')
END
'
INSERT #Users
EXEC sys.sp_MSforeachdb  @command1 =  @Command

SELECT sid
	, name
	, 'DROP LOGIN ' + QUOTENAME(name) + ';'
FROM sys.syslogins
WHERE sid NOT IN (
	SELECT sid
	FROM #Users 
)
ORDER BY name;

