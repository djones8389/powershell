USE [PPD_BCIntegration_SecureMarker]

DECLARE @myUserID INT
	,@myGroupID INT
	,@myTokenStoreID INT
	,@myCompetencyCheckInterval INT
	,@trainingMode BIT
	,@isMarkOneItemPerScript BIT
	,@randomPercentage DECIMAL

SET @myUserID = 118
SET @myGroupID = 1348
SET @myTokenStoreID = 8084988
SET @myCompetencyCheckInterval = 1440
SET @trainingMode = 0
SET @randomPercentage = 22.0585836200316
SET @isMarkOneItemPerScript = 1


DECLARE @CountSinceLastCI INT = 0
DECLARE @MaxResBeforeCI INT = 0
DECLARE @myUniqueGroupResponseId BIGINT
DECLARE @myMarkedMetadata XML
DECLARE @myNumMarkedByUser INT
DECLARE @myUserQuota INT
DECLARE @currentId BIGINT

SELECT @myNumMarkedByUser = QM.NumberMarked
	 ,@myUserQuota = QM.AssignedQuota
FROM dbo.QuotaManagement QM
WHERE QM.GroupId = @myGroupID
	AND QM.UserID = @myUserID


-- Unique responses of group response
DECLARE @groupUniqueResponses TABLE (
	UniqueResponseID BIGINT
	,MarkedMetadata XML
	,IsFilled BIT NOT NULL DEFAULT 0
)



SELECT @myUniqueGroupResponseId = 	UGR.id
FROM dbo.UniqueGroupResponses UGR
INNER JOIN dbo.GroupDefinitions GD ON GD.ID = UGR.GroupDefinitionID
WHERE GD.ID = @myGroupID
	AND UGR.CI_Review = 1
	AND UGR.CI = 1
ORDER BY NEWID();

SELECT  @myUniqueGroupResponseId

-- Get UniqueResponses Ids for group response
INSERT INTO @groupUniqueResponses (UniqueResponseID)
SELECT UR.ID
FROM dbo.UniqueResponses UR
WHERE UR.ID IN (
		SELECT UniqueResponseId
		FROM dbo.UniqueGroupResponseLinks
		WHERE UniqueGroupResponseID = @myUniqueGroupResponseId
		)
            

SELECT UR.ID
FROM dbo.UniqueResponses UR
WHERE UR.ID IN (1266389,1272508)

SELECT UniqueResponseId
FROM dbo.UniqueGroupResponseLinks
WHERE UniqueGroupResponseID in (1346064,1352183)


SELECT count(*)
FROM UniqueGroupResponseLinks UGRL
LEFT JOIN UniqueResponses UR
on UR.id = UGRL.UniqueGroupResponseID
where ur.id is null


select count(*)
FROM UniqueGroupResponseLinks UGRL
LEFT JOIN UniqueResponses UR
on UR.id = UGRL.UniqueResponseId
where ur.id is null


SELECT convert(nvarchar(MAX),UGRL.UniqueResponseId) + ','
FROM UniqueGroupResponseLinks UGRL
LEFT JOIN UniqueResponses UR
on UR.id = UGRL.UniqueResponseId
where ur.id is null
FOR XML PATH(''), type


            -- Select Unique Response level data ---------------------------------------------
            SELECT I.ID                AS ItemId
                  ,I.ExternalItemName  AS ItemName
                  ,I.ExternalItemID    AS ExternalItemID
                  ,I.[Version]         AS ItemVersion
                  ,I.LayoutName        AS LayoutName
                  ,I.TotalMark         AS TotalMark
                  ,I.MarkingType       AS MarkingType
                  ,I.AllowAnnotations  AS AllowAnnotations
                  ,GUR.MarkedMetadata  AS MarkedMetadata
                  ,UR.id               AS responseID
                  ,UR.responseData     AS ResponseData
                  ,GDI.ViewOnly        AS ViewOnly
            FROM   dbo.UniqueResponses UR
                   INNER JOIN dbo.Items I ON  UR.itemId = I.ID
                   INNER JOIN @groupUniqueResponses GUR ON  GUR.UniqueResponseID = UR.ID
                   INNER JOIN dbo.GroupDefinitionItems GDI ON  GDI.ItemID = UR.itemId
                            AND GDI.GroupID = @myGroupID
            ORDER BY GDI.GroupDefinitionItemOrder
                   ----------------------------------------------------------------------------------------
--        END
  --  END
--END
