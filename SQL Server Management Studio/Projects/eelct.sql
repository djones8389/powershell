SELECT COUNT(a.[INDEX])
	,a.[INDEX]
FROM (
	SELECT  
		--Instance
		--,[DATABASE]
		--,[TABLE]
		[INDEX]
		--,user_updates
		--,last_user_update
		--,user_seeks
		--,user_scans
		--,user_lookups
		,user_seeks+user_scans +user_lookups [Select]
		--,last_user_seek
		--,last_user_scan
		--,last_user_lookup

	FROM dbo.IndexMetrics
	WHERE [index] IN ('IX_ExamSessionTable_90', 'IX_ExamSessionTable_examState', 'missing_index_12_11_ExamSessionTable', 'idx_NC_ExamSessionTable_examState_attemptAutoSubmitForAwaitingUpload_CI', 'ExamSessionTable_ExamState_ExternallyDelivered', '_dta_index_ExamSessionTable_c_6_477244755__K1', 'UQ__ExamSessionTable__67FE6514', '_dta_index_ExamSessionTable_6_477244755__K6_K2_K3', '_dta_index_ExamSessionTable_6_477244755__K2_K7_1', 'IX_ExamSessionTable_UserID_NCI')
		AND StartDate > '2017-05-25'
) a
WHERE a.[Select] = 0
	GROUP BY [INDEX]
	ORDER BY 
		1 desc
	

--IX_ExamSessionTable_UserID_NCI