select top 10 *
from ExamSessionItemResponseTable



CREATE FUNCTION dbo.ASCIICheck (

	@String nvarchar(500)

)

RETURNS @T TABLE ([Character] CHAR(1),[ASCII Val] smallint)
AS

		BEGIN	
			
		declare @min tinyint = 1
		declare @max int = (select LEN(@String)+1)

		while (@min < @max)

			INSERT INTO @T
			select substring(@String, @min,1), ASCII((SELECT substring(@String, @min,1)))
			
			select @min = @min + 1
			Return

		END


