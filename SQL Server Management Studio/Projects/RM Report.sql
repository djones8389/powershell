USE ICAEW_SecureAssess

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

SELECT
		ut.Surname
		, ut.Forename [First name]
		, ut.CandidateRef [App No.]
		, ut.DOB
		, ct.CentreName
		, est.ScheduledDurationXml.value('data(duration/value)[1]','smallint') [ExamTime]
		, est.KeyCode
		, est.pinNumber [PIN]
		, ESLT.StateName [State]
		, ISNULL(Tags.Tag,'') Tag
		, scet.ExamName
		, CASE WHEN ExamStateChange.StateChangeDate IS NULL THEN 'Not yet completed'		
			ELSE CONVERT(varchar(11), ExamStateChange.StateChangeDate, 103) + ' ' +  CONVERT(varchar(11), ExamStateChange.StateChangeDate, 108)
				END AS [Exam Date]
		, ct.CentreCode
	FROM dbo.ScheduledExamsTable scet
	INNER JOIN dbo.ExamSessionTable est
	ON est.ScheduledExamID = scet.ID
	INNER JOIN dbo.CentreTable ct
	ON ct.id = scet.CentreID
	INNER JOIN dbo.UserTable ut
	ON ut.ID = est.UserID
	INNER JOIN dbo.ExamStateLookupTable ESLT
	ON ESLT.ID = est.examState
	LEFT JOIN (

		SELECT ExamSessionId
			, ExamSessionTagId
			, CentreId
			, Tag
		FROM [ExamSessionTagReferenceTable]
		LEFT JOIN  [dbo].[ExamSessionTagLookupTable]
		ON [ExamSessionTagLookupTable].Id = ExamSessionTagId

	) Tags
	ON tags.ExamSessionId = est.ID
	LEFT JOIN (
			SELECT ExamSessionID
			,MIN(StateChangeDate) StateChangeDate
		FROM dbo.ExamStateChangeAuditTable
		WHERE NewState IN (6,18,10)
			GROUP BY ExamSessionID

	)  ExamStateChange
	ON ExamStateChange.ExamSessionID = est.ID