sp_whoisactive
xp_readerrorlog

declare @temp table (
	spid int
	, dbid int
	, objid int
	, indid int
	, type nvarchar(100)
	, resource nvarchar(1000)
	, mode char(100)
	, status nvarchar(100)
)

insert @temp
exec ('sp_lock')

select *
from @temp
where status <> 'GRANT'

select count(1)
	, hostname
from sys.sysprocesses
group by hostname
order by 1 desc

select count(1)
	, loginame
from sys.sysprocesses
group by loginame
order by 1 desc