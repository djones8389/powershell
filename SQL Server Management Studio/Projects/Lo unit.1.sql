if OBJECT_ID('tempdb..#ResultHolder') IS NOT NULL DROP TABLE #ResultHolder;

SELECT
	ID [ExamSessionID]
	, StructureXML.value('data(assessmentDetails/qualificationName)[1]','nvarchar(255)') [QualificationName]
	, a.b.value('@id','nvarchar(20)') [ItemID]
	, a.b.value('@name','nvarchar(100)') [ItemName]
	, a.b.value('@unit','nvarchar(100)') [unit]
	, a.b.value('@LO','nvarchar(100)') [LO]
	, structureXML
	, resultDataFull
INTO #ResultHolder
from [SANDBOX_Abcawards_SecureAssess]..WAREHOUSE_ExamSessionTable SA
cross apply structurexml.nodes('assessmentDetails/assessment/section/item') a(b)
where a.b.value('@LO','nvarchar(max)') = ''
	and a.b.value('@unit','nvarchar(max)') != ''

select distinct
		itemID
		, [text]
		, MultipleValuesAllowed
	from #ResultHolder I
	inner join [SANDBOX_Abcawards_ContentAuthor]..SubjectTagValueItems on SubjectTagValueItems.Item_Id = substring(I.ItemID, CHARINDEX('P',I.ItemID)+1, 10)
	inner join [SANDBOX_Abcawards_ContentAuthor]..SubjectTagValues on SubjectTagValues.id = SubjectTagValueItems.SubjectTagValue_Id
	inner join [SANDBOX_Abcawards_ContentAuthor]..SubjectTagTypes on SubjectTagTypes.Id = SubjectTagValues.SubjectTagTypeId
	inner join [SANDBOX_Abcawards_ContentAuthor]..Subjects on subjects.title = I.QualificationName
	where TagTypeKey = 1









set transaction isolation level read uncommitted;

use [SANDBOX_Abcawards_ContentAuthor]


declare @mystring nvarchar(MAX)= (
select cast(SubjectTagValues.[text] + ', ' as nvarchar(MAX))
from Items 

inner join SubjectTagValueItems on SubjectTagValueItems.Item_Id = Items.Id
inner join SubjectTagValues on SubjectTagValues.id = SubjectTagValueItems.SubjectTagValue_Id
inner join SubjectTagTypes on SubjectTagTypes.Id = SubjectTagValues.SubjectTagTypeId
inner join Subjects on subjects.Id = Items.SubjectId

where Items.Name = 'MOTQ113'
	and Title = 'A6003-02 - Level 2 Award in MOT Testing (Classes 4 and 7)'
	and SubjectTagValues.[text] != '05 Carry Out a Statutory Periodic Roadworthiness Test'
	and MultipleValuesAllowed = 1
	for xml path ('')
	)
	
SELECT SUBSTRING(@mystring, 0, LEN(@mystring))


--LO="Know testing methods and how to conduct a vehicle test, Standards" unit="05 Carry Out a Statutory Periodic Roadworthiness Test" quT="10" />
--    Know testing methods and how to conduct a vehicle test, Standards