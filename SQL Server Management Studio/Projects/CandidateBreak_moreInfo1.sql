use master;

IF OBJECT_ID('tempdb..##ItemBank') IS NOT NULL DROP TABLE ##ItemBank;

CREATE TABLE ##ItemBank (
	Client nvarchar(1000)
	,QualificationName nvarchar(200)
	,QualStatus nvarchar(50)
	,TestName  nvarchar(1000)
	,TestFormName nvarchar(1000)
	,AssessmentStatus  nvarchar(50)
	,TestFormReference nvarchar(1000)
	,LastTimeExamTaken datetime null
	,NoOfExamsSat smallint
	,DueToBeSat smallint
	,TotalBreakTimeUsed int
);

DECLARE @dynamic nvarchar(MAX)='';

select @dynamic +=CHAR(13)+ '
use '+QUOTENAME([NAME])+'

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

INSERT ##ItemBank(Client,QualificationName,QualStatus,TestName,TestFormName,AssessmentStatus,TestFormReference)
SELECT 
	db_Name() 
	, QT.QualificationName
	, QL.[Name] QualStatus
	, AGT.Name [TestName]
	, AssessmentName [TestFormName]
	, AL.[Name] AssessmentStatus
	, externalreference [TestFormReference]
FROM AssessmentTable AT
INNER JOIN AssessmentGroupTable AGT ON AGT.ID = AT.AssessmentGroupID
INNER JOIN QualificationTable QT on QT.ID = AGT.QualificationID
INNER JOIN QualificationStatusLookupTable QL on QL.ID = QT.QualificationStatus
INNER JOIN AssessmentStatusLookupTable AL on AL.ID = AT.AssessmentStatus
WHERE EnableCandidateBreak = 1 
	AND CandidateBreakStyle = 1 
'
from sys.databases
where state_desc = 'ONLINE'
	and name like '%[_]ItemBank';

exec(@dynamic);
GO


DECLARE myCursor CURSOR FOR
SELECT 
	substring(client,0,charindex('_',client)) [Client]
	, QualificationName
	, TestName
	, TestFormName
	, TestFormReference
FROM ##ItemBank
--WHERE substring(client,0,charindex('_',client))  = 'Demo';

DECLARE @Client nvarchar(100), @QualificationName nvarchar(100), @TestName nvarchar(100), @TestFormName nvarchar(100), @TestFormReference nvarchar(100);

OPEN myCursor

FETCH NEXT FROM myCursor INTO @Client,@QualificationName,@TestName,@TestFormName,@TestFormReference

WHILE @@FETCH_STATUS = 0

	BEGIN

		DECLARE @LastTime TABLE (LastTime datetime)
		DECLARE @LastTimeSQL NVARCHAR(MAX) = ''

			SELECT @LastTimeSQL = '	

			USE '+@Client+'_SecureAssess

			;with minCTE as (
			SELECT est.id
				, MIN(StateChangeDate) StateChangeDate
			from ScheduledExamsTable SCET
			INNER JOIN ExamSessionTable EST ON EST.ScheduledExamID = SCET.ID
			INNER JOIN IB3QualificationLookup IB3 on IB3.ID = SCET.qualificationID
			LEFT JOIN (
				SELECT examSessionID
					, max(StateChangeDate) StateChangeDate
				FROM ExamStateChangeAuditTable
				where newState in (6,9)
				group by examSessionID

			) ESCAT
			ON ESCAT.ExamSessionID = EST.ID	
			where ib3.QualificationName = '''+@QualificationName+'''
				and examName = '''+@TestName+'''
				and examVersionName= '''+@TestFormName+'''
				and examVersionRef = '''+@TestFormReference+'''
			group by est.id

			UNION

			SELECT examsessionid
				, min(started) StateChangeDate
			FROM WAREHOUSE_ExamSessionTable_Shreded WESTS
			where QualificationName = '''+@QualificationName+'''
				and examName = '''+@TestName+'''
				and examVersionName= '''+@TestFormName+'''
				and examVersionRef = '''+@TestFormReference+'''
				and [started] != ''1900-01-01 00:00:00.000''
			group by examsessionid
			)

			select min(StateChangeDate)
			from minCTE
			'
			INSERT @LastTime
			EXEC(@LastTimeSQL)
				
			DECLARE @noOfExamsSat TABLE (noOfExamsSat smallint)
			DECLARE @noOfExamsSatSQL NVARCHAR(MAX)= ''

			SELECT @noOfExamsSatSQL = '	

			USE '+@Client+'_SecureAssess

			;with examCount as (
			SELECT est.id
			from ScheduledExamsTable SCET
			INNER JOIN ExamSessionTable EST ON EST.ScheduledExamID = SCET.ID
			INNER JOIN IB3QualificationLookup IB3 on IB3.ID = SCET.qualificationID
			LEFT JOIN (
				SELECT examSessionID
					, max(StateChangeDate) StateChangeDate
				FROM ExamStateChangeAuditTable
				where newState in (6,9)
				group by examSessionID

			) ESCAT
			ON ESCAT.ExamSessionID = EST.ID	
			where QualificationName = '''+@QualificationName+'''
				and examName = '''+@TestName+'''
				and examVersionName= '''+@TestFormName+'''
				and examVersionRef = '''+@TestFormReference+'''

				
			UNION

			SELECT WEST.examsessionid
			FROM WAREHOUSE_ExamSessionTable_Shreded WESTS
			INNER JOIN WAREHOUSE_ExamSessionTable WEST
			ON WEST.ID = WESTS.ExamSessionID
			where QualificationName = '''+@QualificationName+'''
				and examName = '''+@TestName+'''
				and examVersionName= '''+@TestFormName+'''
				and examVersionRef = '''+@TestFormReference+'''
				and ExamStateChangeAuditXml.exist(''exam/stateChange[newStateID=6]'') = 1
			)
				
			select COUNT(ID)
			from examCount

			'
			INSERT @noOfExamsSat
			EXEC(@noOfExamsSatSQL)


			DECLARE @DueToBeSat TABLE (DueToBeSat smallint)
			DECLARE @DueToBeSatSQL NVARCHAR(MAX) = ''

			SELECT @DueToBeSatSQL = '	
					
				USE '+@Client+'_SecureAssess

				SELECT COUNT(EST.id)
				from ScheduledExamsTable SCET
				INNER JOIN ExamSessionTable EST ON EST.ScheduledExamID = SCET.ID
				INNER JOIN IB3QualificationLookup IB3 on IB3.ID = SCET.qualificationID					
				where examState in (1,2,3,8,18)
					and QualificationName = '''+@QualificationName+'''
					and examName = '''+@TestName+'''
					and examVersionName= '''+@TestFormName+'''
					and examVersionRef = '''+@TestFormReference+'''
			'
			INSERT @DueToBeSat
			EXEC(@DueToBeSatSQL)


			DECLARE @TotalBreakTimeUsed TABLE (TotalBreakTimeUsed int)
			DECLARE @TotalBreakTimeUsedSQL NVARCHAR(MAX) = ''

			SELECT @TotalBreakTimeUsedSQL = '
				
			USE '+@Client+'_SecureAssess

			;with examCount as (
			SELECT est.id
			from ScheduledExamsTable SCET
			INNER JOIN ExamSessionTable EST ON EST.ScheduledExamID = SCET.ID
			INNER JOIN IB3QualificationLookup IB3 on IB3.ID = SCET.qualificationID
			where QualificationName = '''+@QualificationName+'''
				and examName = '''+@TestName+'''
				and examVersionName= '''+@TestFormName+'''
				and examVersionRef = '''+@TestFormReference+'''
				and TotalBreakTimeUsed > 0
				
			UNION

			SELECT WEST.examsessionid
			FROM WAREHOUSE_ExamSessionTable_Shreded WESTS
			INNER JOIN WAREHOUSE_ExamSessionTable WEST
			ON WEST.ID = WESTS.ExamSessionID
			where QualificationName = '''+@QualificationName+'''
				and examName = '''+@TestName+'''
				and examVersionName= '''+@TestFormName+'''
				and examVersionRef = '''+@TestFormReference+'''
				and TotalBreakTimeUsed > 0
			)
				
			select COUNT(ID)
			from examCount			
			'
			INSERT @TotalBreakTimeUsed
			EXEC(@TotalBreakTimeUsedSQL)

			UPDATE ##ItemBank
			SET LastTimeExamTaken = (SELECT TOP 1 LastTime from @LastTime)
				, noOfExamsSat = (SELECT TOP 1 noOfExamsSat from @noOfExamsSat)
				, DueToBeSat = (SELECT TOP 1 DueToBeSat from @DueToBeSat)
				, TotalBreakTimeUsed = (SELECT TOP 1 TotalBreakTimeUsed from @TotalBreakTimeUsed)
			where QualificationName = @QualificationName
				and @TestName = @TestName
				and TestFormName = TestFormName
				and TestFormReference = @TestFormReference


			DELETE @LastTime
			DELETE @noOfExamsSat
			DELETE @DueToBeSat
			DELETE @TotalBreakTimeUsed

	FETCH NEXT FROM myCursor INTO @Client,@QualificationName,@TestName,@TestFormName,@TestFormReference

END 
CLOSE myCursor
DEALLOCATE myCursor



	SELECT substring(client,0,charindex('_',client)) [Client]
		, QualificationName
		, QualStatus
		, TestName
		, AssessmentStatus
		, TestFormName
		, TestFormReference
		, LastTimeExamTaken
		, NoOfExamsSat
		, DueToBeSat
		, TotalBreakTimeUsed
	FROM ##ItemBank




/*



SELECT A.*
FROM (
	select 
		substring(client,0,charindex('_',client)) [Client]
		, QualificationName
		, QualStatus
		, TestName
		, AssessmentStatus
		, TestFormName
		, TestFormReference
	from ##ItemBank
) A



USE Demo_SecureAssess;

select DISTINCT IB.*
from ScheduledExamsTable SCET
INNER JOIN ExamSessionTable EST ON EST.ScheduledExamID = SCET.ID
INNER JOIN IB3QualificationLookup IB3 on IB3.ID = SCET.qualificationID
INNER JOIN ##ItemBank IB ON IB.QualificationName = IB3.QualificationName
						AND IB.TestName = SCET.examName
LEFT JOIN (
	SELECT MAX(StateChangeDate) StateChangeDate
		,	ExamSessionID
	FROM (
		SELECT MAX(StateChangeDate) StateChangeDate
			,ExamSessionID
		  FROM ExamStateChangeAuditTable
		  where newState in (6,9)
		  GROUP by ExamSessionID
	) B
	GROUP by ExamSessionID
) ESCAT  
	ON ESCAT.ExamSessionID = EST.ID  



	
	SELECT --MIN(StateChangeDate)
		QualificationName
		 , examName
		 , examVersionName
		 , examVersionRef
	from ScheduledExamsTable SCET
	INNER JOIN ExamSessionTable EST ON EST.ScheduledExamID = SCET.ID
	INNER JOIN IB3QualificationLookup IB3 on IB3.ID = SCET.qualificationID
	LEFT JOIN (
		SELECT examSessionID
			, max(StateChangeDate) StateChangeDate
		FROM ExamStateChangeAuditTable
		where newState in (6,9)
		group by examSessionID

	) ESCAT
	ON ESCAT.ExamSessionID = EST.ID	
	where ib3.QualificationName = 'Demonstration!^%'
		and examName = 'Demonstration Test'
		and examVersionName= 'Demonstration Test Form'
		and examVersionRef = 'Demo Form - SF'







	USE Demo_SecureAssess

	;with minCTE as (
	SELECT est.id
		, MIN(StateChangeDate) StateChangeDate
	from ScheduledExamsTable SCET
	INNER JOIN ExamSessionTable EST ON EST.ScheduledExamID = SCET.ID
	INNER JOIN IB3QualificationLookup IB3 on IB3.ID = SCET.qualificationID
	LEFT JOIN (
		SELECT examSessionID
			, max(StateChangeDate) StateChangeDate
		FROM ExamStateChangeAuditTable
		where newState in (6,9)
		group by examSessionID

	) ESCAT
	ON ESCAT.ExamSessionID = EST.ID	
	where ib3.QualificationName = 'Demonstration'
		and examName = 'Demonstration Test'
		and examVersionName= 'Demonstration Test Form'
		and examVersionRef = 'Demo Form - SF'
	group by est.id

	UNION

	SELECT examsessionid
		, min(started) StateChangeDate
	FROM WAREHOUSE_ExamSessionTable_Shreded WESTS
	where QualificationName = 'Demonstration'
		and examName = 'Demonstration Test'
		and examVersionName= 'Demonstration Test Form'
		and examVersionRef = 'Demo Form - SF'
		and [started] != '1900-01-01 00:00:00.000'
	group by examsessionid
	)

	select min(StateChangeDate)
	from minCTE



*/