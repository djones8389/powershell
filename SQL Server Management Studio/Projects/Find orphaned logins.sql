use master

DECLARE @UserHolder TABLE (
	DBUser nvarchar(100)
	, sID varbinary(85)
)

INSERT @UserHolder
exec sp_MSforeachdb
'
use [?];

if (db_id() > 4)
BEGIN
	SELECT name
		, sid
from sys.sysusers
where issqlrole = 0
	and hasdbaccess = 1
END

'

select NAME
, 'DROP LOGIN ' + QUOTENAME(NAME) + ';'
, *
from sys.syslogins
where sid NOT IN(
	SELECT sID 
	FROM @UserHolder
	)
	and name not like '%##%'
	and name not like 'NT%'


