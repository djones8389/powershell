SELECT C.DBName
	, C.YYYY
	, totalMark
	, markingType
	
FROM (
  SELECT 
	 db_NAME() [DBName]
	 , YEAR(WarehouseTime) [YYYY]	 
	 ,  a.b.value('@markingType','int')  markingType
	 ,  a.b.value('@totalMark','int')  totalMark

	 --,  a.b.value('count(@markingType)','int')  totalmarkingType
	 --,  a.b.value('count(@totalMark)','int')  totaltotalMark
  FROM WAREHOUSE_ExamSessionTable
  CROSS APPLY ResultData.nodes('exam/section/item') a(b)
) C

	group by DBName, YYYY,totalMark,markingType
		order by 1,2,3,4

  /*
  	 ,  CASE a.b.value('@markingType','nvarchar(10)')  
			when '0' then 'Computer Marking'
			WHen '1' then 'Human Marking'
			ELSE a.b.value('@markingType','nvarchar(10)')  
		END AS markingType
	 ,  CASE a.b.value('@totalMark','nvarchar(10)') 
			WHEN '0' THEN 'Not Marked' 
			ELSE a.b.value('@totalMark','nvarchar(10)')
		END AS  totalMark
		*/







 SELECT 
	 db_NAME() [DBName]
	 , YEAR(WarehouseTime) [YYYY]	 
	 ,  a.b.value('@markingType','int')  markingType
	 ,  a.b.value('@totalMark','int')  totalMark
	  ,  a.b.query('.')
	 --,  a.b.value('count(@markingType)','int')  totalmarkingType
	 --,  a.b.value('count(@totalMark)','int')  totaltotalMark
  FROM WAREHOUSE_ExamSessionTable
  CROSS APPLY ResultData.nodes('exam/section/item') a(b)
