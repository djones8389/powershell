use master

IF OBJECT_ID ('tempdb..##ResultHolder') IS NOT NULL DROP TABLE ##ResultHolder;


CREATE TABLE ##ResultHolder (

	DBName nvarchar(100)
	, [Total Centres] int
	, [UK Centres] int
	, [Non-UK Centres] int
	, [Unknown Centres] int
)

INSERT  ##ResultHolder
exec sp_MSforeachdb '

use [?];

if(''?'' in (select name from sys.databases where name like ''%SecureAssess%'' and state_desc = ''ONLINE''))

BEGIN
	SET QUOTED_IDENTIFIER ON;

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED


	SELECT DB_NAME()
		,COUNT(CT.ID) [TotalCentres]
		,(
			SELECT COUNT(CT.ID)
			FROM CentreTable CT
			INNER JOIN CountryLookupTable CLT ON CLT.id = CT.Country
			WHERE CentreName NOT LIKE ''%BTL%''
				AND Retired = 0
				AND clt.Country IN (
					''England''
					,''Northern Ireland''
					,''Scotland''
					,''Wales''
					)
			) [UK]
		,(
			SELECT COUNT(CT.ID)
			FROM CentreTable CT
			INNER JOIN CountryLookupTable CLT ON CLT.id = CT.Country
			WHERE CentreName NOT LIKE ''%BTL%''
				AND Retired = 0
				AND clt.Country NOT IN (
					''England''
					,''Northern Ireland''
					,''Scotland''
					,''Wales''
					,''Unknown''
					)
			) [Non-UK]
				,(
			SELECT COUNT(CT.ID)
			FROM CentreTable CT
			INNER JOIN CountryLookupTable CLT ON CLT.id = CT.Country
			WHERE CentreName NOT LIKE ''%BTL%''
				AND Retired = 0
				AND clt.Country IN (''Unknown'')
			) [Unknown]
	FROM CentreTable CT
	INNER JOIN CountryLookupTable CLT ON CLT.id = CT.Country
	WHERE ct.CentreName NOT LIKE ''%BTL%''
		AND Retired = 0;
END
	
'