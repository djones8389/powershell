SELECT *
FROM (
SELECT 
	db_NAME() [DBName]
	,DimTime.CalendarYear AS [YYYY]
	,  case 
		when DimMarkingTypes.[MarkingType] = 'Computer Marking' THEN 'Computer Marking'
		when DimMarkingTypes.[MarkingType] = 'Human Marking' THEN 'Human Marking'
		when DimMarkingTypes.[MarkingType] = 'Not Marked' THEN 'Not Marked' 
		when DimMarkingTypes.[MarkingType] = 'Smart Marking' THEN 'Computer Marking' 
		end as [MarkingType]
    , COUNT(DISTINCT CAST(FactQuestionResponses.ExamSessionKey AS nvarchar(100)) + FactQuestionResponses.CPID) AS [#]
FROM dbo.FactExamSessions
INNER JOIN dbo.FactQuestionResponses
ON FactExamSessions.ExamSessionKey = FactQuestionResponses.ExamSessionKey
INNER JOIN dbo.DimQuestions
ON FactQuestionResponses.CPID = DimQuestions.CPID
	AND FactQuestionResponses.CPVersion = DimQuestions.CPVersion
INNER JOIN dbo.DimMarkingTypes
ON DimQuestions.MarkingTypeKey = DimMarkingTypes.MarkingTypeKey
INNER JOIN dbo.DimTime
ON FactExamSessions.CompletionDateKey = DimTime.TimeKey
WHERE FactExamSessions.FinalExamState <> 10
GROUP BY DimTime.CalendarYear,
        DimMarkingTypes.[MarkingType]
) AS S
PIVOT
(
	SUM(#)
	FOR markingtype IN ([Computer Marking], [Human Marking],[Not Marked])
) as pvt