use master

exec sp_MSforeachdb '

use [?];

if(''?'' like ''%ItemBank''))

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

BEGIN
	SELECT 
		@@SERVERNAME ServerName
		, db_NAME() DBName
		, SUM(b.onscreen) [Onscreen]
		, SUM(b.paper) [Paper]
	FROM (
	SELECT
		 CASE ISNULL(OnScreen, 0) WHEN ''OnScreen'' THEN 1 ELSE 0 END AS [OnScreen]
		, CASE ISNULL(Paper, 0) WHEN ''Paper'' THEN 1 ELSE 0 END AS [Paper]
	FROM (
		select 
			CASE agt.OutputType 
				WHEN 0 THEN ''OnScreen''
			END AS [OnScreen]
			,CASE agt.OutputType 
				WHEN 1 THEN ''Paper''
			END AS [Paper]
		from AssessmentTable AT
		inner join [AssessmentGroupTable] AGT
		on AGT.id = at.Assessmentgroupid
	) A
	) B
END

'