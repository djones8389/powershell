DECLARE @StateStatistics TABLE (StateId INT, Minimun INT, Maximum INT, Diff INT);
DECLARE @Statements TABLE ([Statement] nvarchar(MAX)) -- NVARCHAR(MAX) = '';
DECLARE @TestID smallint = 431;

INSERT @Statements	
SELECT  'IF EXISTS(SELECT TOP (1) * FROM [dbo].[Aggregations_StateManagemet_TimeLine] WHERE TestId = 431)
		BEGIN
	
			INSERT INTO @StateStatistics
			SELECT 
				StateId,
				MIN([Count]) AS Minimum,
				MAX([Count]) AS Maximum,
				MAX([Count]) - MIN([Count]) AS [Difference]
				FROM '+TableName+'
			WHERE TestID = '+cast(@TestID as varchar(10))+'
			GROUP BY StateId
			
		END	
' [Statement]
FROM (VALUES ('[Aggregations_StateManagemet_TimeLine]'),('[StateManagement]')) a(tableName)


SELECT *
FROM @Statements



/*




DECLARE @Dynamic NVARCHAR(MAX) = ''

SELECT @Dynamic += CHAR(13) + '
	IF EXISTS(SELECT TOP (1) * FROM [dbo].[Aggregations_StateManagemet_TimeLine] WHERE TestId = 431)
	BEGIN
	
	INSERT INTO @StateStatistics
	
	SELECT 
		StateId,
		MIN([Count]) AS Minimum,
		MAX([Count]) AS Maximum,
		MAX([Count]) - MIN([Count]) AS [Difference]
	FROM '+TableName+'
	WHERE TestID = 431
	GROUP BY StateId
'
FROM @Tables


PRINT (@Dynamic)


SELECT 
	StateId,
	MIN([Count]) AS Minimum,
	MAX([Count]) AS Maximum,
	MAX([Count]) - MIN([Count]) AS [Difference]
FROM StateManagement
WHERE TestID = 431
GROUP BY StateId

*/