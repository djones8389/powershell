use ICAEW_SecureAssess

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

if OBJECT_ID('tempdb..#badexams') is not null drop table #badexams;

CREATE TABLE #badexams (
	esid int
	, itemid nvarchar(20)
);

INSERT #badexams
SELECT ExamSessionID
	, ItemID
from ExamSessionItemResponseTable (READUNCOMMITTED)
CROSS APPLY itemresponsedata.nodes('p/s/c/i/t/r/c/text') a(b)
where LEFT(a.b.value('.', 'nvarchar(MAX)'), 7) = '%253CP%'

UNION

SELECT west.ExamSessionID
	, ItemID
from warehouse_ExamSessionItemResponseTable wesirt (READUNCOMMITTED)
inner join warehouse_examsessiontable west
on west.id = wesirt.WAREHOUSEExamSessionID
CROSS APPLY itemresponsedata.nodes('p/s/c/i/t/r/c/text') a(b)
where LEFT(a.b.value('.', 'nvarchar(MAX)'), 7) = '%253CP%'
	
create clustered index [IX] on #badexams (esid);

PRINT 'table created'

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

select  a.ID
	, a.examName
	, a.examVersionRef
	, a.KeyCode
	, a.CandidateRef
	, a.Forename
	, a.Surname
	, examState
	, b.Section
	, b.QuestionNo
	, b.ItemID
FROM (
	select
		est.ID
		,examName
		, examVersionRef
		, KeyCode
		, CandidateRef
		, Forename
		, Surname
		,examState
		, esirt.ItemID	
	from ExamSessionTable est 

	inner join ExamSessionItemResponseTable esirt	 
	on est.id = esirt.ExamSessionID
	inner join #badexams b
	on b.esid = est.ID
		and b.itemid = esirt.ItemID
	inner join UserTable ut
	on ut.id = est.UserID
	inner join ScheduledExamsTable scet
	on scet.id = est.ScheduledExamID
) A
inner join (
	select id
		 , ROW_NUMBER () over(partition by a.id, c.d.value('data(../@id)[1]','tinyint') order by a.id) QuestionNo
		 , c.d.value('data(@id)[1]','nvarchar(20)') ItemID
		 , c.d.value('data(../@id)[1]','tinyint') Section
		 , StructureXML
	from ExamSessionTable a
	CROSS APPLY structurexml.nodes('assessmentDetails/assessment/section/item') c(d)
	where a.id in (select esid from #badexams)
) B
on a.ID = b.ID
	and a.itemid = b.ItemID

UNION

select  a.ExamSessionID
	, a.examName
	, a.examVersionRef
	, a.KeyCode
	, a.CandidateRef
	, a.Forename
	, a.Surname
	, '13' examState
	, b.Section
	, b.QuestionNo
	, b.ItemID
FROM (
	select
		west.ExamSessionID
		,wests.examName
		, examVersionRef
		, KeyCode
		, CandidateRef
		, wut.Forename
		, wut.Surname
		--,examState
		, wesirt.ItemID	
	from warehouse_ExamSessionTable west 

	inner join warehouse_ExamSessionItemResponseTable wesirt	 
	on west.id = wesirt.warehouseexamsessionid
	inner join #badexams b
	on b.esid = west.examsessionid
		and b.itemid = wesirt.ItemID
	inner join WAREHOUSE_ExamSessionTable_Shreded wests
	on wests.examsessionid = west.id
	inner join warehouse_UserTable wut
	on wut.id = west.warehouseuserid
	inner join warehouse_ScheduledExamsTable scet
	on scet.id = west.WAREHOUSEScheduledExamID
) A
inner join (
	select ExamSessionID
		 , ROW_NUMBER () over(partition by a.id, c.d.value('data(../@id)[1]','tinyint') order by a.id) QuestionNo
		 , c.d.value('data(@id)[1]','nvarchar(20)') ItemID
		 , c.d.value('data(../@id)[1]','tinyint') Section
		 , StructureXML
	from warehouse_ExamSessionTable a
	CROSS APPLY structurexml.nodes('assessmentDetails/assessment/section/item') c(d)
	where a.ExamSessionID in (select esid from #badexams)
) B
on a.ExamSessionID = b.ExamSessionID
	and a.itemid = b.ItemID








/*
SELECT	ROW_NUMBER () over(partition by a.id, c.d.value('data(../@id)[1]','tinyint') order by a.id) r
		 ,c.d.value('data(../@id)[1]','tinyint') Section
		 ,c.d.value('data(@id)[1]','nvarchar(20)') ItemID
from ExamSessionTable a
CROSS APPLY structurexml.nodes('assessmentDetails/assessment/section/item') c(d)
where keycode in ('V8BQGNF6','RGB6GLF6','93FFV8F6')

--SELECT  replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(candidateResponse,'%253CP%2520ALIGN%253D%2522LEFT%2522%253E%253CFONT%2520FACE%253D%2522Arial%2522%2520SIZE%253D%252212%2522%2520COLOR%253D%2522%2523000000%2522%2520LETTERSPACING%253D%25220%2522%2520KERNING%253D%25220%2522%253E%253CB%253E',''),'%253C/FONT%253E%253C/P%253E',''),'Purchases%253C/B%253E%2520',''),'%2520',' '),'%253C/B%253E',''),'%253CP ALIGN%253D%2522LEFT%2522%253E%253CFONT FACE%253D%2522Arial%2522 SIZE%253D%252212%2522 COLOR%253D%2522%2523000000%2522 LETTERSPACING%253D%25220%2522 KERNING%253D%25220%2522%253E',' '),'%253',''),'CU%',''),'UECUEC/UE',''),'%252C',''''),'CUE',''),'C/UE','')
--FROM #intermediate

	
	select id, examState, KeyCode, StructureXML
	from ExamSessionTable
	where Keycode = '93FFV8F6'

	
	93FFV8F6	4499189	Saba Wanchoo	Mir	2	408	368P983	%3DB5+B6+B7	%3DB5+B6+B7
		--CROSS APPLY itemresponsedata.nodes('p/s/c/i/t/r/c/text') a(b)
	
	--where KeyCode = '93FFV8F6'
	--examsessionid = 5794
	--	and itemid = '368P936'		
	--	and 
		--and LEFT(a.b.value('.', 'nvarchar(MAX)'), 7) = '%253CP%'
	  --and a.b.value('../@bd', 'nvarchar(MAX)')  is not null

	  	--, candidateResponse nvarchar(MAX)
	--, actualCandidateResponse nvarchar(MAX)
	--, cell nvarchar(100)
	)

	*/