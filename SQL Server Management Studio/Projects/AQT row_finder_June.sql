Use ICAEW_SecureAssess

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;


SELECT C.ExamSessionID
	, C.ItemID
	, C.Keycode
	, C.Response
	, cast(C.RowNumber as varchar(1000)) + ' of ' + cast(MaxChecker.Max_RowNumber as varchar(1000)) [Data Row Position]
	, warehouseTime
	, PositionFinder.QuestionPosition
FROM (
	SELECT WEST.ExamSessionID
		, 'Archived' ExamState
		, ItemID
		, Keycode
		, a.b.value('.','nvarchar(max)') [Response]
		, ROW_NUMBER() over (partition by WESIRT.WarehouseExamSessionID, WESIRT.itemID order by (SELECT 1)) RowNumber
		,warehouseTime
	FROM  Warehouse_ExamSessionItemResponseTable WESIRT  WITH (NOLOCK)
	
	INNER JOIN Warehouse_ExamSessionTable WEST  WITH (NOLOCK)
	ON WEST.ID = WESIRT.WarehouseExamSessionID

	CROSS APPLY WESIRT.ItemResponseData.nodes('p/s/c/i/t/r/c[1]') a(b)

	where ItemResponseData.exist('p/s/c[@typ=20]') = 1
		and warehouseTime > '2017-06-01'
) C

INNER JOIN (

	SELECT 
		ExamSessionID
		, KeyCode
		, ItemID
		, MAX(RowNumber) Max_RowNumber
	FROM (
		SELECT WEST.ExamSessionID 
			, ItemID
			, KeyCode
 			, ROW_NUMBER() over (partition by WESIRT.WarehouseExamSessionID, WESIRT.itemID order by (SELECT 1)) RowNumber
			,warehouseTime
		FROM  Warehouse_ExamSessionItemResponseTable WESIRT WITH (NOLOCK)
		
		INNER JOIN Warehouse_ExamSessionTable WEST WITH (NOLOCK)
		ON WEST.ID = WESIRT.WarehouseExamSessionID

		CROSS APPLY WESIRT.ItemResponseData.nodes('p/s/c/i/t/r/c[1]') a(b)

		where ItemResponseData.exist('p/s/c[@typ=20]') = 1
			and warehouseTime > '2017-06-01'
	) f
	group by ExamSessionID
		, KeyCode
		, ItemID
		
) MaxChecker

on MaxChecker.ExamSessionID = C.ExamSessionID	
	and MaxChecker.ItemID = C.ItemID

INNER JOIN (
	select PositionFinder.examsessionid [ESID]	
		, PositionFinder.Section
		, PositionFinder.Item
		, ROW_NUMBER() over(partition by examsessionid,[section] order by [section]) [QuestionPosition]
	from (
	SELECT west.examsessionid
		,exam.section.value('@id','tinyint') [section]
		, section.item.value('@id','nvarchar(15)') [item]
	FROM WAREHOUSE_ExamSessionTable west
	cross apply resultData.nodes('exam/section') exam(section)
	cross apply exam.section.nodes('item') section(item)

	) PositionFinder
) PositionFinder
on PositionFinder.ESID = C.ExamSessionID
	and PositionFinder.item = C.ItemID

where ([Response] IS NOT NULL AND [Response] <> '')
	AND (Max_RowNumber-RowNumber) <= 2
	order by (Max_RowNumber-RowNumber) asc;

