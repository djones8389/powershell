IF OBJECT_ID('tempdb..#AATStats') IS NOT NULL DROP TABLE #AATStats;


CREATE TABLE #AATStats (
	server_name nvarchar(100)
	,database_name nvarchar(100)
	,schema_name nvarchar(100)
	,table_name nvarchar(100)
	,row_count nvarchar(100)
	,reserved_kb int
	,data_kb int
	,index_kb int
	,unused_kb int
	,collection_date datetime
) 

BULK INSERT #AATStats
from 'D:\Users\DaveJ\Documents\SQL Server Management Studio\Projects\Disk Space Metrics\Data.csv'
with (fieldterminator = ',', rowterminator = '\n')
go


select [Collection Date]
, SUM(KB)/1024/1024 GB
FROM (

select 
	CONVERT(VARCHAR(50), [Date], 103) [Collection Date]
	,SUM(totalkb) KB--/1024/1024 GB
from (
SELECT
	collection_date as [Date] --SUBSTRING(convert(nvarchar(100),collection_date), 0, 7) 
	,SUM(data_kb) + SUM(index_kb) [Totalkb]
	, collection_date
FROM #AATStats 
--where database_name in ('BRITISHCOUNCIL_CPProjectAdmin','BRITISHCOUNCIL_ItemBank','BRITISHCOUNCIL_SecureAssess','BRITISHCOUNCIL_SecureMarker','BRITISHCOUNCIL_SurpassDataWarehouse','BritishCouncil_TestPackage','BTLSURPASSAUDIT_BRITISHCOUNCIL','eFlex','EPCAdaptor','LocalScan','PhoneExamSystemContext','ReportServer','ReportServerTempDB')
where server_name in ('430088-BC-SQL\SQL1','430326-BC-SQL2\SQL2')
 group by collection_date

	) A
	group by Date
	--order by Date
) B
group by [Collection Date]
order by [Collection Date]



DECLARE @Date1 datetime = '18 January 2016'
DECLARE @Date2 datetime = '04 April 2016'

SELECT DATEDIFF(DAY,@Date1, @Date2)

--141gb in 77 days
--~13 gb per month

--~54.93506493506494 per month