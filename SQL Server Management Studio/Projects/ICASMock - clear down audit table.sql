IF OBJECT_ID('tempdb..#ClearDownAuditing') IS NOT NULL DROP TABLE #ClearDownAuditing;

select
	ROW_NUMBER() OVER(PARTITION BY ExamSessionID, NewState ORDER BY ExamSessionID, NewState) AS [N]
	, id
	, ExamSessionID
	, NewState
	, StateChangeDate
	, StateInformation
--into #ClearDownAuditing	
from ExamStateChangeAuditTable
--where ExamSessionID IN (
--	select ID
--	from ExamSessionTable
--	where examState = 99
--	);

SELECT *
FROM #ClearDownAuditing
where n <>1 

--SELECT *
--FROM ExamStateChangeAuditTable
--INTO [ExamStateChangeAuditTable_05_02_2018]

DELETE A 
--select DISTINCT A.ID
FROM ExamStateChangeAuditTable A

INNER JOIN	 (

SELECT MAX(N) as N, ID, ExamSessionID, NewState, StateChangeDate
FROM #ClearDownAuditing
GROUP BY ID, ExamSessionID, NewState, StateChangeDate
) B

ON A.ExamSessionID = B.ExamSessionID
	AND A.NewState = B.NewState
	AND A.StateChangeDate = B.StateChangeDate
Where B.N != 1


DROP TABLE #ClearDownAuditing;


