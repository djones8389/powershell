DECLARE @FindChar VARCHAR(1) = '\'


SELECT D.Name
		, F.Name
		, F.physical_name

		, 'ALTER DATABASE [' + D.Name + '] SET OFFLINE; '
			+ 'ALTER DATABASE [' + D.Name + '] MODIFY FILE (NAME = ''' + F.name + ''', NewName =  '''+ 
		 CASE WHEN type_desc = 'ROWS' 
			THEN 	SUBSTRING(F.physical_name, 0, LEN(physical_name) - CHARINDEX(@FindChar,REVERSE(physical_name))+2) + D.Name + '.mdf''' + ');' + 'ALTER DATABASE [' + D.Name + '] SET ONLINE; ' 
			ELSE SUBSTRING(F.physical_name, 0, LEN(physical_name) - CHARINDEX(@FindChar,REVERSE(physical_name))+2) + D.Name + '.ldf''' + ');' + 'ALTER DATABASE [' + D.Name + '] SET ONLINE; ' 
			END AS 'Alter File Name' 
			
FROM sys.master_files as F
inner join sys.databases as D
on D.database_id = F.database_id 
where D.name in ('Demo_SDWH')