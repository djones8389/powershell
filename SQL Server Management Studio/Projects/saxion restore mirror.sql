SET NOCOUNT ON; 

DECLARE @BackupFile NVARCHAR(255), 
            @Suffix NVARCHAR(10),
            @Prefix NVARCHAR(10),
			@MediaPassword NVARCHAR(100),
			@DefaultDataLoc NVARCHAR(512),
            @DefaultLogLoc NVARCHAR(512),
			@SQL NVARCHAR(4000),
			@Password nvarchar(35);

SET @Password = '';
SET @BackupFile = N'T:\Backup\Saxion.ALL.2018.02.02.bak'
SET @Suffix = N'';
SET @Prefix = N'';

BEGIN TRY
	RESTORE FILELISTONLY FROM DISK = @BackupFile 
	SET @MediaPassword = ''
END TRY
BEGIN CATCH
	SET @MediaPassword = 'MEDIAPASSWORD = '''+ @Password+ ''''
END CATCH


EXECUTE xp_instance_regread N'HKEY_LOCAL_MACHINE',N'Software\Microsoft\MSSQLServer\MSSQLServer',N'DefaultData', @DefaultDataLoc OUTPUT;
EXECUTE xp_instance_regread N'HKEY_LOCAL_MACHINE',N'Software\Microsoft\MSSQLServer\MSSQLServer',N'DefaultLog', @DefaultLogLoc OUTPUT;

IF (@DefaultDataLoc IS NULL OR @DefaultLogLoc IS NULL) Select 'Default Locations need defining!!!'
	--set @DefaultDataLoc= 'S:\Data'
	--set @DefaultLogLoc = 'L:\Log'

DECLARE @Header TABLE (
      BackupName NVARCHAR(128)
      ,BackupDescription NVARCHAR(255)
      ,BackupType SMALLINT
      ,ExpirationDate DATETIME
      ,Compressed BIT
      ,Position SMALLINT
      ,DeviceType TINYINT
      ,UserName NVARCHAR(128)
      ,ServerName NVARCHAR(128)
      ,DatabaseName NVARCHAR(128)
      ,DatabaseVersion INT
      ,DatabaseCreationDate DATETIME
      ,BackupSize NUMERIC(20, 0)
      ,FirstLSN NUMERIC(25, 0)
      ,LastLSN NUMERIC(25, 0)
      ,CheckpointLSN NUMERIC(25, 0)
      ,DatabaseBackupLSN NUMERIC(25, 0)
      ,BackupStartDate DATETIME
      ,BackupFinishDate DATETIME
      ,SortOrder SMALLINT
      ,CodePage SMALLINT
      ,UnicodeLocaleId INT
      ,UnicodeComparisonStyle INT
      ,CompatibilityLevel TINYINT
      ,SoftwareVendorId INT
      ,SoftwareVersionMajor INT
      ,SoftwareVersionMinor INT
      ,SoftwareVersionBuild INT
      ,MachineName NVARCHAR(128)
      ,Flags INT
      ,BindingID UNIQUEIDENTIFIER
      ,RecoveryForkID UNIQUEIDENTIFIER
      ,Collation NVARCHAR(128)
      ,FamilyGUID UNIQUEIDENTIFIER
      ,HasBulkLoggedData BIT
      ,IsSnapshot BIT
      ,IsReadOnly BIT
      ,IsSingleUser BIT
      ,HasBackupChecksums BIT
      ,IsDamaged BIT
      ,BeginsLogChain BIT
      ,HasIncompleteMetaData BIT
      ,IsForceOffline BIT
      ,IsCopyOnly BIT
      ,FirstRecoveryForkID UNIQUEIDENTIFIER
      ,ForkPointLSN NUMERIC(25, 0) NULL
      ,RecoveryModel NVARCHAR(60)
      ,DifferentialBaseLSN NUMERIC(25, 0) NULL
      ,DifferentialBaseGUID UNIQUEIDENTIFIER
      ,BackupTypeDescription NVARCHAR(60)
      ,BackupSetGUID UNIQUEIDENTIFIER NULL
      ,CompressedBackupSize NUMERIC(20, 0)
      );

IF @MediaPassword = '' 
	INSERT INTO @Header
	EXECUTE (N'RESTORE HEADERONLY FROM DISK = N''' + @BackupFile + ''' ');
ELSE 
	INSERT INTO @Header
	EXECUTE (N'RESTORE HEADERONLY FROM DISK = N''' + @BackupFile + ''' WITH ' +@MediaPassword+'');



DECLARE Header CURSOR
FOR
SELECT Position
      ,DatabaseName
FROM @Header;

DECLARE @File NVARCHAR(22),
      @DatabaseName NVARCHAR(128),
      @RestoreSize BIGINT = 0;

OPEN Header;

FETCH NEXT
FROM Header
INTO @File
      ,@DatabaseName;

WHILE @@FETCH_STATUS = 0
BEGIN
      DECLARE @FileList TABLE (
            LogicalName NVARCHAR(128)
            ,PhysicalName NVARCHAR(260)
            ,[Type] CHAR(1)
            ,FileGroupName NVARCHAR(128)
            ,Size NUMERIC(20, 0)
            ,MaxSize NUMERIC(20, 0)
            ,FileID BIGINT
            ,CreateLSN NUMERIC(25, 0)
            ,DropLSN NUMERIC(25, 0) NULL
            ,UniqueID UNIQUEIDENTIFIER
            ,ReadOnlyLSN NUMERIC(25, 0) NULL
            ,ReadWriteLSN NUMERIC(25, 0) NULL
            ,BackupSizeInBytes BIGINT
            ,SourceBlockSize INT
            ,FileGroupID INT
            ,LogGroupGUID UNIQUEIDENTIFIER NULL
            ,DifferentialBaseLSN NUMERIC(25, 0) NULL
            ,DifferentialBaseGUID UNIQUEIDENTIFIER
            ,IsReadOnly BIT
            ,IsPresent BIT
            ,TDEThumbPrint VARBINARY(32)
            );

IF @MediaPassword = '' 
      INSERT INTO @FileList
      EXECUTE (N'RESTORE FILELISTONLY FROM DISK = N''' + @BackupFile + ''' WITH FILE =  ' + @File +  ';');
ELSE
	  INSERT INTO @FileList
      EXECUTE (N'RESTORE FILELISTONLY FROM DISK = N''' + @BackupFile + ''' WITH FILE =  ' + @File + ','+ @MediaPassword +';');	

      DECLARE @DataFileName NVARCHAR(128)
            ,@LogFileName NVARCHAR(128);

IF @MediaPassword = ''       
      SET @SQL = N'RESTORE DATABASE [' + @Prefix + @DatabaseName + @Suffix +N'] FROM DISK = N''' + @BackupFile + ''' WITH FILE = ' + @File + ', NOUNLOAD, NOREWIND, REPLACE, STATS = 10,';
ELSE
      SET @SQL = N'RESTORE DATABASE [' + @Prefix + @DatabaseName + @Suffix +N'] FROM DISK = N''' + @BackupFile + ''' WITH FILE = ' + @File + ', NOUNLOAD, NOREWIND, REPLACE, ' + @MediaPassword + ', STATS = 10,';

	  DECLARE @DBName nvarchar(MAX) = @Prefix + @DatabaseName + @Suffix;

	  DECLARE @KillSPIDS  NVARCHAR(MAX) = '';
	  SELECT @KillSPIDS += 'kill ' + convert(varchar(5),spid)		
	  FROM sys.sysprocesses s
	  WHERE db_name(dbid) = @DBName

	  PRINT(@KillSPIDS);

      DECLARE DataFiles CURSOR FOR
            SELECT LogicalName
            FROM @FileList
            WHERE [Type] = N'D';

      OPEN DataFiles;
      
      FETCH NEXT FROM DataFiles INTO @DataFileName;
      WHILE @@FETCH_STATUS = 0
      BEGIN
            SET @SQL = @SQL + N' MOVE '''+@DataFileName+''' TO N'''+@DefaultDataLoc + '\' +@Prefix+@DatabaseName+@Suffix+'.mdf'',';
            FETCH NEXT FROM DataFiles INTO @DataFileName;
      END
      
      CLOSE DataFiles;
      DEALLOCATE DataFiles;

      DECLARE LogFiles CURSOR FOR
           SELECT LogicalName
            FROM @FileList
            WHERE [Type] = N'L';
      
      OPEN LogFiles;
      
      FETCH NEXT FROM LogFiles INTO @LogFileName;
      WHILE @@FETCH_STATUS = 0
      BEGIN
            SET @SQL = @SQL + N' MOVE '''+@LogFileName+''' TO N'''+@DefaultLogLoc + '\' + @Prefix+@DatabaseName+@Suffix+'.ldf''; '
            FETCH NEXT FROM LogFiles INTO @LogFileName;
      END
            
      CLOSE LogFiles;
      DEALLOCATE LogFiles;

      SELECT @RestoreSize = @RestoreSize + SUM(Size)
      FROM @FileList;

      --SET @SQL = LEFT(@SQL, (LEN(@SQL) - 1)) + ';';

      PRINT @SQL;

      DELETE
      FROM @FileList;

      FETCH NEXT
      FROM Header
      INTO @File
            ,@DatabaseName;
END

CLOSE Header;

DEALLOCATE Header;

 
RESTORE DATABASE [Saxion_AnalyticsManagement] FROM DISK = N'T:\Backup\Saxion.ALL.2018.02.02.bak' WITH FILE = 1, NOUNLOAD, NOREWIND, REPLACE, STATS = 10, MOVE 'Data' TO N'S:\DATA\Saxion_AnalyticsManagement.mdf', MOVE 'Log' TO N'L:\LOGS\Saxion_AnalyticsManagement.ldf'; 
 
RESTORE DATABASE [Saxion_ContentAuthor] FROM DISK = N'T:\Backup\Saxion.ALL.2018.02.02.bak' WITH FILE = 2, NOUNLOAD, NOREWIND, REPLACE, STATS = 10, MOVE 'Data' TO N'S:\DATA\Saxion_ContentAuthor.mdf', MOVE 'Log' TO N'L:\LOGS\Saxion_ContentAuthor.ldf'; 
kill 70kill 172
RESTORE DATABASE [Saxion_ItemBank] FROM DISK = N'T:\Backup\Saxion.ALL.2018.02.02.bak' WITH FILE = 3, NOUNLOAD, NOREWIND, REPLACE, STATS = 10, MOVE 'Data' TO N'S:\DATA\Saxion_ItemBank.mdf', MOVE 'Log' TO N'L:\LOGS\Saxion_ItemBank.ldf'; 
kill 63kill 74kill 165
RESTORE DATABASE [Saxion_SecureAssess] FROM DISK = N'T:\Backup\Saxion.ALL.2018.02.02.bak' WITH FILE = 4, NOUNLOAD, NOREWIND, REPLACE, STATS = 10, MOVE 'Data' TO N'S:\DATA\Saxion_SecureAssess.mdf', MOVE 'Log' TO N'L:\LOGS\Saxion_SecureAssess.ldf'; 
kill 51kill 57kill 78
RESTORE DATABASE [Saxion_SecureMarker] FROM DISK = N'T:\Backup\Saxion.ALL.2018.02.02.bak' WITH FILE = 5, NOUNLOAD, NOREWIND, REPLACE, STATS = 10, MOVE 'Data' TO N'S:\DATA\Saxion_SecureMarker.mdf', MOVE 'Log' TO N'L:\LOGS\Saxion_SecureMarker.ldf'; 
 
RESTORE DATABASE [Saxion_SurpassDataWarehouse] FROM DISK = N'T:\Backup\Saxion.ALL.2018.02.02.bak' WITH FILE = 6, NOUNLOAD, NOREWIND, REPLACE, STATS = 10, MOVE 'Data' TO N'S:\DATA\Saxion_SurpassDataWarehouse.mdf', MOVE 'Log' TO N'L:\LOGS\Saxion_SurpassDataWarehouse.ldf'; 
kill 61kill 68kill 161
RESTORE DATABASE [Saxion_SurpassManagement] FROM DISK = N'T:\Backup\Saxion.ALL.2018.02.02.bak' WITH FILE = 7, NOUNLOAD, NOREWIND, REPLACE, STATS = 10, MOVE 'Data' TO N'S:\DATA\Saxion_SurpassManagement.mdf', MOVE 'Log' TO N'L:\LOGS\Saxion_SurpassManagement.ldf'; 
go
USE [Saxion_SecureMarker]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'Saxion_SecureMarkerUser') CREATE USER [Saxion_SecureMarkerUser]FOR LOGIN [Saxion_SecureMarkerUser]; exec sp_addrolemember'db_owner', 'Saxion_SecureMarkerUser' 
USE [Saxion_SurpassDataWarehouse]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'Saxion_ETLUser') CREATE USER [Saxion_ETLUser]FOR LOGIN [Saxion_ETLUser]; exec sp_addrolemember'db_owner', 'Saxion_ETLUser' 
USE [Saxion_SurpassDataWarehouse]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'Saxion_ETLUser') CREATE USER [Saxion_ETLUser]FOR LOGIN [Saxion_ETLUser]; exec sp_addrolemember'db_datareader', 'Saxion_ETLUser' 
USE [Saxion_SurpassDataWarehouse]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'Saxion_ETLUser') CREATE USER [Saxion_ETLUser]FOR LOGIN [Saxion_ETLUser]; exec sp_addrolemember'db_datawriter', 'Saxion_ETLUser' 
USE [Saxion_AnalyticsManagement]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'Saxion_AnalyticsManagementUser') CREATE USER [Saxion_AnalyticsManagementUser]FOR LOGIN [Saxion_AnalyticsManagementUser]; exec sp_addrolemember'db_owner', 'Saxion_AnalyticsManagementUser' 
USE [Saxion_SurpassManagement]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'Saxion_SurpassManagementUser') CREATE USER [Saxion_SurpassManagementUser]FOR LOGIN [Saxion_SurpassManagementUser]; exec sp_addrolemember'db_owner', 'Saxion_SurpassManagementUser' 
USE [Saxion_SurpassManagement]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'Saxion_ETLUser') CREATE USER [Saxion_ETLUser]FOR LOGIN [Saxion_ETLUser]; exec sp_addrolemember'db_datareader', 'Saxion_ETLUser' 
USE [Saxion_ContentAuthor]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'Saxion_ContentAuthorUser') CREATE USER [Saxion_ContentAuthorUser]FOR LOGIN [Saxion_ContentAuthorUser]; exec sp_addrolemember'db_owner', 'Saxion_ContentAuthorUser' 
USE [Saxion_ContentAuthor]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'Saxion_ETLUser') CREATE USER [Saxion_ETLUser]FOR LOGIN [Saxion_ETLUser]; exec sp_addrolemember'db_datareader', 'Saxion_ETLUser' 
USE [Saxion_ItemBank]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'Saxion_ETLUser') CREATE USER [Saxion_ETLUser]FOR LOGIN [Saxion_ETLUser]; exec sp_addrolemember'ETLRole', 'Saxion_ETLUser' 
USE [Saxion_ItemBank]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'Saxion_ItemBankUser') CREATE USER [Saxion_ItemBankUser]FOR LOGIN [Saxion_ItemBankUser]; exec sp_addrolemember'db_owner', 'Saxion_ItemBankUser' 
USE [Saxion_ItemBank]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'Saxion_ETLUser') CREATE USER [Saxion_ETLUser]FOR LOGIN [Saxion_ETLUser]; exec sp_addrolemember'db_datareader', 'Saxion_ETLUser' 
USE [Saxion_ItemBank]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'Saxion_ETLUser') CREATE USER [Saxion_ETLUser]FOR LOGIN [Saxion_ETLUser]; exec sp_addrolemember'db_datawriter', 'Saxion_ETLUser' 
USE [Saxion_SecureAssess]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'Saxion_ETLUser') CREATE USER [Saxion_ETLUser]FOR LOGIN [Saxion_ETLUser]; exec sp_addrolemember'db_datareader', 'Saxion_ETLUser' 
