USE [master]
GO

CREATE DATABASE [ServiceBroker];
GO

ALTER DATABASE [ServiceBroker] SET ENABLE_BROKER WITH NO_WAIT;

USE [ServiceBroker];
GO

--Add message types

CREATE MESSAGE TYPE [RequestMessage] validation = WELL_FORMED_XML;
CREATE MESSAGE TYPE [ReplyMessage] validation = WELL_FORMED_XML;

--Now we can make a contract

CREATE CONTRACT [Contract] (
	[RequestMessage] sent BY Initiator
	,[ReplyMessage] sent BY Target
);

--Now we have a contract, we can create queues.  
--One to send the message from, and one for the message to go too.

	CREATE QUEUE InitiatorQueue;
	CREATE QUEUE TargetQueue;

--Now we can create services on the queues

	CREATE SERVICE [InitiatorService] ON QUEUE InitiatorQueue;
	CREATE SERVICE [TargetService] ON QUEUE TargetQueue;

--Now we can send messages from Initiator to Target.

	DECLARE @conversationId UNIQUEIDENTIFIER
	DECLARE @RequestMessage XML

	BEGIN DIALOG @conversationId
	FROM SERVICE [InitiatorService] TO SERVICE 'TargetService' ON CONTRACT [Contract]
	WITH ENCRYPTION = OFF;

	SELECT @conversationId AS conversationId;

--Prepare the Message

	SET @RequestMessage = N'<RequestMessage> Send a Message to Target </RequestMessage>';

--Send the Message

	SEND ON CONVERSATION @conversationId MESSAGE TYPE [RequestMessage](@RequestMessage);

	SELECT @RequestMessage AS SentRequestMessage;	

--Stop here......

	SELECT * FROM InitiatorQueue;
	SELECT * FROM TargetQueue;

--At this point something is wrong.  The message is still in the InitiatorQueue

	SELECT conversation_handle
		,message_sequence_number
		,STATUS
		,validation
		,cast(message_body AS XML) message_body
	FROM InitiatorQueue;

--Neither service supports the contract!

	ALTER SERVICE [InitiatorService] (ADD CONTRACT [Contract]);
	ALTER SERVICE [TargetService] (ADD CONTRACT [Contract]);
	

	SELECT * FROM InitiatorQueue;
	SELECT * FROM TargetQueue;

--The message is still in the queue.   This is now what's called a poison message.   It's broken!
	
	RECEIVE TOP (1) conversation_group_id
	FROM InitiatorQueue;

	SELECT * FROM InitiatorQueue;
	SELECT * FROM TargetQueue;

--OK, great now the queue's are empty. Let's try sending the message again.

	DECLARE @conversationId UNIQUEIDENTIFIER
	DECLARE @RequestMessage XML

	BEGIN TRANSACTION

	--Determine the Initiator Service, Target Service and the Contract 
		BEGIN DIALOG @conversationId
			FROM service [InitiatorService] TO service 'TargetService' ON contract [Contract]
			WITH ENCRYPTION = OFF;

			SELECT @conversationId AS conversationId;

			--Prepare the Message
			SET @RequestMessage = N'<RequestMessage> Send a Message to Target </RequestMessage>';

			--Send the Message
			SEND ON CONVERSATION @conversationId MESSAGE TYPE [RequestMessage](@RequestMessage);

			SELECT @RequestMessage AS SentRequestMessage;

	COMMIT TRANSACTION

		--to here
		SELECT * FROM InitiatorQueue;

		SELECT * FROM TargetQueue;

		--the message is in the TargetQueue now!   Great.
		RECEIVE TOP (1) Conversation_Handle
			,cast(Message_Body AS XML)
			,Message_Type_Name
		FROM TargetQueue;

		End conversation  @conversationId --'6B7C37E2-E263-E811-81E8-5404A6849B7E'
		GO
/*	
	Great at this point, we can stop.  There is no reason why we cannot write a program that will receive messages one at a time of the queue, and process them.
	This technique is called "EXTERNAL ACTIVATION".   Something external to sql server will receive and process the messages from the service broker queue.
	But what is we could get the database to do all the work directly without needing to do anything external?
	This is called INTERAL ACTIVATION.   Let's set that up now. For this we will need a stored procedure to process the queue.
*/
		CREATE PROCEDURE TargetActivProc
		AS
		DECLARE @RecvReqDlgHandle UNIQUEIDENTIFIER;
		DECLARE @RecvReqMsg XML;
		DECLARE @RecvReqMsgName SYSNAME;

		WHILE (1 = 1)
		BEGIN
			BEGIN TRANSACTION;

			WAITFOR (
					RECEIVE TOP (1) @RecvReqDlgHandle = conversation_handle
					,@RecvReqMsg = message_body
					,@RecvReqMsgName = message_type_name FROM TargetQueue
					)
				,timeout 5000;

			IF (@@rowcount = 0)
			BEGIN
				ROLLBACK TRANSACTION;

				BREAK;
			END

			IF @RecvReqMsgName = N'RequestMessage'
			BEGIN
				DECLARE @ReplyMsg XML;

				SET @ReplyMsg = '<ReplyMsg>Message for Initiator service.</ReplyMsg>';

				SEND ON CONVERSATION @RecvReqDlgHandle MESSAGE TYPE [ReplyMessage](@ReplyMsg);
			END
			ELSE IF @RecvReqMsgName = N'http://schemas.microsoft.com/SQL/ServiceBroker/EndDialog'
			BEGIN
			END CONVERSATION @RecvReqDlgHandle;
		END ELSE

		IF @RecvReqMsgName = N'http://schemas.microsoft.com/SQL/ServiceBroker/Error'
		BEGIN
		END CONVERSATION @RecvReqDlgHandle;
			END

		COMMIT TRANSACTION;END
		GO

ALTER QUEUE TargetQueue
	WITH STATUS = ON
		,ACTIVATION (
			PROCEDURE_NAME = TargetActivProc
			,EXECUTE AS SELF
				,STATUS = ON
				,MAX_QUEUE_READERS = 10
			);

SELECT * FROM InitiatorQueue;
SELECT * FROM TargetQueue;

-- So let's try sending a another message and we'll see what happens.
--select from here
DECLARE @conversationId UNIQUEIDENTIFIER
DECLARE @RequestMessage XML

	BEGIN TRANSACTION

--Determine the Initiator Service, Target Service and the Contract 
		BEGIN DIALOG @conversationId
			FROM SERVICE [InitiatorService] TO service 'TargetService' ON contract [Contract]
			WITH ENCRYPTION = OFF;

		SELECT @conversationId AS conversationId;

--Prepare the Message

		SET @RequestMessage = N'<RequestMessage> Send a Message to Target </RequestMessage>';

--Send the Message

		SEND ON CONVERSATION @conversationId MESSAGE TYPE [RequestMessage](@RequestMessage);

		SELECT @RequestMessage AS SentRequestMessage;

	COMMIT TRANSACTION

--To here.

	SELECT * FROM InitiatorQueue;
	SELECT * FROM TargetQueue;

	--There's messages in the InitiatorQueue, let's pick up the first message
	--select from here
	DECLARE @RecvReqDlgHandle UNIQUEIDENTIFIER;
	DECLARE @RecvReqMsg XML;
	DECLARE @RecvReqMsgName SYSNAME;

	RECEIVE TOP (1) @RecvReqDlgHandle = Conversation_Handle
		,@RecvReqMsg = Message_Body
		,@RecvReqMsgName = Message_Type_Name
	FROM InitiatorQueue;

	SELECT @RecvReqDlgHandle AS Conversation_Handle
		,@RecvReqMsg AS Message_Body
		,@RecvReqMsgName AS Message_Type_Name

	IF (@RecvReqDlgHandle IS NOT NULL)
	BEGIN
	END conversation @RecvReqDlgHandle

	SELECT 'conversation ended'
END;



SELECT * FROM sys.dm_broker_activated_tasks;
SELECT * FROM sys.dm_broker_connections;
SELECT * FROM sys.dm_broker_forwarded_messages;
SELECT * FROM sys.dm_broker_queue_monitors;
SELECT * FROM sys.transmission_queue;
SELECT * FROM sys.conversation_endpoints;
SELECT * FROM sys.conversation_groups;
SELECT * FROM sys.service_message_types;
SELECT * FROM sys.service_queue_usages;
SELECT * FROM sys.services;