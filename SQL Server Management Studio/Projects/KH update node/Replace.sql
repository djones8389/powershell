--DECLARE @XMLHOLDER TABLE (id int, AssessmentID int,myxml nvarchar(max))

if OBJECT_ID('tempdb..#XMLHOLDER') IS NOT NULL DROP TABLE #XMLHOLDER;


CREATE TABLE #XMLHOLDER (id int, AssessmentID int, myxml xml)

INSERT #XMLHOLDER 
SELECT top 7000 id, AssessmentID, Assessment--cast(Assessment as nvarchar(max))
FROM AssessmentAuditTable (NOLOCK)
--where Assessment.value('data(Assessment/ID)[1]','smallint') != AssessmentID
	

UPDATE #XMLHOLDER 
--SET myxml =  REPLACE(myxml, SUBSTRING(myxml , CHARINDEX('<ID>',myxml),CHARINDEX('</ID>',myxml)-LEN(CHARINDEX('<ID>',myxml))-6), '<ID>' + convert(nvarchar(max),AssessmentID) + '</ID>')
SET myxml =  REPLACE(convert(nvarchar(MAX), myxml), SUBSTRING(convert(nvarchar(MAX), myxml) , CHARINDEX('<ID>',convert(nvarchar(MAX), myxml)),CHARINDEX('</ID>',convert(nvarchar(MAX), myxml))-LEN(CHARINDEX('<ID>',convert(nvarchar(MAX), myxml)))-6), '<ID>' + convert(nvarchar(max),AssessmentID) + '</ID>')


SELECT SUBSTRING(myxml , CHARINDEX('<ID>',myxml),CHARINDEX('</ID>',myxml)-LEN(CHARINDEX('<ID>',myxml))-6)
from #XMLHOLDER
where AssessmentID = 7091

--SELECT id, Assessment 
--FROM AssessmentAuditTable
--where ID IN (2905,2906)

/*
select
	ID
	, AssessmentID
	, SUBSTRING(myxml , CHARINDEX('<ID>',myxml),CHARINDEX('</ID>',myxml)-LEN(CHARINDEX('<ID>',myxml))-6)
	, REPLACE(myxml, SUBSTRING(myxml , CHARINDEX('<ID>',myxml),CHARINDEX('</ID>',myxml)-LEN(CHARINDEX('<ID>',myxml))-6), '<ID>' + convert(nvarchar(10),AssessmentID))
	from #XMLHOLDER

	*/



--UPDATE #XMLHOLDER
--SET myxml = REPLACE(myxml, SUBSTRING(myxml , CHARINDEX('<ID>',myxml),CHARINDEX('</ID>',myxml)-LEN(CHARINDEX('<ID>',myxml))-6), '<ID>' + convert(nvarchar(10),AssessmentID))


select REPLACE(myxml, SUBSTRING(myxml , CHARINDEX('<ID>',myxml),CHARINDEX('</ID>',myxml)-LEN(CHARINDEX('<ID>',myxml))-6), '<ID>' + convert(nvarchar(10),AssessmentID) + '</ID>') 
from #XMLHOLDER
where ID = 1535

select Assessment, id
from AssessmentAuditTable
where ID = 1183
--select @dynamic +=CHAR(13) +  'UPDATE #XMLHOLDER SET Assessment = ''' +  REPLACE(myxml, SUBSTRING(myxml , CHARINDEX('<ID>',myxml),CHARINDEX('</ID>',myxml)-LEN(CHARINDEX('<ID>',myxml))-6), '<ID>' + convert(nvarchar(10),AssessmentID) + '</ID>') ' '''