SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

use ICAEW_SecureAssess

if OBJECT_ID('tempdb..#itemids') is not null drop table #itemids;

SELECT  wesirt.ID
into #itemids
FROM WAREHOUSE_ExamSessionItemResponseTable wesirt (READUNCOMMITTED)
inner join WAREHOUSE_ExamSessionTable west (READUNCOMMITTED)
on west.id = wesirt.warehouseexamsessionid
where warehouseTime > DATEADD(DAY, -8, GETDATE());
	

create clustered index [ix_id] on #itemids(id);

	SELECT Affected.*
		, KeyCode
	FROM (
		SELECT ColsInUse.ID
			, WAREHOUSEExamSessionID
			, ItemID
			, SUM(TotalColsPerItem.ColWidth) [MaxColWidth]
		FROM (
			SELECT ID	
				, WAREHOUSEExamSessionID
				, ItemID
				, MAX(R) [MaxColInUse]
			FROM (
			select WESIRT.ID
				 , WESIRT.WAREHOUSEExamSessionID
				 , ItemID
				 ,c.d.query('.') a
				, e.f.query('.') b
				, ROW_NUMBER() OVER (PARTITION BY cast(c.d.query('.') as nvarchar(MAX)) ORDER BY (SELECT 1)) R
			from [dbo].WAREHOUSE_ExamSessionItemResponseTable WESIRT
			INNER JOIN #itemids T on T.id = wesirt.ID
			CROSS APPLY WESIRT.ItemResponseData.nodes('p/s/c/i/t/r') c(d)
			CROSS APPLY c.d.nodes('c') e(f)
			--where WESIRT.ID in (select top 100 id from #itemids)
			 where WAREHOUSEExamSessionID = 9836	and itemid = '368P1041'
			) ColsInUse
			where b.value('data(c/text)[1]','nvarchar(MAX)') IS NOT NULL
			group by ID
				 , WAREHOUSEExamSessionID
				 , ItemID
		) ColsInUse
	
		INNER JOIN (	
			SELECT ID
				,[ColNo]
				,[ColWidth]
			FROM (
			SELECT WESIRT.ID	
				, WESIRT.WAREHOUSEExamSessionID
				, a.b.value('@w','int') [ColWidth]
				, ROW_NUMBER() OVER(partition by WESIRT.ID, WESIRT.WAREHOUSEExamSessionID ORDER BY (SELECT 1)) [ColNo] 
			FROM   WAREHOUSE_ExamSessionItemResponseTable WESIRT (NOLOCK)
			INNER JOIN #itemids T on T.id = wesirt.ID
			CROSS APPLY WESIRT.ItemResponseData.nodes('p/s/c/i/t/r/c[@w]') a(b)
			where WAREHOUSEExamSessionID = 9836	and itemid = '368P1041'
			) B
		) TotalColsPerItem
		on TotalColsPerItem.ID = ColsInUse.ID
			and TotalColsPerItem.ColNo = ColsInUse.MaxColInUse
		group by  ColsInUse.ID
			, WAREHOUSEExamSessionID
			, ItemID
	) Affected
	INNER JOIN WAREHOUSE_ExamSessionTable WEST
	on WEST.ID = Affected.WAREHOUSEExamSessionID
	--where MaxColWidth > 972;
	

	--where WESIRT.ID in (
	--	SELECT  wesirt.ID
	--	FROM WAREHOUSE_ExamSessionItemResponseTable wesirt (READUNCOMMITTED)
	--	inner join WAREHOUSE_ExamSessionTable west (READUNCOMMITTED)
	--	on west.id = wesirt.warehouseexamsessionid
	--	where warehouseTime > DATEADD(WEEK, -1, GETDATE())
	--	)