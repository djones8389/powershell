SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

use ICAEW_SecureAssess

	SELECT WESIRT.ID	
		, WESIRT.WAREHOUSEExamSessionID
		, ItemID
		, c.d.value('.','nvarchar(max)') [ColText]
		, ROW_NUMBER() OVER(partition by WESIRT.ID, WESIRT.WAREHOUSEExamSessionID, itemid,CAST(c.d.query('..') as nvarchar(MAX)) ORDER BY CAST(c.d.query('..') as nvarchar(MAX))) [ColNo] 
	FROM   WAREHOUSE_ExamSessionItemResponseTable WESIRT (NOLOCK)

	CROSS APPLY  WESIRT.ItemResponseData.nodes('p/s/c/i/t/r/c') c(d)

	--where  WAREHOUSEExamSessionID = 9836 and itemid in ('368P1041', '368P998')
		WHERE ISNULL(c.d.value('./@typ', 'tinyint'), 3) = 3 --filters WP
			and id = 77743
			and  c.d.value('.','nvarchar(max)')  <> ''
			--where itemid = '368p1037'
			--	and keycode = '9RPXW8F6'

		IF OBJECT_ID('tempdb..#itemids') is not null DROP TABLE #itemids;


		SELECT WESIRT.ID	
			, WESIRT.WAREHOUSEExamSessionID
			, Keycode
			, ItemID
			, itemresponsedata
		---into #itemids
		FROM WAREHOUSE_ExamSessionItemResponseTable wesirt (READUNCOMMITTED)
		inner join WAREHOUSE_ExamSessionTable west (READUNCOMMITTED)
		on west.id = wesirt.warehouseexamsessionid
		where warehouseTime > DATEADD(DAY, -14, GETDATE())
			and  ItemResponseData.exist('p/s/c[@typ=20]') = 1;       
	   
	   

		CREATE CLUSTERED INDEX [ix_id] on #itemids(id);

		SELECT WESIRT.ID	
			, WESIRT.WAREHOUSEExamSessionID
			
			, ItemID
			,(a.b.value('@w','int')) colwidth
			, ROW_NUMBER() OVER(partition by WESIRT.ID, WESIRT.WAREHOUSEExamSessionID ORDER BY (SELECT 1)) [ColNo] 
		FROM   WAREHOUSE_ExamSessionItemResponseTable WESIRT (NOLOCK)
		INNER JOIN #itemids I ON I.ID = WESIRT.ID 					
		CROSS APPLY WESIRT.ItemResponseData.nodes('p/s/c/i/t/r/c[@w]') a(b)
		--WHERE id = 77743


		SELECT WESIRT.ID	
			, WESIRT.WAREHOUSEExamSessionID
			, Keycode
			, ItemID
			, itemresponsedata
		FROM   WAREHOUSE_ExamSessionItemResponseTable WESIRT (NOLOCK)
		INNER JOIN #itemids I ON I.ID = WESIRT.ID 					
		--CROSS APPLY WESIRT.ItemResponseData.nodes('p/s/c/i/t/r/c[@w]') a(b)
		--WHERE id = 77743

--SELECT wesirt.ID
--	, west.ID
--	, ItemID
--	, ItemVersion
--	, KeyCode
--	, ItemResponseData
--FROM WAREHOUSE_ExamSessionItemResponseTable wesirt (READUNCOMMITTED)
--inner join WAREHOUSE_ExamSessionTable west (READUNCOMMITTED)
--on west.id = wesirt.warehouseexamsessionid
--where itemid = '368p1037'
--	and keycode = '9RPXW8F6'


