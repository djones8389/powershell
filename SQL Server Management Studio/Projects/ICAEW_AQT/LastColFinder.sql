use icaew_secureAssess

if OBJECT_ID('tempdb..#ExamSessionItemResponseTable') is not null drop table #ExamSessionItemResponseTable;

select ESIRT.*
INTO #ExamSessionItemResponseTable
FROM  ExamSessionItemResponseTable ESIRT  WITH (NOLOCK)

INNER JOIN ExamSessionTable EST  WITH (NOLOCK)
ON EST.ID = ESIRT.ExamSessionID

INNER JOIN ExamStateChangeAuditTable ESCAT WITH (NOLOCK)
ON ESCAT.ExamSessionID = EST.ID
	and newState = 6
where ItemResponseData.exist('p/s/c[@typ=20]') = 1
	and statechangedate > '2017-09-12'
	and ExamState in (9,13,15,16,18)
	--and Keycode = 'RVTFQHF6'


create clustered index [ix_examsessionid] on #ExamSessionItemResponseTable(examsessionid);
create nonclustered index [ix_examsessionid2] on #ExamSessionItemResponseTable(examsessionid,itemid);

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

SELECT EST.id
	, ExamState
	, esirt.ItemID
	, Keycode
	, a.b.value('data(c/text)[1]','nvarchar(max)') [text]
	, BiggestCol.ColWidth
	--, a.b.query('.')
	--, a.b.value('data(c/@typ)[1]','tinyint')
--INTO [DJ_WideColStorage2]

FROM   #ExamSessionItemResponseTable ESIRT  WITH (NOLOCK)

INNER JOIN ExamSessionTable EST  WITH (NOLOCK)
ON EST.ID = ESIRT.ExamSessionID

CROSS APPLY ESIRT.ItemResponseData.nodes('p/s/c/i/t/r') a(b)

INNER JOIN (
		SELECT esirt.ExamSessionID		
			, ItemID
			, sum(a.b.value('@w','int')) [ColWidth]
		FROM  ExamSessionItemResponseTable ESIRT  
			
		CROSS APPLY ESIRT.ItemResponseData.nodes('p/s/c/i/t/r/c[@w]') a(b)

		group by 
				esirt.ID
				, ExamSessionID
				, ItemID
	) BiggestCol
	
	on BiggestCol.ExamSessionID = est.ID
		and BiggestCol.ItemID = ESIRT.ItemID

--where LEN(a.b.value('data(c/text)[1]','nvarchar(max)')) > 420
where LEN(a.b.value('data(c/text)[1]','nvarchar(max)')) > [ColWidth]
	and a.b.value('data(c/@typ)[1]','tinyint') != 4

