use icaew_secureAssess

	SELECT WEST.ExamSessionID
		, 'Archived' ExamState
		, ItemID
		, Keycode
		--, resultData
		--, ItemResponseData
		--, ItemResponseData.query('/p/s/c/i/t/r[1]').value('count (r/c)[1]','smallint')  [Cols]
		--, a.b.query('.').value('data(r/c)[10]','nvarchar(max)')
		, a.b.value('data(c/text)[1]','nvarchar(max)') [text]
		--,warehouseTime
	FROM  Warehouse_ExamSessionItemResponseTable WESIRT  WITH (NOLOCK)
	
	INNER JOIN Warehouse_ExamSessionTable WEST  WITH (NOLOCK)
	ON WEST.ID = WESIRT.WarehouseExamSessionID

	CROSS APPLY WESIRT.ItemResponseData.nodes('p/s/c/i/t/r') a(b)

	where keycode = '9RQMDYF6'
		and itemid = '368P1047'






/*
<r h="25">
  <c w="353" bld="1" typ="4">
    <text>%3CP%20ALIGN%3D%22LEFT%22%3E%3CFONT%20FACE%3D%22Arial%22%20SIZE%3D%2212%22%20COLOR%3D%22%23000000%22%20LETTERSPACING%3D%220%22%20KERNING%3D%220%22%3E%3CB%3E1.1%20%28a%29%3C/B%3E%3C/FONT%3E%3C/P%3E</text>
  </c>
  <c w="199" />
  <c w="212" />
  <c w="218" />
  <c w="193" />
  <c w="70" />
  <c w="70" />
  <c w="70" />
  <c w="70" />
  <c w="70" />
  <c w="70" />
</r>
*/


/*


use icaew_secureAssess

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT WEST.ID
		, ExamState
		, ItemID
		, forename + ' ' + surname
		, Keycode
		, resultData
		--, a.b.value('.','nvarchar(max)') [Response]
		--, ROW_NUMBER() over (partition by WESIRT.WarehouseExamSessionID, WESIRT.itemID order by (SELECT 1)) RowNumber
		
	FROM  ExamSessionItemResponseTable WESIRT  
	
	INNER JOIN ExamSessionTable WEST 
	ON WEST.ID = WESIRT.ExamSessionID

	inner join usertable ut  
	on ut.id = west.userid

	--CROSS APPLY WESIRT.ItemResponseData.nodes('p/s/c/i/t/r/c[1]') a(b)
	WHERE CAST(ItemResponseData AS NVARCHAR(max))  like '%8.5*639%'
	--where  a.b.value('.','nvarchar(max)') like '%8.5*639%'

*/
