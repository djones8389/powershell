/*
	CREATE CLUSTERED INDEX [ColWidth] on [DJ_ColWidthLookup](ID);
	CREATE CLUSTERED INDEX [ID] on [DJ_MergeExcluder] (ID);
	CREATE CLUSTERED INDEX [ID] on [DJ_FullXmls] (ID);
	CREATE NONCLUSTERED INDEX [ColWidth2] on [DJ_ColWidthLookup](ID, ColNo,ColWidth);
*/


	
	select 
		c.ID
		, KeyCode
		, ItemID
		, C.ColNo
		, ColWidth.ColWidth
		, C.RowNumber
		, C.RowHeight
		, C.Response		
	INTO [DJ_QueriedData]
	FROM (
		select b.ID
			, ROW_NUMBER() over(partition by ID, RowNumber order by (SELECT 1)) ColNo
			, RowNumber
			, RowHeight
			, Response
		FROM (
			SELECT A.*
				, n.f.query('.').value('data(.)[1]','nvarchar(MAX)') Response
			FROM (
				SELECT ID
					, a.b.value('@h[1]','nvarchar(max)') [RowHeight]
					, a.b.value('.','nvarchar(max)') FullRowResponse
					, a.b.query('.').query('r/c') x
					, ROW_NUMBER() over (partition by id, warehouseexamsessionid, itemID order by (SELECT 1)) RowNumber
				FROM  [dbo].[DJ_FullXmls]
			
				CROSS APPLY ItemResponseData.nodes('p/s/c/i/t/r') a(b)
			) A	

			CROSS APPLY x.nodes('c') n(f)

			WHERE FullRowResponse <> ''

		) b
	) c
	LEFT JOIN [DJ_MergeExcluder] Exclude
	on Exclude.ID =  C.ID
		and Exclude.ColNum = C.ColNo
		and Exclude.RowNum = C.RowNumber
	INNER JOIN [DJ_ColWidthLookup] ColWidth
	on ColWidth.ID = C.ID
		and ColWidth.ColNo = C.ColNo
    INNER JOIN [dbo].[DJ_FullXmls] I
	ON I.ID = C.ID
	where Response <> ''
		and Exclude.ID IS NULL;
