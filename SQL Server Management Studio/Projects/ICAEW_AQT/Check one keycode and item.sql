use ICAEW_SecureAssess

SELECT WESIRT.ID	
	, WESIRT.WAREHOUSEExamSessionID
	, Keycode
	, ItemID
	, itemresponsedata
FROM WAREHOUSE_ExamSessionItemResponseTable wesirt (READUNCOMMITTED)
inner join WAREHOUSE_ExamSessionTable west (READUNCOMMITTED)
on west.id = wesirt.warehouseexamsessionid
where keycode = 'M489VCF6' and itemid = '368P1048'