SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

--where itemid = '368P1048' and keycode = 'M489VCF6'
--where itemid = '368P1037' and keycode = 'LFW4HWF6'

use ICAEW_SecureAssess

--34 mins

IF OBJECT_ID('tempdb..#itemids') is not null DROP TABLE #itemids;
IF OBJECT_ID('tempdb..#RowSplit') is not null DROP TABLE #RowSplit;


	SELECT WESIRT.ID	
		, WESIRT.WAREHOUSEExamSessionID
		, Keycode
		, ItemID
		, itemresponsedata
	INTO #itemids
	FROM WAREHOUSE_ExamSessionItemResponseTable wesirt (READUNCOMMITTED)
	inner join WAREHOUSE_ExamSessionTable west (READUNCOMMITTED)
	on west.id = wesirt.warehouseexamsessionid
	where warehouseTime > '2017-09-10'
		and  ItemResponseData.exist('p/s/c[@typ=20]') = 1;       

	CREATE CLUSTERED INDEX [PK]	on #itemids (ID);


	CREATE TABLE #RowSplit (id int, rownum int);

	DECLARE RowCommaSplit CURSOR FOR	

	SELECT ID
		, a.b.value('@rows','nvarchar(100)') [MergeRow] --+1
	FROM  #itemids
	CROSS APPLY ItemResponseData.nodes('//merge/m') a(b)

	DECLARE @ID int, @MergeRowString nvarchar(max)

	OPEN RowCommaSplit

	FETCH NEXT FROM RowCommaSplit INTO @ID, @MergeRowString
	WHILE @@FETCH_STATUS = 0

		BEGIN
	
		INSERT #rowsplit(ID, rownum)
		SELECT @ID,Value+1
		FROM ParmsToList(@MergeRowString)

		FETCH NEXT FROM RowCommaSplit INTO @ID, @MergeRowString

		END
	CLOSE RowCommaSplit
	DEALLOCATE RowCommaSplit

	CREATE CLUSTERED INDEX [PK1] on #rowsplit (ID);


	CREATE TABLE #ColSplit (id int, colnum int);

	DECLARE ColCommaSplit CURSOR FOR	

	SELECT ID
		, a.b.value('@cols','nvarchar(100)') [MergeCol] --+1
	FROM  #itemids
	CROSS APPLY ItemResponseData.nodes('//merge/m') a(b)

	DECLARE @ItemResponseID int, @MergeColString nvarchar(max)

	OPEN ColCommaSplit

	FETCH NEXT FROM ColCommaSplit INTO @ItemResponseID, @MergeColString
	WHILE @@FETCH_STATUS = 0

		BEGIN
	
		INSERT #ColSplit(ID, colnum)
		SELECT @ItemResponseID,Value
		FROM ParmsToList(@MergeColString)

		FETCH NEXT FROM ColCommaSplit INTO @ItemResponseID, @MergeColString

		END
	CLOSE ColCommaSplit
	DEALLOCATE ColCommaSplit

	CREATE CLUSTERED INDEX [PK2] on #ColSplit (ID);

SELECT [FULL].ID
	, KeyCode
	, ColCheck.ItemID
	, ColCheck.ColNo
	, ColText	
FROM (

SELECT ID	
	, c.d.value('.','nvarchar(max)') [ColText]
	, len(c.d.value('.','nvarchar(max)')) [ColTextLength]
	, ROW_NUMBER() OVER(partition by ID, WAREHOUSEExamSessionID, itemid,CAST(c.d.query('..') as nvarchar(MAX)) ORDER BY CAST(c.d.query('..') as nvarchar(MAX))) [ColNo] 
FROM #itemids

CROSS APPLY ItemResponseData.nodes('p/s/c/i/t/r/c') c(d)

	WHERE ISNULL(c.d.value('./@typ', 'tinyint'), 3) = 3 --filters WP
		and c.d.value('.','nvarchar(max)') <> ''	
		
) [FULL]

INNER JOIN (

SELECT ID
	   , warehouseexamsessionid		
	   , keycode
	   , ItemID
	   , a.b.value('@w','int') [ColWidth]
	   , ROW_NUMBER() over (partition by ID, WAREHOUSEExamSessionID, itemid order by (SELECT 1)) ColNo
FROM  #itemids

CROSS APPLY ItemResponseData.nodes('p/s/c/i/t/r/c[@w]') a(b)

) ColCheck

on [FULL].ID = ColCheck.ID
	and [FULL].ColNo = ColCheck.ColNo

INNER JOIN (
						--row numbers split out
	SELECT A.*
	FROM (
		SELECT ID
			, a.b.value('.','nvarchar(max)') [Response]
			, ROW_NUMBER() over (partition by id, warehouseexamsessionid, itemID order by (SELECT 1)) RowNumber
		FROM  #itemids

		CROSS APPLY ItemResponseData.nodes('p/s/c/i/t/r') a(b)
	) A
	WHERE [Response] <> ''
) RowInfo
on RowInfo.ID = ColCheck.ID
	and substring(RowInfo.Response, 0, 4) = substring([FULL].ColText, 0, 4)

LEFT JOIN #rowsplit IgnoreMerge
on IgnoreMerge.ID = RowInfo.ID
	and IgnoreMerge.rownum = RowInfo.RowNumber

LEFT JOIN #ColSplit IgnoreMerge2
on IgnoreMerge2.ID = ColCheck.ID
	and IgnoreMerge2.colnum = ColCheck.ColNo

where ColTextLength > (ColWidth/4)*2	
	and IgnoreMerge.ID is null
	and IgnoreMerge2.ID is null;

	










--SELECT A.*
--FROM (
--	SELECT ItemID
--		, Keycode
--		, a.b.value('.','nvarchar(max)') [Response]
--		, ROW_NUMBER() over (partition by id, warehouseexamsessionid, itemID, cast(a.b.query('.') as nvarchar(MAX)) order by (SELECT 1)) RowNumber
--		--, a.b.query('.')
--		, c.d.value('.','nvarchar(max)') [RowText]
--	FROM  #itemids

--	CROSS APPLY ItemResponseData.nodes('p/s/c/i/t/r') a(b)
--	CROSS APPLY a.b.nodes('c') c(d)
--	where a.b.value('.','nvarchar(max)') <> ''
--		and itemid = '368P1048' and keycode = 'M489VCF6'
--) A
--where [RowText] <> ''
--order by RowNumber


--LEFT JOIN (

--	SELECT ID
--		, a.b.value('@rows','nvarchar(100)') [MergeRow] --+1
--		,ItemResponseData
--	FROM  #itemids
--	CROSS APPLY ItemResponseData.nodes('//merge/m') a(b)
--	where ID = 58060
--) IgnoreMerge
