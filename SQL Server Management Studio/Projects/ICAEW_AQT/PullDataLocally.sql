SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

use ICAEW_SecureAssess

IF object_id('tempdb..#itemids') IS NOT NULL
	DROP TABLE #itemids;

CREATE TABLE #itemids (
	ID INT
	,WAREHOUSEExamSessionID INT
	,Keycode NVARCHAR(12)
	,ItemID NVARCHAR(20)
	,itemresponsedata xml
	);

INSERT #itemids
SELECT WESIRT.ID
	,WESIRT.WAREHOUSEExamSessionID
	,Keycode
	,ItemID
	,itemresponsedata
FROM WAREHOUSE_ExamSessionItemResponseTable wesirt(READUNCOMMITTED)
INNER JOIN WAREHOUSE_ExamSessionTable west(READUNCOMMITTED) ON west.id = wesirt.warehouseexamsessionid
WHERE warehouseTime > '2017-09-10'
	AND ItemResponseData.exist('p/s/c[@typ=20]') = 1

CREATE CLUSTERED INDEX [ID] ON [#itemids] (ID);

SELECT c.ID
	,KeyCode
	,ItemID
	,C.ColNo
	,ColWidth.ColWidth
	,C.RowNumber
	,C.RowHeight
	,C.Response
FROM (
	SELECT b.ID
		,ROW_NUMBER() OVER (
			PARTITION BY ID
			,RowNumber ORDER BY (
					SELECT 1
					)
			) ColNo
		,RowNumber
		,RowHeight
		,Response
	FROM (
		SELECT A.*
			,n.f.query('.').value('data(.)[1]', 'nvarchar(MAX)') Response
		FROM (
			SELECT ID
				,a.b.value('@h[1]', 'nvarchar(max)') [RowHeight]
				,a.b.value('.', 'nvarchar(max)') FullRowResponse
				,a.b.query('.').query('r/c') x
				,ROW_NUMBER() OVER (
					PARTITION BY id
					,warehouseexamsessionid
					,itemID ORDER BY (
							SELECT 1
							)
					) RowNumber
			FROM #itemids
			CROSS APPLY ItemResponseData.nodes('p/s/c/i/t/r') a(b)
			where itemid = '368P1048' and keycode = 'M489VCF6'
			) A
		CROSS APPLY x.nodes('c') n(f)
		WHERE FullRowResponse <> ''
		) b
	) c
LEFT JOIN [DJ_MergeExcluder] Exclude ON Exclude.ID = C.ID
	AND Exclude.ColNum = C.ColNo
	AND Exclude.RowNum = C.RowNumber
INNER JOIN [DJ_ColWidthLookup] ColWidth ON ColWidth.ID = C.ID
	AND ColWidth.ColNo = C.ColNo
INNER JOIN #itemids I ON I.ID = C.ID
WHERE Response <> ''
	AND Exclude.ID IS NULL