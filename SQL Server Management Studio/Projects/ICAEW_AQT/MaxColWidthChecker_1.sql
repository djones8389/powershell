SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

use ICAEW_SecureAssess

if OBJECT_ID('tempdb..#itemids') is not null drop table #itemids;

SELECT  wesirt.ID
into #itemids
FROM WAREHOUSE_ExamSessionItemResponseTable wesirt (READUNCOMMITTED)
inner join WAREHOUSE_ExamSessionTable west (READUNCOMMITTED)
on west.id = wesirt.warehouseexamsessionid
where warehouseTime > DATEADD(DAY, -8, GETDATE())
	and  ItemResponseData.exist('p/s/c[@typ=20]') = 1;
	

create clustered index [ix_id] on #itemids(id);

	SELECT Affected.*
		, KeyCode
	FROM (
		SELECT ColsInUse.ID
			, WAREHOUSEExamSessionID
			, ItemID
			, [MaxColInUse]--SUM(TotalColsPerItem.ColWidth) [MaxColWidth]
		FROM (
			SELECT ID	
				, WAREHOUSEExamSessionID
				, ItemID
				, max(MaxCol.ColNo) [MaxColInUse]
			FROM (
			SELECT WESIRT.ID	
				, WESIRT.WAREHOUSEExamSessionID
				, ItemID
				--, c.d.query('..')
				, c.d.value('.','nvarchar(max)') [ColText]
				, ROW_NUMBER() OVER(partition by WESIRT.ID, WESIRT.WAREHOUSEExamSessionID, itemid,CAST(c.d.query('..') as nvarchar(MAX)) ORDER BY CAST(c.d.query('..') as nvarchar(MAX))) [ColNo] 
			FROM   WAREHOUSE_ExamSessionItemResponseTable WESIRT (NOLOCK)

			INNER JOIN #itemids T on T.id = wesirt.ID

			CROSS APPLY  WESIRT.ItemResponseData.nodes('p/s/c/i/t/r/c') c(d)

			--where  WAREHOUSEExamSessionID = 9836 and itemid in ('368P1041', '368P998')
				WHERE ISNULL(c.d.value('./@typ', 'tinyint'), 3) = 3 --filters WP
			) MaxCol

			group by ID	
				, WAREHOUSEExamSessionID
				, ItemID
		) ColsInUse
	
		INNER JOIN (	
			SELECT ID
				,max([ColNo]) [MaxColPerItem]
			FROM (
			SELECT WESIRT.ID	
				, WESIRT.WAREHOUSEExamSessionID
				, ItemID
				, ROW_NUMBER() OVER(partition by WESIRT.ID, WESIRT.WAREHOUSEExamSessionID ORDER BY (SELECT 1)) [ColNo] 
			FROM   WAREHOUSE_ExamSessionItemResponseTable WESIRT (NOLOCK)
			
			INNER JOIN #itemids T on T.id = wesirt.ID
			
			CROSS APPLY WESIRT.ItemResponseData.nodes('p/s/c/i/t/r/c[@w]') a(b)
			
			--where  WAREHOUSEExamSessionID = 9836 and itemid in ('368P1041', '368P998')
				--Does include WP, but this is OK.  It's filtered above.
			) B
			group by ID
		) TotalColsPerItem
		on TotalColsPerItem.ID = ColsInUse.ID
			and TotalColsPerItem.[MaxColPerItem] = ColsInUse.[MaxColInUse]
		--group by  ColsInUse.ID
		--	, WAREHOUSEExamSessionID
		--	, ItemID
	) Affected
	INNER JOIN WAREHOUSE_ExamSessionTable WEST
	on WEST.ID = Affected.WAREHOUSEExamSessionID
	--where MaxColWidth > 972;
	

	--where WESIRT.ID in (
	--	SELECT  wesirt.ID
	--	FROM WAREHOUSE_ExamSessionItemResponseTable wesirt (READUNCOMMITTED)
	--	inner join WAREHOUSE_ExamSessionTable west (READUNCOMMITTED)
	--	on west.id = wesirt.warehouseexamsessionid
	--	where warehouseTime > DATEADD(WEEK, -1, GETDATE())
	--	)