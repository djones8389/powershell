IF OBJECT_ID('tempdb..#csv') IS NOT NULL DROP TABLE #csv

create table #csv

(
	id int
	, WarehouseExamSessionID smallint
	, ItemID  nvarchar(20)
	, MaxColinUse smallint
	, Keycode  nvarchar(20) 
);


bulk insert #csv
from 'T:\columns2.csv'
with (fieldterminator = ',', rowterminator = '\n')
go

SELECT a.*
	, qualificationName
	, examName
FROM #csv a
inner join WAREHOUSE_ExamSessionTable west
on west.id = a.WarehouseExamSessionID
inner join WAREHOUSE_ExamSessionTable_Shreded wests
on wests.examSessionId = west.ID