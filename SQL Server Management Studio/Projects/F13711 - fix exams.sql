select west.id	
	, wests.warehouseexamstate
	, wests.previousexamstate
	, qualificationlevel
	,reMarkStatus
	,wests.ExportToSecureMarker
	,closeValue
	,passValue
	,actualDuration
	,scheduledDurationValue
	, examstatechangeauditxml
	, structurexml
from warehouse_examsessiontable west
inner join WAREHOUSE_ExamSessionTable_Shreded wests
on west.id = wests.examsessionid 
where keycode = 'JK7MD75S'

select ID
into #update
from warehouse_examsessiontable
where previousexamstate = 9
	and exporttosecuremarker = 0

update warehouse_examsessiontable
set previousexamstate = 12
from warehouse_examsessiontable a
inner join #update b
on a.id = b.id

update warehouse_examsessiontable_Shreded
set previousexamstate = 12
from warehouse_examsessiontable_Shreded a
inner join #update b
on a.examSessionID = b.id


--update warehouse_examsessiontable
--set previousexamstate = 12  9
--where id = 283

--update WAREHOUSE_ExamSessionTable_Shreded
--set previousexamstate = 12  9
--where examSessionId = 283

