USE PRV_AAT_ContentProducer;

SELECT A.*
FROM (

	SELECT A.ID
		, b.c.value('data(clientDataTime/text())[1]','datetime') [clientDataTime]
		, b.c.value('data(checkedInStatus/text())[1]','nvarchar(20)') [checkedInStatus]
	FROM (
		SELECT ID
			,	CONVERT (XML,ItemValue) [ItemValue]
		FROM PageTable
		WHERE SUBSTRING(ID, 0, CHARINDEX('P', ID)) = 930
	) A
	CROSS APPLY ItemValue.nodes('ItemValue/metaData/HISTORY/USER') b(c)
) A

