SELECT  R.[Id]    
      ,R.[Name]     
      ,[SiteLevel]
      ,[CentreLevel]
      ,[SubjectLevel]
	  , usc.*
  FROM [Aatpreview_SurpassManagement].[dbo].[Roles] R
  inner join RolePermissions RP
  on RP.RoleId = R.Id
  INNER JOIN [Permissions] p
  on p.Id = rp.PermissionId
  inner join [UserCentreSubjectRoles] usc
  on usc.RoleId = r.id
  inner join users u
  on u.Id = usc.UserId
	
  where R.name in ('Item Writer','Item Reviewer','Item Publisher')
  order by 3
  


  
  select
	u.UserName
	, r.Name
	, r.SiteLevel
	, r.CentreLevel
	, r.SubjectLevel
  from [dbo].[UserCentreSubjectRoles] usc
  inner join Users u
  on u.id = usc.UserId	
  inner join roles r
  on r.Id = usc.RoleId
  inner join RolePermissions rp
  on rp.RoleId = r.Id
  inner join [Permissions] p
  on p.Id = rp.PermissionId
  where r.name in ('Item Writer','Item Reviewer','Item Publisher')