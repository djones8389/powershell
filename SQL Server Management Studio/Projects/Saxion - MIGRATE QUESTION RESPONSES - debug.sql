--MIGRATE QUESTION RESPONSES

USE Saxion_SecureAssess

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

;WITH EST AS (
	SELECT 
		EST.ID ESTID
		,ROW_NUMBER() Over(Partition by EST.ID order by EST.ID) ItemPresentationOrder 
		,T1.Section.value('data(@id)[1]','int') SectionID
		,T2.item.value('data(@id)[1]','nvarchar(50)') CPID
		,T2.item.value('data(@markerUserMark)[1]','nvarchar(50)') markerUserMark
		,T2.item.value('data(@userMark)[1]','nvarchar(50)') userMark
		,T2.item.value('data(@totalMark)[1]','nvarchar(50)') totalMark
		,T2.item.value('data(@viewingTime)[1]','int') viewingTime
		,T2.item.value('data(@userAttempted)[1]','bit') userAttempted
		,T2.item.value('data(@version)[1]','int') CPVersion
		,~T2.item.value('data(@nonScored)[1]','bit') Scored
	FROM WAREHOUSE_ExamSessionTable EST  			
		CROSS APPLY EST.resultData.nodes('/exam/section') as T1(Section)
		CROSS APPLY T1.Section.nodes('item') as T2(item)
		
	WHERE 
		EST.warehouseTime > '1900-01-01 00:00:00.000'    
		AND EST.warehouseTime < '2050-01-01 00:00:00.000'    
		AND EST.warehouseExamState = 5 
		AND PreviousExamState <> 10
		and est.id = 3368
)
SELECT
	ESTID
	,ItemPresentationOrder
	,SectionID
	,CPID
	,CPVersion	
	,ISNULL(
		CASE ISNUMERIC(markerUserMark) 
			WHEN 1 THEN  CAST(markerUserMark AS float) 
			ELSE NULL
		END /
		CASE ISNUMERIC(totalMark) 
			WHEN 1 THEN	
				CASE CAST(totalMark AS float) 
					WHEN 0 THEN 1
					ELSE CAST(totalMark AS float)
				END
			ELSE NULL
		END
		, CASE ISNUMERIC(userMark) 
			WHEN 1 THEN  CAST(userMark AS float) 
			ELSE NULL
		END 
	) Mark
	,viewingTime
	,userAttempted
	,ISNULL(Scored,1) Scored
	,ESIRT.[ItemResponseData] RawResponse
	,NULLIF(ESIRT.[Comments], '') Comments
	--,ESIRT.DerivedResponse DerivedResponse
	--,ESIRT.ShortDerivedResponse
FROM EST
	LEFT JOIN WAREHOUSE_ExamSessionItemResponseTable ESIRT 
		ON EST.ESTID = ESIRT.WAREHOUSEExamSessionID
		AND CPID = ESIRT.ItemID --may not join by version because there can be only one version of item in particular session