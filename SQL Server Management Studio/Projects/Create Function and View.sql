CREATE VIEW [vw_MCQReport] 
AS

SELECT cast(ProjectID as nvarchar(10)) + 'P' + cast(ItemID as nvarchar(10)) [ItemID]
	, ItemUsageCount
	, FacilityValue
from ItemStatsTable
GO


CREATE FUNCTION [fn_MCQReport] 
(
	@ExamPrefix smallint 
	,@ExamSuffix nvarchar(MAX)
)
RETURNS @ItemIDs TABLE (
		ItemID nvarchar(20)
	)
AS 

	BEGIN

		INSERT @ItemIDs(ItemID)
		SELECT REPLACE(REPLACE(cast(a.b.query('.') as NVARCHAR(MAX)), '<XPath>@ID="',''), '"</XPath>','') ItemID
		FROM AssessmentTable AT
		
		CROSS APPLY AssessmentRules.nodes('PaperRules//XPath') a(b)

		WHERE LEFT(a.b.value('.','nvarchar(MAX)'), 3) = '@ID'
			AND AT.ID IN (
					SELECT AT.ID
					FROM [dbo].[AssessmentGroupTable] AGT
					INNER JOIN AssessmentTable AT
					ON AGT.ID = AT.AssessmentGroupID
					where substring(Name, CHARINDEX('-',Name)+1,3) IN (
						SELECT [Value]
						FROM [dbo].[ParmsToList](@ExamSuffix)
					)
					AND  [Name] like '%' + cast(@ExamPrefix as nvarchar(4)) + '%'
				)
		RETURN
	END
GO