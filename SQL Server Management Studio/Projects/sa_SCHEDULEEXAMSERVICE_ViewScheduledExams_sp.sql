USE [PPD_AAT_SecureAssess]

set statistics io on
--ALTER PROCEDURE [dbo].[sa_SCHEDULEEXAMSERVICE_ViewScheduledExams_sp]
	--FILTER HEADER PARAMETERS
declare
	@pageSize						INT,
	@pageIndex						INT,
	@filterType						VARCHAR(3),
	--FILTER MAIN PARAMETERS
	@centres						NVARCHAR(MAX),
	@surname						NVARCHAR(MAX),
	@examName						NVARCHAR(MAX),
	@onlyCreatedByMe				INT,
    @startDateStartRange			NVARCHAR(MAX),
	@startDateEndRange				NVARCHAR(MAX),
	@endDateStartRange				NVARCHAR(MAX),
	@endDateEndRange				NVARCHAR(MAX),
	@startTimeStartRange			NVARCHAR(MAX),
	@startTimeEndRange				NVARCHAR(MAX),
	@endTimeStartRange				NVARCHAR(MAX),
	@endTimeEndRange				NVARCHAR(MAX),
	@dateCreatedStartRange			NVARCHAR(MAX),
	@dateCreatedEndRange			NVARCHAR(MAX),
	@qualification					NVARCHAR(MAX),	
	@sorting						VARCHAR(MAX),
	@showOnlyInvigilatedExams		BIT,
	@invigilated					CHAR(1),
	@groupState						NVARCHAR(MAX),
	--OTHER POSSIBLE
    @candidateSurname				NVARCHAR(MAX),
    @candidateRef					NVARCHAR(MAX),
    @candidateUln					NVARCHAR(MAX),
    @dateOfBirthStartRange			NVARCHAR(MAX),
    @dateOfBirthEndRange			NVARCHAR(MAX),
	--OTHER PARAMETERS
	@includeQualityReviewExams		BIT,
	@centreQualificationLevelsXml	NVARCHAR(MAX),
	@assignedQualificationIdList	NVARCHAR(MAX),	
	@useExamSessions				BIT
	--WITH_ENCRYPTION_REPLACE_ME_FOR_LOCAL--
SET @filterType='AND'
SET @pageIndex=0
SET @pageSize=50
SET @centres=N''
SET @surname=N'%'
SET @examName=N'%'
SET @onlyCreatedByMe=-1
SET @startDateStartRange=N''
SET @startDateEndRange=N''
SET @endDateStartRange=N''
SET @endDateEndRange=N''
SET @startTimeStartRange=N''
SET @startTimeEndRange=N''	
SET @endTimeStartRange=N''
SET @endTimeEndRange=N''
SET @dateCreatedStartRange=N'10 Oct 2016'
SET @dateCreatedEndRange=N'09 Nov 2016'
SET @qualification=N''	
SET @sorting='qualification ASC, examName ASC, dateCreated DESC, surname ASC, candidateRef ASC'
SET @showOnlyInvigilatedExams=N'0'
SET @invigilated=N''
SET @groupState=N''
SET @candidateSurname=N'%'
SET @candidateRef=N'%'
SET @candidateUln=N'%'SET @dateOfBirthStartRange=N''
SET @dateOfBirthEndRange=N''
SET @includeQualityReviewExams=0
SET @centreQualificationLevelsXml=N'<list><i level="1" centre="150" editable="1"/><i level="1" centre="519" editable="1"/><i level="1" centre="183" editable="1"/></list>'
SET @assignedQualificationIdList=N'41,141,147,82,150,33,23,225,233,231,159,22,30,31,19,80,175,100,148,138,235,262,252,234,261,251,27,85,255,246,144,25,96,250,24,88,170,164,154,184,160,156,167,139,142,163,91,103,99,21,84,89,297,32,185,186,162,226,227,187,29,90,249,238,258,28,87,43,86,243,229,230,44,97,240,259,224,146,165,176,168,172,256,257,135,78,104,228,239,244,188,189,95,247,145,26,190,83,151,191,79,102,98,232,20,173,174,153,158,143,166,171,149,155,140,161,152,157,196,169,236,260,253,81,177,101,70,192'
SET @useExamSessions=1

	BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
		SET NOCOUNT ON;
		SET DATEFORMAT dmy;
		--Paging vars
		DECLARE @TotalCount			INT,
				@TotalResultCount	INT,
				@RowStart			INT,
				@RowEnd				INT;
		--Dynamic Sql vars
		DECLARE	@selectDetails				VARCHAR(MAX),
				@whereclause				VARCHAR(MAX),
				@qualityReviewWhereClause	VARCHAR(MAX),
				@pagingwhereclause			VARCHAR(MAX),
				@sqlPrePaging				NVARCHAR(MAX),
				@sqlPostPaging				NVARCHAR(MAX),
				@xmlUserSearch				VARCHAR(MAX),
				@orderby					VARCHAR(MAX),
			    @startDateClause			NVARCHAR(MAX),
				@endDateClause				NVARCHAR(MAX),
				@endDateSelect				NVARCHAR(MAX),
				@startTimeClause			NVARCHAR(MAX),
				@endTimeClause				NVARCHAR(MAX);
		-- Other vars
		DECLARE @myCheckAssignedQualifications	BIT,
				@myCheckQualifications			BIT,
				@myCandidateCountSelect			NVARCHAR(MAX),
				@myCandidateDetailsSelect		NVARCHAR(MAX),
				@myCandidateDetailsPart			NVARCHAR(MAX);
		-- XML Document integer
		DECLARE @hdoc INT;
		BEGIN TRY
			-- SET row request vars
			SET @RowStart = @pageSize * @pageIndex +1;
			SET @RowEnd = @RowStart + @pageSize -1;
			-- Prepare the xml document
			EXEC sp_xml_preparedocument @hdoc OUTPUT, @centreQualificationLevelsXml;
			-- Create the Temp Table for the Centre Qualification Levels
			CREATE TABLE #TEMP_CentreQualificationLevels_TABLE([qualLevel] INT, [centreId] INT);
			INSERT INTO #TEMP_CentreQualificationLevels_TABLE(qualLevel, centreId)
				SELECT *
				FROM OPENXML(@hdoc, '//list/i', 1)
				WITH
				(
					[qualLevel]      int  '@level',
					[centreId]     int  '@centre'
				)
			-- SET @myCheckAssignedQualifications value
			IF(LEN(@assignedQualificationIdList) = 0)
				SET @myCheckAssignedQualifications = 0;
			ELSE
     			SET @myCheckAssignedQualifications = 1;
			-- SET @myCheckQualifications value
			IF(LEN(@qualification) = 0)
				SET @myCheckQualifications = 0;
			ELSE
				SET @myCheckQualifications = 1;

			-- Replace the sorting values with values that map to the DB            
			SET @sorting = REPLACE(@sorting, 'centres', 'centreName')            
			SET @sorting = REPLACE(@sorting, 'qualification', 'qualificationName')            
			SET @sorting = REPLACE(@sorting, 'examName', 'examName')            
			SET @sorting = REPLACE(@sorting, 'examWindow', 'startDate')            
			SET @sorting = REPLACE(@sorting, 'endDate', 'endDate')            
			SET @sorting = REPLACE(@sorting, 'dateCreated', 'dateCreated')            
			SET @sorting = REPLACE(@sorting, 'createdBySurname', 'createdBySurname')            
			SET @sorting = REPLACE(@sorting, 'groupState', 'groupState')            
			-- SET @qualityReviewWhereClause starting value
			IF @includeQualityReviewExams=0
				SET @qualityReviewWhereClause = ' AND dbo.ScheduledExamsTable.qualityReview=0';
			ELSE
				SET @qualityReviewWhereClause = '';       
			-- SET @startDateClause starting value
			IF @startDateStartRange <> ''
				SET @startDateClause =' Convert(DATE, dbo.ScheduledExamsTable.[ScheduledStartDateTime]) >= ''' + @startDateStartRange +'''
									AND Convert(DATE, dbo.ScheduledExamsTable.[ScheduledStartDateTime]) <= ''' + @startDateEndRange +''' ';
			ELSE
				SET @startDateClause ='';
			-- SET @startTimeClause starting value
			IF @startTimeStartRange  <> ''	
				SET @startTimeClause =' dbo.ScheduledExamsTable.[ActiveStartTime] >= ' + @startTimeStartRange + '
									AND dbo.ScheduledExamsTable.[ActiveStartTime] <= ' + @startTimeEndRange;
			ELSE
				SET @startTimeClause ='';
			-- SET @endTimeClause starting value
			IF @endTimeStartRange <> ''
				SET @endTimeClause  = ' dbo.ScheduledExamsTable.[ActiveEndTime] >= ' + @endTimeStartRange + '
									AND dbo.ScheduledExamsTable.[ActiveEndTime] <= ' + @endTimeEndRange;
			-- SET Default values
			SET @myCandidateCountSelect = '';
			SET @myCandidateDetailsSelect = '';
			SET @myCandidateDetailsPart = '';
			SET @myCandidateCountSelect = '(SELECT count(*) FROM [dbo].[ExamSessionTable] where ScheduledExamID = dbo.ScheduledExamsTable.[ID]) as totalCandidates,';
			SET @endDateSelect = 'dbo.ScheduledExamsTable.ScheduledEndDateTime ';
	
			IF(@useExamSessions = 1)
				BEGIN
					SET @myCandidateDetailsSelect = ', (SELECT Forename FROM UserTable WHERE UserTable.ID = ExamSessionTable.UserID) AS firstName,
													   (SELECT Surname FROM UserTable WHERE UserTable.ID = ExamSessionTable.UserID) AS surname,
													   (SELECT CandidateRef FROM UserTable WHERE UserTable.ID = ExamSessionTable.UserID) AS candidateRef,
													   (SELECT isNULL(ULN, '''') FROM UserTable WHERE UserTable.ID = ExamSessionTable.UserID) AS candidateUln,
													   (SELECT DOB FROM UserTable WHERE UserTable.ID = ExamSessionTable.UserID) AS candidateDateOfBirth,
														UserID AS candidateID,
														ExamSessionTable.ID AS examSessionID';
					SET @myCandidateDetailsPart = ', firstName, surname, candidateRef, candidateID, examSessionID, candidateUln, candidateDateOfBirth  ';
					SET @endDateSelect = 'CASE WHEN dbo.ExamSessionTable.IsProjectBased = 1
										  THEN
											 CASE WHEN dbo.ExamSessionTable.StructureXML IS NULL
											 THEN
												 -- no strucutreXML so use durationXML
												 DATEADD(MINUTE, ExamSessionTable.ScheduledDurationXml.value(''(/duration/value)[1]'', ''int'')-1440, dbo.ScheduledExamsTable.ScheduledStartDateTime)
											 ELSE
												 -- use structureXML
												 DATEADD(MINUTE, ExamSessionTable.StructureXML.value(''(/assessmentDetails/scheduledDuration/value)[1]'', ''int'')-1440, dbo.ScheduledExamsTable.ScheduledStartDateTime)
											 END
										  ELSE
											 -- use scheduledExam end date
											 dbo.ScheduledExamsTable.ScheduledEndDateTime
										  END ';
				END
				
			
			-- SET @endDateClause starting value
			IF @endDateEndRange <> ''
				SET @endDateClause =' Convert(DATE, '+@endDateSelect+') >= ''' + @endDateStartRange +'''
								  AND Convert(DATE, '+@endDateSelect+') <= ''' + @endDateEndRange +'''';
			ELSE
				SET @endDateClause ='';
				
			-- SET the selection details. This should map to columns identified in the select xml above, it must include the full select. If all the data cannot be retreived at this level            
			-- then the procedure must be re designed. the use of subqueries and joins is advised for complex queries.            
			-- This select also include the sort order passed in            
			SET @selectDetails = 'SELECT qualificationID AS ''qualificationId'',
										 qualificationName,
										 qualificationLevel,
										 examName,
										 startDate,
										 endDate,
										 scheduledExamEndDate,
										 startTime,
										 endTime,
										 totalCandidates,
										 dateCreated,
										 examId,
										 centreName,
										 centreId,
										 createdById,
										 createdByForename,
										 createdBySurname,
										 scheduledExamId,
										 groupState,
										 examVersionId,
										 Invigilated,
										 ScheduledForInvigilate AS ''scheduledForInvigilate'',
										 StrictControlOnDDA'
										 + @myCandidateDetailsPart + '
								  FROM
								  (            
									  SELECT  ROW_NUMBER() OVER (ORDER BY ' + @sorting + ') as RowNumber,
											  qualificationID,
											  qualificationName,
											  qualificationLevel,
											  examName,
											  startDate,
											  endDate,
											  scheduledExamEndDate,
											  startTime,
											  endTime,
											  totalCandidates,
											  dateCreated,
											  examId,
											  centreName,
											  centreId,
											  createdById,
											  createdByForename,
											  createdBySurname,
											  scheduledExamId,
											  groupState,
											  examVersionId,
											  Invigilated,
											  ScheduledForInvigilate,
											  StrictControlOnDDA'
											  + @myCandidateDetailsPart + '
									  FROM             
									  (
									      SELECT dbo.IB3QualificationLookup.id						AS qualificationID,
												 dbo.IB3QualificationLookup.[QualificationName]	    AS qualificationName,
												 dbo.IB3QualificationLookup.[QualificationLevel]	AS qualificationLevel,
												 dbo.ScheduledExamsTable.[examName]					AS examName,
												 dbo.ScheduledExamsTable.[ScheduledStartDateTime]	AS startDate,'
											 	 + @endDateSelect + '								AS endDate,
												 dbo.ScheduledExamsTable.[ScheduledEndDateTime]		AS scheduledExamEndDate,
												 dbo.ScheduledExamsTable.[ActiveStartTime]			AS startTime,
												 dbo.ScheduledExamsTable.[ActiveEndTime]			AS endTime, '
												 + @myCandidateCountSelect + '
												 dbo.ScheduledExamsTable.[CreatedDateTime]			AS dateCreated,
												 dbo.ScheduledExamsTable.[ExamID]					AS examId,
												 dbo.CentreTable.CentreName							AS centreName,
												 dbo.ScheduledExamsTable.CentreID					AS centreId,
												 dbo.ScheduledExamsTable.CreatedBy					AS createdById,
												 dbo.UserTable.forename								AS createdByForename,
												 dbo.UserTable.surname								AS createdBySurname,
												 dbo.ScheduledExamsTable.[ID]						AS scheduledExamId,
												 dbo.ScheduledExamsTable.[groupState]				AS groupState,
												 dbo.ScheduledExamsTable.[examVersionId]			AS examVersionId,
												 dbo.ScheduledExamsTable.[Invigilated]				AS Invigilated,
												 dbo.ScheduledExamsTable.[ScheduledForInvigilate]	AS ScheduledForInvigilate, 
												 dbo.ScheduledExamsTable.StrictControlOnDDA			AS StrictControlOnDDA'
												 + @myCandidateDetailsSelect;

				IF(@useExamSessions = 1)
					SET @selectDetails = @selectDetails + ' FROM [dbo].[ExamSessionTable]
															INNER JOIN
																[dbo].[ScheduledExamsTable] ON [dbo].[ScheduledExamsTable].ID = [dbo].[ExamSessionTable].ScheduledExamID
															INNER JOIN
																CentreTable on dbo.ScheduledExamsTable.CentreID = dbo.CentreTable.ID
															INNER JOIN
																UserTable on dbo.ScheduledExamsTable.CreatedBy = dbo.UserTable.ID
															INNER JOIN
																IB3QualificationLookup on IB3QualificationLookup.id = dbo.ScheduledExamsTable.qualificationId';
				ELSE
					SET @selectDetails = @selectDetails + ' FROM [dbo].[ScheduledExamsTable]
															INNER JOIN
																CentreTable on dbo.ScheduledExamsTable.CentreID = dbo.CentreTable.ID
															INNER JOIN
																UserTable on dbo.ScheduledExamsTable.CreatedBy = dbo.UserTable.ID
															INNER JOIN
																IB3QualificationLookup on IB3QualificationLookup.id = dbo.ScheduledExamsTable.qualificationId';
            
				-- Split Surname 
				DECLARE @SurnameClause VARCHAR(MAX);
				IF CHARINDEX(' ',@surname) > 0
					SET @SurnameClause = ' (dbo.UserTable.forename LIKE ''' + RTRIM(LTRIM(SUBSTRING(@surname,1 , CHARINDEX(' ', @surname)))) + '%''
										 OR dbo.UserTable.surname LIKE ''%' + LTRIM(RTRIM(SUBSTRING(@surname, CHARINDEX(' ', @surname), len(@surname)))) + ''')';
				ELSE
					SET @SurnameClause = ' (dbo.UserTable.surname LIKE ''' + @surname + ''' OR dbo.UserTable.forename LIKE ''' + @surname + ''')';
	 
				-- Change the operators for createdby here based on @onlyCreatedByMe            
				DECLARE @createdBySearchParam VARCHAR(MAX);
				SET @createdBySearchParam = ' <> -1';

				IF @onlyCreatedByMe <> -1        
					SET @createdBySearchParam = ' = ' + CONVERT(VARCHAR(MAX), @onlyCreatedByMe);

				DECLARE @myCandidateWhereClause VARCHAR(MAX);
				SET @myCandidateWhereClause = '';
				-- Handle filtertype operator scenarios of AND and OR
				IF @filterType = 'AND'
					BEGIN
						IF @centres <> ''
							SET @whereclause =  ' WHERE dbo.CentreTable.ID IN( ' + @centres + ' )   AND  ';
						ELSE
							SET @whereclause =  ' WHERE ';
     
						SET @whereclause =  @whereclause + @SurnameClause +	' AND dbo.ScheduledExamsTable.[examName] LIKE ''' + @examName +'''' +
							CASE @showOnlyInvigilatedExams WHEN 1 THEN
								' AND (dbo.ScheduledExamsTable.invigilated = 1 OR dbo.ScheduledExamsTable.ScheduledForInvigilate = 1) '
							ELSE
								' '
					END;
				IF(@myCheckQualifications = 1)
					SET @whereclause = @whereclause + ' AND (qualificationID IN (' + @qualification + '))';

				IF(@myCheckAssignedQualifications = 1)
					SET @whereclause = @whereclause + ' AND (qualificationID IN (' + @assignedQualificationIdList + '))';

				--IF (@totalNoOfCandidates > 0)
				--	SET @whereclause = @whereclause + ' AND (totalCandidates >= '+ @totalNoOfCandidates +')';

			IF(@useExamSessions = 1)
				BEGIN
					SET @myCandidateWhereClause = ' AND (SELECT Surname FROM UserTable WHERE UserTable.ID = ExamSessionTable.UserID) LIKE ''' + @candidateSurname + '''
													AND (SELECT CandidateRef FROM UserTable WHERE UserTable.ID = ExamSessionTable.UserID) LIKE ''' + @candidateRef + '''
													AND (SELECT isNULL(ULN, '''') FROM UserTable WHERE UserTable.ID = ExamSessionTable.UserID) LIKE ''' + @candidateUln + '''';
					IF(LEN(@dateOfBirthStartRange) > 0)
						SET @myCandidateWhereClause = @myCandidateWhereClause + ' AND (SELECT DOB FROM UserTable WHERE UserTable.ID = ExamSessionTable.UserID) >= Convert(smalldatetime, ''' + @dateOfBirthStartRange + ''')';
					IF(LEN(@dateOfBirthEndRange) > 0)
						SET @myCandidateWhereClause = @myCandidateWhereClause + ' AND (SELECT DOB FROM UserTable WHERE UserTable.ID = ExamSessionTable.UserID) <= Convert(smalldatetime, ''' + @dateOfBirthEndRange + ''') ';
				END
	
			IF @dateCreatedStartRange != ''
				SET @whereclause = @whereclause + ' AND dbo.ScheduledExamsTable.[CreatedDateTime] >= Convert(smalldatetime, ''' + @dateCreatedStartRange +''') ';
			
			IF @dateCreatedEndRange != ''
				SET @whereclause = @whereclause + ' AND dbo.ScheduledExamsTable.[CreatedDateTime] <= Convert(smalldatetime, ''' + @dateCreatedEndRange +''') ';

			IF @startDateClause <> ''
				SET @whereclause = @whereclause + ' AND ' + @startDateClause;

   			IF @endDateClause <> ''
   				SET @whereclause = @whereclause + ' AND ' + @endDateClause;

   			IF @startTimeClause <> ''
   				SET @whereclause = @whereclause + ' AND ' + @startTimeClause;
   				   								
   			IF @endTimeClause <> ''
   				SET @whereclause = @whereclause + ' AND ' + @endTimeClause

			IF @invigilated <> ''
				SET @whereclause = @whereclause + ' AND ( dbo.ScheduledExamsTable.invigilated = '+ CONVERT(NVARCHAR(1), @invigilated) +' OR dbo.ScheduledExamsTable.ScheduledForInvigilate = '+ CONVERT(NVARCHAR(1), @invigilated) + ' ) ';

			IF 	@groupState <> ''
				SET @whereclause = @whereclause + ' AND dbo.ScheduledExamsTable.groupState IN (  '+ @groupState +' ) ';
			
			SET @whereclause = @whereclause + ' AND (dbo.ScheduledExamsTable.groupState = 1 OR dbo.ScheduledExamsTable.groupState = 2)
												AND dbo.ScheduledExamsTable.CreatedBy ' + @createdBySearchParam + '
												' + @qualityReviewWhereClause + @myCandidateWhereClause + '
												AND EXISTS(
															SELECT TOP (1) ID FROM #TEMP_CentreQualificationLevels_TABLE
															INNER JOIN IB3QualificationLookup
																ON IB3QualificationLookup.QualificationLevel = #TEMP_CentreQualificationLevels_TABLE.qualLevel
															WHERE ID = dbo.ScheduledExamsTable.qualificationId
															AND (centreId = 1 OR centreId = dbo.CentreTable.ID)
														   )
													)as ExamDetails) as examSearch';
		END
		-- Build the pre and post paging strings
		SET @pagingwhereclause = ' WHERE RowNumber BETWEEN @RowStart AND @RowEnd ORDER BY ' + @sorting;
		SET @sqlPrePaging = 'SELECT @TotalResultCount =  Count(*) FROM (' + @selectDetails + @whereclause + ') as TotalRowCount';
		-- ** We need to execute as an ansi string for the parameterised sorting to work **
		EXEC sp_executesql @sqlPrePaging, N'@TotalResultCount INT OUTPUT'  , @TotalResultCount OUTPUT                
		-- Required variables set before paging applied            
		SET @TotalCount = CEILING((@TotalResultCount * 1.0) / @PageSize)                     

		DECLARE @xmlSelectStart NVARCHAR(MAX);
		DECLARE @xmlSelectEnd NVARCHAR(MAX);
		SET @xmlSelectStart = 'SELECT ''0'' AS ''@errorCode'',(
								SELECT @PageNumber AS ''@pageIndex'',
										@PageSize AS ''@pageSize'',
										@TotalCount AS ''@totalCount'',
										@TotalResultCount AS ''@totalRecords'',(';
		SET @xmlSelectEnd = ' FOR XML PATH(''exam''), TYPE) FOR XML PATH(''return''), TYPE) FOR XML PATH(''result'')';
		SET @sqlPostPaging = @xmlSelectStart + @selectDetails + @whereclause + @pagingwhereclause + @xmlSelectEnd;
	
		EXEC sp_executesql @sqlPostPaging, N'@RowStart INT, @RowEnd INT, @TotalResultCount INT, @TotalCount INT, @PageNumber INT, @PageSize INT',    @RowStart, @RowEnd, @TotalResultCount, @TotalCount, @PageIndex, @PageSize;
		-- Clean the filter doc from memory
		EXEC sp_xml_removedocument @hdoc;
		-- DROP TEMP TABLES
		DROP TABLE #TEMP_CentreQualificationLevels_TABLE;
	END TRY            
	BEGIN CATCH            
		EXEC sa_SHARED_GetErrorDetails_sp   
	END CATCH;           
END             
            


