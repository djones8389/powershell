DECLARE @command1 nvarchar(1000) = '

USE [?];

select ''?''  as DBName
		, u.name as ''User''
		, (select SUSER_SNAME (u.sid)) as ''Login''
		, r.name  as RoleName
		--, u.*
from sys.database_role_members RM 
	right join sys.database_principals U on U.principal_id = RM.member_principal_id
	left join sys.database_principals R on R.principal_id = RM.role_principal_id
where u.type<>''R''
	and U.type = ''S''
	and r.name IS NOT NULL
	and ''?'' not in (''msdb'',''tempdb'',''model'',''master'')
'


DECLARE @UserLogins TABLE (DBName nvarchar(150), userName nvarchar(150), loginName nvarchar(150), RoleName nvarchar(50))
INSERT @UserLogins
EXEC sp_MSforeachdb  @command1


SELECT 
	 'USE ' + QUOTENAME(DBNAME) + ';'
	+ ' IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = '''+userName+''') CREATE USER ' + QUOTENAME(userName) + 'FOR LOGIN ' + QUOTENAME(loginName) + ';'
	+ ' exec sp_addrolemember''' + rolename + ''', '''  +userName+ ''' '
FROM @UserLogins
where DBName like 'Professional%'



USE [Professional_SecureAssess]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'Professional_SecureAssessUser') CREATE USER [Professional_SecureAssessUser]FOR LOGIN [Professional_SecureAssessUser]; exec sp_addrolemember'db_owner', 'Professional_SecureAssessUser' 
USE [Professional_SecureAssess]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'Professional_ETLUser') CREATE USER [Professional_ETLUser]FOR LOGIN [Professional_ETLUser]; exec sp_addrolemember'db_owner', 'Professional_ETLUser' 
USE [Professional_ItemBank]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'Professional_ETLUser') CREATE USER [Professional_ETLUser]FOR LOGIN [Professional_ETLUser]; exec sp_addrolemember'ETLRole', 'Professional_ETLUser' 
USE [Professional_ItemBank]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'Professional_ItemBankUser') CREATE USER [Professional_ItemBankUser]FOR LOGIN [Professional_ItemBankUser]; exec sp_addrolemember'db_owner', 'Professional_ItemBankUser' 
USE [Professional_ItemBank]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'Professional_ETLUser') CREATE USER [Professional_ETLUser]FOR LOGIN [Professional_ETLUser]; exec sp_addrolemember'db_datareader', 'Professional_ETLUser' 
USE [Professional_ItemBank]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'Professional_ETLUser') CREATE USER [Professional_ETLUser]FOR LOGIN [Professional_ETLUser]; exec sp_addrolemember'db_datawriter', 'Professional_ETLUser' 
USE [Professional_ContentAuthor]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'Professional_ContentAuthorUser') CREATE USER [Professional_ContentAuthorUser]FOR LOGIN [Professional_ContentAuthorUser]; exec sp_addrolemember'db_owner', 'Professional_ContentAuthorUser' 
USE [Professional_ContentAuthor]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'Professional_ETLUser') CREATE USER [Professional_ETLUser]FOR LOGIN [Professional_ETLUser]; exec sp_addrolemember'db_datareader', 'Professional_ETLUser' 
USE [Professional_SurpassManagement]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'Professional_SurpassManagementUser') CREATE USER [Professional_SurpassManagementUser]FOR LOGIN [Professional_SurpassManagementUser]; exec sp_addrolemember'db_owner', 'Professional_SurpassManagementUser' 
USE [Professional_SurpassManagement]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'Professional_ETLUser') CREATE USER [Professional_ETLUser]FOR LOGIN [Professional_ETLUser]; exec sp_addrolemember'db_datareader', 'Professional_ETLUser' 
USE [Professional_AnalyticsManagement]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'Professional_AnalyticsManagementUser') CREATE USER [Professional_AnalyticsManagementUser]FOR LOGIN [Professional_AnalyticsManagementUser]; exec sp_addrolemember'db_owner', 'Professional_AnalyticsManagementUser' 
USE [Professional_SurpassDataWarehouse]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'Professional_ETLUser') CREATE USER [Professional_ETLUser]FOR LOGIN [Professional_ETLUser]; exec sp_addrolemember'db_owner', 'Professional_ETLUser' 
USE [Professional_SecureMarker]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'Professional_SecureMarkerUser') CREATE USER [Professional_SecureMarkerUser]FOR LOGIN [Professional_SecureMarkerUser]; exec sp_addrolemember'db_owner', 'Professional_SecureMarkerUser' 