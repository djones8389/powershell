exec sp_MSforeachdb '

use [?];

IF(''?'' like ''%SecureAssess%'''')

DECLARE @Superuser tinyint = (select id from UserTable where username = ''superuser'')

UPDATE CentreTable
set PrimaryContactID = @Superuser
FROM CentreTable CT
LEFT JOIN UserTable UT
on UT.ID = CT.PrimaryContactID
where UT.ID IS NULL


UPDATE CentreTable
set TechnicalContactID = @Superuser
FROM CentreTable CT
LEFT JOIN UserTable UT
on UT.ID = CT.TechnicalContactID
where UT.ID IS NULL

'



--SELECT * FROM @MissingTables order by 1;

/*
use [?];

IF(''?'' like ''%SecureAssess'' and ''?'' != ''PRV_WJEC_SecureAssess'')

select ''?'' [DBName]
	, CT.ID
	, CT.CentreName
from CentreTable CT
LEFT JOIN UserTable UT
on UT.ID IN (CT.PrimaryContactID, CT.TechnicalContactID)
where UT.ID IS NULL














IF(''?'' like ''%SecureAssess%'' and ''?'' != ''PRV_WJEC_SecureAssess'')

DECLARE @Superuser tinyint = (select id from UserTable where username = ''superuser'')

UPDATE CentreTable
set PrimaryContactID = @Superuser
FROM CentreTable CT
LEFT JOIN UserTable UT
on UT.ID = CT.PrimaryContactID
where UT.ID IS NULL


UPDATE CentreTable
set TechnicalContactID = @Superuser
FROM CentreTable CT
LEFT JOIN UserTable UT
on UT.ID = CT.TechnicalContactID
where UT.ID IS NULL


*/
