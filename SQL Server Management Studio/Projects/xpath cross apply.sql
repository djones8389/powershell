DECLARE @x XML = '<?xml version="1.0" encoding="UTF-8"?>
<StaffingOrder>
  <OrderId validFrom="12/31/2015" validTo="12/31/2015">
    <IdValue>JNJNJP00040440</IdValue>
    <Status>Submitted</Status>
  </OrderId>
  <StaffingPosition>
    <CustomFields>
      <CustomField name="Has Candidate previously worked at Client?" mandatory="yes" type="xsd:pickList">
        <pickList isCompleteList="yes" totalNumberOfItems="4">
          <item>1 - Client retiree</item>
          <item>2 - Former client employee</item>
          <item>3 - Contingent Worker</item>
          <item>No</item>
        </pickList>
        <Value />
        <Class>JobSeeker</Class>
      </CustomField>
    </CustomFields>
  </StaffingPosition>
</StaffingOrder>'

SELECT m.value('(text())[1]', 'varchar(100)')
FROM (VALUES (@x)) xx(x)
CROSS APPLY x.nodes('//item') mm(m);