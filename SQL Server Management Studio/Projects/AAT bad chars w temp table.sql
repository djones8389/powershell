SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

--DECLARE @IDs TABLE (ID int)

--INSERT @IDs (ID)
select west.ID
from WAREHOUSE_ExamSessionItemResponseTable WESIRT
INNER JOIN WAREHOUSE_ExamSessionTable WEST
ON WEST.ID = WESIRT.WAREHOUSEExamSessionID
where warehouseTime > DATEADD(MONTH, -3, getDATE())
		and (
			cast(itemresponseData as nvarchar(max)) like '%table extensionName="HIGHLIGHT_TABLE"%'
			or
			cast(itemresponseData as nvarchar(max)) like '%table extensionName="protean"%'
			);
			

	
select a.*
	, d.e.value('.[1]','nvarchar(max)') 
from (

select west.ID	
	, west.KeyCode
	, ItemID
	, west.warehouseTime
	, cast(replace(replace(cast(ItemResponseData as nvarchar(MAX)), '&lt;','<'), '&gt;','>') as xml) [ItemResponseData]
from WAREHOUSE_ExamSessionItemResponseTable WESIRT
INNER JOIN WAREHOUSE_ExamSessionTable WEST
ON WEST.ID = WESIRT.WAREHOUSEExamSessionID
INNER JOIN @IDs TT
on TT.id = west.ID	
) a
 CROSS APPLY [ItemResponseData].nodes('p/s/c/i/table') b(c)
 CROSS APPLY b.c.nodes('r/c') d(e)
 
 where b.c.value('@extensionName', 'nvarchar(100)') in ('protean','HIGHLIGHT_TABLE')
	and (
		 d.e.value('.[1]','nvarchar(max)') like '%&lt;%'
		 or
		 d.e.value('.[1]','nvarchar(max)') like '%]%'
		 or
		 d.e.value('.[1]','nvarchar(max)') like '%,%'
		 or
		 d.e.value('.[1]','nvarchar(max)') like '%>%'
		 or
		 d.e.value('.[1]','nvarchar(max)') like '%/%'
		 or
		 d.e.value('.[1]','nvarchar(max)') like '%|%'
		 )