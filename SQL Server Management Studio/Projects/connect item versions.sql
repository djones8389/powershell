select count(ID)
	, [version]
from (
	select ID
		, a.b.value('@version','int') [version]
	from (
	select ID
		, cast(structurexml as xml) structurexml
	from examsessiontable NOLOCK
	where examState = 4
	) c
	CROSS APPLY structurexml.nodes('assessmentDetails/assessment/section/item') a(b)
	--where a.b.value('@id','nvarchar(20)') = '5050P2982' and a.b.value('@version','int') <> 10
	where structurexml.exist('assessmentDetails/assessment/section/item[@id="5050P2982"]') = 1  -- and @version != "10"
 ) d
 group by [version]



-- sp_whoisactive
--15903

--select count(ID)
--from examsessiontable NOLOCK
--where examState = 4
--and cast(structurexml as xml).exist('assessmentDetails/assessment/section/item[@id="5050P2982" and @version != "10"]') = 1

--select id, StructureXML, examState
--from ExamSessionTable nolock
--where id = 14633

--update ExamSessionTable
--set previousExamState = examState
--	, examState = 1
--where id = 14633


	--and cast(structurexml as nvarchar(MAX)) like '%5050P2982%'


	--CROSS APPLY cast(structurexml as xml).nodes('assessmentDetails/assessment/section/item') a(b)


select a.DBName
	, examState
	, count(ID) [Count]
FROM (
select db_name() DBName
	, ID
	, examState
from ExamSessionTable NOLOCK
) a
group by DBName,examState
order by 1,2