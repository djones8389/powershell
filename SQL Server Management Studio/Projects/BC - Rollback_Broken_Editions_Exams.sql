/*Rollback option*/



UPDATE Warehouse_ExamSessionTable
set resultData = B.resultData
	, resultDataFull = B.resultDataFull
	, structureXML = B.structureXML
FROM Warehouse_ExamSessionTable A
INNER JOIN [DE19467_Rollback_Examsessions] B
	ON A.ID = B.ID;

UPDATE WAREHOUSE_ExamSessionTable_Shreded
set resultData = B.resultData
	, structureXML = B.structureXML
FROM WAREHOUSE_ExamSessionTable_Shreded A
INNER JOIN [DE19467_Rollback_Examsessions] B
	ON A.examSessionID = B.ID;

DELETE FROM WAREHOUSE_ExamSessionTable_ShrededItems 
WHERE examSessionId IN (1530093,1530092,1530088,1530097,1530086,1530083,1530096,1530094,1530089,1530221,1530091,1530081,1530087,1530219,1530080,1530090,1530220,1530095,1530085,1530084,1530082)

	BEGIN
		INSERT INTO WAREHOUSE_ExamSessionTable_ShrededItems
		 (	 ExamSessionID
			,ItemRef
			,ItemName
			,UserMark
			,MarkerUserMark
			,UserAttempted
			,ItemVersion
			,MarkingIgnored
			,MarkedMetadataCount
			,ExamPercentage
			,TotalMark
			,ResponseXML
			,OptionsChosen
			,SelectedCount
			,CorrectAnswerCount
			,itemType
			,FriendItems)
			SELECT	 WAREHOUSE_ExamSessionTable.ID  AS [examSessionId]
					,Result.Item.value('@id', 'nvarchar(15)') AS [itemRef]
					,Result.Item.value('@name', 'nvarchar(200)') AS [itemName]
					,Result.Item.value('@userMark', 'decimal(6, 3)') AS [userMark]
					,Result.Item.value('@markerUserMark', 'nvarchar(max)') AS [markerUserMark]
					,Result.Item.value('@userAttempted', 'bit') AS [userAttempted]
					,Result.Item.value('@version', 'int') AS [itemVersion]
					,Result.Item.value('@markingIgnored', 'tinyint') AS [markingIgnored]
					,Result.Item.value('count(mark)', 'int') AS [markedMetadataCount]
					,WAREHOUSE_ExamSessionTable.ResultData.value('(exam/@userPercentage)[1]', 'decimal(6, 3)') AS [examPercentage]
					,Result.Item.value('@totalMark', 'decimal(6, 3)') AS [totalMark]
					,WAREHOUSE_ExamSessionItemResponseTable.ItemResponseData AS [responseXml]
					,CAST(ISNULL(WAREHOUSE_ExamSessionItemResponseTable.ItemResponseData.query('data(p/s/c[@typ = "10"]/i[@sl = "1"]/@ac)'), ' UA ') AS nvarchar(200)) AS [optionsChosen] --Why?
					,WAREHOUSE_ExamSessionItemResponseTable.ItemResponseData.value('count(p/s/c/i[@sl = "1"])', 'int') AS [selectedCount]
					,WAREHOUSE_ExamSessionItemResponseTable.ItemResponseData.value('count(p/s/c/i[@ca = "1"])', 'int') AS [correctAnswerCount]
					,Result.Item.value('@type', 'int') AS ItemType
					,Result.Item.value('@SurpassFriendItems', 'NVARCHAR(MAX)') AS FriendItems
			 FROM	 WAREHOUSE_ExamSessionTable
			 CROSS APPLY WAREHOUSE_ExamSessionTable.ResultData.nodes('exam/section/item') Result(Item)
			 LEFT JOIN WAREHOUSE_ExamSessionItemResponseTable -- we want to include nulls in the the item response table so that non attempted items are included
					 ON WAREHOUSE_ExamSessionItemResponseTable.WAREHOUSEExamSessionID = WAREHOUSE_ExamSessionTable.ID
					AND WAREHOUSE_ExamSessionItemResponseTable.ItemID = Result.Item.value('@id', 'nvarchar(15)')
			WHERE	 WAREHOUSE_ExamSessionTable.ID IN (1530093,1530092,1530088,1530097,1530086,1530083,1530096,1530094,1530089,1530221,1530091,1530081,1530087,1530219,1530080,1530090,1530220,1530095,1530085,1530084,1530082);
	END

UPDATE WAREHOUSE_ExamSessionItemResponseTable
SET ItemResponseData = B.ItemResponseData
	, MarkerResponseData = B.MarkerResponseData
	, ItemMark = B.ItemMark
	, DerivedResponse = B.DerivedResponse
	, ShortDerivedResponse = B.ShortDerivedResponse
	, MarkingIgnored = B.MarkingIgnored
FROM WAREHOUSE_ExamSessionItemResponseTable A
INNER JOIN [DE19467_Rollback_Exams_WAREHOUSE_ExamSessionItemResponseTable] B
on A.ID = B.ID
	and A.WAREHOUSEExamSessionID = B.WAREHOUSEExamSessionID
	and A.itemID = B.ItemID;

