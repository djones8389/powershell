DECLARE @P1 [nchar] (4)
	   ,@P2 [nchar] (5)
	   ,@P3 [nchar] (4);

--144442  PQUM_00CBT_2013_05749_2/Practice	LG7PL9B8
SET @P1 = N'PQUM';
SET @P2 = N'00CBT';
SET @P3 = N'2013';

SELECT @P1 + N'0035' + RIGHT((N'0000' + CAST((5 + Centre.Rank) AS [nvarchar](100))), 4) + RIGHT((
			N'0000' + CAST(ROW_NUMBER() OVER (
					PARTITION BY WAREHOUSE_ExamSessionTable_Shreded.CentreCode ORDER BY WAREHOUSE_ExamSessionTable_Shreded.CentreCode
					) AS [nvarchar](100))
			), 4)
	,WAREHOUSE_ExamSessionTable_Shreded.centreCode
	,@P2
	,@P3 + LEFT(WAREHOUSE_ExamSessionTable_Shreded.ExamRef, (CHARINDEX(N'/', WAREHOUSE_ExamSessionTable_Shreded.ExamRef) - 1)) AS ExamVersionRef
	,RIGHT(WAREHOUSE_ExamSessionTable_Shreded.ExamRef, (LEN(WAREHOUSE_ExamSessionTable_Shreded.ExamRef) - CHARINDEX(N'/', WAREHOUSE_ExamSessionTable_Shreded.ExamRef)))
	,N'2946'
	,REPLACE(CONVERT([nchar](10), WAREHOUSE_ExamSessionTable_Shreded.dateOfBirth, 103), N'/', '') AS DateOfBirth
	,WAREHOUSE_ExamSessionTable_Shreded.userMark
	,CAST(WAREHOUSE_ExamSessionTable_Shreded.ResultData.query('data(/exam/section/item/@actualUserMark)') AS [nvarchar](max))
	,WAREHOUSE_ExamSessionTable_Shreded.Surname
	,WAREHOUSE_ExamSessionTable_Shreded.Forename
	,WAREHOUSE_ExamSessionTable_Shreded.Gender
	,REPLACE(CONVERT([nchar](10), WAREHOUSE_ExamSessionTable_Shreded.SubmittedDate, 103), N'/', '') AS TestDate
FROM WAREHOUSE_ExamSessionTable_Shreded
INNER JOIN (
	SELECT ROW_NUMBER() OVER (
			ORDER BY X.CentreCode
			) AS [Rank]
		,CentreCode
	FROM (
		SELECT DISTINCT WAREHOUSE_ExamSessionTable_Shreded.CentreCode
		FROM WAREHOUSE_ExamSessionTable_Shreded
		WHERE WAREHOUSE_ExamSessionTable_Shreded.ExamSessionID IN (
				SELECT WAREHOUSE_ExamSessionTable.ID
				FROM WAREHOUSE_ExamSessionTable
				WHERE WAREHOUSE_ExamSessionTable.ExamSessionID IN (145539)
					AND WAREHOUSE_ExamSessionTable_Shreded.ExamVersionRef LIKE @P1 + N'[_]' + @P2 + N'[_]' + @P3 + N'[_]%'
				)
		) X
	) Centre ON WAREHOUSE_ExamSessionTable_Shreded.CentreCode = Centre.CentreCode
WHERE WAREHOUSE_ExamSessionTable_Shreded.ExamSessionID IN (
		SELECT WAREHOUSE_ExamSessionTable.ID
		FROM WAREHOUSE_ExamSessionTable
		WHERE WAREHOUSE_ExamSessionTable.ExamSessionID IN (145539)
			AND WAREHOUSE_ExamSessionTable_Shreded.ExamVersionRef LIKE @P1 + N'[_]' + @P2 + N'[_]' + @P3 + N'[_]%'
		)

