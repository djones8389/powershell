/****** Script for SelectTopNRows command from SSMS  ******/
SELECT TOP (1000) [Id]
      ,[SourceId]
      ,[Version]
      --,[ExecutionGUID]
      --,[OriginalExecutionGUID]
      ,[PeriodStart]
      ,[PeriodEnd]
      ,[Result]
      ,[Comment]
  FROM [Ocr_SurpassDataWarehouse].[ETL].[ExecutionLog]


--2017-07-27 02:43:35.520

SELECT ID
	, warehouseTime
	, WarehouseExamState
--INTO OCR_SecureAssess..SDW_MissingExams
FROM OCR_SecureAssess..WAREHOUSE_ExamSessionTable
where ID NOT IN (
	SELECT examsessionkey
	FROM [Ocr_SurpassDataWarehouse].dbo.FactExamSessions
)
and WarehouseExamState = 1



SELECT *
FROM OCR_SecureAssess..SDW_MissingExams


USE OCR_SecureAssess

/*Update

UPDATE WAREHOUSE_ExamSessionTable_Shreded
set warehouseTime = getDate()
FROM WAREHOUSE_ExamSessionTable_Shreded A
INNER JOIN SDW_MissingExams B
on A.ExamSessionID = B.ID

UPDATE WAREHOUSE_ExamSessionTable
set warehouseTime = getDate()
FROM WAREHOUSE_ExamSessionTable A
INNER JOIN SDW_MissingExams B
on A.ID = B.ID
*/

/*--Rollback

UPDATE WAREHOUSE_ExamSessionTable_Shreded
set warehouseTime = b.warehouseTime
FROM WAREHOUSE_ExamSessionTable_Shreded A
INNER JOIN SDW_MissingExams B
on A.ExamSessionID = B.ID

UPDATE WAREHOUSE_ExamSessionTable
set warehouseTime = b.warehouseTime
FROM WAREHOUSE_ExamSessionTable A
INNER JOIN SDW_MissingExams B
on A.ID = B.ID

*/