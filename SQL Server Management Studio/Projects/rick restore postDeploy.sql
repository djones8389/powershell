USE MASTER

RESTORE DATABASE [PostDeploy_AnalyticsManagement]
FROM DISK = N'F:\Backup\Demo_From_Rackspace-Keep!!\Demo.All.2018.04.19.bak'
WITH FILE = 1
	,NOUNLOAD
	,NOREWIND
	,REPLACE
	,STATS = 10
	,MOVE 'Data' TO N'F:\Data\PostDeploy_AnalyticsManagement.mdf'
	,MOVE 'Log' TO N'F:\Log\PostDeploy_AnalyticsManagement.ldf';

RESTORE DATABASE [PostDeploy_ContentAuthor]
FROM DISK = N'F:\Backup\Demo_From_Rackspace-Keep!!\Demo.All.2018.04.19.bak'
WITH FILE = 2
	,NOUNLOAD
	,NOREWIND
	,REPLACE
	,STATS = 10
	,MOVE 'Data' TO N'F:\Data\PostDeploy_ContentAuthor.mdf'
	,MOVE 'Log' TO N'F:\Log\PostDeploy_ContentAuthor.ldf';

RESTORE DATABASE [PostDeploy_ItemBank]
FROM DISK = N'F:\Backup\Demo_From_Rackspace-Keep!!\Demo.All.2018.04.19.bak'
WITH FILE = 3
	,NOUNLOAD
	,NOREWIND
	,REPLACE
	,STATS = 10
	,MOVE 'Data' TO N'F:\Data\PostDeploy_ItemBank.mdf'
	,MOVE 'Log' TO N'F:\Log\PostDeploy_ItemBank.ldf';

RESTORE DATABASE [PostDeploy_SecureAssess]
FROM DISK = N'F:\Backup\Demo_From_Rackspace-Keep!!\Demo.All.2018.04.19.bak'
WITH FILE = 4
	,NOUNLOAD
	,NOREWIND
	,REPLACE
	,STATS = 10
	,MOVE 'Data' TO N'F:\Data\PostDeploy_SecureAssess.mdf'
	,MOVE 'Log' TO N'F:\Log\PostDeploy_SecureAssess.ldf';

RESTORE DATABASE [PostDeploy_SecureMarker]
FROM DISK = N'F:\Backup\Demo_From_Rackspace-Keep!!\Demo.All.2018.04.19.bak'
WITH FILE = 5
	,NOUNLOAD
	,NOREWIND
	,REPLACE
	,STATS = 10
	,MOVE 'Data' TO N'F:\Data\PostDeploy_SecureMarker.mdf'
	,MOVE 'Log' TO N'F:\Log\PostDeploy_SecureMarker.ldf';

RESTORE DATABASE [PostDeploy_SurpassDataWarehouse]
FROM DISK = N'F:\Backup\Demo_From_Rackspace-Keep!!\Demo.All.2018.04.19.bak'
WITH FILE = 6
	,NOUNLOAD
	,NOREWIND
	,REPLACE
	,STATS = 10
	,MOVE 'Data' TO N'F:\Data\PostDeploy_SurpassDataWarehouse.mdf'
	,MOVE 'Log' TO N'F:\Log\PostDeploy_SurpassDataWarehouse.ldf';

RESTORE DATABASE [PostDeploy_SurpassManagement]
FROM DISK = N'F:\Backup\Demo_From_Rackspace-Keep!!\Demo.All.2018.04.19.bak'
WITH FILE = 7
	,NOUNLOAD
	,NOREWIND
	,REPLACE
	,STATS = 10
	,MOVE 'Data' TO N'F:\Data\PostDeploy_SurpassManagement.mdf'
	,MOVE 'Log' TO N'F:\Log\PostDeploy_SurpassManagement.ldf';


use master;

USE [PostDeploy_SurpassDataWarehouse]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'PostDeploy_ETLUser') CREATE USER [PostDeploy_ETLUser]FOR LOGIN [PostDeploy_ETLUser]; exec sp_addrolemember'db_owner', 'PostDeploy_ETLUser' 
USE [PostDeploy_AnalyticsManagement]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'PostDeploy_AnalyticsManagementUser') CREATE USER [PostDeploy_AnalyticsManagementUser]FOR LOGIN [PostDeploy_AnalyticsManagementUser]; exec sp_addrolemember'db_owner', 'PostDeploy_AnalyticsManagementUser' 
USE [PostDeploy_SurpassManagement]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'PostDeploy_SurpassManagementUser') CREATE USER [PostDeploy_SurpassManagementUser]FOR LOGIN [PostDeploy_SurpassManagementUser]; exec sp_addrolemember'db_owner', 'PostDeploy_SurpassManagementUser' 
USE [PostDeploy_SurpassManagement]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'PostDeploy_ETLUser') CREATE USER [PostDeploy_ETLUser]FOR LOGIN [PostDeploy_ETLUser]; exec sp_addrolemember'db_datareader', 'PostDeploy_ETLUser' 
USE [PostDeploy_ContentAuthor]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'PostDeploy_ContentAuthorUser') CREATE USER [PostDeploy_ContentAuthorUser]FOR LOGIN [PostDeploy_ContentAuthorUser]; exec sp_addrolemember'db_owner', 'PostDeploy_ContentAuthorUser' 
USE [PostDeploy_ContentAuthor]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'PostDeploy_ETLUser') CREATE USER [PostDeploy_ETLUser]FOR LOGIN [PostDeploy_ETLUser]; exec sp_addrolemember'db_datareader', 'PostDeploy_ETLUser' 
USE [PostDeploy_ItemBank]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'PostDeploy_ETLUser') CREATE USER [PostDeploy_ETLUser]FOR LOGIN [PostDeploy_ETLUser]; exec sp_addrolemember'ETLRole', 'PostDeploy_ETLUser' 
USE [PostDeploy_ItemBank]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'PostDeploy_ItemBankUser') CREATE USER [PostDeploy_ItemBankUser]FOR LOGIN [PostDeploy_ItemBankUser]; exec sp_addrolemember'db_owner', 'PostDeploy_ItemBankUser' 
USE [PostDeploy_ItemBank]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'PostDeploy_ETLUser') CREATE USER [PostDeploy_ETLUser]FOR LOGIN [PostDeploy_ETLUser]; exec sp_addrolemember'db_datareader', 'PostDeploy_ETLUser' 
USE [PostDeploy_ItemBank]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'PostDeploy_ETLUser') CREATE USER [PostDeploy_ETLUser]FOR LOGIN [PostDeploy_ETLUser]; exec sp_addrolemember'db_datawriter', 'PostDeploy_ETLUser' 
USE [PostDeploy_SecureAssess]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'PostDeploy_SecureAssessUser') CREATE USER [PostDeploy_SecureAssessUser]FOR LOGIN [PostDeploy_SecureAssessUser]; exec sp_addrolemember'db_owner', 'PostDeploy_SecureAssessUser' 
USE [PostDeploy_SecureAssess]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'PostDeploy_ETLUser') CREATE USER [PostDeploy_ETLUser]FOR LOGIN [PostDeploy_ETLUser]; exec sp_addrolemember'db_datareader', 'PostDeploy_ETLUser' 
USE [PostDeploy_SurpassDataWarehouse]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'PostDeploy_ETLUser') CREATE USER [PostDeploy_ETLUser]FOR LOGIN [PostDeploy_ETLUser]; exec sp_addrolemember'db_owner', 'PostDeploy_ETLUser' 
USE [PostDeploy_AnalyticsManagement]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'PostDeploy_AnalyticsManagementUser') CREATE USER [PostDeploy_AnalyticsManagementUser]FOR LOGIN [PostDeploy_AnalyticsManagementUser]; exec sp_addrolemember'db_owner', 'PostDeploy_AnalyticsManagementUser' 
USE [PostDeploy_SurpassManagement]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'PostDeploy_SurpassManagementUser') CREATE USER [PostDeploy_SurpassManagementUser]FOR LOGIN [PostDeploy_SurpassManagementUser]; exec sp_addrolemember'db_owner', 'PostDeploy_SurpassManagementUser' 
USE [PostDeploy_SurpassManagement]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'PostDeploy_ETLUser') CREATE USER [PostDeploy_ETLUser]FOR LOGIN [PostDeploy_ETLUser]; exec sp_addrolemember'db_datareader', 'PostDeploy_ETLUser' 
USE [PostDeploy_ContentAuthor]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'PostDeploy_ContentAuthorUser') CREATE USER [PostDeploy_ContentAuthorUser]FOR LOGIN [PostDeploy_ContentAuthorUser]; exec sp_addrolemember'db_owner', 'PostDeploy_ContentAuthorUser' 
USE [PostDeploy_ContentAuthor]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'PostDeploy_ETLUser') CREATE USER [PostDeploy_ETLUser]FOR LOGIN [PostDeploy_ETLUser]; exec sp_addrolemember'db_datareader', 'PostDeploy_ETLUser' 
USE [PostDeploy_ItemBank]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'PostDeploy_ETLUser') CREATE USER [PostDeploy_ETLUser]FOR LOGIN [PostDeploy_ETLUser]; exec sp_addrolemember'ETLRole', 'PostDeploy_ETLUser' 
USE [PostDeploy_ItemBank]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'PostDeploy_ItemBankUser') CREATE USER [PostDeploy_ItemBankUser]FOR LOGIN [PostDeploy_ItemBankUser]; exec sp_addrolemember'db_owner', 'PostDeploy_ItemBankUser' 
USE [PostDeploy_ItemBank]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'PostDeploy_ETLUser') CREATE USER [PostDeploy_ETLUser]FOR LOGIN [PostDeploy_ETLUser]; exec sp_addrolemember'db_datareader', 'PostDeploy_ETLUser' 
USE [PostDeploy_ItemBank]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'PostDeploy_ETLUser') CREATE USER [PostDeploy_ETLUser]FOR LOGIN [PostDeploy_ETLUser]; exec sp_addrolemember'db_datawriter', 'PostDeploy_ETLUser' 
USE [PostDeploy_SecureAssess]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'PostDeploy_SecureAssessUser') CREATE USER [PostDeploy_SecureAssessUser]FOR LOGIN [PostDeploy_SecureAssessUser]; exec sp_addrolemember'db_owner', 'PostDeploy_SecureAssessUser' 
USE [PostDeploy_SecureAssess]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'PostDeploy_ETLUser') CREATE USER [PostDeploy_ETLUser]FOR LOGIN [PostDeploy_ETLUser]; exec sp_addrolemember'db_datareader', 'PostDeploy_ETLUser' 

USE [PostDeploy_AnalyticsManagement]; IF EXISTS (SELECT 1 FROM sys.database_principals where name = 'Demo_AnalyticsManagementUser') DROP USER [Demo_AnalyticsManagementUser]
USE [PostDeploy_ContentAuthor]; IF EXISTS (SELECT 1 FROM sys.database_principals where name = 'Demo_ContentAuthorUser') DROP USER [Demo_ContentAuthorUser]
USE [PostDeploy_ContentAuthor];  IF EXISTS (SELECT 1 FROM sys.database_principals where name = 'Demo_ETLUser') DROP USER [Demo_ETLUser]
USE [PostDeploy_ItemBank]; IF EXISTS (SELECT 1 FROM sys.database_principals where name = 'Demo_ItemBankUser') DROP USER [Demo_ItemBankUser]
USE [PostDeploy_ItemBank]; IF EXISTS (SELECT 1 FROM sys.database_principals where name = 'Demo_ETLUser') DROP USER [Demo_ETLUser]
USE [PostDeploy_SecureAssess]; IF EXISTS (SELECT 1 FROM sys.database_principals where name = 'Demo_SecureAssessUser') DROP USER [Demo_SecureAssessUser]
USE [PostDeploy_SecureAssess]; IF EXISTS (SELECT 1 FROM sys.database_principals where name = 'Demo_ETLUser') DROP USER [Demo_ETLUser]
USE [PostDeploy_SurpassDataWarehouse]; IF EXISTS (SELECT 1 FROM sys.database_principals where name = 'Demo_ETLUser') DROP USER [Demo_ETLUser]
USE [PostDeploy_SurpassManagement]; IF EXISTS (SELECT 1 FROM sys.database_principals where name = 'Demo_ETLUser') DROP USER [Demo_ETLUser]
USE [PostDeploy_SurpassManagement]; IF EXISTS (SELECT 1 FROM sys.database_principals where name = 'Demo_SurpassManagementUser') DROP USER [Demo_SurpassManagementUser]