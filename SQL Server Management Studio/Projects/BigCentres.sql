select CentreName
	, CentreCode
	, ct.AddressLine1
	, ct.AddressLine2
	, c.County
	, ctr.Country
	, ct.PostCode
	, ct.town
	, ct.Retired
	, ct.PrimaryContactID
	, ut.Forename
	, ut.Surname
	, ut.DOB
from CentreTable ct
inner join CountryLookupTable ctr on ctr.ID = ct.Country
inner join CountyLookupTable c on c.ID = ct.County
left join UserTable ut on ut.id= PrimaryContactID

UNION

Select
	CentreName
	, CentreCode
	, AddressLine1
	, AddressLine1
	, County
	, Country
	,PostCode
	,Town
	,'' 
	,''
	,'','',''
from WAREHOUSE_CentreTable