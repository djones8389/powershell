USE master

declare @bigSQL nvarchar(MAX) = '';

SELECT @bigSQL +=CHAR(13) +
	'ALTER INDEX ' + C.name +  ' ON ' + 'dbo' + '.' + B.name + ' REORGANIZE;'
	FROM sys.dm_db_index_physical_stats(DB_ID(),NULL,NULL,NULL,NULL) A
	INNER JOIN sys.objects B
	ON A.object_id = B.object_id
	INNER JOIN sys.indexes C
	ON B.object_id = C.object_id AND A.index_id = C.index_id
	INNER JOIN sys.partitions D
	ON B.object_id = D.object_id AND A.index_id = D.index_id
	WHERE C.index_id > 0
		and A.avg_fragmentation_in_percent > 30

USE MarketDev
PRINT(@bigSQL)
USE AdventureWorks2008R2
PRINT(@bigSQL)



