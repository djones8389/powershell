 RESTORE DATABASE [STG_SQA_SecureAssess] 
	FROM DISK = N'T:\FTP\SQA\SQA_SecureAssess.bak' 
		,DISK = N'T:\FTP\SQA\SQA_SecureAssess_1.bak' 
		,DISK = N'T:\FTP\SQA\SQA_SecureAssess_2.bak' 
	WITH FILE = 1, NOUNLOAD, NOREWIND, REPLACE, STATS = 10
		, MOVE 'Data' TO N'E:\DATA\STG_SQA_SecureAssess.mdf', MOVE 'Log' TO N'L:\LOGS\STG_SQA_SecureAssess.ldf'; 
GO

IF NOT EXISTS (SELECT 1 FROM Sys.sql_logins where name ='STG_SQA_SecureAssess_Login') 
	CREATE LOGIN [STG_SQA_SecureAssess_Login] WITH PASSWORD = 'fVTbpXFvaVjnKKJokDbxmwCVVlMm5zUQ';  
	USE [STG_SQA_SecureAssess];  	
	IF EXISTS (
		SELECT 1
		FROM sys.database_principals
		WHERE NAME ='STG_SQA_SecureAssess_User'
		)
	DROP USER [STG_SQA_SecureAssess_User];
	
	CREATE USER [STG_SQA_SecureAssess_User] FOR LOGIN [STG_SQA_SecureAssess_Login]; EXEC sp_addrolemember 'db_owner','STG_SQA_SecureAssess_User'
	