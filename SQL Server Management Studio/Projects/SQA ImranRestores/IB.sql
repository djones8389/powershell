 
RESTORE DATABASE [STG_SQA_ItemBank] 
FROM DISK = N'T:\FTP\SQA\SQA_ItemBank.2017.05.11.bak' WITH FILE = 1, NOUNLOAD, NOREWIND, REPLACE, MEDIAPASSWORD = 's_!85WrenukEtrut+4f5a#-a&ac*aKUs', STATS = 10, MOVE 'Rows' TO N'E:\DATA\STG_SQA_ItemBank.mdf', MOVE 'Log' TO N'L:\LOGS\STG_SQA_ItemBank.ldf'; 
GO

IF NOT EXISTS (SELECT 1 FROM Sys.sql_logins where name ='STG_SQA_ItemBank_Login') 
	CREATE LOGIN [STG_SQA_ItemBank_Login] WITH PASSWORD = 'U9EWgu6eGY0SPxgD7KWZa3HBRbBoG71N';  
	USE [STG_SQA_ItemBank];  	
	IF EXISTS (
		SELECT 1
		FROM sys.database_principals
		WHERE NAME ='STG_SQA_ItemBank_User'
		)
	DROP USER [STG_SQA_ItemBank_User];
	
	CREATE USER [STG_SQA_ItemBank_User] FOR LOGIN [STG_SQA_ItemBank_Login]; EXEC sp_addrolemember 'db_owner','STG_SQA_ItemBank_User'
	
	