 use master
RESTORE DATABASE [STG_SQA_CPProjectAdmin] FROM DISK = N'T:\FTP\SQA\SQA_CPProjectAdmin.2017.06.12.bak' WITH FILE = 1, NOUNLOAD, NOREWIND
, REPLACE, MEDIAPASSWORD = 's_!85WrenukEtrut+4f5a#-a&ac*aKUs', STATS = 1, MOVE 'Data' TO N'E:\DATA\STG_SQA_CPProjectAdmin.mdf', MOVE 'Log' TO N'L:\LOGS\STG_SQA_CPProjectAdmin.ldf'; 
GO

IF NOT EXISTS (SELECT 1 FROM Sys.sql_logins where name ='STG_SQA_CPProjectAdmin_Login') 
	CREATE LOGIN [STG_SQA_CPProjectAdmin_Login] WITH PASSWORD = 'BUokJWagXyLkXBnSK89doCv5ev0mzk7Y';  
	USE [STG_SQA_CPProjectAdmin];  	
	IF EXISTS (
		SELECT 1
		FROM sys.database_principals
		WHERE NAME ='STG_SQA_CPProjectAdmin_User'
		)
	DROP USER [STG_SQA_CPProjectAdmin_User];
	
	CREATE USER [STG_SQA_CPProjectAdmin_User] FOR LOGIN [STG_SQA_CPProjectAdmin_Login]; EXEC sp_addrolemember 'db_owner','STG_SQA_CPProjectAdmin_User'
	