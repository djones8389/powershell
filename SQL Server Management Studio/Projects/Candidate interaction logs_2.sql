SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

SELECT A.*
	, ExamID
	, Count(b.c.value('data(@id)[1]','varchar(20)')) [NoOfItemsInExam]
FROM (
	select ExamSessionID
		  , SUM(ItemAuditing) [ItemRelated]
		  , COUNT(ItemAuditing)-SUM(ItemAuditing) [NotItemRelated]
	from (
	select ExamSessionID
		,  case when itemid <> '' then 1 else 0 end as 'ItemAuditing'
	from ExamSessionCandidateInteractionLogsTable NOLOCK
	) D

	group by ExamSessionID

) A

INNER JOIN ExamSessionTable EST ON EST.ID = A.ExamSessionID
INNER JOIN ScheduledExamsTable SCET ON SCET.ID = EST.ScheduledExamID
CROSS APPLY StructureXML.nodes('assessmentDetails/assessment/section/item') b(c)

group by ExamSessionID
		,[ItemRelated]
		,[NotItemRelated]
		, ExamID;


SELECT A.*
	, ExamID
	, Count(b.c.value('data(@id)[1]','varchar(20)')) [NoOfItemsInExam]
FROM (
	select ExamSessionID
		  , SUM(ItemAuditing) [ItemRelated]
		  , COUNT(ItemAuditing)-SUM(ItemAuditing) [NotItemRelated]
	from (
	select ExamSessionID
		,  case when itemid <> '' then 1 else 0 end as 'ItemAuditing'
	from Warehouse_ExamSessionCandidateInteractionLogsTable NOLOCK
	) D

	group by ExamSessionID

) A

INNER JOIN WAREHOUSE_ExamSessionTable EST ON EST.ID = A.ExamSessionID
INNER JOIN WAREHOUSE_ScheduledExamsTable SCET ON SCET.ID = EST.WAREHOUSEScheduledExamID
CROSS APPLY StructureXML.nodes('assessmentDetails/assessment/section/item') b(c)

group by A.ExamSessionID
		,[ItemRelated]
		,[NotItemRelated]
		, ExamID;
