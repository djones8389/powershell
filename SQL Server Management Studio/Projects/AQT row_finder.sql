SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

USE Icaewmock_SecureAssess;

DECLARE @examName nvarchar(MAX) = 'Tax Compliance Mock 2'

DECLARE @ESIDs TABLE (ESID INT, Keycode nvarchar(10), ExamState tinyint);
INSERT @ESIDs
SELECT EST.ID, EST.Keycode, EST.ExamState
FROM ScheduledExamsTable SCET
INNER JOIN ExamSessionTable EST
on EST.ScheduledExamID = SCET.ID
WHERE examName = @examName
	and KeyCode = 'D8Y9MYH3';



SELECT C.ExamSessionID
	, C.ItemID
	, C.Keycode
	, ExamState
	, C.Response
	, C.RowNumber
	--,[full]
	, MaxChecker.Max_RowNumber
	,(Max_RowNumber-RowNumber)
FROM (
	SELECT ESIRT.ExamSessionID
		, Keycode
		,ExamState
		, ItemID
		--, a.b.query('.') [full]
		, a.b.value('.','nvarchar(max)') [Response]
		, ROW_NUMBER() over (partition by ESIRT.ExamSessionID, ESIRT.itemID order by (SELECT 1)) RowNumber
	FROM  ExamSessionItemResponseTable ESIRT
	INNER JOIN @ESIDs EST
	ON EST.ESID = ESIRT.ExamSessionID

	CROSS APPLY ESIRT.ItemResponseData.nodes('p/s/c/i/t/r/c[1]') a(b)

	where ItemResponseData.exist('p/s/c[@typ=20]') = 1
) C

LEFT JOIN (

	SELECT 
		ESID
		, ItemID
		, MAX(RowNumber) Max_RowNumber
	FROM (
		SELECT ESIRT.ExamSessionID [ESID] 
			, ItemID
 			, ROW_NUMBER() over (partition by ESIRT.ExamSessionID, ESIRT.itemID order by (SELECT 1)) RowNumber
		FROM  ExamSessionItemResponseTable ESIRT
		INNER JOIN @ESIDs EST
		ON EST.ESID = ESIRT.ExamSessionID

		CROSS APPLY ESIRT.ItemResponseData.nodes('p/s/c/i/t/r/c[1]') a(b)

		where ItemResponseData.exist('p/s/c[@typ=20]') = 1
	) f
	group by ESID
		, ItemID
) MaxChecker

on MaxChecker.ESID = C.ExamSessionID	
	and MaxChecker.ItemID = C.ItemID

where [Response] IS NOT NULL
	AND (Max_RowNumber-RowNumber) <= 2;




	/*


SELECT 
	itemID
	, ItemResponseData.query('//text/text()')
	, ItemResponseData
FROM  ExamSessionItemResponseTable ESIRT
--CROSS APPLY ESIRT.ItemResponseData.nodes('p/s/c/i/t/r[@h="22"]') a(b)
where ExamSessionID =  1368
	and ItemResponseData.exist('p/s/c[@typ=20]') = 1
	and itemid = '392P3367'


SELECT ESIRT.ExamSessionID
	, ItemID
	--, a.b.query('.') [full]
	, a.b.value('data(c/text)[1]','nvarchar(max)') [Response]
	, a.b.value('data(c/text)[2]','nvarchar(max)') [Response2]
	--, a.b.query('//text') [Response3]
	, ROW_NUMBER() over (partition by ESIRT.ExamSessionID, ESIRT.itemID order by (SELECT 1)) RowNumber
FROM  ExamSessionItemResponseTable ESIRT

CROSS APPLY ESIRT.ItemResponseData.nodes('p/s/c/i/t/r[@h="22"]') a(b)

where ExamSessionID =  1368
	and ItemResponseData.exist('p/s/c[@typ=20]') = 1
	and itemid = '392P3367'




SELECT 
	itemID
	, ItemResponseData.query('//text')
	, ItemResponseData
FROM  ExamSessionItemResponseTable ESIRT
--CROSS APPLY ESIRT.ItemResponseData.nodes('p/s/c/i/t/r[@h="22"]') a(b)
where ExamSessionID =  1368
	and ItemResponseData.exist('p/s/c[@typ=20]') = 1
	and itemid = '392P3367'

select StructureXML
	, examname
	, est.id
	, examstate
	, forename + ' ' + surname
from examsessiontable est 
inner join ScheduledExamsTable scet
on scet.id = est.scheduledexamid
inner join usertable ut on ut.id = est.userid
where keycode = 'D8Y9MYH3'

*/


