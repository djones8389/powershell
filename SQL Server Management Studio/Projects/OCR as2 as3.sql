USE PPD_OCR_ContentProducer;

IF OBJECT_ID('tempdb..#CPID') IS NOT NULL DROP TABLE #CPID;

CREATE TABLE #CPID (
	CPID NVARCHAR(50)
	,as3 bit
);

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

INSERT #CPID
SELECT SUBSTRING(ID, 0, CHARINDEX('S',ID)),  isnull(as3, 0) as3
from ItemReorderingTable
where grT = 1
union
SELECT SUBSTRING(ID, 0, CHARINDEX('S',ID)),  isnull(as3, 0) as3
from ItemVideoTable
union
SELECT SUBSTRING(ID, 0, CHARINDEX('S',ID)),  isnull(as3, 0) as3
from ItemHotSpotTable
where grT = 'flash'
union
SELECT SUBSTRING(ID, 0, CHARINDEX('S',ID)), isnull(as3, 0) as3
from ItemGraphicTable
where grT = 'flash'
union
SELECT SUBSTRING(ID, 0, CHARINDEX('S',ID)),isnull(as3, 0) as3
from ItemDragAndDropTable
where typ = 16 and grT = 1
union
SELECT SUBSTRING(ID, 0, CHARINDEX('S',ID)),  isnull(as3, 0) as3
from ItemDocumentTable
where grT = 'flash'
union
SELECT SUBSTRING(ID, 0, CHARINDEX('S',ID)),  isnull(as3, 0) as3
from ItemCustomQuestionTable


SELECT PLT.Name
	, A.CPID
FROM (

	SELECT a.CPID
	FROM (
	SELECT CPID
		, ROW_NUMBER() OVER (PARTITION BY cpid, as3 ORDER BY cpid) R
	FROM #CPID
	) a
	--WHERE r = 1
	GROUP BY CPID, R
	HAVING	COUNT(R) > 1
) A
INNER JOIN dbo.ProjectListTable PLT
ON PLT.ID = SUBSTRING(A.CPID, 0, CHARINDEX('p',CPID))

ORDER BY PLT.Name;




----WHERE cpid = '6025P8838'
