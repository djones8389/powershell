if OBJECT_ID('tempdb..#QuestionUpdater') is not null drop table #QuestionUpdater;

select EST.ID
	, ESIRT.ItemID
	, ESIRT.ItemResponseData
INTO #QuestionUpdater
FROM [dbo].[ExamSessionTable] EST
CROSS APPLY StructureXML.nodes('assessmentDetails/assessment/section/item') a(b)
INNER JOIN ExamSessionItemResponseTable ESIRT 
ON ESIRT.ExamSessionID =  EST.ID
	AND ESIRT.ItemID =  a.b.value('@id','nvarchar(20)')

where a.b.value('@userAttempted','bit') = 0;

/*
	Check it's only 1 item per exam as they've said it is.   If there's more than 1 item per exam > check with OCR/SDesk - is this a problem?
    If not, the script below will update all UA and markingType flags where UA = 0, non-dependent on Q number
*/

select ID, ItemID
from #QuestionUpdater;


select EST.ID	
	, StructureXML
	, resultData
	, resultDataFull
INTO #XMLUpdater
from ExamSessionTable EST
WHERE EST.ID IN (SELECT ID FROM #QuestionUpdater);


DECLARE myCursor CURSOR FOR
SELECT
	ID
	, ItemID
	, ItemResponseData
FROM #QuestionUpdater

DECLARE @ESID INT, @ItemID nvarchar(20), @ItemResponseData XML;

OPEN myCursor

FETCH NEXT FROM myCursor INTO @ESID, @ItemID, @ItemResponseData

WHILE @@FETCH_STATUS = 0

BEGIN

	UPDATE #XMLUpdater
	SET  StructureXML.modify('replace value of (/assessmentDetails/assessment/section/item[@id = sql:variable("@ItemID")]/@markingType)[1] with "1"')
		,ResultData.modify('replace value of (/exam/section/item[@id = sql:variable("@ItemID")]/@markingType)[1] with "1"')
		,ResultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item[@id = sql:variable("@ItemID")]/@markingType)[1] with "1"')
	WHERE id = @ESID

	UPDATE #XMLUpdater
	SET StructureXML.modify('replace value of (/assessmentDetails/assessment/section/item[@id = sql:variable("@ItemID")]/@userAttempted)[1] with "1"')
		,ResultData.modify('replace value of (/exam/section/item[@id = sql:variable("@ItemID")]/@userAttempted)[1] with "1"')
		,ResultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item[@id = sql:variable("@ItemID")]/@userAttempted)[1] with "1"')
	WHERE id = @ESID

	UPDATE #XMLUpdater
	SET ResultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item[@id = sql:variable("@ItemID")]/p/@ua)[1] with "1"')
	WHERE  id = @ESID

	UPDATE #QuestionUpdater
	SET  ItemResponseData.modify('replace value of (/p/@ua)[1] with "1"')
	WHERE ID = @ESID
		and itemid = @ItemID

FETCH NEXT FROM myCursor INTO @ESID, @ItemID, @ItemResponseData

END
CLOSE myCursor
DEALLOCATE myCursor

/*
UPDATE EST
SET StructureXML = A.StructureXML
	,resultData = A.ResultData
	,resultDataFull = A.ResultDataFull
FROM ExamSessionTable EST
INNER JOIN #XMLUpdater AS A 
ON A.ID = EST.ID

UPDATE ESIRT
SET A.ItemResponseData
FROM ExamSessionItemResponseTable ESIRT
INNER JOIN #QuestionUpdater A
ON ESIRT.ExamSessionID = A.ID 
	AND ESIRT.ItemID = A.ItemID

DROP TABLE #ExamXmls;
*/