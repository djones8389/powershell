--select top 10 *
--from ProjectManifestTable
--where projectid = 830

--update ProjectManifestTable
--set delCol = 0
--where projectid = 830 and ID IN (3,4)


--select ID, name, ProjectId, delCol
--from ProjectManifestTable
--where name in ('sqa_assessment_template.jpg','sqa_assessment_template.jpg')

--select * from projectlisttable where id = 873


SELECT A.*
FROM (

select 
	projectid, name,  id

from ProjectManifestTable 
) A

INNER JOIN 
(

select 
	plt.ID as ProjectID
	, plt.Name
	, pmt.name as PMTName
	, pmt.id as PMTID
	, pmt.delcol
	, plt.projectStructurexml.value('(/Pro/@prB)[1]','nvarchar(max)') as 'Prb_Preview'
	, plt.projectStructurexml.value('(/Pro/@bac)[1]','nvarchar(max)') + '.jpg' as 'bac_Editor'
 from projectlisttable as plt

inner join ProjectManifestTable as pmt
on pmt.ProjectId = plt.ID

where  location like '%background%'
	and (pmt.name = plt.projectStructurexml.value('(/Pro/@prB)[1]','nvarchar(max)') or pmt.name = plt.projectStructurexml.value('(/Pro/@bac)[1]','nvarchar(max)') + '.jpg')
	and delCol = 1

) B 

ON A.ProjectId = B.ProjectID
	 and A.name = B.PMTName
	 and A.ID = B.PMTID





--update ProjectManifestTable
--set delCol = 0
--where id = (Select PMTID from cte)
--	and name = (select PMTName from cte)
--		and ProjectId = (select ProjectID from cte)




--Prev = lefthandtemplate1030x700.jpg     prB
--Edi =  sqalefteditor.jpg		          bac




select 
	plt.ID as ProjectID
	, plt.Name
	, pmt.name as PMTName
	, pmt.id as PMTID
	--, pmt.delcol
	--, plt.projectStructurexml.value('(/Pro/@prB)[1]','nvarchar(max)') as 'Prb_Preview'
	--, plt.projectStructurexml.value('(/Pro/@bac)[1]','nvarchar(max)') + '.jpg' as 'bac_Editor'
 into #DELCOL
 from projectlisttable as plt

inner join ProjectManifestTable as pmt
on pmt.ProjectId = plt.ID

where  location like '%background%'
	and (pmt.name = plt.projectStructurexml.value('(/Pro/@prB)[1]','nvarchar(max)') or pmt.name = plt.projectStructurexml.value('(/Pro/@bac)[1]','nvarchar(max)') + '.jpg')
	and delCol = 1



select * from #DELCOL



update ProjectManifestTable
set Delcol = 0
from ProjectManifestTable a

inner join #DELCOL b
on b.PMTID = a.ID
	and b.PMTName = a.name
		and b.ProjectID = a.ProjectId


select 
	plt.ID as ProjectID
	, plt.Name
	, pmt.name as PMTName
	, pmt.id as PMTID
	--, pmt.delcol
	--, plt.projectStructurexml.value('(/Pro/@prB)[1]','nvarchar(max)') as 'Prb_Preview'
	--, plt.projectStructurexml.value('(/Pro/@bac)[1]','nvarchar(max)') + '.jpg' as 'bac_Editor'
	--into #DELCOL
 from projectlisttable as plt

inner join ProjectManifestTable as pmt
on pmt.ProjectId = plt.ID

where  location like '%background%'
	and (pmt.name = plt.projectStructurexml.value('(/Pro/@prB)[1]','nvarchar(max)') or pmt.name = plt.projectStructurexml.value('(/Pro/@bac)[1]','nvarchar(max)') + '.jpg')
	and delCol = 1

