SELECT   EST.examState, pinNumber, examName, EST.KeyCode, CT.CentreName, QualificationName
	 , cast(ScheduledStartDateTime as date) as [Start Date], cast(ScheduledEndDateTime as date) as [End Date]
	 , UT.Forename + ' ' + UT.Surname as [Candidate], ut.CandidateRef, invigilated
	 , cast(DATEADD(minute, ActiveStartTime, 0) as time(0)) as [StartTime]
	 , cast(DATEADD(minute, ActiveEndTime, 0) as time(0)) as [EndTime]
	   
  FROM ExamSessionTable as EST

  Inner Join ScheduledExamsTable as SCET
  on SCET.ID = EST.ScheduledExamID
  
  Inner Join UserTable as UT
  on UT.ID = EST.UserID

  Inner Join CentreTable as CT
  on CT.ID = SCET.CentreID

  inner join IB3QualificationLookup as IB
  on IB.ID = qualificationId

where KeyCode = 'BQEBEDA6'
 --where examState < 9 
/*	and examName like '%ICAS%'
	or examName like '%ISYS%'
	or examName like '%DSSK%'
	or examName like '%WKTM%'
	or examName like '%PDBC%'
	or examName like '%SDST5%'
	or examName like '%PRSW%'
	or examName like '%DLPR%'
	or examName like '%OFMW%'
	or examName like '%DVCA%'
	
	order by examState desc*/