--Live--

--select * from ProjectManifestTable where name = 'table_highlight_table.swf' and ProjectId = 241
select distinct ProjectId from ProjectManifestTable where name = 'table_highlight_table.swf'	   

declare @projectID int = 241
 
	select substring(ID,5,4) as PageID, ParentID 
		into #FilteredList
			from ItemGraphicTable
					where ali in  
	 
	(
	 select cast(ID as nvarchar(50)) 
		from ProjectManifestTable 
			where ProjectId = @projectID
				and name = 'table_highlight_table.swf'
		);

select  
	   ProjectListTable.Name as 'ProjectName'
	  , ProjectListTable.ID as 'ProjectID'
	  , page.value('@ID','int') as 'PageID'
	  , page.value('@sta','int') as 'Status'

	  into #PageTable
	from ProjectListTable
	
	CROSS APPLY ProjectStructureXml.nodes('//Pag') pages(page)
      WHERE page.value('local-name((..)[1])', 'nvarchar(1000)') <> 'Rec'
            AND Page.value('(@chO2)[1]', 'NVARCHAR(12)') is not null
            AND id =  @projectID;
	
;with cte as (

select  d.s.value('@id','int') as StatusID
	  , d.s.value('(.)[1]','nvarchar(60)') as StatusName
	from ProjectListTable
	
	cross apply ProjectDefaultXML.nodes ('/Defaults/StatusList/Status') d(s)
		where id = @projectID
	)


select ProjectName, cast(ProjectID as nvarchar(10)) as 'ProjectID', cast(#PageTable.PageID as nvarchar(10)) as 'PageID', Status, StatusName
	from #PageTable
	
	inner join cte on cte.StatusID = #PageTable.Status
	inner join #FilteredList on cast(#FilteredList.PageID as nvarchar(20)) = cast(#PageTable.PageID as nvarchar(20));	
	
drop table #PageTable;
drop table #FilteredList;

	

	
	
