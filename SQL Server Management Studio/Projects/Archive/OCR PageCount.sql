USE UAT_OCR_ContentProducer_IP

  select COUNT(ParentID) as Page_Count, ParentID, Name 
	from PageTable
  
	inner join ProjectListTable as PLT
	on PLT.ID = PageTable.ParentID

WHERE PLT.ID NOT IN (6020,6005,6015,6014,6009,6038,6010,6042,334,327,340,6007,6012,6051,6011,6041,6044,6045,6016, 6072)  --Omit Test projects--
	AND PLT.ID IN (		--Only run this for projects, with pages with graphics--
      SELECT PT.ParentID
      FROM dbo.ItemGraphicTable IGT
      INNER JOIN dbo.ComponentTable CT ON IGT.ParentID = CT.ID
      INNER JOIN dbo.SceneTable ST ON ST.ID = CT.ParentID
      INNER JOIN dbo.PageTable PT ON PT.ID = ST.ParentID
	  where igt.lom is null
	  )

	group by ParentID , Name 
		order by Page_Count desc






select ID, Name
from projectlisttable
where  ID IN (6077, 6017, 4209, 6048, 6049, 6050, 6054, 6059, 6062, 6033)



select COUNT(ParentID) as Page_Count, ParentID, Name 
	from PageTable
  
	inner join ProjectListTable as PLT
	on PLT.ID = PageTable.ParentID

WHERE PLT.ID IN (6077, 6017, 4209, 6048, 6049, 6050, 6054, 6059, 6062, 6033)  --Omit Test projects--
	AND PLT.ID IN (		--Only run this for projects, with pages with graphics--
      SELECT PT.ParentID
      FROM dbo.ItemGraphicTable IGT
      INNER JOIN dbo.ComponentTable CT ON IGT.ParentID = CT.ID
      INNER JOIN dbo.SceneTable ST ON ST.ID = CT.ParentID
      INNER JOIN dbo.PageTable PT ON PT.ID = ST.ParentID
	  where igt.lom is null
	  )

	group by ParentID , Name 
		order by Page_Count desc
