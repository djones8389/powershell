USE EAL_SecureAssess_Restore_131014


--select top 1 * 	from ExamStateChangeAuditTable

--drop table #DJ_ExamStateChangeAuditTable

--create table #DJ_ExamStateChangeAuditTable
--(
--	 ID int identity(1,1)
--	, ExamSessionID int
--	, NewState tinyint
--	, StateChangeDate datetime
--	, StateInformation xml null
	
--)

Select 
	X.ID
	,X.ExamSessionID
	,X.NewState
	,min(X.StateChangeDate) as StateChangeDate
	,X.StateInformation
	into #DJ_ExamStateChangeAuditTable 
from

	(
	select 
	 '' as ID
	, ID as ExamSessionID
	, e.s.value('newStateID[1]','nvarchar(max)') as NewState
	, e.s.value('changeDate[1]','datetime') as StateChangeDate
	, '' as StateInformation
		from WAREHOUSE_ExamSessionTable
		
		cross apply ExamStateChangeAuditXml.nodes('exam/stateChange') e(s)
		
			where ID = 175497
		) x
		
	group by X.ID
			,X.ExamSessionID
			,X.NewState
			,X.StateInformation
		order by StateChangeDate

select * from #DJ_ExamStateChangeAuditTable



 -- select the all entries in the ExamStateChangeAuditTable for the current exam session      
   -- into an xml variable      
   DECLARE @examStateChangeAuditXml xml      
   SET @examStateChangeAuditXml =       
   (SELECT TAG, PARENT, [exam!1!Element], [exam!1!id],       
   [stateChange!2!newStateID!Element], [stateChange!2!newState!Element],       
   [stateChange!2!changeDate!Element], [stateChange!2!information!xml]      
         
   FROM      
   (SELECT 1 AS TAG,      
     NULL  AS  Parent,      
     NULL  as  [exam!1!Element],      
     NULL  as  [exam!1!id],      
     NULL  AS  [stateChange!2!newStateID!Element],      
     NULL  AS  [stateChange!2!newState!Element],      
     NULL  AS  [stateChange!2!changeDate!Element],      
     NULL  AS  [stateChange!2!information!xml]      
           
     UNION ALL      
     SELECT      
     2   AS Tag,      
     1   AS Parent,      
     NULL,      
     #DJ_ExamStateChangeAuditTable.[ExamSessionID],      
     #DJ_ExamStateChangeAuditTable.[NewState],      
     [ExamStateLookupTable].[StateName],      
     CONVERT(VARCHAR(23), #DJ_ExamStateChangeAuditTable.[StateChangeDate], 113),      
     #DJ_ExamStateChangeAuditTable.[StateInformation]      
           
     FROM [dbo].#DJ_ExamStateChangeAuditTable      
      INNER JOIN [dbo].[ExamStateLookupTable] ON      
       [ExamStateLookupTable].[ID] = #DJ_ExamStateChangeAuditTable.[NewState]      
           
     WHERE #DJ_ExamStateChangeAuditTable.[ExamSessionID] = 175497-- @examInstanceId
		)      
    AS stateChangeXml      
    FOR XML EXPLICIT) 



create table #XMLCheck (
	myXML xml
)

insert into #XMLCheck
values(@examStateChangeAuditXml)


select * from #XMLCheck











/*          
   -- select the all entries in the ExamStateChangeAuditTable for the current exam session      
   -- into an xml variable      
   DECLARE @examStateChangeAuditXml xml      
   SET @examStateChangeAuditXml =       
   (SELECT TAG, PARENT, [exam!1!Element], [exam!1!id],       
   [stateChange!2!newStateID!Element], [stateChange!2!newState!Element],       
   [stateChange!2!changeDate!Element], [stateChange!2!information!xml]      
         
   FROM      
   (SELECT 1 AS TAG,      
     NULL  AS  Parent,      
     NULL  as  [exam!1!Element],      
     NULL  as  [exam!1!id],      
     NULL  AS  [stateChange!2!newStateID!Element],      
     NULL  AS  [stateChange!2!newState!Element],      
     NULL  AS  [stateChange!2!changeDate!Element],      
     NULL  AS  [stateChange!2!information!xml]      
           
     UNION ALL      
     SELECT      
     2   AS Tag,      
     1   AS Parent,      
     NULL,      ;
     [ExamStateChangeAuditTable].[ExamSessionID],      
     [ExamStateChangeAuditTable].[NewState],      
     [ExamStateLookupTable].[StateName],      
     CONVERT(VARCHAR(23), [ExamStateChangeAuditTable].[StateChangeDate], 113),      
     [ExamStateChangeAuditTable].[StateInformation]      
           
     FROM [dbo].[ExamStateChangeAuditTable]      
      INNER JOIN [dbo].[ExamStateLookupTable] ON      
       [ExamStateLookupTable].[ID] = [ExamStateChangeAuditTable].[NewState]      
           
     WHERE [ExamStateChangeAuditTable].[ExamSessionID] = @examInstanceId)      
    AS stateChangeXml      
    FOR XML EXPLICIT)  
*/