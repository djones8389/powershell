USE EAL_SecureAssess_Restore_131014

DROP TABLE [dbo].#DJ_ExamStateChangeAuditTable

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

DECLARE ExamXML CURSOR 
FOR
select top 5 ID
	, ExamStateChangeAuditXML
   from WAREHOUSE_ExamSessionTable
	where ExamStateChangeAuditXml.value('count(/exam/stateChange[newStateID=2])', 'int') > 1;
	 

DECLARE @ID INT, @ExamStateChangeAuditXML XML;

Open ExamXML;

FETCH NEXT FROM ExamXML INTO @ID, @ExamStateChangeAuditXML;

WHILE @@FETCH_STATUS = 0
BEGIN

Select 
	X.ID
	,X.ExamSessionID
	,X.NewState
	,min(X.StateChangeDate) as StateChangeDate
	,X.StateInformation
	--into #DJ_ExamStateChangeAuditTable 
from

	(
	select 
	 '' as ID
	, WAREHOUSE_ExamSessionTable.ID as ExamSessionID
	, e.s.value('newStateID[1]','nvarchar(max)') as NewState
	, e.s.value('changeDate[1]','datetime') as StateChangeDate
	, '' as StateInformation
		from WAREHOUSE_ExamSessionTable

		cross apply ExamStateChangeAuditXml.nodes('exam/stateChange') e(s)
		
			where ID = @ID --= 175497
		) x
		
	group by X.ID
			,X.ExamSessionID
			,X.NewState
			,X.StateInformation
		order by ExamSessionID, StateChangeDate, NewState;
		FETCH NEXT FROM ExamXML INTO @ID, @ExamStateChangeAuditXML;
--select * from #DJ_ExamStateChangeAuditTable;

END

CLOSE ExamXML;
DEALLOCATE ExamXML;

select * from #DJ_ExamStateChangeAuditTable;








   -- select the all entries in the ExamStateChangeAuditTable for the current exam session      
   -- into an xml variable      
   DECLARE @examStateChangeAuditXml xml      
   SET @examStateChangeAuditXml =       
   (SELECT TAG, PARENT, [exam!1!Element], [exam!1!id],       
   [stateChange!2!newStateID!Element], [stateChange!2!newState!Element],       
   [stateChange!2!changeDate!Element], [stateChange!2!information!xml]      
         
   FROM      
   (SELECT 1 AS TAG,      
     NULL  AS  Parent,      
     NULL  as  [exam!1!Element],      
     NULL  as  [exam!1!id],      
     NULL  AS  [stateChange!2!newStateID!Element],      
     NULL  AS  [stateChange!2!newState!Element],      
     NULL  AS  [stateChange!2!changeDate!Element],      
     NULL  AS  [stateChange!2!information!xml]      
           
     UNION ALL      
     SELECT      
     2   AS Tag,      
     1   AS Parent,      
     NULL,      
     #DJ_ExamStateChangeAuditTable.[ExamSessionID],      
     #DJ_ExamStateChangeAuditTable.[NewState],      
     [ExamStateLookupTable].[StateName],      
     CONVERT(VARCHAR(23), #DJ_ExamStateChangeAuditTable.[StateChangeDate], 113),      
     #DJ_ExamStateChangeAuditTable.[StateInformation]      
           
     FROM [dbo].#DJ_ExamStateChangeAuditTable      
      INNER JOIN [dbo].[ExamStateLookupTable] ON      
       [ExamStateLookupTable].[ID] = #DJ_ExamStateChangeAuditTable.[NewState]      
           
     WHERE #DJ_ExamStateChangeAuditTable.[ExamSessionID]  = (select top 1 ID from @MultipleStateTwos)-- 175497-- @examInstanceId
		)      
    AS stateChangeXml      
    FOR XML EXPLICIT); 

--drop table #XMLCheck 

create table #XMLCheck (
	myXML xml
);

insert into #XMLCheck(myXML)
values(@examStateChangeAuditXml);


select CAST(myXML as nvarchar(max)) from #XMLCheck;


select myXML  from #XMLCheck;