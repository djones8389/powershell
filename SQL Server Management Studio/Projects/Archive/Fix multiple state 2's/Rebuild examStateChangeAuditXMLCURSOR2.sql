USE EAL_SecureAssess_Restore_131014
/*
drop table #NewXMLStorage;
drop table #DJ_ExamStateChangeAuditTable;
*/
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

--Identify the troublesome exams, and xPath their stateChangeAuditXML into a temp table that we can use--

declare @MultipleStateTwos table (
	 ID int
 )

insert into @MultipleStateTwos (ID)
select top 5 ID
   from WAREHOUSE_ExamSessionTable
	where ExamStateChangeAuditXml.value('count(/exam/stateChange[newStateID=2])', 'int') > 1;
	 

Select 
	X.ID
	,X.ExamSessionID
	,X.NewState
	,min(X.StateChangeDate) as StateChangeDate
	,X.StateInformation
	into #DJ_ExamStateChangeAuditTable 
from

	(
	select 
	 '' as ID
	, WAREHOUSE_ExamSessionTable.ID as ExamSessionID
	, e.s.value('newStateID[1]','nvarchar(max)') as NewState
	, e.s.value('changeDate[1]','datetime') as StateChangeDate
	, '' as StateInformation
		from WAREHOUSE_ExamSessionTable

		cross apply ExamStateChangeAuditXml.nodes('exam/stateChange') e(s)
		
			where ID in (select ID from @MultipleStateTwos) --= 175497
		) x
		
	group by X.ID
			,X.ExamSessionID
			,X.NewState
			,X.StateInformation
		order by ExamSessionID, StateChangeDate, NewState;

--Check this has worked--
--select * from #DJ_ExamStateChangeAuditTable;



--Create a table with ESID and new, fixed StateChangeAuditXML--


create table #NewXMLStorage (

	ExamsessionID int
	, examXML xml
);

DECLARE ESID CURSOR 
FOR
select distinct ExamSessionID 
	from #DJ_ExamStateChangeAuditTable;
   
DECLARE @ESID INT;

OPEN ESID;

FETCH NEXT FROM ESID INTO @ESID;

WHILE @@FETCH_STATUS = 0
BEGIN
   
   
   
   DECLARE @examStateChangeAuditXml xml      
   SET @examStateChangeAuditXml =       
   (SELECT TAG, PARENT, [exam!1!Element], [exam!1!id],       
   [stateChange!2!newStateID!Element], [stateChange!2!newState!Element],       
   [stateChange!2!changeDate!Element], [stateChange!2!information!xml]      
         
   FROM      
   (SELECT 1 AS TAG,      
     NULL  AS  Parent,      
     NULL  as  [exam!1!Element],      
     NULL  as  [exam!1!id],      
     NULL  AS  [stateChange!2!newStateID!Element],      
     NULL  AS  [stateChange!2!newState!Element],      
     NULL  AS  [stateChange!2!changeDate!Element],      
     NULL  AS  [stateChange!2!information!xml]      
           
     UNION ALL      
     SELECT      
     2   AS Tag,      
     1   AS Parent,      
     NULL,      
     #DJ_ExamStateChangeAuditTable.[ExamSessionID],      
     #DJ_ExamStateChangeAuditTable.[NewState],      
     [ExamStateLookupTable].[StateName],      
     CONVERT(VARCHAR(23), #DJ_ExamStateChangeAuditTable.[StateChangeDate], 113),      
     #DJ_ExamStateChangeAuditTable.[StateInformation]      
           
     FROM [dbo].#DJ_ExamStateChangeAuditTable      
      INNER JOIN [dbo].[ExamStateLookupTable] ON      
       [ExamStateLookupTable].[ID] = #DJ_ExamStateChangeAuditTable.[NewState]      
           
     WHERE #DJ_ExamStateChangeAuditTable.[ExamSessionID]  = @ESID
		)      
    AS stateChangeXml      
    FOR XML EXPLICIT); 
    
	insert into #NewXMLStorage(ExamsessionID, examXML)
	values(@ESID, @examStateChangeAuditXml)

FETCH NEXT FROM ESID INTO @ESID;

END

CLOSE ESID;
DEALLOCATE ESID;

--Use this to join to Live table + replace--
select * from #NewXMLStorage;



drop table #NewXMLStorage;
drop table #DJ_ExamStateChangeAuditTable;

/*
--drop table #XMLCheck 

create table #XMLCheck (
	myXML xml
);

insert into #XMLCheck(myXML)
values(@examStateChangeAuditXml);


select CAST(myXML as nvarchar(max)) from #XMLCheck;


select myXML  from #XMLCheck;

*/