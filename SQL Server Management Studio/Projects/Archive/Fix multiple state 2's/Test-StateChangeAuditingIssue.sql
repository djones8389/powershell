USE EAL_SecureAssess_Restore_131014

select COUNT(ID)
	--, ExamStateChangeAuditXml
	--, ExamStateChangeAuditXml.value('count(/exam/stateChange[newStateID=2])', 'int') as CountOfState2
	
	from WAREHOUSE_ExamSessionTable
	where ExamStateChangeAuditXml.value('count(/exam/stateChange[newStateID=2])', 'int') > 1
	group by ID


select top 1 ExamStateChangeAuditXml
	, ExamStateChangeAuditXml.query('data(/exam/stateChange[newStateID=2])') as State2
	, cast (ExamStateChangeAuditXml.query('data(/exam/stateChange[newStateID=2])') as XML) as State2XML
	, ExamStateChangeAuditXml.query('data(exam/stateChange[newStateID=6]/changeDate)[1]') as [Started]
	from WAREHOUSE_ExamSessionTable
		where ID =46--= 175497

select top 1 *
	from ExamStateChangeAuditTable
	

/*
Method:

- Grab the full examStateChangeAuditXML and store it
- Grab the first state 2 entry and store it
- Run a query which deletes the State 2's from the stored XML 
- Add the single state 2 entry back in
- 



*/





/*
This script should remove all but the last client information entry from exams that have got more than 2 entries.

UPDATE WAREHOUSE_EXamsessionTable 
SET clientInformation = CONVERT(XML, '<clientInformation>' + CONVERT(NVARCHAR(MAX), clientInformation.query('clientInformation/systemConfiguration[last()]')) + '</clientInformation>')
WHERE clientInformation.value('count(clientInformation/systemConfiguration)', 'int') > 2
*/

	
/*
<exam>
  <stateChange>
    <newStateID>1</newStateID>
    <newState>Exam Scheduled (but not created)</newState>
    <changeDate>16 Apr 2012 10:01:19:00</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>2</newStateID>
    <newState>Exam Scheduled (but not downloaded)</newState>
    <changeDate>16 Apr 2012 10:04:09:51</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>2</newStateID>
    <newState>Exam Scheduled (but not downloaded)</newState>
    <changeDate>16 Apr 2012 10:04:09:55</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>2</newStateID>
    <newState>Exam Scheduled (but not downloaded)</newState>
    <changeDate>16 Apr 2012 10:04:09:59</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>2</newStateID>
    <newState>Exam Scheduled (but not downloaded)</newState>
    <changeDate>16 Apr 2012 10:04:09:63</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>2</newStateID>
    <newState>Exam Scheduled (but not downloaded)</newState>
    <changeDate>16 Apr 2012 10:04:09:69</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>2</newStateID>
    <newState>Exam Scheduled (but not downloaded)</newState>
    <changeDate>16 Apr 2012 10:04:09:73</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>2</newStateID>
    <newState>Exam Scheduled (but not downloaded)</newState>
    <changeDate>16 Apr 2012 10:04:09:77</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>2</newStateID>
    <newState>Exam Scheduled (but not downloaded)</newState>
    <changeDate>16 Apr 2012 10:04:09:82</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>3</newStateID>
    <newState>Exam Scheduled (but locked)</newState>
    <changeDate>16 Apr 2012 10:04:25:84</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>4</newStateID>
    <newState>Exam Ready</newState>
    <changeDate>18 Apr 2012 00:00:00:36</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>6</newStateID>
    <newState>Exam Delivered To Candidate</newState>
    <changeDate>18 Apr 2012 09:00:52:88</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>9</newStateID>
    <newState>Exam Submitted</newState>
    <changeDate>18 Apr 2012 09:05:47:29</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>11</newStateID>
    <newState>Exam Marked</newState>
    <changeDate>18 Apr 2012 09:05:54:48</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>12</newStateID>
    <newState>Exam Script Verified</newState>
    <changeDate>18 Apr 2012 09:06:09:69</changeDate>
    <information />
  </stateChange>
</exam>
*/
		
		
		
		
		/*
		Shred XML into a table etc
		
		ESID
		STATES
		DATES
		
		
		DELETE 2's from the table except first one
		Then rebuild XML
		*/