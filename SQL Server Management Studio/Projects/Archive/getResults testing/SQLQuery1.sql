select top 100

ID
,wests.warehouseTime
,wests.centreName
,wests.centreCode
--,ExamStateChangeAuditXml 
--,ExamStateChangeAuditXml.value('(/exam/stateChange/newStateID="10")', 'nvarchar(100)') as test
,west.StructureXML.value('(assessmentDetails/qualificationName)[1]', 'nvarchar(100)') as qualificationName
,foreName
,surName
,candidateRef
,wests.userMark
,wests.warehouseTime
--,ExamStateChangeAuditXml.value('count(/exam/stateChange/newStateID)', 'int') as CountOfNewStateIDs
--,ExamStateChangeAuditXml.value('count(/exam/stateChange[newStateID=12])', 'int') as CountOfTwelves
,west.StructureXML.value('(assessmentDetails/assessmentGroupReference)[1]', 'nvarchar(100)') as assessmentGroupReference
--,west.StructureXML
--,wests.ExportedToIntegration
--,west.ExportedToIntegration
,wests.userPercentage
,wests.userMark
,west.resultData

from WAREHOUSE_ExamSessionTable as west

	inner join WAREHOUSE_ExamSessionTable_Shreded as wests
	on wests.examSessionId = west.ID

where west.KeyCode = '5Z5TSLA3'
	--'135903035'--The one they reported
	--'135595470'-- My 'close' Test
order by wests.submittedDate


--2014-02-04 14:05:34.990 = warehouseTime