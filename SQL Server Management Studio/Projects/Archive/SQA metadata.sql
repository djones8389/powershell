SELECT *
FROM ProjectListTable
WHERE CONVERT(NVARCHAR(MAX), projectStructureXML) LIKE '%markedmetadata_%'

--And this will give you any projects that have values set for marked metadata:

SELECT ID, Name, ProjectDefaultXml.query('Defaults/StructureAttributes/Attribute[@Name="MarkedMetadata"]/Value')
FROM ProjectListTable
WHERE LEN(CONVERT(NVARCHAR(MAX), ProjectDefaultXml.query('Defaults/StructureAttributes/Attribute[@Name="MarkedMetadata"]/Value'))) > 0

/*
update ProjectListTable
set ProjectStructureXml = '<Pro ID="915" Nam="BTL Test 3103" CurrentMaxID="1312" UseAsTemplate="1" prB="lefthandtemplate1030x700.jpg" eBa="surpass_standard_left_editor" bac="sqalefteditor" opT="0" AttributeFilter="sta" pEv="0" alF="1" lMD="04/03/2013 10:56:31" lMU="975" SyncMarkSchemes="0">
  <Rec ID="917" Nam="Recycle Bin" />
  <Fol ID="1288" Nam="Templates">
    <Pag ID="1289" Nam="MCQ Template" lMU="1177" lMD="09/09/2013 14:16:25" alF="1" cmt="Recursive Check-in" tes="0" sta="0" pMU="1177" pMD="09/09/2013 12:32:39" quT="10" Purpose="Formative" Score="2" TotalMark="1" MarkingType="0" Topic="-1" pFV="-1" />
    <Pag ID="1290" Nam="MCQ_NO_GRAPHIC_IMPORT_TEMPLATE" lMU="1177" lMD="09/09/2013 12:32:40" MarkingType="0" sta="1" tes="0" alF="1" pMU="1177" pMD="09/09/2013 12:32:40" quT="10" cmt="Recursive Check-in" />
  </Fol>
  <Fol ID="1277" Nam="jl">
    <Fol ID="1310" Nam="test">
      <Pag ID="1283" Nam="jlquestion 3" markingSchemeFor="-1" isWordTemplate="F" isSourceItem="F" lMU="1177" lMD="27/09/2013 15:41:57" alF="0" tes="0" sta="6" pMD="27/09/2013 15:37:15" pMU="1177" cmt="Recursive Check-in" quT="22" pEv="1" Topic="james" />
      <Pag ID="1282" Nam="jlquestion 2" markingSchemeFor="-1" isWordTemplate="F" isSourceItem="F" lMU="1177" lMD="27/09/2013 15:42:00" alF="0" tes="0" sta="6" pMU="1177" pMD="27/09/2013 15:37:12" cmt="Recursive Check-in" quT="22" pEv="1" Topic="james" />
      <Pag ID="1281" Nam="jlquestion 1" markingSchemeFor="-1" isWordTemplate="F" isSourceItem="F" lMU="1177" lMD="27/09/2013 15:42:02" alF="0" tes="0" sta="6" pMU="1177" pMD="27/09/2013 15:37:10" cmt="Recursive Check-in" quT="22" pEv="1" Topic="james" />
    </Fol>
    <Fol ID="1311" Nam="test2">
      <Pag ID="1279" Nam="Task 1" lMU="1177" lMD="27/09/2013 16:11:22" alF="1" pMU="1177" pMD="27/09/2013 15:44:05" quT="11,22" TotalMark="15" tes="0" sta="6" OutcomeNumber="Internal Assessment Component" Keywords="Level 2, English, Writing" UnitTitle="Writing" MarkingType="1" Purpose="Summative" cmt="Recursive Check-in" UnitNumber="FP1J22" pEv="1" Topic="(unspecified)" />
      <Pag ID="1280" Nam="Copy of Copy of Task 1 Mark Scheme" markingSchemeFor="1279" isWordTemplate="F" isSourceItem="F" lMU="1177" lMD="27/09/2013 15:44:03" alF="0" tes="0" sta="6" pMU="1177" pMD="09/09/2013 14:16:34" cmt="Recursive Check-in" quT="22" pEv="1" Typ="8" />
      <Pag ID="1285" Nam="jlnew" markingSchemeFor="-1" isWordTemplate="F" isSourceItem="F" lMU="1177" lMD="27/09/2013 16:11:19" alF="1" tes="0" sta="6" pMU="1177" pMD="27/09/2013 15:43:26" quT="22" pEv="1" cmt="Recursive Check-in" Topic="(unspecified)" />
      <Pag ID="1278" Nam="Task 3 q15" markingSchemeFor="-1" isWordTemplate="F" isSourceItem="F" lMU="1177" lMD="27/09/2013 15:53:06" TotalMark="2" quT="11,22" Keywords="Functional, Skills, English, Level 2" UnitTitle="Reading" MarkingType="1" Purpose="1" sta="6" tes="0" alF="0" pMU="1177" pMD="27/09/2013 15:53:06" UnitNumber="FP1H22" AssessmentVersion="4" pEv="1" />
    </Fol>
  </Fol>
  <Pag ID="1259" Nam="Copy of ojge Mark Scheme" markingSchemeFor="1257" isWordTemplate="F" isSourceItem="F" lMU="1177" lMD="09/09/2013 14:16:47" alF="1" tes="0" sta="0" pMD="09/09/2013 12:33:01" pMU="1177" quT="22" cmt="Recursive Check-in" Typ="8" />
  <Pag ID="1256" Nam="Copy of Copy of MCQ Mark Scheme" markingSchemeFor="1251" isWordTemplate="F" isSourceItem="F" lMU="1177" lMD="09/09/2013 14:16:48" alF="0" tes="0" sta="6" pMU="1177" pMD="09/09/2013 12:33:02" quT="22" cmt="Recursive Check-in" pEv="1" Typ="8" />
  <Pag ID="1274" Nam="Test Page" markingSchemeFor="-1" isWordTemplate="F" isSourceItem="F" lMD="09/09/2013 14:16:50" lMU="1177" quT="10" alF="0" tes="0" sta="6" pMU="1177" pMD="09/09/2013 12:33:04" pEv="1" cmt="Recursive Check-in" />
  <Pag ID="1275" Nam="TestPage2" markingSchemeFor="-1" isWordTemplate="F" isSourceItem="F" lMU="1177" lMD="09/09/2013 14:16:53" quT="10" alF="0" tes="0" sta="6" pMD="09/09/2013 12:33:06" pMU="1177" pEv="1" cmt="Recursive Check-in" />
  <Fol ID="1288" Nam="Import">
    <Fol ID="1289" Nam="1.4 lower">
      <Pag ID="1290" Nam="00821v1" chO="975" chO2="BTL144" sta="0" oID="DaveJTest" originalAuthor="DaveJ" quT="10" TemplateType="MCQ_NO_GRAPHIC" alF="1" tes="0" DateImported="28012013" />
      <Pag ID="1291" Nam="00821v2" sta="1" oID="DaveJTest" originalAuthor="DaveJ" quT="10" TemplateType="MCQ_NO_GRAPHIC" alF="1" tes="0" DateImported="28012014" lMD="09/09/2013 14:16:55" lMU="1177" cmt="Recursive Check-in" pMU="1177" pMD="09/09/2013 12:33:09" />
      <Pag ID="1292" Nam="00821v3" sta="2" oID="DaveJTest" originalAuthor="DaveJ" quT="10" TemplateType="MCQ_NO_GRAPHIC" alF="1" tes="0" DateImported="28012015" lMD="09/09/2013 14:16:56" lMU="1177" cmt="Recursive Check-in" pMU="1177" pMD="09/09/2013 12:33:10" />
      <Pag ID="1293" Nam="00821v4" sta="3" oID="DaveJTest" originalAuthor="DaveJ" quT="10" TemplateType="MCQ_NO_GRAPHIC" alF="1" tes="0" DateImported="28012016" lMD="09/09/2013 14:16:57" lMU="1177" cmt="Recursive Check-in" pMU="1177" pMD="09/09/2013 12:33:10" />
      <Pag ID="1294" Nam="00821v5" sta="4" oID="DaveJTest" originalAuthor="DaveJ" quT="10" TemplateType="MCQ_NO_GRAPHIC" alF="1" tes="0" DateImported="28012017" lMD="09/09/2013 14:16:58" lMU="1177" cmt="Recursive Check-in" pMU="1177" pMD="09/09/2013 12:33:11" />
      <Pag ID="1295" Nam="00821v6" sta="5" oID="DaveJTest" originalAuthor="DaveJ" quT="10" TemplateType="MCQ_NO_GRAPHIC" alF="0" tes="0" DateImported="28012018" lMD="09/09/2013 14:16:59" lMU="1177" cmt="Recursive Check-in" pEv="1" pMU="1177" pMD="09/09/2013 12:33:12" />
      <Pag ID="1296" Nam="00821v7" sta="6" oID="DaveJTest" originalAuthor="DaveJ" quT="10" TemplateType="MCQ_NO_GRAPHIC" alF="0" tes="0" DateImported="28012019" lMD="09/09/2013 14:17:01" lMU="1177" cmt="Recursive Check-in" pEv="1" pMU="1177" pMD="09/09/2013 12:33:15" />
      <Pag ID="1297" Nam="00821v8" sta="7" oID="DaveJTest" originalAuthor="DaveJ" quT="10" TemplateType="MCQ_NO_GRAPHIC" alF="1" tes="0" DateImported="28012020" lMD="09/09/2013 14:17:03" lMU="1177" cmt="Recursive Check-in" pMU="1177" pMD="09/09/2013 12:33:17" />
      <Pag ID="1298" Nam="00821v9" sta="8" oID="DaveJTest" originalAuthor="DaveJ" quT="10" TemplateType="MCQ_NO_GRAPHIC" alF="1" tes="0" DateImported="28012021" />
      <Pag ID="1308" Nam="djtest" markingSchemeFor="-1" isWordTemplate="F" isSourceItem="F" lMU="1177" lMD="09/09/2013 14:16:06" alF="1" tes="0" sta="0" pMD="09/09/2013 12:32:31" pMU="1177" cmt="Recursive Check-in" />
    </Fol>
    <Pag ID="1299" sta="0" quT="10" TemplateType="MCQ_NO_GRAPHIC" alF="1" tes="0" lMD="09/09/2013 14:17:05" lMU="1177" cmt="Recursive Check-in" pMU="1177" pMD="09/09/2013 12:33:19" />
    <Pag ID="1300" sta="0" quT="10" TemplateType="MCQ_NO_GRAPHIC" alF="1" tes="0" lMD="09/09/2013 14:17:06" lMU="1177" cmt="Recursive Check-in" pMU="1177" pMD="09/09/2013 12:33:19" />
    <Pag ID="1301" sta="0" quT="10" TemplateType="MCQ_NO_GRAPHIC" alF="1" tes="0" lMD="09/09/2013 14:17:06" lMU="1177" cmt="Recursive Check-in" pMU="1177" pMD="09/09/2013 12:33:20" />
    <Pag ID="1302" sta="0" quT="10" TemplateType="MCQ_NO_GRAPHIC" alF="1" tes="0" lMD="09/09/2013 14:17:07" lMU="1177" cmt="Recursive Check-in" pMU="1177" pMD="09/09/2013 12:33:21" />
    <Pag ID="1303" sta="0" quT="10" TemplateType="MCQ_NO_GRAPHIC" alF="1" tes="0" lMD="09/09/2013 14:17:08" lMU="1177" cmt="Recursive Check-in" pMU="1177" pMD="09/09/2013 12:33:22" />
    <Pag ID="1304" sta="0" quT="10" TemplateType="MCQ_NO_GRAPHIC" alF="1" tes="0" lMD="09/09/2013 14:17:09" lMU="1177" cmt="Recursive Check-in" pMU="1177" pMD="09/09/2013 12:33:23" />
    <Pag ID="1305" sta="0" quT="10" TemplateType="MCQ_NO_GRAPHIC" alF="1" tes="0" lMD="09/09/2013 14:17:10" lMU="1177" cmt="Recursive Check-in" pMU="1177" pMD="09/09/2013 12:33:24" />
  </Fol>
  <Pag ID="1312" Nam="DaveJPage1" markingSchemeFor="-1" isWordTemplate="F" isSourceItem="F" Typ="1" sta="5" lMU="975" lMD="01/07/2014 11:08:20" pEv="1" alF="0" MarkedMetadata_1="1" tes="0" MarkedMetadata_4="5" />
</Pro>'
where ID = 915
*/

select * from ProjectListTable where ID = 915