USE UAT_OCR_CONTENTPRODUCER_IP

Select ID, Name, projectStructureXML, ProjectDefaultXML, PublishSettingsXML
from ProjectListTable 
where ID IN (6014)--327,340,6009,6007,6012,6042,6014,6010,6015)



--Need to omit recycle bin items--
--Only run this for pages in my other query
--What if it's in IB but not CP?   Ignore these - must have been deleted in CP by the user
--What if it isn't in IB currently, but the publish will put it in there?   We only care about content currently in ItemBank

USE UAT_OCR_CONTENTPRODUCER_IP

select 
	cast(A.ProjectID as nvarchar(10)) + 'P' + cast(A.ItemRef as nvarchar(10)) as ProjectAndPage
   , A.Version as CPVersion
   , B.Version as IBVersion

From 
(
SELECT
	PLT.ID as ProjectID
	, a.b.value('@ID','nvarchar(20)') as ItemRef
	, PT.Ver as Version
from ProjectListTable as PLT

cross apply ProjectStructureXML.nodes('Pro//Pag') a(b)

inner join PageTable as PT
on PT.ParentID = PLT.ID and PT.ID = cast(PLT.ID as nvarchar(10)) + 'P' + cast(a.b.value('@ID','nvarchar(20)') as nvarchar(10))

where 
	--Check Status of the page is within the publishable range
	 a.b.value('@sta','int') Between PublishSettingsXML.value('(/PublishConfig/StatusLevelFrom)[1]','int') and PublishSettingsXML.value('(/PublishConfig/StatusLevelTo)[1]','int')
) A

INNER JOIN (

	select  ProjectID, ItemRef, max(Version) as Version 
		from UAT_OCR_ItemBank_IP..ItemTable 
			group by ProjectID, ItemRef 
) B

ON A.ProjectID = B.ProjectID
	and A.ItemRef = B.ItemRef
	and A.Version <> B.Version
	






/*

select *
from PageTable 
where ID IN ('6014P6033')
--where ID IN ('6014P6018','6014P6020','6014P6021','6014P6022','6014P6023','6014P6030','6014P6039','6014P6040','6014P6041','6014P6042','6014P6044');

select top 5 * from UAT_OCR_ItemBank_IP..ItemTable

ProjectID	ItemRef	Version
327			331		20
327			331		21
327			334		20
327			335		16
327			336		34





USE UAT_OCR_ITEMBANK_IP

SELECT
	 AssessmentTable.IsValid
	, AssessmentGroupTable.IsValid
	, QualificationTable.IsValid
	,	AssessmentTable.*
FROM AssessmentTable 


	inner join AssessmentGroupTable
	on AssessmentGroupTable.ID = AssessmentTable.AssessmentGroupID
	
	inner join QualificationTable
	on QualificationTable.ID = AssessmentGroupTable.QualificationID
	
WHERE AssessmentTable.ID = 373
*/

