--SELECT *
--  FROM [UAT_OCR_SecureAssess_2014_12_02].[dbo].[UserTable]
--  where surname like '%lynch%' and forename like '%talibah%'
--GO




SELECT  [UserExamHistoryVersionTrackerTable].[id]
      ,[UserExamHistoryVersionTrackerTable].[userID]
      ,[UserExamHistoryVersionTrackerTable].[examID]
      ,[UserExamHistoryVersionTrackerTable].[versionID]
      ,[UserExamHistoryVersionTrackerTable].[examSessionID]
      --,ExamSessionTable.StructureXML
      ,ExamSessionTable.examState
      ,ScheduledExamsTable.CreatedBy
      ,ScheduledExamsTable.CreatedDateTime
      ,ScheduledExamsTable.ID
      ,ExamStateChangeAuditTable.StateChangeDate
      
  FROM [dbo].[UserExamHistoryVersionTrackerTable]
  INNER JOIN ExamSessionTable ON
  ExamSessionTable.id = [UserExamHistoryVersionTrackerTable].[examSessionID]
  inner join ScheduledExamsTable ON
  ScheduledExamsTable.id = ExamSessionTable.ScheduledExamId
  inner join ExamStateChangeAuditTable ON
  ExamStateChangeAuditTable.ExamSessionID = ExamSessionTable.ID AND ExamStateChangeAuditTable.NewState = 1
  where [UserExamHistoryVersionTrackerTable].userid = 19766
  
  ORDER BY  ExamStateChangeAuditTable.StateChangeDate
GO

--SELECT * FROM userTable where id = 8798

select count(id) from ExamSessionTable where Examstate = 13

select * from RolePermissionsTable where roleID in (
select RoleID from AssignedUserRolesTable where userid = 8798
)
SELECT [ID]
      ,[Name]
      ,[Description]
      ,[Type]
      ,[HasViewEdit]
      ,[UsesQualLevel]
  FROM [UAT_OCR_SecureAssess_2014_12_02].[dbo].[PermissionsTable]
GO




select * from AssignedUserRolesTable where userid = 8798

select WAREHOUSE_ExamSessionTable_Shreded.* from WAREHOUSE_ExamSessionTable_Shreded
	inner join WAREHOUSE_ExamSessionTable
	on WAREHOUSE_ExamSessionTable_Shreded.examSessionId = WAREHOUSE_ExamSessionTable.ID
where WAREHOUSE_ExamSessionTable.ExamSessionID in (SELECT  [examSessionID]
  FROM [UAT_OCR_SecureAssess_2014_12_02].[dbo].[UserExamHistoryVersionTrackerTable]
  where userid = 19766
  ) 


--select * from ExamSessionTable
--where id in (SELECT  [examSessionID]
--  FROM [UAT_OCR_SecureAssess_2014_12_02].[dbo].[UserExamHistoryVersionTrackerTable]
--  where userid = 19766
--  ) 

