SELECT	 
		 DB_NAME() AS [database],
		 OBJECT_NAME(s.OBJECT_ID) AS [table],
		 i.name AS [index],
		 i.index_id,
         s.user_updates,
         s.user_seeks + s.user_scans + s.user_lookups AS [user_selects],
         [Difference] = user_updates - (user_seeks + user_scans + user_lookups),
		  s.[last_user_seek], s.[last_user_scan], s.[last_user_lookup], s.[last_user_update] 
 FROM		sys.dm_db_index_usage_stats AS s WITH (NOLOCK)
 INNER JOIN sys.indexes AS i WITH (NOLOCK)
		 ON s.[object_id] = i.[object_id]
		AND i.index_id = s.index_id
WHERE	 OBJECTPROPERTY(s.[OBJECT_ID],'IsUserTable') = 1

 AND	 i.index_id > 1
 AND (
	   ([last_user_seek] IS NULL OR [last_user_seek] < DATEADD(MONTH, -1, GETDATE()) )			
		     AND
	   ([last_user_scan] IS NULL OR [last_user_scan] < DATEADD(MONTH, -1, GETDATE()) )
	   		AND
	   ([last_user_lookup] IS NULL OR [last_user_lookup] < DATEADD(MONTH, -1, GETDATE()) )
			AND
	   ([last_user_update] IS NULL OR [last_user_update] < DATEADD(MONTH, -1, GETDATE()) )
	
	)

ORDER BY [DIFFERENCE] DESC
OPTION	 (RECOMPILE);



SELECT DATEADD(MONTH, -1, GETDATE()), *
FROM		sys.dm_db_index_usage_stats s
INNER JOIN sys.indexes AS i WITH (NOLOCK)
		 ON s.[object_id] = i.[object_id]
		AND i.index_id = s.index_id
where i.name = 'IX_ExamSessionTagReferenceTable_ExamSessionId'
ORDER BY [last_user_seek]  DESC

--2015-09-01 14:16:14.010