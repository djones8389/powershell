begin tran

declare @myKeycode nvarchar(10) = 'G28BZNA5'
declare @errorReturnString nvarchar(MAX)
declare @successReturnString nvarchar(MAX)
declare @startProcess nvarchar(MAX)

declare @tableVar as table 
(
	id int,
	examstate int,
	keycode nvarchar(10)
)

insert into @tableVar
select 	id, examstate, keycode	
from ExamSessionTable where KeyCode = @myKeycode


	--1)  Check if it's in Warehouse, if so, we can proceed.  If not,  throw an error--
	IF EXISTS 
		(	
			Select ExamsessionID 
				from WAREHOUSE_ExamSessionTable 
					where ExamsessionID = (select ID from @tableVar)
		)
	
	--set @startProcess = 'This exists in the WarehouseTables, lets start the job'

	BEGIN 
		
	
	--2)  Delete from Live_Shredded Table--
	delete from ExamSessionTable_Shredded 
		where examsessionid in  ( 
			select ID from @tableVar
		);
	
	
	--3)  Change the examState back to '16', which is now allowed, with no PK/FK exception--	
	update ExamSessionTable
	set previousExamState = EST.examState, examState = 16
	from ExamSessionTable as EST
	
	inner join @tableVar as TV
	on TV.Id = EST.id
	
	where TV.Id = EST.id;
	
	
	--4)  Delete from WarehouseTables, as it's not needed there anymore--		
	declare  @warehouseTable as table
	(
		warehouseID int,
		warehouseExamSessionID int,
		warehouseKeycode nvarchar(10)
	)
	
	
	insert into @warehouseTable
	select id, examsessionid, keycode
	from WAREHOUSE_ExamSessionTable
	where KeyCode = @myKeycode;
	

	delete from WAREHOUSE_ExamStateAuditTable
		where WarehouseExamSessionID in (
			select warehouseID from @warehouseTable
		);

	delete from WAREHOUSE_ExamSessionTable
		where examsessionid in  ( 
			select warehouseExamSessionID from @warehouseTable
		);
		
    SET @successReturnString = 'This was in the warehouse, so we have deleted it from ExamSessionTable_Shredded'
    
	END

ELSE

	BEGIN
	
	SET @errorReturnString = 'This was not in the warehouse,  so we have not deleted it from ExamSessionTable_Shredded'
	
	END

select @errorReturnString

rollback