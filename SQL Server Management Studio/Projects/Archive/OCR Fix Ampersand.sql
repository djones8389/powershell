drop table #myTable


create table #myTable (ID int, AssessmentName nvarchar(max), CommentHistoryCommentText xml)

insert into #myTable

SELECT ID
      ,AssessmentName
      ,CommentsHistory--c.t.value('(.)[1]', 'nvarchar(max)') AS [CommentHistoryCommentText]
            
FROM AssessmentTable
CROSS APPLY CommentsHistory.nodes('/commentHistory/comment/text') c(t)
WHERE CommentsHistory IS NOT NULL
AND c.t.value('(.)[1]', 'nvarchar(max)') LIKE N'%&%'


--select top 5 commentshistory,* from AssessmentTable where ID = 446
select 
		ID 
	   ,AssessmentName
	   , [CommentHistoryCommentText]
from #myTable



declare @tablevariable table (MyID int, MyAssessmentName nvarchar(max), MyCommentHistoryCommentText xml)



/*For each assessment*/
DECLARE ExamSessions CURSOR FOR SELECT ID FROM #myTable;
DECLARE @ID INT;

OPEN ExamSessions;
FETCH NEXT FROM ExamSessions INTO @ID;

WHILE @@FETCH_STATUS = 0
BEGIN

     declare @MyID int, 
			 @MyAssessmentName nvarchar(max), 
			 @MyCommentHistoryCommentText xml


      SELECT      @MyID = ID
                  ,@MyAssessmentName = AssessmentName
                  ,@MyCommentHistoryCommentText = CommentsHistory
      FROM  AssessmentTable 
      
      where ID = @ID
      
    declare @replaceamp nchar(3) = 'and'
	SET @MyCommentHistoryCommentText.modify('replace value of (/commentHistory/comment/text[1] = sql:variable("@SectionID")]/@userPercentage)[1] with sql:variable("@SectionUserPercentage")');
  
  
   --  SET @ResultData.modify('replace value of (/exam/section[@id = sql:variable("@SectionID")]/@userMark)[1] with sql:variable("@SectionUserMark")');
      
	insert into @tablevariable
	VALUES(@MyID, @MyAssessmentName, @MyCommentHistoryCommentText)

		select * FROM @tablevariable
      
      FETCH NEXT FROM ExamSessions INTO @ID;
END

CLOSE ExamSessions;
DEALLOCATE ExamSessions;



