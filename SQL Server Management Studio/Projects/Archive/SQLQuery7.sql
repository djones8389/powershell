----------------WAREHOUSE_ExamSessionTable - ResultDataFull----------------

DECLARE @ExamUserPercentage[decimal](6, 3),
		@NewValue int = 30,
		@examSessionID int = 107841,
		@FSectionUserPercentage[decimal](6, 3);
		
select ID, resultData, resultDataFull from WAREHOUSE_ExamSessionTable
where id = @examSessionID

--Delete FROM [SADB].[dbo].[WAREHOUSE_ExamStateAuditTable]
--  Where WarehouseExamSessionID = @examSessionID  

update WAREHOUSE_ExamSessionTable
set resultDataFull.modify('replace value of (/assessmentDetails/assessment/@totalMark)[1] with sql:variable("@NewValue") ') 
where ID = @examSessionID

update WAREHOUSE_ExamSessionTable
set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/@totalMark)[1] with sql:variable("@NewValue") ')   
where ID = @examSessionID	


update WAREHOUSE_ExamSessionTable
SET @FSectionUserPercentage = CAST(((resultDataFull.value('(/assessmentDetails/assessment/@userMark)[1]', 'decimal(13, 10)') / resultDataFull.value('(/assessmentDetails/assessment/@totalMark)[1]', 'decimal(13, 10)')) * 100) AS [decimal](6, 3))
where ID = @examSessionID


update WAREHOUSE_ExamSessionTable
SET  resultDataFull.modify('replace value of (/assessmentDetails/assessment/@userPercentage)[1] with sql:variable("@FSectionUserPercentage")')
where ID = @examSessionID


update WAREHOUSE_ExamSessionTable
SET  resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/@userPercentage)[1] with sql:variable("@FSectionUserPercentage")')
where ID = @examSessionID
 
Select *
FROM [SADB].[dbo].[WAREHOUSE_ExamStateAuditTable]
  Where WarehouseExamSessionID = @examSessionID

select ID, resultData, resultDataFull from WAREHOUSE_ExamSessionTable
where id = @examSessionID