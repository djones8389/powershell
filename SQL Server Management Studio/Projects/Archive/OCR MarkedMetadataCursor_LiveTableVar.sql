SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

DECLARE @ListOfExamSessions table
(
	ExamSessionID INT
)


INSERT @ListOfExamSessions(ExamSessionID)
	select ID
		from WAREHOUSE_ExamSessionTable 
			where ID IN (85655,85654,85653);
			

--Select out to look at the originals--
select ID, KeyCode, ResultData from WAREHOUSE_ExamSessionTable where ID IN (SELECT ExamSessionID FROM @ListOfExamSessions);
select examSessionId, KeyCode, ResultData from WAREHOUSE_ExamSessionTable_Shreded where examSessionId IN (SELECT ExamSessionID FROM @ListOfExamSessions);


DECLARE MarkedMetadata CURSOR FOR 
select 
	ID
	,s.e.value('@id', 'nvarchar(50)') as ItemID
	,s.e.value('sum(mark/@mark)', 'float') as TotalMarkPerItem
	,s.e.value('sum(//@mark)', 'float') as TotalMarkPerExam
from WAREHOUSE_ExamSessionTable

cross apply ResultData.nodes('exam/section/item') s(e)

where ID IN (SELECT ExamSessionID FROM @ListOfExamSessions);


DECLARE @ESID int, @ItemID nvarchar(12), @totalMarkItem int, @totalMarkExam int;

Open MarkedMetadata;

FETCH NEXT FROM MarkedMetadata into  @ESID, @ItemID, @totalMarkItem, @totalMarkExam;

WHILE @@FETCH_STATUS = 0

BEGIN

delete from Warehouse_ExamStateAuditTable where WarehouseExamSessionID IN (SELECT ExamSessionID FROM @ListOfExamSessions);

--Update Item userMark--
Update WAREHOUSE_ExamSessionTable
set ResultData.modify('replace value of(/exam/section/item[@id = sql:variable("@ItemID")]/@userMark)[1] with sql:variable("@totalMarkItem")')
where ID = @ESID;

delete from Warehouse_ExamStateAuditTable where WarehouseExamSessionID IN (SELECT ExamSessionID FROM @ListOfExamSessions);
							
--Update Section userMark--													
Update WAREHOUSE_ExamSessionTable
set ResultData.modify('replace value of(/exam/section/@userMark)[1] with sql:variable("@totalMarkExam")')
where ID = @ESID;

delete from Warehouse_ExamStateAuditTable where WarehouseExamSessionID IN (SELECT ExamSessionID FROM @ListOfExamSessions);

--Update Exam userMark--
Update WAREHOUSE_ExamSessionTable
set ResultData.modify('replace value of(/exam/@userMark)[1] with sql:variable("@totalMarkExam")')
where ID = @ESID;

delete from Warehouse_ExamStateAuditTable where WarehouseExamSessionID IN (SELECT ExamSessionID FROM @ListOfExamSessions);


DECLARE @ExamUserPercentage [decimal] (6, 3);
SELECT @ExamUserPercentage = CAST(((resultdata.value('(/exam/@userMark)[1]', 'decimal(13, 10)') / resultdata.value('(/exam/@totalMark)[1]', 'decimal(13, 10)')) * 100) AS [decimal](6, 3))
FROM WAREHOUSE_ExamSessionTable
WHERE ID = @ESID;

--Update Section userPercentage--
UPDATE WAREHOUSE_ExamSessionTable
SET resultdata.modify('replace value of (/exam/section/@userPercentage)[1] with sql:variable("@ExamUserPercentage")')
where ID = @ESID;

delete from Warehouse_ExamStateAuditTable where WarehouseExamSessionID IN (SELECT ExamSessionID FROM @ListOfExamSessions);

--Update Exam userPercentage--

UPDATE WAREHOUSE_ExamSessionTable
SET resultdata.modify('replace value of (/exam/@userPercentage)[1] with sql:variable("@ExamUserPercentage")')
where ID = @ESID;

delete from Warehouse_ExamStateAuditTable where WarehouseExamSessionID IN (SELECT ExamSessionID FROM @ListOfExamSessions);

FETCH NEXT FROM MarkedMetadata into  @ESID, @ItemID, @totalMarkItem, @totalMarkExam;

END

CLOSE  MarkedMetadata;
DEALLOCATE  MarkedMetadata;


UPDATE WAREHOUSE_ExamSessionTable_Shreded
SET ResultData = WEST.ResultData
FROM WAREHOUSE_ExamSessionTable_Shreded

INNER JOIN WAREHOUSE_ExamSessionTable as WEST
ON WAREHOUSE_ExamSessionTable_Shreded.ExamSessionID = WEST.ID

where WEST.ID = WAREHOUSE_ExamSessionTable_Shreded.ExamSessionID
	and west.ID in (SELECT ExamSessionID FROM @ListOfExamSessions);



select ID, KeyCode, ResultData from WAREHOUSE_ExamSessionTable where ID IN (SELECT ExamSessionID FROM @ListOfExamSessions);
select examSessionId, KeyCode, ResultData from WAREHOUSE_ExamSessionTable_Shreded where examSessionId IN (SELECT ExamSessionID FROM @ListOfExamSessions);



/*

select top 10 ID, KeyCode, ResultData, StructureXML, previousExamState, ExamStateChangeAuditXml
	 from WAREHOUSE_ExamSessionTable 
	--where ID IN (SELECT ExamSessionID FROM @ListOfExamSessions);
	where PreviousExamState <> 10
		and resultData.exist('exam/section/item/mark[@mark]') = 1
		ORDER BY ID DESC;



update WAREHOUSE_ExamSessionTable
set resultData = '<exam passMark="18" passType="0" originalPassMark="18" originalPassType="0" totalMark="34" userMark="0" userPercentage="0" passValue="1" originalPassValue="1" totalTimeSpent="1767577" closeBoundaryType="0" closeBoundaryValue="0" grade="Fail n" originalGrade="Fail n">
  <gradeBoundaryData>
    <gradeBoundaries passLevelType="0" adjustedGradeBoundaries="0">
      <grade modifier="lt" value="18" description="Fail" />
      <grade modifier="gt" value="18" description="Fail n" />
      <grade modifier="gt" value="22" description="Pass" userCreated="1" />
    </gradeBoundaries>
  </gradeBoundaryData>
  <section id="1" passMark="0" passType="1" name="Section 1" totalMark="34" userMark="18" userPercentage="52.941" passValue="1" totalTimeSpent="1767577" itemsToMark="0">
    <item id="6049P6465" name="Copy of BW19_Info1" totalMark="0" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="5304" userAttempted="0" version="25" />
    <item id="6049P6391" name="BW19_Q01" totalMark="21" userMark="0" actualUserMark="11" markingType="1" markerUserMark="11" viewingTime="1126774" userAttempted="1" version="22">
      <mark mark="2" learningOutcome="3" displayName="2W4 Candidate has used grammar accurately" maxMark="3" />
      <mark mark="2" learningOutcome="1" displayName="2W2 Candidate has used spelling accurately" maxMark="3" />
      <mark mark="3" learningOutcome="0" displayName="2W1 Candidate has presented relevant ideas and information effectively" maxMark="4" />
      <mark mark="0" learningOutcome="4" displayName="2W5 Candidate has used an appropriate document format" maxMark="4" />
      <mark mark="3" learningOutcome="5" displayName="2W6 Candidate has used an appropriate style and tone" maxMark="4" />
      <mark mark="1" learningOutcome="2" displayName="2W3 Candidate has used punctuation accurately" maxMark="3" />
    </item>
    <item id="6049P6392" name="BW19_Q02" totalMark="13" userMark="0" actualUserMark="7" markingType="1" markerUserMark="7" viewingTime="635499" userAttempted="1" version="21">
      <mark mark="1" learningOutcome="3" displayName="2W4 Candidate has used grammar accurately" maxMark="2" />
      <mark mark="1" learningOutcome="1" displayName="2W2 Candidate has used spelling accurately" maxMark="2" />
      <mark mark="2" learningOutcome="0" displayName="2W1 Candidate has presented relevant ideas and information effectively" maxMark="3" />
      <mark mark="0" learningOutcome="4" displayName="2W5 Candidate has used an appropriate document format" maxMark="2" />
      <mark mark="2" learningOutcome="5" displayName="2W6 Candidate has used an appropriate style and tone" maxMark="2" />
      <mark mark="1" learningOutcome="2" displayName="2W3 Candidate has used punctuation accurately" maxMark="2" />
    </item>
  </section>
</exam>'
where ID = 85653;

*/