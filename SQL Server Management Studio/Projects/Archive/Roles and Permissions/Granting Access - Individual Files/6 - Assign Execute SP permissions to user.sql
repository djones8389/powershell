IF OBJECT_ID('tempdb..##DBsandSPs') IS NOT NULL DROP TABLE tempdb..##DBsandSPs
 
DECLARE @Username nvarchar(MAX) = 'kevinh'
 
CREATE TABLE ##DBsandSPs (
	
	DatabaseName nvarchar(MAX)
	, SPName nvarchar(MAX) 

)


exec sp_MSforeachdb '

	use [?];

   insert into ##DBsandSPs(DatabaseName, SPName)
   select ''?'', SPECIFIC_NAME 
   from information_schema.routines 
   
   inner join sys.databases
   on sys.databases.name collate Latin1_General_CI_AS = information_schema.routines.SPECIFIC_CATALOG
  
   where routine_type = ''PROCEDURE''
	 and SPECIFIC_CATALOG NOT IN (''master'', ''msdb'', ''SurpassReportServer'', ''SurpassReportServerTempDB'')
		and state_desc = ''ONLINE''
'	


--SELECT * FROM ##DBsandSPs

DECLARE @GrantExecSPAccess varchar(MAX) = ''


SELECT @GrantExecSPAccess += CHAR(13) + 'use [' + DatabaseName + ']; ' + 'GRANT EXECUTE ON [' + SPName + N'] TO '+@Username+';'
from ##DBsandSPs


EXEC(@GrantExecSPAccess)

/*

PRINT(@GrantExecSPAccess)


*/










--select SPECIFIC_CATALOG
--,  SPECIFIC_NAME 
--  from AAT_ContentProducer.information_schema.routines 
  
--  inner join sys.databases
--  on sys.databases.name = information_schema.routines.SPECIFIC_CATALOG
  
-- where routine_type = 'PROCEDURE'
--	and state_desc = 'ONLINE'
 





--SELECT @GrantExecSPAccess += CHAR(13) + 'use ' + DatabaseName + '; '
--from ##DBsandSPs

--SELECT @GrantExecSPAccess += CHAR(13) + 'GRANT EXECUTE ON [' + SPName + N'] TO '+@Username+';' 
--from ##DBsandSPs

--PRINT(@GrantExecSPAccess)

--SELECT  'use [' + DatabaseName + ']; GRANT EXECUTE ON [' + SPName + N'] TO '+@Username+';'
--from ##DBsandSPs




--DECLARE @GrantExecSPAccess nvarchar(MAX) = ''

--SELECT @GrantExecSPAccess += CHAR(13) + 'use ' + DatabaseName + '; GRANT EXECUTE ON [' + SPName + N'] TO '+@Username+';'
--from ##DBsandSPs

--PRINT(@GrantExecSPAccess)