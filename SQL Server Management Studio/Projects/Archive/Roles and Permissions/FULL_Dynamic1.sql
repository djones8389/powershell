CREATE PROCEDURE ##GenPass(@Len INT = 32, @Password NVARCHAR(MAX) OUTPUT)
AS
BEGIN
SET @Password = (
SELECT XChar AS [text()]
FROM  (
              SELECT TOP(@Len) XChar
              FROM  (
                     VALUES  ('A'),('B'),('C'),('D'),('E'),('F'),('G'),('H'),('I'),('J'),('K'),('L'),('M'),('N'),('O'),('P'),('Q'),('R'),('S'),('T'),('U'),('V'),('W'),('X'),('Y'),('Z'),
                                  ('a'),('b'),('c'),('d'),('e'),('f'),('g'),('h'),('i'),('j'),('k'),('l'),('m'),('n'),('o'),('p'),('q'),('r'),('s'),('t'),('u'),('v'),('w'),('x'),('y'),('z'),
                                  ('0'),('1'),('2'),('3'),('4'),('5'),('6'),('7'),('8'),('9'),
                                  ('!'),('�'),('$'),('%'),('^'),('*'),('('),(')'),('-'),('_'),('='),('+'),('@'),('~'),('#'),('?')
                           ) AS XChars(XChar)
              ORDER BY ABS(CHECKSUM(NEWID()))
              ) AS [XChars]
FOR XML PATH('')
)
END;
GO



DECLARE @DatabaseName nvarchar(100) = 'AdventureWorks2008R2'	    --Leave Blank for ALL,  or specify one DB
DECLARE @Username nvarchar(100) = 'Jonesy3'							--Enter username in here
DECLARE @AccessRequired nvarchar(100) = 'Read'						--NEEDS to be read or write, or it won't work 	

--Email them the credentials--
DECLARE @UsersEmail nvarchar(50) = 'zohaib.ghani@btl.com'
DECLARE @Subject nvarchar(30) = 'Your Staging Credentials';
DECLARE @Body nvarchar(MAX);

DECLARE @ReadRole nvarchar(100) = 'btl_developer_access'
DECLARE @WriteRole nvarchar(100) = 'btl_developer_full_access'

DECLARE @CreateLogin nvarchar(MAX) = ''
DECLARE @AssignLogin nvarchar(MAX)= ''
DECLARE @GrantRole nvarchar(MAX)= ''
DECLARE @GrantExecSPAcccess nvarchar(MAX) = ''
DECLARE @AssociateXMLSchemaCollection nvarchar(MAX) = ''
DECLARE @ConnectionString nvarchar(MAX) = '' 




DECLARE	@return_value int,
		@Password nvarchar(max)


EXEC	##GenPass @Len = 32, @Password = @Password OUTPUT

DROP PROCEDURE ##GenPass

/*Create a new Login*/


SELECT @CreateLogin += CHAR(13) + 'EXEC sp_addlogin ' + '''' +  @Username + '''' + ',' + '''' + @Password +  '''' +  ';'


DECLARE @@BigTable TABLE (

		Schema_DatabaseName nvarchar(MAX)
	  , SchemaName nvarchar(50)
	  , XMLSchemaCollection_DatabaseName nvarchar(MAX)
	  , XMLSchemaName nvarchar(50)
	  , Role_DatabaseName nvarchar(MAX)
	  , RoleName nvarchar(50)
)
/*
DECLARE @SPsforDBs TABLE (
		DatabaseName nvarchar(MAX)
	  , SchemaName  nvarchar(50)
	  , SPName nvarchar (MAX)
)
*/

/*Write DB names and DB Schema into a Table Variable*/

INSERT @@BigTable(Schema_DatabaseName, SchemaName)
exec sp_MSforeachdb '

use [?];

select ''?''
	, Name 
from sys.schemas 
where 
	''?'' not in (''msdb'',''master'', ''SurpassReportServer'', ''SurpassReportServerTempDB'')
and name not in 
	(select name 
		from sysusers 
			where issqlrole = 1
			)
and name not in (''guest'',''INFORMATION_SCHEMA'',''sys'')
		and ''?'' in (select name from sys.databases where state_desc = ''ONLINE'')
'  

/*Write DB names and DB XMLSchemaCollections into a Table Variable*/

INSERT @@BigTable(XMLSchemaCollection_DatabaseName, XMLSchemaName)
EXEC sp_MSforeachdb '

	use [?];
	
	select ''?'', name 	
	from sys.xml_schema_collections
	
	where ''?'' not in (''master'', ''msdb'', ''tempdb'', ''model'')
	and name != ''sys''

'

INSERT @@BigTable(Role_DatabaseName, RoleName)
EXEC sp_MSforeachdb  '
USE [?];

--insert into #DatabaseRoleMemberShip
SELECT  ''?'', roleprinc.[name]
FROM sys.database_role_members members

RIGHT JOIN sys.database_principals roleprinc
ON roleprinc.[principal_id] = members.[role_principal_id]

where is_fixed_role  = 0
	and type = ''R''
	and name != ''public''
	and name in (''btl_developer_access'',''btl_developer_full_access'')
	--and roleprinc.[name] in (
		
	--	--Find user Associations

	--	SELECT  roleprinc.[name]
	--	FROM sys.database_role_members members

	--	INNER JOIN sys.database_principals roleprinc
	--	ON roleprinc.[principal_id] = members.[role_principal_id]

	--	where is_fixed_role  = 0

	--)
'

/*Write DB names and DB SP's into a Table Variable*/

/*
INSERT @SPsforDBs(DatabaseName, SchemaName ,SPName)
EXEC sp_MSforeachdb '

	use [?];

   select ''?'', SPECIFIC_SCHEMA,SPECIFIC_NAME 
   from information_schema.routines 
   
   inner join sys.databases
   on sys.databases.name collate Latin1_General_CI_AS = information_schema.routines.SPECIFIC_CATALOG
  
   where routine_type = ''PROCEDURE''
	 and SPECIFIC_CATALOG NOT IN (''master'', ''msdb'', ''SurpassReportServer'', ''SurpassReportServerTempDB'')
		and state_desc = ''ONLINE''
	'	
*/


--SELECT * FROM @@BigTable



IF LEN(@DatabaseName) > 1
BEGIN							
	(
	SELECT @AssignLogin += CHAR(13) + 'use [' + name + ']; ' + 'CREATE USER ' + @Username + ' FOR LOGIN [' + @Username + ']; '
	from sys.databases
	where name  = @DatabaseName
		
	)
	
	( 	
		
	SELECT @GrantRole += CHAR(13) +
	
	
	'use [' + A.name + ']; ' 	+
	
	
   Case When @AccessRequired = 'Read' and @ReadRole IN (select C.RoleName from @@BigTable where C.Role_DatabaseName = @DatabaseName)
			then 
			
			' EXEC sp_addrolemember '''  + @ReadRole + ''', ''' + @Username + '''; '
			
		When @AccessRequired = 'Read' and @ReadRole NOT IN (select C.RoleName from @@BigTable where C.Role_DatabaseName = @DatabaseName)
			then
			'CREATE ROLE '  + @ReadRole +  ';'  
			
			+ ' GRANT SELECT, VIEW DEFINITION ON SCHEMA::' + B.SchemaName + ' TO ' + @ReadRole +';' 				

			+ ' EXEC sp_addrolemember '''  + @ReadRole + ''', ''' + @Username + '''; '

		
		When @AccessRequired = 'Write'  and @WriteRole IN (select C.RoleName from @@BigTable where C.Role_DatabaseName = @DatabaseName)
			then 
		 
			' EXEC sp_addrolemember ''' +	 @WriteRole + ''', ''' + @Username + '''; '
		
		 When @AccessRequired = 'Write'  and @WriteRole NOT IN (select C.RoleName from @@BigTable where C.Role_DatabaseName = @DatabaseName)
			then	
		 
				'CREATE ROLE '  + @WriteRole +  ';'  
	
				+ ' GRANT ALTER, DELETE, EXECUTE, INSERT, SELECT, UPDATE, VIEW DEFINITION ON SCHEMA::' +B.SchemaName + ' TO ' + @WriteRole +';' 		   
						
				+ ' EXEC sp_addrolemember ''' +	 @WriteRole + ''', ''' + @Username + '''; '
									   
		end

	from sys.databases A
	
	LEFT join @@BigTable  B
	on B.Schema_DatabaseName COLLATE Latin1_General_CI_AS = a.name
	LEFT join @@BigTable C
	on C.Role_DatabaseName COLLATE Latin1_General_CI_AS = A.name
		
	where  a.name COLLATE Latin1_General_CI_AS  = @DatabaseName
	)
	
	(
	
	SELECT @AssociateXMLSchemaCollection +=CHAR(13) + 'use ' + A.name + ';'	+ 'GRANT EXECUTE ON XML SCHEMA COLLECTION::' + B.XMLSchemaName + ' To ' + @Username + ';'
		
	from sys.databases A
	INNER join @@BigTable  B
	on B.XMLSchemaCollection_DatabaseName COLLATE Latin1_General_CI_AS = A.name
	
	where B.XMLSchemaCollection_DatabaseName COLLATE Latin1_General_CI_AS  = @DatabaseName
	
	)
	
	
	(
		SELECT @ConnectionString +=CHAR(13) + 'Server=94.236.6.119; Database=' + name + '; User ID=' + @Username + '; Password=' + @Password +';' 
		from sys.databases
		where name not in ('master', 'msdb', 'tempdb', 'model')
		and state_desc = 'ONLINE'	
		and name = @DatabaseName
	)
	
	
--IF @AccessRequired = 'Write' 
--	(
	
--	SELECT @GrantExecSPAcccess +=CHAR(13) + 'use ' + name + ';'	+ 'GRANT EXECUTE ON [' + SchemaName + '].[' + SPName + '] TO '+@Username+';'
--	from sys.databases A
--	INNER join @SPsforDBs B
--	on B.DatabaseName = A.name
	
--	where name COLLATE Latin1_General_CI_AS  = @DatabaseName
	
--	)
	
END
ELSE IF LEN(@DatabaseName) !> 1

BEGIN							
	(
	SELECT @AssignLogin += CHAR(13) + 'use [' + name + ']; ' + 'CREATE USER ' + @Username + ' FOR LOGIN [' + @Username + ']; '
	from sys.databases
	where name not in ('master', 'msdb', 'tempdb', 'model')
		and state_desc = 'ONLINE'
		
		
	)
	 
	( 	
		
	SELECT @GrantRole += CHAR(13) +
	
	
	'use [' + A.name + ']; ' 	+
	
	
   Case When @AccessRequired = 'Read' and @ReadRole IN (select C.RoleName from @@BigTable where C.Role_DatabaseName = @DatabaseName)
		then 
			
			' EXEC sp_addrolemember '''  + @ReadRole + ''', ''' + @Username + '''; '
			
		When @AccessRequired = 'Read' and @ReadRole NOT IN (select C.RoleName from @@BigTable where C.Role_DatabaseName = @DatabaseName)
		then
			'CREATE ROLE '  + @ReadRole +  ' AUTHORIZATION [' + B.SchemaName +'];' 
			
			+ ' GRANT SELECT, VIEW DEFINITION ON SCHEMA::' + B.SchemaName + ' TO ' + @ReadRole +';' 				

			+ ' EXEC sp_addrolemember '''  + @ReadRole + ''', ''' + @Username + '''; '
		
		
		When @AccessRequired = 'Write'  and @WriteRole IN (select C.RoleName from @@BigTable where C.Role_DatabaseName = @DatabaseName)
		 then 
		 
			' EXEC sp_addrolemember ''' +	 @WriteRole + ''', ''' + @Username + '''; '
		
		 When @AccessRequired = 'Write'  and @WriteRole NOT IN (select C.RoleName from @@BigTable where C.Role_DatabaseName = @DatabaseName)
			then	
		 
				'CREATE ROLE '  + @WriteRole +  ' AUTHORIZATION [' + B.SchemaName +'];' 
	
				+ ' GRANT ALTER, DELETE, EXECUTE, INSERT, SELECT, UPDATE, VIEW DEFINITION ON SCHEMA::' +B.SchemaName + ' TO ' + @WriteRole +';' 		   
						
				+ ' EXEC sp_addrolemember ''' +	 @WriteRole + ''', ''' + @Username + '''; '
									   
	end

	from sys.databases A
	
	LEFT join @@BigTable  B
	on B.Schema_DatabaseName COLLATE Latin1_General_CI_AS = a.name
	LEFT join @@BigTable C
	on C.Role_DatabaseName COLLATE Latin1_General_CI_AS = A.name
		
	where  a.name COLLATE Latin1_General_CI_AS  = @DatabaseName
	)
	
	(
	
	SELECT @AssociateXMLSchemaCollection +=CHAR(13) + 'use ' + A.name + ';'	+ 'GRANT EXECUTE ON XML SCHEMA COLLECTION::' + B.XMLSchemaName + ' To ' + @Username + ';'
		
	from sys.databases A
	INNER join @@BigTable  B
	on B.XMLSchemaCollection_DatabaseName COLLATE Latin1_General_CI_AS = A.name
	
	where name not in ('master', 'msdb', 'tempdb', 'model')
		and state_desc = 'ONLINE'
	)
	
	
	(
		SELECT @ConnectionString +=CHAR(13) + 'Server=94.236.6.119; Database=' + name + '; User ID=' + @Username + '; Password=' + @Password +';' 
		from sys.databases
		where name not in ('master', 'msdb', 'tempdb', 'model')
		and state_desc = 'ONLINE'	
		
	)
	
	
--IF @AccessRequired = 'Write' 
--	(

--	SELECT @GrantExecSPAcccess +=CHAR(13) + 'use ' + name + ';'	+ 'GRANT EXECUTE ON [' + SchemaName + '].[' + SPName + '] TO '+@Username+';'
--	from sys.databases A
--	INNER join @SPsforDBs B
--	on B.DatabaseName = A.name
	
--	where name not in ('master', 'msdb', 'tempdb', 'model')
--		and state_desc = 'ONLINE'
--	)
	
END

PRINT(@ConnectionString);
PRINT(@CreateLogin);
PRINT(@AssignLogin);
PRINT(@GrantRole);
PRINT(@AssociateXMLSchemaCollection); --IF NULL, it won't create a query, which is good--
--PRINT(@GrantExecSPAcccess);


--SET @Body = 'Server=94.236.6.119; Database=' + @DatabaseName + '; User ID=' + @Username + ';Password=' + @Password +';'


--EXECUTE	 msdb.dbo.sp_send_dbmail
--		 @profile_name = N'Local SMTP Virtual Server',
--		 @recipients = @UsersEmail,
--		 @subject = @Subject,
--		 @body = @Body,
--		 @query_result_no_padding = 1;
		 

--EXEC(@CreateLogin);
--EXEC(@AssignLogin);
--EXEC(@GrantRole);
--EXEC(@AssociateXMLSchemaCollection); --IF NULL, it won't create a query, which is good--
--EXEC(@GrantExecSPAcccess);