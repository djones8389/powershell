	( 	
		
	SELECT @GrantRole += CHAR(13) 
	
	+ 'use [' + A.name + ']; ' 	+
	
	Case When @AccessRequired = 'Read' 
		then 'CREATE ROLE '  + @ReadRole +  ' AUTHORIZATION [' + SchemaName +'];' 
				
				+ ' GRANT SELECT, VIEW DEFINITION ON SCHEMA::' + SchemaName + ' TO ' + @ReadRole +';' 				
		
				+ ' EXEC sp_addrolemember '''  + @ReadRole + ''', ''' + @Username + '''; '
				
		When @AccessRequired = 'Write' 
		 then 'CREATE ROLE '  + @WriteRole +  ' AUTHORIZATION [' + SchemaName +'];' 
	
				+ ' GRANT ALTER, DELETE, EXECUTE, INSERT, SELECT, UPDATE, VIEW DEFINITION ON SCHEMA::' + SchemaName + ' TO ' + @WriteRole +';' 		   
						
				+ ' EXEC sp_addrolemember ''' +	 @WriteRole + ''', ''' + @Username + '''; '
									   
		End

	from sys.databases A
	INNER join @@BigTable  B
	on B.Schema_DatabaseName COLLATE Latin1_General_CI_AS = a.name

	where B.Schema_DatabaseName COLLATE Latin1_General_CI_AS  = @DatabaseName
			
	)