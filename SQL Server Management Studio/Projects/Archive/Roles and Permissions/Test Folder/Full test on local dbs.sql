CREATE PROCEDURE ##GenPass(@Len INT = 32, @Password NVARCHAR(MAX) OUTPUT)
AS
BEGIN
SET @Password = (
SELECT XChar AS [text()]
FROM  (
              SELECT TOP(@Len) XChar
              FROM  (
                     VALUES  ('A'),('B'),('C'),('D'),('E'),('F'),('G'),('H'),('I'),('J'),('K'),('L'),('M'),('N'),('O'),('P'),('Q'),('R'),('S'),('T'),('U'),('V'),('W'),('X'),('Y'),('Z'),
                                  ('a'),('b'),('c'),('d'),('e'),('f'),('g'),('h'),('i'),('j'),('k'),('l'),('m'),('n'),('o'),('p'),('q'),('r'),('s'),('t'),('u'),('v'),('w'),('x'),('y'),('z'),
                                  ('0'),('1'),('2'),('3'),('4'),('5'),('6'),('7'),('8'),('9'),
                                  ('!'),('�'),('$'),('%'),('^'),('&'),('*'),('('),(')'),('-'),('_'),('='),('+'),('@'),('~'),('#'),('?')
                           ) AS XChars(XChar)
              ORDER BY ABS(CHECKSUM(NEWID()))
              ) AS [XChars]
FOR XML PATH('')
)
END;
GO

DECLARE @Username nvarchar(MAX) = 'HelloThere'
DECLARE @DatabaseName nvarchar(MAX) = 'AQA_SALocal'

/*Create a new Login*/

DECLARE	@return_value int,
		@Password nvarchar(max)


EXEC	##GenPass @Len = 32, @Password = @Password OUTPUT


DROP PROCEDURE ##GenPass

DECLARE @CreateLogin nvarchar(MAX) = ''

SELECT @CreateLogin += CHAR(13) + 'EXEC sp_addlogin ' + '''' +  @Username + '''' + ',' + '''' + @Password +  '''' +  ';'

/*Assign the Login to all DB's*/

DECLARE @AssignLogin nvarchar(MAX)= ''

SELECT @AssignLogin += CHAR(13) + 'use [' + name + ']; ' + 'CREATE USER ' + @Username + ' FOR LOGIN [' + @Username + ']; '
from sys.databases
where name not in ('master', 'msdb', 'tempdb', 'model')
	and state_desc = 'ONLINE'
order by name;


/*Grant datareader to the new user to all DB's*/

DECLARE @GrantDataReader nvarchar(MAX)= ''

SELECT @GrantDataReader += CHAR(13) + 'use [' + name + ']; ' + 'EXEC sp_addrolemember ''db_datareader'', ''' + @Username + '''; '
from sys.databases
where name not in ('master', 'msdb', 'tempdb', 'model')
	and state_desc = 'ONLINE'
order by name;


/*Assign Schemas to users*/

DECLARE @AssignSchema nvarchar(MAX) = ''

IF OBJECT_ID('tempdb..##AssignSchema') IS NOT NULL DROP TABLE tempdb..##AssignSchema

CREATE TABLE  ##AssignSchema  (

	  DatabaseName nvarchar(MAX)
	, SchemaName nvarchar(50) 

)

exec sp_MSforeachdb '

use [?];

insert into ##AssignSchema(DatabaseName, SchemaName)
select ''?''
	, Name 
from sys.schemas 
where 
	''?'' not in (''msdb'',''master'', ''SurpassReportServer'', ''SurpassReportServerTempDB'')
and name not in 
	(select name 
		from sysusers 
			where issqlrole = 1
			)
and name not in (''dbo'',''guest'',''INFORMATION_SCHEMA'',''sys'')

'  

SELECT @AssignSchema += CHAR(13) + 'use ' + DatabaseName + '; GRANT EXECUTE, SELECT, VIEW DEFINITION ON SCHEMA::' + SchemaName + ' to [' + @UserName + '] '
from ##AssignSchema



/*Grant Access to XML Schema Collections*/

IF OBJECT_ID('tempdb..##SchemaEntries') IS NOT NULL DROP TABLE tempdb..##SchemaEntries

CREATE TABLE ##SchemaEntries (

	 DatabaseName nvarchar(75)
   , SchemaName nvarchar(75)
)


EXEC sp_MSforeachdb '

	use [?];
	
	insert ##SchemaEntries
	select ''?'', name 	
	from sys.xml_schema_collections
	
	where ''?'' not in (''master'', ''msdb'', ''tempdb'', ''model'')
	and name != ''sys''
	'

DECLARE @AssignSchemaCollections nvarchar(MAX)= ''

SELECT @AssignSchemaCollections += CHAR(13) + N'use [' + DatabaseName + '];' + 'GRANT EXECUTE ON XML SCHEMA COLLECTION::' + ##SchemaEntries.SchemaName + N' TO '+@Username+';'
			from ##SchemaEntries
DROP TABLE ##SchemaEntries;


/*Grant Execute access to all SP's*/

IF OBJECT_ID('tempdb..##DBsandSPs') IS NOT NULL DROP TABLE tempdb..##DBsandSPs
 
 
CREATE TABLE ##DBsandSPs (
	
	DatabaseName nvarchar(MAX)
	, SPName nvarchar(MAX) 

)


exec sp_MSforeachdb '

	use [?];

   insert into ##DBsandSPs(DatabaseName, SPName)
   select ''?'', SPECIFIC_NAME 
   from information_schema.routines 
   
   inner join sys.databases
   on sys.databases.name collate Latin1_General_CI_AS = information_schema.routines.SPECIFIC_CATALOG
  
   where routine_type = ''PROCEDURE''
	 and SPECIFIC_CATALOG NOT IN (''master'', ''msdb'', ''SurpassReportServer'', ''SurpassReportServerTempDB'')
		and state_desc = ''ONLINE''
'	

DECLARE @GrantExecSPAccess varchar(MAX) = ''


SELECT @GrantExecSPAccess += CHAR(13) + 'use [' + DatabaseName + ']; ' + 'GRANT EXECUTE ON [' + SPName + N'] TO '+@Username+';'
from ##DBsandSPs

DROP TABLE ##DBsandSPs;

PRINT (@CreateLogin);
PRINT (@AssignLogin);
PRINT (@GrantDataReader);
PRINT (@AssignSchema);
PRINT (@AssignSchemaCollections);
PRINT (@GrantExecSPAccess);

--EXEC (@CreateLogin);
--EXEC (@AssignLogin);
--EXEC (@GrantDataReader);
--EXEC (@AssignSchema);
--EXEC (@AssignSchemaCollections);
--EXEC (@GrantExecSPAccess);