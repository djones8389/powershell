DECLARE @Username nvarchar(100) = 'KevinH'
DECLARE @DatabaseName nvarchar(100) = 'AdventureWorks2008R2'				--Leave Blank for ALL		'AQA_SALocal'
DECLARE @AccessRequired nvarchar(100) = 'Read'			
DECLARE @ReadRole nvarchar(100) = '[btl_developer_access]'
DECLARE @WriteRole nvarchar(100) = '[btl_developer_full_access]'
DECLARE @GrantRole nvarchar(MAX)= ''
DECLARE @AssignLogin nvarchar(MAX)= ''
DECLARE @AssociateRoleAccessAndSchema nvarchar(MAX) = ''
DECLARE @AssociateXMLSchemaCollection nvarchar(MAX) = ''

/*Write DB names and DB schemas into a Table Variable*/

DECLARE @@AssignSchema TABLE (DatabaseName nvarchar(MAX), SchemaName nvarchar(50))

INSERT @@AssignSchema
exec sp_MSforeachdb '

use [?];

select ''?''
	, Name 
from sys.schemas 
where 
	''?'' not in (''msdb'',''master'', ''SurpassReportServer'', ''SurpassReportServerTempDB'')
and name not in 
	(select name 
		from sysusers 
			where issqlrole = 1
			)
and name not in (''dbo'',''guest'',''INFORMATION_SCHEMA'',''sys'')
and ''?'' in (select name from sys.databases where state_desc = ''ONLINE'')

'  

/*Write DB names and DB XMLSchemaCollections into a Table Variable*/

DECLARE @@AssignXMLSchemaCollection TABLE (DatabaseName nvarchar(MAX), XMLSchemaCollection nvarchar(50))

INSERT @@AssignXMLSchemaCollection
EXEC sp_MSforeachdb '

	use [?];
	
	select ''?'', name 	
	from sys.xml_schema_collections
	where ''?'' not in (''master'', ''msdb'', ''tempdb'', ''model'')
		and ''?'' in (select name from sys.databases where state_desc = ''ONLINE'')
	
	and name != ''sys''
	'

SELECT * FROM @@AssignSchema
SELECT * FROM @@AssignXMLSchemaCollection

/*1)  Assign the Login to DB(s) required +  2)  Grant Role to the new user to DB(s) required*/



IF LEN(@DatabaseName) > 1
BEGIN							
	(
	SELECT @AssignLogin += CHAR(13) + 'use [' + name + ']; ' + 'CREATE USER ' + @Username + ' FOR LOGIN [' + @Username + ']; '
	from sys.databases
	where name  = @DatabaseName
		and state_desc = 'ONLINE'
		
	)
	 
	( 	
		
	SELECT @GrantRole += CHAR(13) + 'use [' + name + ']; ' 
	
	+
	
	Case When @AccessRequired = 'Read' then 'CREATE ROLE '  + @ReadRole +  ' AUTHORIZATION [dbo]' 
									   Else 'CREATE ROLE '  + @WriteRole +  ' AUTHORIZATION [dbo]' 
		End
	
	+ ' EXEC sp_addrolemember ''' +
	
	Case When @AccessRequired = 'Read' then  @ReadRole 
									   Else  @WriteRole  
		End
	
	 +  ''', ''' + @Username + '''; '
	from sys.databases
	where name = @DatabaseName
		and state_desc = 'ONLINE'
	
	)
	
	(
		

	SELECT @AssociateRoleAccessAndSchema +=CHAR(13) + 'use [' + name +']; GRANT EXECUTE, SELECT, VIEW DEFINITION ON SCHEMA::' + SchemaName + ' To ' + +

		Case When @AccessRequired = 'Read' then @ReadRole  
								   Else  @WriteRole 
		End 
	from sys.databases
	inner join @@AssignSchema A
	on A.DatabaseName collate Latin1_General_CI_AS = sys.databases.name
	
	where name = @DatabaseName
		and state_desc = 'ONLINE'
	
	)
	

	
END
ELSE
BEGIN
	(
	SELECT @AssignLogin += CHAR(13) + 'use [' + name + ']; ' + 'CREATE USER ' + @Username + ' FOR LOGIN [' + @Username + ']; '
	from sys.databases
	where name not in ('master', 'msdb', 'tempdb', 'model')
		and state_desc = 'ONLINE'
		
	)
		
	(	
   SELECT @GrantRole += CHAR(13) + 'use [' + name + ']; ' 
	
	+
	
	Case When @AccessRequired = 'Read' then 'CREATE ROLE '  + @ReadRole +  ' AUTHORIZATION [dbo]' 
									   Else 'CREATE ROLE '  + @WriteRole +  ' AUTHORIZATION [dbo]' 
		End
	
	+ ' EXEC sp_addrolemember '''  + 
	
	Case When @AccessRequired = 'Read' then  @ReadRole 
									   Else  @WriteRole  
		End
	
	 +  ''', ''' + @Username + '''; '
	from sys.databases
	where name not in ('master', 'msdb', 'tempdb', 'model')
		and state_desc = 'ONLINE'		
		
	--order by name;
	)
	
	(
		

	SELECT @AssociateRoleAccessAndSchema +=CHAR(13) + 'use [' + name +']; GRANT EXECUTE, SELECT, VIEW DEFINITION ON SCHEMA::' + SchemaName + ' To ' + +

		Case When @AccessRequired = 'Read' then @ReadRole  
								   Else  @WriteRole 
		End 
	from sys.databases
	inner join @@AssignSchema A
	on A.DatabaseName collate Latin1_General_CI_AS = sys.databases.name
	
	where name not in ('master', 'msdb', 'tempdb', 'model')
	and state_desc = 'ONLINE'
	
	)
	
	
END;
PRINT(@AssignLogin)
PRINT(@GrantRole)	
PRINT(@AssociateRoleAccessAndSchema)