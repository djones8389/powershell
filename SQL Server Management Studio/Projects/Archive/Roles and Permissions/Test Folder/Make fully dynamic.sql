DECLARE @DatabaseName nvarchar(100) = ''				--Leave Blank for ALL,  or specify one DB
DECLARE @Username nvarchar(100) = 'Jonesy'					--Enter username in here
DECLARE @AccessRequired nvarchar(100) = 'Read'				--NEEDS to be read or write, or it won't work 	
DECLARE @ReadRole nvarchar(100) = '[btl_developer_access]'
DECLARE @WriteRole nvarchar(100) = '[btl_developer_full_access]'
DECLARE @AssignLogin nvarchar(MAX)= ''
DECLARE @GrantRole nvarchar(MAX)= ''
DECLARE @GrantExecSPAcccess nvarchar(MAX) = ''
DECLARE @AssociateXMLSchemaCollection nvarchar(MAX) = ''

DECLARE @@BigTable TABLE (
		Schema_DatabaseName nvarchar(MAX)
	  , SchemaName nvarchar(50)
	  , XMLSchemaCollection_DatabaseName nvarchar(MAX)
	  , XMLSchemaName nvarchar(50)

)

DECLARE @SPsforDBs TABLE (
		DatabaseName nvarchar(MAX)
	  , SPName nvarchar (MAX)
)

/*Write DB names and DB Schema into a Table Variable*/

INSERT @@BigTable(Schema_DatabaseName, SchemaName)
exec sp_MSforeachdb '

use [?];

select ''?''
	, Name 
from sys.schemas 
where 
	''?'' not in (''msdb'',''master'', ''SurpassReportServer'', ''SurpassReportServerTempDB'')
and name not in 
	(select name 
		from sysusers 
			where issqlrole = 1
			)
and name not in (''guest'',''INFORMATION_SCHEMA'',''sys'')
		and ''?'' in (select name from sys.databases where state_desc = ''ONLINE'')
'  

/*Write DB names and DB XMLSchemaCollections into a Table Variable*/

INSERT @@BigTable(XMLSchemaCollection_DatabaseName, XMLSchemaName)
EXEC sp_MSforeachdb '

	use [?];
	
	select ''?'', name 	
	from sys.xml_schema_collections
	
	where ''?'' not in (''master'', ''msdb'', ''tempdb'', ''model'')
	and name != ''sys''


'

/*Write DB names and DB SP's into a Table Variable*/

INSERT @SPsforDBs(DatabaseName, SPName)
EXEC sp_MSforeachdb '

	use [?];

   select ''?'', SPECIFIC_NAME 
   from information_schema.routines 
   
   inner join sys.databases
   on sys.databases.name collate Latin1_General_CI_AS = information_schema.routines.SPECIFIC_CATALOG
  
   where routine_type = ''PROCEDURE''
	 and SPECIFIC_CATALOG NOT IN (''master'', ''msdb'', ''SurpassReportServer'', ''SurpassReportServerTempDB'')
		and state_desc = ''ONLINE''
	'	

IF LEN(@DatabaseName) > 1
BEGIN							
	(
	SELECT @AssignLogin += CHAR(13) + 'use [' + name + ']; ' + 'CREATE USER ' + @Username + ' FOR LOGIN [' + @Username + ']; '
	from sys.databases
	where name  = @DatabaseName
		
	)
	 
	( 	
		
	SELECT @GrantRole += CHAR(13) 
	
	+ 'use [' + A.name + ']; ' 	+
	
	Case When @AccessRequired = 'Read' 
		then 'CREATE ROLE '  + @ReadRole +  ' AUTHORIZATION [' + SchemaName +'];' 
				
				+ ' GRANT EXECUTE, SELECT, VIEW DEFINITION ON SCHEMA::' + SchemaName + ' TO ' + @ReadRole +';' 				
		
				+ ' EXEC sp_addrolemember '''  + @ReadRole + ''', ''' + @Username + '''; '
				
		When @AccessRequired = 'Write' 
		 then 'CREATE ROLE '  + @WriteRole +  ' AUTHORIZATION [' + SchemaName +'];' 
	
				+ ' GRANT ALTER, DELETE, EXECUTE, INSERT, SELECT, UPDATE, VIEW DEFINITION ON SCHEMA::' + SchemaName + ' TO ' + @WriteRole +';' 		   
						
				+ ' EXEC sp_addrolemember ''' +	 @WriteRole + ''', ''' + @Username + '''; '
									   
		End

	from sys.databases A
	INNER join @@BigTable  B
	on B.Schema_DatabaseName COLLATE Latin1_General_CI_AS = a.name

	where B.Schema_DatabaseName COLLATE Latin1_General_CI_AS  = @DatabaseName
			
	)
	
	(
	
	SELECT @AssociateXMLSchemaCollection +=CHAR(13) + 'use ' + A.name + ';'	+ 'GRANT EXECUTE ON XML SCHEMA COLLECTION::' + B.XMLSchemaName + ' To ' + @Username + ';'
		
	from sys.databases A
	INNER join @@BigTable  B
	on B.XMLSchemaCollection_DatabaseName COLLATE Latin1_General_CI_AS = A.name
	
	where B.XMLSchemaCollection_DatabaseName COLLATE Latin1_General_CI_AS  = @DatabaseName
	
	)
	
	(
	
	SELECT @GrantExecSPAcccess +=CHAR(13) + 'use ' + name + ';'	+ 'GRANT EXECUTE ON [' + SPName + N'] TO '+@Username+';'
	from sys.databases A
	INNER join @SPsforDBs B
	on B.DatabaseName = A.name
	
	where name COLLATE Latin1_General_CI_AS  = @DatabaseName
	
	)
	
END
ELSE IF LEN(@DatabaseName) !> 1

BEGIN							
	(
	SELECT @AssignLogin += CHAR(13) + 'use [' + name + ']; ' + 'CREATE USER ' + @Username + ' FOR LOGIN [' + @Username + ']; '
	from sys.databases
	where name not in ('master', 'msdb', 'tempdb', 'model')
		and state_desc = 'ONLINE'
		
		
	)
	 
	( 	
		
	SELECT @GrantRole += CHAR(13) 
	
	+ 'use [' + A.name + ']; ' 	+
	
	Case When @AccessRequired = 'Read' 
		then 'CREATE ROLE '  + @ReadRole +  ' AUTHORIZATION [' + SchemaName +'];' 
				
				+ ' GRANT EXECUTE, SELECT, VIEW DEFINITION ON SCHEMA::' + SchemaName + ' TO ' + @ReadRole +';' 				
		
				+ ' EXEC sp_addrolemember '''  + @ReadRole + ''', ''' + @Username + '''; '
				
		When @AccessRequired = 'Write' 
		 then 'CREATE ROLE '  + @WriteRole +  ' AUTHORIZATION [' + SchemaName +'];' 
	
				+ ' GRANT ALTER, DELETE, EXECUTE, INSERT, SELECT, UPDATE, VIEW DEFINITION ON SCHEMA::' + SchemaName + ' TO ' + @WriteRole +';' 		   
						
				+ ' EXEC sp_addrolemember ''' +	 @WriteRole + ''', ''' + @Username + '''; '
									   
		End

	from sys.databases A
	INNER join @@BigTable  B
	on B.Schema_DatabaseName COLLATE Latin1_General_CI_AS = a.name

	where name not in ('master', 'msdb', 'tempdb', 'model')
		and state_desc = 'ONLINE'	
	)
	
	(
	
	SELECT @AssociateXMLSchemaCollection +=CHAR(13) + 'use ' + A.name + ';'	+ 'GRANT EXECUTE ON XML SCHEMA COLLECTION::' + B.XMLSchemaName + ' To ' + @Username + ';'
		
	from sys.databases A
	INNER join @@BigTable  B
	on B.XMLSchemaCollection_DatabaseName COLLATE Latin1_General_CI_AS = A.name
	
	where name not in ('master', 'msdb', 'tempdb', 'model')
		and state_desc = 'ONLINE'
	)
	
	(
	
	SELECT @GrantExecSPAcccess +=CHAR(13) + 'use ' + name + ';'	+ 'GRANT EXECUTE ON [' + SPName + N'] TO '+@Username+';'
	from sys.databases A
	INNER join @SPsforDBs B
	on B.DatabaseName = A.name
	
	where name not in ('master', 'msdb', 'tempdb', 'model')
		and state_desc = 'ONLINE'
	)
	
END


PRINT(@AssignLogin);
PRINT(@GrantRole);
PRINT(@AssociateXMLSchemaCollection); --IF NULL, it won't create a query, which is good--
PRINT(@GrantExecSPAcccess);


EXEC(@AssignLogin);
EXEC(@GrantRole);
EXEC(@AssociateXMLSchemaCollection); --IF NULL, it won't create a query, which is good--
EXEC(@GrantExecSPAcccess);