IF OBJECT_ID('tempdb..#DatabaseRoleMemberShip') IS NOT NULL DROP TABLE tempdb..#DatabaseRoleMemberShip;

SET NOCOUNT ON

DECLARE @AddandDrop nvarchar(max) = '';

CREATE TABLE #DatabaseRoleMemberShip (
	Username NVARCHAR(100)
	,Rolename NVARCHAR(100)
	,Databasename NVARCHAR(100)
)


EXEC sp_MSforeachdb  '
USE [?];

insert into #DatabaseRoleMemberShip
select u.name 
		,r.name 
		,''?'' 
from sys.database_role_members RM 
	inner join sys.database_principals U on U.principal_id = RM.member_principal_id
	inner join sys.database_principals R on R.principal_id = RM.role_principal_id
where u.type<>''R''
	and u.name != ''dbo''
	and u.name not like ''%itembank%'' and u.name not like ''%secureassess%'' and u.name not like ''%content%''
	and u.name not in (''cpuser_aat_r11'',''cpuser_ncfe_r11'',''ReportingUserSQA'', ''ReportingUserOCR'',''securemarker_aat_11'',''UAT_BC_eFlex_User'',''UAT_BC_EPCAdaptor_User'',''UAT_BC_LocalScan_User'',''UAT_BC_PhoneAssess_User'',''UAT_BC_SecureMarker_User'',''UAT_BC_SurpassDataWarehouse_Login'',''UAT_BC_SurpassDataWarehouse_User'',''UAT_BC_TestPackageManager_User'',''UAT_OCR_DataManagement_User'',''UAT_OCR_OpenAssess_User'',''UAT_OCR_SecureMarker_User'',''UAT_OCR_SurpassDataWarehouse_User'', ''UAT_SQA_OpenAssess_Login'', ''UAT_SQA_SurpassDataWarehouse_User'')
	--and r.name not IN(''RSExecRole'',''db_datareader'')
	and ''?'' not IN (''msdb'', ''master'', ''tempdb'')
	
'


Select * from #DatabaseRoleMemberShip order by 1

DECLARE Users Cursor for
select
	username
	, rolename
	, databasename
from #DatabaseRoleMemberShip


DECLARE @Username nvarchar(100), @RoleName nvarchar(100), @DatabaseName nvarchar(100);

OPEN Users;

FETCH NEXT FROM Users INTO @Username, @RoleName, @DatabaseName

WHILE @@FETCH_STATUS = 0

BEGIN

SELECT @AddandDrop += CHAR(13)+  'use ' + @DatabaseName +'; ' + 'EXEC sp_addrolemember ''db_datareader''' + ',' + '''' + @Username + '''' + '; '  
															  + 'EXEC sp_droprolemember ''' + @Rolename +'''' + ',' + '''' + @Username + '''' + ';'

FETCH NEXT FROM Users INTO @Username, @RoleName, @DatabaseName

END
CLOSE Users;
DEALLOCATE Users;

PRINT (@AddandDrop);
--EXEC (@AddandDrop);

DROP TABLE #DatabaseRoleMemberShip;
