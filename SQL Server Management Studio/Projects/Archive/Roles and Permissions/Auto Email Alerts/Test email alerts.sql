DECLARE @Subject nvarchar(30) = 'Your Staging Credentials';
DECLARE @Username nvarchar(20) = 'davej';
DECLARE @Password nvarchar(50) = 'password';
DECLARE @Body nvarchar(MAX);
DECLARE @UsersEmail nvarchar(50) = 'dave.jones@btl.com'

SET @Body = 'Server=94.236.6.119; Database=' + 'DBNAME; User ID=' + @Username + ';Password=' + @Password +';'

EXECUTE	 msdb.dbo.sp_send_dbmail
		 @profile_name = N'Local SMTP Virtual Server',
		 @recipients = @UsersEmail,
		 @subject = @Subject,
		 @body = @Body,
		 @query_result_no_padding = 1;
		 
