--DROP PROCEDURE ##GenPass

CREATE PROCEDURE ##GenPass(@Len INT = 32, @Password NVARCHAR(MAX) OUTPUT)
AS
BEGIN
SET @Password = (
SELECT XChar AS [text()]
FROM  (
              SELECT TOP(@Len) XChar
              FROM  (
                     VALUES  ('A'),('B'),('C'),('D'),('E'),('F'),('G'),('H'),('I'),('J'),('K'),('L'),('M'),('N'),('O'),('P'),('Q'),('R'),('S'),('T'),('U'),('V'),('W'),('X'),('Y'),('Z'),
                                  ('a'),('b'),('c'),('d'),('e'),('f'),('g'),('h'),('i'),('j'),('k'),('l'),('m'),('n'),('o'),('p'),('q'),('r'),('s'),('t'),('u'),('v'),('w'),('x'),('y'),('z'),
                                  ('0'),('1'),('2'),('3'),('4'),('5'),('6'),('7'),('8'),('9'),
                                  ('!'),('�'),('$'),('%'),('^'),('&'),('*'),('('),(')'),('-'),('_'),('='),('+'),('@'),('~'),('#'),('?')
                           ) AS XChars(XChar)
              ORDER BY ABS(CHECKSUM(NEWID()))
              ) AS [XChars]
FOR XML PATH('')
)
END;
GO



DECLARE @DatabaseName nvarchar(100) = 'AAT_SurpassDataWarehouse'	    --Leave Blank for ALL,  or specify one DB
DECLARE @Username nvarchar(100) = 'StuartTheGreat'						--Enter username in here
DECLARE @AccessRequired nvarchar(100) = 'Read'							--NEEDS to be read or write, or it won't work 	

--Email them the credentials--
DECLARE @UsersEmail nvarchar(50) = 'dave.jones@btl.com'
DECLARE @Subject nvarchar(30) = 'Your Staging Credentials';
DECLARE @Body nvarchar(MAX);

DECLARE @ReadRole nvarchar(100) = 'btl_developer_access'
DECLARE @WriteRole nvarchar(100) = 'btl_developer_full_access'
DECLARE @AssignLogin nvarchar(MAX)= ''
DECLARE @GrantRole nvarchar(MAX)= ''
DECLARE @GrantExecSPAcccess nvarchar(MAX) = ''
DECLARE @AssociateXMLSchemaCollection nvarchar(MAX) = ''


DECLARE	@return_value int,
		@Password nvarchar(max)


EXEC	##GenPass @Len = 32, @Password = @Password OUTPUT

DROP PROCEDURE ##GenPass

/*Create a new Login*/

DECLARE @CreateLogin nvarchar(MAX) = ''

SELECT @CreateLogin += CHAR(13) + 'EXEC sp_addlogin ' + '''' +  @Username + '''' + ',' + '''' + @Password +  '''' +  ';'

EXEC(@CreateLogin)


SET @Body = 'Server=94.236.6.119; Database=' + @DatabaseName + '; User ID=' + @Username + ';Password=' + @Password +';'


EXECUTE	 msdb.dbo.sp_send_dbmail
		 @profile_name = N'Local SMTP Virtual Server',
		 @recipients = @UsersEmail,
		 @subject = @Subject,
		 @body = @Body,
		 @query_result_no_padding = 1;
		 

		 
