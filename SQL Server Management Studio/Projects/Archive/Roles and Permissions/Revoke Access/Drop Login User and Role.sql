SET NOCOUNT ON;

DECLARE @Username nvarchar(MAX) = 'TomG'




DECLARE @DropLogin nvarchar(MAX)= 'DROP LOGIN ' + @Username
DECLARE @DropUser nvarchar(MAX)= ''
DECLARE @DropRoles nvarchar(max) = ''

--Find User in ALL associated DB's

DECLARE @FindUSERNAME TABLE (Username nvarchar(100), RoleName nvarchar(100), DBName nvarchar(max)) 
INSERT @FindUSERNAME
EXEC sp_MSforeachdb  '

USE [?];

select u.name 
		,r.name 
		,''?'' 
from sys.database_role_members RM 
	right join sys.database_principals U on U.principal_id = RM.member_principal_id
	left join sys.database_principals R on R.principal_id = RM.role_principal_id
where u.type<>''R''
	--and u.name = @@Username
	
'

--Drop User from ALL associated DB's


SELECT @DropUser += CHAR(13) + 'use [' + name + ']; ' + 'DROP USER ' + @Username + '; '
from sys.databases
inner join @FindUSERNAME U
on U.DBName COLLATE Latin1_General_CI_AS = sys.databases.name
where name not in ('master', 'msdb', 'tempdb', 'model')
	and state_desc = 'ONLINE'
	and U.Username COLLATE Latin1_General_CI_AS = @Username
order by name;


--After dropping the user, see if the role can be deleted too


DECLARE @FindRoles TABLE (DBName nvarchar(max), RoleName nvarchar(max))
INSERT @FindRoles(DBName, RoleName)
EXEC sp_MSforeachdb  '
USE [?];

SELECT  ''?'', roleprinc.[name]
FROM sys.database_role_members members

RIGHT JOIN sys.database_principals roleprinc
ON roleprinc.[principal_id] = members.[role_principal_id]

where is_fixed_role  = 0
	and type = ''R''
	and name != ''public''
	and name in (''btl_developer_access'',''btl_developer_full_access'')
	and roleprinc.[name] not in (
		
		--Find user Associations

		SELECT  roleprinc.[name]
		FROM sys.database_role_members members

		INNER JOIN sys.database_principals roleprinc
		ON roleprinc.[principal_id] = members.[role_principal_id]

		where is_fixed_role  = 0

	)
'

SELECT @DropRoles +=CHAR(13) + 'use [' + DBName + ']; ' + 'DROP ROLE ' + RoleName
from @FindRoles


if (SELECT name from sys.sql_logins where name = @Username) IS NULL

BEGIN
	Print 'Login didn''t need to be dropped'
END

ELSE
BEGIN
	PRINT(@DropLogin);
	--EXEC(@DropLogin);
END


IF LEN(@DropUSER) < 1
BEGIN
	PRINT 'No Users needed to be dropped from any DB';
END
ELSE
BEGIN
	PRINT (@DropUser);
	--EXEC  (@DropUser);
END


IF LEN(@DropRoles) < 1
BEGIN
	PRINT 'No Roles needed to be dropped from any DB';
END
ELSE
BEGIN
	PRINT(@DropRoles)
	--EXEC(@DropRoles)
END



