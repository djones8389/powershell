CREATE PROCEDURE ##GenPass(@Len INT = 32, @Password NVARCHAR(MAX) OUTPUT)
AS
BEGIN
SET @Password = (
SELECT XChar AS [text()]
FROM  (
              SELECT TOP(@Len) XChar
              FROM  (
                     VALUES  ('A'),('B'),('C'),('D'),('E'),('F'),('G'),('H'),('I'),('J'),('K'),('L'),('M'),('N'),('O'),('P'),('Q'),('R'),('S'),('T'),('U'),('V'),('W'),('X'),('Y'),('Z'),
                                  ('a'),('b'),('c'),('d'),('e'),('f'),('g'),('h'),('i'),('j'),('k'),('l'),('m'),('n'),('o'),('p'),('q'),('r'),('s'),('t'),('u'),('v'),('w'),('x'),('y'),('z'),
                                  ('0'),('1'),('2'),('3'),('4'),('5'),('6'),('7'),('8'),('9'),
                                  ('!'),('�'),('$'),('%'),('^'),('*'),('('),(')'),('-'),('_'),('='),('+'),('@'),('~'),('#'),('?')
                           ) AS XChars(XChar)
              ORDER BY ABS(CHECKSUM(NEWID()))
              ) AS [XChars]
FOR XML PATH('')
)
END;
GO



DECLARE @DatabaseName nvarchar(100) = 'UAT_BC2_SecureAssess'			 --Leave Blank for ALL,  or specify one DB
DECLARE @Username nvarchar(100) = 'robertb'							--Enter username in here
DECLARE @AccessRequired nvarchar(100) = 'read'						--NEEDS to be read or write, or it won't work 	

--Email them the credentials--
DECLARE @UsersEmail nvarchar(200) = 'dave.jones@btl.com; andy.brocklesby@btl.com; robert.barnes@btl.com'
DECLARE @Subject nvarchar(200) =  @AccessRequired + '-Access Staging Credentials to ' + @DatabaseName + ' - ' + @Username;
DECLARE @Body nvarchar(MAX);

DECLARE @ReadRole nvarchar(100) = 'btl_developer_access'
DECLARE @WriteRole nvarchar(100) = 'btl_developer_full_access'

DECLARE @CreateLogin nvarchar(MAX) = ''
DECLARE @AssignLogin nvarchar(MAX)= ''
DECLARE @AssignRolePermissions nvarchar(MAX) = '' 
DECLARE @CreateAndGrantRole nvarchar(MAX)= ''
--DECLARE @GrantExecSPAcccess nvarchar(MAX) = ''
DECLARE @AssociateXMLSchemaCollection nvarchar(MAX) = ''
DECLARE @ConnectionString nvarchar(MAX) = '' 




DECLARE	@return_value int,
		@Password nvarchar(max)


EXEC	##GenPass @Len = 32, @Password = @Password OUTPUT

DROP PROCEDURE ##GenPass

/*Create a new Login*/


SELECT @CreateLogin += CHAR(13) + 'EXEC sp_addlogin ' + '''' +  @Username + '''' + ',' + '''' + @Password +  '''' +  ';'


DECLARE @@SchemaTable TABLE (
		Schema_DatabaseName nvarchar(MAX)
	  , SchemaName nvarchar(50)
)

DECLARE @@XMLSchemaCollection TABLE (
		 XMLSchemaCollection_DatabaseName nvarchar(MAX)
	  ,  XMLSchemaCollectionName nvarchar(MAX)
	  ,  SchemaName nvarchar(MAX)
)

DECLARE @@RoleTable TABLE (
	   Role_DatabaseName nvarchar(MAX)
	  , RoleName nvarchar(50)
)
/*
DECLARE @SPsforDBs TABLE (
		DatabaseName nvarchar(MAX)
	  , SchemaName  nvarchar(50)
	  , SPName nvarchar (MAX)
)
*/

/*Write DB names and DB Schema into a Table Variable*/

INSERT @@SchemaTable(Schema_DatabaseName, SchemaName)
exec sp_MSforeachdb '

use [?];

select ''?''
	, Name 
from sys.schemas 
where 
	''?'' not in (''msdb'',''master'', ''SurpassReportServer'', ''SurpassReportServerTempDB'')
and name not in 
	(select name 
		from sysusers 
			where issqlrole = 1
			)
and name not in (''guest'',''INFORMATION_SCHEMA'',''sys'')
		and ''?'' in (select name from sys.databases where state_desc = ''ONLINE'')
'  

/*Write DB names and DB XMLSchemaCollections into a Table Variable*/

INSERT @@XMLSchemaCollection(XMLSchemaCollection_DatabaseName, XMLSchemaCollectionName, SchemaName)
EXEC sp_MSforeachdb '

	use [?];
	
	select ''?'', sys.xml_schema_collections.name, sys.schemas.name
	from sys.xml_schema_collections
	inner join sys.schemas
	on sys.schemas.schema_id = sys.xml_schema_collections.schema_id
	where ''?'' not in (''master'', ''msdb'', ''tempdb'', ''model'')
	and sys.xml_schema_collections.name != ''sys''

'


INSERT @@RoleTable(Role_DatabaseName, RoleName)
EXEC sp_MSforeachdb  '
USE [?];

--insert into #DatabaseRoleMemberShip
SELECT  ''?'', roleprinc.[name]
FROM sys.database_role_members members

RIGHT JOIN sys.database_principals roleprinc
ON roleprinc.[principal_id] = members.[role_principal_id]

where is_fixed_role  = 0
	and type = ''R''
	and name != ''public''
	and name in (''btl_developer_access'',''btl_developer_full_access'')
	--and roleprinc.[name] in (
		
	--	--Find user Associations

	--	SELECT  roleprinc.[name]
	--	FROM sys.database_role_members members

	--	INNER JOIN sys.database_principals roleprinc
	--	ON roleprinc.[principal_id] = members.[role_principal_id]

	--	where is_fixed_role  = 0

	--)
'

--SELECT * FROM @@RoleTable
--SELECT * FROM @@SchemaTable
--SELECT * FROM @@XMLSchemaCollection

/*Write DB names and DB SP's into a Table Variable*/

/*
INSERT @SPsforDBs(DatabaseName, SchemaName ,SPName)
EXEC sp_MSforeachdb '

	use [?];

   select ''?'', SPECIFIC_SCHEMA,SPECIFIC_NAME 
   from information_schema.routines 
   
   inner join sys.databases
   on sys.databases.name collate Latin1_General_CI_AS = information_schema.routines.SPECIFIC_CATALOG
  
   where routine_type = ''PROCEDURE''
	 and SPECIFIC_CATALOG NOT IN (''master'', ''msdb'', ''SurpassReportServer'', ''SurpassReportServerTempDB'')
		and state_desc = ''ONLINE''
	'	
*/


--SELECT * FROM @@BigTable



IF LEN(@DatabaseName) > 1
BEGIN							
	(
	SELECT @AssignLogin += CHAR(13) + 'use [' + name + ']; ' + 'CREATE USER ' + @Username + ' FOR LOGIN [' + @Username + ']; '
	from sys.databases
	where name  = @DatabaseName
		
	)

	(
	
		SELECT @AssignRolePermissions += CHAR(13) +
		
			Case When @AccessRequired = 'Read' and @ReadRole NOT IN (select C.RoleName from @@RoleTable where C.Role_DatabaseName = @DatabaseName)
			then
				'use [' + a.Schema_DatabaseName + ']; ' + 'GRANT SELECT, VIEW DEFINITION ON SCHEMA::' +  A.SchemaName + ' To ' + @ReadRole
			
			 When @AccessRequired = 'Write' and @WriteRole NOT IN (select C.RoleName from @@RoleTable where C.Role_DatabaseName = @DatabaseName)
			then
				'use [' + a.Schema_DatabaseName + ']; '  + 'GRANT ALTER, DELETE, EXECUTE, INSERT, SELECT, UPDATE, VIEW DEFINITION ON SCHEMA::' +  A.SchemaName + ' To ' + @WriteRole
			
			ELSE  ''
			
			END
			
			from @@SchemaTable a
			
			left join @@RoleTable c
			on C.Role_DatabaseName = a.Schema_DatabaseName
			
			where a.Schema_DatabaseName = @DatabaseName
	)

	( 	

	SELECT @CreateAndGrantRole += CHAR(13) +
	
	'use [' + a.name + ']; ' 		
	+
	Case When @AccessRequired = 'Read' and @ReadRole IN (select C.RoleName from @@RoleTable where C.Role_DatabaseName = @DatabaseName)
			then 			
			' EXEC sp_addrolemember '''  + @ReadRole + ''', ''' + @Username + '''; '
			
		When @AccessRequired = 'Read' and @ReadRole NOT IN (select C.RoleName from @@RoleTable where C.Role_DatabaseName = @DatabaseName)
			then
			'CREATE ROLE '  + @ReadRole +  ';'  		
			+			
			' EXEC sp_addrolemember '''  + @ReadRole + ''', ''' + @Username + '''; '
		
		When @AccessRequired = 'Write'  and @WriteRole IN (select C.RoleName from @@RoleTable where C.Role_DatabaseName = @DatabaseName)
			then 		 
			' EXEC sp_addrolemember ''' +	 @WriteRole + ''', ''' + @Username + '''; '
		
		 When @AccessRequired = 'Write'  and @WriteRole NOT IN (select C.RoleName from @@RoleTable where C.Role_DatabaseName = @DatabaseName)
			then			 
				'CREATE ROLE '  + @WriteRole +  ';'  
				+				
				' EXEC sp_addrolemember '''  + @WriteRole + ''', ''' + @Username + '''; '						   
			end
	from sys.databases A
	
	LEFT join @@RoleTable C
	on C.Role_DatabaseName COLLATE Latin1_General_CI_AS = A.name
		
	where  a.name COLLATE Latin1_General_CI_AS  = @DatabaseName
	)
	
	(
	
	SELECT @AssociateXMLSchemaCollection +=CHAR(13) + 
	
	'use ' + A.name + ';'	
	+ 
	Case When @AccessRequired = 'Read'
	then	
	'GRANT VIEW DEFINITION ON XML SCHEMA COLLECTION::' + B.SchemaName + '.' + B.XMLSchemaCollectionName + ' To ' + @Username + ';'
	When @AccessRequired = 'Write'
	then	
	'GRANT EXECUTE ON XML SCHEMA COLLECTION::' + B.SchemaName + '.' + B.XMLSchemaCollectionName + ' To ' + @Username + ';'
		end
	from sys.databases A
	INNER join @@XMLSchemaCollection  B
	on B.XMLSchemaCollection_DatabaseName COLLATE Latin1_General_CI_AS = A.name
	
	where B.XMLSchemaCollection_DatabaseName COLLATE Latin1_General_CI_AS  = @DatabaseName
	
	)
	
	
	(
		SELECT @ConnectionString +=CHAR(13) + 'Server=94.236.6.119; Database=' + name + '; User ID=' + @Username + '; Password=' + @Password +';' 
		from sys.databases
		where name not in ('master', 'msdb', 'tempdb', 'model')
		and state_desc = 'ONLINE'	
		and name = @DatabaseName
	)
	
	
--IF @AccessRequired = 'Write' 
--	(
	
--	SELECT @GrantExecSPAcccess +=CHAR(13) + 'use ' + name + ';'	+ 'GRANT EXECUTE ON [' + SchemaName + '].[' + SPName + '] TO '+@Username+';'
--	from sys.databases A
--	INNER join @SPsforDBs B
--	on B.DatabaseName = A.name
	
--	where name COLLATE Latin1_General_CI_AS  = @DatabaseName
	
--	)
	
END
ELSE IF LEN(@DatabaseName) !> 1

BEGIN							
	(
	SELECT @AssignLogin += CHAR(13) + 'use [' + name + ']; ' + 'CREATE USER ' + @Username + ' FOR LOGIN [' + @Username + ']; '
	from sys.databases
	where name not in ('master', 'msdb', 'tempdb', 'model')
		and state_desc = 'ONLINE'
			
	)
	 
	( 	
		
	SELECT @CreateAndGrantRole += CHAR(13) +	
	
	'use [' + A.name + ']; ' 	+	
	
   Case When @AccessRequired = 'Read' and @ReadRole IN (select C.RoleName from @@RoleTable where C.Role_DatabaseName = @DatabaseName)
			then 			
			' EXEC sp_addrolemember '''  + @ReadRole + ''', ''' + @Username + '''; '
			
		When @AccessRequired = 'Read' and @ReadRole NOT IN (select C.RoleName from @@RoleTable where C.Role_DatabaseName = @DatabaseName)
			then
			'CREATE ROLE '  + @ReadRole +  ' AUTHORIZATION [' + B.SchemaName +'];' + ' GRANT SELECT, VIEW DEFINITION ON SCHEMA::' + B.SchemaName + ' TO ' + @ReadRole +';' + ' EXEC sp_addrolemember '''  + @ReadRole + ''', ''' + @Username + '''; '

		When @AccessRequired = 'Write'  and @WriteRole IN (select C.RoleName from @@RoleTable where C.Role_DatabaseName = @DatabaseName)
		 	then	 
			' EXEC sp_addrolemember ''' +	 @WriteRole + ''', ''' + @Username + '''; '
		
		 When @AccessRequired = 'Write'  and @WriteRole NOT IN (select C.RoleName from @@RoleTable where C.Role_DatabaseName = @DatabaseName)
			then			 
			'CREATE ROLE '  + @WriteRole +  ' AUTHORIZATION [' + B.SchemaName +'];' + ' GRANT ALTER, DELETE, EXECUTE, INSERT, SELECT, UPDATE, VIEW DEFINITION ON SCHEMA::' +B.SchemaName + ' TO ' + @WriteRole +';' + ' EXEC sp_addrolemember ''' +	 @WriteRole + ''', ''' + @Username + '''; '
									   
	end

	from sys.databases A
	
	LEFT join @@SchemaTable  B
	on B.Schema_DatabaseName COLLATE Latin1_General_CI_AS = a.name
	LEFT join @@RoleTable C
	on C.Role_DatabaseName COLLATE Latin1_General_CI_AS = A.name
		
	where  a.name not in ('master', 'msdb', 'tempdb', 'model')
		and state_desc = 'ONLINE'	
	)
	
	(
	
	--SELECT @AssociateXMLSchemaCollection +=CHAR(13) + 'use ' + A.name + ';'	+ 'GRANT EXECUTE ON XML SCHEMA COLLECTION::' + B.SchemaName + '.' + B.XMLSchemaCollectionName + ' To ' + @Username + ';'
	SELECT @AssociateXMLSchemaCollection +=CHAR(13) + 
	
	'use ' + A.name + ';'	
	+ 
	Case When @AccessRequired = 'Read'
	then	
	'GRANT VIEW DEFINITION ON XML SCHEMA COLLECTION::' + B.SchemaName + '.' + B.XMLSchemaCollectionName + ' To ' + @Username + ';'
	When @AccessRequired = 'Write'
	then	
	'GRANT EXECUTE ON XML SCHEMA COLLECTION::' + B.SchemaName + '.' + B.XMLSchemaCollectionName + ' To ' + @Username + ';'
		end	
	from sys.databases A
	INNER join @@XMLSchemaCollection  B
	on B.XMLSchemaCollection_DatabaseName COLLATE Latin1_General_CI_AS = A.name
	
	where name not in ('master', 'msdb', 'tempdb', 'model')
		and state_desc = 'ONLINE'
	)
	
	
	(
		SELECT @ConnectionString +=CHAR(13) + 'Server=94.236.6.119; Database=' + name + '; User ID=' + @Username + '; Password=' + @Password +';' 
		from sys.databases
		where name not in ('master', 'msdb', 'tempdb', 'model')
		and state_desc = 'ONLINE'	
		
	)
	
	
--IF @AccessRequired = 'Write' 
--	(

--	SELECT @GrantExecSPAcccess +=CHAR(13) + 'use ' + name + ';'	+ 'GRANT EXECUTE ON [' + SchemaName + '].[' + SPName + '] TO '+@Username+';'
--	from sys.databases A
--	INNER join @SPsforDBs B
--	on B.DatabaseName = A.name
	
--	where name not in ('master', 'msdb', 'tempdb', 'model')
--		and state_desc = 'ONLINE'
--	)
	
END

PRINT(@ConnectionString);
PRINT(@CreateLogin);
PRINT(@AssignLogin);
PRINT(@CreateAndGrantRole);
PRINT(@AssignRolePermissions);
PRINT(@AssociateXMLSchemaCollection); --IF NULL, it won't create a query, which is good--
/*PRINT(@GrantExecSPAcccess);*/


SET @Body = 'Server=94.236.6.119; Database=' + @DatabaseName + '; User ID=' + @Username + ';Password=' + @Password +';'


EXECUTE	 msdb.dbo.sp_send_dbmail
		 @profile_name = N'Local SMTP Virtual Server',
		 @recipients = @UsersEmail,
		 @subject = @Subject,
		 @body = @Body,
		 @query_result_no_padding = 1;
		 

EXEC(@CreateLogin);
EXEC(@AssignLogin);
EXEC(@CreateAndGrantRole);
EXEC(@AssignRolePermissions);
EXEC(@AssociateXMLSchemaCollection); --IF NULL, it won't create a query, which is good--
/*EXEC(@GrantExecSPAcccess);*/