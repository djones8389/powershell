--Do not run on live--

DECLARE Metadata CURSOR 
FOR
--Select all metadata variations into a cursor--
select
   distinct d.a.value('@Name[1]', 'nvarchar(max)') as Metadata	
from ProjectListTable

cross apply ProjectDefaultXml.nodes('Defaults/StructureAttributes/Attribute') d(a);

Declare @Metadata nvarchar(max);

Open Metadata;

FETCH NEXT FROM Metadata INTO @Metadata;

WHILE @@FETCH_STATUS = 0

BEGIN
--Find where these metadata values contain an apostrophe, as this causes Group View to break
 PRINT N' select distinct ProjectListTable.ID as ProjectID, ProjectListTable.Name as ProjectName, PageTable.ID from ProjectListTable inner join PageTable on PageTable.ParentID = ProjectListTable.ID   cross apply ProjectStructureXML.nodes(''Pro//Pag'') Pro(pag) where  pro.pag.value((''@' +  @Metadata  +  '''),''nvarchar(max)'') like ''%''''%''';

FETCH NEXT FROM Metadata INTO @Metadata

END
CLOSE Metadata;
DEALLOCATE Metadata;



--PRINT N' select  ProjectListTable.ID as ProjectID, ProjectListTable.Name as ProjectName, PageTable.ID from ProjectListTable inner join PageTable on PageTable.ParentID = ProjectListTable.ID   cross apply ProjectStructureXML.nodes(''Pro//Pag'') Pro(pag) where ProjectStructureXML.exist(''Pro//Pag[@' +  @Metadata  +  ']'') = 1;'