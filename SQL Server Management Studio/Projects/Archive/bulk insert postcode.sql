create table #ScottishPostcodes (
	postcode nvarchar(20)
)


bulk insert #ScottishPostcodes
from 'C:\Users\DaveJ\Desktop\fulllist.txt'
with (fieldterminator = ',', rowterminator = '\n')
go


select CentreName, PostCode from CentreTable

where PostCode in (
	select PostCode		
		from #ScottishPostcodes
	)

drop table #ScottishPostcodes