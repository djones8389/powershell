--begin tran

declare @newTotalMark table (NewTotalMark int);
	
insert into @newTotalMark(NewTotalMark)


	select
		s.e.value('sum(mark/@mark)', 'float') as NewTotalMark
		, resultData
	from WAREHOUSE_ExamSessionTable

	cross apply resultData.nodes('exam/section/item') s(e)
	where ID IN (78065);
	
	--select * from @newTotalMark

update WAREHOUSE_ExamSessionTable
set resultData.modify('replace value of (/exam/@userMark)[1] with sql:column ("NewTotalMark")')
where ID IN (78065);

--rollback




select
	--s.e.value('sum(//@mark)', 'float') as NewTotalMark
	--, s.e.value('sum(mark/@mark)', 'float') as NewSectionMark
	--, s.e.value('@id','nvarchar(100)') as Page
	 resultData

		--into #OriginalXML
from WAREHOUSE_ExamSessionTable

cross apply resultData.nodes('exam/section') s(e)
cross apply s.e.nodes('item') c1 (item)

where ID IN (78065);






      UPDATE #OriginalXML
      SET resultData.modify('replace value of (/exam/section/item[@id = sql:column("Page")]/@userMark)[1] with sql:column("NewSectionMark")')
      --WHERE ID = @ID;

select * from #OriginalXML
drop table #OriginalXML