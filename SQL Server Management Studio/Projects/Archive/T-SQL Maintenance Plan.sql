DECLARE @BackupFilePath nvarchar(1000) = 'D:\DJMaintenancePlan\'
DECLARE @BackupDBFileName nvarchar(1000) = (SELECT 'D:\DJMaintenancePlan\' + 'AnalyticsManagement' + '_' + convert(varchar(500),GetDate(),112) + '.bak')
DECLARE @BackupLogFileName nvarchar(1000) = (SELECT 'D:\DJMaintenancePlan\' + 'AnalyticsManagement' + '_' + convert(varchar(500),GetDate(),112) + '.trn')
DECLARE @TwoDayClearupDB  datetime  = (SELECT GetDate()-2)

--Task 1:  Check DB integrity,  backup DB, clear-up old DB's

		/*Check DB Integrity*/
		DBCC CHECKDB(N'AnalyticsManagement') WITH NO_INFOMSGS

		
		/*Backup DB*/		 
		BACKUP DATABASE [AnalyticsManagement] TO  DISK = @BackupDBFileName WITH NOFORMAT, NOINIT,  NAME = @BackupDBFileName, SKIP, REWIND, NOUNLOAD, NO_COMPRESSION,  STATS = 10
		declare @backupSetId as int
		select @backupSetId = position from msdb..backupset where database_name=N'AnalyticsManagement' and backup_set_id=(select max(backup_set_id) from msdb..backupset where database_name=N'AnalyticsManagement' )

		/*If backup fails..*/

		if @backupSetId is null 
			begin 
			 raiserror(N'Verify failed. Backup information for database ''?'' not found.', 16, 1) 
			 EXECUTE msdb.dbo.sp_notify_operator @name=N'Dave Jones',@subject=N'Database backup',@body=N'Database backup has failed for AnalyticsManagement'
			end

		RESTORE VERIFYONLY FROM  DISK = @BackupDBFileName WITH  FILE = @backupSetId,  NOUNLOAD,  NOREWIND

		/*Cleanup Task*/
		--EXECUTE master.dbo.xp_delete_file 0,@BackupFilePath,N'bak',N'2015-03-02T07:27:26';
		

		EXECUTE master.dbo.xp_delete_file 0,@BackupDBFileName,N'bak',@TwoDayClearupDB,0;
			
--Task 2: Backup Transaction log,  clear-up old transaction log(s)			


		BACKUP LOG [AnalyticsManagement] TO  DISK = @BackupLogFileName WITH NOFORMAT, NOINIT,  NAME = @BackupLogFileName, SKIP, REWIND, NOUNLOAD,  STATS = 10
		GO
		declare @backupSetId as int
		select @backupSetId = position from msdb..backupset where database_name=N'AnalyticsManagement' and backup_set_id=(select max(backup_set_id) from msdb..backupset where database_name=N'AnalyticsManagement' )
		if @backupSetId is null 
			begin 
				raiserror(N'Verify failed. Backup information for database ''AnalyticsManagement'' not found.', 16, 1) 
				EXECUTE msdb.dbo.sp_notify_operator @name=N'Dave Jones',@subject=N'Log backup',@body=N'Log backup has failed for AnalyticsManagement'
			end
		
		DECLARE @BackupLogFileNameLog nvarchar(1000) = (SELECT 'D:\DJMaintenancePlan\' + 'AnalyticsManagement' + '_' + convert(varchar(500),GetDate(),112) + '.trn')	
		DECLARE @TwoDayClearupDBLog  datetime  = (SELECT GetDate()-2)
		
		RESTORE VERIFYONLY FROM  DISK = @BackupLogFileNameLog WITH  FILE = @backupSetId,  NOUNLOAD,  NOREWIND

		/*Cleanup Task*/
		--EXECUTE master.dbo.xp_delete_file 0,N'D:\DJMaintenancePlan',N'trn',N'2015-03-02T08:53:45'
		EXECUTE master.dbo.xp_delete_file 0,@BackupLogFileNameLog,N'trn',@TwoDayClearupDBLog,0;

--Task 3: Reorganize Indexes

	--Need an if statement to notify operator if it fails--
	EXEC sp_MSforeachtable @command1="ALTER INDEX ALL ON ? REORGANIZE", @command2 = "print 'Reorganising Indexes for:' + '?'";


	--Doesn't let me pass in 'WITH FULLSCAN',  like the UPDATE STATISTICS command does
	EXEC sp_updatestats;  

/*
USE AdventureWorks2012; 
GO
-- Reorganize all indexes on the HumanResources.Employee table.
ALTER INDEX ALL ON HumanResources.Employee
REORGANIZE ; 
GO
*/