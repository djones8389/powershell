use BritishCouncil_SecureAssess_LIVE

create table #MyTable (

	MTKeycode nvarchar(10)
	, MTExamID int
	, MTQualID int
	, MYPackageID int
)

insert into #MyTable

select
	wests.qualificationID, wests.ExamID, wests.examName, wests.qualificationName, examVersionName, examVersionRef, wests.ExternalReference
	,wests.candidateRef, west.KeyCode
	,userMark, userPercentage
	from WAREHOUSE_ExamSessionTable as west

  Inner Join WAREHOUSE_ScheduledExamsTable as WSCET
  on WSCET.ID = west.WAREHOUSEScheduledExamID

  Inner Join WAREHOUSE_UserTable as WUT
  on WUT.ID = WSCET.WAREHOUSECreatedBy

  inner join WAREHOUSE_ExamSessionTable_Shreded as wests
  on wests.examSessionId = west.ID

where west.KeyCode collate Latin1_General_CI_AS not in (
	
	select ScheduledExamRef from BritishCouncil_TPM..ScheduledPackageCandidateExams
)
	and wut.Username = 'ServiceUser_BritishCouncil'
	and west.previousExamState != 10
 
	
	ORDER BY wests.qualificationID, wests.ExamID
	
	
	
	select * from WAREHOUSE_ExamSessionTable where KeyCode = 'V2N6WJ01'-- '3DRAH401'
	
	
	/*
SELECT distinct --PackageExamId, PackageExams.PackageId, SurpassExamId, SurpassExamRef, PackageExams.Name, SurpassQualificationId, Packages.PackageId, Packages.Name, *
	SurpassExamId, SurpassQualificationId, PackageExams.PackageId
	--into #TPMLookup
   FROM BRITISHCOUNCIL_TPM..PackageExams 

inner join BRITISHCOUNCIL_TPM..Packages on Packages.PackageId = PackageExams.PackageId
ORDER BY SurpassExamId, SurpassQualificationId;
	
	
	
	select distinct 
	MTKeycode, MTExamID, MTQualID, MYPackageID
	
	 from #MyTable
	 inner join #TPMLookup on #TPMLookup.SurpassExamId = #MyTable.MTExamID
		and #TPMLookup.SurpassQualificationId = #MyTable.MTQualID
	
	
	drop table #MyTable
	
	update #MyTable
	set #MyTable.MYPackageID = #TPMLookup.PackageId
	from #MyTable	
	
	inner join #TPMLookup
	on #TPMLookup.SurpassExamId = #MyTable.MTExamID
	and #TPMLookup.SurpassQualificationId = #MyTable.MTQualID
		
		where #TPMLookup.SurpassExamId = #MyTable.MTExamID
			and #TPMLookup.SurpassQualificationId = #MyTable.MTQualID
			
	