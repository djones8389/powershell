--Report 1--

select
	foreName
	,surName
	,candidateRef
	,centreName	
	,s.i.value('@id','nvarchar(100)') as 'Item 3'
	,t.u.value('@id','nvarchar(100)') as 'Item 5'
	,centreCode
	,qualificationName
	,examName
	,case grade when 'Fail' then 'Not yet compentent' else 'Competent' end as [Grade]
	,userMark
	,userPercentage
	,submittedDate
	,'Warehoused' as [ExamState]
 from WAREHOUSE_ExamSessionTable_Shreded 
	
  inner join WAREHOUSE_ExamSessionTable on WAREHOUSE_ExamSessionTable.id = WAREHOUSE_ExamSessionTable_Shreded.examSessionId
			
	cross apply WAREHOUSE_ExamSessionTable_Shreded.ResultData.nodes('/exam/section/item[3]') as s(i)
	cross apply WAREHOUSE_ExamSessionTable_Shreded.ResultData.nodes('/exam/section/item[5]') as t(u)
	
	where qualificationName = 'Processing bookkeeping transactions (AQ2013)'
		and examName like '%PBKT%' 
		and userPercentage >= '68'
		and grade <> 'Pass'
	order by foreName, surName;
	
--Report 2--
	
select
	foreName
	,surName
	,candidateRef
	,centreName	
	--,s.i.value('@id','nvarchar(100)') as 'Item 3'
	--,t.u.value('@id','nvarchar(100)') as 'Item 5'
	,centreCode
	,qualificationName
	,examName
	,case grade when 'Fail' then 'Not yet compentent' else 'Competent' end as [Grade]
	,userMark
	,userPercentage
	,submittedDate
	,'Warehoused' as [ExamState]
 from WAREHOUSE_ExamSessionTable_Shreded 
	
  inner join WAREHOUSE_ExamSessionTable on WAREHOUSE_ExamSessionTable.id = WAREHOUSE_ExamSessionTable_Shreded.examSessionId
			
	--cross apply WAREHOUSE_ExamSessionTable_Shreded.ResultData.nodes('/exam/section/item[3]') as s(i)
	--cross apply WAREHOUSE_ExamSessionTable_Shreded.ResultData.nodes('/exam/section/item[5]') as t(u)
	
	where qualificationName = 'Business taxation (AQ2013)'
		and examName like '%BTAX%' 
		and userPercentage >= '68.9'
		and grade <> 'Pass'
	order by foreName, surName;
		
	
--Report 3--
	
	
select
	foreName
	,surName
	,candidateRef
	,centreName	
	--,s.i.value('@id','nvarchar(100)') as 'Item 3'
	--,t.u.value('@id','nvarchar(100)') as 'Item 5'
	,centreCode
	,qualificationName
	,examName
	,case grade when 'Fail' then 'Not yet compentent' else 'Competent' end as [Grade]
	,userMark
	,userPercentage
	,submittedDate
	,'Warehoused' as [ExamState]
 from WAREHOUSE_ExamSessionTable_Shreded 
	
  inner join WAREHOUSE_ExamSessionTable on WAREHOUSE_ExamSessionTable.id = WAREHOUSE_ExamSessionTable_Shreded.examSessionId
			
	--cross apply WAREHOUSE_ExamSessionTable_Shreded.ResultData.nodes('/exam/section/item[3]') as s(i)
	--cross apply WAREHOUSE_ExamSessionTable_Shreded.ResultData.nodes('/exam/section/item[5]') as t(u)
	
	where qualificationName = 'Business taxation (AQ2010)'
		and examName like '%BTX%' 
		and userPercentage >= '67.9'
		and grade <> 'Pass'
	order by foreName, surName;
		
		
		
		
		
	
	
	
	
	/*
--79--

select
	foreName
	,surName
	,candidateRef
	,CentreTable.CentreName	
	--,s.i.value('@id','nvarchar(100)') as 'Item 3'
	--,t.u.value('@id','nvarchar(100)') as 'Item 5'
	,centreCode
	--,QualificationName
	,ScheduledExamsTable.examName
	,grade
	,userMark
	,userPercentage
	,ExamStateChangeAuditTable.StateChangeDate
	, ExamSessionTable.examState as [ExamState]
	, ScheduledExamsTable.qualificationId
	
	
 from ExamSessionTable
	
  --inner join ExamSessionTable_Shredded on ExamSessionTable_Shredded.examSessionId =  ExamSessionTable.id
  inner join ScheduledExamsTable on ScheduledExamsTable.ID = ExamSessionTable.ScheduledExamID
  inner join CentreTable on CentreTable.ID = ScheduledExamsTable.CentreID 
  inner join ExamStateChangeAuditTable on ExamStateChangeAuditTable.ExamSessionID = ExamSessionTable.ID
  inner join UserTable on usertable.ID = ExamSessionTable.UserID
  			
	----cross apply ExamSessionTable_Shredded.ResultData.nodes('/exam/section/item[3]') as s(i)
	----cross apply ExamSessionTable_Shredded.ResultData.nodes('/exam/section/item[5]') as t(u)
	
	where ScheduledExamsTable.qualificationId = 79--qualificationName = 'Processing bookkeeping transactions (AQ2013)'
		and ScheduledExamsTable.examName like '%PBKT%' 
	--	and userPercentage >= '68'
	--	and grade <> 'Pass'		
	and ExamStateChangeAuditTable.NewState = 9
	and examState <> 13
	order by foreName, surName
	
	
	
	
	select ExamSessionTable.ID 
	
	
	 from ExamSessionTable
	
 -- inner join ExamSessionTable_Shredded on ExamSessionTable_Shredded.examSessionId =  ExamSessionTable.id
  inner join ScheduledExamsTable on ScheduledExamsTable.ID = ExamSessionTable.ScheduledExamID
  inner join CentreTable on CentreTable.ID = ScheduledExamsTable.CentreID 
  inner join ExamStateChangeAuditTable on ExamStateChangeAuditTable.ExamSessionID = ExamSessionTable.ID
  
  where ScheduledExamsTable.qualificationId = 79 and ExamStateChangeAuditTable.NewState = 9
  
  */