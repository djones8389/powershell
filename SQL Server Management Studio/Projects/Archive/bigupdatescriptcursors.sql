DECLARE IDs CURSOR FOR
	SELECT ExamSessionID
	FROM WAREHOUSE_ExamSessionTable_Shreded
	WHERE ExternalReference = '1306101194'
		AND ExamSessionID NOT IN (319125,319126)
		AND WarehouseTime > '28 Aug 2013'
		AND WarehouseTime < '29 Aug 2013'
		AND ResultData.exist('exam[@totalMark="31"]') = 1
		AND ResultData.exist('exam/section/item[@id="869P3933"]') = 1
		AND ResultData.exist('exam/section/item[@totalMark="2"]') = 1;

OPEN IDs;

DECLARE @ID int;

FETCH NEXT FROM IDs INTO @ID;

WHILE @@FETCH_STATUS = 0
BEGIN

	----------------WAREHOUSE_ExamSessionTable_Shreded----------------
	DECLARE @ExamUserPercentage [decimal] (6, 3);

	UPDATE WAREHOUSE_ExamSessionTable_Shreded
	SET resultData.modify('replace value of (/exam/@totalMark)[1] with sql:variable("@newValue") ')
	WHERE examSessionId = @ID

	UPDATE WAREHOUSE_ExamSessionTable_Shreded
	SET resultData.modify('replace value of (/exam/section/@totalMark)[1] with sql:variable("@newValue") ')
	WHERE examSessionId = @ID

	SELECT @ExamUserPercentage = CAST(((resultdata.value('(/exam/@userMark)[1]', 'decimal(13, 10)') / resultdata.value('(/exam/@totalMark)[1]', 'decimal(13, 10)')) * 100) AS [decimal](6, 3))
	FROM WAREHOUSE_ExamSessionTable_Shreded
	WHERE examSessionId = @ID

	UPDATE WAREHOUSE_ExamSessionTable_Shreded
	SET resultdata.modify('replace value of (/exam/@userPercentage)[1] with sql:variable("@ExamUserPercentage1")')
	WHERE examSessionId = @ID

	UPDATE WAREHOUSE_ExamSessionTable_Shreded
	SET resultdata.modify('replace value of (/exam/section/@userPercentage)[1] with sql:variable("@ExamUserPercentage1")')
	WHERE examSessionId = @ID

	--update totalMark of Q10--
	UPDATE WAREHOUSE_ExamSessionTable_Shreded
	SET resultdata.modify('replace value of (/exam/section/item[@id = "869P3933" and @version = "20"]/@totalMark)[1] with 1')
	WHERE examSessionId = @ID

	--update SectionMark for Section 3--
	UPDATE WAREHOUSE_ExamSessionTable_Shreded
	--SET  resultdata.modify('replace value of (/exam/section/@totalMark)[1] with 8')
	SET resultdata.modify('replace value of (/exam/section[@id="3" and @totalMark = "11"]/@totalMark)[1] with 10')
	WHERE examSessionId = @ID

	----------------WAREHOUSE_ExamSessionTable----------------
	UPDATE WAREHOUSE_ExamSessionTable
	SET resultData.modify('replace value of (/exam/@totalMark)[1] with sql:variable("@newValue") ')
	WHERE ID = @ID

	UPDATE WAREHOUSE_ExamSessionTable
	SET resultData.modify('replace value of (/exam/section/@totalMark)[1] with sql:variable("@newValue") ')
	WHERE ID = @ID

	UPDATE WAREHOUSE_ExamSessionTable
	SET @ExamUserPercentage = CAST(((resultdata.value('(/exam/@userMark)[1]', 'decimal(13, 10)') / resultdata.value('(/exam/@totalMark)[1]', 'decimal(13, 10)')) * 100) AS [decimal](6, 3))
	WHERE ID = @ID

	UPDATE WAREHOUSE_ExamSessionTable
	SET resultdata.modify('replace value of (/exam/@userPercentage)[1] with sql:variable("@ExamUserPercentage2")')
	WHERE ID = @ID

	UPDATE WAREHOUSE_ExamSessionTable
	SET resultdata.modify('replace value of (/exam/section/@userPercentage)[1] with sql:variable("@ExamUserPercentage2")')
	WHERE ID = @ID

	--update totalMark of Q10--
	UPDATE WAREHOUSE_ExamSessionTable
	SET resultdata.modify('replace value of (/exam/section/item[@id = "869P3933" and @version = "20"]/@totalMark)[1] with 1')
	WHERE ID = @ID

	--update SectionMark for Section 2--
	UPDATE WAREHOUSE_ExamSessionTable
	SET resultdata.modify('replace value of (/exam/section[@id="3" and @totalMark = "11"]/@totalMark)[1] with 10')
	WHERE ID = @ID

	----------------WAREHOUSE_ExamSessionTable----------------
	DECLARE @FSectionUserPercentage [decimal](6, 3);

	UPDATE WAREHOUSE_ExamSessionTable
	SET resultDataFull.modify('replace value of (/assessmentDetails/assessment/@totalMark)[1] with sql:variable("@newValue") ')
	WHERE ID = @ID

	UPDATE WAREHOUSE_ExamSessionTable
	SET resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/@totalMark)[1] with sql:variable("@newValue") ')
	WHERE ID = @ID

	SELECT @FSectionUserPercentage = CAST(((resultDataFull.value('(/assessmentDetails/assessment/@userMark)[1]', 'decimal(13, 10)') / resultDataFull.value('(/assessmentDetails/assessment/@totalMark)[1]', 'decimal(13, 10)')) * 100) AS [decimal](6, 3))
	FROM WAREHOUSE_ExamSessionTable
	WHERE ID = @ID

	UPDATE WAREHOUSE_ExamSessionTable
	SET resultDataFull.modify('replace value of (/assessmentDetails/assessment/@userPercentage)[1] with sql:variable("@FSectionUserPercentage")')
	WHERE ID = @ID

	UPDATE WAREHOUSE_ExamSessionTable
	SET resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/@userPercentage)[1] with sql:variable("@FSectionUserPercentage")')
	WHERE ID = @ID

	--update totalMark of Q10--
	UPDATE WAREHOUSE_ExamSessionTable
	SET resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item[@id = "869P3933" and @version = "20"]/@totalMark)[1] with 1')
	WHERE ID = @ID

	--update SectionMark for Section 2--
	UPDATE WAREHOUSE_ExamSessionTable
	SET resultDataFull.modify('replace value of (/assessmentDetails/assessment/section[@id="3" and @totalMark = "11"]/@totalMark)[1] with 10')
	WHERE ID = @ID

	UPDATE WAREHOUSE_ExamSessionTable
	SET StructureXML.modify('replace value of (/assessmentDetails/assessment/section/item[@id = "869P3933" and @version = "20"]/@totalMark)[1] with 1')
	WHERE ID = @ID

	FETCH NEXT FROM IDs INTO @ID;
END

CLOSE IDs;
DEALLOCATE IDs;