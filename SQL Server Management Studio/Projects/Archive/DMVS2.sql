--Find missing indexes + find out which table it relates to:

SELECT name, sys.dm_db_missing_index_details.* 
From	sys.dm_db_missing_index_details

inner join sys.databases
on sys.databases.database_id =  sys.dm_db_missing_index_details.database_id


order by name


select top 10 name, sys.dm_db_index_usage_stats.database_id, sys.dm_db_index_usage_stats.object_id, index_id, user_seeks, user_scans, last_user_seek, last_user_scan
	, last_system_seek, last_system_scan, last_user_update, system_seeks,equality_columns, inequality_columns, included_columns, statement, index_id
	 from sys.dm_db_index_usage_stats 

inner join sys.databases
on sys.databases.database_id =  sys.dm_db_index_usage_stats.database_id

inner join sys.dm_db_missing_index_details
on sys.dm_db_missing_index_details.database_id = sys.dm_db_index_usage_stats.database_id

order by user_seeks desc