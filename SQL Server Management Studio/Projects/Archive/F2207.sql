select  
	ProjectListTable.id as ProjectID
	, pro.pag.value('@ID','nvarchar(100)') as FolderID
	, pro.pag.value('(//Pag/@ID)[8]','nvarchar(100)') as PageID
	, Name

	--, ProjectStructureXml
from ProjectListTable (NOLOCK)

	cross apply ProjectStructureXml.nodes('/*[local-name(.)!="Rec"]//Fol') pro(pag)

WHERE id = 891


--	cross apply ProjectStructureXml.nodes('Pro/*[local-name(.)!="Rec"]//Pag') pro(pag)--



select  
	ProjectListTable.id as ProjectID
	--, ProjectStructureXml.value('(//Fol[ID=921]//Pag/@ID)[1]','nvarchar(100)')
	, ProjectStructureXml.query('data(//Fol/@ID=921)')  -- = true
	--, ProjectStructureXml.query('data(//Fol/@ID=921//Pag)')
	, ProjectStructureXml.value('(//Fol[@ID="921"]/Pag[@ID="922"]/@Nam)[1]', 'nvarchar(100)')
	, ProjectStructureXml.value('(//Fol[@ID="921"]/Pag//@ID)[1]', 'nvarchar(100)')
	--, ProjectStructureXml
	, pro.pag.value('(//Fol[@ID="921"]/Pag//@ID)[1]', 'nvarchar(100)')
from ProjectListTable (NOLOCK)

	cross apply ProjectStructureXml.nodes('/*[local-name(.)!="Rec"]//Fol') pro(pag)

WHERE id = 891


























DECLARE @MyTable TABLE (projectID smallint, FolderCount SMALLINT, ItemCount smallint)
INSERT @MyTable(projectID, FolderCount, ItemCount)


SELECT *
FROM
(

SELECT  
	ProjectListTable.id as ProjectID
	--, pro.pag.value('@ID','nvarchar(100)') as ProjectID
	, COUNT(pro.fol.value('@ID','int')) AS NumberOfFolders
	--, pro.fol.value('@ID','int') AS FolderID
	--, pag.es.value('@ID','int') AS PageID
	--INTO #TEST
from ProjectListTable (NOLOCK)

	cross apply ProjectStructureXml.nodes('Pro//Fol') pro(fol)
--WHERE id = 891
	GROUP BY ProjectListTable.id

EXCEPT

select  
	ProjectListTable.id as ProjectID
	--, pro.pag.value('@ID','nvarchar(100)') as ProjectID
	, COUNT(pro.fol.value('@ID','int')) AS NumberOfFolders
	--, pro.fol.value('@ID','int') AS FolderID
	--, pag.es.value('@ID','int') AS PageID
	--INTO #TEST
from ProjectListTable (NOLOCK)

	cross apply ProjectStructureXml.nodes('Pro/Rec//Fol') pro(fol)
--WHERE id = 891
	GROUP BY ProjectListTable.id
) A
	
LEFT JOIN (
--INSERT @MyTable(projectID, ItemCount)
select  
	ProjectListTable.id as ProjectID
	--, pro.pag.value('@ID','nvarchar(100)') as ProjectID
	, COUNT(pro.pag.value('@ID','int')) AS NumberofPages
	--, pag.es.value('@ID','int') AS PageID
	--INTO #TEST
from ProjectListTable (NOLOCK)

cross apply ProjectStructureXml.nodes('Pro//Pag') pro(pag)

--WHERE id = 891
		GROUP BY ProjectListTable.id

EXCEPT

select  
	ProjectListTable.id as ProjectID
	--, pro.pag.value('@ID','nvarchar(100)') as ProjectID
	, COUNT(pro.pag.value('@ID','int')) AS NumberofPages
	--, pag.es.value('@ID','int') AS PageID
	--INTO #TEST
from ProjectListTable (NOLOCK)

cross apply ProjectStructureXml.nodes('Pro/Rec//Pag') pro(pag)

--WHERE id = 891
		GROUP BY ProjectListTable.id
) B

ON A.ProjectID = B.ProjectID
