SELECT EST.ID, EST.KeyCode, EST.examState, UT.Forename, UT.Surname, CT.CentreName, SCET.examName
  , CreatedBy  
  
  FROM ExamSessionTable as EST

  Inner Join ScheduledExamsTable as SCET
  on SCET.ID = EST.ScheduledExamID
  
  Inner Join UserTable as UT
  on UT.ID = EST.UserID

  Inner Join CentreTable as CT
  on CT.ID = SCET.CentreID

where forename = 'Ben'
	and surname like 'vt%'
	
	
	
	SELECT c.name AS ColName, t.name AS TableName
FROM sys.columns c
    JOIN sys.tables t ON c.object_id = t.object_id
WHERE c.name LIKE '%created%'


select * from UserTable where ID = 180685



select --top 10

--    UT.ID
      Forename
      ,Surname
      ,CentreName
--    ,RT.ID as RoleID
      ,RT.Name as RoleName
      ,PT.Name as PermissionName

from UserTable as UT

inner join AssignedUserRolesTable as AURT
on AURT.UserID = UT.ID

inner join CentreTable as CT
on CT.ID = AURT.CentreID

inner join RolesTable as RT
on RT.ID = AURT.RoleID

inner join RolePermissionsTable as RPT
on RPT.RoleID = RT.ID

inner join PermissionsTable as PT
on PT.ID = RPT.PermissionID

where ut.id = 180685
order by PermissionName asc
