use [QEYADAH_SecureAssess_10.0]

select 

qualificationname
,examname
,foreName
,surName
,candidateRef
,ULN
,grade
,userPercentage
,centreName
,completionDate
,duration

from

WAREHOUSE_ExamSessionTable_Shreded

inner join WAREHOUSE_ExamSessionTable
on WAREHOUSE_ExamSessionTable.ID = WAREHOUSE_ExamSessionTable_Shreded.examSessionId

where completionDate >= '01 Oct 2012'
	and  completionDate <= '31 Oct 2013'
