SELECT  --1350896--
				   WAREHOUSE_ExamSessionTable_Shreded.examSessionId ,
                  [examVersionID],
                  [examName],
                  [ExamRef],
                  [examVersionName],
                  [ExamVersionRef],
                  [qualificationId],
                  [QualificationName],
                  [QualificationRef]
                  FROM WAREHOUSE_ExamSessionTable_Shreded

inner join WAREHOUSE_ExamSessionTable_ShrededItems 
ON WAREHOUSE_ExamSessionTable_ShrededItems.examSessionId = WAREHOUSE_ExamSessionTable_Shreded.examSessionId 

where examVersionId = '372'
and examName = 'CMGT (SA)'
and examRef = 'CACMGT'
and examVersionName = 'CMGT (SA)'
and examVersionRef = 'CACMGT'
and qualificationid = '45'
and qualificationName = 'Cash Management (SA) (AQ2010)'
and qualificationRef = 'SAQCF'