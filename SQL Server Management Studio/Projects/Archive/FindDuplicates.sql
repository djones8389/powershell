select
      ID
      ,Name
      ,a.b.value('@ID', 'int') as PageID

into #myTempTable
from ProjectListTable 

cross apply ProjectStructureXml.nodes('/Pro//Pag') a(b)

--where ID = 6057

select * from #myTempTable  
--drop table #myTempTable

SELECT Name as ProjectName, S.ID as ProjectID, s.PageID
    FROM #myTempTable S
    INNER JOIN
    (
        SELECT ID, PageID
        FROM #myTempTable
        GROUP BY ID, PageID
        HAVING COUNT(*) > 1
    ) as T
    ON S.ID=T.ID AND s.PageID=T.PageID
    
  order by ProjectID, PageID  
  
  drop table #myTempTable


select * from ProjectListTable where ID = 867