SELECT top 10 EST.ID, EST.KeyCode, EST.examState, UT.Forename, UT.Surname, CT.CentreName, SCET.examName,  structurexml, resultdata
  FROM ExamSessionTable as EST

  Inner Join ScheduledExamsTable as SCET
  on SCET.ID = EST.ScheduledExamID
  
  Inner Join UserTable as UT
  on UT.ID = EST.UserID

  Inner Join CentreTable as CT
  on CT.ID = SCET.CentreID

where examState = 15 and EST.ID in (select examsessionid from ExamSessionItemResponseTable)

--where EST.ID = 22730--examState = 15 and EST.ID in (select examsessionid from ExamSessionItemResponseTable)


select * from ExamSessionItemResponseTable where ExamSessionID = 22730 and ItemResponseData.exist('/p/s/c/i[@sl="1" and @ca="1"]') = 1


--update ExamSessionTable
--set previousExamState = examState, examState = 10
--where ID = 22730

select ID, resultdata from WAREHOUSE_ExamSessionTable where ExamSessionID = 22730

--update ExamSessionTable
--set StructureXML = '<assessmentDetails>
--  <assessmentID>54</assessmentID>
--  <qualificationID>116</qualificationID>
--  <qualificationName>Installation Test</qualificationName>
--  <qualificationReference>July 500 Pilot</qualificationReference>
--  <assessmentGroupName>Familiarisation Test</assessmentGroupName>
--  <assessmentGroupID>33</assessmentGroupID>
--  <assessmentGroupReference>500 Pilot</assessmentGroupReference>
--  <assessmentName>Version 1</assessmentName>
--  <validFromDate>13 Apr 2012</validFromDate>
--  <expiryDate>14 Apr 2022</expiryDate>
--  <startTime>00:00:00</startTime>
--  <endTime>23:59:59</endTime>
--  <duration>10</duration>
--  <defaultDuration>10</defaultDuration>
--  <scheduledDuration>
--    <value>10</value>
--    <reason></reason>
--  </scheduledDuration>
--  <sRBonus>30</sRBonus>
--  <sRBonusMaximum>30</sRBonusMaximum>
--  <externalReference>500 Pilot</externalReference>
--  <passLevelValue>0</passLevelValue>
--  <passLevelType>1</passLevelType>
--  <status>2</status>
--  <testFeedbackType>
--    <passFail>0</passFail>
--    <percentageMark>0</percentageMark>
--    <allowSummaryFeedback>0</allowSummaryFeedback>
--    <summaryFeedbackType>1</summaryFeedbackType>
--    <itemSummary>0</itemSummary>
--    <itemReview>0</itemReview>
--    <itemReviewCorrectAnswer>0</itemReviewCorrectAnswer>
--    <itemFeedback>0</itemFeedback>
--    <printableSummary>0</printableSummary>
--    <candidateDetails>0</candidateDetails>
--    <feedbackByReference>0</feedbackByReference>
--    <allowFeedbackDuringExam>0</allowFeedbackDuringExam>
--    <allowFeedbackDuringSummary>0</allowFeedbackDuringSummary>
--  </testFeedbackType>
--  <itemFeedback>0</itemFeedback>
--  <allowCalculator>0</allowCalculator>
--  <lastInUseDate>20 Jun 2012 09:13:55</lastInUseDate>
--  <completedScriptReview>0</completedScriptReview>
--  <assessment currentSection="1" totalTime="0" currentTime="0">
--    <intro id="0" name="Introduction" currentItem="">
--      <item id="3507P6050" name="Familiarisation Test" totalMark="1" version="12" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="0" flagged="0" />
--    </intro>
--    <outro id="0" name="Finish" currentItem="" />
--    <tools id="0" name="Tools" currentItem="" />
--    <section id="1" name="Section 1" passLevelValue="0" passLevelType="1" itemsToMark="0" currentItem="3507P5964">
--      <item id="3507P5324" name="Demo_Gr1" totalMark="1" version="16" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="0" flagged="0" quT="10" />
--      <item id="3507P5829" name="Demo_Gr2" totalMark="1" version="17" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="0" flagged="1" quT="10" />
--      <item id="3507P5830" name="Demo_GR3" totalMark="1" version="17" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="0" flagged="0" quT="10" />
--      <item id="3507P5831" name="Demo_Gr4" totalMark="1" version="17" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="0" flagged="0" quT="10" />
--      <item id="3507P5832" name="Demo_Gr5" totalMark="1" version="17" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="0" flagged="0" quT="10" />
--      <item id="3507P5833" name="Demo_Gr6" totalMark="1" version="18" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="0" flagged="0" quT="10" />
--      <item id="3507P5834" name="Demo_Gr7" totalMark="1" version="18" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="0" flagged="0" quT="10" />
--      <item id="3507P5835" name="Demo_GR8" totalMark="1" version="18" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="0" flagged="0" quT="10" />
--      <item id="3507P5845" name="Demo_GR9" totalMark="1" version="18" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="0" flagged="0" quT="10" />
--      <item id="3507P5847" name="Demo_Gr10" totalMark="1" version="18" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="0" flagged="0" quT="10" />
--      <item id="3507P5848" name="Demo_Gr11" totalMark="1" version="18" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="0" flagged="0" quT="10" />
--      <item id="3507P5849" name="Demo_Gr12" totalMark="1" version="18" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="0" flagged="0" quT="10" />
--      <item id="3507P5852" name="Demo Voc Coll" totalMark="1" version="12" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="0" flagged="0" quT="12" />
--      <item id="3507P5851" name="Demo Voc Syn" totalMark="1" version="13" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="0" flagged="0" quT="12" />
--      <item id="3507P5850" name="Demo Voc Def" totalMark="1" version="11" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="0" flagged="0" quT="12" />
--      <item id="3507P5959" name="Demo listening" totalMark="1" version="27" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="0" flagged="0" quT="10" />
--      <item id="3507P5853" name="Demo_write_Task_1" totalMark="5" version="7" markingType="1" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="0" flagged="0" quT="11" />
--      <item id="3507P5854" name="Demo_write_Task_2" totalMark="5" version="8" markingType="1" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="0" flagged="0" quT="11" />
--      <item id="3507P5855" name="Demo_write_Task_3" totalMark="5" version="7" markingType="1" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="0" flagged="0" quT="11" />
--      <item id="3507P5856" name="Demo_write_Task_4" totalMark="5" version="8" markingType="1" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="0" flagged="0" quT="11" />
--      <item id="3507P5970" name="Demo A1 cloze" totalMark="5" version="10" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="0" flagged="0" quT="12" />
--      <item id="3507P5962" name="Demo A2 ordering" totalMark="6" version="9" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="0" flagged="0" quT="13" />
--      <item id="3507P6025" name="Demo B1 cloze" totalMark="1" version="9" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="0" flagged="0" quT="16" />
--      <item id="3507P5964" name="Demo B2 matching" totalMark="6" version="8" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="0" flagged="0" quT="12" />
--    </section>
--  </assessment>
--  <isValid>1</isValid>
--  <isPrintable>0</isPrintable>
--  <qualityReview>0</qualityReview>
--  <numberOfGenerations>1</numberOfGenerations>
--  <requiresInvigilation>1</requiresInvigilation>
--  <advanceContentDownloadTimespan>120</advanceContentDownloadTimespan>
--  <automaticVerification>1</automaticVerification>
--  <offlineMode>2</offlineMode>
--  <requiresValidation>0</requiresValidation>
--  <requiresSecureClient>1</requiresSecureClient>
--  <examType>0</examType>
--  <advanceDownload>0</advanceDownload>
--  <gradeBoundaryData>
--    <gradeBoundaries passLevelType="1" adjustedGradeBoundaries="0">
--      <grade modifier="lt" value="0" description="Fail" />
--      <grade modifier="gt" value="0" description="Pass" />
--    </gradeBoundaries>
--  </gradeBoundaryData>
--  <autoViewExam>0</autoViewExam>
--  <autoProgressItems>0</autoProgressItems>
--  <confirmationText>
--    <confirmationText></confirmationText>
--  </confirmationText>
--  <requiresConfirmationCheck>0</requiresConfirmationCheck>
--  <allowCloseWithoutSubmit>0</allowCloseWithoutSubmit>
--  <useSecureMarker>0</useSecureMarker>
--  <annotationVersion>0</annotationVersion>
--  <allowPackagingDelivery>1</allowPackagingDelivery>
--  <scoreBoundaryData>
--    <scoreBoundaries showScoreBoundaryColumn="0">
--      <score modifier="lt" value="40" description="Not met" higherBoundarySet="1" />
--      <score modifier="lt" value="40" description="Not met" higherBoundarySet="0" />
--      <score modifier="gt" value="40" description="Close to met" higherBoundarySet="1" />
--      <score modifier="gt" value="40" description="Met" higherBoundarySet="0" />
--      <score modifier="gt" value="45" description="Met" higherBoundarySet="1" />
--      <score modifier="gt" value="80" description="Exceeded" higherBoundarySet="1" />
--    </scoreBoundaries>
--  </scoreBoundaryData>
--  <showCandidateReportResultColumn>0</showCandidateReportResultColumn>
--  <showCandidateReportPercentageColumn>1</showCandidateReportPercentageColumn>
--  <certifiedAccessible>0</certifiedAccessible>
--  <markingProgressVisible>1</markingProgressVisible>
--  <markingProgressMode>1</markingProgressMode>
--  <secureClientOperationMode>1</secureClientOperationMode>
--  <examDeliverySystemStyleType>delivery</examDeliverySystemStyleType>
--</assessmentDetails>'
--where id = 22730