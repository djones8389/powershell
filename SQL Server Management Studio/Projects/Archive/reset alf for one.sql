USE UAT_BC_ContentProducer

select ID, Name, ProjectStructureXml 
	from ProjectListTable 
		where id = 3527;


update ProjectListTable
set ProjectStructureXml = '<Pro ID="3527" Nam="BTLTest1" CurrentMaxID="3543" UseAsTemplate="0" prB="chrysanthemum.jpg" eBa="Chrysanthemum" bac="Chrysanthemum" alF="0" opT="0" AttributeFilter="sta" pEv="0" SyncMarkSchemes="0">
  <Rec ID="3529" Nam="Recycle Bin" />
  <Pag ID="3540" Nam="Copy of A213_6b" lMU="116" lMD="21/03/2014 16:31:26" quT="16" sta="4" alF="1" tes="0" pMU="116" pMD="21/03/2014 16:28:55" TotalMark="1" Component="Listening" Domain="General" MarkingType="0" pEv="1" />
  <Pag ID="3530" Nam="1" markingSchemeFor="-1" isWordTemplate="F" isSourceItem="F" chO="101" chO2="BTL145" lMU="101" lMD="10/08/2012 14:00:49" />
  <Pag ID="3531" Nam="23" markingSchemeFor="-1" isWordTemplate="F" isSourceItem="F" chO="101" chO2="BTL145" lMU="101" lMD="15/08/2012 15:49:12" />
  <Fol ID="3532" Nam="1">
    <Pag ID="3538" Nam="jltest" markingSchemeFor="-1" isWordTemplate="F" isSourceItem="F" lMU="113" lMD="28/05/2014 16:39:52" pMU="113" pMD="28/05/2014 16:39:52" />
    <Pag ID="3533" Nam="hello" markingSchemeFor="-1" isWordTemplate="F" isSourceItem="F" chO="101" chO2="BTL145" lMU="101" lMD="16/08/2012 08:49:50" />
    <Pag ID="3534" Nam="2" markingSchemeFor="-1" isWordTemplate="F" isSourceItem="F" chO="101" chO2="BTL145" lMU="101" lMD="10/09/2012 09:15:45" />
  </Fol>
  <Fol ID="3535" Nam="DaveJ">
    <Pag ID="3536" Nam="human" markingSchemeFor="-1" isWordTemplate="F" isSourceItem="F" lMD="21/09/2012 16:23:38" lMU="93" MarkingType="1" TotalMark="1" quT="10" sta="6" tes="0" alF="0" />
    <Pag ID="3537" Nam="computer" markingSchemeFor="-1" isWordTemplate="F" isSourceItem="F" lMD="21/09/2012 16:24:13" lMU="93" MarkingType="0" TotalMark="1" quT="10" sta="6" tes="0" alF="0" pMU="93" pMD="21/09/2012 16:24:13" />
  </Fol>
  <Pag ID="3539" Nam="IMYTest" markingSchemeFor="-1" isWordTemplate="F" isSourceItem="F" Typ="1" sta="6" lMU="116" lMD="21/03/2014 16:33:32" quT="10,11" alF="1" tes="0" pEv="1" pMU="116" pMD="21/03/2014 16:32:35" />
  <Pag ID="3543" Nam="IMYTest Mark Scheme" markingSchemeFor="3539" isWordTemplate="F" isSourceItem="F" Typ="8" TotalMark="0" sta="0" />
  <Pag ID="3541" Nam="BTL" markingSchemeFor="-1" isWordTemplate="F" isSourceItem="F" Typ="1" sta="0" />
  <Pag ID="3542" Nam="BTL Mark Scheme" markingSchemeFor="3541" isWordTemplate="F" isSourceItem="F" Typ="8" TotalMark="0" sta="0" />
</Pro>'
where id = 3527;


update PageTable
set ver = ver + 1
where ID = '3527p3539';