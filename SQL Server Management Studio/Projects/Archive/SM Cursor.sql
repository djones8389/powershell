DECLARE Items CURSOR 
FOR

SELECT 
	 CONVERT(NVARCHAR(10), ItemTable.ProjectID) + 'P' + CONVERT(NVARCHAR(10), ItemTable.ItemRef) AS ItemId
	, ItemTable.TotalMark
	, [version]
FROM (
SELECT
      ProjectID, 
      ItemRef,
      CONVERT(NVARCHAR(10), ProjectID) + 'P' + CONVERT(NVARCHAR(10), ItemRef) AS ItemID,
      MAX(version) AS MAxVersion
FROM ItemTable
GROUP BY ProjectID, ItemRef
HAVING COUNT(DISTINCT TotalMark) > 1) AS ChangedItems
INNER JOIN ItemTable
ON ItemTable.ProjectID = ChangedItems.ProjectID AND ItemTable.ItemRef = ChangedItems.ItemRef AND ChangedItems.MAxVersion = ItemTable.[Version]
GROUP BY ItemTable.ProjectID, ItemTable.ItemRef, ItemTable.TotalMark, version



DECLARE @ItemID NVARCHAR(100), @TotalMark INT, @Version int;

OPEN Items;

FETCH NEXT FROM Items INTO @ItemID, @TotalMark, @Version;

WHILE @@FETCH_STATUS = 0
BEGIN

declare @myID nvarchar(100) = @ItemID;
declare @myversion int = @Version;
declare @mytotalmark int = @TotalMark;


SELECT ID, resultData, Keycode
FROM BritishCouncil_SecureAssess_LIVE..WAREHOUSE_ExamSessionTable
WHERE resultData.exist('//item[@id=sql:variable("@myID")] [@version=sql:variable("@myversion")] [@totalMark=sql:variable("@mytotalmark")]') = 1

FETCH NEXT FROM Items INTO @ItemID, @TotalMark, @Version;

END

CLOSE Items;
DEALLOCATE Items;
