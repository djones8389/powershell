--Select all of the images out of the ProjectManifestTable, into a TempTable that have a filename such as the one in Project 942 that is NULL--

select ROW_NUMBER() over (order by name) rownum
			 , name
			 , Image
	 into #TempTable
	from ProjectManifestTable

where Image is not null
	and name in (
					select name
						from ProjectManifestTable
							where ProjectId = 942
							and Image is null
							);

--Delete duplicate filenames,  so there is only 1 of each filename and they have an image in there
--#TempTable has 1,089,  need to filter duplicates (leaving 176 unique Names)

delete
from #TempTable
where rownum not in (	
	
	select rownum
	from 
	#TempTable as R
	inner join
		(
		select  name, MAX(rownum) as RN --Max(DATALENGTH(image)) as 'Myimage'
			from #TempTable			
			
			group by name
			
		) X
	on r.rownum = X.RN
);

/*

update ProjectManifestTable
set Image = (select Image from #TempTable where #TempTable.name = ProjectManifestTable.name)
where ProjectId = 942 and Image is null;

*/
/*
select * from #TempTable

drop table  #TempTable
*/