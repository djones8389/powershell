SELECT * FROM sys.sql_expression_dependencies
WHERE referenced_entity_name = 'ExamSessionTable'

select * from sys.objects where object_id in (SELECT referencing_id FROM sys.objects
WHERE referenced_entity_name = 'ExamSessionTable')
	and type != 'P'

SELECT D.*, C.name FROM sys.sql_dependencies D

inner join sys.columns C
on C.object_id = D.object_id

inner join sys.objects O
on O.object_id = D.object_id
	and c.column_id = D.column_id

where D.object_id in (SELECT object_id from sys.objects where name = 'ExamSessionTable')

select * from sys.columns where column_id = '36'
	and object_id = '1387868011'


select distinct a.parent_object_ID, c.name, t.name
, fk.*

 from sys.foreign_keys FK

left join sys.objects a
on a.object_id = FK.object_id
	or a.object_id = FK.parent_object_id

inner join sys.columns c
on c.object_id = a.parent_object_id

inner join sys.tables t
on t.object_id = a.parent_object_ID

ORDER BY 3


/*


SELECT * FROM sys.tables ORDER BY 1


select * from sys.objects where name = 'ExamSessionTable'
select * from sys.objects where name = 'UserTable'

select * from sys.sysdepends 
--inner join sys.objects as O
--on O.object_id = sys.sysdepends.id
where depid = '1510296440'
	and id = '905770284'


Select  * 
from sys.objects O
inner join sys.tables T
on T.object_id = O.object_id
inner join sys.columns C
on C.object_id = O.object_id

where T.name = 'UserTable'


*/