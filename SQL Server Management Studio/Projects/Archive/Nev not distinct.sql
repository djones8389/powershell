select ID
	, Name
	, ProjectDefaultXml 
	, s.st.value('@id', 'int') as ID2
	into #ProjectStatusTable	
	from ProjectListTable 
	
	cross apply ProjectDefaultXml.nodes('/Defaults/StatusList/Status') s(st)
		order by ID
		;

select * from #ProjectStatusTable a where exists (
		select ID2 from #ProjectStatusTable b
		where a.ID2 = b.ID2
			and a.ID = b.ID
			group by ID2
		having COUNT(*) > 1
		)
		
drop table #ProjectStatusTable;