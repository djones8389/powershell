DECLARE IDs CURSOR FOR
	select ID from IB3QualificationLookup
	where ID IN  ('159','160','161','162','163','164','165','166','167','168','169','170','171','172','173','174','175','176','177','184','185','186','187','188','189','190','192')


OPEN IDs;


DECLARE @ID int;

FETCH NEXT FROM IDs INTO @ID;

WHILE @@FETCH_STATUS = 0
BEGIN



DECLARE @QualificationID int = @ID

select UserID
	into #OmitThese
	from UserQualificationsTable
		where QualificationID = @QualificationID;
		
		
create table #myTemp
(
	userID int,
	QualificationID int,
	CQN int null
);


		
		
with cte as (
		
select ID--, Forename, Surname, CandidateRef
	 from UserTable 
	 
where CandidateRef = ''
	and Retired = 0
		and ID not in (select UserID from #OmitThese)
)

--select * from cte

insert into #myTemp(userID, QualificationID)

SELECT distinct userID
      , @QualificationID--QualificationID
     -- , 'NULL'
      
  from cte
  
  left join UserQualificationsTable
  on UserQualificationsTable.UserID = cte.ID;
  
  
--select * from #myTemp order by userID

--begin tran

merge into UserQualificationsTable as TGT
using #myTemp as SRC
	on 1 = 2
when not matched then
insert(UserID, QualificationID)
values(SRC.UserID, SRC.QualificationID);

--rollback

drop table #myTemp;
drop table #OmitThese;

FETCH NEXT FROM IDs INTO @ID;
END

CLOSE IDs;
DEALLOCATE IDs;
