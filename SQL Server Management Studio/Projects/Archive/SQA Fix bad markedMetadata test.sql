SELECT * FROM #XMLTEST


UPDATE ExamSessionTable
set myXML = REPLACE(CAST(myXML as nvarchar(MAX)), N'plan1</value>',N'1</value>');
where examState = 99

UPDATE ExamSessionTable
set myXML = REPLACE(CAST(myXML as nvarchar(MAX)), N'plan2</value>',N'2</value>');
where examState = 99

UPDATE ExamSessionTable
set myXML = REPLACE(CAST(myXML as nvarchar(MAX)), N'plan3</value>',N'3</value>');
where examState = 99

UPDATE ExamSessionTable
set myXML = REPLACE(CAST(myXML as nvarchar(MAX)), N'plan4</value>',N'4</value>');
where examState = 99


UPDATE ExamSessionTable
set myXML = REPLACE(CAST(myXML as nvarchar(MAX)), N'dev1</value>',N'1</value>');
where examState = 99

UPDATE ExamSessionTable
set myXML = REPLACE(CAST(myXML as nvarchar(MAX)), N'dev2</value>',N'2</value>');
where examState = 99

UPDATE ExamSessionTable
set myXML = REPLACE(CAST(myXML as nvarchar(MAX)), N'dev3</value>',N'3</value>');
where examState = 99

UPDATE ExamSessionTable
set myXML = REPLACE(CAST(myXML as nvarchar(MAX)), N'dev4</value>',N'4</value>');
where examState = 99



UPDATE ExamSessionTable
set myXML = REPLACE(CAST(myXML as nvarchar(MAX)), N'eval1</value>',N'1</value>');
where examState = 99

UPDATE ExamSessionTable
set myXML = REPLACE(CAST(myXML as nvarchar(MAX)), N'eval2</value>',N'2</value>');
where examState = 99

UPDATE ExamSessionTable
set myXML = REPLACE(CAST(myXML as nvarchar(MAX)), N'eval3</value>',N'3</value>');
where examState = 99

UPDATE ExamSessionTable
set myXML = REPLACE(CAST(myXML as nvarchar(MAX)), N'eval4</value>',N'4</value>');
where examState = 99


--create table #XMLTEST

--(
--	myXML xml
--)

--insert into #XMLTEST
--values('<assessmentDetails>
--  <assessmentID>3542</assessmentID>
--  <qualificationID>329</qualificationID>
--  <qualificationName>HNC Care and Administration Practice</qualificationName>
--  <qualificationReference>hncap</qualificationReference>
--  <assessmentGroupName>Graded Unit 1: Planning</assessmentGroupName>
--  <assessmentGroupID>2716</assessmentGroupID>
--  <assessmentGroupReference>gu1plan</assessmentGroupReference>
--  <assessmentName>planning</assessmentName>
--  <validFromDate>13 May 2014</validFromDate>
--  <expiryDate>13 May 2024</expiryDate>
--  <startTime>00:00:00</startTime>
--  <endTime>23:59:59</endTime>
--  <duration>0</duration>
--  <defaultDuration>0</defaultDuration>
--  <scheduledDuration>
--    <value>0</value>
--    <reason></reason>
--  </scheduledDuration>
--  <sRBonus>0</sRBonus>
--  <sRBonusMaximum>0</sRBonusMaximum>
--  <externalReference>plan</externalReference>
--  <passLevelValue>0</passLevelValue>
--  <passLevelType>0</passLevelType>
--  <status>2</status>
--  <testFeedbackType>
--    <passFail>1</passFail>
--    <percentageMark>1</percentageMark>
--    <allowSummaryFeedback>1</allowSummaryFeedback>
--    <summaryFeedbackType>1</summaryFeedbackType>
--    <itemSummary>0</itemSummary>
--    <itemReview>0</itemReview>
--    <itemReviewCorrectAnswer>0</itemReviewCorrectAnswer>
--    <itemFeedback>0</itemFeedback>
--    <printableSummary>1</printableSummary>
--    <candidateDetails>1</candidateDetails>
--    <feedbackByReference>1</feedbackByReference>
--    <allowFeedbackDuringExam>0</allowFeedbackDuringExam>
--    <allowFeedbackDuringSummary>0</allowFeedbackDuringSummary>
--  </testFeedbackType>
--  <itemFeedback>0</itemFeedback>
--  <allowCalculator>0</allowCalculator>
--  <lastInUseDate>13 May 2014 11:47:09</lastInUseDate>
--  <completedScriptReview>0</completedScriptReview>
--  <assessment currentSection="1" totalTime="0" currentTime="0">
--    <intro id="0" name="Introduction" currentItem="">
--      <item id="1129P1137" name="planning" totalMark="0" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="4" />
--    </intro>
--    <outro id="0" name="Finish" currentItem="">
--      <item id="1129P1138" name="planning" totalMark="0" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="2" />
--    </outro>
--    <tools id="0" name="Tools" currentItem="">
--      <item id="1129P1168" name="planq1" toolName="planq1" totalMark="0" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="7" />
--      <item id="1129P1169" name="planq2" toolName="planq2" totalMark="0" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="7" />
--      <item id="1129P1170" name="planq3" toolName="planq3" totalMark="0" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="7" />
--      <item id="1129P1171" name="planq4" toolName="planq4" totalMark="0" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="7" />
--      <item id="1129P1172" name="planning" toolName="planning" totalMark="0" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="7" />
--    </tools>
--    <section id="1" name="Planning" passLevelValue="0" passLevelType="0" itemsToMark="0" currentItem="1129P1164" fixed="1">
--      <item id="1129P1166" name="planintro" totalMark="0" version="45" markingType="2" markingState="0" userMark="-1" markerUserMark="" userAttempted="0" viewingTime="9169" flagged="0" type="1" Topic="Introduction" OutcomeNumber="Planning" quT="11" />
--      <item id="1129P1142" name="start1" totalMark="0" version="16" markingType="2" markingState="0" userMark="-1" markerUserMark="" userAttempted="0" viewingTime="18353" flagged="0" type="1" quT="11" />
--      <item id="1129P1158" name="planq1" totalMark="10" version="46" markingType="1" markingState="1" userMark="0" markerUserMark="1" userAttempted="1" viewingTime="4072" flagged="0" type="1" Topic="Understanding of the patient/client�s current health and well-being." OutcomeNumber="Planning" quT="11" MarkedMetadata_plan1="10" />
--      <item id="1129P1160" name="planq2" totalMark="6" version="67" markingType="1" markingState="1" userMark="0" markerUserMark="4" userAttempted="1" viewingTime="4096" flagged="0" type="1" Topic="Assessment of the patient/client�s current psychological needs" OutcomeNumber="Planning" quT="11" MarkedMetadata_plan2="10" MarkedMetadata_plan1="10" MarkedMetadata_plan3="5" MarkedMetadata_plan4="5" />
--      <item id="1129P1162" name="planq3" totalMark="10" version="61" markingType="1" markingState="1" userMark="0" markerUserMark="4" userAttempted="1" viewingTime="4049" flagged="0" type="1" Topic="Appropriateness of the selected activity" OutcomeNumber="Planning" quT="11" MarkedMetadata_plan2="10" MarkedMetadata_plan1="10" MarkedMetadata_plan3="5" MarkedMetadata_plan4="5" />
--      <item id="1129P1164" name="planq4" totalMark="4" version="50" markingType="1" markingState="1" userMark="0" markerUserMark="1" userAttempted="1" viewingTime="5093" flagged="0" type="1" Topic="Understanding of the patient/client�s current health and well-being" OutcomeNumber="Planning" quT="11" MarkedMetadata_plan4="4" />
--    </section>
--  </assessment>
--  <isValid>1</isValid>
--  <isPrintable>0</isPrintable>
--  <qualityReview>0</qualityReview>
--  <numberOfGenerations>1</numberOfGenerations>
--  <requiresInvigilation>0</requiresInvigilation>
--  <advanceContentDownloadTimespan>120</advanceContentDownloadTimespan>
--  <automaticVerification>1</automaticVerification>
--  <offlineMode>0</offlineMode>
--  <requiresValidation>0</requiresValidation>
--  <requiresSecureClient>0</requiresSecureClient>
--  <examType>0</examType>
--  <advanceDownload>0</advanceDownload>
--  <gradeBoundaryData>
--    <gradeBoundaries passLevelType="0" adjustedGradeBoundaries="0">
--      <grade modifier="lt" value="0" description="Fail" />
--      <grade modifier="gt" value="0" description="Pass" />
--    </gradeBoundaries>
--  </gradeBoundaryData>
--  <autoViewExam>1</autoViewExam>
--  <autoProgressItems>0</autoProgressItems>
--  <confirmationText>
--    <confirmationText>By ticking this box you confirm that the above details are correct and you will abide by the specified conditions of assessment.</confirmationText>
--  </confirmationText>
--  <requiresConfirmationCheck>1</requiresConfirmationCheck>
--  <allowCloseWithoutSubmit>1</allowCloseWithoutSubmit>
--  <useSecureMarker>0</useSecureMarker>
--  <annotationVersion>0</annotationVersion>
--  <allowPackagingDelivery>1</allowPackagingDelivery>
--  <scoreBoundaryData>
--    <scoreBoundaries showScoreBoundaryColumn="0">
--      <score modifier="lt" value="40" description="Not met" higherBoundarySet="1" />
--      <score modifier="lt" value="40" description="Not met" higherBoundarySet="0" />
--      <score modifier="gt" value="40" description="Close to met" higherBoundarySet="1" />
--      <score modifier="gt" value="40" description="Met" higherBoundarySet="0" />
--      <score modifier="gt" value="45" description="Met" higherBoundarySet="1" />
--      <score modifier="gt" value="80" description="Exceeded" higherBoundarySet="1" />
--    </scoreBoundaries>
--  </scoreBoundaryData>
--  <showCandidateReportResultColumn>0</showCandidateReportResultColumn>
--  <showCandidateReportPercentageColumn>1</showCandidateReportPercentageColumn>
--  <certifiedAccessible>0</certifiedAccessible>
--  <markingProgressVisible>1</markingProgressVisible>
--  <markingProgressMode>1</markingProgressMode>
--  <secureClientOperationMode>0</secureClientOperationMode>
--  <examDeliverySystemStyleType>delivery</examDeliverySystemStyleType>
--  <markingAutoVoidPeriod>365</markingAutoVoidPeriod>
--  <showPageRequiresScrollingAlert>0</showPageRequiresScrollingAlert>
--  <project ID="891" />
--  <project ID="1129">
--    <value display="Understanding of the patient or clients current health and well being">plan1</value>
--    <value display="Assessment of the patient or clients current psychological needs">plan2</value>
--    <value display="Appropriateness of the selected activity">plan3</value>
--    <value display="Relevant legislation discussion of safe practice and issues around confidentiality">plan4</value>
--    <value display="Application of practice and perspectives to the fulfilment of aims and objectives within the plan">dev1</value>
--    <value display="Activity has been followed according to the plan">dev2</value>
--    <value display="Demonstrate the ability to manage materials and resources">dev3</value>
--    <value display="Written verification from the placement mentor that the activity was carried out as reported">dev4</value>
--  </project>
--  <project ID="1130">
--    <value display="Able to review the quality of their own work with some reflection shown">eval1</value>
--    <value display="Discussion of the strengths and weaknesses of the original plan">eval2</value>
--    <value display="Review of the project including any modifications improvements recommendations for future activities">eval3</value>
--    <value display="Evaluation of professional practice measured against the appropriate policies and standards">eval4</value>
--  </project>
--</assessmentDetails>')