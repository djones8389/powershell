--CF Report--


with cte as (

	select Keycode 
		from BritishCouncil_SecureAssess_LIVE..WAREHOUSE_ExamSessionTable as WEST
			Inner join BritishCouncil_SecureAssess_LIVE..WAREHOUSE_ScheduledExamsTable as WSCET
			on WSCET.ID = WEST.WAREHOUSEScheduledExamID
		
		where CreatedDateTime > '17 Nov 2014'	

)

select distinct
	SPC.IsVoided
	, P.Name
	, FirstName
	, LastName
	, SurpassCandidateRef
	, PackageScore
	--, WESTS.centreName
	, SP.CenterName
	, SPC.DateCompleted
	--,*
from ScheduledPackageCandidates as SPC

	left join ScheduledPackageCandidateExams as SPCE
	on SPCE.Candidate_ScheduledPackageCandidateId = SPC.ScheduledPackageCandidateId

	left join PackageExams as PE
	on PE.PackageExamId = SPCE.PackageExamId
	
	left join Packages as P
	on P.PackageId = PE.PackageId	

	inner join ScheduledPackages as SP
	on SP.ScheduledPackageId = SPC.ScheduledPackage_ScheduledPackageId

	--inner join BritishCouncil_SecureAssess_LIVE..WAREHOUSE_ExamSessionTable as WEST
	--on WEST.KeyCode = SPCE.ScheduledExamRef
	
	--Inner join BritishCouncil_SecureAssess_LIVE..WAREHOUSE_ScheduledExamsTable as WSCET
	--on WSCET.ID = WEST.WAREHOUSEScheduledExamID

--where SPC.DateCompleted >= '01 Sep 2014' 
--	and SPC.DateCompleted <= '30 Sep 2014'
--	order by SPC.DateCompleted;
where ScheduledExamRef in (select Keycode from cte)


