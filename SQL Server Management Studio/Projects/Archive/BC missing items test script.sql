;with cte as
	(
	SELECT distinct ExternalSessionID
		FROM Candidateexamversions 
			INNER JOIN CandidateResponses
			ON CandidateExamVersionID = Candidateexamversions.ID
			INNER JOIN UniqueResponses
			ON UniqueResponseID = UniqueResponses.ID
	where CONVERT(NVARCHAR(MAX), responseData) = ''
	)

Select WEST.ID, west.KeyCode, wesirt.ItemID, WESIRT.ItemResponseData as [SAResponses], UniqueResponses.responseData as [SMResponses]
	 from BritishCouncil_SecureAssess_LIVE..Warehouse_ExamSessionTable as WEST

	left join BritishCouncil_SecureAssess_LIVE..Warehouse_ExamSessionItemResponseTable as WESIRT on WESIRT.WarehouseExamSessionID = WEST.ID

	inner join CandidateExamVersions as CEV on CEV.ExternalSessionID = WEST.ID
	
	INNER JOIN CandidateResponses ON CandidateExamVersionID = CEV.ID
	
	INNER JOIN UniqueResponses ON UniqueResponseID = UniqueResponses.ID
	
	left outer Join Items as I on I.ID = UniqueResponses.itemId --and I.ExternalItemID <> WESIRT.ItemID
		

where WEST.ID IN (Select ExternalSessionID from cte) --and I.ExternalItemID <> WESIRT.ItemID
	--and CONVERT(NVARCHAR(MAX), ItemResponseData) <> CONVERT(NVARCHAR(MAX), responseData)
	
	
