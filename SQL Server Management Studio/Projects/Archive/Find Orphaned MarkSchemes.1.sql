USE UAT_SQA_ContentProducer

IF OBJECT_ID('tempdb..#Temp') IS NOT NULL DROP TABLE #Temp;
IF OBJECT_ID('tempdb..#ProjectStructure') IS NOT NULL DROP TABLE #ProjectStructure;

select  
	ProjectListTable.id as ProjectID
	, pro.pag.value('@ID','nvarchar(100)') as PageID
	, Name
	, pro.pag.value('@markingSchemeFor','nvarchar(100)')  as markingSchemeFor
	into #Temp
from ProjectListTable (NOLOCK)

	cross apply ProjectStructureXml.nodes('Pro/*[local-name(.)!="Rec"]//Pag') pro(pag);
		
	--Find your orphaned users

DECLARE @HoldingTable TABLE (ProjectID int, PageID int)	
INSERT INTO @HoldingTable(ProjectID, PageID)

	select ProjectID, markingSchemeFor from #Temp
	EXCEPT
	select ProjectID, PageID from #Temp

DROP TABLE #Temp;
	
	--These are the affected pages
	
SELECT  ProjectID
	 , (select Name from ProjectListTable where ID = ProjectID) as ProjectName
	 , (select case OutputMedium when '0' then 'Computer' else 'Paper' end as ProjectType from ProjectListTable where ID = ProjectID) as ProjectType
	 , PageID		
FROM @HoldingTable
	 
	left join PageTable as PT
	on PT.ID = cast(ProjectID as nvarchar(6)) + 'P' + cast(PageID as nvarchar(6))
	
where PageID is not null		
	and PageID != -1
	and PT.ID IS NULL
	
	--Write ProjectStructureXml into TempTable for the cursor
	
select ID, ProjectStructureXml							
	into #ProjectStructure
	from ProjectListTable
 	where ID IN (
		select distinct projectid  from @HoldingTable
			where PageID is not null
			and PageID != -1
	)

select * from #ProjectStructure;  --Store for backup purposes

	--Create a cursor and narrow it down to the pages we need
	
DECLARE DeleteProjectStructure 
CURSOR FOR 

SELECT  ProjectID, PageID		
FROM @HoldingTable
	 
	left join PageTable as PT
	on PT.ID = cast(ProjectID as nvarchar(6)) + 'P' + cast(PageID as nvarchar(6))
	
where PageID is not null		
	and PageID != -1
	and PT.ID IS NULL


Open DeleteProjectStructure;	

DECLARE @ProjectID int, @PageID int;

FETCH NEXT FROM DeleteProjectStructure INTO @ProjectID, @PageID;

WHILE @@FETCH_STATUS = 0

BEGIN

--PRINT 'Update #ProjectStructure set ProjectStructureXml.modify(''delete(/Pro[@ID=sql:variable("' + cast(@ProjectID as nvarchar(MAX)) +'")] [@markingSchemeFor=sql:variable("' + cast(@PAGEID as nvarchar(MAX)) + '")])'') where ID = ' + cast(@ProjectID as nvarchar(MAX))
Update #ProjectStructure
set ProjectStructureXml.modify('delete(/Pro//Pag[@markingSchemeFor=sql:variable("@PageID")])')
where ID = @ProjectID

FETCH NEXT FROM DeleteProjectStructure INTO @ProjectID, @PageID;

END

CLOSE DeleteProjectStructure;
DEALLOCATE DeleteProjectStructure;

select * from #ProjectStructure; --Store for comparing purposes

begin tran
update ProjectListTable
set ProjectStructureXml = b.ProjectStructureXml
from ProjectListTable a
inner join #ProjectStructure b
on b.ID = a.ID
rollback


DROP TABLE #ProjectStructure;
