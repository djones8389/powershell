USE SADB
GO
/****** Object:  StoredProcedure [dbo].[sa_SECUREASSESSINTEGRATION_getCandidateResults_sp]    Script Date: 11/06/2013 10:42:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--ALTER PROCEDURE [dbo].[sa_SECUREASSESSINTEGRATION_getCandidateResults_sp]    
DECLARE
 @startDate      nvarchar(max) = N'10/01/2013',    
 @endDate      nvarchar(max) = N'10/02/2013',    
 @examReference     nvarchar(max) =  N'*',    
 @centreReference    nvarchar(max)= N'*',    
 @useSubmissionTime    bit = 0,    
 @pageNumber      decimal = 100,  
 @userID       decimal = 1084,
 @warehouseStates	nvarchar(20)   = 1


--WITH_ENCRYPTION_REPLACE_ME_FOR_LOCAL--    
---AS    
BEGIN    

 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
 SET DATEFORMAT dmy;    

 DECLARE @errorReturnString nvarchar(max)    
 DECLARE @errorNum  nvarchar(100)    
 DECLARE @errorMess nvarchar(max)    


 DECLARE @pageSize   decimal    
 SET @pageSize = 100    

 DECLARE @mySelect nvarchar(max)    
 SET @mySelect = null    


 DECLARE @myWhere nvarchar(max)    
 SET @myWhere = null    
 DECLARE @myRest nvarchar(max)    
 SET @myRest = null    
 DECLARE @myFinalQuery nvarchar(max)    
 SET @myFinalQuery  = null    





  BEGIN TRY    

   SET @mySelect = '    
    WITH MyCTE AS    
    (    

    SELECT x.* FROM (    
     SELECT    
      ROW_NUMBER() OVER(ORDER BY [WAREHOUSE_ExamSessionTable].[warehouseTime]) AS ''RowNumber'',    
      [WAREHOUSE_ExamSessionTable].[ID] AS ''ID'',    
      [WAREHOUSE_ScheduledExamsTable].[ExamID] AS ''ExamID'',    
      [WAREHOUSE_ExamSessionTable].[warehouseTime] AS ''WarehousedTime'',    
      Convert(nvarchar(max),[WAREHOUSE_ExamSessionTable].[StructureXml].query(''data(//assessmentGroupReference/text())''))  AS ''ExamRef'', 
      Convert(nvarchar(max),[WAREHOUSE_ExamSessionTable].[StructureXml].query(''data(//externalReference/text())'')) AS ''ExamVersionRef'',          
      [WAREHOUSE_ScheduledExamsTable].[examVersionId] AS ''ExamVersionID'',    
      Convert(nvarchar(max),[WAREHOUSE_ExamSessionTable].[StructureXml].query(''data(//qualificationReference/text())'')) AS ''qualificationReference'',        
      Convert(nvarchar(max),[WAREHOUSE_ExamSessionTable].[StructureXml].query(''data(//validFromDate/text())'')) AS ''start'',        
      Convert(nvarchar(max),[WAREHOUSE_ExamSessionTable].[StructureXml].query(''data(//expiryDate/text())'')) AS ''end'',      
      Convert(nvarchar(max),[WAREHOUSE_ExamSessionTable].[resultData].query(''data(//exam/@totalTimeSpent)'')) AS ''ActualDuration'',     
      Convert(nvarchar(max),[WAREHOUSE_ExamSessionTable].[StructureXml].query(''data(//scheduledDuration/value/text())'')) AS ''allowedDuration'',        
      Convert(nvarchar(max),[WAREHOUSE_ExamSessionTable].[StructureXml].query(''data(//defaultDuration/text())'')) AS ''defaultDuration'',        
      Convert(nvarchar(max),[WAREHOUSE_ExamSessionTable].[StructureXml].query(''data(//scheduledDuration/reason/text())'')) AS ''reason'',            
      [WAREHOUSE_CentreTable].[CentreID] AS ''CentreId'',    
      [WAREHOUSE_CentreTable].[CentreName] AS ''CentreName'',    
      [WAREHOUSE_UserTable].[Userid] AS ''CandidateID'',    
      [WAREHOUSE_UserTable].[Gender] AS ''CandidateGender'',    
      [WAREHOUSE_UserTable].[DOB] AS ''CandidateDOB'',    
      [WAREHOUSE_UserTable].[EthnicOrigin] AS ''CandidateEthnicOriginID'',    
      [WAREHOUSE_UserTable].[CandidateRef] AS ''CandidateRef'',    
      [WAREHOUSE_UserTable].[Surname] AS ''CandidateSurname'',    
      [WAREHOUSE_UserTable].[Forename] AS ''CandidateForename'',    
      Convert(nvarchar(max),[WAREHOUSE_ExamSessionTable].[resultData].query(''data(//exam/@userMark)'')) AS ''CandidateMark'',    
      Convert(nvarchar(max),[WAREHOUSE_ExamSessionTable].[resultData].query(''data(//exam/@passMark)'')) AS ''PassMark'',    
      Convert(nvarchar(max),[WAREHOUSE_ExamSessionTable].[resultData].query(''data(//exam/@totalMark)'')) AS ''TotalMark'',    
      [WAREHOUSE_ExamSessionTable].[PreviousExamState] As ''PreviousExamState'',    
      [VoidJustificationLookupTable].[name] AS ''VoidReason'',    
      Convert(nvarchar(max),[WAREHOUSE_ExamSessionTable].[resultData].query(''data(//exam/@passValue)'')) AS ''PassValue'',    
      Convert(nvarchar(max),[WAREHOUSE_ExamSessionTable].[StructureXml].query(''data(//automaticVerification/text())'')) AS ''AutoVerify'',    
      Convert(nvarchar(max),[WAREHOUSE_ExamSessionTable].[ExamStateChangeAuditXml].query(''data(//stateChange[newStateID=12]/information/stateChangeInformation/type/text())'')) AS ''resultSampled'',    
      Convert(nvarchar(max),[WAREHOUSE_ExamSessionTable].[ExamStateChangeAuditXml].query(''data(//stateChange[newStateID=6][1]/changeDate/text())'')) AS ''started'',    
      Convert(nvarchar(max),[WAREHOUSE_ExamSessionTable].[ExamStateChangeAuditXml].query(''data(//stateChange[newStateID=9 or newStateID=10][last()]/changeDate/text())'')) AS ''submitted'',    
      Convert(xml,Convert(nvarchar(max),''<sectionDataRaw>'' + Convert(nvarchar(max),[WAREHOUSE_ExamSessionTable].[resultData].query(''//section'')) + ''</sectionDataRaw>'')) AS ''sectionDataRaw'' ,    
      dbo.fn_getMarkers([WAREHOUSE_ExamSessionTable].[ID]) AS ''Marker'',           
      [WAREHOUSE_ScheduledExamsTable].[ScheduledStartDateTime] AS ''WindowsStartTime'',     
      [WAREHOUSE_CentreTable].[CentreCode] AS ''CentreRef'',    
      Convert(nvarchar(max),[WAREHOUSE_ExamSessionTable].[resultData].query(''data(//exam/@originalGrade)'')) AS ''Grade'' ,   
	  [WAREHOUSE_ExamSessionTable].[keyCode] AS ''keyCode'' ,  
      [WAREHOUSE_UserTable].[ULN] AS ''ULN'',
      (SELECT CQN from [dbo].[UserQualificationsTable] WHERE UserID = [WAREHOUSE_UserTable].[Userid] AND QualificationID = StructureXml.value(''(//qualificationID)[1]'',''int'')) AS ''CQN'',
      Convert(nvarchar(max),[WAREHOUSE_ExamSessionTable].[resultData].query(''data(//exam/@passType)'')) AS ''PassType'',
      [WAREHOUSE_ExamSessionTable].[WarehouseExamState] AS ''WarehouseExamState''

      FROM [dbo].[WAREHOUSE_ExamSessionTable]    

      INNER JOIN [dbo].[WAREHOUSE_ScheduledExamsTable]    
       ON [dbo].[WAREHOUSE_ScheduledExamsTable].[ID] = [WAREHOUSE_ExamSessionTable].[WAREHOUSEScheduledExamID]    
      INNER JOIN [dbo].[WAREHOUSE_CentreTable]    
       ON [dbo].[WAREHOUSE_CentreTable].[ID] = [WAREHOUSE_ScheduledExamsTable].[WAREHOUSECentreID]    
      INNER JOIN [dbo].[WAREHOUSE_UserTable]    
       ON [dbo].[WAREHOUSE_UserTable].[ID] = [WAREHOUSE_ExamSessionTable].[WAREHOUSEUserID]                            
      LEFT JOIN [dbo].[VoidJustificationLookupTable]    
       ON Convert(nvarchar(max),[WAREHOUSE_ExamSessionTable].[ExamStateChangeAuditXml].query(''data(//stateChange[newStateID=10][1]/information/stateChangeInformation/reason/text())'')) = [VoidJustificationLookupTable].[ID]    

     '    



  DECLARE @myDateClause nvarchar(max)  
  DECLARE @myTestCentreClause nvarchar(max)  
  DECLARE @myPracticeTestClause nvarchar(max)  
  DECLARE @myCentreRefClause nvarchar(max)  
  DECLARE @myExamRefClause nvarchar(max)  
  DECLARE @myOmrClause nvarchar(max)  
  DECLARE @myPermissionClause nvarchar(max)  

  IF (@useSubmissionTime = 0)  
   BEGIN  
    SET @myDateClause = '(  
           [WAREHOUSE_ExamSessionTable].[warehouseTime] >= ''' +@startDate+ '''  
           AND   
           [WAREHOUSE_ExamSessionTable].[warehouseTime] <= ''' +@endDate+ '''  
           )'  
   END  
  ELSE  
   BEGIN  
    SET @myDateClause = '(  
           [WAREHOUSE_ExamSessionTable].[completionDate] >= ''' +@startDate+ '''  
           AND   
           [WAREHOUSE_ExamSessionTable].[completionDate] <= ''' +@endDate+ '''  
           )'  
   END  

  SET @myTestCentreClause = 'AND ([WAREHOUSE_CentreTable].[CentreName] NOT LIKE ''%BTL Test Centre%'')'  

  SET @myPracticeTestClause = 'AND   
         (  
          Convert  
          (  
           nvarchar(max),  
           [WAREHOUSE_ExamSessionTable].[StructureXml].query(''data(//assessmentGroupReference/text())'')  
          )   
          NOT LIKE ''%/PRACTICE%''  
         )'  

  IF(@centreReference = '')  
   BEGIN  
    SET @myCentreRefClause = ''  
   END  
  ELSE  
   BEGIN  
    SET @myCentreRefClause = 'AND ([WAREHOUSE_CentreTable].[CentreCode]  = ''' + convert(nvarchar(max),@centreReference) +''')'  
   END  

  IF(@examReference = '')  
   BEGIN  
    SET @myExamRefClause = ''  
   END  
  ELSE  
   BEGIN  
    SET @myExamRefClause = 'AND (Convert(nvarchar(max),[WAREHOUSE_ExamSessionTable].[StructureXml].query(''data(//assessmentGroupReference/text())''))  = ''' + convert(nvarchar(max),@examReference) +''')'  
   END  

  SET @myOmrClause = 'AND (not WAREHOUSE_ExamSessionTable.previousexamstate = 17)'  

  SET @myPermissionClause = ' AND   
         (  
          (  
           '+convert(nvarchar(max),@userID)+ ' = 1   
           OR   
           (  
            [WAREHOUSE_CentreTable].[CentreID] IN   
            (  
             Select   
              [AssignedUserRolesTable].CentreID   
             FROM AssignedUserRolesTable   
             INNER JOIN RolePermissionsTable   
              ON AssignedUserRolesTable.RoleID = RolePermissionsTable.RoleID  
             WHERE       
             (  
              (   
               RolePermissionsTable.PermissionID = 29)   
               AND    
               [AssignedUserRolesTable].UserID = '+convert(nvarchar(max),@userID)+ '  
              )  
              AND   
              (  
               Convert  
               (  
                nvarchar(max),  
                [WAREHOUSE_ExamSessionTable].[StructureXml].query(''data(//qualificationID/text())''  
               )  
              )  
              IN   
              (  
               Select   
                QualificationID   
               From UserQualificationsTable   
               Where UserID = '+convert(nvarchar(max),@userID)+ '  
              )  
             )  
            )  
           )  
          )  
          OR  
          (  
           EXISTS   
           (  
            SELECT   
             UserID   
            FROM AssignedUserRolesTable   
            INNER JOIN RolePermissionsTable   
             ON AssignedUserRolesTable.RoleID = RolePermissionsTable.RoleID  
            WHERE   
             PermissionID = 29   
             AND   
             UserID = '+convert(nvarchar(max),@userID)+ '   
             AND   
             CentreID = 1  
           )  
          )  
         )'  
  DECLARE @myWarehouseStates	nvarchar(200)
  SET @myWarehouseStates = ' AND [WAREHOUSE_ExamSessionTable].WarehouseExamState IN (' + @warehouseStates + ')'
  SET @myWhere = 'WHERE ' + @myDateClause + @myTestCentreClause + @myPracticeTestClause + @myCentreRefClause + @myExamRefClause + @myOmrClause + @myPermissionClause + @myWarehouseStates

  Set @myRest = '  
       ) As X  
      )  

      SELECT  
       *,   
       (SELECT COUNT(RowNumber) FROM MyCTE) As ''TotalResultsAvailable'',  
       '+ convert(nvarchar(max),@pageSize) + ' AS ''PageSize''   
      FROM MyCTE   
      WHERE   
       RowNumber  > (' + convert(nvarchar(max),@pageSize) + ' * (' + convert(nvarchar(max), @pageNumber) + ' - 1))  
       AND   
       RowNumber  <= (' + convert(nvarchar(max),@pageSize) + ' *' +  convert(nvarchar(max), @pageNumber) + ')  
      ORDER BY   
        [RowNumber] ASC  
       ,[ExamVersionRef] ASC  
       ,[CentreId] ASC  
       ,[CandidateSurname] ASC  
       ,[ID] ASC  
    '  

  Set @myFinalQuery =  @mySelect + @myWhere + @myRest    

    SELECT @myFinalQuery     as test
    exec sp_executesql @myFinalQuery, N'@startDate nvarchar(max), @endDate nvarchar(max), @centreReference nvarchar(max), @examReference nvarchar(max),    
      @pageSize nvarchar(max), @pageNumber decimal',     
     @startDate, @endDate, @centreReference, @examReference, @pageSize, @pageNumber    


 END TRY    
 BEGIN CATCH    
  DECLARE @ErrorMessage NVARCHAR(4000);    
  DECLARE @ErrorSeverity INT;    

  SELECT @ErrorMessage = ERROR_MESSAGE(),    
      @ErrorSeverity = ERROR_SEVERITY();    

  -- Use RAISERROR inside the CATCH block to return     
  -- error information about the original error that     
  -- caused execution to jump to the CATCH block.    
  RAISERROR (@ErrorMessage, -- Message text.    
       @ErrorSeverity, -- Severity.    
       1 -- State.    
       );    
 END CATCH    

END    


