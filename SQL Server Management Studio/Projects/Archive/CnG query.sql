--select top 5 
--structurexml 
--,structurexml.query('data(assessmentDetails/qualificationName)[1]') as 'QualName'
--,structurexml.query('data(assessmentDetails/assessmentName)[1]') as 'ExamName'
--from ExamSessionTable


SELECT 

  EST.ID
, EST.examState 
, EST.KeyCode
, structurexml.query('data(assessmentDetails/qualificationName)[1]') as 'Str_QualificationName'
, IB.QualificationName
, SCET.qualificationId
, structurexml.query('data(assessmentDetails/assessmentGroupName)[1]') as 'Str_ExamName'
, SCET.examName
, SCET.ExamID

FROM ExamSessionTable as EST

  Inner Join ScheduledExamsTable as SCET
  on SCET.ID = EST.ScheduledExamID
  
  inner join IB3QualificationLookup as IB
  on IB.ID = SCET.qualificationId

where (qualificationId in (135,334) or ExamID in (2448, 2449))
	