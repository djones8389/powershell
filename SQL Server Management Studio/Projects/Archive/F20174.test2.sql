---177 out of 222

--drop table #MyTempTable;

		select 

				X.name as NullName
			  , S.name as PopulatedName
			  --, S.ProjectId as PopulatedProjectID
			  --, S.ID as PopulatedID
			into #MyTempTable
		from
		(   --Find my null values, for 1 project only, to start with
			select distinct  name
			from ProjectManifestTable 
				where ProjectId = 942 
					and Image is null
		) X

		INNER JOIN ( --Find other instances of these images, where it is not null
			SELECT distinct name
			FROM ProjectManifestTable
			where Image is not null
			
		) S
		ON X.name = S.name;


--	select * from #MyTempTable

	--select NullName
	--	from #MyTempTable	
	--group by NullName

--select * from #MyTempTable

--inner join ProjectManifestTable 
--on ProjectManifestTable.name = #MyTempTable.PopulatedName
	




--select * from #MyTempTable

--inner join ProjectManifestTable 
--on ProjectManifestTable.ID = #MyTempTable.PopulatedID 
--	and ProjectManifestTable.ProjectId = #MyTempTable.PopulatedProjectID;

--; 


--select top 1 image from ProjectManifestTable where name = '03_boxes__scene_1.swf' and Image is not null


begin tran

select  name from ProjectManifestTable where ProjectId = 942 and Image is null;  --(325 row(s) affected)

update ProjectManifestTable
set Image = (select top 1 image from ProjectManifestTable where name in (select PopulatedName from #MyTempTable) and Image is not null)
where ProjectId = 942;

select  name from ProjectManifestTable where ProjectId = 942 and Image is null;

rollback