DECLARE @myKeycode NVARCHAR(10) = 'G28BZNA5'
DECLARE @errorReturnString NVARCHAR(MAX)
DECLARE @successReturnString NVARCHAR(MAX)
DECLARE @startProcess NVARCHAR(MAX)


create table #tempStorage (

	ESID INT,
	Keycode NVARCHAR(10)
)

insert into #tempStorage
select id
	, keycode 
from ExamSessionTable
where KeyCode =  @myKeycode


DECLARE @tableVar AS TABLE (

	id INT
	,examstate INT
	,keycode NVARCHAR(10)
	)

INSERT INTO @tableVar
SELECT id
	,examstate
	,ExamSessionTable.keycode
FROM ExamSessionTable
	inner join #tempStorage
	on #tempStorage.ESID = ID
WHERE id = ESID


--1)  Check if it's in Warehouse, if so, we can proceed.  If not,  throw an error--
IF EXISTS (
		SELECT ExamsessionID
		FROM WAREHOUSE_ExamSessionTable
		WHERE ExamsessionID = (
				SELECT ID
				FROM @tableVar
				)	
		)
	
BEGIN
	--2)  Delete from Live_Shredded Table--
	
	set @startProcess = 'This exists in the WarehouseTables, lets start the job'
	select @startProcess
	
	DELETE
	FROM ExamSessionTable_Shredded
	WHERE examsessionid IN (
			SELECT ID
			FROM @tableVar
			);

	--3)  Change the examState back to '16', which is now allowed, with no PK/FK exception--	
	UPDATE ExamSessionTable
	SET previousExamState = EST.examState
		,examState = 16
	FROM ExamSessionTable AS EST
	INNER JOIN @tableVar AS TV ON TV.Id = EST.id
	WHERE TV.Id = EST.id;

	--4)  Delete from WarehouseTables, as it's not needed there anymore--		
	DECLARE @warehouseTable AS TABLE (
		warehouseID INT
		,warehouseExamSessionID INT
		,warehouseKeycode NVARCHAR(10)
		)

	INSERT INTO @warehouseTable
	SELECT id
		,examsessionid
		,WAREHOUSE_ExamSessionTable.keycode
	FROM WAREHOUSE_ExamSessionTable	
		inner join #tempStorage
		on #tempStorage.ESID = ExamSessionID	
	WHERE #tempStorage.ESID = ExamSessionID;


	DELETE
	FROM WAREHOUSE_ExamStateAuditTable
	WHERE WarehouseExamSessionID IN (
			SELECT warehouseID
			FROM @warehouseTable
			);

	DELETE
	FROM WAREHOUSE_ExamSessionTable
	WHERE examsessionid IN (
			SELECT warehouseExamSessionID
			FROM @warehouseTable
			);
	
	
	drop table #tempStorage
	
	SET @successReturnString = 'Moved back into Moderation and removed from Warehouse'
	select @successReturnString
END
ELSE
BEGIN
	drop table #tempStorage
	SET @errorReturnString = 'This was not in the warehouse,  so we have not deleted it from ExamSessionTable_Shredded'
	select @errorReturnString
END
