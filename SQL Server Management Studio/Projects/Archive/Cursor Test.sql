--delete from WAREHOUSE_ExamSessionTable_ShrededItems where examSessionId = 455067;

--select * from WAREHOUSE_ExamSessionTable_ShrededItems where examSessionId = 452077

select ID, KeyCode from  WAREHOUSE_ExamSessionTable where Id in ('399785','399784','399783','399782','399781','399780','399779','399778','399777','399776');
select * from  WAREHOUSE_ExamSessionTable_ShrededItems where examSessionId in ('399785','399784','399783','399782','399781','399780','399779','399778','399777','399776');
delete from  WAREHOUSE_ExamSessionTable_ShrededItems where examSessionId in ('399785','399784','399783','399782','399781','399780','399779','399778','399777','399776');



--declare @myNewWarehouseExamSessionId int = 455067

BEGIN TRANSACTION;
INSERT INTO WAREHOUSE_ExamSessionTable_ShrededItems
		 (	 examSessionId
			,ItemRef
			,ItemName
			,UserMark
			,MarkerUserMark
			,UserAttempted
			,ItemVersion
			,MarkingIgnored
			,MarkedMetadataCount
			,ExamPercentage
			,TotalMark
			,ResponseXML
			,OptionsChosen
			,SelectedCount
			,CorrectAnswerCount
			,itemType
			,FriendItems)
			SELECT	 WAREHOUSE_ExamSessionTable.ID AS [examSessionId]
					,Result.Item.value('@id', 'nvarchar(15)') AS [itemRef]
					,Result.Item.value('@name', 'nvarchar(200)') AS [itemName]
					,Result.Item.value('@userMark', 'decimal(6, 3)') AS [userMark]
					,Result.Item.value('@markerUserMark', 'nvarchar(max)') AS [markerUserMark]
					,Result.Item.value('@userAttempted', 'bit') AS [userAttempted]
					,Result.Item.value('@version', 'int') AS [itemVersion]
					,Result.Item.value('@markingIgnored', 'tinyint') AS [markingIgnored]
					,Result.Item.value('count(mark)', 'int') AS [markedMetadataCount]
					,WAREHOUSE_ExamSessionTable.ResultData.value('(exam/@userPercentage)[1]', 'decimal(6, 3)') AS [examPercentage]
					,Result.Item.value('@totalMark', 'decimal(6, 3)') AS [totalMark]
					,WAREHOUSE_ExamSessionItemResponseTable.ItemResponseData AS [responseXml]
					,CAST(ISNULL(WAREHOUSE_ExamSessionItemResponseTable.ItemResponseData.query('data(p/s/c[@typ = "10"]/i[@sl = "1"]/@ac)'), ' UA ') AS nvarchar(200)) AS [optionsChosen] --Why?
					,WAREHOUSE_ExamSessionItemResponseTable.ItemResponseData.value('count(p/s/c/i[@sl = "1"])', 'int') AS [selectedCount]
					,WAREHOUSE_ExamSessionItemResponseTable.ItemResponseData.value('count(p/s/c/i[@ca = "1"])', 'int') AS [correctAnswerCount]
					,Result.Item.value('@type', 'int') AS ItemType
					,Result.Item.value('@SurpassFriendItems', 'NVARCHAR(MAX)') AS FriendItems
			 FROM	 WAREHOUSE_ExamSessionTable
			 CROSS APPLY WAREHOUSE_ExamSessionTable.ResultData.nodes('exam/section/item') Result(Item)
			 LEFT JOIN WAREHOUSE_ExamSessionItemResponseTable -- we want to include nulls in the the item response table so that non attempted items are included
					 ON WAREHOUSE_ExamSessionItemResponseTable.WAREHOUSEExamSessionID = WAREHOUSE_ExamSessionTable.ID
					AND WAREHOUSE_ExamSessionItemResponseTable.ItemID = Result.Item.value('@id', 'nvarchar(15)')
			WHERE	 WAREHOUSE_ExamSessionTable.ID in ('399785','399784','399783','399782','399781','399780','399779','399778','399777','399776');
COMMIT TRANSACTION;