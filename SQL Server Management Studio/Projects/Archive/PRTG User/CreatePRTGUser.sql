CREATE PROCEDURE ##GenPass(@Len INT = 32, @Password NVARCHAR(MAX) OUTPUT)
AS
BEGIN
SET @Password = (
SELECT XChar AS [text()]
FROM  (
              SELECT TOP(@Len) XChar
              FROM  (
                     VALUES  ('A'),('B'),('C'),('D'),('E'),('F'),('G'),('H'),('I'),('J'),('K'),('L'),('M'),('N'),('O'),('P'),('Q'),('R'),('S'),('T'),('U'),('V'),('W'),('X'),('Y'),('Z'),
                                  ('a'),('b'),('c'),('d'),('e'),('f'),('g'),('h'),('i'),('j'),('k'),('l'),('m'),('n'),('o'),('p'),('q'),('r'),('s'),('t'),('u'),('v'),('w'),('x'),('y'),('z'),
                                  ('0'),('1'),('2'),('3'),('4'),('5'),('6'),('7'),('8'),('9'),
                                  ('!'),('�'),('$'),('%'),('^'),('*'),('('),(')'),('-'),('_'),('='),('+'),('@'),('~'),('#'),('?')
                           ) AS XChars(XChar)
              ORDER BY ABS(CHECKSUM(NEWID()))
              ) AS [XChars]
FOR XML PATH('')
)
END;
GO

DECLARE @DatabaseName nvarchar(100) = ''			 --Leave Blank for ALL,  or specify one DB
DECLARE @Username nvarchar(100) = 'PRTGUser'							--Enter username in here
DECLARE @AccessRequired nvarchar(100) = 'Read'						--NEEDS to be read or write, or it won't work 	

--Email them the credentials--
DECLARE @UsersEmail nvarchar(200) = 'dave.jones@btl.com; andy.brocklesby@btl.com;'
DECLARE @Subject nvarchar(200) =  @AccessRequired + '-Access Staging Credentials to ' + @DatabaseName + ' - ' + @Username;
DECLARE @Body nvarchar(MAX);

DECLARE @ReadRole nvarchar(100) = 'db_datareader'

DECLARE @CreateLogin nvarchar(MAX) = ''
DECLARE @AssignLogin nvarchar(MAX)= ''
DECLARE @AssignRolePermissions nvarchar(MAX) = '' 
DECLARE @CreateAndGrantRole nvarchar(MAX)= ''
DECLARE @ConnectionString nvarchar(MAX) = '' 
DECLARE @MasterAccess nvarchar(MAX) = '' 


DECLARE	@return_value int,
		@Password nvarchar(max)


EXEC	##GenPass @Len = 32, @Password = @Password OUTPUT

DROP PROCEDURE ##GenPass


SELECT @CreateLogin += CHAR(13) + 'EXEC sp_addlogin ' + '''' +  @Username + '''' + ',' + '''' + @Password +  '''' +  ';'


IF LEN(@DatabaseName) > 1
BEGIN							
	(
	SELECT @AssignLogin += CHAR(13) + 'use [' + name + ']; ' + 'CREATE USER ' + @Username + ' FOR LOGIN [' + @Username + ']; '
	from sys.databases
	where name  = @DatabaseName	
		and name like '%secureassess%' 	
	)

	( 	

	SELECT @CreateAndGrantRole += CHAR(13) +
	
	'use [' + a.name + ']; ' 		
	+
	Case When @AccessRequired = 'Read'
			then 			
			' EXEC sp_addrolemember '''  + @ReadRole + ''', ''' + @Username + '''; '					   
			end
	from sys.databases A
		
	where  a.name COLLATE Latin1_General_CI_AS  = @DatabaseName
		and a.name like '%secureassess%' 
	)
			
	(
		SELECT @ConnectionString +=CHAR(13) + 'Server=94.236.6.119; Database=' + name + '; User ID=' + @Username + '; Password=' + @Password +';' 
		from sys.databases
		where name like '%secureassess%' --not in ('master', 'msdb', 'tempdb', 'model')
		and state_desc = 'ONLINE'	
		and name = @DatabaseName
	)
	
	(	
		SELECT @MasterAccess +=CHAR(13) + 'use master;  GRANT VIEW SERVER STATE TO ' + @Username
	)
	

	
END
ELSE IF LEN(@DatabaseName) !> 1

BEGIN							
	(
	SELECT @AssignLogin += CHAR(13) + 'use [' + name + ']; ' + 'CREATE USER ' + @Username + ' FOR LOGIN [' + @Username + ']; '
	from sys.databases
	where name like '%secureassess%' --not in ('master', 'msdb', 'tempdb', 'model')
		and state_desc = 'ONLINE'			
	)
	 
	( 	
		
	SELECT @CreateAndGrantRole += CHAR(13) +	
	
	'use [' + A.name + ']; ' 	+	
	
   Case When @AccessRequired = 'Read' 
			then 			
			' EXEC sp_addrolemember '''  + @ReadRole + ''', ''' + @Username + '''; '					   
	end

	from sys.databases A
			
	where  name like '%secureassess%' --not in ('master', 'msdb', 'tempdb', 'model')
		and state_desc = 'ONLINE'	
	)
	
	
	(
		SELECT @ConnectionString +=CHAR(13) + 'Server=94.236.6.119; Database=' + name + '; User ID=' + @Username + '; Password=' + @Password +';' 
		from sys.databases
		where name like '%secureassess%' --not in ('master', 'msdb', 'tempdb', 'model')
		and state_desc = 'ONLINE'			
	)
	
	
	(	
		SELECT @MasterAccess +=CHAR(13) + 'use master;  GRANT VIEW SERVER STATE TO ' + @Username
	)
	
END

--PRINT(@ConnectionString);
PRINT(@CreateLogin);
PRINT(@AssignLogin);
PRINT(@CreateAndGrantRole);
PRINT(@AssignRolePermissions);
PRINT(@MasterAccess);

SET @Body = 'Server=94.236.6.119; Database=' + @DatabaseName + '; User ID=' + @Username + ';Password=' + @Password +';'


EXECUTE	 msdb.dbo.sp_send_dbmail
		 @profile_name = N'Local SMTP Virtual Server',
		 @recipients = @UsersEmail,
		 @subject = @Subject,
		 @body = @Body,
		 @query_result_no_padding = 1;
		 

EXEC(@CreateLogin);
EXEC(@AssignLogin);
EXEC(@CreateAndGrantRole);
EXEC(@AssignRolePermissions);
EXEC(@MasterAccess);
