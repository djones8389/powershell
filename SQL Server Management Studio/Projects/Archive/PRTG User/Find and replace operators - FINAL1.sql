USE [MSDB];

DECLARE @NewOperatorName nvarchar(20) = 'DBAdmin';
DECLARE @OldOperatorName nvarchar(20) = (select name from msdb.dbo.sysoperators where name like 'DJ_%');
DECLARE @EmailAddress  nvarchar(25) = 'DBAdmin@btl.com';
DECLARE @CreateNewOperator nvarchar(MAX) = '';
DECLARE @AssignAndDropOperatorNotifications nvarchar(MAX) = '';
DECLARE @DropAndyAsAlert nvarchar(MAX) = '';
DECLARE @ChangeJobNotification nvarchar(MAX) = '';
DECLARE @ChangeSQLServerAgent nvarchar(MAX) = '';

--Create a new Operator

SELECT  @CreateNewOperator +=CHAR(13) + '

IF (select name from msdb.dbo.sysoperators where name =N'+ '''' + @NewOperatorName + ''') IS NOT NULL

	BEGIN		
		PRINT ''Operator already exists''
	END
	ELSE
	BEGIN

	   EXEC msdb.dbo.sp_add_operator @name=N'+ '''' + @NewOperatorName + '''' + ', 
			@enabled=1, 
			@pager_days=0, 
			@email_address=N'+ '''' + @EmailAddress + '''' + '
		
	END	
'


--Assign all notifications to this new Operator and drop the old Operator

select  @AssignAndDropOperatorNotifications +=CHAR(13) +  '
BEGIN
	
	IF (select ID FROM msdb.dbo.sysoperators where name =N'+ '''' + @NewOperatorName + '''
	and ID not IN (
				SELECT operator_id FROM msdb.dbo.sysnotifications
				inner join msdb.dbo.sysalerts
				on msdb.dbo.sysalerts.id = msdb.dbo.sysnotifications.alert_id
			) ) IS NOT NULL

	EXEC msdb.dbo.sp_add_notification @alert_name=''' +  name + ''', @operator_name =N'+ '''' + @NewOperatorName + ''', @notification_method = 1; 
	
	ELSE
	BEGIN
	 PRINT ''Operator is already assigned to this notification'' 
    END

END


	IF (select ID FROM msdb.dbo.sysoperators where name =N'+ '''' + @NewOperatorName + '''
	and ID not IN (
				SELECT operator_id FROM msdb.dbo.sysnotifications
				inner join msdb.dbo.sysalerts
				on msdb.dbo.sysalerts.id = msdb.dbo.sysnotifications.alert_id
			) ) IS NOT NULL
	BEGIN
	EXEC msdb.dbo.sp_delete_notification @alert_name=N'+ ''''+ name + ''', @operator_name=N'+ '''' + @OldOperatorName + '''; 
	
	PRINT ''Notification removed for '+ name + '''
	END
	ELSE
	BEGIN
	 PRINT ''Operator was never assigned to this notification''
	END	
		'
from msdb.dbo.sysalerts	


--Change Jobs's Email + NetSend Operators'--

SELECT @ChangeJobNotification +=CHAR(13) + '

IF (SELECT MSDB.dbo.sysjobs.name--, notify_email_operator_id
FROM MSDB.dbo.sysjobs
inner join MSDB.dbo.sysoperators
on MSDB.dbo.sysoperators.id = MSDB.dbo.sysjobs.notify_email_operator_id
where MSDB.dbo.sysoperators.name=N'+ ''''+ @OldOperatorName + ''') IS NOT NULL

	BEGIN
	
	EXEC msdb.dbo.sp_update_job @job_name =N''' + name + ''',
		@notify_level_netsend=2, 
		@notify_level_page=2,
		@notify_email_operator_name =N'+ '''' + @NewOperatorName + ''', 
		@notify_netsend_operator_name =N'+ '''' + @NewOperatorName + '''
	
	PRINT ''Notifications have been updated''
	
	END
	ELSE
	BEGIN
	   
	   PRINT ''Notifications do not need changing''
	 
	END
'
from MSDB.dbo.sysjobs

	
Select @ChangeSQLServerAgent += char(13) + '

	EXEC master.dbo.sp_MSsetalertinfo @failsafeoperator=N'+ ''''+ @NewOperatorName + ''',
		@notificationmethod=1
'

--PRINT (@CreateNewOperator)
--PRINT (@AssignAndDropOperatorNotifications);
--PRINT (@ChangeJobNotification);
--PRINT (@ChangeSQLServerAgent);

BEGIN TRY
    BEGIN TRANSACTION 
		EXEC (@CreateNewOperator)
		EXEC (@AssignAndDropOperatorNotifications);
		EXEC (@ChangeJobNotification);
		EXEC (@ChangeSQLServerAgent);
    COMMIT TRANSACTION
END TRY
BEGIN CATCH
    SELECT 
        ERROR_NUMBER() AS ErrorNumber
        ,ERROR_SEVERITY() AS ErrorSeverity
        ,ERROR_STATE() AS ErrorState
        ,ERROR_PROCEDURE() AS ErrorProcedure
        ,ERROR_LINE() AS ErrorLine
        ,ERROR_MESSAGE() AS ErrorMessage;

    IF @@TRANCOUNT > 0
        ROLLBACK TRANSACTION;
END CATCH;

IF @@TRANCOUNT > 0
    COMMIT TRANSACTION;
GO




	
	
--Drop all notifications from the old Operator

--select @DropAndyAsAlert  +=CHAR(13) +  '

--	IF (select ID FROM msdb.dbo.sysoperators where name =N'+ '''' + @NewOperatorName + '''
--	and ID not IN (
--				SELECT operator_id FROM msdb.dbo.sysnotifications
--				inner join msdb.dbo.sysalerts
--				on msdb.dbo.sysalerts.id = msdb.dbo.sysnotifications.alert_id
--			) ) IS NOT NULL
--	BEGIN
--	EXEC msdb.dbo.sp_delete_notification @alert_name=N'+ ''''+ name + ''', @operator_name=N'+ '''' + @OldOperatorName + '''; 
	
--	PRINT ''Notification removed for '+ name + '''
--	END
--	ELSE
--	BEGIN
--	 PRINT ''Operator was never assigned to this notification''
--	END
--'
--FROM msdb.dbo.sysalerts
	