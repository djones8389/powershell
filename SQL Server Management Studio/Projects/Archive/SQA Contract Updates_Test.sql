
begin tran
 declare @ProjectID int = 1107;
 declare @ContractID int = 304;
 declare @UserName nvarchar(20) = '122678';
  
  -- 1 is Writer,  2 is Checker
 declare @ContractType int = 1;

select * from Contract
  where ProjectID = @ProjectID
	and Type = @ContractType
	and UserID =  (select UserID from UserTable where Username = @UserName)
	and ContractID = @ContractID
    and ItemSpecificationID in (select ID from ItemSpecificationLookup where SpecificationName IN (1, 5, 6))
    order by ItemSpecificationID;

 update Contract --with (NOLOCK)
 set ContractItems = 
			(select COUNT(PageID) 
				FROM ContractPage with (NOLOCK) 
					where ProjectID = @ProjectID
					and ContractPage.ContractID = @ContractID
					)
  where ProjectID = @ProjectID
	and Type = @ContractType
	and UserID =  (select UserID from UserTable where Username = @UserName)
	and ContractID = @ContractID
    and ItemSpecificationID in (select ID from ItemSpecificationLookup where SpecificationName IN (1, 5, 6));
 
select * from Contract
  where ProjectID = @ProjectID
	and Type = @ContractType
	and UserID =  (select UserID from UserTable where Username = @UserName)
	and ContractID = @ContractID
    and ItemSpecificationID in (select ID from ItemSpecificationLookup where SpecificationName IN (1, 5, 6))
    order by ItemSpecificationID;    
    
rollback