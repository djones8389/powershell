USE UAT_SQA_ContentProducer_BeforeFix

IF OBJECT_ID('tempdb..#SharedLib2') IS NOT NULL DROP TABLE #SharedLib2;
IF OBJECT_ID('tempdb..#tmp_tocheck') IS NOT NULL DROP TABLE #tmp_tocheck;
IF OBJECT_ID('tempdb..#FindPages') IS NOT NULL DROP TABLE #FindPages;
IF OBJECT_ID('tempdb..#Projects') IS NOT NULL  DROP TABLE #Projects;

select ProjectId, CAST(Structurexml as xml) as StructureXML
      into #SharedLib2
            from SharedLibraryTable with(NOLOCK);

--;with CTE AS (
 
SELECT t.*
INTO #tmp_tocheck
FROM (
      SELECT 
          plt.Name as ProjectName
          ,pmt.ProjectID
            ,pmt.ID
            ,pmt.NAME
            ,(
                  SELECT COUNT(1)
                  FROM ProjectManifestTable(NOLOCK) inner_pmt
                  WHERE inner_pmt.ID = pmt.id
                        AND inner_pmt.ProjectID != pmt.Projectid
                        AND CAST(inner_pmt.IMAGE AS VARBINARY(max)) = CAST(pmt.IMAGE AS VARBINARY(max))
                        and inner_pmt.name != pmt.name
                        
                  ) AS [Number of DB-Wide Instances]            
      FROM ProjectManifestTable (NOLOCK) pmt
      
      inner join ProjectListTable as plt on plt.ID = pmt.ProjectId
      
      ) t

	INNER JOIN (

		  select ProjectId
				, a.b.value('@id','int') as SLID
				, a.b.value('@name','nvarchar(128)') as SLName  
		  from #SharedLib2 (NOLOCK)
      
		  cross apply #SharedLib2.StructureXML.nodes('/sharedLibrary/item') a(b)
      
	) C

	on t.ProjectID = c.projectid 
		  and t.name = c.SLName
		  and t.ID = c.SLID
	WHERE t.[Number of DB-Wide Instances] > 0
--)

Select * from #tmp_tocheck  --List of duplicate images

SELECT PLT.ID as ProjectID
	  , PLT.Name
	  , Items.id
	  , Items.ver
into #FindPages
FROM
	(
		  Select i.id, i.ver, i.moD, it.aLI
		  from ItemGraphicTable it 
				INNER JOIN ComponentTable c ON c.ID = it.ParentID 
				INNER JOIN SceneTable s ON s.ID = c.ParentID 
				INNER JOIN PageTable i ON i.id = S.ParentID
		  UNION
		  Select i.id, i.ver, i.moD, it.aLI  
		  from ItemVideoTable it 
				INNER JOIN ComponentTable c ON c.ID = it.ParentID 
				INNER JOIN SceneTable s ON s.ID = c.ParentID 
				INNER JOIN PageTable i ON i.id = S.ParentID
		  UNION
		  Select i.id, i.ver, i.moD, it.aLI  
				from ItemHotSpotTable it 
				INNER JOIN ComponentTable c ON c.ID = it.ParentID 
				INNER JOIN SceneTable s ON s.ID = c.ParentID 
				INNER JOIN PageTable i ON i.id = S.ParentID
		  UNION
		  Select i.id, i.ver, i.moD, it.aLI 
		  from ItemCustomQuestionTable it 
				INNER JOIN ComponentTable c ON c.ID = it.ParentID 
				INNER JOIN SceneTable s ON s.ID = c.ParentID 
				INNER JOIN PageTable i ON i.id = S.ParentID
	) AS Items
INNER JOIN #tmp_tocheck 
      ON Items.aLI = SUBSTRING(#tmp_tocheck.NAME , 0, CHARINDEX('.',#tmp_tocheck.NAME ))
      AND SUBSTRING (Items.ID, 0, CHARINDEX('P', Items.ID)) = #tmp_tocheck.ProjectID
--INNER JOIN CTE 
--      ON Items.aLI = SUBSTRING(CTE.NAME , 0, CHARINDEX('.',CTE.NAME ))
--      AND SUBSTRING (Items.ID, 0, CHARINDEX('P', Items.ID)) = CTE.ProjectID
INNER JOIN PageTable as PT
	  on PT.ID = Items.ID and PT.ver = Items.ver      
Inner Join ProjectListTable as PLT
	  on PLT.ID = PT.ParentID

where Items.id IN (
					--Only apply this to publishable pages
	select 
		 cast(PLT.ID as nvarchar(10)) + 'P' + cast(p.r.value('@ID','nvarchar(20)') as nvarchar(10))
		from dbo.ProjectListTable as PLT with (NOLOCK)
	
	cross apply ProjectStructureXml.nodes('Pro/*[local-name(.)!="Rec"]//Pag') p(r) 
	
	where p.r.value('@sta','int') Between PublishSettingsXML.value('(/PublishConfig/StatusLevelFrom)[1]','int') and PublishSettingsXML.value('(/PublishConfig/StatusLevelTo)[1]','int')

	)
	
ORDER BY Items.ID, Items.ver   
     
SELECT * FROM #FindPages  --List of affected items


DECLARE  @ProjectsToTarget table (ProjectID int, PageID int)
INSERT @ProjectsToTarget(ProjectID, PageID)
SELECT 
       SUBSTRING(#FindPages.ID, 0 , CHARINDEX(N'P', #FindPages.ID))  as ProjectID
	   , SUBSTRING(#FindPages.ID, CHARINDEX('P', #FindPages.ID) + 1, LEN(#FindPages.ID)) as PageID
    FROM #FindPages--where ProjectID= 845

select Id, ver 
from UAT_SQA_ContentProducer..PageTable 
where id in (select ID from #FindPages) order by parentid;

select ProjectID, ItemRef, MAX(Version)
from UAT_SQA_ItemBank..ItemTable
where (Projectid in (select ProjectID from @ProjectsToTarget) and itemRef in (select PageID from @ProjectsToTarget))
group by ProjectID, ItemRef
order by projectid;



SELECT ID 
	 , Name
      ,ProjectStructureXML
INTO #Projects
FROM UAT_SQA_ContentProducer_BeforeFix..ProjectListTable 
Where ID IN (SELECT ProjectID from @ProjectsToTarget);

--select * from #Projects 

DECLARE PageXML CURSOR FOR 
	select ProjectID, PageID
		from @ProjectsToTarget

DECLARE @ProjectID int, @PageID int;

OPEN PAGEXML;

FETCH NEXT FROM PageXML into @ProjectID, @PageID;

WHILE @@FETCH_STATUS = 0

BEGIN

--PRINT @ProjectID
--PRINT @PageID

Update #Projects
set ProjectStructureXML.modify('replace value of (/Pro//Pag[@ID = sql:variable("@PageID")]/@alF)[1] with ("1")')
where ID = @ProjectID;

FETCH NEXT FROM PageXML into @ProjectID, @PageID

END
CLOSE PageXML;
DEALLOCATE PageXML;


--select * from #Projects; 


update UAT_SQA_ContentProducer..ProjectListTable
set ProjectStructureXML = b.ProjectStructureXml
from UAT_SQA_ContentProducer..ProjectListTable a

inner join #Projects as b
on b.ID = a.ID and b.name = a.name

where b.ID = a.ID and b.name = a.name;


DROP TABLE #Projects;


UPDATE UAT_SQA_ContentProducer..PageTable
SET ver = ver + 1 
where id in (select id from #FindPages) -- and parentid = 845;
--where ID in (SELECT cast(ProjectID as nvarchar(10)) + 'P' + cast(PageID as nvarchar(10)) FROM @ProjectsToTarget);

select Id, ver 
from UAT_SQA_ContentProducer..PageTable 
where id in (select ID from #FindPages) order by parentid;

select ProjectID, ItemRef, MAX(Version)
from UAT_SQA_ItemBank..ItemTable
where (Projectid in (select ProjectID from @ProjectsToTarget) and itemRef in (select PageID from @ProjectsToTarget))
group by ProjectID, ItemRef
order by projectid;

DROP TABLE #FindPages;


--select top 10 id, name, ProjectStructureXml, PublishSettingsXML, ProjectDefaultXML
--from UAT_SQA_ContentProducer..ProjectListTable where id = 845


--begin tran

--select top 10 id, name, ProjectStructureXml
--from UAT_SQA_ContentProducer..ProjectListTable

--update UAT_SQA_ContentProducer..ProjectListTable
--set ProjectStructureXml = b.ProjectStructureXml
--from UAT_SQA_ContentProducer..ProjectListTable a

--inner join UAT_SQA_ContentProducer_BeforeFix..ProjectListTable b
--on b.id = a.id and a.Name = b.Name

--where b.id = a.id and a.Name = b.Name

--select top 10 id, name, ProjectStructureXml
--from UAT_SQA_ContentProducer..ProjectListTable

--rollback

--select ProjectID, ItemRef, MAX(Version)
--from UAT_SQA_ItemBank..ItemTable
--where (Projectid in (select ProjectID from @ProjectsToTarget) and itemRef in (select PageID from @ProjectsToTarget))
--group by ProjectID, ItemRef ;

--select Id, ver from UAT_SQA_ContentProducer..PageTable where id in (select PageID from @ProjectsToTarget);