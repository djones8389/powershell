select UserID
	from UserQualificationsTable
		where QualificationID = 201
		

create table #myTemp
(
	userID int,
	QualificationID int,
	CQN int null

)
		
with cte as (
		
select ID--, Forename, Surname, CandidateRef
	 from UserTable 
	 
where CandidateRef = ''
	and Retired = 0
		and ID not in (195094,195095,195099,195100,195214)
)

--select * from cte

insert into #myTemp(userID, QualificationID)

SELECT distinct userID
      , '201'--QualificationID
     -- , 'NULL'
      
  from cte
  
  left join UserQualificationsTable
  on UserQualificationsTable.UserID = cte.ID
  
  
select * from #myTemp order by userID


merge into UserQualificationsTable as TGT
using #myTemp as SRC
	on 1 = 2
when not matched then
insert(UserID, QualificationID)
values(SRC.UserID, SRC.QualificationID);



drop table #myTemp