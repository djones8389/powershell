
--This tells you how many records you expect to update

use BRITISHCOUNCIL_SecureAssess

select count(qualificationName), qualificationName, examName, examVersionRef, examVersionId
	 from WAREHOUSE_ExamSessionTable_Shreded
	where examname = '' 
	 group by qualificationName, examName, examVersionRef, examVersionId
		
	order by qualificationName asc



--Declare your ExamVersionID

use BRITISHCOUNCIL_ItemBank

declare @examversion int = 304

--begin tran

update BRITISHCOUNCIL_SecureAssess..WAREHOUSE_ExamSessionTable_Shreded
set examName = 
	( 
		select AGT.Name	
			from BRITISHCOUNCIL_ItemBank..AssessmentGroupTable as AGT
			
			inner join QualificationTable as QT
			on QT.ID = AGT.QualificationID			
			
			inner join AssessmentTable as AT
			on AT.AssessmentGroupID = AGT.ID
			
			where AT.ID = @examversion
			)
			
where examVersionId = @examversion
	and examName = ''

--rollback