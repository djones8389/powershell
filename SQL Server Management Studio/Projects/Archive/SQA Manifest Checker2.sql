DROP TABLE #SharedLib

select ProjectId, CAST(Structurexml as xml) as StructureXML
      into #SharedLib
            from SharedLibraryTable with(NOLOCK);
      
SELECT t.*
FROM (
      SELECT 
          plt.Name as ProjectName
          ,pmt.ProjectID
            ,pmt.ID
            ,pmt.NAME
            ,(
                  SELECT COUNT(1)
                  FROM ProjectManifestTable(NOLOCK) inner_pmt
                  WHERE inner_pmt.ID = pmt.id
                        AND inner_pmt.ProjectID != pmt.Projectid
                        AND CAST(inner_pmt.IMAGE AS VARBINARY(max)) = CAST(pmt.IMAGE AS VARBINARY(max))
                        and inner_pmt.name != pmt.name
                        
                  ) AS [Number of DB-Wide Instances]
             ,(
                      SELECT COUNT(1) FROM 
                              (Select ID, aLI  from ItemGraphicTable WHERE aLI = SUBSTRING(pmt.NAME, 0, CHARINDEX('.',pmt.NAME))
                              UNION
                              Select ID, aLI  from ItemVideoTable WHERE aLI = SUBSTRING(pmt.NAME, 0, CHARINDEX('.',pmt.NAME))
                              UNION
                              Select ID, aLI  from ItemHotSpotTable WHERE alI = SUBSTRING(pmt.NAME, 0, CHARINDEX('.',pmt.NAME))
                              UNION
                              Select ID, aLI  from ItemCustomQuestionTable WHERE alI = SUBSTRING(pmt.NAME, 0, CHARINDEX('.',pmt.NAME))
                              ) AS Usages                        
                          
              ) AS [Page Usage Count]
      FROM ProjectManifestTable (NOLOCK) pmt
      
      inner join ProjectListTable as plt on plt.ID = pmt.ProjectId
      
      ) t

INNER JOIN (

      select ProjectId
            , a.b.value('@id','int') as SLID
            , a.b.value('@name','nvarchar(128)') as SLName  
      from #SharedLib with(NOLOCK)
      
      cross apply #SharedLib.StructureXML.nodes('/sharedLibrary/item') a(b)
      
) C

on t.ProjectID = c.projectid 
      and t.name = c.SLName
      and t.ID = c.SLID

WHERE t.[Number of DB-Wide Instances] > 1
	and [Page Usage Count] > 0
	
order by t.ProjectName
