/*Backup your tables*/

SELECT * INTO UAT_BCGuilds_SecureAssess_11_7_223_0_IP.dbo.[BACKUP_RolesTable]
FROM UAT_BCGuilds_SecureAssess_11_7_223_0_IP.dbo.RolesTable

SELECT * INTO UAT_BCGuilds_SecureAssess_11_7_223_0_IP.dbo.[BACKUP_RolePermissionsTable]
FROM  UAT_BCGuilds_SecureAssess_11_7_223_0_IP.dbo.RolePermissionsTable


/*Write Roles into table variable*/

SELECT Id, Name, Type, HasViewEdit, UsesQualLevel FROM UAT_BC2_SecureAssess..PermissionsTable 
EXCEPT
SELECT Id, Name, Type, HasViewEdit, UsesQualLevel FROM UAT_BCGuilds_SecureAssess_11_7_223_0_IP..PermissionsTable 
IF @@ROWCOUNT = 0

BEGIN

       DECLARE @RolesTable TABLE (
              [ID] [INT],
              [Name] [NVARCHAR](50),
              [type] [INT] ,
              [removable] [BIT] ,
              [seeded] [BIT],
              [AssignToTechnicalContactsOnCreation] [BIT],
              [AssignToPrimaryContactsOnCreation] [BIT],
              [AssignableToGlobalCentre] [BIT],
              [AssignableAtCentres] [BIT]
              )
       INSERT @RolesTable
                     ( ID ,
                       Name ,
                       type ,
                       removable ,
                       seeded ,
                       AssignToTechnicalContactsOnCreation ,
                       AssignToPrimaryContactsOnCreation ,
                       AssignableToGlobalCentre ,
                       AssignableAtCentres
                     )

       SELECT * FROM UAT_BC2_SecureAssess..RolesTable 
       WHERE ID IN (
              SELECT ID 
              FROM UAT_BCGuilds_SecureAssess_11_7_223_0_IP..RolesTable
              )

       /*Write RolesPermissions into table variable*/

       DECLARE @RolesPermissionTable TABLE (
              [RoleID] [INT] ,
              [PermissionID] [INT] ,
              [QualificationLevel] [INT],
              [Edit] [BIT]
       )
       INSERT @RolesPermissionTable
                     ( RoleID ,
                       PermissionID ,
                       QualificationLevel ,
                       Edit
                     )
       SELECT * FROM  UAT_BC2_SecureAssess..RolePermissionsTable
       WHERE RoleID IN (
              SELECT ID 
              FROM UAT_BCGuilds_SecureAssess_11_7_223_0_IP..RolesTable
              )


       /*When RoleID and RoleName are equal in BC_SA and CGBCSA then write over the top*/

       PRINT 'Updating RolesTable'

       MERGE UAT_BCGuilds_SecureAssess_11_7_223_0_IP..RolesTable AS TGT
       USING (SELECT ID ,
                       Name ,
                       type ,
                       removable ,
                       seeded ,
                       AssignToTechnicalContactsOnCreation ,
                       AssignToPrimaryContactsOnCreation ,
                       AssignableToGlobalCentre ,
                       AssignableAtCentres
                     FROM @RolesTable
                     ) AS SRC
              ON TGT.ID = SRC.ID
                AND TGT.Name = SRC.Name
       WHEN MATCHED THEN
       UPDATE 
       SET TGT.TYPE = src.TYPE
              , TGT.removable = SRC.removable
                , TGT.seeded = SRC.seeded
                , tgt.AssignToTechnicalContactsOnCreation = src.AssignToTechnicalContactsOnCreation
                , tgt.AssignToPrimaryContactsOnCreation = src.AssignToPrimaryContactsOnCreation
                , tgt.AssignableToGlobalCentre = src.AssignableToGlobalCentre
                , tgt.AssignableAtCentres = src.AssignableAtCentres
       ;

       /*When RoleID are equal in BC_SA and CGBCSA then write the permissions and levels over the top*/

       PRINT 'Updating RolePermissionsTable'
       MERGE UAT_BCGuilds_SecureAssess_11_7_223_0_IP..RolePermissionsTable AS TGT
       USING    ( SELECT RoleID ,
                             PermissionID ,
                             QualificationLevel ,
                             Edit
                       FROM @RolesPermissionTable
                     )  AS SRC
       ON TGT.RoleID = SRC.RoleID
              AND TGT.PermissionID = SRC.PermissionID
              AND TGT.QualificationLevel = SRC.QualificationLevel
              --AND TGT.Edit = SRC.Edit
       WHEN MATCHED THEN
       UPDATE SET
              TGT.Edit = SRC.Edit  
        WHEN NOT MATCHED THEN
       INSERT (RoleID, PermissionID, QualificationLevel, Edit)
       VALUES(SRC.RoleID, SRC.PermissionID, SRC.QualificationLevel, src.Edit)
;

END
ELSE PRINT 'Permissions are not aligned, please re-check this'

