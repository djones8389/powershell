USE [DBName]

/*
**If username exists already, this will bomb out,  no further action**

If it doesn't exist, it will:

Create User
Assign user to the three roles it needs (Roles + permissions have already been synch'ed via previous script)
Assign user to all qualifications
Assign user to cohort table

*/


/*Delete additional superuser role if it's there*/

IF EXISTS (select 1 from RolesTable where Name = 'superuser' and ID > 1)
	DELETE from RolesTable where Name = 'superuser' and ID > 1 
ELSE
	Print 'Additional Superuser never existed'


IF EXISTS (select 1 from Usertable where username = 'ServiceUser_BritishCouncil')
	PRINT 'Already Exists'

ELSE 
	
	BEGIN

	Insert dbo.usertable (Forename, Surname, DOB, Gender, AddressLine1, AddressLine2, Town, County, Country, PostCode, Telephone, Email, EthnicOriginID, AccountCreationDate, AccountExpiryDate, Username, Password, Retired, version, Seeded, IsExternal, IsExternalUser)
	VALUES('Service', 'User',  '1982-10-31 00:00:00', 'M', 'BTL Group Ltd',	'Salts Wharf, Ashley Lane',	'Shipley','7','0', 'BD17 7DB', '01274 203250', 'LiveServices@btl.com', '0', GetDate(), DATEADD(Year, 5,GETDATE()), 'DJTESTING', 'Password', '0', '1', '0', '0', '0')

	DECLARE @newID tinyint = scope_identity()

	--Grant ServiceUser_BritishCouncil the roles it needs--

	DECLARE @RolesToInsert TABLE (RoleID tinyint, newUserID tinyint, GlobalCentreID tinyint)
	INSERT @RolesToInsert(RoleID, newUserID, GlobalCentreID)
	Select ID, @newID, '1'
	from rolestable
	WHere name in ('Base','IntegrationRole','Standard Integration')

	INSERT AssignedUserRolesTable(UserID, RoleID, CentreID)	
	SELECT newUserID, RoleID, GlobalCentreID FROM @RolesToInsert

	--Grant them access to all qualifications--

	DECLARE @QualsToInsert TABLE(qualificationID tinyint)
	INSERT @QualsToInsert(qualificationID)
	Select DISTINCT QualificationID
	FROM UserQualificationsTable

	INSERT UserQualificationsTable(UserID, QualificationID)
	SELECT @newID, qualificationID
	FROM @QualsToInsert

	--They need access to the Cohort so it shows as ticked in the UI

	INSERT CohortUsersTable
	VALUES('1', @newID)

	END

	