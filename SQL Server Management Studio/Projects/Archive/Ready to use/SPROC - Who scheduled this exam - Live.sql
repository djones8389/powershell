use SADB
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[BTL_DaveJ_WhoScheduledThisExam_Live]

@examSessionID int
AS
BEGIN


create table #CreatedByTempTable
(
	userID int,
	Forename nvarchar(20),
	Surname nvarchar(20)

)


SELECT CreatedBy, UserTable.Forename, UserTable.Surname
  FROM ScheduledExamsTable 
  
  inner Join UserTable
  on UserTable.ID = ScheduledExamsTable.CreatedBy
  
where ScheduledExamsTable.ID = @examSessionID


drop table #CreatedByTempTable
--drop procedure [dbo].[BTL_DaveJ_WhoScheduledThisExam_Live]
END