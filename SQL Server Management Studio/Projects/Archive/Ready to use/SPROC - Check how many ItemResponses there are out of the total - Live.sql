use SADB
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[BTL_DaveJ_HowManyItemResponses_Live]

@Keycode nvarchar(12)
AS
BEGIN

declare @ID int = (
select ID
	from ExamSessionTable
		where KeyCode = @Keycode
	)


create table #ItemResponseTempTable

(
      ID2 int,
      TotalNumberOfQuestions int
)

insert into #ItemResponseTempTable

      select
                  ID,
                  StructureXML.value('count(/assessmentDetails/assessment/section/item)[1]', 'int')
      from ExamSessionTable
     where KeyCode = @Keycode
      
      
	select 				
		   COUNT (ExamSessionID) as NumberOfQuestionsAnswered
		   ,  TT.TotalNumberOfQuestions      

		from ExamSessionItemResponseTable as ESIRT

		inner join ExamSessionTable as EST
		on EST.ID = ESIRT.ExamSessionID
		
		inner join #ItemResponseTempTable as TT
        on TT.ID2 = ESIRT.ExamSessionID
		
		where KeyCode = @Keycode
		
	group by ExamSessionID, TT.TotalNumberOfQuestions 
	
	drop table #ItemResponseTempTable
	--drop procedure BTL_DaveJ_HowManyItemResponses_Live
END
