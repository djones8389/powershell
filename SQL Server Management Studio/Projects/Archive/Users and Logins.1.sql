DECLARE DBNAMES 
CURSOR FOR

	select top 20 '[' + name + ']'
		from sys.databases 
			where state_desc = 'ONLINE'
			
OPEN DBNAMES;


DECLARE @NAME nvarchar(150);

FETCH NEXT FROM DBNAMES INTO @NAME;

WHILE @@FETCH_STATUS = 0
BEGIN

PRINT 'Select u.name as ''DB_User'', l.name as [Missing from: ' + @NAME + ' from ' + @NAME + '.sys.sysusers as u left join ' + @NAME + '.sys.syslogins as l on l.sid = u.sid where issqlrole = 0 and l.name is NULL and u.name not in(''sys'', ''guest'', ''dbo'', ''INFORMATION_SCHEMA'')'

FETCH NEXT FROM DBNAMES INTO @NAME;
END

CLOSE DBNAMES;
DEALLOCATE DBNAMES;










--Select u.name from BRITISHCOUNCIL_ItemBank.sys.syslogins l inner join BRITISHCOUNCIL_ItemBank.sys.sysusers u on l.sid = u.sid where issqlrole = 0
--Select * from BRITISHCOUNCIL_ItemBank.sys.syslogins where name = 'ReportingUser'
--Select * from BRITISHCOUNCIL_ItemBank.sys.sysusers where issqlrole = 0
----where it's in sysusers but not syslogins
--select * from sys.syslogins order by name