begin tran

SELECT *
FROM ProjectListTable
WHERE  ID = 1036;


CREATE TABLE #Xml
	(ProjectID INT NOT NULL
	,ProjectXml XML NOT NULL);

INSERT INTO #Xml
	SELECT ID
		,ProjectStructureXml
	FROM dbo.ProjectListTable;
	--WHERE  ID = 1036;
	
WITH CTE AS
(
	SELECT ProjectID
		,CAST(N'/' + node.value(N'fn:local-name(.)', N'nvarchar(255)') + N'[@ID="' + node.value(N'@ID', 'nvarchar(255)') + N'"]' AS nvarchar(510)) AS xpath
		,node.query(N'.') AS me
		,node.query(N'*') AS children
	FROM #Xml AS myXml
	CROSS APPLY myXml.ProjectXml.nodes('/*') AS roots(node)
	
	UNION ALL
	
	SELECT ProjectID
		,CAST(x.xpath + N'/' + node.value(N'fn:local-name(.)', N'nvarchar(255)') + N'[@ID="' + node.value(N'@ID', 'nvarchar(255)') + N'"]' AS nvarchar(510))
		,node.query(N'.')
		,node.query('*')
	FROM CTE x
	CROSS APPLY x.children.nodes('*') AS child(node)
)
SELECT N'UPDATE ProjectListTable SET ProjectXml.modify(''delete '+ xpath +'/@'+ attrName +''') WHERE ID = '+ CAST(ProjectID AS nvarchar(100)) + N';'
FROM (
	SELECT ProjectID
		,xpath
		,my.attr.value(N'fn:local-name(.)', N'nvarchar(255)') attrName
	FROM CTE
	CROSS APPLY me.nodes(N'/*[1]/@*') my(attr)
	WHERE my.attr.value(N'fn:local-name(.)', N'nvarchar(255)') LIKE N'MarkedMetaData%'
	AND my.attr.value(N'fn:local-name(.)', N'nvarchar(255)') NOT LIKE N'MarkedMetaData[_][0-9]'
	) X

DROP TABLE #Xml;

SELECT *
FROM ProjectListTable
WHERE  ID = 1036;

rollback
