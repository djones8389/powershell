USE BritishCouncil_SecureMarker_19_11_2014

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

;with cte as (

SELECT 
	UniqueResponsesWithDocs.ID
	, ExpectedNumDocs
	, COUNT(UniqueResponseDocuments.ID) AS ActualNumDocs
		
FROM (
	SELECT responseData.value('count(p/s/c/i/data)', 'int') AS ExpectedNumDocs, ID
	FROM UniqueResponses
	WHERE responseData.exist('p/s/c/i/data') = 1) UniqueResponsesWithDocs
	INNER JOIN UniqueResponseDocuments
	ON UniqueResponseDocuments.UniqueResponseID = UniqueResponsesWithDocs.ID
GROUP BY UniqueResponsesWithDocs.ID, ExpectedNumDocs
HAVING ExpectedNumDocs <> COUNT(UniqueResponseDocuments.ID)

)

SELECT cev.ExternalSessionID, cev.Keycode, Urd.Name
FROM CandidateExamVersions as CEV

INNER JOIN CandidateResponses as CR
ON CR.CandidateExamVersionID = CEV.ID

INNER JOIN UniqueResponses as UR
ON UR.ID = UniqueResponseID 

left join UniqueResponseDocuments as URD
ON URD.UniqueResponseID = UR.id 
	
Inner Join Items as I
on I.ID = ur.itemId

inner join ExamVersions as EV
on EV.ID = I.ExamVersionID

INNER JOIN Exams as E
ON E.ID = EV.ExamID

INNER JOIN Qualifications
ON E.QualificationID = Qualifications.ID


where UR.id in (select id from cte)
	order by  cev.ExternalSessionID desc