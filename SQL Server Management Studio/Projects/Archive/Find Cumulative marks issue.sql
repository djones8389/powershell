;with cte as 
(
	select ExamSessionId
		from CumulativeMarkingTable
			inner join ExamSessionTable on ExamSessionTable.ID = CumulativeMarkingTable.ExamSessionId
			where MarkingProgress is  null and CumulativeUserMarks = 0 and examState = 15
	
)

select 
	ExamSessionTable.ID
	--,s.i.value('@id[1]','nvarchar(20)') as ItemID
	, StructureXML
	, Forename
	, Surname
	, examName
	, ExamRef
	, examVersionName
	, examVersionRef
	, StateChangeDate as [Completed]
	from ExamSessionTable
	
	inner join UserTable
	on UserTable.ID = ExamSessionTable.UserID
	
	inner join ScheduledExamsTable
	on ScheduledExamsTable.ID = ExamSessionTable.ScheduledExamID
	
	inner join ExamStateChangeAuditTable as escat
	on escat.ExamSessionID = ExamSessionTable.id
	
	cross apply StructureXML.nodes('assessmentDetails/assessment/section/item') s(i)
	
	where ExamSessionTable.ID IN (SELECT ExamSessionId FROM cte)
	
		and s.i.value('@markingType[1]','decimal') = 0
		and s.i.value('@userMark[1]','decimal') > 0
		and NewState = 9
	order by Surname;