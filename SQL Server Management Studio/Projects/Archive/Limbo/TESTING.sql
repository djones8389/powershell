-------Testing--------


create function CheckItemResponsesForLimboExams()
RETURNS @myTable TABLE (NumberOfQuestionsAnswered int, ID int, KeyCode nvarchar(100), Forename nvarchar(100), Surname nvarchar(100), CandidateRef nvarchar(100))
AS
BEGIN 

declare @myStorage int = (

select 
StructureXML.value('count(/assessmentDetails/assessment/section/item)[1]', 'int')
from ExamSessionTable
where ID = 1059236

)

select 
	   COUNT (ExamSessionID) as NumberOfQuestionsAnswered
	   , @myStorage
	   --, StructureXML.value('count(/assessmentDetails/assessment/section/item)[1]', 'int') as TotalNumberOfQuestions
	   , EST.ID
	   , EST.KeyCode
	   , UT.Forename
	   , UT.Surname  
	   , UT.CandidateRef 
from ExamSessionItemResponseTable as ESIRT

inner join ExamSessionTable as EST
on EST.ID = ESIRT.ExamSessionID

inner join UserTable as UT
on UT.ID = EST.UserID

WHERE examState = 6

	--and ExamSessionID in (
	--select ID
	--	from ExamSessionTable
	--	)
	
GROUP BY ExamSessionID, EST.ID, EST.KeyCode, UT.Forename, UT.Surname, UT.CandidateRef 
--HAVING COUNT (ExamSessionID) <> 0
	--order by NumberOfQuestionsAnswered desc
	RETURN
END
	
GO


/*
declare @myStorage int = (

select 
StructureXML.value('count(/assessmentDetails/assessment/section/item)[1]', 'int')
from ExamSessionTable
where ID = 1059236

)
*/