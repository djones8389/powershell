CREATE PROCEDURE [dbo].[DaveJTest_sp]

AS
BEGIN


		select 
				
			   COUNT (ExamSessionID) as NumberOfQuestionsAnswered
			   , EST.ID as ID
			   , EST.KeyCode as Keycode
			   , UT.Forename as Forename
			   , UT.Surname as Surname
			   , UT.CandidateRef as CandidateRef
		from ExamSessionItemResponseTable as ESIRT

		inner join ExamSessionTable as EST
		on EST.ID = ESIRT.ExamSessionID

		inner join UserTable as UT
		on UT.ID = EST.UserID

		WHERE examState = 6

	
GROUP BY ExamSessionID, EST.ID, EST.KeyCode, UT.Forename, UT.Surname, CandidateRef	

END;
GO





