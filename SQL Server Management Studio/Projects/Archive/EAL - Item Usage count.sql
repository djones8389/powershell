SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

select de.ExamName,
dqt.questiontype,
 COUNT(DQT.QuestionType) 
 
from DimExams as DE

Inner Join FactExamSessions as FES
on DE.examkey = fes.examkey

inner join FactComponentResponses as FCR
on fes.ExamSessionKey = fcr.ExamSessionKey

inner join DimQuestions as DQ
on dq.CPID = fcr.CPID
and  dq.CPVersion = fcr.CPVersion

inner join DimQuestionTypes as DQT
on dq.QuestionTypeKey = DQT.QuestionTypeKey


GROUP BY ExamName, dqt.QuestionType
order by 1