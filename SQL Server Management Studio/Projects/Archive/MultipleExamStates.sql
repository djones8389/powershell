select newState, COUNT(newState) 'Count', ExamSessionID
from ExamStateChangeAuditTable 
 group by NewState, ExamSessionID
having COUNT(*) > 1
order by 2 desc
