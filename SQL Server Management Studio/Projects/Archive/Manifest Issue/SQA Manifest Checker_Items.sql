USE UAT_SQA_ContentProducer

select ProjectId, CAST(Structurexml as xml) as StructureXML
	into #SharedLib
		from SharedLibraryTable (NOLOCK);
	
	
SELECT t.*
FROM (
	SELECT 
	    plt.Name as ProjectName
	    ,pmt.ProjectID
		,pmt.ID
		,pmt.NAME
		,(
			SELECT COUNT(1)
			FROM ProjectManifestTable(NOLOCK) inner_pmt
			WHERE inner_pmt.ID = pmt.id
				AND inner_pmt.ProjectID != pmt.Projectid
				AND CAST(inner_pmt.IMAGE AS VARBINARY(max)) = CAST(pmt.IMAGE AS VARBINARY(max))
				and inner_pmt.name != pmt.name
				
			) AS [Number of DB-Wide Instances]
	FROM ProjectManifestTable (NOLOCK) as pmt
	
	inner join ProjectListTable as plt on plt.ID = pmt.ProjectId
	
	) t

INNER JOIN (

	select ProjectId
		, a.b.value('@id','int') as SLID
		, a.b.value('@name','nvarchar(128)') as SLName
	
	from #SharedLib with(NOLOCK)
	
	cross apply #SharedLib.StructureXML.nodes('/sharedLibrary/item') a(b)
	
) C

on t.ProjectID = c.projectid 
	and t.name = c.SLName
	and t.ID = c.SLID

WHERE t.[Number of DB-Wide Instances] > 1
	order by t.ID;

drop table #SharedLib;

