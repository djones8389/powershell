USE SQA_CPProjectAdmin

IF OBJECT_ID('tempdb..#SharedLib') IS NOT NULL DROP TABLE #SharedLib;
IF OBJECT_ID('tempdb..#tmp_tocheck') IS NOT NULL DROP TABLE #tmp_tocheck;
IF OBJECT_ID('tempdb..#PagesToCheckOutAndBackIn') IS NOT NULL DROP TABLE #PagesToCheckOutAndBackIn;

select ProjectId, CAST(Structurexml as xml) as StructureXML
      into #SharedLib
            from SharedLibraryTable with(NOLOCK);
      
SELECT t.*
INTO #tmp_tocheck
FROM (
      SELECT 
          plt.Name as ProjectName
          ,pmt.ProjectID
            ,pmt.ID
            ,pmt.NAME
            ,(
                  SELECT COUNT(1)
                  FROM ProjectManifestTable(NOLOCK) inner_pmt
                  WHERE inner_pmt.ID = pmt.id
                        AND inner_pmt.ProjectID != pmt.Projectid
                        AND CAST(inner_pmt.IMAGE AS VARBINARY(max)) = CAST(pmt.IMAGE AS VARBINARY(max))
                        and inner_pmt.name != pmt.name
                        
                  ) AS [Number of DB-Wide Instances]            
      FROM ProjectManifestTable (NOLOCK) pmt
      
      inner join ProjectListTable as plt on plt.ID = pmt.ProjectId
      
      ) t

INNER JOIN (

      select ProjectId
            , a.b.value('@id','int') as SLID
            , a.b.value('@name','nvarchar(128)') as SLName  
      from #SharedLib (NOLOCK)
      
      cross apply #SharedLib.StructureXML.nodes('/sharedLibrary/item') a(b)
      
) C

on t.ProjectID = c.projectid 
      and t.name = c.SLName
      and t.ID = c.SLID
WHERE t.[Number of DB-Wide Instances] > 0

SELECT 
		PLT.ID as ProjectID
	   , PLT.Name
	   , Items.id
	   , Items.ver
into #PagesToCheckOutAndBackIn
FROM
(
      Select i.id, i.ver, i.moD, it.aLI
      from ItemGraphicTable it 
            INNER JOIN ComponentTable c ON c.ID = it.ParentID 
            INNER JOIN SceneTable s ON s.ID = c.ParentID 
            INNER JOIN PageTable i ON i.id = S.ParentID
      UNION
      Select i.id, i.ver, i.moD, it.aLI  
      from ItemVideoTable it 
            INNER JOIN ComponentTable c ON c.ID = it.ParentID 
            INNER JOIN SceneTable s ON s.ID = c.ParentID 
            INNER JOIN PageTable i ON i.id = S.ParentID
      UNION
      Select i.id, i.ver, i.moD, it.aLI  
            from ItemHotSpotTable it 
            INNER JOIN ComponentTable c ON c.ID = it.ParentID 
            INNER JOIN SceneTable s ON s.ID = c.ParentID 
            INNER JOIN PageTable i ON i.id = S.ParentID
      UNION
      Select i.id, i.ver, i.moD, it.aLI 
      from ItemCustomQuestionTable it 
            INNER JOIN ComponentTable c ON c.ID = it.ParentID 
            INNER JOIN SceneTable s ON s.ID = c.ParentID 
            INNER JOIN PageTable i ON i.id = S.ParentID
) AS Items
INNER JOIN #tmp_tocheck 
      ON Items.aLI = SUBSTRING(#tmp_tocheck.NAME , 0, CHARINDEX('.',#tmp_tocheck.NAME ))
      AND SUBSTRING (Items.ID, 0, CHARINDEX('P', Items.ID)) = #tmp_tocheck.ProjectID
INNER JOIN PageTable as PT
	  on PT.ID = Items.ID and PT.ver = Items.ver      
Inner Join ProjectListTable as PLT
	  on PLT.ID = PT.ParentID

ORDER BY Items.ID, Items.ver   
     
--select * from #PagesToCheckOutAndBackIn order by ProjectID;

DECLARE CPPAGES CURSOR FOR

select 
	  SUBSTRING(PageTable.ID, 0, CHARINDEX('P', PageTable.ID)) as ProjectID
	, SUBSTRING(PageTable.ID, CHARINDEX('P',PageTable.ID) + 1, CHARINDEX('P',PageTable.ID)) as PageID
	from PageTable (NOLOCK)
where ID IN(Select ID from #PagesToCheckOutAndBackIn)

DECLARE @ProjectID int, @PageID int

OPEN CPPAGES;

FETCH NEXT FROM CPPAGES INTO @ProjectID, @PageID

WHILE @@FETCH_STATUS = 0

BEGIN

PRINT N'update ProjectListTable set ProjectStructureXml.modify(''replace value of (//Pag[@ID='+ cast(@PageID as nvarchar(MAX)) +'][1]/@alF)[1] with ("1")'') where id =' + cast(@ProjectID as nvarchar(MAX))

FETCH NEXT FROM CPPAGES INTO @ProjectID, @PageID

END
CLOSE CPPAGES;
DEALLOCATE CPPAGES;

select parentid, ID, ver FROM PageTable 
	where ID IN (Select ID from #PagesToCheckOutAndBackIn)
		order by ParentID;

DROP TABLE #tmp_tocheck;
DROP TABLE #SharedLib;


--Run Alf Updates, then the below--


--Update PageTable
--set ver = ver + 1
--where ID IN(Select ID from #PagesToCheckOutAndBackIn);

--DROP TABLE #PagesToCheckOutAndBackIn;

--SDesk to do a publish now--