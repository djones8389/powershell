with cte as (

	select ID 
		from WAREHOUSE_ExamSessionTable
			where PreviousExamState <> 10
)


select 
	 examSessionId
	, userAttempted
	, responsexml.p.value('@ua[1]','bit') as 'UA from XML'
	
 from WAREHOUSE_ExamSessionTable_ShrededItems 

	--inner join WAREHOUSE_ExamSessionTable
	--on WAREHOUSE_ExamSessionTable.id  = WAREHOUSE_ExamSessionTable_ShrededItems.examSessionId

cross apply responsexml.nodes('/p') responsexml(p)

where userAttempted < responsexml.p.value('@ua[1]','bit')
	and examSessionId in 
		(
			Select ID
				 from cte
			 )