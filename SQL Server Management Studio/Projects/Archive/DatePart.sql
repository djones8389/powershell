select top 10 * from ExamStateChangeAuditTable


select COUNT(examSessionID) as 'ExamCount'
	, CAST(DATEPART(DAY, StateChangeDate) as nvarchar(100))
	, CAST(DATEPART(HOUR, StateChangeDate) as nvarchar(100))
	, CAST(DATEPART(minute, StateChangeDate) as nvarchar(100))
from ExamStateChangeAuditTable
where DATEPART(
	group by examSessionID
		, CAST(DATEPART(DAY, StateChangeDate) as nvarchar(100))
		, CAST(DATEPART(HOUR, StateChangeDate) as nvarchar(100))
		, CAST(DATEPART(minute, StateChangeDate) as nvarchar(100))