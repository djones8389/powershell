select ID, ItemValue
	from PageTable
		where parentid = 972
			and ItemValue like '%0707mcge%'
			or ItemValue like '%110373%';
		
	--drop table #XMLStore	
		
select id, CAST(ItemValue as XML) as 'ItemValue'
	into #XMLStore
	from PageTable
		where parentid = 972
		
		
select 
	--h.u.value('.[1]','nvarchar(1000)')
	ID
	 ,c1.userName.value('.[1]', 'nvarchar(MAX)') as userName
	, c2.userId.value('.[1]', 'nvarchar(MAX)') as userId
	, c3.clientDataTime.value('.[1]', 'nvarchar(MAX)') as clientDataTime
	--, c4.clientDataTime.value('.[2]', 'nvarchar(MAX)') as clientDataTime1
	, ItemValue
 from #XMLStore		

cross apply ItemValue.nodes('/ItemValue/metaData/HISTORY/USER') h (u)		
cross apply h.u.nodes('userName') c1 (userName)
cross apply h.u.nodes('userId') c2 (userId)
cross apply h.u.nodes('clientDataTime') c3 (clientDataTime)
--cross apply h.u.nodes('clientDataTime') c4 (clientDataTime)

	where c1.userName.value ('.[1]', 'nvarchar(MAX)') = '0707mcge'
		or  c1.userName.value ('.[1]', 'nvarchar(MAX)') = '110373'
		order by ID
	
	
	
	
	
	
	select 	  
--	c1.centreID.value('.[1]', 'nvarchar(MAX)') as CentreID
    c2.centreName.value('.[1]', 'nvarchar(MAX)') as CentreName
,  c3.centreCode.value('.[1]', 'nvarchar(MAX)') as CentreCode
,  c4.installKey.value('.[1]', 'nvarchar(MAX)') as InstallationKey
,  c5.lastReportedVersion.value('.[1]', 'nvarchar(MAX)') as SoftwareVersion
,  c6.lastAuthenticationTime.value('.[1]', 'nvarchar(MAX)') as LastAuthenticationTime
,  c8.lastUploadTime.value('.[1]', 'nvarchar(MAX)') as LastAssessmentUploadTime
,  c9.numInstalled.value('.[1]', 'nvarchar(MAX)') as ClientInstallCount
,  c7.clockTimeDiff.value('.[1]', 'nvarchar(MAX)') as ClockTimeDifference_InSeconds

	from DJXML

	cross apply XMLStore.nodes('/parent/centre') as parent(centre)
	cross apply parent.centre.nodes('centreID') c1(centreID)
	cross apply parent.centre.nodes('centreName') c2(centreName)
	cross apply parent.centre.nodes('centreCode') c3(centreCode)
	cross apply parent.centre.nodes('installKey') c4(installKey)
	cross apply parent.centre.nodes('lastReportedVersion') c5(lastReportedVersion)
	cross apply parent.centre.nodes('lastAuthenticationTime') c6(lastAuthenticationTime)
	cross apply parent.centre.nodes('clockTimeDiff') c7(clockTimeDiff)
	cross apply parent.centre.nodes('lastUploadTime') c8(lastUploadTime)
	cross apply parent.centre.nodes('numInstalled') c9(numInstalled)


		
		select 
			UserID
			,  Username
			,  Forename
			,  Surname
		from UserTable 
		where UserID in (1063,1948);