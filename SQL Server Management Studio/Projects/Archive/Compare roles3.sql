/****** Script for SelectTopNRows command from SSMS  ******/
SELECT *
  FROM [UAT_BCGuilds_SecureAssess_11_7_223_0_IP].[dbo].RolePermissionsTable




SELECT A.*, u.Username, r.Name
  FROM [UAT_BC2_SecureAssess].[dbo].AssignedUserRolesTable  A
  INNER JOIN [UAT_BC2_SecureAssess].[dbo].UserTable U
  ON U.id = A.UserID
  INNER JOIN [UAT_BC2_SecureAssess].[dbo].RolesTable R
  ON R.id = A.RoleID
WHERE U.Username IN  ('ServiceUser_BritishCouncil', 'superuser')
	AND A.RoleID IN (SELECT id FROM [UAT_BC2_SecureAssess].[dbo].RolesTable)
	AND centreid = 1
--EXCEPT

SELECT A.*, u.Username, r.Name
  FROM [UAT_BCGuilds_SecureAssess_11_7_223_0_IP].[dbo].AssignedUserRolesTable A
  INNER JOIN [UAT_BCGuilds_SecureAssess_11_7_223_0_IP].[dbo].UserTable U
  ON U.id = A.UserID
  INNER JOIN [UAT_BCGuilds_SecureAssess_11_7_223_0_IP].[dbo].RolesTable R
  ON R.id = A.RoleID
WHERE U.Username IN  ('ServiceUser_BritishCouncil', 'superuser')
	AND A.RoleID IN (SELECT id FROM [UAT_BCGuilds_SecureAssess_11_7_223_0_IP].[dbo].RolesTable)
	AND centreid = 1




SELECT *
  FROM [UAT_BC2_SecureAssess].[dbo].CentreTable WHERE id = 30

SELECT *
  FROM [UAT_BCGuilds_SecureAssess_11_7_223_0_IP].[dbo].CentreTable WHERE id = 31
--EXCEPT

SELECT *
  FROM [UAT_BCGuilds_SecureAssess_11_7_223_0_IP].[dbo].RolesTable --WHERE id > 113




UserID	RoleID	CentreID
1081	2		1
1081	18		1
1081	101		1
1081	113		1

1		109		30
1		110		30
1		114		30