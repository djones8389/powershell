IF OBJECT_ID('tempdb..#SchedulesToDelete') IS NOT NULL DROP TABLE #SchedulesToDelete;

SELECT  WSCET.ID as ScheduleID, WEST.ID as ExamSessionID, '' As BeenDeleted
INTO #SchedulesToDelete
FROM WAREHOUSE_ExamSessionTable as WEST (NOLOCK)

  Inner Join WAREHOUSE_ScheduledExamsTable as WSCET (NOLOCK)
  on WSCET.ID = WEST.WAREHOUSEScheduledExamID
  
 where PreviousExamState = 10 
	and warehouseTime <= DATEADD (month , -6 , GETDATE())
	and reMarkStatus != 2		--Check this
  order by warehouseTime asc;
 

--select * from #SchedulesToDelete  where BeenDeleted = ''
  
DECLARE @Counter int = (SELECT COUNT(ExamSessionID) from #SchedulesToDelete where BeenDeleted = '');	
DECLARE @Increment smallint = 200;
DECLARE @DeletingIncrement TABLE ( 

	ESID INT

);

WHILE @Counter >= 37785

BEGIN 

INSERT @DeletingIncrement
SELECT TOP (@Increment) ExamSessionID from #SchedulesToDelete where BeenDeleted = '';


	Delete FROM Warehouse_ExamSessionAvailableItemsTable WHERE EXAMSESSIONID IN (SELECT ESID from @DeletingIncrement);
	Delete FROM WAREHOUSE_ExamSessionItemResponseTable WHERE WarehouseEXAMSESSIONID IN (SELECT ESID from @DeletingIncrement);
	Delete FROM WAREHOUSE_ExamSessionResultHistoryTable  WHERE WarehouseEXAMSESSIONTableID IN (SELECT ESID from @DeletingIncrement);
	Delete FROM WAREHOUSE_ExamSessionTable_ShreddedItems_Mark WHERE EXAMSESSIONID IN  (SELECT ESID from @DeletingIncrement);
	Delete FROM WAREHOUSE_ExamSessionTable_ShrededItems WHERE EXAMSESSIONID IN (SELECT ESID from @DeletingIncrement);
	Delete FROM WAREHOUSE_ExamStateAuditTable WHERE WarehouseEXAMSESSIONID IN (SELECT ESID from @DeletingIncrement);

	Delete FROM WAREHOUSE_ExamSessionDocumentTable WHERE WarehouseEXAMSESSIONID IN (SELECT ESID from @DeletingIncrement);
	Delete FROM WAREHOUSE_ExamSessionTable WHERE ID IN (SELECT ESID from @DeletingIncrement);
	Delete FROM WAREHOUSE_ExamSessionTable_Shreded WHERE ExamSessionID IN (SELECT ESID from @DeletingIncrement);
	Delete FROM WAREHOUSE_ScheduledExamsTable WHERE ID IN (SELECT ESID from @DeletingIncrement);

	UPDATE  #SchedulesToDelete
	SET BeenDeleted = 1
	Where ExamSessionID in (select ESID FROM @DeletingIncrement)
	
	SET @Counter -= @Increment;
	PRINT @Counter;

END
  
  
  
--SELECT WEST.ID, WEST.KeyCode, WUT.Forename, WUT.Surname, WUT.CandidateRef, WCT.CentreName, WSCET.examName, WEST.completionDate
--	, WESTS.ExternalReference, wscet.id	
--FROM WAREHOUSE_ExamSessionTable as WEST (NOLOCK)

--  Inner Join WAREHOUSE_ScheduledExamsTable as WSCET (NOLOCK)
--  on WSCET.ID = WEST.WAREHOUSEScheduledExamID
  
--  Inner Join WAREHOUSE_UserTable as WUT (NOLOCK)
--  on WUT.ID = WEST.WAREHOUSEUserID

--  Inner Join WAREHOUSE_CentreTable as WCT (NOLOCK)
--  on WCT.ID = WSCET.WAREHOUSECentreID

--  Inner Join WAREHOUSE_ExamSessionTable_Shreded as WESTS (NOLOCK)
--  on WESTS.examSessionId = WEST.ID

--where west.ID = 160429
--	or wests.examSessionId = 160429
  
  --DELETE FROM WAREHOUSE_ScheduledExamsTable WHERE ID = 160429
  --select * from WAREHOUSE_ExamSessionTable_Shreded where examsessionid = 160429
  
  
  --Delete 5,000 via the above method:  
  
  --Update SchedulesToDelete
  --SET BeenDeleted = 1;
  
  
  
  
  
  
  --SELECT * FROM #SchedulesToDelete where BeenDeleted = 1
    
  
--SELECT * from #SchedulesToDelete

--SELECT * from #SchedulesToDeletewhere BeenDeleted = '');


--CREATE TABLE #SchedulesToDelete

--	(
--		ScheduleID int
--		, ExamSessionID int
--		, BeenDeleted bit
--	);

--DECLARE @SchdulesToDeleteTABLE 
--	(
--		ScheduleID int
--		, ExamSessionID int
--		, BeenDeleted bit
--	);
--INSERT @SchdulesToDelete(ScheduleID, ExamSessionID)	

