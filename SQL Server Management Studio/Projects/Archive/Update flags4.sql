begin tran

declare @totalMark int = '50'
declare @NewValue int = 0
declare @myDecimal decimal(10,3) = '0.000'
declare @esid int = 107839

select StructureXML, resultData, resultDataFull from WAREHOUSE_ExamSessionTable where ID = @esid


select
	sum(s.i.value('@totalMark','int')) as sumTotalMark
from WAREHOUSE_ExamSessionTable 

cross apply resultData.nodes('/exam/section/item') as s(i)

where ID = @esid


--WEST.ResultData--

	--SectionLevel--

	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/@userMark)[1] with sql:variable("@NewValue") ')    
	where id = @esid

	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid

	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/@totalMark)[1] with sql:variable("@totalMark") ')    
	where id = @esid

	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/@userPercentage)[1] with sql:variable("@NewValue") ')    
	where id = @esid
	
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid

	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/@userMark)[1] with sql:variable("@NewValue") ')    
	where id = @esid

	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid

	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/@userPercentage)[1] with sql:variable("@NewValue") ')    
	where id = @esid
	
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid

	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/@totalMark)[1] with sql:variable("@totalMark") ')    
	where id = @esid
	--ItemLevel--
	
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@markerUserMark)[1] with sql:variable("@NewValue") ')    
	where id = @esid
	
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@markerUserMark)[2] with sql:variable("@NewValue") ')    
	where id = @esid
		
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@markerUserMark)[3] with sql:variable("@NewValue") ')    
	where id = @esid
	
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@markerUserMark)[4] with sql:variable("@NewValue") ')    
	where id = @esid	
	
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@markerUserMark)[5] with sql:variable("@NewValue") ')    
	where id = @esid
	
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@markerUserMark)[6] with sql:variable("@NewValue") ')    
	where id = @esid
		
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@markerUserMark)[7] with sql:variable("@NewValue") ')    
	where id = @esid
	
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@markerUserMark)[8] with sql:variable("@NewValue") ')    
	where id = @esid
		
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@markerUserMark)[9] with sql:variable("@NewValue") ')    
	where id = @esid
	
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@markerUserMark)[10] with sql:variable("@NewValue") ')    
	where id = @esid	
	
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@actualUserMark)[1] with sql:variable("@NewValue") ')    
	where id = @esid
		
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@actualUserMark)[2] with sql:variable("@NewValue") ')    
	where id = @esid
	
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@actualUserMark)[3] with sql:variable("@NewValue") ')    
	where id = @esid
		
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@actualUserMark)[4] with sql:variable("@NewValue") ')    
	where id = @esid
		
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@actualUserMark)[5] with sql:variable("@NewValue") ')    
	where id = @esid
		
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@actualUserMark)[6] with sql:variable("@NewValue") ')    
	where id = @esid
		
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@actualUserMark)[7] with sql:variable("@NewValue") ')    
	where id = @esid
		
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@actualUserMark)[8] with sql:variable("@NewValue") ')    
	where id = @esid
		
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
		
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@actualUserMark)[9] with sql:variable("@NewValue") ')    
	where id = @esid
		
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@actualUserMark)[10] with sql:variable("@NewValue") ')    
	where id = @esid
	
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@userMark)[1] with sql:variable("@NewValue") ')    
	where id = @esid
		
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@userMark)[9] with sql:variable("@NewValue") ')    
	where id = @esid
		
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@userMark)[10] with sql:variable("@NewValue") ')    
	where id = @esid	
	
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid

	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@userMark)[2] with sql:variable("@NewValue") ')    
	where id = @esid
		
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@userMark)[3] with sql:variable("@NewValue") ')    
	where id = @esid
		
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@userMark)[4] with sql:variable("@NewValue") ')    
	where id = @esid
		
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@userMark)[5] with sql:variable("@NewValue") ')    
	where id = @esid
		
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@userMark)[6] with sql:variable("@NewValue") ')    
	where id = @esid
		
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@userMark)[7] with sql:variable("@NewValue") ')    
	where id = @esid
		
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@userMark)[8] with sql:variable("@NewValue") ')    
	where id = @esid

	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@viewingTime)[1] with sql:variable("@NewValue") ')    
	where id = @esid
		
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@viewingTime)[2] with sql:variable("@NewValue") ')    
	where id = @esid
	
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@viewingTime)[3] with sql:variable("@NewValue") ')    
	where id = @esid
		
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@viewingTime)[4] with sql:variable("@NewValue") ')    
	where id = @esid
		
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@viewingTime)[5] with sql:variable("@NewValue") ')    
	where id = @esid
		
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@viewingTime)[6] with sql:variable("@NewValue") ')    
	where id = @esid
		
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@viewingTime)[7] with sql:variable("@NewValue") ')    
	where id = @esid
		
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@viewingTime)[8] with sql:variable("@NewValue") ')    
	where id = @esid
		
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@viewingTime)[9] with sql:variable("@NewValue") ')    
	where id = @esid
		
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@viewingTime)[10] with sql:variable("@NewValue") ')    
	where id = @esid
	
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@userAttempted)[1] with sql:variable("@NewValue") ')    
	where id = @esid
	
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@userAttempted)[2] with sql:variable("@NewValue") ')    
	where id = @esid
		
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@userAttempted)[3] with sql:variable("@NewValue") ')    
	where id = @esid
	
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@userAttempted)[4] with sql:variable("@NewValue") ')    
	where id = @esid
		
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@userAttempted)[5] with sql:variable("@NewValue") ')    
	where id = @esid
	
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@userAttempted)[6] with sql:variable("@NewValue") ')    
	where id = @esid
		
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@userAttempted)[7] with sql:variable("@NewValue") ')    
	where id = @esid
	
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@userAttempted)[8] with sql:variable("@NewValue") ')    
	where id = @esid
		
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@userAttempted)[9] with sql:variable("@NewValue") ')    
	where id = @esid
	
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@userAttempted)[10] with sql:variable("@NewValue") ')    
	where id = @esid

--WEST.ResultDataFull--

	--SectionLevel--
	
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid

	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/@totalMark)[1] with sql:variable("@totalMark") ')    
	where id = @esid
	
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid

	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/@totalMark)[1] with sql:variable("@totalMark") ')    
	where id = @esid
	
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/@userMark)[1] with sql:variable("@NewValue") ')    
	where id = @esid
	
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/@userPercentage)[1] with sql:variable("@NewValue") ')    
	where id = @esid
	
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/@userMark)[1] with sql:variable("@NewValue") ')    
	where id = @esid
	
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid

	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/@userPercentage)[1] with sql:variable("@NewValue") ')    
	where id = @esid

	--ItemLevel--
	--UserMark--
	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@userMark)[1] with sql:variable("@NewValue") ')    
	where id = @esid
	
	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@userMark)[2] with sql:variable("@NewValue") ')    
	where id = @esid

	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@userMark)[3] with sql:variable("@NewValue") ')    
	where id = @esid
	
	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@userMark)[4] with sql:variable("@NewValue") ')    
	where id = @esid

	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@userMark)[5] with sql:variable("@NewValue") ')    
	where id = @esid
	
	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@userMark)[6] with sql:variable("@NewValue") ')    
	where id = @esid

	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@userMark)[7] with sql:variable("@NewValue") ')    
	where id = @esid
	
	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@userMark)[8] with sql:variable("@NewValue") ')    
	where id = @esid

	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@userMark)[9] with sql:variable("@NewValue") ')    
	where id = @esid
	
	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@userMark)[10] with sql:variable("@NewValue") ')    
	where id = @esid

	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@userAttempted)[1] with sql:variable("@NewValue") ')    
	where id = @esid
	
	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@userAttempted)[2] with sql:variable("@NewValue") ')    
	where id = @esid
	
	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@userAttempted)[3] with sql:variable("@NewValue") ')    
	where id = @esid
	
	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@userAttempted)[4] with sql:variable("@NewValue") ')    
	where id = @esid
	
	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@userAttempted)[5] with sql:variable("@NewValue") ')    
	where id = @esid
	
	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@userAttempted)[6] with sql:variable("@NewValue") ')    
	where id = @esid
	
	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@userAttempted)[7] with sql:variable("@NewValue") ')    
	where id = @esid
	
	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@userAttempted)[8] with sql:variable("@NewValue") ')    
	where id = @esid
	
	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@userAttempted)[9] with sql:variable("@NewValue") ')    
	where id = @esid
	
	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@userAttempted)[10] with sql:variable("@NewValue") ')    
	where id = @esid

	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@viewingTime)[1] with sql:variable("@NewValue") ')    
	where id = @esid
	
	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@viewingTime)[2] with sql:variable("@NewValue") ')    
	where id = @esid
	
	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@viewingTime)[3] with sql:variable("@NewValue") ')    
	where id = @esid

	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@viewingTime)[4] with sql:variable("@NewValue") ')    
	where id = @esid
	
	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@viewingTime)[5] with sql:variable("@NewValue") ')    
	where id = @esid
	
	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@viewingTime)[6] with sql:variable("@NewValue") ')    
	where id = @esid
	
	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@viewingTime)[7] with sql:variable("@NewValue") ')    
	where id = @esid
	
	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@viewingTime)[8] with sql:variable("@NewValue") ')    
	where id = @esid
	
	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@viewingTime)[9] with sql:variable("@NewValue") ')    
	where id = @esid
	
	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@viewingTime)[10] with sql:variable("@NewValue") ')    
	where id = @esid
	
select StructureXML, resultData, resultDataFull from WAREHOUSE_ExamSessionTable where ID = @esid

Rollback












--WEST.StructureXML--

	--update WAREHOUSE_ExamSessionTable
	--set StructureXML.modify('replace value of (//item/@totalMark)[1] with sql:variable("@myDecimal") ')    
	--where id = @esid
	
	--update WAREHOUSE_ExamSessionTable
	--set StructureXML.modify('replace value of (//item/@userAttempted)[1] with sql:variable("@NewValue") ')    
	--where id = @esid
	
	--update WAREHOUSE_ExamSessionTable
	--set StructureXML.modify('replace value of (//item/@markerUserMark)[1] with sql:variable("@NewValue") ')    
	--where id = @esid

	--update WAREHOUSE_ExamSessionTable
	--set StructureXML.modify('replace value of (//item/@viewingTime)[1] with sql:variable("@NewValue") ')    
	--where id = @esid