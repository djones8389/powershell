SELECT *
FROM WAREHOUSE_ExamSessionTable
WHERE 
      ExportToSecureMarker = 1 
      and WarehouseExamState = 1 
      AND NOT EXISTS(
            SELECT *
            FROM WAREHOUSE_ExamStateAuditTable
            WHERE WAREHOUSE_ExamStateAuditTable.WarehouseExamSessionID = WAREHOUSE_ExamSessionTable.ID);
            

SELECT WAREHOUSE_ExamSessionTable.ID, WAREHOUSE_ExamSessionTable.KeyCode
FROM WAREHOUSE_ExamSessionTable
INNER JOIN [430327-AAT-SQL2\SQL2].AAT_SecureMarker.dbo.CandidateExamVersions as CEV
ON CEV.ExternalSessionID = WAREHOUSE_ExamSessionTable.ID
WHERE WAREHOUSE_ExamSessionTable.WarehouseExamState = 3 AND CEV.ExamSessionState > 3;




SELECT userMark, userPercentage
FROM WAREHOUSE_ExamSessionTable_Shreded
INNER JOIN [430327-AAT-SQL2\SQL2].AAT_SecureMarker.dbo.CandidateExamVersions as CEV
ON CEV.ExternalSessionID = WAREHOUSE_ExamSessionTable_Shreded.examSessionId
WHERE WAREHOUSE_ExamSessionTable_Shreded.WarehouseExamState = 4 AND CEV.ExamSessionState = 4;
