use AAT_SecureAssess

select est.ID, examState, KeyCode, CentreName
from ExamSessionTable as EST

inner join ScheduledExamsTable as SCT
on sct.ID = EST.ScheduledExamID

Inner Join CentreTable as CT
on CT.ID = SCT.CentreID

Where examState = 8 and CentreName = 'Botswana Accountancy College'