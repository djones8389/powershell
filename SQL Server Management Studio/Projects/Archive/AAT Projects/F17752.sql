SELECT WEST.ID, WEST.KeyCode, WUT.Forename, WUT.Surname, WUT.CandidateRef, WCT.CentreName, WSCET.examName, WEST.completionDate
	, WESTS.ExternalReference, west.StructureXML ,wests.userMark, rest.*
FROM WAREHOUSE_ExamSessionTable as WEST

  Inner Join WAREHOUSE_ScheduledExamsTable as WSCET
  on WSCET.ID = WEST.WAREHOUSEScheduledExamID
  
  Inner Join WAREHOUSE_UserTable as WUT
  on WUT.ID = WEST.WAREHOUSEUserID

  Inner Join WAREHOUSE_CentreTable as WCT
  on WCT.ID = WSCET.WAREHOUSECentreID

  Inner Join WAREHOUSE_ExamSessionTable_Shreded as WESTS
  on WESTS.examSessionId = WEST.ID
 
  inner join ReMarkExamSessionTable as REST
  on REST.WarehouseID = west.ID

where west.ID = 1366283


select * from ReMarkExamSessionTable where WarehouseID = 1366283

begin tran
/*
update ReMarkExamSessionTable 
set StructureXML = '<assessmentDetails>
  <assessmentID>2029</assessmentID>
  <qualificationID>95</qualificationID>
  <qualificationName>Personal tax (AQ2013)</qualificationName>
  <qualificationReference>AATQCF</qualificationReference>
  <assessmentGroupName>PTAX FA 2013</assessmentGroupName>
  <assessmentGroupID>1914</assessmentGroupID>
  <assessmentGroupReference>CAPTAX</assessmentGroupReference>
  <assessmentName>PTAX FA 2013</assessmentName>
  <validFromDate>06 Jan 2014</validFromDate>
  <expiryDate>31 Jul 2014</expiryDate>
  <startTime>00:00:00</startTime>
  <endTime>23:59:59</endTime>
  <duration>120</duration>
  <defaultDuration>120</defaultDuration>
  <scheduledDuration>
    <value>120</value>
    <reason></reason>
  </scheduledDuration>
  <sRBonus>0</sRBonus>
  <sRBonusMaximum>40</sRBonusMaximum>
  <externalReference>CAPTAX</externalReference>
  <passLevelValue>70</passLevelValue>
  <passLevelType>1</passLevelType>
  <status>2</status>
  <testFeedbackType>
    <passFail>0</passFail>
    <percentageMark>0</percentageMark>
    <allowSummaryFeedback>0</allowSummaryFeedback>
    <summaryFeedbackType>1</summaryFeedbackType>
    <itemSummary>0</itemSummary>
    <itemReview>0</itemReview>
    <itemReviewCorrectAnswer>0</itemReviewCorrectAnswer>
    <itemFeedback>0</itemFeedback>
    <printableSummary>0</printableSummary>
    <candidateDetails>0</candidateDetails>
    <feedbackByReference>0</feedbackByReference>
    <allowFeedbackDuringExam>0</allowFeedbackDuringExam>
    <allowFeedbackDuringSummary>0</allowFeedbackDuringSummary>
  </testFeedbackType>
  <itemFeedback>0</itemFeedback>
  <allowCalculator>0</allowCalculator>
  <lastInUseDate>06 Dec 2013 11:42:24</lastInUseDate>
  <completedScriptReview>0</completedScriptReview>
  <assessment currentSection="1" totalTime="0" currentTime="0">
    <intro id="0" name="Introduction" currentItem="">
      <item id="924P1161" name="Intro PTAX" totalMark="1" version="19" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" LO="Introduction" unit="Personal Tax" />
    </intro>
    <outro id="0" name="Finish" currentItem="" />
    <tools id="0" name="Tools" currentItem="">
      <item id="924P971" name="PTAX FA2013 Pop up 1" toolName="PTAX FA2013 Pop up 1" totalMark="1" version="7" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" LO="Pop-up" unit="Personal Tax" />
      <item id="924P972" name="PTAX FA2013 Pop up 2" toolName="PTAX FA2013 Pop up 2" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" LO="Pop-up" unit="Personal Tax" />
    </tools>
    <section id="1" name="1" passLevelValue="70" passLevelType="1" itemsToMark="0" currentItem="924P1078" fixed="0">
      <item id="924P985" name="01 PTAX FA13 SY 001" totalMark="9" version="30" markingType="0" markingState="0" userMark="1" markerUserMark="" userAttempted="1" viewingTime="274717" flagged="0" LO="01. Benefits in Kind � provision of cars" unit="Personal Tax" quT="10,11" />
      <item id="924P999" name="02 PTAX FA13 SY 001" totalMark="10" version="25" markingType="0" markingState="0" userMark="0.8" markerUserMark="" userAttempted="1" viewingTime="377568" flagged="0" LO="02. Benefits in Kind � all excluding cars" unit="Personal Tax" quT="10,11" />
      <item id="924P1138" name="03 PTAX FA13 KB 005" totalMark="10" version="18" markingType="0" markingState="0" userMark="0.8" markerUserMark="" userAttempted="1" viewingTime="194828" flagged="0" LO="03. Income from property" unit="Personal Tax" quT="11,12" />
      <item id="924P1076" name="04 PTAX FA13 SY 004" totalMark="6" version="24" markingType="0" markingState="0" userMark="1" markerUserMark="" userAttempted="1" viewingTime="119215" flagged="0" LO="04. Investment income" unit="Personal Tax" quT="11,12" />
      <item id="924P1078" name="05 PTAX FA13 SY 003" totalMark="12" version="30" markingType="0" markingState="0" userMark="0.83333333" markerUserMark="" userAttempted="1" viewingTime="636247" flagged="1" LO="05. Computation of total and taxable income" unit="Personal Tax" quT="11" />
      <item id="924P1102" name="06 PTAX FA13 KB 003" totalMark="10" version="27" markingType="1" markingState="1" userMark="0.875" markerUserMark="7" userAttempted="1" viewingTime="981585" flagged="0" LO="06. Computation of tax payable and payment of tax" unit="Personal Tax" quT="11,20" />
      <item id="924P1090" name="07 PTAX FA13 KB 002" totalMark="10" version="31" markingType="1" markingState="1" userMark="0" markerUserMark="5" userAttempted="1" viewingTime="564143" flagged="0" LO="07. Theory underpinning topic and penalties" unit="Personal Tax" quT="11" />
      <item id="924P1095" name="08 PTAX FA13 SY 001" totalMark="7" version="33" markingType="1" markingState="1" userMark="0" markerUserMark="1" userAttempted="1" viewingTime="112991" flagged="0" LO="08. Tax returns" unit="Personal Tax" quT="11" />
      <item id="924P1085" name="09 PTAX FA13 SY 004" totalMark="12" version="26" markingType="0" markingState="0" userMark="0.58333333" markerUserMark="" userAttempted="1" viewingTime="338786" flagged="0" LO="09. Basics of capital gains tax" unit="Personal Tax" quT="10,11,12" />
      <item id="924P1092" name="10 PTAX FA13 KB 002" totalMark="8" version="19" markingType="1" markingState="0" userMark="1" markerUserMark="" userAttempted="1" viewingTime="3135" flagged="0" LO="10. Taxation of shares " unit="Personal Tax" quT="20" />
      <item id="924P1039" name="11 PTAX FA13 SY 004" totalMark="6" version="20" markingType="0" markingState="0" userMark="0.33333333" markerUserMark="" userAttempted="1" viewingTime="462229" flagged="0" LO="11. Capital gains tax exemptions, losses, reliefs and tax payable" unit="Personal Tax" quT="11" />
    </section>
  </assessment>
  <isValid>1</isValid>
  <isPrintable>0</isPrintable>
  <qualityReview>0</qualityReview>
  <numberOfGenerations>1</numberOfGenerations>
  <requiresInvigilation>1</requiresInvigilation>
  <advanceContentDownloadTimespan>60</advanceContentDownloadTimespan>
  <automaticVerification>0</automaticVerification>
  <offlineMode>2</offlineMode>
  <requiresValidation>0</requiresValidation>
  <requiresSecureClient>1</requiresSecureClient>
  <examType>0</examType>
  <advanceDownload>0</advanceDownload>
  <gradeBoundaryData>
    <gradeBoundaries passLevelType="1" adjustedGradeBoundaries="0">
      <grade modifier="lt" value="70" description="Fail" />
      <grade modifier="gt" value="70" description="Pass" />
    </gradeBoundaries>
  </gradeBoundaryData>
  <autoViewExam>0</autoViewExam>
  <autoProgressItems>0</autoProgressItems>
  <confirmationText>
    <confirmationText></confirmationText>
  </confirmationText>
  <requiresConfirmationCheck>0</requiresConfirmationCheck>
  <allowCloseWithoutSubmit>0</allowCloseWithoutSubmit>
  <useSecureMarker>0</useSecureMarker>
  <annotationVersion>0</annotationVersion>
  <allowPackagingDelivery>1</allowPackagingDelivery>
  <scoreBoundaryData>
    <scoreBoundaries showScoreBoundaryColumn="1">
      <score modifier="lt" value="36" description="Significantly below" higherBoundarySet="1" />
      <score modifier="lt" value="36" description="Significantly below" higherBoundarySet="0" />
      <score modifier="gt" value="36" description="Below requirement" higherBoundarySet="1" />
      <score modifier="gt" value="60" description="Borderline" higherBoundarySet="1" />
      <score modifier="gt" value="70" description="Met" higherBoundarySet="0" />
      <score modifier="gt" value="70" description="Met" higherBoundarySet="1" />
      <score modifier="gt" value="36" description="Below requirement" higherBoundarySet="0" userCreated="1" />
      <score modifier="gt" value="60" description="Borderline" higherBoundarySet="0" userCreated="1" />
      <score modifier="gt" value="85" description="Exceeded" higherBoundarySet="1" userCreated="1" />
    </scoreBoundaries>
  </scoreBoundaryData>
  <showCandidateReportResultColumn>0</showCandidateReportResultColumn>
  <showCandidateReportPercentageColumn>1</showCandidateReportPercentageColumn>
  <certifiedAccessible>0</certifiedAccessible>
  <markingProgressVisible>1</markingProgressVisible>
  <markingProgressMode>0</markingProgressMode>
  <secureClientOperationMode>1</secureClientOperationMode>
  <examDeliverySystemStyleType>delivery</examDeliverySystemStyleType>
  <markingAutoVoidPeriod>365</markingAutoVoidPeriod>
  <showPageRequiresScrollingAlert>0</showPageRequiresScrollingAlert>
  <project ID="924" />
</assessmentDetails>', ResultData = '<exam passMark="70" passType="1" originalPassMark="70" originalPassType="1" totalMark="100" userMark="71.000" userPercentage="71.000" passValue="1" originalPassValue="1" totalTimeSpent="4065444" closeBoundaryType="0" closeBoundaryValue="0" grade="Pass" originalGrade="Pass">
  <gradeBoundaryData>
    <gradeBoundaries passLevelType="1" adjustedGradeBoundaries="0">
      <grade modifier="lt" value="70" description="Fail" />
      <grade modifier="gt" value="70" description="Pass" />
    </gradeBoundaries>
  </gradeBoundaryData>
  <section id="1" passMark="70" passType="1" name="1" totalMark="100" userMark="71.000" userPercentage="71.000" passValue="1" totalTimeSpent="4065444" itemsToMark="0">
    <item id="924P985" name="01 PTAX FA13 SY 001" totalMark="9" userMark="1" actualUserMark="9" markingType="0" markerUserMark="" viewingTime="274717" userAttempted="1" version="30" />
    <item id="924P999" name="02 PTAX FA13 SY 001" totalMark="10" userMark="0.8" actualUserMark="8.0" markingType="0" markerUserMark="" viewingTime="377568" userAttempted="1" version="25" />
    <item id="924P1138" name="03 PTAX FA13 KB 005" totalMark="10" userMark="0.8" actualUserMark="8.0" markingType="0" markerUserMark="" viewingTime="194828" userAttempted="1" version="18" />
    <item id="924P1076" name="04 PTAX FA13 SY 004" totalMark="6" userMark="1" actualUserMark="6" markingType="0" markerUserMark="" viewingTime="119215" userAttempted="1" version="24" />
    <item id="924P1078" name="05 PTAX FA13 SY 003" totalMark="12" userMark="0.83333333" actualUserMark="9.99999996" markingType="0" markerUserMark="" viewingTime="636247" userAttempted="1" version="30" />
    <item id="924P1102" name="06 PTAX FA13 KB 003" totalMark="10" userMark="0" actualUserMark="7" markingType="1" markerUserMark="7" viewingTime="981585" userAttempted="1" version="27" />
    <item id="924P1090" name="07 PTAX FA13 KB 002" totalMark="10" userMark="0" actualUserMark="5" markingType="1" markerUserMark="5" viewingTime="564143" userAttempted="1" version="31" />
    <item id="924P1095" name="08 PTAX FA13 SY 001" totalMark="7" userMark="0" actualUserMark="1" markingType="1" markerUserMark="1" viewingTime="112991" userAttempted="1" version="33" />
    <item id="924P1085" name="09 PTAX FA13 SY 004" totalMark="12" userMark="0.58333333" actualUserMark="6.99999996" markingType="0" markerUserMark="" viewingTime="338786" userAttempted="1" version="26" />
    <item id="924P1092" name="10 PTAX FA13 KB 002" totalMark="8" userMark="1" actualUserMark="8" markingType="1" markerUserMark="" viewingTime="3135" userAttempted="1" version="19" />
    <item id="924P1039" name="11 PTAX FA13 SY 004" totalMark="6" userMark="0.33333333" actualUserMark="1.99999998" markingType="0" markerUserMark="" viewingTime="462229" userAttempted="1" version="20" />
  </section>
</exam>', ResultDataFull = '<assessmentDetails>
  <assessmentID>2029</assessmentID>
  <qualificationID>95</qualificationID>
  <qualificationName>Personal tax (AQ2013)</qualificationName>
  <qualificationReference>AATQCF</qualificationReference>
  <assessmentGroupName>PTAX FA 2013</assessmentGroupName>
  <assessmentGroupID>1914</assessmentGroupID>
  <assessmentGroupReference>CAPTAX</assessmentGroupReference>
  <assessmentName>PTAX FA 2013</assessmentName>
  <validFromDate>06 Jan 2014</validFromDate>
  <expiryDate>31 Jul 2014</expiryDate>
  <startTime>00:00:00</startTime>
  <endTime>23:59:59</endTime>
  <duration>120</duration>
  <defaultDuration>120</defaultDuration>
  <scheduledDuration>
    <value>120</value>
    <reason />
  </scheduledDuration>
  <sRBonus>0</sRBonus>
  <sRBonusMaximum>40</sRBonusMaximum>
  <externalReference>CAPTAX</externalReference>
  <passLevelValue>70</passLevelValue>
  <passLevelType>1</passLevelType>
  <status>2</status>
  <testFeedbackType>
    <passFail>0</passFail>
    <percentageMark>0</percentageMark>
    <allowSummaryFeedback>0</allowSummaryFeedback>
    <summaryFeedbackType>1</summaryFeedbackType>
    <itemSummary>0</itemSummary>
    <itemReview>0</itemReview>
    <itemReviewCorrectAnswer>0</itemReviewCorrectAnswer>
    <itemFeedback>0</itemFeedback>
    <printableSummary>0</printableSummary>
    <candidateDetails>0</candidateDetails>
    <feedbackByReference>0</feedbackByReference>
    <allowFeedbackDuringExam>0</allowFeedbackDuringExam>
    <allowFeedbackDuringSummary>0</allowFeedbackDuringSummary>
  </testFeedbackType>
  <itemFeedback>0</itemFeedback>
  <allowCalculator>0</allowCalculator>
  <lastInUseDate>Jul 14 2014  3:56PM</lastInUseDate>
  <completedScriptReview>0</completedScriptReview>
  <assessment currentSection="1" totalTime="0" currentTime="0" originalPassMark="70" originalPassType="1" passMark="70" passType="1" totalMark="100" userMark="71.000" userPercentage="71.000" passValue="1" originalGrade="Pass">
    <intro id="0" name="Introduction" currentItem="">
      <item id="924P1161" name="Intro PTAX" totalMark="1" version="19" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" LO="Introduction" unit="Personal Tax" />
    </intro>
    <outro id="0" name="Finish" currentItem="" />
    <tools id="0" name="Tools" currentItem="">
      <item id="924P971" name="PTAX FA2013 Pop up 1" toolName="PTAX FA2013 Pop up 1" totalMark="1" version="7" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" LO="Pop-up" unit="Personal Tax" />
      <item id="924P972" name="PTAX FA2013 Pop up 2" toolName="PTAX FA2013 Pop up 2" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" LO="Pop-up" unit="Personal Tax" />
    </tools>
    <section id="1" name="1" passLevelValue="70" passLevelType="1" itemsToMark="0" currentItem="924P1078" fixed="0" totalMark="100" userMark="71.000" userPercentage="71.000" passValue="1">
      <item id="924P985" name="01 PTAX FA13 SY 001" totalMark="9" version="30" markingType="0" markingState="0" userMark="1" markerUserMark="" userAttempted="1" viewingTime="274717" flagged="0" LO="01. Benefits in Kind � provision of cars" unit="Personal Tax" quT="10,11">
        <p um="1" cs="1" ua="1" id="924P985">
          <s ua="0" um="-1" id="1">
            <c typ="4" id="14">
              <i cc="0" id="1" />
            </c>
            <c typ="4" id="18">
              <i cc="0" id="1" />
            </c>
          </s>
          <s ua="1" um="1" id="2">
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="2">
              <i ca="20" id="1">20</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="3">
              <i ca="" id="1">2140</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="4">
              <i ca="32" id="1">32</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="5">
              <i ca="" id="1">4960</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="6">
              <i ca="" id="1">2110</i>
            </c>
            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="15">
              <i ca="" id="1">4250</i>
            </c>
          </s>
          <s ua="1" um="1" id="3">
            <c wei="2" maS="2" ie="1" typ="10" rv="0" mv="0" ua="1" um="1" id="5">
              <i ac="A" sl="0" id="1" />
              <i ca="1" ac="B" sl="1" id="2" />
              <i ca="1" ac="C" sl="1" id="3" />
              <i ac="D" sl="0" id="4" />
            </c>
          </s>
        </p>
      </item>
      <item id="924P999" name="02 PTAX FA13 SY 001" totalMark="10" version="25" markingType="0" markingState="0" userMark="0.8" markerUserMark="" userAttempted="1" viewingTime="377568" flagged="0" LO="02. Benefits in Kind � all excluding cars" unit="Personal Tax" quT="10,11">
        <p um="0.8" cs="1" ua="1" id="924P999">
          <s ua="1" um="0.5" id="1">
            <c typ="4" id="8">
              <i cc="0" id="1" />
            </c>
            <c typ="4" id="12">
              <i cc="0" id="1" />
            </c>
            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="21">
              <i ca="200" id="1">200</i>
            </c>
            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="0" um="0" id="22">
              <i ca="" id="1" />
            </c>
          </s>
          <s ua="1" um="1" id="2">
            <c wei="5" maS="5" ie="1" typ="10" rv="0" mv="0" ua="1" um="1" id="27">
              <i ac="A" sl="0" id="1" />
              <i ac="J" sl="0" id="10" />
              <i ac="K" sl="0" id="11" />
              <i ca="1" ac="B" sl="1" id="2" />
              <i ac="C" sl="0" id="3" />
              <i ca="1" ac="D" sl="1" id="4" />
              <i ac="E" sl="0" id="5" />
              <i ca="1" ac="F" sl="1" id="6" />
              <i ca="1" ac="G" sl="1" id="7" />
              <i ca="1" ac="H" sl="1" id="8" />
              <i ac="I" sl="0" id="9" />
            </c>
          </s>
          <s ua="1" um="1" id="3">
            <c wei="1" maS="1" ie="1" typ="10" rv="0" mv="0" ua="1" um="1" id="4">
              <i ac="A" sl="0" id="1" />
              <i ca="1" ac="B" sl="1" id="2" />
              <i ac="C" sl="0" id="3" />
              <i ac="D" sl="0" id="4" />
            </c>
          </s>
        </p>
      </item>
      <item id="924P1138" name="03 PTAX FA13 KB 005" totalMark="10" version="18" markingType="0" markingState="0" userMark="0.8" markerUserMark="" userAttempted="1" viewingTime="194828" flagged="0" LO="03. Income from property" unit="Personal Tax" quT="11,12">
        <p um="0.8" cs="1" ua="1" id="924P1138">
          <s ua="1" um="0.88" id="1">
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="7">
              <i ca="5850" id="1">5850</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="8">
              <i ca="0" id="1">0</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="9">
              <i ca="949" id="1">984</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="10">
              <i ca="400" id="1">400</i>
            </c>
            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="11">
              <i ca="" id="1">372</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="12">
              <i ca="" id="1">2364</i>
            </c>
            <c typ="4" id="15">
              <i cc="0" id="1" />
            </c>
            <c typ="4" id="19">
              <i cc="0" id="1" />
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="23">
              <i ca="1730" id="1">1730</i>
            </c>
          </s>
          <s ua="1" um="0.5" id="2">
            <c wei="1" maS="0" ie="1" typ="12" rv="0" mv="0" ua="1" um="0" id="4">
              <i sl="5" id="1" />
            </c>
            <c wei="1" maS="0" ie="1" typ="12" rv="0" mv="0" ua="1" um="1" id="7">
              <i sl="2" id="1" />
            </c>
          </s>
        </p>
      </item>
      <item id="924P1076" name="04 PTAX FA13 SY 004" totalMark="6" version="24" markingType="0" markingState="0" userMark="1" markerUserMark="" userAttempted="1" viewingTime="119215" flagged="0" LO="04. Investment income" unit="Personal Tax" quT="11,12">
        <p um="1" cs="1" ua="1" id="924P1076">
          <s ua="1" um="1" id="1">
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="7">
              <i ca="" id="1">140</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="8">
              <i ca="" id="1">460</i>
            </c>
            <c typ="4" id="11">
              <i cc="0" id="1" />
            </c>
            <c typ="4" id="15">
              <i cc="1" id="1" />
            </c>
            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="22">
              <i ca="" id="1">315</i>
            </c>
            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="25">
              <i ca="2300" id="1">2300</i>
            </c>
            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="26">
              <i ca="1400" id="1">1400</i>
            </c>
            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="27">
              <i ca="" id="1">460</i>
            </c>
            <c wei="2" maS="0" ie="1" typ="12" rv="0" mv="0" ua="1" um="1" id="32">
              <i sl="3" id="1" />
            </c>
          </s>
        </p>
      </item>
      <item id="924P1078" name="05 PTAX FA13 SY 003" totalMark="12" version="30" markingType="0" markingState="0" userMark="0.83333333" markerUserMark="" userAttempted="1" viewingTime="636247" flagged="1" LO="05. Computation of total and taxable income" unit="Personal Tax" quT="11">
        <p um="0.833333333333333" cs="1" ua="1" id="924P1078">
          <s ua="0" um="-1" id="1">
            <c typ="4" id="12">
              <i cc="0" id="1" />
            </c>
            <c typ="4" id="16">
              <i cc="0" id="1" />
            </c>
          </s>
          <s ua="1" um="1" id="2">
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="14">
              <i ca="0" id="1">0</i>
            </c>
            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="15">
              <i ca="29360" id="1">29360</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="16">
              <i ca="2488" id="1">2488</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="17">
              <i ca="3376" id="1">3376</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="18">
              <i ca="2440" id="1">2440</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="19">
              <i ca="0" id="1">0</i>
            </c>
            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="20">
              <i ca="" id="1">4404</i>
            </c>
            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="21">
              <i ca="250" id="1">250</i>
            </c>
            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="5">
              <i ca="750" id="1">750</i>
            </c>
            <c wei="4" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="6">
              <i ca="" id="2">32760</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="26">
              <i ca="0" id="1">0</i>
            </c>
          </s>
          <s ua="1" um="0.33" id="3">
            <c wei="4" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="9">
              <i ca="11285" id="1">11025</i>
            </c>
            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="10">
              <i ca="11025" id="1">11025</i>
            </c>
          </s>
        </p>
      </item>
      <item id="924P1102" name="06 PTAX FA13 KB 003" totalMark="10" version="27" markingType="1" markingState="1" userMark="0.875" markerUserMark="7" userAttempted="1" viewingTime="981585" flagged="0" LO="06. Computation of tax payable and payment of tax" unit="Personal Tax" quT="11,20">
        <p um="0.875" cs="1" ua="1" id="924P1102">
          <s ua="0" um="-1" id="1">
            <c typ="4" id="19">
              <i cc="0" id="1" />
            </c>
            <c typ="4" id="23">
              <i cc="1" id="1" />
            </c>
          </s>
          <s ua="1" um="0.88" id="2">
            <c wei="7" maS="0" ie="1" typ="20" rv="0" mv="0" ua="1" um="1" id="10">
              <i um="1" ia="1" id="1">&lt;table extensionName="HIGHLIGHT_TABLE"&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;Workings&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;�&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;Annual Salary&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;175000&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;Building society interest&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;11250&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;186250&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;32010 + 12500 = 44510&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;44510 x 20%&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;8902&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;150000 - 44510 = 105490&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;105490 x 40%&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;42196&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;186250 - 150000 = 36250&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;36250 x 45%&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;16312.50&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;67410.5&lt;/c&gt;&lt;/r&gt;&lt;/table&gt;</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="13">
              <i ca="" id="1">The additional rate for dividends is 37.5%, this differs from the additional rate for savings income which is 45%. Also Building society interest received net of 20% removedwhereas dividends are received net of a 10% tax credit meaning that the gross for dividends would have been �10,000 instead of �11,250 reducing total income by �1,250. There would also be a decrease in the tax liability due of �750 (36,250 - 10,000 = 26250, 26,250 x 40% = 11,812.50 + 10,000 x 37.5% = 3750)</i>
            </c>
          </s>
        </p>
      </item>
      <item id="924P1090" name="07 PTAX FA13 KB 002" totalMark="10" version="31" markingType="1" markingState="1" userMark="0" markerUserMark="5" userAttempted="1" viewingTime="564143" flagged="0" LO="07. Theory underpinning topic and penalties" unit="Personal Tax" quT="11">
        <p um="0" cs="1" ua="1" id="924P1090">
          <s ua="1" um="0" id="1">
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="9">
              <i ca="" id="1">As stated in the AAT''s code of professional ethics it is highly important that any inforrmation regarding clients accounts are kept strictly confidential and should not be shared socially (including with friends and family) there are only a small amount of situations when it is okay to disclose information. An example would be if you suspect that money laudering is/has taken place or if you lawfully need to do so. I believe that my partners behaviour is disrespectful to the client and he is breaching their trust.#!#</i>
            </c>
            <c typ="4" id="12">
              <i cc="0" id="1" />
            </c>
            <c typ="4" id="16">
              <i cc="0" id="1" />
            </c>
          </s>
          <s ua="1" um="0" id="3">
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="4">
              <i ca="" id="1">The information that should be included in the guidence notes should be:#!##!#The documents that are requred to be kept and for what amount of time, they should be advised that if they do not keep the relevant information there could be a fine of around �3000 payable.#!##!#All working notes should be kept#!#A copy of the tax return#!#Any invoices/ expenses claimed for should be documented and kept.</i>
            </c>
          </s>
        </p>
      </item>
      <item id="924P1095" name="08 PTAX FA13 SY 001" totalMark="7" version="33" markingType="1" markingState="1" userMark="0" markerUserMark="1" userAttempted="1" viewingTime="112991" flagged="0" LO="08. Tax returns" unit="Personal Tax" quT="11">
        <p um="0" cs="1" ua="1" id="924P1095">
          <s ua="0" um="-1" id="1">
            <c typ="4" id="6">
              <i cc="0" id="1" />
            </c>
            <c typ="4" id="10">
              <i cc="0" id="1" />
            </c>
          </s>
          <s ua="1" um="0" id="2">
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="2">
              <i ca="" id="1">99000</i>
              <i ca="" id="2" />
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="0" um="0" id="3">
              <i ca="" id="1" />
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="0" um="0" id="4">
              <i ca="" id="1" />
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="5">
              <i ca="" id="1">Corwen Leung</i>
            </c>
          </s>
          <s ua="0" um="0" id="3">
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="0" um="0" id="2">
              <i ca="" id="1" />
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="0" um="0" id="3">
              <i ca="" id="1" />
              <i ca="" id="2" />
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="0" um="0" id="4">
              <i ca="" id="1" />
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="0" um="0" id="5">
              <i ca="" id="1" />
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="0" um="0" id="6">
              <i ca="" id="1" />
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="0" um="0" id="7">
              <i ca="" id="1" />
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="0" um="0" id="8">
              <i ca="" id="1" />
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="0" um="0" id="11">
              <i ca="" id="1" />
            </c>
          </s>
          <s ua="0" um="0" id="4">
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="0" um="0" id="4">
              <i ca="" id="1" />
              <i ca="" id="2" />
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="0" um="0" id="5">
              <i ca="" id="1" />
              <i ca="" id="2" />
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="0" um="0" id="6">
              <i ca="" id="1" />
              <i ca="" id="2" />
            </c>
          </s>
        </p>
      </item>
      <item id="924P1085" name="09 PTAX FA13 SY 004" totalMark="12" version="26" markingType="0" markingState="0" userMark="0.58333333" markerUserMark="" userAttempted="1" viewingTime="338786" flagged="0" LO="09. Basics of capital gains tax" unit="Personal Tax" quT="10,11,12">
        <p um="0.583333333333333" cs="1" ua="1" id="924P1085">
          <s ua="1" um="0.56" id="1">
            <c typ="4" id="24">
              <i cc="0" id="1" />
            </c>
            <c typ="4" id="28">
              <i cc="0" id="1" />
            </c>
            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="41">
              <i ca="11500" id="1">11500</i>
            </c>
            <c wei="1" maS="0" ie="1" typ="12" rv="0" mv="0" ua="1" um="1" id="42">
              <i sl="2" id="1" />
            </c>
            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="47">
              <i ca="4746" id="1">4746</i>
            </c>
            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="55">
              <i ca="7500" id="1">10000</i>
            </c>
            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="56">
              <i ca="" id="1">7500</i>
            </c>
          </s>
          <s ua="1" um="0.67" id="3">
            <c wei="1" maS="1" ie="1" typ="10" rv="0" mv="0" ua="1" um="1" id="27">
              <i ac="A" sl="0" id="1" />
              <i ac="B" sl="0" id="2" />
              <i ac="C" sl="0" id="3" />
              <i ca="1" ac="D" sl="1" id="4" />
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="33">
              <i ca="1183" id="1">3517</i>
            </c>
            <c wei="1" maS="1" ie="1" typ="10" rv="0" mv="0" ua="1" um="1" id="43">
              <i ac="A" sl="0" id="1" />
              <i ac="B" sl="0" id="2" />
              <i ac="C" sl="0" id="3" />
              <i ca="1" ac="D" sl="1" id="4" />
              <i ac="E" sl="0" id="5" />
              <i ac="F" sl="0" id="6" />
            </c>
          </s>
        </p>
      </item>
      <item id="924P1092" name="10 PTAX FA13 KB 002" totalMark="8" version="19" markingType="1" markingState="0" userMark="1" markerUserMark="" userAttempted="1" viewingTime="3135" flagged="0" LO="10. Taxation of shares " unit="Personal Tax" quT="20">
        <p um="1" cs="1" ua="0" id="924P1092">
          <s ua="0" um="1" id="1">
            <c wei="1" maS="0" ie="1" typ="20" rv="0" mv="0" ua="0" um="1" id="5">
              <i um="1" id="1">&lt;table extensionName="HIGHLIGHT_TABLE"&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;/table&gt;</i>
            </c>
            <c typ="4" id="6">
              <i cc="0" id="1" />
            </c>
            <c typ="4" id="10">
              <i cc="0" id="1" />
            </c>
          </s>
        </p>
      </item>
      <item id="924P1039" name="11 PTAX FA13 SY 004" totalMark="6" version="20" markingType="0" markingState="0" userMark="0.33333333" markerUserMark="" userAttempted="1" viewingTime="462229" flagged="0" LO="11. Capital gains tax exemptions, losses, reliefs and tax payable" unit="Personal Tax" quT="11">
        <p um="0.333333333333333" cs="1" ua="1" id="924P1039">
          <s ua="1" um="0.5" id="1">
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="11">
              <i ca="200" id="1">200</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="12">
              <i ca="175" id="1">142</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="0" um="0" id="13">
              <i ca="" id="1" />
            </c>
            <c typ="4" id="14">
              <i cc="0" id="1" />
            </c>
            <c typ="4" id="18">
              <i cc="0" id="1" />
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="25">
              <i ca="" id="1">58</i>
            </c>
          </s>
          <s ua="1" um="0" id="2">
            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="10">
              <i ca="0" id="1">2000</i>
            </c>
          </s>
        </p>
      </item>
    </section>
  </assessment>
  <isValid>1</isValid>
  <isPrintable>0</isPrintable>
  <qualityReview>0</qualityReview>
  <numberOfGenerations>1</numberOfGenerations>
  <requiresInvigilation>1</requiresInvigilation>
  <advanceContentDownloadTimespan>60</advanceContentDownloadTimespan>
  <automaticVerification>0</automaticVerification>
  <offlineMode>2</offlineMode>
  <requiresValidation>0</requiresValidation>
  <requiresSecureClient>1</requiresSecureClient>
  <examType>0</examType>
  <advanceDownload>0</advanceDownload>
  <gradeBoundaryData>
    <gradeBoundaries passLevelType="1" adjustedGradeBoundaries="0">
      <grade modifier="lt" value="70" description="Fail" />
      <grade modifier="gt" value="70" description="Pass" />
    </gradeBoundaries>
  </gradeBoundaryData>
  <autoViewExam>0</autoViewExam>
  <autoProgressItems>0</autoProgressItems>
  <confirmationText>
    <confirmationText />
  </confirmationText>
  <requiresConfirmationCheck>0</requiresConfirmationCheck>
  <allowCloseWithoutSubmit>0</allowCloseWithoutSubmit>
  <useSecureMarker>0</useSecureMarker>
  <annotationVersion>0</annotationVersion>
  <allowPackagingDelivery>1</allowPackagingDelivery>
  <scoreBoundaryData>
    <scoreBoundaries showScoreBoundaryColumn="1">
      <score modifier="lt" value="36" description="Significantly below" higherBoundarySet="1" />
      <score modifier="lt" value="36" description="Significantly below" higherBoundarySet="0" />
      <score modifier="gt" value="36" description="Below requirement" higherBoundarySet="1" />
      <score modifier="gt" value="60" description="Borderline" higherBoundarySet="1" />
      <score modifier="gt" value="70" description="Met" higherBoundarySet="0" />
      <score modifier="gt" value="70" description="Met" higherBoundarySet="1" />
      <score modifier="gt" value="36" description="Below requirement" higherBoundarySet="0" userCreated="1" />
      <score modifier="gt" value="60" description="Borderline" higherBoundarySet="0" userCreated="1" />
      <score modifier="gt" value="85" description="Exceeded" higherBoundarySet="1" userCreated="1" />
    </scoreBoundaries>
  </scoreBoundaryData>
  <showCandidateReportResultColumn>0</showCandidateReportResultColumn>
  <showCandidateReportPercentageColumn>1</showCandidateReportPercentageColumn>
  <certifiedAccessible>0</certifiedAccessible>
  <markingProgressVisible>1</markingProgressVisible>
  <markingProgressMode>0</markingProgressMode>
  <secureClientOperationMode>1</secureClientOperationMode>
  <examDeliverySystemStyleType>delivery</examDeliverySystemStyleType>
  <markingAutoVoidPeriod>365</markingAutoVoidPeriod>
  <showPageRequiresScrollingAlert>0</showPageRequiresScrollingAlert>
  <project ID="924" />
</assessmentDetails>'
where WarehouseID = 1366283
*/

update ReMarkExamSessionTable 
set StructureXML = '<assessmentDetails>
  <assessmentID>2029</assessmentID>
  <qualificationID>95</qualificationID>
  <qualificationName>Personal tax (AQ2013)</qualificationName>
  <qualificationReference>AATQCF</qualificationReference>
  <assessmentGroupName>PTAX FA 2013</assessmentGroupName>
  <assessmentGroupID>1914</assessmentGroupID>
  <assessmentGroupReference>CAPTAX</assessmentGroupReference>
  <assessmentName>PTAX FA 2013</assessmentName>
  <validFromDate>06 Jan 2014</validFromDate>
  <expiryDate>31 Jul 2014</expiryDate>
  <startTime>00:00:00</startTime>
  <endTime>23:59:59</endTime>
  <duration>120</duration>
  <defaultDuration>120</defaultDuration>
  <scheduledDuration>
    <value>120</value>
    <reason></reason>
  </scheduledDuration>
  <sRBonus>0</sRBonus>
  <sRBonusMaximum>40</sRBonusMaximum>
  <externalReference>CAPTAX</externalReference>
  <passLevelValue>70</passLevelValue>
  <passLevelType>1</passLevelType>
  <status>2</status>
  <testFeedbackType>
    <passFail>0</passFail>
    <percentageMark>0</percentageMark>
    <allowSummaryFeedback>0</allowSummaryFeedback>
    <summaryFeedbackType>1</summaryFeedbackType>
    <itemSummary>0</itemSummary>
    <itemReview>0</itemReview>
    <itemReviewCorrectAnswer>0</itemReviewCorrectAnswer>
    <itemFeedback>0</itemFeedback>
    <printableSummary>0</printableSummary>
    <candidateDetails>0</candidateDetails>
    <feedbackByReference>0</feedbackByReference>
    <allowFeedbackDuringExam>0</allowFeedbackDuringExam>
    <allowFeedbackDuringSummary>0</allowFeedbackDuringSummary>
  </testFeedbackType>
  <itemFeedback>0</itemFeedback>
  <allowCalculator>0</allowCalculator>
  <lastInUseDate>06 Dec 2013 11:42:24</lastInUseDate>
  <completedScriptReview>0</completedScriptReview>
  <assessment currentSection="1" totalTime="0" currentTime="0">
    <intro id="0" name="Introduction" currentItem="">
      <item id="924P1161" name="Intro PTAX" totalMark="1" version="19" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" LO="Introduction" unit="Personal Tax" />
    </intro>
    <outro id="0" name="Finish" currentItem="" />
    <tools id="0" name="Tools" currentItem="">
      <item id="924P971" name="PTAX FA2013 Pop up 1" toolName="PTAX FA2013 Pop up 1" totalMark="1" version="7" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" LO="Pop-up" unit="Personal Tax" />
      <item id="924P972" name="PTAX FA2013 Pop up 2" toolName="PTAX FA2013 Pop up 2" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" LO="Pop-up" unit="Personal Tax" />
    </tools>
    <section id="1" name="1" passLevelValue="70" passLevelType="1" itemsToMark="0" currentItem="924P1078" fixed="0">
      <item id="924P985" name="01 PTAX FA13 SY 001" totalMark="9" version="30" markingType="0" markingState="0" userMark="1" markerUserMark="" userAttempted="1" viewingTime="274717" flagged="0" LO="01. Benefits in Kind � provision of cars" unit="Personal Tax" quT="10,11" />
      <item id="924P999" name="02 PTAX FA13 SY 001" totalMark="10" version="25" markingType="0" markingState="0" userMark="0.8" markerUserMark="" userAttempted="1" viewingTime="377568" flagged="0" LO="02. Benefits in Kind � all excluding cars" unit="Personal Tax" quT="10,11" />
      <item id="924P1138" name="03 PTAX FA13 KB 005" totalMark="10" version="18" markingType="0" markingState="0" userMark="0.8" markerUserMark="" userAttempted="1" viewingTime="194828" flagged="0" LO="03. Income from property" unit="Personal Tax" quT="11,12" />
      <item id="924P1076" name="04 PTAX FA13 SY 004" totalMark="6" version="24" markingType="0" markingState="0" userMark="1" markerUserMark="" userAttempted="1" viewingTime="119215" flagged="0" LO="04. Investment income" unit="Personal Tax" quT="11,12" />
      <item id="924P1078" name="05 PTAX FA13 SY 003" totalMark="12" version="30" markingType="0" markingState="0" userMark="0.83333333" markerUserMark="" userAttempted="1" viewingTime="636247" flagged="1" LO="05. Computation of total and taxable income" unit="Personal Tax" quT="11" />
      <item id="924P1102" name="06 PTAX FA13 KB 003" totalMark="10" version="27" markingType="1" markingState="1" userMark="0.875" markerUserMark="7" userAttempted="1" viewingTime="981585" flagged="0" LO="06. Computation of tax payable and payment of tax" unit="Personal Tax" quT="11,20" />
      <item id="924P1090" name="07 PTAX FA13 KB 002" totalMark="10" version="31" markingType="1" markingState="1" userMark="0" markerUserMark="5" userAttempted="1" viewingTime="564143" flagged="0" LO="07. Theory underpinning topic and penalties" unit="Personal Tax" quT="11" />
      <item id="924P1095" name="08 PTAX FA13 SY 001" totalMark="7" version="33" markingType="1" markingState="1" userMark="0" markerUserMark="1" userAttempted="1" viewingTime="112991" flagged="0" LO="08. Tax returns" unit="Personal Tax" quT="11" />
      <item id="924P1085" name="09 PTAX FA13 SY 004" totalMark="12" version="26" markingType="0" markingState="0" userMark="0.58333333" markerUserMark="" userAttempted="1" viewingTime="338786" flagged="0" LO="09. Basics of capital gains tax" unit="Personal Tax" quT="10,11,12" />
      <item id="924P1092" reMarked="1" name="10 PTAX FA13 KB 002" totalMark="8" version="19" markingType="1" markingState="1" userMark="1" markerUserMark="0" userAttempted="0" viewingTime="3135" flagged="0" LO="10. Taxation of shares " unit="Personal Tax" quT="20" />
      <item id="924P1039" name="11 PTAX FA13 SY 004" totalMark="6" version="20" markingType="0" markingState="0" userMark="0.33333333" markerUserMark="" userAttempted="1" viewingTime="462229" flagged="0" LO="11. Capital gains tax exemptions, losses, reliefs and tax payable" unit="Personal Tax" quT="11" />
    </section>
  </assessment>
  <isValid>1</isValid>
  <isPrintable>0</isPrintable>
  <qualityReview>0</qualityReview>
  <numberOfGenerations>1</numberOfGenerations>
  <requiresInvigilation>1</requiresInvigilation>
  <advanceContentDownloadTimespan>60</advanceContentDownloadTimespan>
  <automaticVerification>0</automaticVerification>
  <offlineMode>2</offlineMode>
  <requiresValidation>0</requiresValidation>
  <requiresSecureClient>1</requiresSecureClient>
  <examType>0</examType>
  <advanceDownload>0</advanceDownload>
  <gradeBoundaryData>
    <gradeBoundaries passLevelType="1" adjustedGradeBoundaries="0">
      <grade modifier="lt" value="70" description="Fail" />
      <grade modifier="gt" value="70" description="Pass" />
    </gradeBoundaries>
  </gradeBoundaryData>
  <autoViewExam>0</autoViewExam>
  <autoProgressItems>0</autoProgressItems>
  <confirmationText>
    <confirmationText></confirmationText>
  </confirmationText>
  <requiresConfirmationCheck>0</requiresConfirmationCheck>
  <allowCloseWithoutSubmit>0</allowCloseWithoutSubmit>
  <useSecureMarker>0</useSecureMarker>
  <annotationVersion>0</annotationVersion>
  <allowPackagingDelivery>1</allowPackagingDelivery>
  <scoreBoundaryData>
    <scoreBoundaries showScoreBoundaryColumn="1">
      <score modifier="lt" value="36" description="Significantly below" higherBoundarySet="1" />
      <score modifier="lt" value="36" description="Significantly below" higherBoundarySet="0" />
      <score modifier="gt" value="36" description="Below requirement" higherBoundarySet="1" />
      <score modifier="gt" value="60" description="Borderline" higherBoundarySet="1" />
      <score modifier="gt" value="70" description="Met" higherBoundarySet="0" />
      <score modifier="gt" value="70" description="Met" higherBoundarySet="1" />
      <score modifier="gt" value="36" description="Below requirement" higherBoundarySet="0" userCreated="1" />
      <score modifier="gt" value="60" description="Borderline" higherBoundarySet="0" userCreated="1" />
      <score modifier="gt" value="85" description="Exceeded" higherBoundarySet="1" userCreated="1" />
    </scoreBoundaries>
  </scoreBoundaryData>
  <showCandidateReportResultColumn>0</showCandidateReportResultColumn>
  <showCandidateReportPercentageColumn>1</showCandidateReportPercentageColumn>
  <certifiedAccessible>0</certifiedAccessible>
  <markingProgressVisible>1</markingProgressVisible>
  <markingProgressMode>0</markingProgressMode>
  <secureClientOperationMode>1</secureClientOperationMode>
  <examDeliverySystemStyleType>delivery</examDeliverySystemStyleType>
  <markingAutoVoidPeriod>365</markingAutoVoidPeriod>
  <showPageRequiresScrollingAlert>0</showPageRequiresScrollingAlert>
  <project ID="924" />
</assessmentDetails>', ResultData = '<exam passMark="70" passType="1" originalPassMark="70" originalPassType="1" totalMark="100" userMark="63.000" userPercentage="63.000" passValue="0" totalTimeSpent="4065444" closeBoundaryType="0" closeBoundaryValue="0" originalPassValue="1" grade="Fail" originalGrade="Pass">
  <gradeBoundaryData>
    <gradeBoundaries passLevelType="1" adjustedGradeBoundaries="0">
      <grade modifier="lt" value="70" description="Fail" />
      <grade modifier="gt" value="70" description="Pass" />
    </gradeBoundaries>
  </gradeBoundaryData>
  <section id="1" passMark="70" passType="1" totalMark="100" userMark="63.000" userPercentage="63.000" passValue="0" totalTimeSpent="4065444" itemsToMark="0">
    <item id="924P985" name="01 PTAX FA13 SY 001" totalMark="9" userMark="1" actualUserMark="9" markingType="0" markerUserMark="" viewingTime="274717" userAttempted="1" version="30" />
    <item id="924P999" name="02 PTAX FA13 SY 001" totalMark="10" userMark="0.8" actualUserMark="8.0" markingType="0" markerUserMark="" viewingTime="377568" userAttempted="1" version="25" />
    <item id="924P1138" name="03 PTAX FA13 KB 005" totalMark="10" userMark="0.8" actualUserMark="8.0" markingType="0" markerUserMark="" viewingTime="194828" userAttempted="1" version="18" />
    <item id="924P1076" name="04 PTAX FA13 SY 004" totalMark="6" userMark="1" actualUserMark="6" markingType="0" markerUserMark="" viewingTime="119215" userAttempted="1" version="24" />
    <item id="924P1078" name="05 PTAX FA13 SY 003" totalMark="12" userMark="0.83333333" actualUserMark="9.99999996" markingType="0" markerUserMark="" viewingTime="636247" userAttempted="1" version="30" />
    <item id="924P1102" name="06 PTAX FA13 KB 003" totalMark="10" userMark="0" actualUserMark="7" markingType="1" markerUserMark="7" viewingTime="981585" userAttempted="1" version="27" />
    <item id="924P1090" name="07 PTAX FA13 KB 002" totalMark="10" userMark="0" actualUserMark="5" markingType="1" markerUserMark="5" viewingTime="564143" userAttempted="1" version="31" />
    <item id="924P1095" name="08 PTAX FA13 SY 001" totalMark="7" userMark="0" actualUserMark="1" markingType="1" markerUserMark="1" viewingTime="112991" userAttempted="1" version="33" />
    <item id="924P1085" name="09 PTAX FA13 SY 004" totalMark="12" userMark="0.58333333" actualUserMark="6.99999996" markingType="0" markerUserMark="" viewingTime="338786" userAttempted="1" version="26" />
    <item id="924P1092" name="10 PTAX FA13 KB 002" totalMark="8" userMark="0" actualUserMark="0" markingType="1" markerUserMark="0" viewingTime="3135" userAttempted="0" version="19" />
    <item id="924P1039" name="11 PTAX FA13 SY 004" totalMark="6" userMark="0.33333333" actualUserMark="1.99999998" markingType="0" markerUserMark="" viewingTime="462229" userAttempted="1" version="20" />
  </section>
</exam>', ResultDataFull = '<assessmentDetails xmlns="">
  <assessmentID>2029</assessmentID>
  <qualificationID>95</qualificationID>
  <qualificationName>Personal tax (AQ2013)</qualificationName>
  <qualificationReference>AATQCF</qualificationReference>
  <assessmentGroupName>PTAX FA 2013</assessmentGroupName>
  <assessmentGroupID>1914</assessmentGroupID>
  <assessmentGroupReference>CAPTAX</assessmentGroupReference>
  <assessmentName>PTAX FA 2013</assessmentName>
  <validFromDate>06 Jan 2014</validFromDate>
  <expiryDate>31 Jul 2014</expiryDate>
  <startTime>00:00:00</startTime>
  <endTime>23:59:59</endTime>
  <duration>120</duration>
  <defaultDuration>120</defaultDuration>
  <scheduledDuration>
    <value>120</value>
    <reason />
  </scheduledDuration>
  <sRBonus>0</sRBonus>
  <sRBonusMaximum>40</sRBonusMaximum>
  <externalReference>CAPTAX</externalReference>
  <passLevelValue>70</passLevelValue>
  <passLevelType>1</passLevelType>
  <status>2</status>
  <testFeedbackType>
    <passFail>0</passFail>
    <percentageMark>0</percentageMark>
    <allowSummaryFeedback>0</allowSummaryFeedback>
    <summaryFeedbackType>1</summaryFeedbackType>
    <itemSummary>0</itemSummary>
    <itemReview>0</itemReview>
    <itemReviewCorrectAnswer>0</itemReviewCorrectAnswer>
    <itemFeedback>0</itemFeedback>
    <printableSummary>0</printableSummary>
    <candidateDetails>0</candidateDetails>
    <feedbackByReference>0</feedbackByReference>
    <allowFeedbackDuringExam>0</allowFeedbackDuringExam>
    <allowFeedbackDuringSummary>0</allowFeedbackDuringSummary>
  </testFeedbackType>
  <itemFeedback>0</itemFeedback>
  <allowCalculator>0</allowCalculator>
  <lastInUseDate>06 Dec 2013 11:42:24</lastInUseDate>
  <completedScriptReview>0</completedScriptReview>
  <assessment currentSection="1" totalTime="0" currentTime="0" originalPassMark="70" originalPassType="1" passMark="70" passType="1" totalMark="100" userMark="63.000" userPercentage="63.000" passValue="0" originalGrade="Pass">
    <intro id="0" name="Introduction" currentItem="">
      <item id="924P1161" name="Intro PTAX" totalMark="1" version="19" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" LO="Introduction" unit="Personal Tax" />
    </intro>
    <outro id="0" name="Finish" currentItem="" />
    <tools id="0" name="Tools" currentItem="">
      <item id="924P971" name="PTAX FA2013 Pop up 1" toolName="PTAX FA2013 Pop up 1" totalMark="1" version="7" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" LO="Pop-up" unit="Personal Tax" />
      <item id="924P972" name="PTAX FA2013 Pop up 2" toolName="PTAX FA2013 Pop up 2" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" LO="Pop-up" unit="Personal Tax" />
    </tools>
    <section id="1" name="1" passLevelValue="70" passLevelType="1" itemsToMark="0" currentItem="924P1078" fixed="0" totalMark="100" userMark="63.000" userPercentage="63.000" passValue="0">
      <item id="924P985" name="01 PTAX FA13 SY 001" totalMark="9" version="30" markingType="0" markingState="0" userMark="1" markerUserMark="" userAttempted="1" viewingTime="274717" flagged="0" LO="01. Benefits in Kind � provision of cars" unit="Personal Tax" quT="10,11">
        <p um="1" cs="1" ua="1" id="924P985">
          <s ua="0" um="-1" id="1">
            <c typ="4" id="14">
              <i cc="0" id="1" />
            </c>
            <c typ="4" id="18">
              <i cc="0" id="1" />
            </c>
          </s>
          <s ua="1" um="1" id="2">
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="2">
              <i ca="20" id="1">20</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="3">
              <i ca="" id="1">2140</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="4">
              <i ca="32" id="1">32</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="5">
              <i ca="" id="1">4960</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="6">
              <i ca="" id="1">2110</i>
            </c>
            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="15">
              <i ca="" id="1">4250</i>
            </c>
          </s>
          <s ua="1" um="1" id="3">
            <c wei="2" maS="2" ie="1" typ="10" rv="0" mv="0" ua="1" um="1" id="5">
              <i ac="A" sl="0" id="1" />
              <i ca="1" ac="B" sl="1" id="2" />
              <i ca="1" ac="C" sl="1" id="3" />
              <i ac="D" sl="0" id="4" />
            </c>
          </s>
        </p>
      </item>
      <item id="924P999" name="02 PTAX FA13 SY 001" totalMark="10" version="25" markingType="0" markingState="0" userMark="0.8" markerUserMark="" userAttempted="1" viewingTime="377568" flagged="0" LO="02. Benefits in Kind � all excluding cars" unit="Personal Tax" quT="10,11">
        <p um="0.8" cs="1" ua="1" id="924P999">
          <s ua="1" um="0.5" id="1">
            <c typ="4" id="8">
              <i cc="0" id="1" />
            </c>
            <c typ="4" id="12">
              <i cc="0" id="1" />
            </c>
            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="21">
              <i ca="200" id="1">200</i>
            </c>
            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="0" um="0" id="22">
              <i ca="" id="1" />
            </c>
          </s>
          <s ua="1" um="1" id="2">
            <c wei="5" maS="5" ie="1" typ="10" rv="0" mv="0" ua="1" um="1" id="27">
              <i ac="A" sl="0" id="1" />
              <i ac="J" sl="0" id="10" />
              <i ac="K" sl="0" id="11" />
              <i ca="1" ac="B" sl="1" id="2" />
              <i ac="C" sl="0" id="3" />
              <i ca="1" ac="D" sl="1" id="4" />
              <i ac="E" sl="0" id="5" />
              <i ca="1" ac="F" sl="1" id="6" />
              <i ca="1" ac="G" sl="1" id="7" />
              <i ca="1" ac="H" sl="1" id="8" />
              <i ac="I" sl="0" id="9" />
            </c>
          </s>
          <s ua="1" um="1" id="3">
            <c wei="1" maS="1" ie="1" typ="10" rv="0" mv="0" ua="1" um="1" id="4">
              <i ac="A" sl="0" id="1" />
              <i ca="1" ac="B" sl="1" id="2" />
              <i ac="C" sl="0" id="3" />
              <i ac="D" sl="0" id="4" />
            </c>
          </s>
        </p>
      </item>
      <item id="924P1138" name="03 PTAX FA13 KB 005" totalMark="10" version="18" markingType="0" markingState="0" userMark="0.8" markerUserMark="" userAttempted="1" viewingTime="194828" flagged="0" LO="03. Income from property" unit="Personal Tax" quT="11,12">
        <p um="0.8" cs="1" ua="1" id="924P1138">
          <s ua="1" um="0.88" id="1">
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="7">
              <i ca="5850" id="1">5850</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="8">
              <i ca="0" id="1">0</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="9">
              <i ca="949" id="1">984</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="10">
              <i ca="400" id="1">400</i>
            </c>
            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="11">
              <i ca="" id="1">372</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="12">
              <i ca="" id="1">2364</i>
            </c>
            <c typ="4" id="15">
              <i cc="0" id="1" />
            </c>
            <c typ="4" id="19">
              <i cc="0" id="1" />
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="23">
              <i ca="1730" id="1">1730</i>
            </c>
          </s>
          <s ua="1" um="0.5" id="2">
            <c wei="1" maS="0" ie="1" typ="12" rv="0" mv="0" ua="1" um="0" id="4">
              <i sl="5" id="1" />
            </c>
            <c wei="1" maS="0" ie="1" typ="12" rv="0" mv="0" ua="1" um="1" id="7">
              <i sl="2" id="1" />
            </c>
          </s>
        </p>
      </item>
      <item id="924P1076" name="04 PTAX FA13 SY 004" totalMark="6" version="24" markingType="0" markingState="0" userMark="1" markerUserMark="" userAttempted="1" viewingTime="119215" flagged="0" LO="04. Investment income" unit="Personal Tax" quT="11,12">
        <p um="1" cs="1" ua="1" id="924P1076">
          <s ua="1" um="1" id="1">
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="7">
              <i ca="" id="1">140</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="8">
              <i ca="" id="1">460</i>
            </c>
            <c typ="4" id="11">
              <i cc="0" id="1" />
            </c>
            <c typ="4" id="15">
              <i cc="1" id="1" />
            </c>
            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="22">
              <i ca="" id="1">315</i>
            </c>
            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="25">
              <i ca="2300" id="1">2300</i>
            </c>
            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="26">
              <i ca="1400" id="1">1400</i>
            </c>
            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="27">
              <i ca="" id="1">460</i>
            </c>
            <c wei="2" maS="0" ie="1" typ="12" rv="0" mv="0" ua="1" um="1" id="32">
              <i sl="3" id="1" />
            </c>
          </s>
        </p>
      </item>
      <item id="924P1078" name="05 PTAX FA13 SY 003" totalMark="12" version="30" markingType="0" markingState="0" userMark="0.83333333" markerUserMark="" userAttempted="1" viewingTime="636247" flagged="1" LO="05. Computation of total and taxable income" unit="Personal Tax" quT="11">
        <p um="0.833333333333333" cs="1" ua="1" id="924P1078">
          <s ua="0" um="-1" id="1">
            <c typ="4" id="12">
              <i cc="0" id="1" />
            </c>
            <c typ="4" id="16">
              <i cc="0" id="1" />
            </c>
          </s>
          <s ua="1" um="1" id="2">
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="14">
              <i ca="0" id="1">0</i>
            </c>
            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="15">
              <i ca="29360" id="1">29360</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="16">
              <i ca="2488" id="1">2488</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="17">
              <i ca="3376" id="1">3376</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="18">
              <i ca="2440" id="1">2440</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="19">
              <i ca="0" id="1">0</i>
            </c>
            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="20">
              <i ca="" id="1">4404</i>
            </c>
            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="21">
              <i ca="250" id="1">250</i>
            </c>
            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="5">
              <i ca="750" id="1">750</i>
            </c>
            <c wei="4" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="6">
              <i ca="" id="2">32760</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="26">
              <i ca="0" id="1">0</i>
            </c>
          </s>
          <s ua="1" um="0.33" id="3">
            <c wei="4" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="9">
              <i ca="11285" id="1">11025</i>
            </c>
            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="10">
              <i ca="11025" id="1">11025</i>
            </c>
          </s>
        </p>
      </item>
      <item id="924P1102" name="06 PTAX FA13 KB 003" totalMark="10" version="27" markingType="1" markingState="1" userMark="0.875" markerUserMark="7" userAttempted="1" viewingTime="981585" flagged="0" LO="06. Computation of tax payable and payment of tax" unit="Personal Tax" quT="11,20">
        <p um="0.875" cs="1" ua="1" id="924P1102">
          <s ua="0" um="-1" id="1">
            <c typ="4" id="19">
              <i cc="0" id="1" />
            </c>
            <c typ="4" id="23">
              <i cc="1" id="1" />
            </c>
          </s>
          <s ua="1" um="0.88" id="2">
            <c wei="7" maS="0" ie="1" typ="20" rv="0" mv="0" ua="1" um="1" id="10">
              <i um="1" ia="1" id="1">&lt;table extensionName="HIGHLIGHT_TABLE"&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;Workings&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;�&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;Annual Salary&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;175000&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;Building society interest&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;11250&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;186250&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;32010 + 12500 = 44510&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;44510 x 20%&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;8902&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;150000 - 44510 = 105490&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;105490 x 40%&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;42196&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;186250 - 150000 = 36250&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;36250 x 45%&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;16312.50&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;67410.5&lt;/c&gt;&lt;/r&gt;&lt;/table&gt;</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="13">
              <i ca="" id="1">The additional rate for dividends is 37.5%, this differs from the additional rate for savings income which is 45%. Also Building society interest received net of 20% removedwhereas dividends are received net of a 10% tax credit meaning that the gross for dividends would have been �10,000 instead of �11,250 reducing total income by �1,250. There would also be a decrease in the tax liability due of �750 (36,250 - 10,000 = 26250, 26,250 x 40% = 11,812.50 + 10,000 x 37.5% = 3750)</i>
            </c>
          </s>
        </p>
      </item>
      <item id="924P1090" name="07 PTAX FA13 KB 002" totalMark="10" version="31" markingType="1" markingState="1" userMark="0" markerUserMark="5" userAttempted="1" viewingTime="564143" flagged="0" LO="07. Theory underpinning topic and penalties" unit="Personal Tax" quT="11">
        <p um="0" cs="1" ua="1" id="924P1090">
          <s ua="1" um="0" id="1">
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="9">
              <i ca="" id="1">As stated in the AAT''s code of professional ethics it is highly important that any inforrmation regarding clients accounts are kept strictly confidential and should not be shared socially (including with friends and family) there are only a small amount of situations when it is okay to disclose information. An example would be if you suspect that money laudering is/has taken place or if you lawfully need to do so. I believe that my partners behaviour is disrespectful to the client and he is breaching their trust.#!#</i>
            </c>
            <c typ="4" id="12">
              <i cc="0" id="1" />
            </c>
            <c typ="4" id="16">
              <i cc="0" id="1" />
            </c>
          </s>
          <s ua="1" um="0" id="3">
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="4">
              <i ca="" id="1">The information that should be included in the guidence notes should be:#!##!#The documents that are requred to be kept and for what amount of time, they should be advised that if they do not keep the relevant information there could be a fine of around �3000 payable.#!##!#All working notes should be kept#!#A copy of the tax return#!#Any invoices/ expenses claimed for should be documented and kept.</i>
            </c>
          </s>
        </p>
      </item>
      <item id="924P1095" name="08 PTAX FA13 SY 001" totalMark="7" version="33" markingType="1" markingState="1" userMark="0" markerUserMark="1" userAttempted="1" viewingTime="112991" flagged="0" LO="08. Tax returns" unit="Personal Tax" quT="11">
        <p um="0" cs="1" ua="1" id="924P1095">
          <s ua="0" um="-1" id="1">
            <c typ="4" id="6">
              <i cc="0" id="1" />
            </c>
            <c typ="4" id="10">
              <i cc="0" id="1" />
            </c>
          </s>
          <s ua="1" um="0" id="2">
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="2">
              <i ca="" id="1">99000</i>
              <i ca="" id="2" />
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="0" um="0" id="3">
              <i ca="" id="1" />
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="0" um="0" id="4">
              <i ca="" id="1" />
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="5">
              <i ca="" id="1">Corwen Leung</i>
            </c>
          </s>
          <s ua="0" um="0" id="3">
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="0" um="0" id="2">
              <i ca="" id="1" />
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="0" um="0" id="3">
              <i ca="" id="1" />
              <i ca="" id="2" />
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="0" um="0" id="4">
              <i ca="" id="1" />
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="0" um="0" id="5">
              <i ca="" id="1" />
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="0" um="0" id="6">
              <i ca="" id="1" />
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="0" um="0" id="7">
              <i ca="" id="1" />
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="0" um="0" id="8">
              <i ca="" id="1" />
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="0" um="0" id="11">
              <i ca="" id="1" />
            </c>
          </s>
          <s ua="0" um="0" id="4">
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="0" um="0" id="4">
              <i ca="" id="1" />
              <i ca="" id="2" />
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="0" um="0" id="5">
              <i ca="" id="1" />
              <i ca="" id="2" />
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="0" um="0" id="6">
              <i ca="" id="1" />
              <i ca="" id="2" />
            </c>
          </s>
        </p>
      </item>
      <item id="924P1085" name="09 PTAX FA13 SY 004" totalMark="12" version="26" markingType="0" markingState="0" userMark="0.58333333" markerUserMark="" userAttempted="1" viewingTime="338786" flagged="0" LO="09. Basics of capital gains tax" unit="Personal Tax" quT="10,11,12">
        <p um="0.583333333333333" cs="1" ua="1" id="924P1085">
          <s ua="1" um="0.56" id="1">
            <c typ="4" id="24">
              <i cc="0" id="1" />
            </c>
            <c typ="4" id="28">
              <i cc="0" id="1" />
            </c>
            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="41">
              <i ca="11500" id="1">11500</i>
            </c>
            <c wei="1" maS="0" ie="1" typ="12" rv="0" mv="0" ua="1" um="1" id="42">
              <i sl="2" id="1" />
            </c>
            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="47">
              <i ca="4746" id="1">4746</i>
            </c>
            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="55">
              <i ca="7500" id="1">10000</i>
            </c>
            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="56">
              <i ca="" id="1">7500</i>
            </c>
          </s>
          <s ua="1" um="0.67" id="3">
            <c wei="1" maS="1" ie="1" typ="10" rv="0" mv="0" ua="1" um="1" id="27">
              <i ac="A" sl="0" id="1" />
              <i ac="B" sl="0" id="2" />
              <i ac="C" sl="0" id="3" />
              <i ca="1" ac="D" sl="1" id="4" />
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="33">
              <i ca="1183" id="1">3517</i>
            </c>
            <c wei="1" maS="1" ie="1" typ="10" rv="0" mv="0" ua="1" um="1" id="43">
              <i ac="A" sl="0" id="1" />
              <i ac="B" sl="0" id="2" />
              <i ac="C" sl="0" id="3" />
              <i ca="1" ac="D" sl="1" id="4" />
              <i ac="E" sl="0" id="5" />
              <i ac="F" sl="0" id="6" />
            </c>
          </s>
        </p>
      </item>
      <item id="924P1092" reMarked="1" name="10 PTAX FA13 KB 002" totalMark="8" version="19" markingType="1" markingState="1" userMark="1" markerUserMark="0" userAttempted="0" viewingTime="3135" flagged="0" LO="10. Taxation of shares " unit="Personal Tax" quT="20">
        <p um="1" cs="1" ua="0" id="924P1092">
          <s ua="0" um="1" id="1">
            <c wei="1" maS="0" ie="1" typ="20" rv="0" mv="0" ua="0" um="1" id="5">
              <i um="1" id="1">&lt;table extensionName="HIGHLIGHT_TABLE"&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;/table&gt;</i>
            </c>
            <c typ="4" id="6">
              <i cc="0" id="1" />
            </c>
            <c typ="4" id="10">
              <i cc="0" id="1" />
            </c>
          </s>
        </p>
      </item>
      <item id="924P1039" name="11 PTAX FA13 SY 004" totalMark="6" version="20" markingType="0" markingState="0" userMark="0.33333333" markerUserMark="" userAttempted="1" viewingTime="462229" flagged="0" LO="11. Capital gains tax exemptions, losses, reliefs and tax payable" unit="Personal Tax" quT="11">
        <p um="0.333333333333333" cs="1" ua="1" id="924P1039">
          <s ua="1" um="0.5" id="1">
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="11">
              <i ca="200" id="1">200</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="12">
              <i ca="175" id="1">142</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="0" um="0" id="13">
              <i ca="" id="1" />
            </c>
            <c typ="4" id="14">
              <i cc="0" id="1" />
            </c>
            <c typ="4" id="18">
              <i cc="0" id="1" />
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="25">
              <i ca="" id="1">58</i>
            </c>
          </s>
          <s ua="1" um="0" id="2">
            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="10">
              <i ca="0" id="1">2000</i>
            </c>
          </s>
        </p>
      </item>
    </section>
  </assessment>
  <isValid>1</isValid>
  <isPrintable>0</isPrintable>
  <qualityReview>0</qualityReview>
  <numberOfGenerations>1</numberOfGenerations>
  <requiresInvigilation>1</requiresInvigilation>
  <advanceContentDownloadTimespan>60</advanceContentDownloadTimespan>
  <automaticVerification>0</automaticVerification>
  <offlineMode>2</offlineMode>
  <requiresValidation>0</requiresValidation>
  <requiresSecureClient>1</requiresSecureClient>
  <examType>0</examType>
  <advanceDownload>0</advanceDownload>
  <gradeBoundaryData>
    <gradeBoundaries passLevelType="1" adjustedGradeBoundaries="0">
      <grade modifier="lt" value="70" description="Fail" />
      <grade modifier="gt" value="70" description="Pass" />
    </gradeBoundaries>
  </gradeBoundaryData>
  <autoViewExam>0</autoViewExam>
  <autoProgressItems>0</autoProgressItems>
  <confirmationText>
    <confirmationText />
  </confirmationText>
  <requiresConfirmationCheck>0</requiresConfirmationCheck>
  <allowCloseWithoutSubmit>0</allowCloseWithoutSubmit>
  <useSecureMarker>0</useSecureMarker>
  <annotationVersion>0</annotationVersion>
  <allowPackagingDelivery>1</allowPackagingDelivery>
  <scoreBoundaryData>
    <scoreBoundaries showScoreBoundaryColumn="1">
      <score modifier="lt" value="36" description="Significantly below" higherBoundarySet="1" />
      <score modifier="lt" value="36" description="Significantly below" higherBoundarySet="0" />
      <score modifier="gt" value="36" description="Below requirement" higherBoundarySet="1" />
      <score modifier="gt" value="60" description="Borderline" higherBoundarySet="1" />
      <score modifier="gt" value="70" description="Met" higherBoundarySet="0" />
      <score modifier="gt" value="70" description="Met" higherBoundarySet="1" />
      <score modifier="gt" value="36" description="Below requirement" higherBoundarySet="0" userCreated="1" />
      <score modifier="gt" value="60" description="Borderline" higherBoundarySet="0" userCreated="1" />
      <score modifier="gt" value="85" description="Exceeded" higherBoundarySet="1" userCreated="1" />
    </scoreBoundaries>
  </scoreBoundaryData>
  <showCandidateReportResultColumn>0</showCandidateReportResultColumn>
  <showCandidateReportPercentageColumn>1</showCandidateReportPercentageColumn>
  <certifiedAccessible>0</certifiedAccessible>
  <markingProgressVisible>1</markingProgressVisible>
  <markingProgressMode>0</markingProgressMode>
  <secureClientOperationMode>1</secureClientOperationMode>
  <examDeliverySystemStyleType>delivery</examDeliverySystemStyleType>
  <markingAutoVoidPeriod>365</markingAutoVoidPeriod>
  <showPageRequiresScrollingAlert>0</showPageRequiresScrollingAlert>
  <project ID="924" />
</assessmentDetails>'
where WarehouseID = 1366283


rollback