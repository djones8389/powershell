SELECT WEST.ID, WEST.KeyCode, west.ExportedToIntegration, wests.ExportedToIntegration
	, west.warehouseTime, wests.warehousetime
FROM WAREHOUSE_ExamSessionTable as WEST

  Inner Join WAREHOUSE_ScheduledExamsTable as WSCET
  on WSCET.ID = WEST.WAREHOUSEScheduledExamID
  
  Inner Join WAREHOUSE_UserTable as WUT
  on WUT.ID = WEST.WAREHOUSEUserID

  Inner Join WAREHOUSE_CentreTable as WCT
  on WCT.ID = WSCET.WAREHOUSECentreID

  Inner Join WAREHOUSE_ExamSessionTable_Shreded as WESTS
  on WESTS.examSessionId = WEST.ID

where west.warehouseTime > '01 Oct 2014' and west.warehouseTime < '02 Oct 2014'
	order by west.ID asc
--west.ID IN (1416253,1416254,1416255,1416256,1416257,1416259,1416260)	or west.ID = 1416258


select COUNT(ExportedToIntegration) as 'Count of exams', ExportedToIntegration
	from WAREHOUSE_ExamSessionTable_Shreded
		where ExportedToIntegration = 0
			group by ExportedToIntegration;
		
select COUNT(ExportedToIntegration) as 'Count of exams', ExportedToIntegration
	from WAREHOUSE_ExamSessionTable_Shreded
		where ExportedToIntegration = 1
			group by ExportedToIntegration;