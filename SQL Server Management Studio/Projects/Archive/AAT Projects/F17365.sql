SELECT EST.ID, EST.KeyCode, EST.examState, UT.Forename, UT.Surname, CT.CentreName, SCET.examName
	, est.StructureXML, est.resultData, est.resultDataFull
		, ests.structureXml, est.resultData
  
  FROM ExamSessionTable as EST

  Inner Join ScheduledExamsTable as SCET
  on SCET.ID = EST.ScheduledExamID
  
  Inner Join UserTable as UT
  on UT.ID = EST.UserID

  Inner Join CentreTable as CT
  on CT.ID = SCET.CentreID

  Inner Join ExamSessionTable_Shredded as ESTS
  on ESTS.examSessionId = EST.ID

where ut.CandidateRef = '10346719'




update ExamSessionTable
set StructureXML = '<assessmentDetails>
  <assessmentID>2030</assessmentID>
  <qualificationID>26</qualificationID>
  <qualificationName>Personal Taxation (AQ2010)</qualificationName>
  <qualificationReference>AATQCF</qualificationReference>
  <assessmentGroupName>PTX FA 2013</assessmentGroupName>
  <assessmentGroupID>1915</assessmentGroupID>
  <assessmentGroupReference>CAPTX</assessmentGroupReference>
  <assessmentName>PTX FA 2013</assessmentName>
  <validFromDate>06 Jan 2014</validFromDate>
  <expiryDate>31 Jul 2014</expiryDate>
  <startTime>00:00:00</startTime>
  <endTime>23:59:59</endTime>
  <duration>120</duration>
  <defaultDuration>120</defaultDuration>
  <scheduledDuration>
    <value>120</value>
    <reason></reason>
  </scheduledDuration>
  <sRBonus>0</sRBonus>
  <sRBonusMaximum>40</sRBonusMaximum>
  <externalReference>CAPTX</externalReference>
  <passLevelValue>70</passLevelValue>
  <passLevelType>1</passLevelType>
  <status>2</status>
  <testFeedbackType>
    <passFail>0</passFail>
    <percentageMark>0</percentageMark>
    <allowSummaryFeedback>0</allowSummaryFeedback>
    <summaryFeedbackType>1</summaryFeedbackType>
    <itemSummary>0</itemSummary>
    <itemReview>0</itemReview>
    <itemReviewCorrectAnswer>0</itemReviewCorrectAnswer>
    <itemFeedback>0</itemFeedback>
    <printableSummary>0</printableSummary>
    <candidateDetails>0</candidateDetails>
    <feedbackByReference>0</feedbackByReference>
    <allowFeedbackDuringExam>0</allowFeedbackDuringExam>
    <allowFeedbackDuringSummary>0</allowFeedbackDuringSummary>
  </testFeedbackType>
  <itemFeedback>0</itemFeedback>
  <allowCalculator>0</allowCalculator>
  <lastInUseDate>06 Dec 2013 14:39:22</lastInUseDate>
  <completedScriptReview>0</completedScriptReview>
  <assessment currentSection="2" totalTime="0" currentTime="0">
    <intro id="0" name="Introduction" currentItem="">
      <item id="882P4434" name="Introduction" totalMark="1" version="14" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
    </intro>
    <outro id="0" name="Finish" currentItem="" />
    <tools id="0" name="Tools" currentItem="">
      <item id="882P4143" name="PTAX FA2013 Pop up 1" toolName="PTAX FA2013 Pop up 1" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="882P4144" name="PTAX FA2013 Pop up 2" toolName="PTAX FA2013 Pop up 2" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
    </tools>
    <section id="1" name="1" passLevelValue="60" passLevelType="1" itemsToMark="0" currentItem="882P4288" fixed="0">
      <item id="882P4152" name="1.1 PTX FA13 KB 002" totalMark="2" version="18" markingType="0" markingState="0" userMark="0.5" markerUserMark="" userAttempted="1" viewingTime="58999" flagged="0" LO="1.1 Legislation and procedures" unit="Personal Tax" quT="10" />
      <item id="882P4163" name="1.2 PTX FA13 KB 002" totalMark="2" version="18" markingType="0" markingState="0" userMark="1" markerUserMark="" userAttempted="1" viewingTime="56020" flagged="0" LO="1.2 Duties and responsibilities of a tax practitioner" unit="Personal Tax" quT="16" />
      <item id="882P4175" name="1.3 PTX FA13 KB 004" totalMark="2" version="16" markingType="0" markingState="0" userMark="0.5" markerUserMark="" userAttempted="1" viewingTime="83835" flagged="0" LO="1.3 Legislative features of income from employment" unit="Personal Tax" quT="10,12" />
      <item id="882P4191" name="1.4 PTX FA13 KB 009" totalMark="3" version="13" markingType="0" markingState="0" userMark="0.66666666" markerUserMark="" userAttempted="1" viewingTime="364058" flagged="0" LO="1.4 Assessment for employment income" unit="Personal Tax" quT="11" />
      <item id="882P4202" name="1.5 PTX FA13 KB 009" totalMark="9" version="15" markingType="0" markingState="0" userMark="0.77777777" markerUserMark="" userAttempted="1" viewingTime="273437" flagged="0" LO="1.5 Income from employment (including benefits in kind)" unit="Personal Tax" quT="10,11" />
      <item id="882P4206" name="1.6 PTX FA13 KB 002" totalMark="8" version="19" markingType="0" markingState="0" userMark="0.375" markerUserMark="" userAttempted="1" viewingTime="228088" flagged="0" LO="1.6 Taxable benefits in kind" unit="Personal Tax" quT="10,11" />
      <item id="882P4220" name="1.7 PTX FA13 KB 005" totalMark="4" version="18" markingType="0" markingState="0" userMark="0.75" markerUserMark="" userAttempted="1" viewingTime="79826" flagged="0" LO="1.7 Tax free benefits" unit="Personal Tax" quT="16" />
      <item id="882P4234" name="1.8 PTX FA13 KB 008" totalMark="4" version="19" markingType="0" markingState="0" userMark="1" markerUserMark="" userAttempted="1" viewingTime="48688" flagged="0" LO="1.8 Deduction from employment income" unit="Personal Tax" quT="10" />
      <item id="882P4241" name="1.9 PTX FA13 KB 004" totalMark="4" version="17" markingType="0" markingState="0" userMark="0.75" markerUserMark="" userAttempted="1" viewingTime="493896" flagged="0" LO="1.9 Income from savings" unit="Personal Tax" quT="11" />
      <item id="882P4256" name="1.10 PTX FA13 KB 008" totalMark="2" version="19" markingType="0" markingState="0" userMark="1" markerUserMark="" userAttempted="1" viewingTime="63165" flagged="0" LO="1.10 Income from dividends" unit="Personal Tax" quT="11" />
      <item id="882P4264" name="1.11 PTX FA13 KB 005" totalMark="2" version="13" markingType="0" markingState="0" userMark="0.5" markerUserMark="" userAttempted="1" viewingTime="120089" flagged="0" LO="1.11 Rules for income treated as exempt" unit="Personal Tax" quT="11,12" />
      <item id="882P4277" name="1.12 PTX FA13 KB 007" totalMark="7" version="24" markingType="1" markingState="1" userMark="1" markerUserMark="7" userAttempted="1" viewingTime="486549" flagged="0" LO="1.12 Schedule of income from all sources" unit="Personal Tax" quT="20" />
      <item id="882P4288" name="1.13 PTX FA13 KB 007" totalMark="9" version="20" markingType="1" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="358598" flagged="0" LO="1.13 Self assessment, payments on account and balancing payment" unit="Personal Tax" quT="11,20" />
      <item id="882P4295" name="1.14 PTX FA13 KB 003" totalMark="7" version="31" markingType="1" markingState="1" userMark="0" markerUserMark="7" userAttempted="1" viewingTime="463569" flagged="0" LO="1.14 Completing and filing tax returns" unit="Personal Tax" quT="11" />
    </section>
    <section id="2" name="2" passLevelValue="60" passLevelType="1" itemsToMark="0" currentItem="882P4361" fixed="0">
      <item id="882P4311" name="2.1 PTX FA13 KB 008" totalMark="1" version="14" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="1" viewingTime="102758" flagged="0" LO="2.1 Legislative features relating to property income" unit="Personal Tax" quT="10" />
      <item id="882P4318" name="2.2 PTX FA13 KB 004" totalMark="7" version="23" markingType="0" markingState="0" userMark="0.85714285" markerUserMark="" userAttempted="1" viewingTime="372264" flagged="0" LO="2.2 Income from furnished and unfurnished property" unit="Personal Tax" quT="11" />
      <item id="882P4333" name="2.3 PTX FA13 KB 009" totalMark="4" version="14" markingType="0" markingState="0" userMark="1" markerUserMark="" userAttempted="1" viewingTime="202222" flagged="0" LO="2.3 Income from rent a room schemes and buy to let investments" unit="Personal Tax" quT="10,11" />
      <item id="882P4338" name="2.4 PTX FA13 KB 003" totalMark="2" version="17" markingType="0" markingState="0" userMark="1" markerUserMark="" userAttempted="1" viewingTime="194813" flagged="0" LO="2.4 Chargeable persons, chargeable assets and chargeable disposals" unit="Personal Tax" quT="10" />
      <item id="882P4353" name="2.5 PTX FA13 KB 007" totalMark="3" version="15" markingType="0" markingState="0" userMark="1" markerUserMark="" userAttempted="1" viewingTime="215608" flagged="0" LO="2.5 Chargeable disposals" unit="Personal Tax" quT="11,12" />
      <item id="882P4361" name="2.6 PTX FA13 KB 004" totalMark="7" version="18" markingType="1" markingState="1" userMark="1" markerUserMark="5" userAttempted="1" viewingTime="737757" flagged="0" LO="2.6 Shares and rights issues" unit="Personal Tax" quT="20" />
      <item id="882P4370" name="2.7 PTX FA13 KB 002" totalMark="5" version="27" markingType="0" markingState="0" userMark="1" markerUserMark="" userAttempted="1" viewingTime="412308" flagged="0" LO="2.7 Part disposal and chattel" unit="Personal Tax" quT="11" />
      <item id="882P4387" name="2.8 PTX FA13 KB 008" totalMark="3" version="18" markingType="0" markingState="0" userMark="0.66666666" markerUserMark="" userAttempted="1" viewingTime="163582" flagged="0" LO="2.8 Exempt assets " unit="Personal Tax" quT="10,11" />
      <item id="882P4392" name="2.9 PTX FA13 KB 002" totalMark="3" version="15" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="1" viewingTime="231800" flagged="0" LO="2.9 Annual exemption, loss relief and capital gains tax payable" unit="Personal Tax" quT="11" />
    </section>
  </assessment>
  <isValid>1</isValid>
  <isPrintable>0</isPrintable>
  <qualityReview>0</qualityReview>
  <numberOfGenerations>1</numberOfGenerations>
  <requiresInvigilation>1</requiresInvigilation>
  <advanceContentDownloadTimespan>60</advanceContentDownloadTimespan>
  <automaticVerification>0</automaticVerification>
  <offlineMode>2</offlineMode>
  <requiresValidation>0</requiresValidation>
  <requiresSecureClient>1</requiresSecureClient>
  <examType>0</examType>
  <advanceDownload>0</advanceDownload>
  <gradeBoundaryData>
    <gradeBoundaries passLevelType="1" adjustedGradeBoundaries="0">
      <grade modifier="lt" value="70" description="Fail" />
      <grade modifier="gt" value="70" description="Pass" />
    </gradeBoundaries>
  </gradeBoundaryData>
  <autoViewExam>0</autoViewExam>
  <autoProgressItems>0</autoProgressItems>
  <confirmationText>
    <confirmationText></confirmationText>
  </confirmationText>
  <requiresConfirmationCheck>0</requiresConfirmationCheck>
  <allowCloseWithoutSubmit>0</allowCloseWithoutSubmit>
  <useSecureMarker>0</useSecureMarker>
  <annotationVersion>0</annotationVersion>
  <allowPackagingDelivery>1</allowPackagingDelivery>
  <scoreBoundaryData>
    <scoreBoundaries showScoreBoundaryColumn="1">
      <score modifier="lt" value="36" description="Significantly below" higherBoundarySet="1" />
      <score modifier="lt" value="36" description="Significantly below" higherBoundarySet="0" />
      <score modifier="gt" value="36" description="Below requirement" higherBoundarySet="1" />
      <score modifier="gt" value="60" description="Borderline" higherBoundarySet="1" />
      <score modifier="gt" value="70" description="Met" higherBoundarySet="0" />
      <score modifier="gt" value="70" description="Met" higherBoundarySet="1" />
      <score modifier="gt" value="36" description="Below requirement" higherBoundarySet="0" userCreated="1" />
      <score modifier="gt" value="60" description="Borderline" higherBoundarySet="0" userCreated="1" />
      <score modifier="gt" value="85" description="Exceeded" higherBoundarySet="1" userCreated="1" />
    </scoreBoundaries>
  </scoreBoundaryData>
  <showCandidateReportResultColumn>0</showCandidateReportResultColumn>
  <showCandidateReportPercentageColumn>1</showCandidateReportPercentageColumn>
  <certifiedAccessible>0</certifiedAccessible>
  <markingProgressVisible>1</markingProgressVisible>
  <markingProgressMode>0</markingProgressMode>
  <secureClientOperationMode>1</secureClientOperationMode>
  <examDeliverySystemStyleType>delivery</examDeliverySystemStyleType>
  <markingAutoVoidPeriod>365</markingAutoVoidPeriod>
  <showPageRequiresScrollingAlert>0</showPageRequiresScrollingAlert>
  <project ID="882" />
</assessmentDetails>', resultData = '<exam passMark="70" passType="1" originalPassMark="70" originalPassType="1" totalMark="100" userMark="70.900" userPercentage="70.900" passValue="1" originalPassValue="1" totalTimeSpent="5811929" closeBoundaryType="0" closeBoundaryValue="0" grade="Pass" originalGrade="Pass">
  <gradeBoundaryData>
    <gradeBoundaries passLevelType="1" adjustedGradeBoundaries="0">
      <grade modifier="lt" value="70" description="Fail" />
      <grade modifier="gt" value="70" description="Pass" />
    </gradeBoundaries>
  </gradeBoundaryData>
  <section id="1" passMark="60" passType="1" name="1" totalMark="65" userMark="43.900" userPercentage="67.538" passValue="1" totalTimeSpent="3178817" itemsToMark="0">
    <item id="882P4152" name="1.1 PTX FA13 KB 002" totalMark="2" userMark="0.5" actualUserMark="1.0" markingType="0" markerUserMark="" viewingTime="58999" userAttempted="1" version="18" />
    <item id="882P4163" name="1.2 PTX FA13 KB 002" totalMark="2" userMark="1" actualUserMark="2" markingType="0" markerUserMark="" viewingTime="56020" userAttempted="1" version="18" />
    <item id="882P4175" name="1.3 PTX FA13 KB 004" totalMark="2" userMark="0.5" actualUserMark="1.0" markingType="0" markerUserMark="" viewingTime="83835" userAttempted="1" version="16" />
    <item id="882P4191" name="1.4 PTX FA13 KB 009" totalMark="3" userMark="0.66666666" actualUserMark="1.99999998" markingType="0" markerUserMark="" viewingTime="364058" userAttempted="1" version="13" />
    <item id="882P4202" name="1.5 PTX FA13 KB 009" totalMark="9" userMark="0.77777777" actualUserMark="6.99999993" markingType="0" markerUserMark="" viewingTime="273437" userAttempted="1" version="15" />
    <item id="882P4206" name="1.6 PTX FA13 KB 002" totalMark="8" userMark="0.375" actualUserMark="3.000" markingType="0" markerUserMark="" viewingTime="228088" userAttempted="1" version="19" />
    <item id="882P4220" name="1.7 PTX FA13 KB 005" totalMark="4" userMark="0.75" actualUserMark="3.00" markingType="0" markerUserMark="" viewingTime="79826" userAttempted="1" version="18" />
    <item id="882P4234" name="1.8 PTX FA13 KB 008" totalMark="4" userMark="1" actualUserMark="4" markingType="0" markerUserMark="" viewingTime="48688" userAttempted="1" version="19" />
    <item id="882P4241" name="1.9 PTX FA13 KB 004" totalMark="4" userMark="0.75" actualUserMark="3.00" markingType="0" markerUserMark="" viewingTime="493896" userAttempted="1" version="17" />
    <item id="882P4256" name="1.10 PTX FA13 KB 008" totalMark="2" userMark="1" actualUserMark="2" markingType="0" markerUserMark="" viewingTime="63165" userAttempted="1" version="19" />
    <item id="882P4264" name="1.11 PTX FA13 KB 005" totalMark="2" userMark="0.5" actualUserMark="1.0" markingType="0" markerUserMark="" viewingTime="120089" userAttempted="1" version="13" />
    <item id="882P4277" name="1.12 PTX FA13 KB 007" totalMark="7" userMark="0" actualUserMark="7" markingType="1" markerUserMark="7" viewingTime="486549" userAttempted="1" version="24" />
    <item id="882P4288" name="1.13 PTX FA13 KB 007" totalMark="9" userMark="0" actualUserMark="0" markingType="1" markerUserMark="" viewingTime="358598" userAttempted="0" version="20" />
    <item id="882P4295" name="1.14 PTX FA13 KB 003" totalMark="7" userMark="0" actualUserMark="7" markingType="1" markerUserMark="7" viewingTime="463569" userAttempted="1" version="31" />
  </section>
  <section id="2" passMark="60" passType="1" name="2" totalMark="35" userMark="27.000" userPercentage="77.143" passValue="1" totalTimeSpent="2633112" itemsToMark="0">
    <item id="882P4311" name="2.1 PTX FA13 KB 008" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="102758" userAttempted="1" version="14" />
    <item id="882P4318" name="2.2 PTX FA13 KB 004" totalMark="7" userMark="0.85714285" actualUserMark="5.99999995" markingType="0" markerUserMark="" viewingTime="372264" userAttempted="1" version="23" />
    <item id="882P4333" name="2.3 PTX FA13 KB 009" totalMark="4" userMark="1" actualUserMark="4" markingType="0" markerUserMark="" viewingTime="202222" userAttempted="1" version="14" />
    <item id="882P4338" name="2.4 PTX FA13 KB 003" totalMark="2" userMark="1" actualUserMark="2" markingType="0" markerUserMark="" viewingTime="194813" userAttempted="1" version="17" />
    <item id="882P4353" name="2.5 PTX FA13 KB 007" totalMark="3" userMark="1" actualUserMark="3" markingType="0" markerUserMark="" viewingTime="215608" userAttempted="1" version="15" />
    <item id="882P4361" name="2.6 PTX FA13 KB 004" totalMark="7" userMark="0" actualUserMark="5" markingType="1" markerUserMark="5" viewingTime="737757" userAttempted="1" version="18" />
    <item id="882P4370" name="2.7 PTX FA13 KB 002" totalMark="5" userMark="1" actualUserMark="5" markingType="0" markerUserMark="" viewingTime="412308" userAttempted="1" version="27" />
    <item id="882P4387" name="2.8 PTX FA13 KB 008" totalMark="3" userMark="0.66666666" actualUserMark="1.99999998" markingType="0" markerUserMark="" viewingTime="163582" userAttempted="1" version="18" />
    <item id="882P4392" name="2.9 PTX FA13 KB 002" totalMark="3" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="231800" userAttempted="1" version="15" />
  </section>
</exam>', resultDataFull = '<assessmentDetails>
  <assessmentID>2030</assessmentID>
  <qualificationID>26</qualificationID>
  <qualificationName>Personal Taxation (AQ2010)</qualificationName>
  <qualificationReference>AATQCF</qualificationReference>
  <assessmentGroupName>PTX FA 2013</assessmentGroupName>
  <assessmentGroupID>1915</assessmentGroupID>
  <assessmentGroupReference>CAPTX</assessmentGroupReference>
  <assessmentName>PTX FA 2013</assessmentName>
  <validFromDate>06 Jan 2014</validFromDate>
  <expiryDate>31 Jul 2014</expiryDate>
  <startTime>00:00:00</startTime>
  <endTime>23:59:59</endTime>
  <duration>120</duration>
  <defaultDuration>120</defaultDuration>
  <scheduledDuration>
    <value>120</value>
    <reason />
  </scheduledDuration>
  <sRBonus>0</sRBonus>
  <sRBonusMaximum>40</sRBonusMaximum>
  <externalReference>CAPTX</externalReference>
  <passLevelValue>70</passLevelValue>
  <passLevelType>1</passLevelType>
  <status>2</status>
  <testFeedbackType>
    <passFail>0</passFail>
    <percentageMark>0</percentageMark>
    <allowSummaryFeedback>0</allowSummaryFeedback>
    <summaryFeedbackType>1</summaryFeedbackType>
    <itemSummary>0</itemSummary>
    <itemReview>0</itemReview>
    <itemReviewCorrectAnswer>0</itemReviewCorrectAnswer>
    <itemFeedback>0</itemFeedback>
    <printableSummary>0</printableSummary>
    <candidateDetails>0</candidateDetails>
    <feedbackByReference>0</feedbackByReference>
    <allowFeedbackDuringExam>0</allowFeedbackDuringExam>
    <allowFeedbackDuringSummary>0</allowFeedbackDuringSummary>
  </testFeedbackType>
  <itemFeedback>0</itemFeedback>
  <allowCalculator>0</allowCalculator>
  <lastInUseDate>06 Dec 2013 14:39:22</lastInUseDate>
  <completedScriptReview>0</completedScriptReview>
  <assessment currentSection="2" totalTime="0" currentTime="0" originalPassMark="70" originalPassType="1" passMark="70" passType="1" totalMark="100" userMark="70.900" userPercentage="70.900" passValue="1" originalGrade="Pass">
    <intro id="0" name="Introduction" currentItem="">
      <item id="882P4434" name="Introduction" totalMark="1" version="14" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
    </intro>
    <outro id="0" name="Finish" currentItem="" />
    <tools id="0" name="Tools" currentItem="">
      <item id="882P4143" name="PTAX FA2013 Pop up 1" toolName="PTAX FA2013 Pop up 1" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="882P4144" name="PTAX FA2013 Pop up 2" toolName="PTAX FA2013 Pop up 2" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
    </tools>
    <section id="1" name="1" passLevelValue="60" passLevelType="1" itemsToMark="0" currentItem="882P4288" fixed="0" totalMark="65" userMark="43.900" userPercentage="67.538" passValue="1">
      <item id="882P4152" name="1.1 PTX FA13 KB 002" totalMark="2" version="18" markingType="0" markingState="0" userMark="0.5" markerUserMark="" userAttempted="1" viewingTime="58999" flagged="0" LO="1.1 Legislation and procedures" unit="Personal Tax" quT="10">
        <p um="0.5" cs="1" ua="1" id="882P4152">
          <s ua="1" um="0.5" id="1">
            <c wei="1" maS="1" ie="1" typ="10" rv="0" mv="0" ua="1" um="0" id="45">
              <i ac="A" sl="1" id="1" />
              <i ca="1" ac="B" sl="0" id="2" />
            </c>
            <c wei="1" maS="1" ie="1" typ="10" rv="0" mv="0" ua="1" um="1" id="46">
              <i ac="A" sl="0" id="1" />
              <i ca="1" ac="B" sl="1" id="2" />
            </c>
            <c typ="4" id="47">
              <i cc="0" id="1" />
            </c>
            <c typ="4" id="48">
              <i cc="0" id="1" />
            </c>
          </s>
        </p>
      </item>
      <item id="882P4163" name="1.2 PTX FA13 KB 002" totalMark="2" version="18" markingType="0" markingState="0" userMark="1" markerUserMark="" userAttempted="1" viewingTime="56020" flagged="0" LO="1.2 Duties and responsibilities of a tax practitioner" unit="Personal Tax" quT="16">
        <p um="1" cs="1" ua="1" id="882P4163">
          <s ua="1" um="1" id="1">
            <c ua="1" um="1" typ="16" id="17">
              <z id="11">
                <i y="260" x="630" ID="11" />
              </z>
              <z id="10" />
              <z id="6">
                <i y="220" x="495" ID="1" />
              </z>
              <z id="5">
                <i y="140" x="495" ID="1" />
              </z>
              <z id="2">
                <i y="215" x="630" ID="2" />
              </z>
              <z id="1" />
            </c>
            <c typ="4" id="21">
              <i cc="0" id="1" />
            </c>
            <c typ="4" id="22">
              <i cc="0" id="1" />
            </c>
          </s>
        </p>
      </item>
      <item id="882P4175" name="1.3 PTX FA13 KB 004" totalMark="2" version="16" markingType="0" markingState="0" userMark="0.5" markerUserMark="" userAttempted="1" viewingTime="83835" flagged="0" LO="1.3 Legislative features of income from employment" unit="Personal Tax" quT="10,12">
        <p um="0.5" cs="1" ua="1" id="882P4175">
          <s ua="1" um="0.5" id="1">
            <c wei="1" maS="1" ie="1" typ="10" rv="0" mv="0" ua="1" um="0" id="42">
              <i ca="1" ac="A" sl="0" id="1" />
              <i ac="B" sl="0" id="2" />
              <i ac="C" sl="1" id="3" />
              <i ac="D" sl="0" id="4" />
            </c>
            <c wei="1" maS="0" ie="1" typ="12" rv="0" mv="0" ua="1" um="1" id="70">
              <i sl="3" id="1" />
            </c>
            <c typ="4" id="71">
              <i cc="0" id="1" />
            </c>
            <c typ="4" id="72">
              <i cc="0" id="1" />
            </c>
          </s>
        </p>
      </item>
      <item id="882P4191" name="1.4 PTX FA13 KB 009" totalMark="3" version="13" markingType="0" markingState="0" userMark="0.66666666" markerUserMark="" userAttempted="1" viewingTime="364058" flagged="0" LO="1.4 Assessment for employment income" unit="Personal Tax" quT="11">
        <p um="0.666666666666667" cs="1" ua="1" id="882P4191">
          <s ua="1" um="0.67" id="1">
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="58">
              <i ca="10614" id="1">10,614</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="59">
              <i ca="1308" id="1">600</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="60">
              <i ca="1188" id="1">1,188</i>
            </c>
            <c typ="4" id="61">
              <i cc="0" id="1" />
            </c>
            <c typ="4" id="62">
              <i cc="0" id="1" />
            </c>
          </s>
        </p>
      </item>
      <item id="882P4202" name="1.5 PTX FA13 KB 009" totalMark="9" version="15" markingType="0" markingState="0" userMark="0.77777777" markerUserMark="" userAttempted="1" viewingTime="273437" flagged="0" LO="1.5 Income from employment (including benefits in kind)" unit="Personal Tax" quT="10,11">
        <p um="0.777777777777778" cs="1" ua="1" id="882P4202">
          <s ua="1" um="0.71" id="1">
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="41">
              <i ca="26" id="1">26</i>
            </c>
            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="42">
              <i ca="" id="1">3042</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="45">
              <i ca="32" id="1">32</i>
            </c>
            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="46">
              <i ca="" id="1">2863</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="47">
              <i ca="" id="1">5905</i>
            </c>
            <c typ="4" id="49">
              <i cc="2" id="1" />
            </c>
            <c typ="4" id="53">
              <i cc="1" id="1" />
            </c>
          </s>
          <s ua="1" um="1" id="2">
            <c wei="1" maS="1" ie="1" typ="10" rv="0" mv="0" ua="1" um="1" id="8">
              <i ac="A" sl="0" id="1" />
              <i ca="1" ac="B" sl="1" id="2" />
            </c>
            <c wei="1" maS="1" ie="1" typ="10" rv="0" mv="0" ua="1" um="1" id="9">
              <i ca="1" ac="A" sl="1" id="1" />
              <i ac="B" sl="0" id="2" />
            </c>
          </s>
        </p>
      </item>
      <item id="882P4206" name="1.6 PTX FA13 KB 002" totalMark="8" version="19" markingType="0" markingState="0" userMark="0.375" markerUserMark="" userAttempted="1" viewingTime="228088" flagged="0" LO="1.6 Taxable benefits in kind" unit="Personal Tax" quT="10,11">
        <p um="0.375" cs="1" ua="1" id="882P4206">
          <s ua="1" um="0.25" id="1">
            <c wei="1" maS="1" ie="1" typ="10" rv="0" mv="0" ua="1" um="0" id="6">
              <i ca="1" ac="A" sl="0" id="1" />
              <i ac="B" sl="1" id="2" />
            </c>
            <c wei="1" maS="1" ie="1" typ="10" rv="0" mv="0" ua="1" um="0" id="7">
              <i ca="1" ac="A" sl="0" id="1" />
              <i ac="B" sl="1" id="2" />
            </c>
            <c wei="1" maS="1" ie="1" typ="10" rv="0" mv="0" ua="1" um="0" id="11">
              <i ac="A" sl="1" id="1" />
              <i ca="1" ac="B" sl="0" id="2" />
            </c>
            <c wei="1" maS="1" ie="1" typ="10" rv="0" mv="0" ua="1" um="1" id="12">
              <i ac="A" sl="0" id="1" />
              <i ca="1" ac="B" sl="1" id="2" />
            </c>
            <c typ="4" id="20">
              <i cc="0" id="1" />
            </c>
            <c typ="4" id="21">
              <i cc="1" id="1" />
            </c>
          </s>
          <s ua="1" um="0.5" id="2">
            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="2">
              <i ca="6600" id="1">6,600</i>
            </c>
            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="3">
              <i ca="600" id="1">280</i>
            </c>
          </s>
        </p>
      </item>
      <item id="882P4220" name="1.7 PTX FA13 KB 005" totalMark="4" version="18" markingType="0" markingState="0" userMark="0.75" markerUserMark="" userAttempted="1" viewingTime="79826" flagged="0" LO="1.7 Tax free benefits" unit="Personal Tax" quT="16">
        <p um="0.75" cs="1" ua="1" id="882P4220">
          <s ua="1" um="0.75" id="1">
            <c ua="1" um="0.75" typ="16" id="17">
              <z id="10" />
              <z id="8">
                <i y="400" x="585" ID="2" />
              </z>
              <z id="7">
                <i y="305" x="585" ID="2" />
              </z>
              <z id="6">
                <i y="215" x="585" ID="2" />
              </z>
              <z id="5">
                <i y="125" x="585" ID="1" />
              </z>
              <z id="2">
                <i y="270" x="715" ID="2" />
              </z>
              <z id="1">
                <i y="160" x="715" ID="1" />
              </z>
            </c>
            <c typ="4" id="21">
              <i cc="0" id="1" />
            </c>
            <c typ="4" id="22">
              <i cc="0" id="1" />
            </c>
          </s>
        </p>
      </item>
      <item id="882P4234" name="1.8 PTX FA13 KB 008" totalMark="4" version="19" markingType="0" markingState="0" userMark="1" markerUserMark="" userAttempted="1" viewingTime="48688" flagged="0" LO="1.8 Deduction from employment income" unit="Personal Tax" quT="10">
        <p um="1" cs="1" ua="1" id="882P4234">
          <s ua="1" um="1" id="1">
            <c wei="1" maS="1" ie="1" typ="10" rv="0" mv="0" ua="1" um="1" id="33">
              <i ac="A" sl="0" id="1" />
              <i ca="1" ac="B" sl="1" id="2" />
            </c>
            <c wei="1" maS="1" ie="1" typ="10" rv="0" mv="0" ua="1" um="1" id="38">
              <i ac="A" sl="0" id="1" />
              <i ca="1" ac="B" sl="1" id="2" />
            </c>
            <c wei="1" maS="1" ie="1" typ="10" rv="0" mv="0" ua="1" um="1" id="47">
              <i ca="1" ac="A" sl="1" id="1" />
              <i ac="B" sl="0" id="2" />
            </c>
            <c wei="1" maS="1" ie="1" typ="10" rv="0" mv="0" ua="1" um="1" id="48">
              <i ca="1" ac="A" sl="1" id="1" />
              <i ac="B" sl="0" id="2" />
            </c>
            <c typ="4" id="58">
              <i cc="0" id="1" />
            </c>
            <c typ="4" id="62">
              <i cc="0" id="1" />
            </c>
          </s>
        </p>
      </item>
      <item id="882P4241" name="1.9 PTX FA13 KB 004" totalMark="4" version="17" markingType="0" markingState="0" userMark="0.75" markerUserMark="" userAttempted="1" viewingTime="493896" flagged="0" LO="1.9 Income from savings" unit="Personal Tax" quT="11">
        <p um="0.75" cs="1" ua="1" id="882P4241">
          <s ua="1" um="0.75" id="1">
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="29">
              <i ca="0" id="1">0</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="30">
              <i ca="640" id="1">640</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="31">
              <i ca="640" id="1">384</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="32">
              <i ca="280" id="1">280</i>
            </c>
            <c typ="4" id="33">
              <i cc="1" id="1" />
            </c>
            <c typ="4" id="34">
              <i cc="0" id="1" />
            </c>
          </s>
        </p>
      </item>
      <item id="882P4256" name="1.10 PTX FA13 KB 008" totalMark="2" version="19" markingType="0" markingState="0" userMark="1" markerUserMark="" userAttempted="1" viewingTime="63165" flagged="0" LO="1.10 Income from dividends" unit="Personal Tax" quT="11">
        <p um="1" cs="1" ua="1" id="882P4256">
          <s ua="1" um="1" id="1">
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="29">
              <i ca="125" id="1">125</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="32">
              <i ca="1250" id="1">1250</i>
            </c>
            <c typ="4" id="37">
              <i cc="0" id="1" />
            </c>
            <c typ="4" id="41">
              <i cc="0" id="1" />
            </c>
          </s>
        </p>
      </item>
      <item id="882P4264" name="1.11 PTX FA13 KB 005" totalMark="2" version="13" markingType="0" markingState="0" userMark="0.5" markerUserMark="" userAttempted="1" viewingTime="120089" flagged="0" LO="1.11 Rules for income treated as exempt" unit="Personal Tax" quT="11,12">
        <p um="0.5" cs="1" ua="1" id="882P4264">
          <s ua="1" um="0.5" id="1">
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="44">
              <i ca="760" id="1">6,520.00</i>
            </c>
            <c wei="1" maS="0" ie="1" typ="12" rv="0" mv="0" ua="1" um="1" id="26">
              <i sl="3" id="1" />
            </c>
            <c typ="4" id="52">
              <i cc="0" id="1" />
            </c>
            <c typ="4" id="56">
              <i cc="0" id="1" />
            </c>
          </s>
        </p>
      </item>
      <item id="882P4277" name="1.12 PTX FA13 KB 007" totalMark="7" version="24" markingType="1" markingState="1" userMark="1" markerUserMark="7" userAttempted="1" viewingTime="486549" flagged="0" LO="1.12 Schedule of income from all sources" unit="Personal Tax" quT="20">
        <p um="1" cs="1" ua="1" id="882P4277">
          <s ua="1" um="1" id="1">
            <c wei="7" maS="0" ie="1" typ="20" rv="0" mv="0" ua="1" um="1" id="19">
              <i um="1" ia="1" id="1">&lt;table extensionName="HIGHLIGHT_TABLE"&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;Amount in �&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;Salary Income&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;22,000&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;Property Income&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;6,000&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;Total &lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;28,000&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;Personal allowance&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;9,550&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;Taxable income&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;18,450&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;Tax liability 18,450@20%&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;3,690&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;Personal allowance calculation&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;28,000-26,100=1,900/2=950&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;10,500-950=9,550&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;/table&gt;</i>
            </c>
            <c typ="4" id="25">
              <i cc="0" id="1" />
            </c>
            <c typ="4" id="29">
              <i cc="6" id="1" />
            </c>
          </s>
        </p>
      </item>
      <item id="882P4288" name="1.13 PTX FA13 KB 007" totalMark="9" version="20" markingType="1" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="358598" flagged="0" LO="1.13 Self assessment, payments on account and balancing payment" unit="Personal Tax" quT="11,20">
        <p um="0" cs="1" ua="0" id="882P4288">
          <s ua="0" um="0" id="1">
            <c wei="9" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="0" um="0" id="37">
              <i ca="" id="1" />
            </c>
            <c wei="1" maS="0" ie="1" typ="20" rv="0" mv="0" ua="0" um="1" id="38">
              <i um="1" id="1">&lt;table extensionName="HIGHLIGHT_TABLE"&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;/table&gt;</i>
            </c>
            <c typ="4" id="42">
              <i cc="0" id="1" />
            </c>
            <c typ="4" id="46">
              <i cc="0" id="1" />
            </c>
          </s>
        </p>
      </item>
      <item id="882P4295" name="1.14 PTX FA13 KB 003" totalMark="7" version="31" markingType="1" markingState="1" userMark="0" markerUserMark="7" userAttempted="1" viewingTime="463569" flagged="0" LO="1.14 Completing and filing tax returns" unit="Personal Tax" quT="11">
        <p um="0" cs="1" ua="1" id="882P4295">
          <s ua="0" um="-1" id="1">
            <c typ="4" id="30">
              <i cc="0" id="1" />
            </c>
            <c typ="4" id="34">
              <i cc="0" id="1" />
            </c>
          </s>
          <s ua="1" um="0" id="5">
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="2">
              <i ca="" id="1">22,000</i>
              <i ca="" id="2" />
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="0" um="0" id="3">
              <i ca="" id="1" />
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="0" um="0" id="4">
              <i ca="" id="1" />
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="5">
              <i ca="" id="1">Glen Tree</i>
            </c>
          </s>
          <s ua="1" um="0" id="6">
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="2">
              <i ca="" id="1">1,000</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="0" um="0" id="3">
              <i ca="" id="1" />
              <i ca="" id="2" />
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="4">
              <i ca="" id="1">1,000</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="0" um="0" id="5">
              <i ca="" id="1" />
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="0" um="0" id="6">
              <i ca="" id="1" />
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="0" um="0" id="7">
              <i ca="" id="1" />
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="0" um="0" id="8">
              <i ca="" id="1" />
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="0" um="0" id="11">
              <i ca="" id="1" />
            </c>
          </s>
          <s ua="1" um="0" id="7">
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="4">
              <i ca="" id="1">3</i>
              <i ca="" id="2">54,000</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="5">
              <i ca="" id="1">22,000</i>
              <i ca="" id="2">33,000</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="0" um="0" id="6">
              <i ca="" id="1" />
              <i ca="" id="2" />
            </c>
          </s>
        </p>
      </item>
    </section>
    <section id="2" name="2" passLevelValue="60" passLevelType="1" itemsToMark="0" currentItem="882P4361" fixed="0" totalMark="35" userMark="27.000" userPercentage="77.143" passValue="1">
      <item id="882P4311" name="2.1 PTX FA13 KB 008" totalMark="1" version="14" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="1" viewingTime="102758" flagged="0" LO="2.1 Legislative features relating to property income" unit="Personal Tax" quT="10">
        <p um="0" cs="1" ua="1" id="882P4311">
          <s ua="1" um="0" id="1">
            <c wei="1" maS="1" ie="1" typ="10" rv="0" mv="0" ua="1" um="0" id="40">
              <i ac="A" sl="1" id="1" />
              <i ca="1" ac="B" sl="0" id="2" />
              <i ac="C" sl="0" id="3" />
            </c>
            <c typ="4" id="41">
              <i cc="0" id="1" />
            </c>
            <c typ="4" id="45">
              <i cc="0" id="1" />
            </c>
          </s>
        </p>
      </item>
      <item id="882P4318" name="2.2 PTX FA13 KB 004" totalMark="7" version="23" markingType="0" markingState="0" userMark="0.85714285" markerUserMark="" userAttempted="1" viewingTime="372264" flagged="0" LO="2.2 Income from furnished and unfurnished property" unit="Personal Tax" quT="11">
        <p um="0.857142857142857" cs="1" ua="1" id="882P4318">
          <s ua="1" um="0.86" id="1">
            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="29">
              <i ca="11040" id="1">11,040</i>
            </c>
            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="30">
              <i ca="7670" id="1">7,670</i>
            </c>
            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="31">
              <i ca="" id="1">460</i>
            </c>
            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="32">
              <i ca="540" id="1">540</i>
            </c>
            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="33">
              <i ca="610" id="1">610</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="34">
              <i ca="" id="1">9,836</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="35">
              <i ca="" id="1">6,600</i>
            </c>
            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="36">
              <i ca="" id="1">664</i>
            </c>
            <c typ="4" id="45">
              <i cc="0" id="1" />
            </c>
            <c typ="4" id="49">
              <i cc="0" id="1" />
            </c>
          </s>
        </p>
      </item>
      <item id="882P4333" name="2.3 PTX FA13 KB 009" totalMark="4" version="14" markingType="0" markingState="0" userMark="1" markerUserMark="" userAttempted="1" viewingTime="202222" flagged="0" LO="2.3 Income from rent a room schemes and buy to let investments" unit="Personal Tax" quT="10,11">
        <p um="1" cs="1" ua="1" id="882P4333">
          <s ua="1" um="1" id="1">
            <c wei="1" maS="1" ie="1" typ="10" rv="0" mv="0" ua="1" um="1" id="19">
              <i ac="A" sl="0" id="1" />
              <i ca="1" ac="B" sl="1" id="2" />
            </c>
            <c wei="1" maS="1" ie="1" typ="10" rv="0" mv="0" ua="1" um="1" id="20">
              <i ac="A" sl="0" id="1" />
              <i ca="1" ac="B" sl="1" id="2" />
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="25">
              <i ca="2500" id="1">2,500</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="26">
              <i ca="5700" id="1">5,700</i>
            </c>
            <c typ="4" id="35">
              <i cc="0" id="1" />
            </c>
            <c typ="4" id="39">
              <i cc="0" id="1" />
            </c>
          </s>
        </p>
      </item>
      <item id="882P4338" name="2.4 PTX FA13 KB 003" totalMark="2" version="17" markingType="0" markingState="0" userMark="1" markerUserMark="" userAttempted="1" viewingTime="194813" flagged="0" LO="2.4 Chargeable persons, chargeable assets and chargeable disposals" unit="Personal Tax" quT="10">
        <p um="1" cs="1" ua="1" id="882P4338">
          <s ua="1" um="1" id="1">
            <c wei="2" maS="2" ie="1" typ="10" rv="0" mv="0" ua="1" um="1" id="29">
              <i ac="A" sl="0" id="1" />
              <i ca="1" ac="C" sl="1" id="3" />
              <i ca="1" ac="D" sl="1" id="4" />
              <i ac="E" sl="0" id="5" />
              <i ac="F" sl="0" id="6" />
            </c>
            <c typ="4" id="30">
              <i cc="1" id="1" />
            </c>
            <c typ="4" id="34">
              <i cc="1" id="1" />
            </c>
          </s>
        </p>
      </item>
      <item id="882P4353" name="2.5 PTX FA13 KB 007" totalMark="3" version="15" markingType="0" markingState="0" userMark="1" markerUserMark="" userAttempted="1" viewingTime="215608" flagged="0" LO="2.5 Chargeable disposals" unit="Personal Tax" quT="11,12">
        <p um="1" cs="1" ua="1" id="882P4353">
          <s ua="1" um="1" id="1">
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="44">
              <i ca="28500" id="1">28,500</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="52">
              <i ca="25350" id="1">25,350</i>
            </c>
            <c wei="1" maS="0" ie="1" typ="12" rv="0" mv="0" ua="1" um="1" id="26">
              <i sl="2" id="1" />
            </c>
            <c typ="4" id="59">
              <i cc="0" id="1" />
            </c>
            <c typ="4" id="63">
              <i cc="0" id="1" />
            </c>
          </s>
        </p>
      </item>
      <item id="882P4361" name="2.6 PTX FA13 KB 004" totalMark="7" version="18" markingType="1" markingState="1" userMark="1" markerUserMark="5" userAttempted="1" viewingTime="737757" flagged="0" LO="2.6 Shares and rights issues" unit="Personal Tax" quT="20">
        <p um="1" cs="1" ua="1" id="882P4361">
          <s ua="1" um="1" id="1">
            <c wei="1" maS="0" ie="1" typ="20" rv="0" mv="0" ua="1" um="1" id="80">
              <i um="1" ia="1" id="1">&lt;table extensionName="HIGHLIGHT_TABLE"&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;number of shares &lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;cost in � &lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;31/10 Purchase &lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;5,000@5&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;25,000&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;06/04Bonus Issue&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;1,250&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;Total&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;6,250&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;25,000&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;28/01 Sold&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;5,000@10&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;50,000&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;Cost of shares sold&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;(20,000)&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;25,000/6250=4 &lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;Gain&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;30,000&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;4*5000=20000&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;Balance of shares 6,250-5,000&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;1,250&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;5,000&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;21/02 Purchase&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;1500@7.5&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;11,250&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;Total&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;2,750&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;16,250&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;/table&gt;</i>
            </c>
            <c typ="4" id="86">
              <i cc="0" id="1" />
            </c>
            <c typ="4" id="90">
              <i cc="0" id="1" />
            </c>
          </s>
        </p>
      </item>
      <item id="882P4370" name="2.7 PTX FA13 KB 002" totalMark="5" version="27" markingType="0" markingState="0" userMark="1" markerUserMark="" userAttempted="1" viewingTime="412308" flagged="0" LO="2.7 Part disposal and chattel" unit="Personal Tax" quT="11">
        <p um="1" cs="1" ua="1" id="882P4370">
          <s ua="1" um="1" id="1">
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="29">
              <i ca="300000" id="1">300,000</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="32">
              <i ca="" id="1">182,278</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="37">
              <i ca="480000" id="1">480,000</i>
            </c>
            <c wei="2" ca="300000�490000" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="30">
              <i ca="" id="1">300,000</i>
              <i ca="" id="2">490,000</i>
            </c>
            <c typ="4" id="43">
              <i cc="0" id="1" />
            </c>
            <c typ="4" id="47">
              <i cc="0" id="1" />
            </c>
          </s>
        </p>
      </item>
      <item id="882P4387" name="2.8 PTX FA13 KB 008" totalMark="3" version="18" markingType="0" markingState="0" userMark="0.66666666" markerUserMark="" userAttempted="1" viewingTime="163582" flagged="0" LO="2.8 Exempt assets " unit="Personal Tax" quT="10,11">
        <p um="0.666666666666667" cs="1" ua="1" id="882P4387">
          <s ua="1" um="0.67" id="1">
            <c wei="1" maS="1" ie="1" typ="10" rv="0" mv="0" ua="1" um="1" id="50">
              <i ac="A" sl="0" id="1" />
              <i ca="1" ac="B" sl="1" id="2" />
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="57">
              <i ca="13" id="1">3</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="58">
              <i ca="4" id="1">4</i>
            </c>
            <c typ="4" id="59">
              <i cc="0" id="1" />
            </c>
            <c typ="4" id="63">
              <i cc="0" id="1" />
            </c>
          </s>
        </p>
      </item>
      <item id="882P4392" name="2.9 PTX FA13 KB 002" totalMark="3" version="15" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="1" viewingTime="231800" flagged="0" LO="2.9 Annual exemption, loss relief and capital gains tax payable" unit="Personal Tax" quT="11">
        <p um="0" cs="1" ua="1" id="882P4392">
          <s ua="1" um="0" id="1">
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="58">
              <i ca="1000" id="1">0</i>
            </c>
            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="57">
              <i ca="4528" id="1">3,618</i>
            </c>
            <c typ="4" id="70">
              <i cc="2" id="1" />
            </c>
            <c typ="4" id="74">
              <i cc="2" id="1" />
            </c>
          </s>
        </p>
      </item>
    </section>
  </assessment>
  <isValid>1</isValid>
  <isPrintable>0</isPrintable>
  <qualityReview>0</qualityReview>
  <numberOfGenerations>1</numberOfGenerations>
  <requiresInvigilation>1</requiresInvigilation>
  <advanceContentDownloadTimespan>60</advanceContentDownloadTimespan>
  <automaticVerification>0</automaticVerification>
  <offlineMode>2</offlineMode>
  <requiresValidation>0</requiresValidation>
  <requiresSecureClient>1</requiresSecureClient>
  <examType>0</examType>
  <advanceDownload>0</advanceDownload>
  <gradeBoundaryData>
    <gradeBoundaries passLevelType="1" adjustedGradeBoundaries="0">
      <grade modifier="lt" value="70" description="Fail" />
      <grade modifier="gt" value="70" description="Pass" />
    </gradeBoundaries>
  </gradeBoundaryData>
  <autoViewExam>0</autoViewExam>
  <autoProgressItems>0</autoProgressItems>
  <confirmationText>
    <confirmationText />
  </confirmationText>
  <requiresConfirmationCheck>0</requiresConfirmationCheck>
  <allowCloseWithoutSubmit>0</allowCloseWithoutSubmit>
  <useSecureMarker>0</useSecureMarker>
  <annotationVersion>0</annotationVersion>
  <allowPackagingDelivery>1</allowPackagingDelivery>
  <scoreBoundaryData>
    <scoreBoundaries showScoreBoundaryColumn="1">
      <score modifier="lt" value="36" description="Significantly below" higherBoundarySet="1" />
      <score modifier="lt" value="36" description="Significantly below" higherBoundarySet="0" />
      <score modifier="gt" value="36" description="Below requirement" higherBoundarySet="1" />
      <score modifier="gt" value="60" description="Borderline" higherBoundarySet="1" />
      <score modifier="gt" value="70" description="Met" higherBoundarySet="0" />
      <score modifier="gt" value="70" description="Met" higherBoundarySet="1" />
      <score modifier="gt" value="36" description="Below requirement" higherBoundarySet="0" userCreated="1" />
      <score modifier="gt" value="60" description="Borderline" higherBoundarySet="0" userCreated="1" />
      <score modifier="gt" value="85" description="Exceeded" higherBoundarySet="1" userCreated="1" />
    </scoreBoundaries>
  </scoreBoundaryData>
  <showCandidateReportResultColumn>0</showCandidateReportResultColumn>
  <showCandidateReportPercentageColumn>1</showCandidateReportPercentageColumn>
  <certifiedAccessible>0</certifiedAccessible>
  <markingProgressVisible>1</markingProgressVisible>
  <markingProgressMode>0</markingProgressMode>
  <secureClientOperationMode>1</secureClientOperationMode>
  <examDeliverySystemStyleType>delivery</examDeliverySystemStyleType>
  <markingAutoVoidPeriod>365</markingAutoVoidPeriod>
  <showPageRequiresScrollingAlert>0</showPageRequiresScrollingAlert>
  <project ID="882" />
</assessmentDetails>'
where ID = 1330055

update ExamSessionTable_Shredded
set structureXml = '<assessmentDetails>
  <assessmentID>2030</assessmentID>
  <qualificationID>26</qualificationID>
  <qualificationName>Personal Taxation (AQ2010)</qualificationName>
  <qualificationReference>AATQCF</qualificationReference>
  <assessmentGroupName>PTX FA 2013</assessmentGroupName>
  <assessmentGroupID>1915</assessmentGroupID>
  <assessmentGroupReference>CAPTX</assessmentGroupReference>
  <assessmentName>PTX FA 2013</assessmentName>
  <validFromDate>06 Jan 2014</validFromDate>
  <expiryDate>31 Jul 2014</expiryDate>
  <startTime>00:00:00</startTime>
  <endTime>23:59:59</endTime>
  <duration>120</duration>
  <defaultDuration>120</defaultDuration>
  <scheduledDuration>
    <value>120</value>
    <reason></reason>
  </scheduledDuration>
  <sRBonus>0</sRBonus>
  <sRBonusMaximum>40</sRBonusMaximum>
  <externalReference>CAPTX</externalReference>
  <passLevelValue>70</passLevelValue>
  <passLevelType>1</passLevelType>
  <status>2</status>
  <testFeedbackType>
    <passFail>0</passFail>
    <percentageMark>0</percentageMark>
    <allowSummaryFeedback>0</allowSummaryFeedback>
    <summaryFeedbackType>1</summaryFeedbackType>
    <itemSummary>0</itemSummary>
    <itemReview>0</itemReview>
    <itemReviewCorrectAnswer>0</itemReviewCorrectAnswer>
    <itemFeedback>0</itemFeedback>
    <printableSummary>0</printableSummary>
    <candidateDetails>0</candidateDetails>
    <feedbackByReference>0</feedbackByReference>
    <allowFeedbackDuringExam>0</allowFeedbackDuringExam>
    <allowFeedbackDuringSummary>0</allowFeedbackDuringSummary>
  </testFeedbackType>
  <itemFeedback>0</itemFeedback>
  <allowCalculator>0</allowCalculator>
  <lastInUseDate>06 Dec 2013 14:39:22</lastInUseDate>
  <completedScriptReview>0</completedScriptReview>
  <assessment currentSection="2" totalTime="0" currentTime="0">
    <intro id="0" name="Introduction" currentItem="">
      <item id="882P4434" name="Introduction" totalMark="1" version="14" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
    </intro>
    <outro id="0" name="Finish" currentItem="" />
    <tools id="0" name="Tools" currentItem="">
      <item id="882P4143" name="PTAX FA2013 Pop up 1" toolName="PTAX FA2013 Pop up 1" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="882P4144" name="PTAX FA2013 Pop up 2" toolName="PTAX FA2013 Pop up 2" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
    </tools>
    <section id="1" name="1" passLevelValue="60" passLevelType="1" itemsToMark="0" currentItem="882P4288" fixed="0">
      <item id="882P4152" name="1.1 PTX FA13 KB 002" totalMark="2" version="18" markingType="0" markingState="0" userMark="0.5" markerUserMark="" userAttempted="1" viewingTime="58999" flagged="0" LO="1.1 Legislation and procedures" unit="Personal Tax" quT="10" />
      <item id="882P4163" name="1.2 PTX FA13 KB 002" totalMark="2" version="18" markingType="0" markingState="0" userMark="1" markerUserMark="" userAttempted="1" viewingTime="56020" flagged="0" LO="1.2 Duties and responsibilities of a tax practitioner" unit="Personal Tax" quT="16" />
      <item id="882P4175" name="1.3 PTX FA13 KB 004" totalMark="2" version="16" markingType="0" markingState="0" userMark="0.5" markerUserMark="" userAttempted="1" viewingTime="83835" flagged="0" LO="1.3 Legislative features of income from employment" unit="Personal Tax" quT="10,12" />
      <item id="882P4191" name="1.4 PTX FA13 KB 009" totalMark="3" version="13" markingType="0" markingState="0" userMark="0.66666666" markerUserMark="" userAttempted="1" viewingTime="364058" flagged="0" LO="1.4 Assessment for employment income" unit="Personal Tax" quT="11" />
      <item id="882P4202" name="1.5 PTX FA13 KB 009" totalMark="9" version="15" markingType="0" markingState="0" userMark="0.77777777" markerUserMark="" userAttempted="1" viewingTime="273437" flagged="0" LO="1.5 Income from employment (including benefits in kind)" unit="Personal Tax" quT="10,11" />
      <item id="882P4206" name="1.6 PTX FA13 KB 002" totalMark="8" version="19" markingType="0" markingState="0" userMark="0.375" markerUserMark="" userAttempted="1" viewingTime="228088" flagged="0" LO="1.6 Taxable benefits in kind" unit="Personal Tax" quT="10,11" />
      <item id="882P4220" name="1.7 PTX FA13 KB 005" totalMark="4" version="18" markingType="0" markingState="0" userMark="0.75" markerUserMark="" userAttempted="1" viewingTime="79826" flagged="0" LO="1.7 Tax free benefits" unit="Personal Tax" quT="16" />
      <item id="882P4234" name="1.8 PTX FA13 KB 008" totalMark="4" version="19" markingType="0" markingState="0" userMark="1" markerUserMark="" userAttempted="1" viewingTime="48688" flagged="0" LO="1.8 Deduction from employment income" unit="Personal Tax" quT="10" />
      <item id="882P4241" name="1.9 PTX FA13 KB 004" totalMark="4" version="17" markingType="0" markingState="0" userMark="0.75" markerUserMark="" userAttempted="1" viewingTime="493896" flagged="0" LO="1.9 Income from savings" unit="Personal Tax" quT="11" />
      <item id="882P4256" name="1.10 PTX FA13 KB 008" totalMark="2" version="19" markingType="0" markingState="0" userMark="1" markerUserMark="" userAttempted="1" viewingTime="63165" flagged="0" LO="1.10 Income from dividends" unit="Personal Tax" quT="11" />
      <item id="882P4264" name="1.11 PTX FA13 KB 005" totalMark="2" version="13" markingType="0" markingState="0" userMark="0.5" markerUserMark="" userAttempted="1" viewingTime="120089" flagged="0" LO="1.11 Rules for income treated as exempt" unit="Personal Tax" quT="11,12" />
      <item id="882P4277" name="1.12 PTX FA13 KB 007" totalMark="7" version="24" markingType="1" markingState="1" userMark="1" markerUserMark="7" userAttempted="1" viewingTime="486549" flagged="0" LO="1.12 Schedule of income from all sources" unit="Personal Tax" quT="20" />
      <item id="882P4288" name="1.13 PTX FA13 KB 007" totalMark="9" version="20" markingType="1" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="358598" flagged="0" LO="1.13 Self assessment, payments on account and balancing payment" unit="Personal Tax" quT="11,20" />
      <item id="882P4295" name="1.14 PTX FA13 KB 003" totalMark="7" version="31" markingType="1" markingState="1" userMark="0" markerUserMark="7" userAttempted="1" viewingTime="463569" flagged="0" LO="1.14 Completing and filing tax returns" unit="Personal Tax" quT="11" />
    </section>
    <section id="2" name="2" passLevelValue="60" passLevelType="1" itemsToMark="0" currentItem="882P4361" fixed="0">
      <item id="882P4311" name="2.1 PTX FA13 KB 008" totalMark="1" version="14" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="1" viewingTime="102758" flagged="0" LO="2.1 Legislative features relating to property income" unit="Personal Tax" quT="10" />
      <item id="882P4318" name="2.2 PTX FA13 KB 004" totalMark="7" version="23" markingType="0" markingState="0" userMark="0.85714285" markerUserMark="" userAttempted="1" viewingTime="372264" flagged="0" LO="2.2 Income from furnished and unfurnished property" unit="Personal Tax" quT="11" />
      <item id="882P4333" name="2.3 PTX FA13 KB 009" totalMark="4" version="14" markingType="0" markingState="0" userMark="1" markerUserMark="" userAttempted="1" viewingTime="202222" flagged="0" LO="2.3 Income from rent a room schemes and buy to let investments" unit="Personal Tax" quT="10,11" />
      <item id="882P4338" name="2.4 PTX FA13 KB 003" totalMark="2" version="17" markingType="0" markingState="0" userMark="1" markerUserMark="" userAttempted="1" viewingTime="194813" flagged="0" LO="2.4 Chargeable persons, chargeable assets and chargeable disposals" unit="Personal Tax" quT="10" />
      <item id="882P4353" name="2.5 PTX FA13 KB 007" totalMark="3" version="15" markingType="0" markingState="0" userMark="1" markerUserMark="" userAttempted="1" viewingTime="215608" flagged="0" LO="2.5 Chargeable disposals" unit="Personal Tax" quT="11,12" />
      <item id="882P4361" name="2.6 PTX FA13 KB 004" totalMark="7" version="18" markingType="1" markingState="1" userMark="1" markerUserMark="5" userAttempted="1" viewingTime="737757" flagged="0" LO="2.6 Shares and rights issues" unit="Personal Tax" quT="20" />
      <item id="882P4370" name="2.7 PTX FA13 KB 002" totalMark="5" version="27" markingType="0" markingState="0" userMark="1" markerUserMark="" userAttempted="1" viewingTime="412308" flagged="0" LO="2.7 Part disposal and chattel" unit="Personal Tax" quT="11" />
      <item id="882P4387" name="2.8 PTX FA13 KB 008" totalMark="3" version="18" markingType="0" markingState="0" userMark="0.66666666" markerUserMark="" userAttempted="1" viewingTime="163582" flagged="0" LO="2.8 Exempt assets " unit="Personal Tax" quT="10,11" />
      <item id="882P4392" name="2.9 PTX FA13 KB 002" totalMark="3" version="15" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="1" viewingTime="231800" flagged="0" LO="2.9 Annual exemption, loss relief and capital gains tax payable" unit="Personal Tax" quT="11" />
    </section>
  </assessment>
  <isValid>1</isValid>
  <isPrintable>0</isPrintable>
  <qualityReview>0</qualityReview>
  <numberOfGenerations>1</numberOfGenerations>
  <requiresInvigilation>1</requiresInvigilation>
  <advanceContentDownloadTimespan>60</advanceContentDownloadTimespan>
  <automaticVerification>0</automaticVerification>
  <offlineMode>2</offlineMode>
  <requiresValidation>0</requiresValidation>
  <requiresSecureClient>1</requiresSecureClient>
  <examType>0</examType>
  <advanceDownload>0</advanceDownload>
  <gradeBoundaryData>
    <gradeBoundaries passLevelType="1" adjustedGradeBoundaries="0">
      <grade modifier="lt" value="70" description="Fail" />
      <grade modifier="gt" value="70" description="Pass" />
    </gradeBoundaries>
  </gradeBoundaryData>
  <autoViewExam>0</autoViewExam>
  <autoProgressItems>0</autoProgressItems>
  <confirmationText>
    <confirmationText></confirmationText>
  </confirmationText>
  <requiresConfirmationCheck>0</requiresConfirmationCheck>
  <allowCloseWithoutSubmit>0</allowCloseWithoutSubmit>
  <useSecureMarker>0</useSecureMarker>
  <annotationVersion>0</annotationVersion>
  <allowPackagingDelivery>1</allowPackagingDelivery>
  <scoreBoundaryData>
    <scoreBoundaries showScoreBoundaryColumn="1">
      <score modifier="lt" value="36" description="Significantly below" higherBoundarySet="1" />
      <score modifier="lt" value="36" description="Significantly below" higherBoundarySet="0" />
      <score modifier="gt" value="36" description="Below requirement" higherBoundarySet="1" />
      <score modifier="gt" value="60" description="Borderline" higherBoundarySet="1" />
      <score modifier="gt" value="70" description="Met" higherBoundarySet="0" />
      <score modifier="gt" value="70" description="Met" higherBoundarySet="1" />
      <score modifier="gt" value="36" description="Below requirement" higherBoundarySet="0" userCreated="1" />
      <score modifier="gt" value="60" description="Borderline" higherBoundarySet="0" userCreated="1" />
      <score modifier="gt" value="85" description="Exceeded" higherBoundarySet="1" userCreated="1" />
    </scoreBoundaries>
  </scoreBoundaryData>
  <showCandidateReportResultColumn>0</showCandidateReportResultColumn>
  <showCandidateReportPercentageColumn>1</showCandidateReportPercentageColumn>
  <certifiedAccessible>0</certifiedAccessible>
  <markingProgressVisible>1</markingProgressVisible>
  <markingProgressMode>0</markingProgressMode>
  <secureClientOperationMode>1</secureClientOperationMode>
  <examDeliverySystemStyleType>delivery</examDeliverySystemStyleType>
  <markingAutoVoidPeriod>365</markingAutoVoidPeriod>
  <showPageRequiresScrollingAlert>0</showPageRequiresScrollingAlert>
  <project ID="882" />
</assessmentDetails>', resultData = '<exam passMark="70" passType="1" originalPassMark="70" originalPassType="1" totalMark="100" userMark="70.900" userPercentage="70.900" passValue="1" originalPassValue="1" totalTimeSpent="5811929" closeBoundaryType="0" closeBoundaryValue="0" grade="Pass" originalGrade="Pass">
  <gradeBoundaryData>
    <gradeBoundaries passLevelType="1" adjustedGradeBoundaries="0">
      <grade modifier="lt" value="70" description="Fail" />
      <grade modifier="gt" value="70" description="Pass" />
    </gradeBoundaries>
  </gradeBoundaryData>
  <section id="1" passMark="60" passType="1" name="1" totalMark="65" userMark="43.900" userPercentage="67.538" passValue="1" totalTimeSpent="3178817" itemsToMark="0">
    <item id="882P4152" name="1.1 PTX FA13 KB 002" totalMark="2" userMark="0.5" actualUserMark="1.0" markingType="0" markerUserMark="" viewingTime="58999" userAttempted="1" version="18" />
    <item id="882P4163" name="1.2 PTX FA13 KB 002" totalMark="2" userMark="1" actualUserMark="2" markingType="0" markerUserMark="" viewingTime="56020" userAttempted="1" version="18" />
    <item id="882P4175" name="1.3 PTX FA13 KB 004" totalMark="2" userMark="0.5" actualUserMark="1.0" markingType="0" markerUserMark="" viewingTime="83835" userAttempted="1" version="16" />
    <item id="882P4191" name="1.4 PTX FA13 KB 009" totalMark="3" userMark="0.66666666" actualUserMark="1.99999998" markingType="0" markerUserMark="" viewingTime="364058" userAttempted="1" version="13" />
    <item id="882P4202" name="1.5 PTX FA13 KB 009" totalMark="9" userMark="0.77777777" actualUserMark="6.99999993" markingType="0" markerUserMark="" viewingTime="273437" userAttempted="1" version="15" />
    <item id="882P4206" name="1.6 PTX FA13 KB 002" totalMark="8" userMark="0.375" actualUserMark="3.000" markingType="0" markerUserMark="" viewingTime="228088" userAttempted="1" version="19" />
    <item id="882P4220" name="1.7 PTX FA13 KB 005" totalMark="4" userMark="0.75" actualUserMark="3.00" markingType="0" markerUserMark="" viewingTime="79826" userAttempted="1" version="18" />
    <item id="882P4234" name="1.8 PTX FA13 KB 008" totalMark="4" userMark="1" actualUserMark="4" markingType="0" markerUserMark="" viewingTime="48688" userAttempted="1" version="19" />
    <item id="882P4241" name="1.9 PTX FA13 KB 004" totalMark="4" userMark="0.75" actualUserMark="3.00" markingType="0" markerUserMark="" viewingTime="493896" userAttempted="1" version="17" />
    <item id="882P4256" name="1.10 PTX FA13 KB 008" totalMark="2" userMark="1" actualUserMark="2" markingType="0" markerUserMark="" viewingTime="63165" userAttempted="1" version="19" />
    <item id="882P4264" name="1.11 PTX FA13 KB 005" totalMark="2" userMark="0.5" actualUserMark="1.0" markingType="0" markerUserMark="" viewingTime="120089" userAttempted="1" version="13" />
    <item id="882P4277" name="1.12 PTX FA13 KB 007" totalMark="7" userMark="0" actualUserMark="7" markingType="1" markerUserMark="7" viewingTime="486549" userAttempted="1" version="24" />
    <item id="882P4288" name="1.13 PTX FA13 KB 007" totalMark="9" userMark="0" actualUserMark="0" markingType="1" markerUserMark="" viewingTime="358598" userAttempted="0" version="20" />
    <item id="882P4295" name="1.14 PTX FA13 KB 003" totalMark="7" userMark="0" actualUserMark="7" markingType="1" markerUserMark="7" viewingTime="463569" userAttempted="1" version="31" />
  </section>
  <section id="2" passMark="60" passType="1" name="2" totalMark="35" userMark="27.000" userPercentage="77.143" passValue="1" totalTimeSpent="2633112" itemsToMark="0">
    <item id="882P4311" name="2.1 PTX FA13 KB 008" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="102758" userAttempted="1" version="14" />
    <item id="882P4318" name="2.2 PTX FA13 KB 004" totalMark="7" userMark="0.85714285" actualUserMark="5.99999995" markingType="0" markerUserMark="" viewingTime="372264" userAttempted="1" version="23" />
    <item id="882P4333" name="2.3 PTX FA13 KB 009" totalMark="4" userMark="1" actualUserMark="4" markingType="0" markerUserMark="" viewingTime="202222" userAttempted="1" version="14" />
    <item id="882P4338" name="2.4 PTX FA13 KB 003" totalMark="2" userMark="1" actualUserMark="2" markingType="0" markerUserMark="" viewingTime="194813" userAttempted="1" version="17" />
    <item id="882P4353" name="2.5 PTX FA13 KB 007" totalMark="3" userMark="1" actualUserMark="3" markingType="0" markerUserMark="" viewingTime="215608" userAttempted="1" version="15" />
    <item id="882P4361" name="2.6 PTX FA13 KB 004" totalMark="7" userMark="0" actualUserMark="5" markingType="1" markerUserMark="5" viewingTime="737757" userAttempted="1" version="18" />
    <item id="882P4370" name="2.7 PTX FA13 KB 002" totalMark="5" userMark="1" actualUserMark="5" markingType="0" markerUserMark="" viewingTime="412308" userAttempted="1" version="27" />
    <item id="882P4387" name="2.8 PTX FA13 KB 008" totalMark="3" userMark="0.66666666" actualUserMark="1.99999998" markingType="0" markerUserMark="" viewingTime="163582" userAttempted="1" version="18" />
    <item id="882P4392" name="2.9 PTX FA13 KB 002" totalMark="3" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="231800" userAttempted="1" version="15" />
  </section>
</exam>'
where examSessionId = 1330055


/*

select * from ExamSessionItemResponseTable where ExamSessionID = 1330055 and ItemID = '882P4288'


update ExamSessionItemResponseTable 
set MarkerResponseData = '<entries>
  <entry>
    <comment>Computer marked where applicable</comment>
    <userId>-1</userId>
    <userForename>Computer</userForename>
    <userSurname>Marked</userSurname>
    <assignedMark>0</assignedMark>
    <version>0</version>
    <dateCreated>05/06/2014</dateCreated>
  </entry>
</entries>'
where ExamSessionID = 1330055 and ItemID = '882P4288'