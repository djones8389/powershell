SELECT EST.ID, EST.KeyCode, EST.examState, UT.Forename, UT.Surname, CT.CentreName, SCET.examName
	, SCET.ScheduledStartDateTime, SCET.ScheduledEndDateTime, DATEDIFF(DAY, SCET.ScheduledStartDateTime, SCET.ScheduledEndDateTime) as 'Day Difference'
  FROM ExamSessionTable as EST

  Inner Join ScheduledExamsTable as SCET
  on SCET.ID = EST.ScheduledExamID
  
  Inner Join UserTable as UT
  on UT.ID = EST.UserID

  Inner Join CentreTable as CT
  on CT.ID = SCET.CentreID

 where ExamState = 99;	
	
	
	
select * from StateManagementException where ExamSessionID in (1416962,1416960,1416959)
select * from ExamStateChangeAuditTable where ExamSessionID in (1416962,1416960,1416959)

/*
update ExamSessionTable
set examState = 1
where ID in (1416962,1416960,1416959)
*/

select ID, examState from ExamSessionTable where ID in (1416962,1416960,1416959)