use AAT_SecureMarker

select top 10 * from MarkSchemes
	where ExternalItemID in ('8924P990','924P1045','924P1143','924P1043')
	
	
	
use AAT_CPProjectAdmin
	
	select ID, SupportingFiles
		from PageTable
			where ID in ('8924P990','924P1045','924P1143','924P1043')
			
	select ProjectStructureXml
		from ProjectListTable
			where ID = 924	


--select *
--	from MarkSchemes

SELECT markSchemeID, latestmarkschemeidattimeofimport
	, externalitemid, version, externalitemname

FROM CandidateExamVersions as CEV

INNER JOIN CandidateResponses as CR
ON CR.CandidateExamVersionID = CEV.ID

INNER JOIN UniqueResponses as UR
ON UR.ID = CR.UniqueResponseID 

INNER JOIN UniqueGroupResponseLinks as UGRL
on UGRL.UniqueResponseId = UR.id

INNER JOIN UniqueGroupResponses as UGR
on UGR.ID = UGRL.UniqueGroupResponseID

INNER JOIN Items as I
on I.ID = UR.itemId

where ExternalItemID IN   ('8924P990','924P1045','924P1143','924P1043')
      order by ugr.ID asc
