SELECT WEST.ID, WEST.KeyCode, WUT.Forename, WUT.Surname, WUT.CandidateRef, WCT.CentreName, WSCET.examName, WEST.completionDate
	, WESTS.ExternalReference
FROM WAREHOUSE_ExamSessionTable as WEST

  Inner Join WAREHOUSE_ScheduledExamsTable as WSCET
  on WSCET.ID = WEST.WAREHOUSEScheduledExamID
  
  Inner Join WAREHOUSE_UserTable as WUT
  on WUT.ID = WEST.WAREHOUSEUserID

  Inner Join WAREHOUSE_CentreTable as WCT
  on WCT.ID = WSCET.WAREHOUSECentreID

  Inner Join WAREHOUSE_ExamSessionTable_Shreded as WESTS
  on WESTS.examSessionId = WEST.ID

where wut.candidateref = '20040458'


select * from WAREHOUSE_ExamSessionTable where ID = 1345546

select * from WAREHOUSE_ExamSessionItemResponseTable where WAREHOUSEExamSessionID = 1345546

/*

update WAREHOUSE_ExamSessionTable 
set StructureXML = '<assessmentDetails>
  <assessmentID>2078</assessmentID>
  <qualificationID>134</qualificationID>
  <qualificationName>Professional ethics (SU NZ AQ2013)</qualificationName>
  <qualificationReference>NZAQSU</qualificationReference>
  <assessmentGroupName>PETH (SU NZ AQ2013)</assessmentGroupName>
  <assessmentGroupID>1961</assessmentGroupID>
  <assessmentGroupReference>CAPETHNZ2</assessmentGroupReference>
  <assessmentName>PETH (SU NZ AQ2013)</assessmentName>
  <validFromDate>01 Mar 2014</validFromDate>
  <expiryDate>31 Dec 2016</expiryDate>
  <startTime>00:00:00</startTime>
  <endTime>23:59:59</endTime>
  <duration>150</duration>
  <defaultDuration>150</defaultDuration>
  <scheduledDuration>
    <value>150</value>
    <reason></reason>
  </scheduledDuration>
  <sRBonus>0</sRBonus>
  <sRBonusMaximum>50</sRBonusMaximum>
  <externalReference>CAPETHNZ2</externalReference>
  <passLevelValue>70</passLevelValue>
  <passLevelType>1</passLevelType>
  <status>2</status>
  <testFeedbackType>
    <passFail>0</passFail>
    <percentageMark>0</percentageMark>
    <allowSummaryFeedback>0</allowSummaryFeedback>
    <summaryFeedbackType>1</summaryFeedbackType>
    <itemSummary>0</itemSummary>
    <itemReview>0</itemReview>
    <itemReviewCorrectAnswer>0</itemReviewCorrectAnswer>
    <itemFeedback>0</itemFeedback>
    <printableSummary>0</printableSummary>
    <candidateDetails>0</candidateDetails>
    <feedbackByReference>0</feedbackByReference>
    <allowFeedbackDuringExam>0</allowFeedbackDuringExam>
    <allowFeedbackDuringSummary>0</allowFeedbackDuringSummary>
  </testFeedbackType>
  <itemFeedback>0</itemFeedback>
  <allowCalculator>0</allowCalculator>
  <lastInUseDate>12 Mar 2014 14:29:45</lastInUseDate>
  <completedScriptReview>0</completedScriptReview>
  <assessment currentSection="1" totalTime="0" currentTime="0">
    <intro id="0" name="Introduction" currentItem="">
      <item id="919P971" name="PETHNZ introduction" totalMark="1" version="10" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
    </intro>
    <outro id="0" name="Finish" currentItem="" />
    <tools id="0" name="Tools" currentItem="" />
    <section id="1" name="Section 1" passLevelValue="70" passLevelType="1" itemsToMark="0" currentItem="919P974" fixed="0">
      <item id="919P980" name="01 PETHNZ GF 002" totalMark="5" version="30" markingType="1" markingState="1" userMark="0" markerUserMark="4" userAttempted="1" viewingTime="342858" flagged="0" LO="01. Regulatory environment: Regulatory environment, professional bodies, NZICA Act" unit="Professional Ethics New Zealand" quT="11" />
      <item id="919P979" name="02 PETHNZ GF 002" totalMark="10" version="33" markingType="1" markingState="1" userMark="0" markerUserMark="8" userAttempted="1" viewingTime="1407449" flagged="0" LO="01. Regulatory environment: NZICA structure, membership, discipline" unit="Professional Ethics New Zealand" quT="11" />
      <item id="919P978" name="03 PETHNZ GF 002" totalMark="5" version="28" markingType="1" markingState="1" userMark="0" markerUserMark="5" userAttempted="1" viewingTime="567326" flagged="0" LO="03. Working ethically: Codes of practice, fundamental principles, handling client monies" unit="Professional Ethics New Zealand" quT="11" />
      <item id="919P977" name="04 PETHNZ GF 002" totalMark="10" version="30" markingType="1" markingState="1" userMark="0" markerUserMark="7" userAttempted="1" viewingTime="1498881" flagged="0" LO="04. Ethical decision making: Use an ethical decision making process" unit="Professional Ethics New Zealand" quT="11" />
      <item id="919P976" name="05 PETHNZ GF 002" totalMark="10" version="40" markingType="1" markingState="1" userMark="0" markerUserMark="9" userAttempted="1" viewingTime="977855" flagged="0" LO="05. Case study � matter 1: Integrity, threats, organisational values, codes of practice" unit="Professional Ethics New Zealand" quT="11" />
      <item id="919P975" name="06 PETHNZ GF 002" totalMark="10" version="28" markingType="1" markingState="1" userMark="0" markerUserMark="7" userAttempted="1" viewingTime="757600" flagged="0" LO="06. Case study � matter 2: Objectivity, conflicts of interest, receiving gifts" unit="Professional Ethics New Zealand" quT="11" />
      <item id="919P974" name="07 PETHNZ GF 002" totalMark="10" version="30" markingType="1" markingState="1" userMark="0" markerUserMark="10" userAttempted="1" viewingTime="960274" flagged="0" LO="07. Case study � matter 3: Professional competence and due care, working beyond expertise" unit="Professional Ethics New Zealand" quT="11" />
      <item id="919P973" name="08 PETHNZ GF 002" totalMark="10" version="30" markingType="1" markingState="1" userMark="0" markerUserMark="8" userAttempted="1" viewingTime="1466045" flagged="0" LO="08. Case study � matter 4: Confidentiality, reporting unethical or illegal acts" unit="Professional Ethics New Zealand" quT="11" />
      <item id="919P972" name="09 PETHNZ GF 002" totalMark="10" version="28" markingType="1" markingState="1" userMark="0" markerUserMark="10" userAttempted="1" viewingTime="1022004" flagged="0" LO="09. Case study � matter 5: Professional behaviour, money laundering, new clients" unit="Professional Ethics" quT="11" />
    </section>
  </assessment>
  <isValid>1</isValid>
  <isPrintable>0</isPrintable>
  <qualityReview>0</qualityReview>
  <numberOfGenerations>1</numberOfGenerations>
  <requiresInvigilation>1</requiresInvigilation>
  <advanceContentDownloadTimespan>60</advanceContentDownloadTimespan>
  <automaticVerification>0</automaticVerification>
  <offlineMode>2</offlineMode>
  <requiresValidation>0</requiresValidation>
  <requiresSecureClient>1</requiresSecureClient>
  <examType>0</examType>
  <advanceDownload>0</advanceDownload>
  <gradeBoundaryData>
    <gradeBoundaries passLevelType="1" adjustedGradeBoundaries="0">
      <grade modifier="lt" value="70" description="Fail" />
      <grade modifier="gt" value="70" description="Pass" />
    </gradeBoundaries>
  </gradeBoundaryData>
  <autoViewExam>0</autoViewExam>
  <autoProgressItems>0</autoProgressItems>
  <confirmationText>
    <confirmationText></confirmationText>
  </confirmationText>
  <requiresConfirmationCheck>0</requiresConfirmationCheck>
  <allowCloseWithoutSubmit>0</allowCloseWithoutSubmit>
  <useSecureMarker>0</useSecureMarker>
  <annotationVersion>0</annotationVersion>
  <allowPackagingDelivery>1</allowPackagingDelivery>
  <scoreBoundaryData>
    <scoreBoundaries showScoreBoundaryColumn="1">
      <score modifier="lt" value="36" description="Significantly below" higherBoundarySet="1" />
      <score modifier="lt" value="36" description="Significantly below" higherBoundarySet="0" />
      <score modifier="gt" value="36" description="Below requirement" higherBoundarySet="1" />
      <score modifier="gt" value="60" description="Borderline" higherBoundarySet="1" />
      <score modifier="gt" value="70" description="Met" higherBoundarySet="0" />
      <score modifier="gt" value="70" description="Met" higherBoundarySet="1" />
      <score modifier="gt" value="36" description="Below requirement" higherBoundarySet="0" userCreated="1" />
      <score modifier="gt" value="60" description="Borderline" higherBoundarySet="0" userCreated="1" />
      <score modifier="gt" value="85" description="Exceeded" higherBoundarySet="1" userCreated="1" />
    </scoreBoundaries>
  </scoreBoundaryData>
  <showCandidateReportResultColumn>0</showCandidateReportResultColumn>
  <showCandidateReportPercentageColumn>1</showCandidateReportPercentageColumn>
  <certifiedAccessible>0</certifiedAccessible>
  <markingProgressVisible>1</markingProgressVisible>
  <markingProgressMode>0</markingProgressMode>
  <secureClientOperationMode>1</secureClientOperationMode>
  <examDeliverySystemStyleType>delivery</examDeliverySystemStyleType>
  <markingAutoVoidPeriod>365</markingAutoVoidPeriod>
  <showPageRequiresScrollingAlert>0</showPageRequiresScrollingAlert>
  <project ID="919" />
</assessmentDetails>',  resultDataFull = '<assessmentDetails>
  <assessmentID>2078</assessmentID>
  <qualificationID>134</qualificationID>
  <qualificationName>Professional ethics (SU NZ AQ2013)</qualificationName>
  <qualificationReference>NZAQSU</qualificationReference>
  <assessmentGroupName>PETH (SU NZ AQ2013)</assessmentGroupName>
  <assessmentGroupID>1961</assessmentGroupID>
  <assessmentGroupReference>CAPETHNZ2</assessmentGroupReference>
  <assessmentName>PETH (SU NZ AQ2013)</assessmentName>
  <validFromDate>01 Mar 2014</validFromDate>
  <expiryDate>31 Dec 2016</expiryDate>
  <startTime>00:00:00</startTime>
  <endTime>23:59:59</endTime>
  <duration>150</duration>
  <defaultDuration>150</defaultDuration>
  <scheduledDuration>
    <value>150</value>
    <reason />
  </scheduledDuration>
  <sRBonus>0</sRBonus>
  <sRBonusMaximum>50</sRBonusMaximum>
  <externalReference>CAPETHNZ2</externalReference>
  <passLevelValue>70</passLevelValue>
  <passLevelType>1</passLevelType>
  <status>2</status>
  <testFeedbackType>
    <passFail>0</passFail>
    <percentageMark>0</percentageMark>
    <allowSummaryFeedback>0</allowSummaryFeedback>
    <summaryFeedbackType>1</summaryFeedbackType>
    <itemSummary>0</itemSummary>
    <itemReview>0</itemReview>
    <itemReviewCorrectAnswer>0</itemReviewCorrectAnswer>
    <itemFeedback>0</itemFeedback>
    <printableSummary>0</printableSummary>
    <candidateDetails>0</candidateDetails>
    <feedbackByReference>0</feedbackByReference>
    <allowFeedbackDuringExam>0</allowFeedbackDuringExam>
    <allowFeedbackDuringSummary>0</allowFeedbackDuringSummary>
  </testFeedbackType>
  <itemFeedback>0</itemFeedback>
  <allowCalculator>0</allowCalculator>
  <lastInUseDate>Jun 25 2014  5:04PM</lastInUseDate>
  <completedScriptReview>0</completedScriptReview>
  <assessment currentSection="1" totalTime="0" currentTime="0" originalPassMark="70" originalPassType="1" passMark="70" passType="1" totalMark="80" userMark="68" userPercentage="85.00" passValue="1" originalGrade="Pass">
    <intro id="0" name="Introduction" currentItem="">
      <item id="919P971" name="PETHNZ introduction" totalMark="1" version="10" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
    </intro>
    <outro id="0" name="Finish" currentItem="" />
    <tools id="0" name="Tools" currentItem="" />
    <section id="1" name="Section 1" passLevelValue="70" passLevelType="1" itemsToMark="0" currentItem="919P974" fixed="0" totalMark="80" userMark="68" userPercentage="85.00" passValue="1">
      <item id="919P980" name="01 PETHNZ GF 002" totalMark="5" version="30" markingType="1" markingState="1" userMark="0" markerUserMark="4" userAttempted="1" viewingTime="342858" flagged="0" LO="01. Regulatory environment: Regulatory environment, professional bodies, NZICA Act" unit="Professional Ethics New Zealand" quT="11">
        <p um="0" cs="1" ua="1" id="919P980">
          <s ua="1" um="0" id="1">
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="2">
              <i ca="" id="1">IFAC - International Federation for Accountants</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="9">
              <i ca="" id="1">Yes but under a certain limit of income</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="12">
              <i ca="" id="1">Auditing</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="15">
              <i ca="" id="1">To ensure the quliaty of the work provided by the member are at the satifactory level#!#To ensure the good reputation of the profession</i>
            </c>
          </s>
        </p>
      </item>
      <item id="919P979" name="02 PETHNZ GF 002" totalMark="10" version="33" markingType="1" markingState="1" userMark="0" markerUserMark="8" userAttempted="1" viewingTime="1407449" flagged="0" LO="01. Regulatory environment: NZICA structure, membership, discipline" unit="Professional Ethics New Zealand" quT="11">
        <p um="0" cs="1" ua="1" id="919P979">
          <s ua="1" um="0" id="1">
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="2">
              <i ca="" id="1">Nomination and Governance Committee and Council</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="9">
              <i ca="" id="1">Taxation#!#Financial Reporting#!#Auditing and Assurance#!#Insolvency#!#</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="17">
              <i ca="" id="1">Must be a member of the College of Chartered Accountants for at least 2 years#!#The member is a fit and proper person to offering accounting services to the public#!#Must have 2 years public service experience#!#Must have references</i>
            </c>
          </s>
          <s ua="1" um="0" id="2">
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="2">
              <i ca="" id="1">Proved of the CPD for the last two years#!#Certificate of membership of the NZICA</i>
            </c>
          </s>
        </p>
      </item>
      <item id="919P978" name="03 PETHNZ GF 002" totalMark="5" version="28" markingType="1" markingState="1" userMark="0" markerUserMark="5" userAttempted="1" viewingTime="567326" flagged="0" LO="03. Working ethically: Codes of practice, fundamental principles, handling client monies" unit="Professional Ethics New Zealand" quT="11">
        <p um="0" cs="1" ua="1" id="919P978">
          <s ua="1" um="0" id="1">
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="2">
              <i ca="" id="1">Professional behaviour</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="12">
              <i ca="" id="1">Salman should make appropriate enquiries about the source of the money and make sure there was no illegal activities involved. COE 270.3</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="22">
              <i ca="" id="1">Salman should#!#1. the money should be deposit into a separate bank account from his or the firm''s bank account#!#2. Use the money only for the intended purposes. In this case hold the money and return it when Navami back from overseas#!#3. At all time be ready to account for the money and any income. In this case, interest fromthe deposit#!#COE 270.2#!#</i>
            </c>
          </s>
        </p>
      </item>
      <item id="919P977" name="04 PETHNZ GF 002" totalMark="10" version="30" markingType="1" markingState="1" userMark="0" markerUserMark="7" userAttempted="1" viewingTime="1498881" flagged="0" LO="04. Ethical decision making: Use an ethical decision making process" unit="Professional Ethics New Zealand" quT="11">
        <p um="0" cs="1" ua="1" id="919P977">
          <s ua="1" um="0" id="1">
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="2">
              <i ca="" id="1">There is a self interest threat of objectivity as the owner willing to offer a very generous cash bonus. COE 100.12#!#As the information acquired is confidential, the accounting firm can not disclose it to the outside parties COE 140</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="9">
              <i ca="" id="1">1. Talk to the owner again and explain to his the situation and ask him to report this to the Inland Revenue#!#2. Whistle blow - If the owner refuse to disclose it to the Inland Revenue, then it becomes to illegal activity (money lanudering), the acccount has the obligation to inform the Inland Revenue about this.#!#3. The accounting firm need to inform the client that they will disconnect all the services to them#!#4. Do what the client asked and get the bonus</i>
            </c>
          </s>
          <s ua="1" um="0" id="2">
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="5">
              <i ca="" id="1">1. It is ethically acceptable as the accounting firm asking the client to disclose the error to the Inland Revenue#!#2. It is ethically acceptable as it is becomes to illegal activity (money laundering) then the accountant is not bounded by the fundamental principle (confidentiality) anymore and she has the obligation to disclose it.#!#3. It is ethically acceptable as the accountant refuse to involve in illegal activity#!#4. It is not ethically acceptable for accountant dealing with false information. If the accountant accept the bonus, there is a potential risk of intimidation threat</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="7">
              <i ca="" id="1">The accountant should take the option 1, 2 and 3 and they are all ethically acceptable.#!#It prevent the accountant to dealing with false and misleading information as well as fulfill the obligation of complete all assignment by following laws and regulations.</i>
            </c>
          </s>
        </p>
      </item>
      <item id="919P976" name="05 PETHNZ GF 002" totalMark="10" version="40" markingType="1" markingState="1" userMark="0" markerUserMark="9" userAttempted="1" viewingTime="977855" flagged="0" LO="05. Case study � matter 1: Integrity, threats, organisational values, codes of practice" unit="Professional Ethics New Zealand" quT="11">
        <p um="0" cs="1" ua="1" id="919P976">
          <s ua="1" um="0" id="2">
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="3">
              <i ca="" id="1">Integrity COE 110.2#!#Jill should not dealing with any false and misleading information</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="6">
              <i ca="" id="1">This is self interest threat COE 100.12</i>
              <i ca="" id="2">1. Jill should inform this to a higer level management such as board of directors#!#2. Whistle blow - Jill should disclose this to the relevant party#!#3. Jill should stop providing services to the client</i>
            </c>
          </s>
          <s ua="1" um="0" id="3">
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="5">
              <i ca="" id="2">To keep good reputation of the profession#!#To stop anyone bring the Institu into disrepute</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="15">
              <i ca="" id="1">1. Professional behaviour - all actions and advice will conform from revelant laws#!#2. Confidetiality - handling clients information professionally#!#3. Objectivity - avoid conflicts of interest#!#4. Integrity - all services will be provided honestly</i>
            </c>
          </s>
        </p>
      </item>
      <item id="919P975" name="06 PETHNZ GF 002" totalMark="10" version="28" markingType="1" markingState="1" userMark="0" markerUserMark="7" userAttempted="1" viewingTime="757600" flagged="0" LO="06. Case study � matter 2: Objectivity, conflicts of interest, receiving gifts" unit="Professional Ethics New Zealand" quT="11">
        <p um="0" cs="1" ua="1" id="919P975">
          <s ua="1" um="0" id="1">
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="17">
              <i ca="" id="1">1. Objectivity - as Tamati knows both clients quite well, and advise he provides to one of the clients may disadvantage the counter party. Tamati should not involve in the situation that contains conflict of interest#!#2. Confidentiality - Tamati make acquire some information during the services to Fred and Mei. There is a possibility that he might disclose one client''s business information to another</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="22">
              <i ca="" id="1">Inform the client that any information that he acquired in the past re another client is confidential that he is willing to disclose it to the client</i>
            </c>
          </s>
          <s ua="1" um="0" id="2">
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="1">
              <i ca="" id="1">Professional and tecchnical competence and due care - as the club not happy with the quality of the work#!#Objectivity - there is familiarity threat to Tamati as he is a member of the football club, he might let things go for his close club mates</i>
            </c>
          </s>
        </p>
      </item>
      <item id="919P974" name="07 PETHNZ GF 002" totalMark="10" version="30" markingType="1" markingState="1" userMark="0" markerUserMark="10" userAttempted="1" viewingTime="960274" flagged="0" LO="07. Case study � matter 3: Professional competence and due care, working beyond expertise" unit="Professional Ethics New Zealand" quT="11">
        <p um="0" cs="1" ua="1" id="919P974">
          <s ua="1" um="0" id="1">
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="2">
              <i ca="" id="1">Marama should #!#1. refuse to accept the job as she does not have the expertise to accomplished the job#!#2. taking related training course and then do the job COE 330.3#!#3. Accept the job only with assists of another experienced colleague COE 330.3</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="12">
              <i ca="" id="1">1. Taking related course to gain expetise #!#2. Receiving help from an experienced colleague#!#3. Ensure there is sufficient time for her to complete the job</i>
            </c>
          </s>
          <s ua="1" um="0" id="2">
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="1">
              <i ca="" id="1">Attending to professional courses</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="4">
              <i ca="" id="1">In order to provide the work at the quality required.#!#The accounting professional need to work within the confines of their professional experience in order to maintain the trust of the client, the trust of employer and keep good reputation of the profession. Not bring the Institute into disrepute (if the accounting professional is a member of the Instute)</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="6">
              <i ca="" id="1">Taxation#!#Auditing and Assurance#!#Insolvency#!#Financial Reporting</i>
            </c>
          </s>
        </p>
      </item>
      <item id="919P973" name="08 PETHNZ GF 002" totalMark="10" version="30" markingType="1" markingState="1" userMark="0" markerUserMark="8" userAttempted="1" viewingTime="1466045" flagged="0" LO="08. Case study � matter 4: Confidentiality, reporting unethical or illegal acts" unit="Professional Ethics New Zealand" quT="11">
        <p um="0" cs="1" ua="1" id="919P973">
          <s ua="1" um="0" id="1">
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="2">
              <i ca="" id="1">Confidentiality - the information acquired from theprevious work is also confidential even Jonathon left the work.</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="12">
              <i ca="" id="1">Crime Act 1961</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="17">
              <i ca="" id="1">Avoid paying income tax is money laundering, Jonathon should report it to AML/TF officer or Serious Fraud Office</i>
            </c>
          </s>
          <s ua="1" um="0" id="2">
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="1">
              <i ca="" id="1">NZICA#!#Serious Fraud Office</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="3">
              <i ca="" id="1">Jonathon can not share the information with his clients#!#The information is confidential and he can not disclose it unless there is a permission from Shady Importer. #!#Jonathon can disclose the information if there is a legal requirement or professional duty but only disclose the information to the relevant authority.</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="7">
              <i ca="" id="1">The information is required as an evdence in the course of legal proceeding COE 140.7#!#To respond to an enquiry or investigation by a professional body or regulatory body COE 140.7</i>
            </c>
          </s>
        </p>
      </item>
      <item id="919P972" name="09 PETHNZ GF 002" totalMark="10" version="28" markingType="1" markingState="1" userMark="0" markerUserMark="10" userAttempted="1" viewingTime="1022004" flagged="0" LO="09. Case study � matter 5: Professional behaviour, money laundering, new clients" unit="Professional Ethics" quT="11">
        <p um="0" cs="1" ua="1" id="919P972">
          <s ua="1" um="0" id="1">
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="2">
              <i ca="" id="1">Request the permission from the prospective new client to communicate with the existing accountant COE 210.12.1#!#On the receipt of the client''s permisson, request in writing to the existing accountant that if there is any reason that should not accpet the appointment COE 210.12.1</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="12">
              <i ca="" id="1">To find out the structure of the client''s company#!#To find out the background of the client''s management#!#To find out the client is using information from a proper source#!#This is a saftguard to Kenneth as he will find out if the client has been involved in any illegal activities such as money laundering</i>
            </c>
          </s>
          <s ua="1" um="0" id="2">
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="3">
              <i ca="" id="1">Professional behaviour - should not make exaggerated claims for the services and their work COE 150.2</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="5">
              <i ca="" id="1">It is not acceptable.#!#Kenneth should not make exaggerated claims for the service offered COE 250.2#!#She can try her best to make sure her clients not paying more tax but not guarantee paying less tax</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="9">
              <i ca="" id="1">The requirement of the tasks that Kenneth needs to provided to the client#!#The limitation of the services provided by Kenneth (disclaimer clause)#!#The fee of the professional services covered</i>
            </c>
          </s>
        </p>
      </item>
    </section>
  </assessment>
  <isValid>1</isValid>
  <isPrintable>0</isPrintable>
  <qualityReview>0</qualityReview>
  <numberOfGenerations>1</numberOfGenerations>
  <requiresInvigilation>1</requiresInvigilation>
  <advanceContentDownloadTimespan>60</advanceContentDownloadTimespan>
  <automaticVerification>0</automaticVerification>
  <offlineMode>2</offlineMode>
  <requiresValidation>0</requiresValidation>
  <requiresSecureClient>1</requiresSecureClient>
  <examType>0</examType>
  <advanceDownload>0</advanceDownload>
  <gradeBoundaryData>
    <gradeBoundaries passLevelType="1" adjustedGradeBoundaries="0">
      <grade modifier="lt" value="70" description="Fail" />
      <grade modifier="gt" value="70" description="Pass" />
    </gradeBoundaries>
  </gradeBoundaryData>
  <autoViewExam>0</autoViewExam>
  <autoProgressItems>0</autoProgressItems>
  <confirmationText>
    <confirmationText />
  </confirmationText>
  <requiresConfirmationCheck>0</requiresConfirmationCheck>
  <allowCloseWithoutSubmit>0</allowCloseWithoutSubmit>
  <useSecureMarker>0</useSecureMarker>
  <annotationVersion>0</annotationVersion>
  <allowPackagingDelivery>1</allowPackagingDelivery>
  <scoreBoundaryData>
    <scoreBoundaries showScoreBoundaryColumn="1">
      <score modifier="lt" value="36" description="Significantly below" higherBoundarySet="1" />
      <score modifier="lt" value="36" description="Significantly below" higherBoundarySet="0" />
      <score modifier="gt" value="36" description="Below requirement" higherBoundarySet="1" />
      <score modifier="gt" value="60" description="Borderline" higherBoundarySet="1" />
      <score modifier="gt" value="70" description="Met" higherBoundarySet="0" />
      <score modifier="gt" value="70" description="Met" higherBoundarySet="1" />
      <score modifier="gt" value="36" description="Below requirement" higherBoundarySet="0" userCreated="1" />
      <score modifier="gt" value="60" description="Borderline" higherBoundarySet="0" userCreated="1" />
      <score modifier="gt" value="85" description="Exceeded" higherBoundarySet="1" userCreated="1" />
    </scoreBoundaries>
  </scoreBoundaryData>
  <showCandidateReportResultColumn>0</showCandidateReportResultColumn>
  <showCandidateReportPercentageColumn>1</showCandidateReportPercentageColumn>
  <certifiedAccessible>0</certifiedAccessible>
  <markingProgressVisible>1</markingProgressVisible>
  <markingProgressMode>0</markingProgressMode>
  <secureClientOperationMode>1</secureClientOperationMode>
  <examDeliverySystemStyleType>delivery</examDeliverySystemStyleType>
  <markingAutoVoidPeriod>365</markingAutoVoidPeriod>
  <showPageRequiresScrollingAlert>0</showPageRequiresScrollingAlert>
  <project ID="919" />
</assessmentDetails>'
where ID = 1345546

update WAREHOUSE_ExamSessionTable_Shreded 
set StructureXML = '<assessmentDetails>
  <assessmentID>2078</assessmentID>
  <qualificationID>134</qualificationID>
  <qualificationName>Professional ethics (SU NZ AQ2013)</qualificationName>
  <qualificationReference>NZAQSU</qualificationReference>
  <assessmentGroupName>PETH (SU NZ AQ2013)</assessmentGroupName>
  <assessmentGroupID>1961</assessmentGroupID>
  <assessmentGroupReference>CAPETHNZ2</assessmentGroupReference>
  <assessmentName>PETH (SU NZ AQ2013)</assessmentName>
  <validFromDate>01 Mar 2014</validFromDate>
  <expiryDate>31 Dec 2016</expiryDate>
  <startTime>00:00:00</startTime>
  <endTime>23:59:59</endTime>
  <duration>150</duration>
  <defaultDuration>150</defaultDuration>
  <scheduledDuration>
    <value>150</value>
    <reason></reason>
  </scheduledDuration>
  <sRBonus>0</sRBonus>
  <sRBonusMaximum>50</sRBonusMaximum>
  <externalReference>CAPETHNZ2</externalReference>
  <passLevelValue>70</passLevelValue>
  <passLevelType>1</passLevelType>
  <status>2</status>
  <testFeedbackType>
    <passFail>0</passFail>
    <percentageMark>0</percentageMark>
    <allowSummaryFeedback>0</allowSummaryFeedback>
    <summaryFeedbackType>1</summaryFeedbackType>
    <itemSummary>0</itemSummary>
    <itemReview>0</itemReview>
    <itemReviewCorrectAnswer>0</itemReviewCorrectAnswer>
    <itemFeedback>0</itemFeedback>
    <printableSummary>0</printableSummary>
    <candidateDetails>0</candidateDetails>
    <feedbackByReference>0</feedbackByReference>
    <allowFeedbackDuringExam>0</allowFeedbackDuringExam>
    <allowFeedbackDuringSummary>0</allowFeedbackDuringSummary>
  </testFeedbackType>
  <itemFeedback>0</itemFeedback>
  <allowCalculator>0</allowCalculator>
  <lastInUseDate>12 Mar 2014 14:29:45</lastInUseDate>
  <completedScriptReview>0</completedScriptReview>
  <assessment currentSection="1" totalTime="0" currentTime="0">
    <intro id="0" name="Introduction" currentItem="">
      <item id="919P971" name="PETHNZ introduction" totalMark="1" version="10" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
    </intro>
    <outro id="0" name="Finish" currentItem="" />
    <tools id="0" name="Tools" currentItem="" />
    <section id="1" name="Section 1" passLevelValue="70" passLevelType="1" itemsToMark="0" currentItem="919P974" fixed="0">
      <item id="919P980" name="01 PETHNZ GF 002" totalMark="5" version="30" markingType="1" markingState="1" userMark="0" markerUserMark="4" userAttempted="1" viewingTime="342858" flagged="0" LO="01. Regulatory environment: Regulatory environment, professional bodies, NZICA Act" unit="Professional Ethics New Zealand" quT="11" />
      <item id="919P979" name="02 PETHNZ GF 002" totalMark="10" version="33" markingType="1" markingState="1" userMark="0" markerUserMark="8" userAttempted="1" viewingTime="1407449" flagged="0" LO="01. Regulatory environment: NZICA structure, membership, discipline" unit="Professional Ethics New Zealand" quT="11" />
      <item id="919P978" name="03 PETHNZ GF 002" totalMark="5" version="28" markingType="1" markingState="1" userMark="0" markerUserMark="5" userAttempted="1" viewingTime="567326" flagged="0" LO="03. Working ethically: Codes of practice, fundamental principles, handling client monies" unit="Professional Ethics New Zealand" quT="11" />
      <item id="919P977" name="04 PETHNZ GF 002" totalMark="10" version="30" markingType="1" markingState="1" userMark="0" markerUserMark="7" userAttempted="1" viewingTime="1498881" flagged="0" LO="04. Ethical decision making: Use an ethical decision making process" unit="Professional Ethics New Zealand" quT="11" />
      <item id="919P976" name="05 PETHNZ GF 002" totalMark="10" version="40" markingType="1" markingState="1" userMark="0" markerUserMark="9" userAttempted="1" viewingTime="977855" flagged="0" LO="05. Case study � matter 1: Integrity, threats, organisational values, codes of practice" unit="Professional Ethics New Zealand" quT="11" />
      <item id="919P975" name="06 PETHNZ GF 002" totalMark="10" version="28" markingType="1" markingState="1" userMark="0" markerUserMark="7" userAttempted="1" viewingTime="757600" flagged="0" LO="06. Case study � matter 2: Objectivity, conflicts of interest, receiving gifts" unit="Professional Ethics New Zealand" quT="11" />
      <item id="919P974" name="07 PETHNZ GF 002" totalMark="10" version="30" markingType="1" markingState="1" userMark="0" markerUserMark="10" userAttempted="1" viewingTime="960274" flagged="0" LO="07. Case study � matter 3: Professional competence and due care, working beyond expertise" unit="Professional Ethics New Zealand" quT="11" />
      <item id="919P973" name="08 PETHNZ GF 002" totalMark="10" version="30" markingType="1" markingState="1" userMark="0" markerUserMark="8" userAttempted="1" viewingTime="1466045" flagged="0" LO="08. Case study � matter 4: Confidentiality, reporting unethical or illegal acts" unit="Professional Ethics New Zealand" quT="11" />
      <item id="919P972" name="09 PETHNZ GF 002" totalMark="10" version="28" markingType="1" markingState="1" userMark="0" markerUserMark="10" userAttempted="1" viewingTime="1022004" flagged="0" LO="09. Case study � matter 5: Professional behaviour, money laundering, new clients" unit="Professional Ethics" quT="11" />
    </section>
  </assessment>
  <isValid>1</isValid>
  <isPrintable>0</isPrintable>
  <qualityReview>0</qualityReview>
  <numberOfGenerations>1</numberOfGenerations>
  <requiresInvigilation>1</requiresInvigilation>
  <advanceContentDownloadTimespan>60</advanceContentDownloadTimespan>
  <automaticVerification>0</automaticVerification>
  <offlineMode>2</offlineMode>
  <requiresValidation>0</requiresValidation>
  <requiresSecureClient>1</requiresSecureClient>
  <examType>0</examType>
  <advanceDownload>0</advanceDownload>
  <gradeBoundaryData>
    <gradeBoundaries passLevelType="1" adjustedGradeBoundaries="0">
      <grade modifier="lt" value="70" description="Fail" />
      <grade modifier="gt" value="70" description="Pass" />
    </gradeBoundaries>
  </gradeBoundaryData>
  <autoViewExam>0</autoViewExam>
  <autoProgressItems>0</autoProgressItems>
  <confirmationText>
    <confirmationText></confirmationText>
  </confirmationText>
  <requiresConfirmationCheck>0</requiresConfirmationCheck>
  <allowCloseWithoutSubmit>0</allowCloseWithoutSubmit>
  <useSecureMarker>0</useSecureMarker>
  <annotationVersion>0</annotationVersion>
  <allowPackagingDelivery>1</allowPackagingDelivery>
  <scoreBoundaryData>
    <scoreBoundaries showScoreBoundaryColumn="1">
      <score modifier="lt" value="36" description="Significantly below" higherBoundarySet="1" />
      <score modifier="lt" value="36" description="Significantly below" higherBoundarySet="0" />
      <score modifier="gt" value="36" description="Below requirement" higherBoundarySet="1" />
      <score modifier="gt" value="60" description="Borderline" higherBoundarySet="1" />
      <score modifier="gt" value="70" description="Met" higherBoundarySet="0" />
      <score modifier="gt" value="70" description="Met" higherBoundarySet="1" />
      <score modifier="gt" value="36" description="Below requirement" higherBoundarySet="0" userCreated="1" />
      <score modifier="gt" value="60" description="Borderline" higherBoundarySet="0" userCreated="1" />
      <score modifier="gt" value="85" description="Exceeded" higherBoundarySet="1" userCreated="1" />
    </scoreBoundaries>
  </scoreBoundaryData>
  <showCandidateReportResultColumn>0</showCandidateReportResultColumn>
  <showCandidateReportPercentageColumn>1</showCandidateReportPercentageColumn>
  <certifiedAccessible>0</certifiedAccessible>
  <markingProgressVisible>1</markingProgressVisible>
  <markingProgressMode>0</markingProgressMode>
  <secureClientOperationMode>1</secureClientOperationMode>
  <examDeliverySystemStyleType>delivery</examDeliverySystemStyleType>
  <markingAutoVoidPeriod>365</markingAutoVoidPeriod>
  <showPageRequiresScrollingAlert>0</showPageRequiresScrollingAlert>
  <project ID="919" />
</assessmentDetails>'
where examSessionId = 1345546