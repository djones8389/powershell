DECLARE @myCorrectMarks TABLE (uniqueresponseid BIGINT, uniquegroupresponseid BIGINt, mark DECIMAL(18,10), itemname NVARCHAR(20), keycode NVARCHAR(10))

insert into @myCorrectMarks
select uniqueresponses.id, uniquegroupresponses.id, (responsedata.value('(p/@um)[1]', 'decimal(18,10)') * Items.TotalMark), Items.ExternalItemID, Keycode
	from CandidateExamVersions
	inner join CandidateResponses
	on CandidateResponses.CandidateExamVersionID = CandidateExamVersions.id
	inner join UniqueResponses
	on UniqueResponses.id = CandidateResponses.UniqueResponseID
	inner join UniqueGroupResponseLinks
	on UniqueGroupResponseLinks.UniqueResponseId = UniqueResponses.id
	inner join UniqueGroupResponses
	on UniqueGroupResponses.ID = UniqueGroupResponseLinks.UniqueGroupResponseID
	INNER join Items
	on items.ID = itemId
		where ExternalSessionID in (1502590,1510435,1510401) and MarkingType <> 1
		and abs((responsedata.value('(p/@um)[1]', 'decimal(18,10)') * Items.TotalMark)- UniqueGroupResponses.confirmedMark) > 0.0001
		order by ExternalSessionID, abs((responsedata.value('(p/@um)[1]', 'decimal(18,10)') * Items.TotalMark)- UniqueGroupResponses.confirmedMark) desc
		
select *
from @myCorrectMarks

/*
SELECT UGR.ID, UR.ID
FROM CandidateExamVersions as CEV

INNER JOIN CandidateResponses as CR
ON CR.CandidateExamVersionID = CEV.ID

INNER JOIN UniqueResponses as UR
ON UR.ID = CR.UniqueResponseID 

INNER JOIN UniqueGroupResponseLinks as UGRL
on UGRL.UniqueResponseId = UR.id

INNER JOIN UniqueGroupResponses as UGR
on UGR.ID = UGRL.UniqueGroupResponseID

INNER JOIN Items as I
on I.ID = UR.itemId

where Keycode in ('FDF7P6A6','U35CU7A6','KWJR2GA6')
*/

begin tran

select * from UniqueResponses where id IN (113848,121103,121104,121097,121099,121171,121170,121169,121168,121174,121173,121167);

UPDATE UniqueResponses
SET [checksum] = HASHBYTES('sha1', Checksum + CAST(UniqueResponses.id AS nvarchar(20))) 
where id IN (113848,121103,121104,121097,121099,121171,121170,121169,121168,121174,121173,121167);

select * from UniqueResponses where id IN (113848,121103,121104,121097,121099,121171,121170,121169,121168,121174,121173,121167);



select *   from UniqueGroupResponses   where id IN (113851,121104,121105,121106,121100,121172,121170,121175,121174,121176,121171,121173);

update UniqueGroupResponses
SET [checksum] = HASHBYTES('sha1', Checksum + CAST(UniqueGroupResponses.id AS nvarchar(20)))
where id IN (113851,121104,121105,121106,121100,121172,121170,121175,121174,121176,121171,121173);

select *   from UniqueGroupResponses   where id IN (113851,121104,121105,121106,121100,121172,121170,121175,121174,121176,121171,121173);

rollback