--use AAT_ItemBank select  ID, QualificationID, Name from AssessmentGroupTable where IsProjectBasedTest = 1 order by QualificationID;

use AAT_SecureAssess 
SELECT EST.ID, EST.KeyCode, EST.examState, UT.Forename, UT.Surname, CT.CentreName, SCET.examName, IsProjectBased, CreatedBy, scet.ScheduledStartDateTime
	, purchaseOrder, ExamID, qualificationId
  FROM ExamSessionTable as EST

  Inner Join ScheduledExamsTable as SCET
  on SCET.ID = EST.ScheduledExamID
  
  Inner Join UserTable as UT
  on UT.ID = EST.UserID

  Inner Join CentreTable as CT
  on CT.ID = SCET.CentreID

where CentreName = 'Practice Qualification Centre'
	and scet.ScheduledStartDateTime > '27 aug 2014'
	and CreatedBy = 1
	order by EST.ID DESC
	order by ExamState desc;
--IsProjectBased = 1 order by examstate;


--delete from ExamSessionTable

--where id in (1404675,1404676,1404677)


update ExamSessionTable
set previousExamState = examState , examState = 1
where ID = 1404694

select ID, examState, KeyCode from ExamSessionTable where ID = 1404694

select * from StateManagementException where ExamSessionID = 1404789

select COUNT(examState) as CT, examState
	from ExamSessionTable
		group by examState
		
		