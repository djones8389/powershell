--184--

SELECT CentreID
  FROM [AAT_SecureAssess].[dbo].[CentreQualificationsTable]
    where QualificationID = 184
  
  --MERGE--
  
  
create table #DJTable
(
	CentreID int,
	QualificationID int
)


with cte  as (

SELECT distinct CentreID as 'CentreID'
  FROM [AAT_SecureAssess].[dbo].[CentreQualificationsTable]
      where CentreID not IN (856)
)
    


insert into #DJTable(CentreID, QualificationID)

SELECT distinct cte.CentreID
	    , '184'
  from cte
  
select * from #DJTable order by CentreID

--begin tran

merge into CentreQualificationsTable as TGT
using #DJTable as SRC
	on 1 = 2 -- TGT.CentreID = SRC.CentreID
when not matched then 
	insert (CentreID, QualificationID)
	Values (SRC.CentreID,  SRC.QualificationID);
	
--rollback
	
drop table #DJTable

