--SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

--Select distinct KeyCode, warehouseTime,wesirt.ItemID--, warehouseTime, resultDataFull, s.i.value('@userAttempted','bit') as 'UserAttempted'

----, s.i.value('@id','nvarchar(20)') as ItemID
--from WAREHOUSE_ExamSessionTable as west

--inner join WAREHOUSE_ExamSessionItemResponseTable as wesirt
--on wesirt.WAREHOUSEExamSessionID = west.ID

--cross apply resultdata.nodes('/exam/section/item') s(i)

--where west.ID in (1406920,1416932,1455373,1405416,1407062,1413908,1426792,1440871,1442909,1457127,1488444,1510435,1514315,1355262,1381801,1406949,1410317,1425642,1487156,1404432,1406815,1406838,1436328,1436967,1507367,1406371,1429197,1429198,1429199,1429200,1429201,1441900,1501040,1413941,1429366,1431632,1431698,1443113,1389289,1389290,1406233,1418270,1421554,1426822,1426880,1429186,1429187,1429188,1429189,1429190,1429191,1429192,1429193,1429194,1429195,1429196,1402076,1403058,1406683,1406703,1427215,1453726,1510401,1388610,1425575,1425576,1450793,1357754,1408340,1479911,1392876,1404675,1406447,1451954,1457191,1466239,1434514,1436329,1436330,1441495,1474976,1502590)
--	and wesirt.ItemID = s.i.value('@id','nvarchar(20)')
--	and WarehouseExamState = 1
--	order by warehouseTime desc;
----where resultDataFull.exist('//section/item[@userAttempted=0 and p[@ua=1]]') = 1	and warehouseTime > '01 July 2014';


SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

select 
	west.ID
	, west.KeyCode
	, west.ExportToSecureMarker
	from WAREHOUSE_ExamSessionTable as west
	
	inner join WAREHOUSE_ExamSessionTable_Shreded as wests
	on wests.examSessionId = west.ID
	
	inner join WAREHOUSE_ExamSessionTable_ShrededItems as westsi
	on westsi.examSessionId = west.ID
	
WHERE WESTS.userMark = 0
					AND examPercentage = 0
					AND userAttempted = 0
					AND WEST.warehouseTime > '2014-07-14'
					AND WEST.PreviousExamState <> 10
					AND wests.examVersionRef NOT LIKE '%Practice'	
					and resultDataFull.exist('/assessmentDetails/assessment/section/item[@userAttempted=0 and p[@ua=1]]') = 1;