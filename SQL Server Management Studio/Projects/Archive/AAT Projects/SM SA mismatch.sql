use AAT_SecureAssess

SELECT WAREHOUSE_ExamSessionTable.ID, WAREHOUSE_ExamSessionTable.Keycode	
FROM WAREHOUSE_ExamSessionTable
INNER JOIN [430327-AAT-SQL2\SQL2].AAT_SecureMarker.dbo.CandidateExamVersions
ON CandidateExamVersions.ExternalSessionID = WAREHOUSE_ExamSessionTable.ID
WHERE WAREHOUSE_ExamSessionTable.WarehouseExamState = 3 AND CandidateExamVersions.ExamSessionState > 3

