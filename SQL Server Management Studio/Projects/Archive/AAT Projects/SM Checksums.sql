--DECLARE @myCorrectMarks TABLE (uniqueresponseid BIGINT, uniquegroupresponseid BIGINt, mark DECIMAL(18,10), itemname NVARCHAR(20), keycode NVARCHAR(10))

--insert into @myCorrectMarks
--select uniqueresponses.id, uniquegroupresponses.id, (responsedata.value('(p/@um)[1]', 'decimal(18,10)') * Items.TotalMark), Items.ExternalItemID, Keycode
--	from CandidateExamVersions
--	inner join CandidateResponses
--	on CandidateResponses.CandidateExamVersionID = CandidateExamVersions.id
--	inner join UniqueResponses
--	on UniqueResponses.id = CandidateResponses.UniqueResponseID
--	inner join UniqueGroupResponseLinks
--	on UniqueGroupResponseLinks.UniqueResponseId = UniqueResponses.id
--	inner join UniqueGroupResponses
--	on UniqueGroupResponses.ID = UniqueGroupResponseLinks.UniqueGroupResponseID
--	INNER join Items
--	on items.ID = itemId
--		where ExternalSessionID in (1502590,1510435,1510401) and MarkingType <> 1
--		and abs((responsedata.value('(p/@um)[1]', 'decimal(18,10)') * Items.TotalMark)- UniqueGroupResponses.confirmedMark) > 0.0001
--		order by ExternalSessionID, abs((responsedata.value('(p/@um)[1]', 'decimal(18,10)') * Items.TotalMark)- UniqueGroupResponses.confirmedMark) desc
		
--select *
--from @myCorrectMarks


SELECT UGR.ID, UR.ID
FROM CandidateExamVersions as CEV

INNER JOIN CandidateResponses as CR
ON CR.CandidateExamVersionID = CEV.ID

INNER JOIN UniqueResponses as UR
ON UR.ID = CR.UniqueResponseID 

INNER JOIN UniqueGroupResponseLinks as UGRL
on UGRL.UniqueResponseId = UR.id

INNER JOIN UniqueGroupResponses as UGR
on UGR.ID = UGRL.UniqueGroupResponseID

INNER JOIN Items as I
on I.ID = UR.itemId

where Keycode in ('34FF6KA6','ZQ6GJPA6')


begin tran

select * from UniqueResponses where id IN (9336,18205,39009,53826,96751,96755,96753,96750,96756,96752,96754,134253,143651,164044,164046,164048,164049,164047,164042,164045,164043,164041);

UPDATE UniqueResponses
SET [checksum] = HASHBYTES('sha1', Checksum + CAST(UniqueResponses.id AS nvarchar(20))) 
where id IN (9336,18205,39009,53826,96751,96755,96753,96750,96756,96752,96754,134253,143651,164044,164046,164048,164049,164047,164042,164045,164043,164041);

select * from UniqueResponses where id IN (9336,18205,39009,53826,96751,96755,96753,96750,96756,96752,96754,134253,143651,164044,164046,164048,164049,164047,164042,164045,164043,164041);


select *   from UniqueGroupResponses   where id IN (9336,18201,39013,53824,96750,96751,96752,96753,96754,96755,96756,134249,143657,164041,164042,164043,164044,164045,164046,164047,164048,164049);

update UniqueGroupResponses
SET [checksum] = HASHBYTES('sha1', Checksum + CAST(UniqueGroupResponses.id AS nvarchar(20)))
where id IN (9336,18201,39013,53824,96750,96751,96752,96753,96754,96755,96756,134249,143657,164041,164042,164043,164044,164045,164046,164047,164048,164049);

select *   from UniqueGroupResponses   where id IN (9336,18201,39013,53824,96750,96751,96752,96753,96754,96755,96756,134249,143657,164041,164042,164043,164044,164045,164046,164047,164048,164049);

rollback