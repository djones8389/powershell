SELECT EST.ID, EST.KeyCode, EST.examState, UT.Forename, UT.Surname, CT.CentreName, SCET.examName
	, AutoVoidDate,  DATEADD(day, EST.[MarkingAutoVoidPeriod], GETDATE()), EST.AwaitingUploadGracePeriodInDays, EST.AwaitingUploadStateChangingTime, attemptAutoSubmitForAwaitingUpload
		, SCET.ID, EST.[MarkingAutoVoidPeriod]
  FROM ExamSessionTable as EST

  Inner Join ScheduledExamsTable as SCET
  on SCET.ID = EST.ScheduledExamID
  
  Inner Join UserTable as UT
  on UT.ID = EST.UserID

  Inner Join CentreTable as CT
  on CT.ID = SCET.CentreID

where surname = 'Poucek' or Surname = 'Dehnavi';

select * from ExamStateChangeAuditTable where ExamSessionID = 1422133 or ExamSessionID = 1449184;


--select * from ExamStateChangeAuditTable where NewState in (18,9,15)


with cte as (

select ExamSessionTable.ID, MarkingAutoVoidPeriod, AutoVoidDate, DATEADD(day, MarkingAutoVoidPeriod, GETDATE()) 'GetDate', examState
	 from ExamSessionTable 
	
	inner join UserTable on UserTable.ID = ExamSessionTable.UserID

where AutoVoidDate is  null 
	and ExamSessionTable.ID IN (Select examSessionID from ExamStateChangeAuditTable where NewState = 9)
	and examState <> 13

	)
	
	
Select * from ExamStateChangeAuditTable where ExamSessionID IN (

--1417101,1431955,1420495,1421762,1417103,1422133,1413643
select ID from cte
)