select CentreName
		, CentreCode
		, case Retired when 0 then 'Active' when 1 then 'Retired' end as 'Status'

from CentreTable

order by CentreName