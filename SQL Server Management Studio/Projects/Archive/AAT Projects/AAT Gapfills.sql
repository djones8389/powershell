Select distinct 
		ProjectListTable.Name
		, SUBSTRING(ItemGapfillTable.ID, CHARINDEX('P', ItemGapfillTable.ID) + 1, CHARINDEX('S',ItemGapfillTable.ID) - CHARINDEX('P', ItemGapfillTable.ID) - 2 + Len('S')) as PageNumber
		, ItemGapfillTable.ID as 'Project_Page_Scene_Component'
		, hei as 'Height'
 from ItemGapfillTable 
 
      inner join ProjectListTable
      on ProjectListTable.ID = SUBSTRING(ItemGapfillTable.ParentID, 0 , CHARINDEX(N'P', ItemGapfillTable.ParentID)) 

      inner join PageTable
      on PageTable.id = SUBSTRING(ItemGapfillTable.ID, 0, CHARINDEX('S',ItemGapfillTable.ID))

 where muL = 1 
	and hei >= 47
	and hei <= 801
		order by ProjectListTable.Name asc