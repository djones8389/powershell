SELECT --WEST.ID, WEST.KeyCode, WUT.Forename, WUT.Surname, WUT.CandidateRef, WCT.CentreName, WSCET.examName, WEST.completionDate
	--, WESTS.ExternalReference, west.StructureXML
	west.ID as [ExamSessionID]
	,ItemID as [ItemID]
	,west.KeyCode
FROM WAREHOUSE_ExamSessionTable as WEST

  Inner Join WAREHOUSE_ScheduledExamsTable as WSCET
  on WSCET.ID = WEST.WAREHOUSEScheduledExamID
  
  Inner Join WAREHOUSE_UserTable as WUT
  on WUT.ID = WEST.WAREHOUSEUserID

  Inner Join WAREHOUSE_CentreTable as WCT
  on WCT.ID = WSCET.WAREHOUSECentreID

  Inner Join WAREHOUSE_ExamSessionTable_Shreded as WESTS
  on WESTS.examSessionId = WEST.ID

  inner join WAREHOUSE_ExamSessionItemResponseTable as WESIRT
  on WESIRT.WAREHOUSEExamSessionID = west.ID

where west.KeyCode  in ('N95Z9DA6','HBK8EFA6','C6D6QJA6','ACL2VKA6','QB42CYA6')
order by west.ID,ItemID