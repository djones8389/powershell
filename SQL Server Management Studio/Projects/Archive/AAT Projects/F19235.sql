select distinct
	 Username
      ,Forename
      ,Surname
      ,CentreName
      ,CentreCode


from UserTable as UT

inner join AssignedUserRolesTable as AURT
on AURT.UserID = UT.ID

inner join CentreTable as CT
on CT.ID = AURT.CentreID

where 
      CandidateRef = ''
      and Email not like '%@btl.com'
      and UT.Retired = 0
      
order by CentreName, Forename, Surname asc
