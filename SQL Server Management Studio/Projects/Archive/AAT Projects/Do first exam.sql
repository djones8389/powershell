--begin tran

declare @totalMark int = '150'
declare @NewValue int = 0
declare @esid int = 1393628

--(1393378,1393498,1393533,1393628,1393705,1394110,1394182)--


/*
1)  Set the TotalMark using the Cross Apply calculator
2)  Set the StructureXML manually,  see line 530
3)  Check for Section 2
*/

	update WAREHOUSE_ExamSessionTable
	set StructureXML = '<assessmentDetails xmlns="">
  <assessmentID>804</assessmentID>
  <qualificationID>43</qualificationID>
  <qualificationName>Financial Statements (AQ2010)</qualificationName>
  <qualificationReference>AATQCF</qualificationReference>
  <assessmentGroupName>FNST</assessmentGroupName>
  <assessmentGroupID>349</assessmentGroupID>
  <assessmentGroupReference>CAFNST</assessmentGroupReference>
  <assessmentName>FNST pop up variant 5</assessmentName>
  <validFromDate>01 Jan 2012</validFromDate>
  <expiryDate>31 Dec 2014</expiryDate>
  <startTime>00:00:00</startTime>
  <endTime>23:59:59</endTime>
  <duration>150</duration>
  <defaultDuration>150</defaultDuration>
  <scheduledDuration>
    <value>150</value>
    <reason></reason>
  </scheduledDuration>
  <sRBonus>0</sRBonus>
  <sRBonusMaximum>50</sRBonusMaximum>
  <externalReference>CBTAAT</externalReference>
  <passLevelValue>70</passLevelValue>
  <passLevelType>1</passLevelType>
  <status>2</status>
  <testFeedbackType>
    <passFail>0</passFail>
    <percentageMark>0</percentageMark>
    <allowSummaryFeedback>0</allowSummaryFeedback>
    <summaryFeedbackType>1</summaryFeedbackType>
    <itemSummary>0</itemSummary>
    <itemReview>0</itemReview>
    <itemReviewCorrectAnswer>0</itemReviewCorrectAnswer>
    <itemFeedback>0</itemFeedback>
    <printableSummary>0</printableSummary>
    <candidateDetails>0</candidateDetails>
    <feedbackByReference>0</feedbackByReference>
    <allowFeedbackDuringExam>0</allowFeedbackDuringExam>
    <allowFeedbackDuringSummary>0</allowFeedbackDuringSummary>
  </testFeedbackType>
  <itemFeedback>0</itemFeedback>
  <allowCalculator>0</allowCalculator>
  <lastInUseDate>10 Nov 2011 17:36:04</lastInUseDate>
  <completedScriptReview>0</completedScriptReview>
  <assessment currentSection="0" totalTime="0" currentTime="0" originalPassMark="70" originalPassType="1" passMark="70" passType="1" totalMark="300" userMark="164" userPercentage="54.667" passValue="0" originalGrade="Fail">
    <intro id="0" name="Introduction" currentItem="">
      <item id="879P2208" name="Copy of Introduction" totalMark="1" version="10" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
    </intro>
    <outro id="0" name="Finish" currentItem="" />
    <tools id="0" name="Tools" currentItem="">
      <item id="879P1629" name="1.1 FNST AC 001 Asker Ltd popup A" toolName="1.1 FNST AC 001 Asker Ltd popup A" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1628" name="1.1 FNST AC 001 Asker Ltd popup B" toolName="1.1 FNST AC 001 Asker Ltd popup B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1620" name="1.1 FNST DO 001 Polo Ltd popup A" toolName="1.1 FNST DO 001 Polo Ltd popup A" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1619" name="1.1 FNST DO 001 Polo Ltd popup B" toolName="1.1 FNST DO 001 Polo Ltd popup B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1368" name="1.1 FNST AC002 Cole Ltd popup A" toolName="1.1 FNST AC002 Cole Ltd popup A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1369" name="1.1 FNST AC002 Cole Ltd popup B" toolName="1.1 FNST AC002 Cole Ltd popup B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1370" name="1.1 FNST AC003 Allen Ltd popup A" toolName="1.1 FNST AC003 Allen Ltd popup A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1371" name="1.1 FNST AC003 Allen Ltd popup B" toolName="1.1 FNST AC003 Allen Ltd popup B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1372" name="1.1 FNST DO002 Hughes Ltd popup A" toolName="1.1 FNST DO002 Hughes Ltd popup A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1373" name="1.1 FNST DO002 Hughes Ltd popup B" toolName="1.1 FNST DO002 Hughes Ltd popup B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1374" name="1.1 FNST DO003 Burgess Ltd popup A" toolName="1.1 FNST DO003 Burgess Ltd popup A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1375" name="1.1 FNST DO003 Burgess Ltd popup B" toolName="1.1 FNST DO003 Burgess Ltd popup B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1376" name="1.5 FNST AC002 Austell popup A" toolName="1.5 FNST AC002 Austell popup A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1377" name="1.5 FNST AC002 Austell popup B" toolName="1.5 FNST AC002 Austell popup B" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1378" name="1.5 FNST AC003 Avon popup A" toolName="1.5 FNST AC003 Avon popup A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1379" name="1.5 FNST AC003 Avon popup B" toolName="1.5 FNST AC003 Avon popup B" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1380" name="1.5 FNST DO003 Finch popup A" toolName="1.5 FNST DO003 Finch popup A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1381" name="1.5 FNST DO003 Finch popup B" toolName="1.5 FNST DO003 Finch popup B" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1382" name="2.1 FNST AC003 popup A" toolName="2.1 FNST AC003 popup A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1383" name="2.1 FNST AC003 popup B" toolName="2.1 FNST AC003 popup B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1384" name="2.1 FNST AC002 popup A" toolName="2.1 FNST AC002 popup A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1385" name="2.1 FNST AC002 popup B" toolName="2.1 FNST AC002 popup B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1386" name="2.1 FNST AC001 popup A" toolName="2.1 FNST AC001 popup A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1387" name="2.1 FNST AC001 popup B" toolName="2.1 FNST AC001 popup B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1388" name="2.1 FNST DO002 popup A" toolName="2.1 FNST DO002 popup A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1389" name="2.1 FNST DO002 popup B" toolName="2.1 FNST DO002 popup B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1390" name="2.1 FNST DO003 popup A" toolName="2.1 FNST DO003 popup A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1391" name="2.1 FNST DO003 popup B" toolName="2.1 FNST DO003 popup B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1633" name="1.1 FNST AC 001 Iron Ltd popup A" toolName="1.1 FNST AC 001 Iron Ltd popup A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1634" name="1.1 FNST AC 001 Iron Ltd popup B" toolName="1.1 FNST AC 001 Iron Ltd popup B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1646" name="1.1 FNST AC 002 Camel A" toolName="1.1 FNST AC 002 Camel A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1647" name="1.1 FNST AC 002 Camel B" toolName="1.1 FNST AC 002 Camel B" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1653" name="1.1 FNST AC 003 Trent A" toolName="1.1 FNST AC 003 Trent A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1654" name="1.1 FNST AC 003 Trent B" toolName="1.1 FNST AC 003 Trent B" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1641" name="1.1 FNST DO 001 Shelford Ltd popup A" toolName="1.1 FNST DO 001 Shelford Ltd popup A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1642" name="1.1 FNST DO 001 Shelford Ltd popup B" toolName="1.1 FNST DO 001 Shelford Ltd popup B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1637" name="1.5 FNST AC 001 Mole Ltd popup A" toolName="1.5 FNST AC 001 Mole Ltd popup A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1638" name="1.5 FNST AC 001 Madford Plc popup A" toolName="1.5 FNST AC 001 Madford Plc popup A" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1649" name="1.5 FNST AC 002 Chelt A" toolName="1.5 FNST AC 002 Chelt A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1650" name="1.5 FNST AC 002 Chelt B" toolName="1.5 FNST AC 002 Chelt B" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1655" name="1.5 FNST AC 003 Kinder A" toolName="1.5 FNST AC 003 Kinder A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1643" name="1.5 FNST DO 001 Rickling Ltd popup A" toolName="1.5 FNST DO 001 Rickling Ltd popup A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1644" name="1.5 FNST DO 001 Stebbing Ltd popup A" toolName="1.5 FNST DO 001 Stebbing Ltd popup A" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1661" name="2.1 FNST AC 001 Durham Ltd popup A" toolName="2.1 FNST AC 001 Durham Ltd popup A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1662" name="2.1 FNST AC 001 Durham Ltd popup B" toolName="2.1 FNST AC 001 Durham Ltd popup B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1665" name="2.1 FNST AC 002 Tweed A" toolName="2.1 FNST AC 002 Tweed A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1666" name="2.1 FNST AC 002 Tweed B" toolName="2.1 FNST AC 002 Tweed B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1667" name="2.1 FNST AC 003 Arun A" toolName="2.1 FNST AC 003 Arun A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1668" name="2.1 FNST AC 003 Arun B" toolName="2.1 FNST AC 003 Arun B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1663" name="2.1 FNST DO 001 Wembling A" toolName="2.1 FNST DO 001 Wembling A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1664" name="2.1 FNST DO 001 Wembling B" toolName="2.1 FNST DO 001 Wembling B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2356" name="1.5 FNST DO 010 pop up A" toolName="1.5 FNST DO 010 pop up A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2358" name="1.5 FNST DO 010 pop up B" toolName="1.5 FNST DO 010 pop up B" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2359" name="1.5 FNST DO 010 pop up C" toolName="1.5 FNST DO 010 pop up C" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2305" name="1.5 FNST DO 013 Platt Popup B" toolName="1.5 FNST DO 013 Platt Popup B" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2303" name="1.5 FNST DO 013 Platt Popup A" toolName="1.5 FNST DO 013 Platt Popup A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2249" name="1.5 FNST AC 010 Bywell Popup A" toolName="1.5 FNST AC 010 Bywell Popup A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2250" name="1.5 FNST AC 010 Bywell Popup B" toolName="1.5 FNST AC 010 Bywell Popup B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2322" name="1.5 FNST AC 011 Barton Popup B" toolName="1.5 FNST AC 011 Barton Popup B" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2320" name="1.5 FNST AC 011 Barton Popup A" toolName="1.5 FNST AC 011 Barton Popup A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2258" name="1.5 FNST AC 014 Lowick Popup A" toolName="1.5 FNST AC 014 Lowick Popup A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2257" name="1.5 FNST AC 014 Birtley Popup A" toolName="1.5 FNST AC 014 Birtley Popup A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2334" name="1.5 FNST AC 012 Pop up B" toolName="1.5 FNST AC 012 Pop up B" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2332" name="1.5 FNST AC 012 Pop up A" toolName="1.5 FNST AC 012 Pop up A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2288" name="1.5 FNST AC 013 Earle Popup A" toolName="1.5 FNST AC 013 Earle Popup A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2290" name="1.5 FNST AC 013 Earle Popup B" toolName="1.5 FNST AC 013 Earle Popup B" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2346" name="1.5 FNST DO 011 Hills Pop up A" toolName="1.5 FNST DO 011 Hills Pop up A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2348" name="1.5 FNST DO 011 Grant Pop up B" toolName="1.5 FNST DO 011 Grant Pop up B" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2372" name="1.5 FNST DO 012 Pop up a" toolName="1.5 FNST DO 012 Pop up a" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2374" name="1.5 FNST DO 012 Pop up B" toolName="1.5 FNST DO 012 Pop up B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2369" name="2.1 FNST DO 010 pop up A" toolName="2.1 FNST DO 010 pop up A" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2370" name="2.1 FNST DO 010 pop up B" toolName="2.1 FNST DO 010 pop up B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2251" name="2.1 FNST AC 010 Abberwick Popup A" toolName="2.1 FNST AC 010 Abberwick Popup A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2252" name="2.1 FNST AC 010 Abberwick Popup B" toolName="2.1 FNST AC 010 Abberwick Popup B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2307" name="2.1 FNST DO 013 Court Popup A" toolName="2.1 FNST DO 013 Court Popup A" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2309" name="2.1 FNST DO 013 Court Popup B" toolName="2.1 FNST DO 013 Court Popup B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2260" name="2.1 FNST AC 014 Belsay Popup B" toolName="2.1 FNST AC 014 Belsay Popup B" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2259" name="2.1 FNST AC 014 Belsay Popup A" toolName="2.1 FNST AC 014 Belsay Popup A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2326" name="2.1 FNST AC 011 Anick Popup B" toolName="2.1 FNST AC 011 Anick Popup B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2324" name="2.1 FNST AC 011 Anick Popup A" toolName="2.1 FNST AC 011 Anick Popup A" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2381" name="2.1 FNST DO 012 Pop up B" toolName="2.1 FNST DO 012 Pop up B" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2380" name="2.1 FNST DO 012 Pop up A" toolName="2.1 FNST DO 012 Pop up A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2352" name="2.1 FNST DO 011 Leigh Pop up B" toolName="2.1 FNST DO 011 Leigh Pop up B" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2351" name="2.1 FNST DO 011 Leigh Pop up A" toolName="2.1 FNST DO 011 Leigh Pop up A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2294" name="2.1 FNST AC 013 Lucker Popup B" toolName="2.1 FNST AC 013 Lucker Popup B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2292" name="2.1 FNST AC 013 Lucker Popup A" toolName="2.1 FNST AC 013 Lucker Popup A" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2330" name="2.1 FNST AC 012 Glanton Pop up B" toolName="2.1 FNST AC 012 Glanton Pop up B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2329" name="2.1 FNST AC 012 Glanton Pop up A" toolName="2.1 FNST AC 012 Glanton Pop up A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2394" name="1.5 FNST AC 003 Kinder B v2" toolName="1.5 FNST AC 003 Kinder B v2" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
    </tools>
    <section id="1" name="1" passLevelValue="60" passLevelType="1" itemsToMark="0" currentItem="" fixed="0" totalMark="90" userMark="0" userPercentage="0" passValue="1">
      <item id="879P1617" name="Copy of 1.1 FNST DO 001" totalMark="28" version="12" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="0" flagged="0" LO="1.1 Prepare a statement of comprehensive income/financial position/change in equity/cash flow" unit="Financial Statements" quT="20" />
      <item id="879P1624" name="Copy of 1.2 FNST DO 001" totalMark="8" version="8" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="0" flagged="0" LO="1.2 Prepare a statement of comprehensive income/financial position/change in equity/cash flow" unit="Financial Statements" quT="20" />
      <item id="879P1698" name="1.3 FNST AC 010" totalMark="12" version="23" markingType="1" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="0" flagged="0" LO="1.3 Knowledge and understanding of International Financial Reporting Standards (written)" unit="Financial Statements" quT="11" />
      <item id="879P1703" name="1.4 FNST AC 010" totalMark="12" version="18" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="0" flagged="0" LO="1.4 Application of reporting standards" unit="Financial Statements" quT="10" />
      <item id="879P1697" name="1.5 FNST AC 010 " totalMark="30" version="23" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="0" flagged="0" LO="1.5 Drafting consolidated financial statements" unit="Financial Statements" quT="20" />
    </section>
    <section id="2" name="2" passLevelValue="60" passLevelType="1" itemsToMark="0" currentItem="" fixed="0" totalMark="60" userMark="0" userPercentage="0" passValue="0">
      <item id="879P1799" name="2.1 FNST DO 011" totalMark="32" version="22" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="0" flagged="0" LO="2.1 Analysis of financial statements using ratios" unit="Financial Statements" quT="11,12" />
      <item id="879P2089" name="2.2 FNST AC 002 2011" totalMark="18" version="26" markingType="1" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="0" flagged="0" LO="2.2 Interpretation of financial statements using ratios (written)" unit="Financial Statements" quT="11" />
      <item id="879P1523" name="2.3 FNST AC 005" totalMark="10" version="15" markingType="1" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="0" flagged="0" LO="2.3 Legal and regulatory framework (written)" unit="Financial Statements" quT="11,20" />
    </section>
  </assessment>
  <isValid>1</isValid>
  <isPrintable>0</isPrintable>
  <qualityReview>0</qualityReview>
  <numberOfGenerations>1</numberOfGenerations>
  <requiresInvigilation>1</requiresInvigilation>
  <advanceContentDownloadTimespan>60</advanceContentDownloadTimespan>
  <automaticVerification>1</automaticVerification>
  <offlineMode>2</offlineMode>
  <requiresValidation>0</requiresValidation>
  <requiresSecureClient>1</requiresSecureClient>
  <examType>0</examType>
  <advanceDownload>0</advanceDownload>
  <gradeBoundaryData>
    <gradeBoundaries passLevelType="1" adjustedGradeBoundaries="0">
      <grade modifier="lt" value="70" description="Fail" />
      <grade modifier="gt" value="70" description="Pass" />
    </gradeBoundaries>
  </gradeBoundaryData>
  <autoViewExam>0</autoViewExam>
  <autoProgressItems>0</autoProgressItems>
  <confirmationText>
    <confirmationText></confirmationText>
  </confirmationText>
  <requiresConfirmationCheck>0</requiresConfirmationCheck>
  <allowCloseWithoutSubmit>0</allowCloseWithoutSubmit>
  <useSecureMarker>1</useSecureMarker>
  <annotationVersion>1</annotationVersion>
  <allowPackagingDelivery>1</allowPackagingDelivery>
  <scoreBoundaryData>
    <scoreBoundaries showScoreBoundaryColumn="1">
      <score modifier="lt" value="36" description="Significantly below" higherBoundarySet="1" />
      <score modifier="lt" value="36" description="Significantly below" higherBoundarySet="0" />
      <score modifier="gt" value="36" description="Below requirement" higherBoundarySet="1" />
      <score modifier="gt" value="60" description="Borderline" higherBoundarySet="1" />
      <score modifier="gt" value="70" description="Met" higherBoundarySet="0" />
      <score modifier="gt" value="70" description="Met" higherBoundarySet="1" />
      <score modifier="gt" value="36" description="Below requirement" higherBoundarySet="0" userCreated="1" />
      <score modifier="gt" value="60" description="Borderline" higherBoundarySet="0" userCreated="1" />
      <score modifier="gt" value="85" description="Exceeded" higherBoundarySet="1" userCreated="1" />
    </scoreBoundaries>
  </scoreBoundaryData>
  <showCandidateReportResultColumn>1</showCandidateReportResultColumn>
  <showCandidateReportPercentageColumn>1</showCandidateReportPercentageColumn>
  <certifiedAccessible>0</certifiedAccessible>
  <markingProgressVisible>1</markingProgressVisible>
  <markingProgressMode>0</markingProgressMode>
  <secureClientOperationMode>1</secureClientOperationMode>
  <examDeliverySystemStyleType>delivery</examDeliverySystemStyleType>
  <markingAutoVoidPeriod>365</markingAutoVoidPeriod>
  <showPageRequiresScrollingAlert>0</showPageRequiresScrollingAlert>
  <project ID="879" />
</assessmentDetails>'
	where id = @esid;
	

	update WAREHOUSE_ExamSessionTable
	set resultData = '<exam passMark="70" passType="1" originalPassMark="70" originalPassType="1" totalMark="150" userMark="0" userPercentage="0" passValue="0" originalPassValue="0" totalTimeSpent="0" closeBoundaryType="0" closeBoundaryValue="0" grade="Fail" originalGrade="Fail">
  <gradeBoundaryData>
    <gradeBoundaries passLevelType="1" adjustedGradeBoundaries="0">
      <grade modifier="lt" value="70" description="Fail" />
      <grade modifier="gt" value="70" description="Pass" />
    </gradeBoundaries>
  </gradeBoundaryData>
  <section id="1" passMark="60" passType="1" name="1" totalMark="90" userMark="0" userPercentage="0" passValue="1" totalTimeSpent="0" itemsToMark="0">
    <item id="879P1617" name="Copy of 1.1 FNST DO 001" totalMark="28" userMark="0" actualUserMark="0" markingType="0" markerUserMark="0" viewingTime="0" userAttempted="0" version="12" />
    <item id="879P1624" name="Copy of 1.2 FNST DO 001" totalMark="8" userMark="0" actualUserMark="0" markingType="0" markerUserMark="0" viewingTime="0" userAttempted="0" version="8" />
    <item id="879P1698" name="1.3 FNST AC 010" totalMark="12" userMark="0" actualUserMark="0" markingType="0" markerUserMark="0" viewingTime="0" userAttempted="0" version="23" />
    <item id="879P1703" name="1.4 FNST AC 010" totalMark="12" userMark="0" actualUserMark="0" markingType="0" markerUserMark="0" viewingTime="0" userAttempted="0" version="18" />
    <item id="879P1697" name="1.5 FNST AC 010 " totalMark="30" userMark="0" actualUserMark="0" markingType="0" markerUserMark="0" viewingTime="0" userAttempted="0" version="23" />
  </section>
  <section id="2" passMark="60" passType="1" name="2" totalMark="60" userMark="0" userPercentage="0" passValue="0" totalTimeSpent="0" itemsToMark="0">
    <item id="879P1799" name="2.1 FNST DO 011" totalMark="32" userMark="0" actualUserMark="0" markingType="0" markerUserMark="0" viewingTime="0" userAttempted="0" version="22" />
    <item id="879P2089" name="2.2 FNST AC 002 2011" totalMark="18" userMark="0" actualUserMark="0" markingType="0" markerUserMark="0" viewingTime="0" userAttempted="0" version="26" />
    <item id="879P1523" name="2.3 FNST AC 005" totalMark="10" userMark="0" actualUserMark="0" markingType="0" markerUserMark="0" viewingTime="0" userAttempted="0" version="15" />
  </section>
</exam>'
	where id = @esid;

	update WAREHOUSE_ExamSessionTable
	set resultDataFull = '<assessmentDetails xmlns="">
  <assessmentID>804</assessmentID>
  <qualificationID>43</qualificationID>
  <qualificationName>Financial Statements (AQ2010)</qualificationName>
  <qualificationReference>AATQCF</qualificationReference>
  <assessmentGroupName>FNST</assessmentGroupName>
  <assessmentGroupID>349</assessmentGroupID>
  <assessmentGroupReference>CAFNST</assessmentGroupReference>
  <assessmentName>FNST pop up variant 5</assessmentName>
  <validFromDate>01 Jan 2012</validFromDate>
  <expiryDate>31 Dec 2014</expiryDate>
  <startTime>00:00:00</startTime>
  <endTime>23:59:59</endTime>
  <duration>150</duration>
  <defaultDuration>150</defaultDuration>
  <scheduledDuration>
    <value>150</value>
    <reason />
  </scheduledDuration>
  <sRBonus>0</sRBonus>
  <sRBonusMaximum>50</sRBonusMaximum>
  <externalReference>CBTAAT</externalReference>
  <passLevelValue>70</passLevelValue>
  <passLevelType>1</passLevelType>
  <status>2</status>
  <testFeedbackType>
    <passFail>0</passFail>
    <percentageMark>0</percentageMark>
    <allowSummaryFeedback>0</allowSummaryFeedback>
    <summaryFeedbackType>1</summaryFeedbackType>
    <itemSummary>0</itemSummary>
    <itemReview>0</itemReview>
    <itemReviewCorrectAnswer>0</itemReviewCorrectAnswer>
    <itemFeedback>0</itemFeedback>
    <printableSummary>0</printableSummary>
    <candidateDetails>0</candidateDetails>
    <feedbackByReference>0</feedbackByReference>
    <allowFeedbackDuringExam>0</allowFeedbackDuringExam>
    <allowFeedbackDuringSummary>0</allowFeedbackDuringSummary>
  </testFeedbackType>
  <itemFeedback>0</itemFeedback>
  <allowCalculator>0</allowCalculator>
  <lastInUseDate>Aug 18 2014  3:27PM</lastInUseDate>
  <completedScriptReview>0</completedScriptReview>
  <assessment currentSection="0" totalTime="0" currentTime="0" originalPassMark="70" originalPassType="1" passMark="70" passType="1" totalMark="150" userMark="0" userPercentage="0" passValue="0" originalGrade="Fail">
    <intro id="0" name="Introduction" currentItem="">
      <item id="879P2208" name="Copy of Introduction" totalMark="1" version="10" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
    </intro>
    <outro id="0" name="Finish" currentItem="" />
    <tools id="0" name="Tools" currentItem="">
      <item id="879P1629" name="1.1 FNST AC 001 Asker Ltd popup A" toolName="1.1 FNST AC 001 Asker Ltd popup A" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1628" name="1.1 FNST AC 001 Asker Ltd popup B" toolName="1.1 FNST AC 001 Asker Ltd popup B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1620" name="1.1 FNST DO 001 Polo Ltd popup A" toolName="1.1 FNST DO 001 Polo Ltd popup A" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1619" name="1.1 FNST DO 001 Polo Ltd popup B" toolName="1.1 FNST DO 001 Polo Ltd popup B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1368" name="1.1 FNST AC002 Cole Ltd popup A" toolName="1.1 FNST AC002 Cole Ltd popup A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1369" name="1.1 FNST AC002 Cole Ltd popup B" toolName="1.1 FNST AC002 Cole Ltd popup B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1370" name="1.1 FNST AC003 Allen Ltd popup A" toolName="1.1 FNST AC003 Allen Ltd popup A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1371" name="1.1 FNST AC003 Allen Ltd popup B" toolName="1.1 FNST AC003 Allen Ltd popup B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1372" name="1.1 FNST DO002 Hughes Ltd popup A" toolName="1.1 FNST DO002 Hughes Ltd popup A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1373" name="1.1 FNST DO002 Hughes Ltd popup B" toolName="1.1 FNST DO002 Hughes Ltd popup B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1374" name="1.1 FNST DO003 Burgess Ltd popup A" toolName="1.1 FNST DO003 Burgess Ltd popup A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1375" name="1.1 FNST DO003 Burgess Ltd popup B" toolName="1.1 FNST DO003 Burgess Ltd popup B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1376" name="1.5 FNST AC002 Austell popup A" toolName="1.5 FNST AC002 Austell popup A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1377" name="1.5 FNST AC002 Austell popup B" toolName="1.5 FNST AC002 Austell popup B" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1378" name="1.5 FNST AC003 Avon popup A" toolName="1.5 FNST AC003 Avon popup A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1379" name="1.5 FNST AC003 Avon popup B" toolName="1.5 FNST AC003 Avon popup B" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1380" name="1.5 FNST DO003 Finch popup A" toolName="1.5 FNST DO003 Finch popup A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1381" name="1.5 FNST DO003 Finch popup B" toolName="1.5 FNST DO003 Finch popup B" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1382" name="2.1 FNST AC003 popup A" toolName="2.1 FNST AC003 popup A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1383" name="2.1 FNST AC003 popup B" toolName="2.1 FNST AC003 popup B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1384" name="2.1 FNST AC002 popup A" toolName="2.1 FNST AC002 popup A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1385" name="2.1 FNST AC002 popup B" toolName="2.1 FNST AC002 popup B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1386" name="2.1 FNST AC001 popup A" toolName="2.1 FNST AC001 popup A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1387" name="2.1 FNST AC001 popup B" toolName="2.1 FNST AC001 popup B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1388" name="2.1 FNST DO002 popup A" toolName="2.1 FNST DO002 popup A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1389" name="2.1 FNST DO002 popup B" toolName="2.1 FNST DO002 popup B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1390" name="2.1 FNST DO003 popup A" toolName="2.1 FNST DO003 popup A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1391" name="2.1 FNST DO003 popup B" toolName="2.1 FNST DO003 popup B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1633" name="1.1 FNST AC 001 Iron Ltd popup A" toolName="1.1 FNST AC 001 Iron Ltd popup A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1634" name="1.1 FNST AC 001 Iron Ltd popup B" toolName="1.1 FNST AC 001 Iron Ltd popup B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1646" name="1.1 FNST AC 002 Camel A" toolName="1.1 FNST AC 002 Camel A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1647" name="1.1 FNST AC 002 Camel B" toolName="1.1 FNST AC 002 Camel B" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1653" name="1.1 FNST AC 003 Trent A" toolName="1.1 FNST AC 003 Trent A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1654" name="1.1 FNST AC 003 Trent B" toolName="1.1 FNST AC 003 Trent B" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1641" name="1.1 FNST DO 001 Shelford Ltd popup A" toolName="1.1 FNST DO 001 Shelford Ltd popup A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1642" name="1.1 FNST DO 001 Shelford Ltd popup B" toolName="1.1 FNST DO 001 Shelford Ltd popup B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1637" name="1.5 FNST AC 001 Mole Ltd popup A" toolName="1.5 FNST AC 001 Mole Ltd popup A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1638" name="1.5 FNST AC 001 Madford Plc popup A" toolName="1.5 FNST AC 001 Madford Plc popup A" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1649" name="1.5 FNST AC 002 Chelt A" toolName="1.5 FNST AC 002 Chelt A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1650" name="1.5 FNST AC 002 Chelt B" toolName="1.5 FNST AC 002 Chelt B" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1655" name="1.5 FNST AC 003 Kinder A" toolName="1.5 FNST AC 003 Kinder A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1643" name="1.5 FNST DO 001 Rickling Ltd popup A" toolName="1.5 FNST DO 001 Rickling Ltd popup A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1644" name="1.5 FNST DO 001 Stebbing Ltd popup A" toolName="1.5 FNST DO 001 Stebbing Ltd popup A" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1661" name="2.1 FNST AC 001 Durham Ltd popup A" toolName="2.1 FNST AC 001 Durham Ltd popup A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1662" name="2.1 FNST AC 001 Durham Ltd popup B" toolName="2.1 FNST AC 001 Durham Ltd popup B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1665" name="2.1 FNST AC 002 Tweed A" toolName="2.1 FNST AC 002 Tweed A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1666" name="2.1 FNST AC 002 Tweed B" toolName="2.1 FNST AC 002 Tweed B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1667" name="2.1 FNST AC 003 Arun A" toolName="2.1 FNST AC 003 Arun A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1668" name="2.1 FNST AC 003 Arun B" toolName="2.1 FNST AC 003 Arun B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1663" name="2.1 FNST DO 001 Wembling A" toolName="2.1 FNST DO 001 Wembling A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1664" name="2.1 FNST DO 001 Wembling B" toolName="2.1 FNST DO 001 Wembling B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2356" name="1.5 FNST DO 010 pop up A" toolName="1.5 FNST DO 010 pop up A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2358" name="1.5 FNST DO 010 pop up B" toolName="1.5 FNST DO 010 pop up B" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2359" name="1.5 FNST DO 010 pop up C" toolName="1.5 FNST DO 010 pop up C" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2305" name="1.5 FNST DO 013 Platt Popup B" toolName="1.5 FNST DO 013 Platt Popup B" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2303" name="1.5 FNST DO 013 Platt Popup A" toolName="1.5 FNST DO 013 Platt Popup A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2249" name="1.5 FNST AC 010 Bywell Popup A" toolName="1.5 FNST AC 010 Bywell Popup A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2250" name="1.5 FNST AC 010 Bywell Popup B" toolName="1.5 FNST AC 010 Bywell Popup B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2322" name="1.5 FNST AC 011 Barton Popup B" toolName="1.5 FNST AC 011 Barton Popup B" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2320" name="1.5 FNST AC 011 Barton Popup A" toolName="1.5 FNST AC 011 Barton Popup A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2258" name="1.5 FNST AC 014 Lowick Popup A" toolName="1.5 FNST AC 014 Lowick Popup A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2257" name="1.5 FNST AC 014 Birtley Popup A" toolName="1.5 FNST AC 014 Birtley Popup A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2334" name="1.5 FNST AC 012 Pop up B" toolName="1.5 FNST AC 012 Pop up B" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2332" name="1.5 FNST AC 012 Pop up A" toolName="1.5 FNST AC 012 Pop up A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2288" name="1.5 FNST AC 013 Earle Popup A" toolName="1.5 FNST AC 013 Earle Popup A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2290" name="1.5 FNST AC 013 Earle Popup B" toolName="1.5 FNST AC 013 Earle Popup B" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2346" name="1.5 FNST DO 011 Hills Pop up A" toolName="1.5 FNST DO 011 Hills Pop up A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2348" name="1.5 FNST DO 011 Grant Pop up B" toolName="1.5 FNST DO 011 Grant Pop up B" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2372" name="1.5 FNST DO 012 Pop up a" toolName="1.5 FNST DO 012 Pop up a" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2374" name="1.5 FNST DO 012 Pop up B" toolName="1.5 FNST DO 012 Pop up B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2369" name="2.1 FNST DO 010 pop up A" toolName="2.1 FNST DO 010 pop up A" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2370" name="2.1 FNST DO 010 pop up B" toolName="2.1 FNST DO 010 pop up B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2251" name="2.1 FNST AC 010 Abberwick Popup A" toolName="2.1 FNST AC 010 Abberwick Popup A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2252" name="2.1 FNST AC 010 Abberwick Popup B" toolName="2.1 FNST AC 010 Abberwick Popup B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2307" name="2.1 FNST DO 013 Court Popup A" toolName="2.1 FNST DO 013 Court Popup A" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2309" name="2.1 FNST DO 013 Court Popup B" toolName="2.1 FNST DO 013 Court Popup B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2260" name="2.1 FNST AC 014 Belsay Popup B" toolName="2.1 FNST AC 014 Belsay Popup B" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2259" name="2.1 FNST AC 014 Belsay Popup A" toolName="2.1 FNST AC 014 Belsay Popup A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2326" name="2.1 FNST AC 011 Anick Popup B" toolName="2.1 FNST AC 011 Anick Popup B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2324" name="2.1 FNST AC 011 Anick Popup A" toolName="2.1 FNST AC 011 Anick Popup A" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2381" name="2.1 FNST DO 012 Pop up B" toolName="2.1 FNST DO 012 Pop up B" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2380" name="2.1 FNST DO 012 Pop up A" toolName="2.1 FNST DO 012 Pop up A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2352" name="2.1 FNST DO 011 Leigh Pop up B" toolName="2.1 FNST DO 011 Leigh Pop up B" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2351" name="2.1 FNST DO 011 Leigh Pop up A" toolName="2.1 FNST DO 011 Leigh Pop up A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2294" name="2.1 FNST AC 013 Lucker Popup B" toolName="2.1 FNST AC 013 Lucker Popup B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2292" name="2.1 FNST AC 013 Lucker Popup A" toolName="2.1 FNST AC 013 Lucker Popup A" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2330" name="2.1 FNST AC 012 Glanton Pop up B" toolName="2.1 FNST AC 012 Glanton Pop up B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2329" name="2.1 FNST AC 012 Glanton Pop up A" toolName="2.1 FNST AC 012 Glanton Pop up A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2394" name="1.5 FNST AC 003 Kinder B v2" toolName="1.5 FNST AC 003 Kinder B v2" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
    </tools>
    <section id="1" name="1" passLevelValue="60" passLevelType="1" itemsToMark="0" currentItem="" fixed="0" totalMark="90" userMark="0" userPercentage="0" passValue="1">
      <item id="879P1617" name="Copy of 1.1 FNST DO 001" totalMark="28" version="12" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="0" flagged="0" LO="1.1 Prepare a statement of comprehensive income/financial position/change in equity/cash flow" unit="Financial Statements" quT="20">
        <p um="0.785714285714286" cs="1" ua="1" id="879P1617">
          <s ua="0" um="-1" id="1">
            <c typ="4" id="5">
              <i cc="4" id="1" />
            </c>
            <c typ="4" id="6">
              <i cc="2" id="1" />
            </c>
          </s>
          <s ua="1" um="1" id="2">
            <c wei="12" maS="0" ie="1" typ="20" rv="0" mv="0" ua="1" um="1" id="1">
              <i um="1" ia="1" id="1">&lt;table extensionName="linkedproforma"&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Profit from operations"&gt;Profit from operations&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="7417"&gt;7417&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="0" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Depreciation"&gt;Depreciation&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="9245"&gt;9245&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Loss on disposal of PPE"&gt;Loss on disposal of PPE&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="120"&gt;120&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Adjustment in respect of inventories"&gt;Adjustment in respect of inventories&lt;/c&gt;&lt;c h="0" tp="1" mark="2" cor="-2559"&gt;-2559&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Adjustment in respect of trade receivables"&gt;Adjustment in respect of trade receivables&lt;/c&gt;&lt;c h="0" tp="1" mark="2" cor="2699"&gt;2699&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Adjustment in respect of trade payables"&gt;Adjustment in respect of trade payables&lt;/c&gt;&lt;c h="0" tp="1" mark="2" cor="4216"&gt;4216&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Tax paid"&gt;Interest paid&lt;/c&gt;&lt;c h="0" tp="1" mark="2" cor="-1098"&gt;-756&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Interest paid"&gt;Tax paid&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="-756"&gt;-1098&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;/table&gt;</i>
            </c>
          </s>
          <s ua="1" um="0.63" id="3">
            <c wei="2" maS="0" ie="1" typ="20" rv="0" mv="0" ua="1" um="1" id="2">
              <i um="1" ia="1" id="1">&lt;table extensionName="linkedproforma"&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Carrying amount of PPE sold"&gt;Carrying amount of PPE sold&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="456"&gt;456&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Loss on disposal"&gt;Loss on disposal&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="-120"&gt;-120&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;/table&gt;</i>
            </c>
            <c wei="5" maS="0" ie="1" typ="20" rv="0" mv="0" ua="1" um="0.8" id="3">
              <i um="0.8" ia="1" id="1">&lt;table extensionName="linkedproforma"&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="57124"&gt;57124&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Depreciation charge"&gt;Depreciation charge&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="-9245"&gt;-9245&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Carrying amount of PPE sold"&gt;Loss on disposal of PPE&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="-456"&gt;-120&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Revaluation"&gt;Revaluation&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="3000"&gt;3000&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="PPE at end of year"&gt;PPE at end of year&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="-75356"&gt;-75356&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;/table&gt;</i>
            </c>
            <c wei="16" maS="0" ie="1" typ="20" rv="0" mv="0" ua="1" um="0.63" id="1">
              <i um="0.625" ia="1" id="1">&lt;table extensionName="linkedproforma"&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="19284"&gt;19284&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="0" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Proceeds on disposal of PPE"&gt;Purchases of PPE&lt;/c&gt;&lt;c h="0" tp="1" mark="2" cor="336�[s3c2i1c2r1]+[s3c2i1c2r2]"&gt;-24597&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Purchases of PPE"&gt;Proceeds on disposal of PPE&lt;/c&gt;&lt;c h="0" tp="1" mark="5" cor="-24933�[s3c3i1c2r1]+[s3c3i1c2r2]+[s3c3i1c2r3]+[s3c3i1c2r4]"&gt;336&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="0" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Bank loans repaid"&gt;Proceeds of share issue&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="-250"&gt;7730&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Proceeds of share issue"&gt;Bank loans repaid&lt;/c&gt;&lt;c h="0" tp="1" mark="2" cor="7730"&gt;3850&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Dividends paid"&gt;Dividends paid&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="-710"&gt;710&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="2" cor="1457"&gt;7313&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="-560"&gt;-560&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="897"&gt;897&lt;/c&gt;&lt;/r&gt;&lt;/table&gt;</i>
            </c>
          </s>
        </p>
      </item>
      <item id="879P1624" name="Copy of 1.2 FNST DO 001" totalMark="8" version="8" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="0" flagged="0" LO="1.2 Prepare a statement of comprehensive income/financial position/change in equity/cash flow" unit="Financial Statements" quT="20">
        <p um="1" cs="1" ua="1" id="879P1624">
          <s ua="0" um="-1" id="1">
            <c typ="4" id="5">
              <i cc="1" id="1" />
            </c>
            <c typ="4" id="6">
              <i cc="1" id="1" />
            </c>
          </s>
          <s ua="1" um="1" id="2">
            <c wei="8" maS="0" ie="1" typ="20" rv="0" mv="0" ua="1" um="1" id="1">
              <i um="1" ia="1" id="1">&lt;table&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="15500"&gt;15500&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="6420"&gt;6420&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="36478"&gt;36478&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="0" cor=""&gt;0&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="3000"&gt;3000&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="5420"&gt;5420&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="0" cor=""&gt;0&lt;/c&gt;&lt;c h="0" tp="1" mark="0" cor=""&gt;0&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="-710"&gt;-710&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="2500"&gt;2500&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="5230"&gt;5230&lt;/c&gt;&lt;c h="0" tp="1" mark="0" cor=""&gt;0&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;/table&gt;</i>
            </c>
          </s>
        </p>
      </item>
      <item id="879P1698" name="1.3 FNST AC 010" totalMark="12" version="23" markingType="1" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="0" flagged="0" LO="1.3 Knowledge and understanding of International Financial Reporting Standards (written)" unit="Financial Statements" quT="11">
        <p um="0" cs="1" ua="1" id="879P1698">
          <s ua="1" um="0" id="2">
            <c wei="12" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="1">
              <i ca="" id="1">Property, plant and equipment ar#!##!#the cost of an item of property, plant and equipment should be recognised as an asset if;#!#-it is probable that the asset will generate future economic benefits attributable to the entity#!#-its cost can be measured realibly#!##!#the cost of an item of property, plant and equipment include the cost of purchase,cost of conversion and any cost attributed to bring the property to condition and location.#!##!#The total cost that must be recognised as property, plant and equipment is 32010(30000+650+870+490)</i>
            </c>
          </s>
        </p>
      </item>
      <item id="879P1703" name="1.4 FNST AC 010" totalMark="12" version="18" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="0" flagged="0" LO="1.4 Application of reporting standards" unit="Financial Statements" quT="10">
        <p um="0.333333333333333" cs="1" ua="1" id="879P1703">
          <s ua="1" um="0" id="1">
            <c wei="2" maS="1" ie="1" typ="10" rv="0" mv="0" ua="1" um="0" id="15">
              <i ac="A" sl="0" id="1" />
              <i ac="B" sl="1" id="2" />
              <i ca="1" ac="C" sl="0" id="3" />
              <i ac="D" sl="0" id="4" />
            </c>
          </s>
          <s ua="1" um="0" id="2">
            <c wei="2" maS="1" ie="1" typ="10" rv="0" mv="0" ua="1" um="0" id="5">
              <i ca="1" ac="A" sl="0" id="1" />
              <i ac="B" sl="0" id="2" />
              <i ac="C" sl="0" id="3" />
              <i ac="D" sl="1" id="4" />
            </c>
          </s>
          <s ua="1" um="0" id="3">
            <c wei="2" maS="1" ie="1" typ="10" rv="0" mv="0" ua="1" um="0" id="18">
              <i ac="A" sl="0" id="1" />
              <i ac="B" sl="0" id="2" />
              <i ac="C" sl="1" id="3" />
              <i ca="1" ac="D" sl="0" id="4" />
            </c>
          </s>
          <s ua="1" um="1" id="6">
            <c wei="2" maS="1" ie="1" typ="10" rv="0" mv="0" ua="1" um="1" id="5">
              <i ca="1" ac="A" sl="1" id="1" />
              <i ac="B" sl="0" id="2" />
            </c>
          </s>
          <s ua="1" um="1" id="7">
            <c wei="2" maS="1" ie="1" typ="10" rv="0" mv="0" ua="1" um="1" id="13">
              <i ac="A" sl="0" id="1" />
              <i ca="1" ac="B" sl="1" id="2" />
              <i ac="C" sl="0" id="3" />
              <i ac="D" sl="0" id="4" />
            </c>
          </s>
          <s ua="1" um="0" id="8">
            <c wei="2" maS="1" ie="1" typ="10" rv="0" mv="0" ua="1" um="0" id="3">
              <i ac="A" sl="0" id="1" />
              <i ac="B" sl="0" id="2" />
              <i ca="1" ac="C" sl="0" id="3" />
              <i ac="D" sl="1" id="4" />
            </c>
          </s>
        </p>
      </item>
      <item id="879P1697" name="1.5 FNST AC 010 " totalMark="30" version="23" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="0" flagged="0" LO="1.5 Drafting consolidated financial statements" unit="Financial Statements" quT="20">
        <p um="0.866666666666667" cs="1" ua="1" id="879P1697">
          <s ua="0" um="-1" id="1">
            <c typ="4" id="13">
              <i cc="3" id="1" />
            </c>
            <c typ="4" id="14">
              <i cc="3" id="1" />
            </c>
          </s>
          <s ua="1" um="0.87" id="2">
            <c wei="16" maS="0" ie="1" typ="20" rv="0" mv="0" ua="1" um="0.88" id="2">
              <i um="0.875" ia="1" id="1">&lt;table extensionName="linkedproforma"&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="0" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="0" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="5000"&gt;5000&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="1000"&gt;360&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="5" cor="1670�[s2c5i1c2r1]+[s2c5i1c2r2]+[s2c5i1c2r3]"&gt;1830&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="7" cor="192�[s2c4i1c2r1]+[s2c4i1c2r2]+[s2c4i1c2r3]+[s2c4i1c2r4]+[s2c4i1c2r5]"&gt;192&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="1407"&gt;1407&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="321"&gt;321&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;/table&gt;</i>
            </c>
            <c wei="5" maS="0" ie="1" typ="20" rv="0" mv="0" ua="1" um="0.8" id="5">
              <i um="0.8" ia="1" id="1">&lt;table extensionName="linkedproforma"&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Ancroft Plc (excluding any inter-company adjustment)"&gt;Ancroft Plc (excluding any inter-company adjustment)&lt;/c&gt;&lt;c h="0" tp="1" mark="2" cor="1639"&gt;1639&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Bywell Ltd - attributable to Ancroft Plc"&gt;Bywell Ltd - attributable to Ancroft Plc&lt;/c&gt;&lt;c h="0" tp="1" mark="2" cor="45"&gt;45&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Inter-company adjustment "&gt;Inter-company adjustment &lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="-14"&gt;-146&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;/table&gt;</i>
            </c>
            <c wei="7" maS="0" ie="1" typ="20" rv="0" mv="0" ua="1" um="1" id="4">
              <i um="1" ia="1" id="1">&lt;table extensionName="linkedproforma"&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Share capital - attributable to NCI"&gt;Share capital - attributable to NCI&lt;/c&gt;&lt;c h="0" tp="1" mark="2" cor="90"&gt;90&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Share premium - attributable to NCI"&gt;Share premium - attributable to NCI&lt;/c&gt;&lt;c h="0" tp="1" mark="2" cor="40"&gt;40&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Revaluation reserve - attributable to NCI"&gt;Retained earnings - attributable to NCI&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="30"&gt;32&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Retained earnings - attributable to NCI"&gt;Revaluation reserve - attributable to NCI&lt;/c&gt;&lt;c h="0" tp="1" mark="2" cor="32"&gt;30&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;/table&gt;</i>
            </c>
            <c wei="8" maS="0" ie="1" typ="20" rv="0" mv="0" ua="1" um="1" id="3">
              <i um="1" ia="1" id="1">&lt;table extensionName="linkedproforma"&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Share capital - attributable to Ancroft Plc"&gt;Price paid&lt;/c&gt;&lt;c h="0" tp="1" mark="2" cor="-810"&gt;2200&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Share premium - attributable to Ancroft Plc"&gt;Share capital - attributable to Ancroft Plc&lt;/c&gt;&lt;c h="0" tp="1" mark="2" cor="-360"&gt;-810&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Revaluation reserve - attributable to Ancroft Plc"&gt;Share premium - attributable to Ancroft Plc&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="-270"&gt;-360&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Retained earnings - attributable to Ancroft Plc"&gt;Retained earnings - attributable to Ancroft Plc&lt;/c&gt;&lt;c h="0" tp="1" mark="2" cor="-243"&gt;-243&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Price paid"&gt;Revaluation reserve - attributable to Ancroft Plc&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="2200"&gt;-270&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;/table&gt;</i>
            </c>
            <c wei="14" maS="0" ie="1" typ="20" rv="0" mv="0" ua="1" um="0.86" id="16">
              <i um="0.857142857142857" ia="1" id="1">&lt;table extensionName="linkedproforma"&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="0" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="0" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="8" cor="[s2c3i1c2r1]+[s2c3i1c2r2]+[s2c3i1c2r3]+[s2c3i1c2r4]+[s2c3i1c2r5]�517"&gt;517&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="2" cor="7695"&gt;7695&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="0" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="2" cor="771"&gt;785&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="442"&gt;442&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="165"&gt;165&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;/table&gt;</i>
            </c>
          </s>
        </p>
      </item>
    </section>
    <section id="2" name="2" passLevelValue="60" passLevelType="1" itemsToMark="0" currentItem="" fixed="0" totalMark="60" userMark="0" userPercentage="0" passValue="0">
      <item id="879P1799" name="2.1 FNST DO 011" totalMark="32" version="22" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="0" flagged="0" LO="2.1 Analysis of financial statements using ratios" unit="Financial Statements" quT="11,12">
        <p um="0.6875" cs="1" ua="1" id="879P1799">
          <s ua="0" um="-1" id="1">
            <c typ="4" id="10">
              <i cc="1" id="1" />
            </c>
            <c typ="4" id="11">
              <i cc="1" id="1" />
            </c>
          </s>
          <s ua="1" um="0.69" id="3">
            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="47">
              <i ca="62.2�62.3" id="1">62</i>
            </c>
            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="48">
              <i ca="1.4�1.5" id="1">1.4</i>
            </c>
            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="49">
              <i ca="12.8�12.9" id="1">12.8</i>
            </c>
            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="50">
              <i ca="34.8�34.9�0.30�0.40" id="1">52.0</i>
            </c>
            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="51">
              <i ca="5.8�5.9" id="1">0.2</i>
            </c>
            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="52">
              <i ca="10.9�11.0" id="1">10.9</i>
            </c>
            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="53">
              <i ca="20.4�20.5" id="1">20.4</i>
            </c>
            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="54">
              <i ca="1.8�1.9" id="1">1.8</i>
            </c>
            <c wei="2" maS="0" ie="1" typ="12" rv="0" mv="0" ua="1" um="0" id="26">
              <i sl="5" id="1" />
            </c>
            <c wei="2" maS="0" ie="1" typ="12" rv="0" mv="0" ua="1" um="1" id="29">
              <i sl="4" id="1" />
            </c>
            <c wei="2" maS="0" ie="1" typ="12" rv="0" mv="0" ua="1" um="1" id="30">
              <i sl="2" id="1" />
            </c>
            <c wei="2" maS="0" ie="1" typ="12" rv="0" mv="0" ua="1" um="1" id="31">
              <i sl="11" id="1" />
            </c>
            <c wei="2" maS="0" ie="1" typ="12" rv="0" mv="0" ua="1" um="1" id="27">
              <i sl="4" id="1" />
            </c>
            <c wei="2" maS="0" ie="1" typ="12" rv="0" mv="0" ua="1" um="1" id="33">
              <i sl="5" id="1" />
            </c>
            <c wei="2" maS="0" ie="1" typ="12" rv="0" mv="0" ua="1" um="0" id="34">
              <i sl="4" id="1" />
            </c>
            <c wei="2" maS="0" ie="1" typ="12" rv="0" mv="0" ua="1" um="1" id="35">
              <i sl="2" id="1" />
            </c>
          </s>
        </p>
      </item>
      <item id="879P2089" name="2.2 FNST AC 002 2011" totalMark="18" version="26" markingType="1" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="0" flagged="0" LO="2.2 Interpretation of financial statements using ratios (written)" unit="Financial Statements" quT="11">
        <p um="0" cs="1" ua="1" id="879P2089">
          <s ua="1" um="0" id="2">
            <c wei="18" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="1">
              <i ca="" id="1">Gearing#!#the gearing ratio have fallen.This might be due to the fact that the entity borrowed loans and keeps overdrafting while there is no enough profits to cover the interest or repayments of the loans#!##!#Acid test ratio#!#the acid test ratio have improved.this might be due to increased current asset or decrease in liabilities as the operating activities seems to be good.#!##!#working capital#!#the working capital of the entity have reduced by15days.the might be due to the fact that the entity have too much loans and interest to pay so it reduced the receivable holding period and increased the payable holding period.#!##!#the bank should terminate its overdraft because the entity have more loans and interest to pay.And the entity have no enough profits to pay.#!##!#if working capital cycle is reduced</i>
            </c>
          </s>
        </p>
      </item>
      <item id="879P1523" name="2.3 FNST AC 005" totalMark="10" version="15" markingType="1" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="0" flagged="0" LO="2.3 Legal and regulatory framework (written)" unit="Financial Statements" quT="11,20">
        <p um="0" cs="1" ua="1" id="879P1523">
          <s ua="1" um="0" id="2">
            <c wei="10" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="1">
              <i ca="" id="1">The  primary users of general purpose financial reports are;#!#potential investors and existing investors-these people might be interested in the information provided by reports to see if the entity have ability to generate future economic benefits.They also want to see if the entity have the ability to to generate profits so that they can invest in the entity#!##!#potential lenders and existing lenders#!#interested in checking if the entity have the ability to generate enough profits in the future for it to repay its loans and interest.As for the existing lenders want to see if the entity is still able to generate profit and  able to repay the loans when due.#!##!#suppliers#!#suppliers are interest in seeing if the entity is able to repay its credit or balances.The want to see if the entiity still and will generate profits in future or if the entity will go in to liquidation.#!##!#customers#!#customers want to see if the entity will continue to operate in future.#!##!#the information that should be provided in general purpose financial reports are;#!#statement of cash flows-to show the entity's cash inflow and outflow of the entity#!#statement of comprehensive income-to show the entity's perfomance and be able to compare the previous profits with the current one#!#statement of financial position-to show the enitity position related to assets aquierd and liabilty assumed#!#</i>
            </c>
            <c wei="1" maS="0" ie="1" typ="20" rv="0" mv="0" ua="0" um="0" id="2">
              <i um="0" id="1" />
            </c>
          </s>
        </p>
      </item>
    </section>
  </assessment>
  <isValid>1</isValid>
  <isPrintable>0</isPrintable>
  <qualityReview>0</qualityReview>
  <numberOfGenerations>1</numberOfGenerations>
  <requiresInvigilation>1</requiresInvigilation>
  <advanceContentDownloadTimespan>60</advanceContentDownloadTimespan>
  <automaticVerification>1</automaticVerification>
  <offlineMode>2</offlineMode>
  <requiresValidation>0</requiresValidation>
  <requiresSecureClient>1</requiresSecureClient>
  <examType>0</examType>
  <advanceDownload>0</advanceDownload>
  <gradeBoundaryData>
    <gradeBoundaries passLevelType="1" adjustedGradeBoundaries="0">
      <grade modifier="lt" value="70" description="Fail" />
      <grade modifier="gt" value="70" description="Pass" />
    </gradeBoundaries>
  </gradeBoundaryData>
  <autoViewExam>0</autoViewExam>
  <autoProgressItems>0</autoProgressItems>
  <confirmationText>
    <confirmationText />
  </confirmationText>
  <requiresConfirmationCheck>0</requiresConfirmationCheck>
  <allowCloseWithoutSubmit>0</allowCloseWithoutSubmit>
  <useSecureMarker>1</useSecureMarker>
  <annotationVersion>1</annotationVersion>
  <allowPackagingDelivery>1</allowPackagingDelivery>
  <scoreBoundaryData>
    <scoreBoundaries showScoreBoundaryColumn="1">
      <score modifier="lt" value="36" description="Significantly below" higherBoundarySet="1" />
      <score modifier="lt" value="36" description="Significantly below" higherBoundarySet="0" />
      <score modifier="gt" value="36" description="Below requirement" higherBoundarySet="1" />
      <score modifier="gt" value="60" description="Borderline" higherBoundarySet="1" />
      <score modifier="gt" value="70" description="Met" higherBoundarySet="0" />
      <score modifier="gt" value="70" description="Met" higherBoundarySet="1" />
      <score modifier="gt" value="36" description="Below requirement" higherBoundarySet="0" userCreated="1" />
      <score modifier="gt" value="60" description="Borderline" higherBoundarySet="0" userCreated="1" />
      <score modifier="gt" value="85" description="Exceeded" higherBoundarySet="1" userCreated="1" />
    </scoreBoundaries>
  </scoreBoundaryData>
  <showCandidateReportResultColumn>1</showCandidateReportResultColumn>
  <showCandidateReportPercentageColumn>1</showCandidateReportPercentageColumn>
  <certifiedAccessible>0</certifiedAccessible>
  <markingProgressVisible>1</markingProgressVisible>
  <markingProgressMode>0</markingProgressMode>
  <secureClientOperationMode>1</secureClientOperationMode>
  <examDeliverySystemStyleType>delivery</examDeliverySystemStyleType>
  <markingAutoVoidPeriod>365</markingAutoVoidPeriod>
  <showPageRequiresScrollingAlert>0</showPageRequiresScrollingAlert>
  <project ID="879" />
</assessmentDetails>'
	where id = @esid;

--Get the totalMark of the exam--
select
	sum(s.i.value('@totalMark','int')) as sumTotalMark
from WAREHOUSE_ExamSessionTable 

cross apply resultData.nodes('/exam/section/item') as s(i)

where id = @esid;


select StructureXML, resultData, resultDataFull from WAREHOUSE_ExamSessionTable where id = @esid;

--WEST.ResultData--

	--SectionLevel--

	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/@userMark)[1] with sql:variable("@NewValue") ')    
	where id = @esid;

	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid

	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/@totalMark)[1] with sql:variable("@totalMark") ')    
	where id = @esid;

	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/@userPercentage)[1] with sql:variable("@NewValue") ')    
	where id = @esid;
	
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid

	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/@userMark)[1] with sql:variable("@NewValue") ')    
	where id = @esid;

	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid

	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/@userPercentage)[1] with sql:variable("@NewValue") ')    
	where id = @esid;
	
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid

	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/@totalMark)[1] with sql:variable("@totalMark") ')    
	where id = @esid;
	--ItemLevel--
	
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@markerUserMark)[1] with sql:variable("@NewValue") ')    
	where id = @esid;
	
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@markerUserMark)[2] with sql:variable("@NewValue") ')    
	where id = @esid;
		
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@markerUserMark)[3] with sql:variable("@NewValue") ')    
	where id = @esid;
	
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@markerUserMark)[4] with sql:variable("@NewValue") ')    
	where id = @esid;	
	
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@markerUserMark)[5] with sql:variable("@NewValue") ')    
	where id = @esid;
	
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@markerUserMark)[6] with sql:variable("@NewValue") ')    
	where id = @esid;
		
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@markerUserMark)[7] with sql:variable("@NewValue") ')    
	where id = @esid;
	
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@markerUserMark)[8] with sql:variable("@NewValue") ')    
	where id = @esid;
		
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@markerUserMark)[9] with sql:variable("@NewValue") ')    
	where id = @esid;
	
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@markerUserMark)[10] with sql:variable("@NewValue") ')    
	where id = @esid;	
	
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@actualUserMark)[1] with sql:variable("@NewValue") ')    
	where id = @esid;
		
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@actualUserMark)[2] with sql:variable("@NewValue") ')    
	where id = @esid;
	
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@actualUserMark)[3] with sql:variable("@NewValue") ')    
	where id = @esid;
		
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@actualUserMark)[4] with sql:variable("@NewValue") ')    
	where id = @esid;
		
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@actualUserMark)[5] with sql:variable("@NewValue") ')    
	where id = @esid;
		
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@actualUserMark)[6] with sql:variable("@NewValue") ')    
	where id = @esid;
		
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@actualUserMark)[7] with sql:variable("@NewValue") ')    
	where id = @esid;
		
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@actualUserMark)[8] with sql:variable("@NewValue") ')    
	where id = @esid;
		
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
		
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@actualUserMark)[9] with sql:variable("@NewValue") ')    
	where id = @esid;
		
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@actualUserMark)[10] with sql:variable("@NewValue") ')    
	where id = @esid;
	
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@userMark)[1] with sql:variable("@NewValue") ')    
	where id = @esid;
		
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@userMark)[9] with sql:variable("@NewValue") ')    
	where id = @esid;
		
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@userMark)[10] with sql:variable("@NewValue") ')    
	where id = @esid;	
	
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid

	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@userMark)[2] with sql:variable("@NewValue") ')    
	where id = @esid;
		
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@userMark)[3] with sql:variable("@NewValue") ')    
	where id = @esid;
		
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@userMark)[4] with sql:variable("@NewValue") ')    
	where id = @esid;
		
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@userMark)[5] with sql:variable("@NewValue") ')    
	where id = @esid;
		
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@userMark)[6] with sql:variable("@NewValue") ')    
	where id = @esid;
		
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@userMark)[7] with sql:variable("@NewValue") ')    
	where id = @esid;
		
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@userMark)[8] with sql:variable("@NewValue") ')    
	where id = @esid;

	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@viewingTime)[1] with sql:variable("@NewValue") ')    
	where id = @esid;
		
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@viewingTime)[2] with sql:variable("@NewValue") ')    
	where id = @esid;
	
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@viewingTime)[3] with sql:variable("@NewValue") ')    
	where id = @esid;
		
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@viewingTime)[4] with sql:variable("@NewValue") ')    
	where id = @esid;
		
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@viewingTime)[5] with sql:variable("@NewValue") ')    
	where id = @esid;
		
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@viewingTime)[6] with sql:variable("@NewValue") ')    
	where id = @esid;
		
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@viewingTime)[7] with sql:variable("@NewValue") ')    
	where id = @esid;
		
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@viewingTime)[8] with sql:variable("@NewValue") ')    
	where id = @esid;
		
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@viewingTime)[9] with sql:variable("@NewValue") ')    
	where id = @esid;
		
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@viewingTime)[10] with sql:variable("@NewValue") ')    
	where id = @esid;
	
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@userAttempted)[1] with sql:variable("@NewValue") ')    
	where id = @esid;
	
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@userAttempted)[2] with sql:variable("@NewValue") ')    
	where id = @esid;
		
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@userAttempted)[3] with sql:variable("@NewValue") ')    
	where id = @esid;
	
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@userAttempted)[4] with sql:variable("@NewValue") ')    
	where id = @esid;
		
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@userAttempted)[5] with sql:variable("@NewValue") ')    
	where id = @esid;
	
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@userAttempted)[6] with sql:variable("@NewValue") ')    
	where id = @esid;
		
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@userAttempted)[7] with sql:variable("@NewValue") ')    
	where id = @esid;
	
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@userAttempted)[8] with sql:variable("@NewValue") ')    
	where id = @esid;
		
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@userAttempted)[9] with sql:variable("@NewValue") ')    
	where id = @esid;
	
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@userAttempted)[10] with sql:variable("@NewValue") ')    
	where id = @esid;

--WEST.ResultDataFull--

	--SectionLevel--
	
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid

	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/@totalMark)[1] with sql:variable("@totalMark") ')    
	where id = @esid;
	
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid

	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/@totalMark)[1] with sql:variable("@totalMark") ')    
	where id = @esid;
	
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/@userMark)[1] with sql:variable("@NewValue") ')    
	where id = @esid;
	
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/@userPercentage)[1] with sql:variable("@NewValue") ')    
	where id = @esid;
	
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/@userMark)[1] with sql:variable("@NewValue") ')    
	where id = @esid;
	
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid

	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/@userPercentage)[1] with sql:variable("@NewValue") ')    
	where id = @esid;

	--ItemLevel--
	--UserMark--
	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@userMark)[1] with sql:variable("@NewValue") ')    
	where id = @esid;
	
	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@userMark)[2] with sql:variable("@NewValue") ')    
	where id = @esid;

	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@userMark)[3] with sql:variable("@NewValue") ')    
	where id = @esid;
	
	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@userMark)[4] with sql:variable("@NewValue") ')    
	where id = @esid;

	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@userMark)[5] with sql:variable("@NewValue") ')    
	where id = @esid;
	
	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@userMark)[6] with sql:variable("@NewValue") ')    
	where id = @esid;

	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@userMark)[7] with sql:variable("@NewValue") ')    
	where id = @esid;
	
	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@userMark)[8] with sql:variable("@NewValue") ')    
	where id = @esid;

	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@userMark)[9] with sql:variable("@NewValue") ')    
	where id = @esid;
	
	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@userMark)[10] with sql:variable("@NewValue") ')    
	where id = @esid;

	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@userAttempted)[1] with sql:variable("@NewValue") ')    
	where id = @esid;
	
	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@userAttempted)[2] with sql:variable("@NewValue") ')    
	where id = @esid;
	
	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@userAttempted)[3] with sql:variable("@NewValue") ')    
	where id = @esid;
	
	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@userAttempted)[4] with sql:variable("@NewValue") ')    
	where id = @esid;
	
	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@userAttempted)[5] with sql:variable("@NewValue") ')    
	where id = @esid;
	
	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@userAttempted)[6] with sql:variable("@NewValue") ')    
	where id = @esid;
	
	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@userAttempted)[7] with sql:variable("@NewValue") ')    
	where id = @esid;
	
	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@userAttempted)[8] with sql:variable("@NewValue") ')    
	where id = @esid;
	
	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@userAttempted)[9] with sql:variable("@NewValue") ')    
	where id = @esid;
	
	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@userAttempted)[10] with sql:variable("@NewValue") ')    
	where id = @esid;

	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@viewingTime)[1] with sql:variable("@NewValue") ')    
	where id = @esid;
	
	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@viewingTime)[2] with sql:variable("@NewValue") ')    
	where id = @esid;
	
	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@viewingTime)[3] with sql:variable("@NewValue") ')    
	where id = @esid;

	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@viewingTime)[4] with sql:variable("@NewValue") ')    
	where id = @esid;
	
	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@viewingTime)[5] with sql:variable("@NewValue") ')    
	where id = @esid;
	
	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@viewingTime)[6] with sql:variable("@NewValue") ')    
	where id = @esid;
	
	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@viewingTime)[7] with sql:variable("@NewValue") ')    
	where id = @esid;
	
	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@viewingTime)[8] with sql:variable("@NewValue") ')    
	where id = @esid;
	
	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@viewingTime)[9] with sql:variable("@NewValue") ')    
	where id = @esid;
	
	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@viewingTime)[10] with sql:variable("@NewValue") ')    
	where id = @esid;

	update WAREHOUSE_ExamSessionTable
	set StructureXML = '<assessmentDetails>
  <assessmentID>1767</assessmentID>
  <qualificationID>87</qualificationID>
  <qualificationName>Financial performance (AQ2013)</qualificationName>
  <qualificationReference>AATQCF</qualificationReference>
  <assessmentGroupName>FPFM (AQ2013)</assessmentGroupName>
  <assessmentGroupID>1665</assessmentGroupID>
  <assessmentGroupReference>CAFPFM</assessmentGroupReference>
  <assessmentName>Financial performance (AQ 2013)</assessmentName>
  <validFromDate>01 Oct 2013</validFromDate>
  <expiryDate>31 Dec 2016</expiryDate>
  <startTime>00:00:00</startTime>
  <endTime>23:59:59</endTime>
  <duration>150</duration>
  <defaultDuration>150</defaultDuration>
  <scheduledDuration>
    <value>150</value>
    <reason></reason>
  </scheduledDuration>
  <sRBonus>0</sRBonus>
  <sRBonusMaximum>50</sRBonusMaximum>
  <externalReference>CBTAAT</externalReference>
  <passLevelValue>70</passLevelValue>
  <passLevelType>1</passLevelType>
  <status>2</status>
  <testFeedbackType>
    <passFail>0</passFail>
    <percentageMark>0</percentageMark>
    <allowSummaryFeedback>0</allowSummaryFeedback>
    <summaryFeedbackType>1</summaryFeedbackType>
    <itemSummary>0</itemSummary>
    <itemReview>0</itemReview>
    <itemReviewCorrectAnswer>0</itemReviewCorrectAnswer>
    <itemFeedback>0</itemFeedback>
    <printableSummary>0</printableSummary>
    <candidateDetails>0</candidateDetails>
    <feedbackByReference>0</feedbackByReference>
    <allowFeedbackDuringExam>0</allowFeedbackDuringExam>
    <allowFeedbackDuringSummary>0</allowFeedbackDuringSummary>
  </testFeedbackType>
  <itemFeedback>0</itemFeedback>
  <allowCalculator>0</allowCalculator>
  <lastInUseDate>16 Sep 2013 12:37:17</lastInUseDate>
  <completedScriptReview>0</completedScriptReview>
  <assessment currentSection="1" totalTime="0" currentTime="0">
    <intro id="0" name="Introduction" currentItem="">
      <item id="911P2197" name="Introduction" totalMark="1" version="13" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" LO="Introduction" unit="Financial Performance" />
    </intro>
    <outro id="0" name="Finish" currentItem="" />
    <tools id="0" name="Tools" currentItem="" />
    <section id="1" name="1" passLevelValue="70" passLevelType="1" itemsToMark="0" currentItem="911P2162" fixed="0">
      <item id="911P2098" name="01 FPFM CF010" totalMark="12" version="17" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="0" flagged="0" LO="01. Identification of costing information" unit="Financial Performance" quT="11" />
      <item id="911P2108" name="02 FPFM CF010" totalMark="16" version="22" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="0" flagged="0" LO="02. Direct Materials, Labour and variable overhead variances" unit="Financial Performance" quT="11,12" />
      <item id="911P2115" name="03 FPFM CF007" totalMark="16" version="16" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="0" flagged="0" LO="03. Fixed overhead variances" unit="Financial Performance" quT="11,12" />
      <item id="911P2127" name="04 FPFM CF009" totalMark="12" version="15" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="0" flagged="0" LO="04. Operating statement" unit="Financial Performance" quT="20" />
      <item id="911P2132" name="05 FPFM CF004" totalMark="12" version="21" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="0" flagged="0" LO="05. Statistical information" unit="Financial Performance" quT="10,11,12" />
      <item id="911P2194" name="06 FPFM CF010" totalMark="22" version="25" markingType="1" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="0" flagged="0" LO="06. Variance analysis" unit="Financial Performance" quT="11" />
      <item id="911P2158" name="07 FPFM CF008" totalMark="20" version="24" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="0" flagged="0" LO="07. Calculation of performance indicators" unit="Financial Performance" quT="11" />
      <item id="911P2162" name="08 FPFM CF006" totalMark="12" version="26" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="0" flagged="0" LO="08. Decision making" unit="Financial Performance" quT="10,11,20" />
      <item id="911P2141" name="09 FPFM CF002" totalMark="12" version="25" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="0" flagged="0" LO="09. Cost management techniques" unit="Financial Performance" quT="11" />
      <item id="911P2224" name="10 FPFM CF009 v2" totalMark="22" version="16" markingType="1" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="0" flagged="0" LO="10. Report on performance indicators and scenario planning (what if analysis)" unit="Financial Performance" quT="11" />
    </section>
  </assessment>
  <isValid>1</isValid>
  <isPrintable>0</isPrintable>
  <qualityReview>0</qualityReview>
  <numberOfGenerations>1</numberOfGenerations>
  <requiresInvigilation>1</requiresInvigilation>
  <advanceContentDownloadTimespan>60</advanceContentDownloadTimespan>
  <automaticVerification>1</automaticVerification>
  <offlineMode>2</offlineMode>
  <requiresValidation>0</requiresValidation>
  <requiresSecureClient>1</requiresSecureClient>
  <examType>0</examType>
  <advanceDownload>0</advanceDownload>
  <gradeBoundaryData>
    <gradeBoundaries passLevelType="1" adjustedGradeBoundaries="0">
      <grade modifier="lt" value="70" description="Fail" />
      <grade modifier="gt" value="70" description="Pass" />
    </gradeBoundaries>
  </gradeBoundaryData>
  <autoViewExam>0</autoViewExam>
  <autoProgressItems>0</autoProgressItems>
  <confirmationText>
    <confirmationText></confirmationText>
  </confirmationText>
  <requiresConfirmationCheck>0</requiresConfirmationCheck>
  <allowCloseWithoutSubmit>0</allowCloseWithoutSubmit>
  <useSecureMarker>1</useSecureMarker>
  <annotationVersion>1</annotationVersion>
  <allowPackagingDelivery>1</allowPackagingDelivery>
  <scoreBoundaryData>
    <scoreBoundaries showScoreBoundaryColumn="1">
      <score modifier="lt" value="36" description="Significantly below" higherBoundarySet="1" />
      <score modifier="lt" value="36" description="Significantly below" higherBoundarySet="0" />
      <score modifier="gt" value="36" description="Below requirement" higherBoundarySet="1" />
      <score modifier="gt" value="60" description="Borderline" higherBoundarySet="1" />
      <score modifier="gt" value="70" description="Met" higherBoundarySet="0" />
      <score modifier="gt" value="70" description="Met" higherBoundarySet="1" />
      <score modifier="gt" value="36" description="Below requirement" higherBoundarySet="0" userCreated="1" />
      <score modifier="gt" value="60" description="Borderline" higherBoundarySet="0" userCreated="1" />
      <score modifier="gt" value="85" description="Exceeded" higherBoundarySet="1" userCreated="1" />
    </scoreBoundaries>
  </scoreBoundaryData>
  <showCandidateReportResultColumn>0</showCandidateReportResultColumn>
  <showCandidateReportPercentageColumn>1</showCandidateReportPercentageColumn>
  <certifiedAccessible>0</certifiedAccessible>
  <markingProgressVisible>1</markingProgressVisible>
  <markingProgressMode>0</markingProgressMode>
  <secureClientOperationMode>1</secureClientOperationMode>
  <examDeliverySystemStyleType>delivery</examDeliverySystemStyleType>
  <markingAutoVoidPeriod>365</markingAutoVoidPeriod>
  <showPageRequiresScrollingAlert>0</showPageRequiresScrollingAlert>
  <project ID="911" />
</assessmentDetails>'
	where id = @esid;
		

	 insert into WAREHOUSE_ExamSessionTable_Shreded  
     (examSessionId,      
      structureXml,      
      examVersionName,      
      examVersionRef,      
      examVersionId,      
      examName,      
      examRef,      
      qualificationid,      
      qualificationName,      
      qualificationRef,      
      resultData,      
      submittedDate,      
      originatorId,      
      centreName,      
      centreCode,      
      foreName,      
      dateOfBirth,      
      gender,      
      candidateRef,      
      surName,      
      scheduledDurationValue,      
      previousExamState,      
      examStateInformation,      
      examResult,      
      passValue,      
      closeValue,      
      --0,      
      externalReference,      
      examType,      
      warehouseTime,      
      CQN,      
      actualDuration,      
      appeal,      
      reMarkStatus,        
      qualificationLevel,   
      centreId,     
      uln,  
      AllowPackageDelivery,  
      ExportToSecureMarker,  
      ContainsBTLOffice,
      ExportedToIntegration,
      WarehouseExamState,
      [language],
      KeyCode,
      TargetedForVoid,
      EnableOverrideMarking,
      AddressLine1,
      AddressLine2,
      Town,
      County,
      Country,
      Postcode,
      ScoreBoundaryData,
      itemMarksUploaded,
      passMark,
      totalMark,
      passType,
      voidJustificationLookupTableId,
      automaticVerification,
      resultSampled,
      [started],
      submitted,
      validFromDate,
      expiryDate,
      totalTimeSpent,
      defaultDuration,
      scheduledDurationReason,
      WAREHOUSEScheduledExamID,
      WAREHOUSEUserID,
      itemDataFull,
      examId,
      userId,
      TakenThroughLocalScan,
	  LocalScanDownloadDate,
	  LocalScanUploadDate,
	  LocalScanNumPages,
	  CertifiedForTablet,
	  IsProjectBased)    
     SELECT      
      examSessionId,      
      structureXml,      
      examVersionName,      
      examVersionRef,      
      examVersionId,      
      examName,      
      examRef,      
      qualificationid,      
      qualificationName,      
      qualificationRef,      
      resultData,      
      submittedDate,      
      originatorId,      
      centreName,      
      centreCode,      
      foreName,      
      dateOfBirth,      
      gender,      
      candidateRef,      
      surName,      
      scheduledDurationValue,      
      previousExamState,      
      examStateInformation,      
      examResult,      
      passValue,      
      closeValue,      
      --0,      
      externalReference,      
      examType,      
      warehouseTime,      
      CQN,      
      actualDuration,      
      appeal,      
      reMarkStatus,        
      qualificationLevel,   
      centreId,     
      uln,  
      AllowPackageDelivery,  
      ExportToSecureMarker,  
      ContainsBTLOffice,
      ExportedToIntegration,
      WarehouseExamState,
      [language],
      KeyCode,
      TargetedForVoid,
      EnableOverrideMarking,
      AddressLine1,
      AddressLine2,
      Town,
      County,
      Country,
      Postcode,
      ScoreBoundaryData,
      itemMarksUploaded,
      passMark,
      totalMark,
      passType,
      voidJustificationLookupTableId,
      automaticVerification,
      resultSampled,
      [started],
      submitted,
      validFromDate,
      expiryDate,
      totalTimeSpent,
      defaultDuration,
      scheduledDurationReason,
      WAREHOUSEScheduledExamID,
      WAREHOUSEUserID,
      itemDataFull,
      examId,
      userId,
      TakenThroughLocalScan,
	  LocalScanDownloadDate,
	  LocalScanUploadDate,
	  LocalScanNumPages,
	  CertifiedForTablet,
	  IsProjectBased
     FROM sa_CandidateExamAudit_View      
     WHERE examSessionId = @esid;		
		
	BEGIN TRANSACTION;

		INSERT INTO WAREHOUSE_ExamSessionTable_ShrededItems
		 (	 ExamSessionID
			,ItemRef
			,ItemName
			,UserMark
			,MarkerUserMark
			,UserAttempted
			,ItemVersion
			,MarkingIgnored
			,MarkedMetadataCount
			,ExamPercentage
			,TotalMark
			,ResponseXML
			,OptionsChosen
			,SelectedCount
			,CorrectAnswerCount
			,itemType
			,FriendItems)
			SELECT	 @esid AS [examSessionId]
					,Result.Item.value('@id', 'nvarchar(15)') AS [itemRef]
					,Result.Item.value('@name', 'nvarchar(200)') AS [itemName]
					,Result.Item.value('@userMark', 'decimal(6, 3)') AS [userMark]
					,Result.Item.value('@markerUserMark', 'nvarchar(max)') AS [markerUserMark]
					,Result.Item.value('@userAttempted', 'bit') AS [userAttempted]
					,Result.Item.value('@version', 'int') AS [itemVersion]
					,Result.Item.value('@markingIgnored', 'tinyint') AS [markingIgnored]
					,Result.Item.value('count(mark)', 'int') AS [markedMetadataCount]
					,WAREHOUSE_ExamSessionTable.ResultData.value('(exam/@userPercentage)[1]', 'decimal(6, 3)') AS [examPercentage]
					,Result.Item.value('@totalMark', 'decimal(6, 3)') AS [totalMark]
					,WAREHOUSE_ExamSessionItemResponseTable.ItemResponseData AS [responseXml]
					,CAST(ISNULL(WAREHOUSE_ExamSessionItemResponseTable.ItemResponseData.query('data(p/s/c[@typ = "10"]/i[@sl = "1"]/@ac)'), ' UA ') AS nvarchar(200)) AS [optionsChosen] --Why?
					,WAREHOUSE_ExamSessionItemResponseTable.ItemResponseData.value('count(p/s/c/i[@sl = "1"])', 'int') AS [selectedCount]
					,WAREHOUSE_ExamSessionItemResponseTable.ItemResponseData.value('count(p/s/c/i[@ca = "1"])', 'int') AS [correctAnswerCount]
					,Result.Item.value('@type', 'int') AS ItemType
					,Result.Item.value('@SurpassFriendItems', 'NVARCHAR(MAX)') AS FriendItems
			 FROM	 WAREHOUSE_ExamSessionTable
			 CROSS APPLY WAREHOUSE_ExamSessionTable.ResultData.nodes('exam/section/item') Result(Item)
			 LEFT JOIN WAREHOUSE_ExamSessionItemResponseTable -- we want to include nulls in the the item response table so that non attempted items are included
					 ON WAREHOUSE_ExamSessionItemResponseTable.WAREHOUSEExamSessionID = @esid
					AND WAREHOUSE_ExamSessionItemResponseTable.ItemID = Result.Item.value('@id', 'nvarchar(15)')
			WHERE	 WAREHOUSE_ExamSessionTable.ID = @esid;
	COMMIT TRANSACTION;	
		
select StructureXML, resultData, resultDataFull from WAREHOUSE_ExamSessionTable where id = @esid;

--Rollback
