SELECT UniqueGroupResponseLinks.confirmedMark, AssignedGroupMarks.AssignedMark
FROM AssignedGroupMarks
INNER JOIN UniqueGroupResponseLinks
ON UniqueGroupResponseLinks.UniqueGroupResponseID = AssignedGroupMarks.UniqueGroupResponseId
INNER JOIN AssignedItemMarks
ON AssignedItemMarks.GroupMarkId = AssignedGroupMarks.ID
INNER JOIN CandidateResponses
ON CandidateResponses.UniqueResponseID = AssignedItemMarks.UniqueResponseId
INNER JOIN CandidateExamVersions
ON CandidateExamVersionID = CandidateExamVersions.ID
WHERE MarkingMethodId = 12 AND IsConfirmedMark = 1 and ROUND(UniqueGroupResponseLinks.confirmedMark, 0) <> AssignedGroupMarks.AssignedMark AND confirmedMark <> 0
--AND UniqueGroupResponseLinks.confirmedMark - 0.5 <> AssignedGroupMarks.AssignedMark
--GROUP BY Keycode, CandidateExamVersions.DateSubmitted
--ORDER BY CandidateExamVersions.DateSubmitted

SELECT UniqueGroupResponseLinks.confirmedMark, AssignedGroupMarks.AssignedMark, responseData.value('(p/@um)[1]', 'DECIMAL(18,10)') * Items.TotalMark, 
responseData, Keycode, DateSubmitted, Items.ExternalItemName, Items.ExternalItemID
FROM UniqueGroupResponseLinks
INNER JOIN AssignedGroupMarks
ON AssignedGroupMarks.UniqueGroupResponseId = UniqueGroupResponseLinks.UniqueGroupResponseID
INNER JOIN UniqueResponses
ON UniqueResponses.id = UniqueGroupResponseLinks.UniqueResponseId
INNER JOIN Items
ON itemId = Items.ID
INNER JOIN CandidateResponses
ON CandidateResponses.UniqueResponseID = UniqueResponses.id
INNER JOIN CandidateExamVersions
ON CandidateExamVersionID = CandidateExamVersions.ID
WHERE UniqueGroupResponseLinks.confirmedMark <> AssignedGroupMarks.AssignedMark AND MarkingMethodId = 12 AND IsConfirmedMark = 1 
AND CandidateExamVersions.ExamSessionState >= 3
ORDER BY DateSubmitted DESC

SELECT *
FROM AssignedGroupMarks
INNER JOIN UniqueGroupResponseLinks
ON AssignedGroupMarks.UniqueGroupResponseId = UniqueGroupResponseLinks.UniqueGroupResponseID
WHERE ROUND(AssignedMark, 0) <> AssignedMark

