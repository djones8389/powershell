
with cte as (

SELECT ExamSessionTable.ID

FROM ExamSessionTable
	INNER JOIN ExamStateChangeAuditTable
	ON ExamStateChangeAuditTable.ExamSessionID = ExamSessionTable.ID AND NewState = 9
	INNER JOIN ScheduledExamsTable
	ON ScheduledExamsTable.ID = ScheduledExamID
	
WHERE AutoVoidDate IS NULL AND MarkingAutoVoidPeriod IS NOT NULL
	--AND NOT EXISTS(SELECT * FROM ExamStateChangeAuditTable WHERE ExamSessionID = ExamSessionTable.ID)
--ORDER BY StateChangeDate DESC

)

select * from ExamStateChangeAuditTable where ExamSessionID in (Select cte.ID from cte)