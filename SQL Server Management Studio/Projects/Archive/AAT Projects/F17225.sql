--select * from ExamSessionTable where KeyCode in ('CZE57CA6','Z9KPXAA6','GJ7KYWA6','NN8XYUA6','NVBWUMA6','9K42K2A6','RUH4SPA6')
select * from WAREHOUSE_ExamSessionTable where KeyCode in ('CZE57CA6','Z9KPXAA6','GJ7KYWA6','NN8XYUA6','NVBWUMA6','9K42K2A6','RUH4SPA6')


select * from WAREHOUSE_ExamSessionItemResponseTable
	where WAREHOUSEExamSessionID in (1290653,1290667,1290713,1290725,1290747,1290760,1290761)
		order by WAREHOUSEExamSessionID


--declare @ID int = (
--select ID
--	from WAREHOUSE_ExamSessionTable
--		where KeyCode = @Keycode
--	)


create table #ItemResponseTempTable

(
      ID2 int,
      TotalNumberOfQuestions int
)

insert into #ItemResponseTempTable

      select
                  ID,
                  StructureXML.value('count(/assessmentDetails/assessment/section/item)[1]', 'int')
      from WAREHOUSE_ExamSessionTable
     where KeyCode in ('CZE57CA6','Z9KPXAA6','GJ7KYWA6','NN8XYUA6','NVBWUMA6','9K42K2A6','RUH4SPA6')
      
      
	select 				
		   COUNT (ExamSessionID) as NumberOfQuestionsAnswered
		   ,  TT.TotalNumberOfQuestions 
		   , KeyCode     

		from WAREHOUSE_ExamSessionItemResponseTable as ESIRT

		inner join WAREHOUSE_ExamSessionTable as EST
		on EST.ID = ESIRT.WAREHOUSEExamSessionID
		
		inner join #ItemResponseTempTable as TT
        	on TT.ID2 = ESIRT.WAREHOUSEExamSessionID
		
		   where KeyCode in ('CZE57CA6','Z9KPXAA6','GJ7KYWA6','NN8XYUA6','NVBWUMA6','9K42K2A6','RUH4SPA6')
		
	group by ExamSessionID, TT.TotalNumberOfQuestions, KeyCode
	
	drop table #ItemResponseTempTable
	--drop procedure BTL_DaveJ_HowManyItemResponses_Live
END
