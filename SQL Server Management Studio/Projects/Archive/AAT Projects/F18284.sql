select id, candidateref, Forename, Surname
	from UserTable	
		where candidateref in ('10437283','20013138','10442869','20031531')
			order by Forename, Surname;
			
			
SELECT EST.ID, EST.KeyCode, EST.examState, UT.Forename, UT.Surname, CT.CentreName, SCET.examName
	, UserAssociationMarkerAssignable, UserAssociationModeratorAssignable
	, UserAssociationModeratorRequired, UserAssociationModeratorAssignable 
	, UserAssociationRestrictUsers
  FROM ExamSessionTable as EST

  Inner Join ScheduledExamsTable as SCET
  on SCET.ID = EST.ScheduledExamID
  
  Inner Join UserTable as UT
  on UT.ID = EST.UserID

  Inner Join CentreTable as CT
  on CT.ID = SCET.CentreID

where candidateref in ('10437283','20013138','10442869','20031531');





update ExamSessionTable
set UserAssociationRestrictUsers = 0
where id in (1406354,1406355,1406339,1406385)