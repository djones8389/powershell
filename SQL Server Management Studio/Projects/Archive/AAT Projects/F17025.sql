SELECT EST.ID, EST.KeyCode, EST.examState, UT.Forename, UT.Surname, CT.CentreName, SCET.examName
	, est.StructureXML ,est.resultData, est.resultDataFull
		,ests.structureXml, ests.resultData
  FROM ExamSessionTable as EST

  Inner Join ScheduledExamsTable as SCET
  on SCET.ID = EST.ScheduledExamID
  
  Inner Join UserTable as UT
  on UT.ID = EST.UserID

  Inner Join CentreTable as CT
  on CT.ID = SCET.CentreID
  
  inner join ExamSessionTable_Shredded as ESTS
  on ESTS.examSessionId = EST.ID

where EST.ID = 1264267



update ExamSessionTable
set StructureXML = '<assessmentDetails>
  <assessmentID>2032</assessmentID>
  <qualificationID>25</qualificationID>
  <qualificationName>Business Taxation (AQ2010)</qualificationName>
  <qualificationReference>AATQCF</qualificationReference>
  <assessmentGroupName>BTX FA 2013</assessmentGroupName>
  <assessmentGroupID>1918</assessmentGroupID>
  <assessmentGroupReference>CABTX</assessmentGroupReference>
  <assessmentName>BTX FA 2013</assessmentName>
  <validFromDate>06 Jan 2014</validFromDate>
  <expiryDate>31 Dec 2014</expiryDate>
  <startTime>00:00:00</startTime>
  <endTime>23:59:59</endTime>
  <duration>120</duration>
  <defaultDuration>120</defaultDuration>
  <scheduledDuration>
    <value>120</value>
    <reason></reason>
  </scheduledDuration>
  <sRBonus>0</sRBonus>
  <sRBonusMaximum>40</sRBonusMaximum>
  <externalReference>CABTX</externalReference>
  <passLevelValue>70</passLevelValue>
  <passLevelType>1</passLevelType>
  <status>2</status>
  <testFeedbackType>
    <passFail>0</passFail>
    <percentageMark>0</percentageMark>
    <allowSummaryFeedback>0</allowSummaryFeedback>
    <summaryFeedbackType>1</summaryFeedbackType>
    <itemSummary>0</itemSummary>
    <itemReview>0</itemReview>
    <itemReviewCorrectAnswer>0</itemReviewCorrectAnswer>
    <itemFeedback>0</itemFeedback>
    <printableSummary>0</printableSummary>
    <candidateDetails>0</candidateDetails>
    <feedbackByReference>0</feedbackByReference>
    <allowFeedbackDuringExam>0</allowFeedbackDuringExam>
    <allowFeedbackDuringSummary>0</allowFeedbackDuringSummary>
  </testFeedbackType>
  <itemFeedback>0</itemFeedback>
  <allowCalculator>0</allowCalculator>
  <lastInUseDate>09 Dec 2013 14:23:51</lastInUseDate>
  <completedScriptReview>0</completedScriptReview>
  <assessment currentSection="1" totalTime="0" currentTime="0">
    <intro id="0" name="Introduction" currentItem="">
      <item id="881P3590" name="Introduction" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
    </intro>
    <outro id="0" name="Finish" currentItem="" />
    <tools id="0" name="Tools" currentItem="">
      <item id="881P3366" name="BTAX tax data FA13 pop up 1" toolName="BTAX tax data FA13 pop up 1" totalMark="1" version="6" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="881P3367" name="BTAX tax data FA13 pop up 2" toolName="BTAX tax data FA13 pop up 2" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
    </tools>
    <section id="1" name="1" passLevelValue="60" passLevelType="1" itemsToMark="0" currentItem="881P3415" fixed="0">
      <item id="881P3374" name="1.1 BTX FA13 KB 003" totalMark="3" version="16" markingType="0" markingState="0" userMark="1" markerUserMark="" userAttempted="1" viewingTime="18736" flagged="0" LO="1.1 Determine if trade is being carried on/distinction between capital and revenue expenditure" unit="Business Tax" quT="16" />
      <item id="881P3389" name="1.2 BTX FA13 KB 009" totalMark="7" version="18" markingType="0" markingState="0" userMark="1" markerUserMark="" userAttempted="1" viewingTime="132195" flagged="0" LO="1.2 Adjusted profits" unit="Business Tax" quT="11" />
      <item id="881P3393" name="1.3 BTX FA13 KB 004" totalMark="6" version="29" markingType="0" markingState="0" userMark="1" markerUserMark="" userAttempted="1" viewingTime="209071" flagged="0" LO="1.3 Assessment for sole traders and partnerships" unit="Business Tax" quT="11,20" />
      <item id="881P3408" name="1.4 BTX FA13 KB 007" totalMark="4" version="30" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="1" viewingTime="66362" flagged="0" LO="1.4 Relief of trading loss" unit="Business Tax" quT="11" />
      <item id="881P3415" name="1.5 BTX FA13 KB 005" totalMark="9" version="31" markingType="0" markingState="0" userMark="0.55555555" markerUserMark="" userAttempted="1" viewingTime="128497" flagged="0" LO="1.5 Allocation of profits within a partnership" unit="Business Tax" quT="11" />
      <item id="881P3423" name="1.6 BTX FA13 KB 004" totalMark="5" version="23" markingType="0" markingState="0" userMark="0.8" markerUserMark="" userAttempted="1" viewingTime="45490" flagged="0" LO="1.6 Profits chargeable to corporation tax, short/long periods of account" unit="Business Tax" quT="10" />
      <item id="881P3433" name="1.7 BTX FA13 KB 005" totalMark="16" version="36" markingType="1" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="446223" flagged="0" LO="1.7 Capital allowances" unit="Business Tax" quT="20" />
    </section>
    <section id="2" name="2" passLevelValue="60" passLevelType="1" itemsToMark="0" currentItem="881P3524" fixed="0">
      <item id="881P3440" name="2.1 BTX FA13 KB 002" totalMark="2" version="18" markingType="0" markingState="0" userMark="1" markerUserMark="" userAttempted="1" viewingTime="127702" flagged="0" LO="2.1 Chargeable persons/assets/disposal, connected persons, capital gains and losses" unit="Business Tax" quT="16" />
      <item id="881P3452" name="2.2 BTX FA13 KB 005" totalMark="5" version="33" markingType="0" markingState="0" userMark="0.8" markerUserMark="" userAttempted="1" viewingTime="122273" flagged="0" LO="2.2 Capital disposals - gains and losses" unit="Business Tax" quT="11" />
      <item id="881P3461" name="2.3 BTX FA13 KB 005" totalMark="7" version="43" markingType="1" markingState="1" userMark="1" markerUserMark="2" userAttempted="1" viewingTime="438736" flagged="0" LO="2.3 Capital disposals - shares" unit="Business Tax" quT="20" />
      <item id="881P3473" name="2.4 BTX FA13 KB 008" totalMark="4" version="16" markingType="0" markingState="0" userMark="0.5" markerUserMark="" userAttempted="1" viewingTime="83897" flagged="0" LO="2.4 Capital gain tax reliefs" unit="Business Tax" quT="10" />
      <item id="881P3483" name="2.5 BTX FA13 KB 009" totalMark="3" version="12" markingType="0" markingState="0" userMark="0.33333333" markerUserMark="" userAttempted="1" viewingTime="131447" flagged="0" LO="2.5 Rollover relief" unit="Business Tax" quT="11" />
      <item id="881P3492" name="2.6 BTX FA13 KB 009" totalMark="4" version="15" markingType="0" markingState="0" userMark="0.5" markerUserMark="" userAttempted="1" viewingTime="498360" flagged="0" LO="2.6 Class 2 and 4 NIC" unit="Business Tax" quT="11" />
      <item id="881P3495" name="2.7 BTX FA13 KB 003" totalMark="4" version="12" markingType="0" markingState="0" userMark="0.75" markerUserMark="" userAttempted="1" viewingTime="73444" flagged="0" LO="2.7 Self-assessment" unit="Business Tax" quT="10" />
      <item id="881P3503" name="2.8 BTX FA13 KB 002" totalMark="9" version="19" markingType="0" markingState="0" userMark="0.66666666" markerUserMark="" userAttempted="1" viewingTime="370391" flagged="0" LO="2.8 Corporation tax payable" unit="Business Tax" quT="11" />
      <item id="881P3512" name="2.9 BTX FA13 KB 002" totalMark="6" version="17" markingType="0" markingState="0" userMark="0.83333333" markerUserMark="" userAttempted="1" viewingTime="88155" flagged="0" LO="2.9 Penalties, surcharge and interest" unit="Business Tax" quT="10" />
      <item id="881P3524" name="2.10 BTX FA13 KB 005" totalMark="6" version="21" markingType="1" markingState="1" userMark="0" markerUserMark="3" userAttempted="1" viewingTime="175984" flagged="0" LO="2.10 Tax return" unit="Business Tax" quT="11" />
    </section>
  </assessment>
  <isValid>1</isValid>
  <isPrintable>0</isPrintable>
  <qualityReview>0</qualityReview>
  <numberOfGenerations>1</numberOfGenerations>
  <requiresInvigilation>1</requiresInvigilation>
  <advanceContentDownloadTimespan>60</advanceContentDownloadTimespan>
  <automaticVerification>0</automaticVerification>
  <offlineMode>2</offlineMode>
  <requiresValidation>0</requiresValidation>
  <requiresSecureClient>1</requiresSecureClient>
  <examType>0</examType>
  <advanceDownload>0</advanceDownload>
  <gradeBoundaryData>
    <gradeBoundaries passLevelType="1" adjustedGradeBoundaries="0">
      <grade modifier="lt" value="70" description="Fail" />
      <grade modifier="gt" value="70" description="Pass" />
    </gradeBoundaries>
  </gradeBoundaryData>
  <autoViewExam>0</autoViewExam>
  <autoProgressItems>0</autoProgressItems>
  <confirmationText>
    <confirmationText></confirmationText>
  </confirmationText>
  <requiresConfirmationCheck>0</requiresConfirmationCheck>
  <allowCloseWithoutSubmit>0</allowCloseWithoutSubmit>
  <useSecureMarker>0</useSecureMarker>
  <annotationVersion>0</annotationVersion>
  <allowPackagingDelivery>1</allowPackagingDelivery>
  <scoreBoundaryData>
    <scoreBoundaries showScoreBoundaryColumn="1">
      <score modifier="lt" value="36" description="Significantly below" higherBoundarySet="1" />
      <score modifier="lt" value="36" description="Significantly below" higherBoundarySet="0" />
      <score modifier="gt" value="36" description="Below requirement" higherBoundarySet="1" />
      <score modifier="gt" value="60" description="Borderline" higherBoundarySet="1" />
      <score modifier="gt" value="70" description="Met" higherBoundarySet="0" />
      <score modifier="gt" value="70" description="Met" higherBoundarySet="1" />
      <score modifier="gt" value="36" description="Below requirement" higherBoundarySet="0" userCreated="1" />
      <score modifier="gt" value="60" description="Borderline" higherBoundarySet="0" userCreated="1" />
      <score modifier="gt" value="85" description="Exceeded" higherBoundarySet="1" userCreated="1" />
    </scoreBoundaries>
  </scoreBoundaryData>
  <showCandidateReportResultColumn>0</showCandidateReportResultColumn>
  <showCandidateReportPercentageColumn>1</showCandidateReportPercentageColumn>
  <certifiedAccessible>0</certifiedAccessible>
  <markingProgressVisible>1</markingProgressVisible>
  <markingProgressMode>0</markingProgressMode>
  <secureClientOperationMode>1</secureClientOperationMode>
  <examDeliverySystemStyleType>delivery</examDeliverySystemStyleType>
  <markingAutoVoidPeriod>365</markingAutoVoidPeriod>
  <showPageRequiresScrollingAlert>0</showPageRequiresScrollingAlert>
  <project ID="881" />
</assessmentDetails>', resultData = '<exam passMark="70" passType="1" originalPassMark="70" originalPassType="1" totalMark="100" userMark="55.000" userPercentage="55.000" passValue="1" originalPassValue="1" totalTimeSpent="3156963" closeBoundaryType="0" closeBoundaryValue="0" grade="Pass" originalGrade="Pass">
  <gradeBoundaryData>
    <gradeBoundaries passLevelType="1" adjustedGradeBoundaries="0">
      <grade modifier="lt" value="70" description="Fail" />
      <grade modifier="gt" value="70" description="Pass" />
    </gradeBoundaries>
  </gradeBoundaryData>
  <section id="1" passMark="60" passType="1" name="1" totalMark="50" userMark="41.000" userPercentage="82.000" passValue="1" totalTimeSpent="1046574" itemsToMark="0">
    <item id="881P3374" name="1.1 BTX FA13 KB 003" totalMark="3" userMark="1" actualUserMark="3" markingType="0" markerUserMark="" viewingTime="18736" userAttempted="1" version="16" />
    <item id="881P3389" name="1.2 BTX FA13 KB 009" totalMark="7" userMark="1" actualUserMark="7" markingType="0" markerUserMark="" viewingTime="132195" userAttempted="1" version="18" />
    <item id="881P3393" name="1.3 BTX FA13 KB 004" totalMark="6" userMark="1" actualUserMark="6" markingType="0" markerUserMark="" viewingTime="209071" userAttempted="1" version="29" />
    <item id="881P3408" name="1.4 BTX FA13 KB 007" totalMark="4" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="66362" userAttempted="1" version="30" />
    <item id="881P3415" name="1.5 BTX FA13 KB 005" totalMark="9" userMark="0.55555555" actualUserMark="4.99999995" markingType="0" markerUserMark="" viewingTime="128497" userAttempted="1" version="31" />
    <item id="881P3423" name="1.6 BTX FA13 KB 004" totalMark="5" userMark="0.8" actualUserMark="4.0" markingType="0" markerUserMark="" viewingTime="45490" userAttempted="1" version="23" />
    <item id="881P3433" name="1.7 BTX FA13 KB 005" totalMark="16" userMark="0" actualUserMark="16" markingType="1" markerUserMark="" viewingTime="446223" userAttempted="0" version="36" />
  </section>
  <section id="2" passMark="60" passType="1" name="2" totalMark="50" userMark="30.000" userPercentage="60.000" passValue="1" totalTimeSpent="2110389" itemsToMark="0">
    <item id="881P3440" name="2.1 BTX FA13 KB 002" totalMark="2" userMark="1" actualUserMark="2" markingType="0" markerUserMark="" viewingTime="127702" userAttempted="1" version="18" />
    <item id="881P3452" name="2.2 BTX FA13 KB 005" totalMark="5" userMark="0.8" actualUserMark="4.0" markingType="0" markerUserMark="" viewingTime="122273" userAttempted="1" version="33" />
    <item id="881P3461" name="2.3 BTX FA13 KB 005" totalMark="7" userMark="0" actualUserMark="2" markingType="1" markerUserMark="2" viewingTime="438736" userAttempted="1" version="43" />
    <item id="881P3473" name="2.4 BTX FA13 KB 008" totalMark="4" userMark="0.5" actualUserMark="2.0" markingType="0" markerUserMark="" viewingTime="83897" userAttempted="1" version="16" />
    <item id="881P3483" name="2.5 BTX FA13 KB 009" totalMark="3" userMark="0.33333333" actualUserMark="0.99999999" markingType="0" markerUserMark="" viewingTime="131447" userAttempted="1" version="12" />
    <item id="881P3492" name="2.6 BTX FA13 KB 009" totalMark="4" userMark="0.5" actualUserMark="2.0" markingType="0" markerUserMark="" viewingTime="498360" userAttempted="1" version="15" />
    <item id="881P3495" name="2.7 BTX FA13 KB 003" totalMark="4" userMark="0.75" actualUserMark="3.00" markingType="0" markerUserMark="" viewingTime="73444" userAttempted="1" version="12" />
    <item id="881P3503" name="2.8 BTX FA13 KB 002" totalMark="9" userMark="0.66666666" actualUserMark="5.99999994" markingType="0" markerUserMark="" viewingTime="370391" userAttempted="1" version="19" />
    <item id="881P3512" name="2.9 BTX FA13 KB 002" totalMark="6" userMark="0.83333333" actualUserMark="4.99999998" markingType="0" markerUserMark="" viewingTime="88155" userAttempted="1" version="17" />
    <item id="881P3524" name="2.10 BTX FA13 KB 005" totalMark="6" userMark="0" actualUserMark="3" markingType="1" markerUserMark="3" viewingTime="175984" userAttempted="1" version="21" />
  </section>
</exam>', resultDataFull = '<assessmentDetails>
  <assessmentID>2032</assessmentID>
  <qualificationID>25</qualificationID>
  <qualificationName>Business Taxation (AQ2010)</qualificationName>
  <qualificationReference>AATQCF</qualificationReference>
  <assessmentGroupName>BTX FA 2013</assessmentGroupName>
  <assessmentGroupID>1918</assessmentGroupID>
  <assessmentGroupReference>CABTX</assessmentGroupReference>
  <assessmentName>BTX FA 2013</assessmentName>
  <validFromDate>06 Jan 2014</validFromDate>
  <expiryDate>31 Dec 2014</expiryDate>
  <startTime>00:00:00</startTime>
  <endTime>23:59:59</endTime>
  <duration>120</duration>
  <defaultDuration>120</defaultDuration>
  <scheduledDuration>
    <value>120</value>
    <reason />
  </scheduledDuration>
  <sRBonus>0</sRBonus>
  <sRBonusMaximum>40</sRBonusMaximum>
  <externalReference>CABTX</externalReference>
  <passLevelValue>70</passLevelValue>
  <passLevelType>1</passLevelType>
  <status>2</status>
  <testFeedbackType>
    <passFail>0</passFail>
    <percentageMark>0</percentageMark>
    <allowSummaryFeedback>0</allowSummaryFeedback>
    <summaryFeedbackType>1</summaryFeedbackType>
    <itemSummary>0</itemSummary>
    <itemReview>0</itemReview>
    <itemReviewCorrectAnswer>0</itemReviewCorrectAnswer>
    <itemFeedback>0</itemFeedback>
    <printableSummary>0</printableSummary>
    <candidateDetails>0</candidateDetails>
    <feedbackByReference>0</feedbackByReference>
    <allowFeedbackDuringExam>0</allowFeedbackDuringExam>
    <allowFeedbackDuringSummary>0</allowFeedbackDuringSummary>
  </testFeedbackType>
  <itemFeedback>0</itemFeedback>
  <allowCalculator>0</allowCalculator>
  <lastInUseDate>09 Dec 2013 14:23:51</lastInUseDate>
  <completedScriptReview>0</completedScriptReview>
  <assessment currentSection="1" totalTime="0" currentTime="0" originalPassMark="70" originalPassType="1" passMark="70" passType="1" totalMark="100" userMark="55.000" userPercentage="55.000" passValue="1" originalGrade="Pass">
    <intro id="0" name="Introduction" currentItem="">
      <item id="881P3590" name="Introduction" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
    </intro>
    <outro id="0" name="Finish" currentItem="" />
    <tools id="0" name="Tools" currentItem="">
      <item id="881P3366" name="BTAX tax data FA13 pop up 1" toolName="BTAX tax data FA13 pop up 1" totalMark="1" version="6" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="881P3367" name="BTAX tax data FA13 pop up 2" toolName="BTAX tax data FA13 pop up 2" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
    </tools>
    <section id="1" name="1" passLevelValue="60" passLevelType="1" itemsToMark="0" currentItem="881P3415" fixed="0" totalMark="50" userMark="41.000" userPercentage="82.000" passValue="1">
      <item id="881P3374" name="1.1 BTX FA13 KB 003" totalMark="3" version="16" markingType="0" markingState="0" userMark="1" markerUserMark="" userAttempted="1" viewingTime="18736" flagged="0" LO="1.1 Determine if trade is being carried on/distinction between capital and revenue expenditure" unit="Business Tax" quT="16">
        <p um="1" cs="1" ua="1" id="881P3374">
          <s ua="1" um="1" id="1">
            <c ua="1" um="1" typ="16" id="33">
              <z id="10" />
              <z id="7">
                <i y="240" x="300" ID="3" />
              </z>
              <z id="6">
                <i y="190" x="300" ID="1" />
              </z>
              <z id="5">
                <i y="140" x="300" ID="3" />
              </z>
              <z id="3">
                <i y="185" x="440" ID="3" />
              </z>
              <z id="1">
                <i y="140" x="440" ID="1" />
              </z>
            </c>
            <c typ="4" id="35">
              <i cc="0" id="1" />
            </c>
            <c typ="4" id="39">
              <i cc="0" id="1" />
            </c>
          </s>
        </p>
      </item>
      <item id="881P3389" name="1.2 BTX FA13 KB 009" totalMark="7" version="18" markingType="0" markingState="0" userMark="1" markerUserMark="" userAttempted="1" viewingTime="132195" flagged="0" LO="1.2 Adjusted profits" unit="Business Tax" quT="11">
        <p um="1" cs="1" ua="1" id="881P3389">
          <s ua="1" um="1" id="1">
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="39">
              <i ca="14556" id="1">14556</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="40">
              <i ca="2500" id="1">2500</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="41">
              <i ca="0" id="1">0</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="42">
              <i ca="2880" id="1">2880</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="43">
              <i ca="0" id="1">0</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="44">
              <i ca="3375" id="1">3375</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="45">
              <i ca="0" id="1">0</i>
            </c>
            <c typ="4" id="51">
              <i cc="0" id="1" />
            </c>
            <c typ="4" id="55">
              <i cc="0" id="1" />
            </c>
          </s>
        </p>
      </item>
      <item id="881P3393" name="1.3 BTX FA13 KB 004" totalMark="6" version="29" markingType="0" markingState="0" userMark="1" markerUserMark="" userAttempted="1" viewingTime="209071" flagged="0" LO="1.3 Assessment for sole traders and partnerships" unit="Business Tax" quT="11,20">
        <p um="1" cs="1" ua="1" id="881P3393">
          <s ua="1" um="1" id="1">
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="39">
              <i ca="7080" id="1">7080</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="40">
              <i ca="42480" id="1">42480</i>
            </c>
            <c wei="1" maS="0" ie="1" typ="20" rv="0" mv="0" ua="1" um="1" id="74">
              <i um="1" ia="1" id="1">&lt;table extensionName="protean"&gt;&lt;r&gt;&lt;c h="0"&gt;387959&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="2013"&gt;2013&lt;/c&gt;&lt;/r&gt;&lt;/table&gt;</i>
            </c>
            <c wei="1" maS="0" ie="1" typ="20" rv="0" mv="0" ua="1" um="1" id="73">
              <i um="1" ia="1" id="1">&lt;table extensionName="protean"&gt;&lt;r&gt;&lt;c h="0"&gt;387959&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="2012"&gt;2012&lt;/c&gt;&lt;/r&gt;&lt;/table&gt;</i>
            </c>
            <c wei="1" maS="0" ie="1" typ="20" rv="0" mv="0" ua="1" um="1" id="72">
              <i um="1" ia="1" id="1">&lt;table extensionName="protean"&gt;&lt;r&gt;&lt;c h="0"&gt;387983&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="2012"&gt;2012&lt;/c&gt;&lt;/r&gt;&lt;/table&gt;</i>
            </c>
            <c wei="1" maS="0" ie="1" typ="20" rv="0" mv="0" ua="1" um="1" id="71">
              <i um="1" ia="1" id="1">&lt;table extensionName="protean"&gt;&lt;r&gt;&lt;c h="0"&gt;386448&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="2012"&gt;2012&lt;/c&gt;&lt;/r&gt;&lt;/table&gt;</i>
            </c>
            <c typ="4" id="77">
              <i cc="0" id="1" />
            </c>
            <c typ="4" id="81">
              <i cc="0" id="1" />
            </c>
          </s>
        </p>
      </item>
      <item id="881P3408" name="1.4 BTX FA13 KB 007" totalMark="4" version="30" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="1" viewingTime="66362" flagged="0" LO="1.4 Relief of trading loss" unit="Business Tax" quT="11">
        <p um="0" cs="1" ua="1" id="881P3408">
          <s ua="1" um="0" id="1">
            <c wei="3" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="0" um="0" id="45">
              <i ca="0" id="1" />
            </c>
            <c wei="3" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="0" um="0" id="46">
              <i ca="10000" id="1" />
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="0" um="0" id="47">
              <i ca="0" id="1" />
            </c>
            <c wei="3" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="48">
              <i ca="22650" id="1">22225</i>
            </c>
            <c wei="3" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="49">
              <i ca="24025" id="1">30400</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="50">
              <i ca="0" id="1">8000</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="0" um="0" id="55">
              <i ca="" id="1" />
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="0" um="0" id="56">
              <i ca="" id="1" />
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="0" um="0" id="57">
              <i ca="" id="1" />
            </c>
            <c typ="4" id="58">
              <i cc="0" id="1" />
            </c>
            <c typ="4" id="62">
              <i cc="0" id="1" />
            </c>
          </s>
        </p>
      </item>
      <item id="881P3415" name="1.5 BTX FA13 KB 005" totalMark="9" version="31" markingType="0" markingState="0" userMark="0.55555555" markerUserMark="" userAttempted="1" viewingTime="128497" flagged="0" LO="1.5 Allocation of profits within a partnership" unit="Business Tax" quT="11">
        <p um="0.555555555555556" cs="1" ua="1" id="881P3415">
          <s ua="1" um="0.56" id="1">
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="10">
              <i ca="10000" id="1">10000</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="11">
              <i ca="0" id="1">0</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="12">
              <i ca="7875" id="1">10500</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="17">
              <i ca="400" id="1">400</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="18">
              <i ca="360" id="1">360</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="19">
              <i ca="600" id="1">600</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="24">
              <i ca="30000" id="1">41541</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="25">
              <i ca="12500" id="1">17308.75</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="26">
              <i ca="7500" id="1">10385.25</i>
            </c>
            <c typ="4" id="31">
              <i cc="0" id="1" />
            </c>
            <c typ="4" id="35">
              <i cc="0" id="1" />
            </c>
          </s>
        </p>
      </item>
      <item id="881P3423" name="1.6 BTX FA13 KB 004" totalMark="5" version="23" markingType="0" markingState="0" userMark="0.8" markerUserMark="" userAttempted="1" viewingTime="45490" flagged="0" LO="1.6 Profits chargeable to corporation tax, short/long periods of account" unit="Business Tax" quT="10">
        <p um="0.8" cs="1" ua="1" id="881P3423">
          <s ua="1" um="0.8" id="1">
            <c wei="1" maS="1" ie="1" typ="10" rv="0" mv="0" ua="1" um="1" id="21">
              <i ac="A" sl="0" id="1" />
              <i ca="1" ac="B" sl="1" id="2" />
            </c>
            <c wei="1" maS="1" ie="1" typ="10" rv="0" mv="0" ua="1" um="1" id="30">
              <i ca="1" ac="A" sl="1" id="1" />
              <i ac="B" sl="0" id="2" />
            </c>
            <c wei="1" maS="1" ie="1" typ="10" rv="0" mv="0" ua="1" um="1" id="41">
              <i ca="1" ac="A" sl="1" id="1" />
              <i ac="B" sl="0" id="2" />
            </c>
            <c wei="1" maS="1" ie="1" typ="10" rv="0" mv="0" ua="1" um="0" id="60">
              <i ca="1" ac="A" sl="0" id="1" />
              <i ac="B" sl="1" id="2" />
            </c>
            <c wei="1" maS="1" ie="1" typ="10" rv="0" mv="0" ua="1" um="1" id="61">
              <i ca="1" ac="A" sl="1" id="1" />
              <i ac="B" sl="0" id="2" />
            </c>
            <c typ="4" id="71">
              <i cc="0" id="1" />
            </c>
            <c typ="4" id="75">
              <i cc="0" id="1" />
            </c>
          </s>
        </p>
      </item>
      <item id="881P3433" name="1.7 BTX FA13 KB 005" totalMark="16" version="36" markingType="1" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="446223" flagged="0" LO="1.7 Capital allowances" unit="Business Tax" quT="20">
        <p um="1" cs="1" ua="0" id="881P3433">
          <s ua="0" um="-1" id="1">
            <c typ="4" id="24">
              <i cc="0" id="1" />
            </c>
            <c typ="4" id="28">
              <i cc="0" id="1" />
            </c>
          </s>
          <s ua="0" um="1" id="2">
            <c wei="1" maS="0" ie="1" typ="20" rv="0" mv="0" ua="0" um="1" id="1">
              <i um="1" id="1">&lt;table extensionName="HIGHLIGHT_TABLE"&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;/table&gt;</i>
            </c>
          </s>
        </p>
      </item>
    </section>
    <section id="2" name="2" passLevelValue="60" passLevelType="1" itemsToMark="0" currentItem="881P3524" fixed="0" totalMark="50" userMark="30.000" userPercentage="60.000" passValue="1">
      <item id="881P3440" name="2.1 BTX FA13 KB 002" totalMark="2" version="18" markingType="0" markingState="0" userMark="1" markerUserMark="" userAttempted="1" viewingTime="127702" flagged="0" LO="2.1 Chargeable persons/assets/disposal, connected persons, capital gains and losses" unit="Business Tax" quT="16">
        <p um="1" cs="1" ua="1" id="881P3440">
          <s ua="1" um="1" id="1">
            <c ua="1" um="1" typ="16" id="33">
              <z id="10" />
              <z id="6">
                <i y="190" x="265" ID="3" />
              </z>
              <z id="5">
                <i y="130" x="265" ID="1" />
              </z>
              <z id="3">
                <i y="150" x="425" ID="3" />
              </z>
              <z id="1">
                <i y="105" x="425" ID="1" />
              </z>
            </c>
            <c typ="4" id="39">
              <i cc="0" id="1" />
            </c>
            <c typ="4" id="43">
              <i cc="0" id="1" />
            </c>
          </s>
        </p>
      </item>
      <item id="881P3452" name="2.2 BTX FA13 KB 005" totalMark="5" version="33" markingType="0" markingState="0" userMark="0.8" markerUserMark="" userAttempted="1" viewingTime="122273" flagged="0" LO="2.2 Capital disposals - gains and losses" unit="Business Tax" quT="11">
        <p um="0.8" cs="1" ua="1" id="881P3452">
          <s ua="1" um="0.8" id="1">
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="17">
              <i ca="7800" id="1">7800</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="18">
              <i ca="300" id="1">300</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="19">
              <i ca="1500" id="1">1500</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="22">
              <i ca="" id="1">6000</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="30">
              <i ca="3000" id="1">0</i>
            </c>
            <c typ="4" id="37">
              <i cc="1" id="1" />
            </c>
            <c typ="4" id="41">
              <i cc="3" id="1" />
            </c>
          </s>
        </p>
      </item>
      <item id="881P3461" name="2.3 BTX FA13 KB 005" totalMark="7" version="43" markingType="1" markingState="1" userMark="1" markerUserMark="2" userAttempted="1" viewingTime="438736" flagged="0" LO="2.3 Capital disposals - shares" unit="Business Tax" quT="20">
        <p um="1" cs="1" ua="1" id="881P3461">
          <s ua="0" um="-1" id="1">
            <c typ="4" id="12">
              <i cc="0" id="1" />
            </c>
            <c typ="4" id="16">
              <i cc="0" id="1" />
            </c>
          </s>
          <s ua="1" um="1" id="2">
            <c wei="1" maS="0" ie="1" typ="20" rv="0" mv="0" ua="1" um="1" id="1">
              <i um="1" ia="1" id="1">&lt;table extensionName="HIGHLIGHT_TABLE"&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;Quanitity&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;Cost&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;Total Cost&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;June 1992&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;5000&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;2&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;10000&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;May 2006 - Rights Issue&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;1000&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;2&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;2000&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;6000&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;12000&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;December 2011 - Bonus Issue&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;3000&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;12000&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;9000&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;1.33&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;12000&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;November 2013&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;-3000&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;-5000&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;Balance of shares carried forward&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;6000&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;1.16&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;7000&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;/table&gt;</i>
            </c>
          </s>
        </p>
      </item>
      <item id="881P3473" name="2.4 BTX FA13 KB 008" totalMark="4" version="16" markingType="0" markingState="0" userMark="0.5" markerUserMark="" userAttempted="1" viewingTime="83897" flagged="0" LO="2.4 Capital gain tax reliefs" unit="Business Tax" quT="10">
        <p um="0.5" cs="1" ua="1" id="881P3473">
          <s ua="1" um="0.5" id="1">
            <c wei="1" maS="4" ie="1" typ="10" rv="0" mv="0" ua="1" um="0.5" id="35">
              <i ac="A" sl="1" id="1" />
              <i ac="B" sl="1" id="2" />
              <i ca="1" ac="C" sl="1" id="3" />
              <i ca="1" ac="D" sl="0" id="4" />
              <i ca="1" ac="E" sl="0" id="5" />
              <i ac="F" sl="0" id="6" />
              <i ca="1" ac="G" sl="1" id="7" />
            </c>
            <c typ="4" id="40">
              <i cc="0" id="1" />
            </c>
            <c typ="4" id="44">
              <i cc="1" id="1" />
            </c>
          </s>
        </p>
      </item>
      <item id="881P3483" name="2.5 BTX FA13 KB 009" totalMark="3" version="12" markingType="0" markingState="0" userMark="0.33333333" markerUserMark="" userAttempted="1" viewingTime="131447" flagged="0" LO="2.5 Rollover relief" unit="Business Tax" quT="11">
        <p um="0.333333333333333" cs="1" ua="1" id="881P3483">
          <s ua="1" um="0.33" id="1">
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="44">
              <i ca="19700" id="1">9595</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="43">
              <i ca="26000" id="1">45700</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="42">
              <i ca="45700" id="1">45700</i>
            </c>
            <c typ="4" id="54">
              <i cc="1" id="1" />
            </c>
            <c typ="4" id="58">
              <i cc="1" id="1" />
            </c>
          </s>
        </p>
      </item>
      <item id="881P3492" name="2.6 BTX FA13 KB 009" totalMark="4" version="15" markingType="0" markingState="0" userMark="0.5" markerUserMark="" userAttempted="1" viewingTime="498360" flagged="0" LO="2.6 Class 2 and 4 NIC" unit="Business Tax" quT="11">
        <p um="0.5" cs="1" ua="1" id="881P3492">
          <s ua="1" um="0.5" id="1">
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="39">
              <i ca="140.40" id="1">140.40</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="40">
              <i ca="0" id="1">667.80</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="41">
              <i ca="140.40" id="1">140.40</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="42">
              <i ca="3175.55" id="1">3873.50</i>
            </c>
            <c typ="4" id="52">
              <i cc="2" id="1" />
            </c>
            <c typ="4" id="56">
              <i cc="3" id="1" />
            </c>
          </s>
        </p>
      </item>
      <item id="881P3495" name="2.7 BTX FA13 KB 003" totalMark="4" version="12" markingType="0" markingState="0" userMark="0.75" markerUserMark="" userAttempted="1" viewingTime="73444" flagged="0" LO="2.7 Self-assessment" unit="Business Tax" quT="10">
        <p um="0.75" cs="1" ua="1" id="881P3495">
          <s ua="1" um="0.75" id="1">
            <c wei="1" maS="1" ie="1" typ="10" rv="0" mv="0" ua="1" um="1" id="51">
              <i ca="1" ac="A" sl="1" id="1" />
              <i ac="B" sl="0" id="2" />
            </c>
            <c wei="1" maS="1" ie="1" typ="10" rv="0" mv="0" ua="1" um="0" id="52">
              <i ac="A" sl="1" id="1" />
              <i ca="1" ac="B" sl="0" id="2" />
            </c>
            <c wei="1" maS="1" ie="1" typ="10" rv="0" mv="0" ua="1" um="1" id="53">
              <i ca="1" ac="A" sl="1" id="1" />
              <i ac="B" sl="0" id="2" />
            </c>
            <c wei="1" maS="1" ie="1" typ="10" rv="0" mv="0" ua="1" um="1" id="54">
              <i ac="A" sl="0" id="1" />
              <i ca="1" ac="B" sl="1" id="2" />
            </c>
            <c typ="4" id="55">
              <i cc="1" id="1" />
            </c>
            <c typ="4" id="59">
              <i cc="1" id="1" />
            </c>
          </s>
        </p>
      </item>
      <item id="881P3503" name="2.8 BTX FA13 KB 002" totalMark="9" version="19" markingType="0" markingState="0" userMark="0.66666666" markerUserMark="" userAttempted="1" viewingTime="370391" flagged="0" LO="2.8 Corporation tax payable" unit="Business Tax" quT="11">
        <p um="0.666666666666667" cs="1" ua="1" id="881P3503">
          <s ua="1" um="0.67" id="1">
            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="28">
              <i ca="312500" id="1">750000</i>
            </c>
            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="30">
              <i ca="437500" id="1">750000</i>
            </c>
            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="32">
              <i ca="301000" id="1">301000</i>
            </c>
            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="33">
              <i ca="317450" id="1">317450</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="34">
              <i ca="" id="1">4101</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="35">
              <i ca="" id="1">3721</i>
            </c>
            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="36">
              <i ca="215000" id="2">215000</i>
            </c>
            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="54">
              <i ca="226750" id="1">226750</i>
            </c>
            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="31">
              <i ca="317450" id="1">317450</i>
            </c>
            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="29">
              <i ca="226750" id="1">226750</i>
            </c>
            <c typ="4" id="69">
              <i cc="1" id="1" />
            </c>
            <c typ="4" id="73">
              <i cc="0" id="1" />
            </c>
          </s>
        </p>
      </item>
      <item id="881P3512" name="2.9 BTX FA13 KB 002" totalMark="6" version="17" markingType="0" markingState="0" userMark="0.83333333" markerUserMark="" userAttempted="1" viewingTime="88155" flagged="0" LO="2.9 Penalties, surcharge and interest" unit="Business Tax" quT="10">
        <p um="0.833333333333333" cs="1" ua="1" id="881P3512">
          <s ua="1" um="0.83" id="1">
            <c wei="1" maS="1" ie="1" typ="10" rv="0" mv="0" ua="1" um="0" id="64">
              <i ca="1" ac="A" sl="0" id="1" />
              <i ac="B" sl="1" id="2" />
            </c>
            <c wei="1" maS="1" ie="1" typ="10" rv="0" mv="0" ua="1" um="1" id="74">
              <i ca="1" ac="A" sl="1" id="1" />
              <i ac="B" sl="0" id="2" />
            </c>
            <c wei="1" maS="1" ie="1" typ="10" rv="0" mv="0" ua="1" um="1" id="83">
              <i ac="A" sl="0" id="1" />
              <i ca="1" ac="B" sl="1" id="2" />
            </c>
            <c wei="1" maS="1" ie="1" typ="10" rv="0" mv="0" ua="1" um="1" id="84">
              <i ac="A" sl="0" id="1" />
              <i ca="1" ac="B" sl="1" id="2" />
            </c>
            <c wei="1" maS="1" ie="1" typ="10" rv="0" mv="0" ua="1" um="1" id="85">
              <i ca="1" ac="A" sl="1" id="1" />
              <i ac="B" sl="0" id="2" />
            </c>
            <c wei="1" maS="1" ie="1" typ="10" rv="0" mv="0" ua="1" um="1" id="86">
              <i ac="A" sl="0" id="1" />
              <i ca="1" ac="B" sl="1" id="2" />
            </c>
            <c typ="4" id="91">
              <i cc="0" id="1" />
            </c>
            <c typ="4" id="95">
              <i cc="0" id="1" />
            </c>
          </s>
        </p>
      </item>
      <item id="881P3524" name="2.10 BTX FA13 KB 005" totalMark="6" version="21" markingType="1" markingState="1" userMark="0" markerUserMark="3" userAttempted="1" viewingTime="175984" flagged="0" LO="2.10 Tax return" unit="Business Tax" quT="11">
        <p um="0" cs="1" ua="1" id="881P3524">
          <s ua="0" um="-1" id="1">
            <c typ="4" id="25">
              <i cc="0" id="1" />
            </c>
            <c typ="4" id="29">
              <i cc="0" id="1" />
            </c>
          </s>
          <s ua="1" um="0" id="3">
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="2">
              <i ca="" id="1">97521</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="3">
              <i ca="" id="1">0</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="4">
              <i ca="" id="1">0</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="5">
              <i ca="" id="1">1160</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="6">
              <i ca="" id="1">0</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="7">
              <i ca="" id="1">2800</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="8">
              <i ca="" id="1">0</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="9">
              <i ca="" id="1">3441</i>
            </c>
          </s>
        </p>
      </item>
    </section>
  </assessment>
  <isValid>1</isValid>
  <isPrintable>0</isPrintable>
  <qualityReview>0</qualityReview>
  <numberOfGenerations>1</numberOfGenerations>
  <requiresInvigilation>1</requiresInvigilation>
  <advanceContentDownloadTimespan>60</advanceContentDownloadTimespan>
  <automaticVerification>0</automaticVerification>
  <offlineMode>2</offlineMode>
  <requiresValidation>0</requiresValidation>
  <requiresSecureClient>1</requiresSecureClient>
  <examType>0</examType>
  <advanceDownload>0</advanceDownload>
  <gradeBoundaryData>
    <gradeBoundaries passLevelType="1" adjustedGradeBoundaries="0">
      <grade modifier="lt" value="70" description="Fail" />
      <grade modifier="gt" value="70" description="Pass" />
    </gradeBoundaries>
  </gradeBoundaryData>
  <autoViewExam>0</autoViewExam>
  <autoProgressItems>0</autoProgressItems>
  <confirmationText>
    <confirmationText />
  </confirmationText>
  <requiresConfirmationCheck>0</requiresConfirmationCheck>
  <allowCloseWithoutSubmit>0</allowCloseWithoutSubmit>
  <useSecureMarker>0</useSecureMarker>
  <annotationVersion>0</annotationVersion>
  <allowPackagingDelivery>1</allowPackagingDelivery>
  <scoreBoundaryData>
    <scoreBoundaries showScoreBoundaryColumn="1">
      <score modifier="lt" value="36" description="Significantly below" higherBoundarySet="1" />
      <score modifier="lt" value="36" description="Significantly below" higherBoundarySet="0" />
      <score modifier="gt" value="36" description="Below requirement" higherBoundarySet="1" />
      <score modifier="gt" value="60" description="Borderline" higherBoundarySet="1" />
      <score modifier="gt" value="70" description="Met" higherBoundarySet="0" />
      <score modifier="gt" value="70" description="Met" higherBoundarySet="1" />
      <score modifier="gt" value="36" description="Below requirement" higherBoundarySet="0" userCreated="1" />
      <score modifier="gt" value="60" description="Borderline" higherBoundarySet="0" userCreated="1" />
      <score modifier="gt" value="85" description="Exceeded" higherBoundarySet="1" userCreated="1" />
    </scoreBoundaries>
  </scoreBoundaryData>
  <showCandidateReportResultColumn>0</showCandidateReportResultColumn>
  <showCandidateReportPercentageColumn>1</showCandidateReportPercentageColumn>
  <certifiedAccessible>0</certifiedAccessible>
  <markingProgressVisible>1</markingProgressVisible>
  <markingProgressMode>0</markingProgressMode>
  <secureClientOperationMode>1</secureClientOperationMode>
  <examDeliverySystemStyleType>delivery</examDeliverySystemStyleType>
  <markingAutoVoidPeriod>365</markingAutoVoidPeriod>
  <showPageRequiresScrollingAlert>0</showPageRequiresScrollingAlert>
  <project ID="881" />
</assessmentDetails>'
where ID = 1264267

update ExamSessionTable_Shredded
set StructureXML = '<assessmentDetails>
  <assessmentID>2032</assessmentID>
  <qualificationID>25</qualificationID>
  <qualificationName>Business Taxation (AQ2010)</qualificationName>
  <qualificationReference>AATQCF</qualificationReference>
  <assessmentGroupName>BTX FA 2013</assessmentGroupName>
  <assessmentGroupID>1918</assessmentGroupID>
  <assessmentGroupReference>CABTX</assessmentGroupReference>
  <assessmentName>BTX FA 2013</assessmentName>
  <validFromDate>06 Jan 2014</validFromDate>
  <expiryDate>31 Dec 2014</expiryDate>
  <startTime>00:00:00</startTime>
  <endTime>23:59:59</endTime>
  <duration>120</duration>
  <defaultDuration>120</defaultDuration>
  <scheduledDuration>
    <value>120</value>
    <reason></reason>
  </scheduledDuration>
  <sRBonus>0</sRBonus>
  <sRBonusMaximum>40</sRBonusMaximum>
  <externalReference>CABTX</externalReference>
  <passLevelValue>70</passLevelValue>
  <passLevelType>1</passLevelType>
  <status>2</status>
  <testFeedbackType>
    <passFail>0</passFail>
    <percentageMark>0</percentageMark>
    <allowSummaryFeedback>0</allowSummaryFeedback>
    <summaryFeedbackType>1</summaryFeedbackType>
    <itemSummary>0</itemSummary>
    <itemReview>0</itemReview>
    <itemReviewCorrectAnswer>0</itemReviewCorrectAnswer>
    <itemFeedback>0</itemFeedback>
    <printableSummary>0</printableSummary>
    <candidateDetails>0</candidateDetails>
    <feedbackByReference>0</feedbackByReference>
    <allowFeedbackDuringExam>0</allowFeedbackDuringExam>
    <allowFeedbackDuringSummary>0</allowFeedbackDuringSummary>
  </testFeedbackType>
  <itemFeedback>0</itemFeedback>
  <allowCalculator>0</allowCalculator>
  <lastInUseDate>09 Dec 2013 14:23:51</lastInUseDate>
  <completedScriptReview>0</completedScriptReview>
  <assessment currentSection="1" totalTime="0" currentTime="0">
    <intro id="0" name="Introduction" currentItem="">
      <item id="881P3590" name="Introduction" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
    </intro>
    <outro id="0" name="Finish" currentItem="" />
    <tools id="0" name="Tools" currentItem="">
      <item id="881P3366" name="BTAX tax data FA13 pop up 1" toolName="BTAX tax data FA13 pop up 1" totalMark="1" version="6" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="881P3367" name="BTAX tax data FA13 pop up 2" toolName="BTAX tax data FA13 pop up 2" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
    </tools>
    <section id="1" name="1" passLevelValue="60" passLevelType="1" itemsToMark="0" currentItem="881P3415" fixed="0">
      <item id="881P3374" name="1.1 BTX FA13 KB 003" totalMark="3" version="16" markingType="0" markingState="0" userMark="1" markerUserMark="" userAttempted="1" viewingTime="18736" flagged="0" LO="1.1 Determine if trade is being carried on/distinction between capital and revenue expenditure" unit="Business Tax" quT="16" />
      <item id="881P3389" name="1.2 BTX FA13 KB 009" totalMark="7" version="18" markingType="0" markingState="0" userMark="1" markerUserMark="" userAttempted="1" viewingTime="132195" flagged="0" LO="1.2 Adjusted profits" unit="Business Tax" quT="11" />
      <item id="881P3393" name="1.3 BTX FA13 KB 004" totalMark="6" version="29" markingType="0" markingState="0" userMark="1" markerUserMark="" userAttempted="1" viewingTime="209071" flagged="0" LO="1.3 Assessment for sole traders and partnerships" unit="Business Tax" quT="11,20" />
      <item id="881P3408" name="1.4 BTX FA13 KB 007" totalMark="4" version="30" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="1" viewingTime="66362" flagged="0" LO="1.4 Relief of trading loss" unit="Business Tax" quT="11" />
      <item id="881P3415" name="1.5 BTX FA13 KB 005" totalMark="9" version="31" markingType="0" markingState="0" userMark="0.55555555" markerUserMark="" userAttempted="1" viewingTime="128497" flagged="0" LO="1.5 Allocation of profits within a partnership" unit="Business Tax" quT="11" />
      <item id="881P3423" name="1.6 BTX FA13 KB 004" totalMark="5" version="23" markingType="0" markingState="0" userMark="0.8" markerUserMark="" userAttempted="1" viewingTime="45490" flagged="0" LO="1.6 Profits chargeable to corporation tax, short/long periods of account" unit="Business Tax" quT="10" />
      <item id="881P3433" name="1.7 BTX FA13 KB 005" totalMark="16" version="36" markingType="1" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="446223" flagged="0" LO="1.7 Capital allowances" unit="Business Tax" quT="20" />
    </section>
    <section id="2" name="2" passLevelValue="60" passLevelType="1" itemsToMark="0" currentItem="881P3524" fixed="0">
      <item id="881P3440" name="2.1 BTX FA13 KB 002" totalMark="2" version="18" markingType="0" markingState="0" userMark="1" markerUserMark="" userAttempted="1" viewingTime="127702" flagged="0" LO="2.1 Chargeable persons/assets/disposal, connected persons, capital gains and losses" unit="Business Tax" quT="16" />
      <item id="881P3452" name="2.2 BTX FA13 KB 005" totalMark="5" version="33" markingType="0" markingState="0" userMark="0.8" markerUserMark="" userAttempted="1" viewingTime="122273" flagged="0" LO="2.2 Capital disposals - gains and losses" unit="Business Tax" quT="11" />
      <item id="881P3461" name="2.3 BTX FA13 KB 005" totalMark="7" version="43" markingType="1" markingState="1" userMark="1" markerUserMark="2" userAttempted="1" viewingTime="438736" flagged="0" LO="2.3 Capital disposals - shares" unit="Business Tax" quT="20" />
      <item id="881P3473" name="2.4 BTX FA13 KB 008" totalMark="4" version="16" markingType="0" markingState="0" userMark="0.5" markerUserMark="" userAttempted="1" viewingTime="83897" flagged="0" LO="2.4 Capital gain tax reliefs" unit="Business Tax" quT="10" />
      <item id="881P3483" name="2.5 BTX FA13 KB 009" totalMark="3" version="12" markingType="0" markingState="0" userMark="0.33333333" markerUserMark="" userAttempted="1" viewingTime="131447" flagged="0" LO="2.5 Rollover relief" unit="Business Tax" quT="11" />
      <item id="881P3492" name="2.6 BTX FA13 KB 009" totalMark="4" version="15" markingType="0" markingState="0" userMark="0.5" markerUserMark="" userAttempted="1" viewingTime="498360" flagged="0" LO="2.6 Class 2 and 4 NIC" unit="Business Tax" quT="11" />
      <item id="881P3495" name="2.7 BTX FA13 KB 003" totalMark="4" version="12" markingType="0" markingState="0" userMark="0.75" markerUserMark="" userAttempted="1" viewingTime="73444" flagged="0" LO="2.7 Self-assessment" unit="Business Tax" quT="10" />
      <item id="881P3503" name="2.8 BTX FA13 KB 002" totalMark="9" version="19" markingType="0" markingState="0" userMark="0.66666666" markerUserMark="" userAttempted="1" viewingTime="370391" flagged="0" LO="2.8 Corporation tax payable" unit="Business Tax" quT="11" />
      <item id="881P3512" name="2.9 BTX FA13 KB 002" totalMark="6" version="17" markingType="0" markingState="0" userMark="0.83333333" markerUserMark="" userAttempted="1" viewingTime="88155" flagged="0" LO="2.9 Penalties, surcharge and interest" unit="Business Tax" quT="10" />
      <item id="881P3524" name="2.10 BTX FA13 KB 005" totalMark="6" version="21" markingType="1" markingState="1" userMark="0" markerUserMark="3" userAttempted="1" viewingTime="175984" flagged="0" LO="2.10 Tax return" unit="Business Tax" quT="11" />
    </section>
  </assessment>
  <isValid>1</isValid>
  <isPrintable>0</isPrintable>
  <qualityReview>0</qualityReview>
  <numberOfGenerations>1</numberOfGenerations>
  <requiresInvigilation>1</requiresInvigilation>
  <advanceContentDownloadTimespan>60</advanceContentDownloadTimespan>
  <automaticVerification>0</automaticVerification>
  <offlineMode>2</offlineMode>
  <requiresValidation>0</requiresValidation>
  <requiresSecureClient>1</requiresSecureClient>
  <examType>0</examType>
  <advanceDownload>0</advanceDownload>
  <gradeBoundaryData>
    <gradeBoundaries passLevelType="1" adjustedGradeBoundaries="0">
      <grade modifier="lt" value="70" description="Fail" />
      <grade modifier="gt" value="70" description="Pass" />
    </gradeBoundaries>
  </gradeBoundaryData>
  <autoViewExam>0</autoViewExam>
  <autoProgressItems>0</autoProgressItems>
  <confirmationText>
    <confirmationText></confirmationText>
  </confirmationText>
  <requiresConfirmationCheck>0</requiresConfirmationCheck>
  <allowCloseWithoutSubmit>0</allowCloseWithoutSubmit>
  <useSecureMarker>0</useSecureMarker>
  <annotationVersion>0</annotationVersion>
  <allowPackagingDelivery>1</allowPackagingDelivery>
  <scoreBoundaryData>
    <scoreBoundaries showScoreBoundaryColumn="1">
      <score modifier="lt" value="36" description="Significantly below" higherBoundarySet="1" />
      <score modifier="lt" value="36" description="Significantly below" higherBoundarySet="0" />
      <score modifier="gt" value="36" description="Below requirement" higherBoundarySet="1" />
      <score modifier="gt" value="60" description="Borderline" higherBoundarySet="1" />
      <score modifier="gt" value="70" description="Met" higherBoundarySet="0" />
      <score modifier="gt" value="70" description="Met" higherBoundarySet="1" />
      <score modifier="gt" value="36" description="Below requirement" higherBoundarySet="0" userCreated="1" />
      <score modifier="gt" value="60" description="Borderline" higherBoundarySet="0" userCreated="1" />
      <score modifier="gt" value="85" description="Exceeded" higherBoundarySet="1" userCreated="1" />
    </scoreBoundaries>
  </scoreBoundaryData>
  <showCandidateReportResultColumn>0</showCandidateReportResultColumn>
  <showCandidateReportPercentageColumn>1</showCandidateReportPercentageColumn>
  <certifiedAccessible>0</certifiedAccessible>
  <markingProgressVisible>1</markingProgressVisible>
  <markingProgressMode>0</markingProgressMode>
  <secureClientOperationMode>1</secureClientOperationMode>
  <examDeliverySystemStyleType>delivery</examDeliverySystemStyleType>
  <markingAutoVoidPeriod>365</markingAutoVoidPeriod>
  <showPageRequiresScrollingAlert>0</showPageRequiresScrollingAlert>
  <project ID="881" />
</assessmentDetails>', resultData = '<exam passMark="70" passType="1" originalPassMark="70" originalPassType="1" totalMark="100" userMark="55.000" userPercentage="55.000" passValue="1" originalPassValue="1" totalTimeSpent="3156963" closeBoundaryType="0" closeBoundaryValue="0" grade="Pass" originalGrade="Pass">
  <gradeBoundaryData>
    <gradeBoundaries passLevelType="1" adjustedGradeBoundaries="0">
      <grade modifier="lt" value="70" description="Fail" />
      <grade modifier="gt" value="70" description="Pass" />
    </gradeBoundaries>
  </gradeBoundaryData>
  <section id="1" passMark="60" passType="1" name="1" totalMark="50" userMark="41.000" userPercentage="82.000" passValue="1" totalTimeSpent="1046574" itemsToMark="0">
    <item id="881P3374" name="1.1 BTX FA13 KB 003" totalMark="3" userMark="1" actualUserMark="3" markingType="0" markerUserMark="" viewingTime="18736" userAttempted="1" version="16" />
    <item id="881P3389" name="1.2 BTX FA13 KB 009" totalMark="7" userMark="1" actualUserMark="7" markingType="0" markerUserMark="" viewingTime="132195" userAttempted="1" version="18" />
    <item id="881P3393" name="1.3 BTX FA13 KB 004" totalMark="6" userMark="1" actualUserMark="6" markingType="0" markerUserMark="" viewingTime="209071" userAttempted="1" version="29" />
    <item id="881P3408" name="1.4 BTX FA13 KB 007" totalMark="4" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="66362" userAttempted="1" version="30" />
    <item id="881P3415" name="1.5 BTX FA13 KB 005" totalMark="9" userMark="0.55555555" actualUserMark="4.99999995" markingType="0" markerUserMark="" viewingTime="128497" userAttempted="1" version="31" />
    <item id="881P3423" name="1.6 BTX FA13 KB 004" totalMark="5" userMark="0.8" actualUserMark="4.0" markingType="0" markerUserMark="" viewingTime="45490" userAttempted="1" version="23" />
    <item id="881P3433" name="1.7 BTX FA13 KB 005" totalMark="16" userMark="0" actualUserMark="16" markingType="1" markerUserMark="" viewingTime="446223" userAttempted="0" version="36" />
  </section>
  <section id="2" passMark="60" passType="1" name="2" totalMark="50" userMark="30.000" userPercentage="60.000" passValue="1" totalTimeSpent="2110389" itemsToMark="0">
    <item id="881P3440" name="2.1 BTX FA13 KB 002" totalMark="2" userMark="1" actualUserMark="2" markingType="0" markerUserMark="" viewingTime="127702" userAttempted="1" version="18" />
    <item id="881P3452" name="2.2 BTX FA13 KB 005" totalMark="5" userMark="0.8" actualUserMark="4.0" markingType="0" markerUserMark="" viewingTime="122273" userAttempted="1" version="33" />
    <item id="881P3461" name="2.3 BTX FA13 KB 005" totalMark="7" userMark="0" actualUserMark="2" markingType="1" markerUserMark="2" viewingTime="438736" userAttempted="1" version="43" />
    <item id="881P3473" name="2.4 BTX FA13 KB 008" totalMark="4" userMark="0.5" actualUserMark="2.0" markingType="0" markerUserMark="" viewingTime="83897" userAttempted="1" version="16" />
    <item id="881P3483" name="2.5 BTX FA13 KB 009" totalMark="3" userMark="0.33333333" actualUserMark="0.99999999" markingType="0" markerUserMark="" viewingTime="131447" userAttempted="1" version="12" />
    <item id="881P3492" name="2.6 BTX FA13 KB 009" totalMark="4" userMark="0.5" actualUserMark="2.0" markingType="0" markerUserMark="" viewingTime="498360" userAttempted="1" version="15" />
    <item id="881P3495" name="2.7 BTX FA13 KB 003" totalMark="4" userMark="0.75" actualUserMark="3.00" markingType="0" markerUserMark="" viewingTime="73444" userAttempted="1" version="12" />
    <item id="881P3503" name="2.8 BTX FA13 KB 002" totalMark="9" userMark="0.66666666" actualUserMark="5.99999994" markingType="0" markerUserMark="" viewingTime="370391" userAttempted="1" version="19" />
    <item id="881P3512" name="2.9 BTX FA13 KB 002" totalMark="6" userMark="0.83333333" actualUserMark="4.99999998" markingType="0" markerUserMark="" viewingTime="88155" userAttempted="1" version="17" />
    <item id="881P3524" name="2.10 BTX FA13 KB 005" totalMark="6" userMark="0" actualUserMark="3" markingType="1" markerUserMark="3" viewingTime="175984" userAttempted="1" version="21" />
  </section>
</exam>'
where examSessionid = 1264267


select * from ExamSessionItemResponseTable where ExamSessionID = 1264267--
	and ExamSessionItemResponseTable.ItemID = '881P3433'

update ExamSessionItemResponseTable 
set MarkerResponseData = '<entries>
  <entry>
    <comment>Computer marked where applicable</comment>
    <userId>-1</userId>
    <userForename>Computer</userForename>
    <userSurname>Marked</userSurname>
    <assignedMark>0</assignedMark>
    <version>0</version>
    <dateCreated>16/04/2014</dateCreated>
  </entry>
</entries>'
where ExamSessionID = 1264267--
	and ExamSessionItemResponseTable.ItemID = '881P3433'
