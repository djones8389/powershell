--SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
--SELECT ExamSessionTable.ID,ExamSessionTable.examState,ExamSessionTable.KeyCode
--FROM dbo.ExamSessionTable
--WHERE ExamSessionTable.ExamState in (15,16)
--AND CAST(ExamSessionTable.StructureXML AS XML).exist('//item[@userAttempted="1"]') = 0
--AND EXISTS(
--       SELECT TOP(1) 1
--       FROM dbo.ExamSessionItemResponseTable
--       WHERE ExamSessionItemResponseTable.ExamSessionID = ExamSessionTable.ID
--       AND ItemResponseData.exist('//*[@ua="1" and @typ!="20"]') = 1
--       );

--Find incorrectly updated cumulative marks entries in Live Tables
/*
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
Select *
from CumulativeMarkingTable 
where CumulativeUserMarks = 0
and ExamSessionId IN (
      select ExamSessionID
      from ExamSessionItemResponseTable
      
      inner join ExamSessionTable as EST
      on EST.ID = ExamSessionItemResponseTable.ExamSessionID
      
      
      where MarkerResponseData.exist('entries/entry[assignedMark > 0]') = 1
);

--Find none marked scripts in Warehouse*/

use AAT_SecureAssess

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

SELECT TOP 1000 WAREHOUSE_ExamSessionTable.ID
	,WAREHOUSE_ExamSessionTable.KeyCode
	,resultDataFull
	,WAREHOUSE_ExamSessionTable.PreviousExamState
	,WAREHOUSE_ExamSessionTable.ExportToSecureMarker
	,WAREHOUSE_ExamSessionTable.warehouseTime
FROM WAREHOUSE_ExamSessionTable
WHERE ID IN (
		SELECT DISTINCT examSessionId
		FROM [AAT_SecureAssess].[dbo].[WAREHOUSE_ExamSessionTable_ShrededItems] AS WESTSI
		WHERE ExamSessionID IN (
				SELECT DISTINCT WEST.ID
				FROM WAREHOUSE_ExamSessionTable_ShrededItems WESTSI
				INNER JOIN WAREHOUSE_ExamSessionTable AS WEST ON WESTSI.examSessionId = WEST.ID
				INNER JOIN WAREHOUSE_UserTable AS WUT ON WUT.ID = WEST.WAREHOUSEUserID
				INNER JOIN WAREHOUSE_ExamSessionTable_Shreded AS WES ON WES.examSessionId = WEST.ID
				WHERE WESTSI.userMark = 0
					AND examPercentage = 0
					AND userAttempted = 0
					AND correctAnswerCount > 0
					AND WEST.warehouseTime > '2014-07-14'
					AND WEST.PreviousExamState <> 10
					AND WES.examVersionRef NOT LIKE '%Practice'
				)
		)
	AND ID NOT IN (	
		1366170	,1366432 ,1368194 ,1366651 ,1375402 ,1372673,1380414, 1380421,1381075,1382448, 1388011, 1387931, 1388286, 1394724, 1399580, 1398755, 1398455, 1399859, 1405549
			, 1406905, 1411148, 1412878, 1413735, 1414328, 1414577, 1416129, 1428355,  1427422, 1432411
	);
	