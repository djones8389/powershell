SELECT EST.ID, EST.KeyCode, EST.examState, UT.Forename, UT.Surname, ut.CandidateRef, CT.CentreName, SCET.examName, IsProjectBased
	, scet.ScheduledStartDateTime, scet.ScheduledEndDateTime, CreatedDateTime, DATEDIFF(day,ScheduledStartDateTime, ScheduledEndDateTime)
  FROM ExamSessionTable as EST

  Inner Join ScheduledExamsTable as SCET
  on SCET.ID = EST.ScheduledExamID
  
  Inner Join UserTable as UT
  on UT.ID = EST.UserID

  Inner Join CentreTable as CT
  on CT.ID = SCET.CentreID

where est.ID in (1475227,1497737,1503447,1503497)		
		
--select * from StateManagementException where ExamSession

--update ExamSessionTable
--set previousExamState = examState, examState = 1
--where ID in (1475227,1497737,1503447,1503497)
--select * from ExamStateChangeAuditTable where ExamSessionID in (1475227,1497737,1503447,1503497)
--select id, keycode, resultData from WAREHOUSE_ExamSessionTable where ExamSessionID in (1475227,1497737,1503447,1503497)


SELECT top 10 WEST.ID, WEST.KeyCode, WUT.Forename, WUT.Surname, WUT.CandidateRef, WCT.CentreName, WSCET.examName, WEST.completionDate
	, WESTS.ExternalReference,   DATEDIFF(day,ScheduledStartDateTime, ScheduledEndDateTime)
FROM WAREHOUSE_ExamSessionTable as WEST

  Inner Join WAREHOUSE_ScheduledExamsTable as WSCET
  on WSCET.ID = WEST.WAREHOUSEScheduledExamID
  
  Inner Join WAREHOUSE_UserTable as WUT
  on WUT.ID = WEST.WAREHOUSEUserID

  Inner Join WAREHOUSE_CentreTable as WCT
  on WCT.ID = WSCET.WAREHOUSECentreID

  Inner Join WAREHOUSE_ExamSessionTable_Shreded as WESTS
  on WESTS.examSessionId = WEST.ID

where west.ID In (1484578,1483008,1482781,1482780,1482779,1482778,1482777,482776,1482775,1482774)