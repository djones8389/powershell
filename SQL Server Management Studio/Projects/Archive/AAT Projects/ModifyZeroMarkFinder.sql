SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

SELECT WAREHOUSE_ExamSessionTable.ID
	,WAREHOUSE_ExamSessionTable.KeyCode
	,cast(resultDataFull AS NVARCHAR(max)) AS ResultDataFull
	,WAREHOUSE_ExamSessionTable.PreviousExamState
	,WAREHOUSE_ExamSessionTable.ExportToSecureMarker
	,WAREHOUSE_ExamSessionTable.warehouseTime
	,sum(WAREHOUSE_ExamSessionItemResponseTable.ItemResponseData.value('count(p[@um = "1"])', 'int')) AS [correctAnswerCount1]
FROM WAREHOUSE_ExamSessionTable
INNER JOIN WAREHOUSE_ExamSessionItemResponseTable ON WAREHOUSE_ExamSessionItemResponseTable.WAREHOUSEExamSessionID = WAREHOUSE_ExamSessionTable.ID
WHERE WAREHOUSE_ExamSessionTable.ID IN (
		SELECT DISTINCT examSessionId
		FROM [AAT_SecureAssess].[dbo].[WAREHOUSE_ExamSessionTable_ShrededItems] AS WESTSI
		WHERE ExamSessionID IN (
				SELECT DISTINCT WEST.ID
				FROM WAREHOUSE_ExamSessionTable_ShrededItems WESTSI
				INNER JOIN WAREHOUSE_ExamSessionTable AS WEST ON WESTSI.examSessionId = WEST.ID
				INNER JOIN WAREHOUSE_UserTable AS WUT ON WUT.ID = WEST.WAREHOUSEUserID
				INNER JOIN WAREHOUSE_ExamSessionTable_Shreded AS WES ON WES.examSessionId = WEST.ID
				WHERE WESTSI.userMark = 0
					AND examPercentage = 0
					AND userAttempted = 0
					--AND correctAnswerCount > 0
					AND WEST.warehouseTime > '2014-07-14'
					AND WEST.PreviousExamState <> 10
					AND WES.examVersionRef NOT LIKE '%Practice'
				)
		)
	
GROUP BY WAREHOUSE_ExamSessionTable.ID
	,WAREHOUSE_ExamSessionTable.KeyCode
	,cast(resultDataFull AS NVARCHAR(max))
	,WAREHOUSE_ExamSessionTable.PreviousExamState
	,WAREHOUSE_ExamSessionTable.ExportToSecureMarker
	,WAREHOUSE_ExamSessionTable.warehouseTime
	
	order by sum(WAREHOUSE_ExamSessionItemResponseTable.ItemResponseData.value('count(p[@um = "1"])', 'int')) desc;