select ID, ExamSessionID , KeyCode ,StructureXML, resultData, resultDataFull, ExportToSecureMarker
	from WAREHOUSE_ExamSessionTable
		where ID = 1401159;
		
		
		
SELECT  E.ID AS [WAREHOUSEExamSessionID]  
  ,I.ItemRef AS [itemID]  
  ,I.itemVersion  
  ,CASE LEN(I.MarkerUserMark)  
   WHEN 0 THEN I.UserMark * I.TotalMark  
   ELSE I.MarkerUserMark  
   END AS [userMark]  
  ,I.userAttempted AS [userAttempt]  
  ,I.totalMark  
  ,ISNULL(I.markingIgnored, 0) AS [markingIgnored]  
  ,I.markedMetadataCount  
 FROM  WAREHOUSE_ExamSessionTable E  
 INNER JOIN WAREHOUSE_ExamSessionTable_ShrededItems I  
   ON E.ID = I.ExamSessionID  
   Where I.ExamSessionID = (1401159)  ;
