declare @Keycode nvarchar(10) = 'VMA5QUA6'

declare @ID int = (
select ID
	from ExamSessionTable
		where KeyCode = @Keycode
	)


create table #tempTable

(
      ID2 int,
      TotalNumberOfQuestions int
)

insert into #tempTable

      select
                  ID,
                  StructureXML.value('count(/assessmentDetails/assessment/section/item)[1]', 'int')
      from ExamSessionTable
     where KeyCode = @Keycode
      
      
	select 				
		   COUNT (ExamSessionID) as NumberOfQuestionsAnswered
		   ,  TT.TotalNumberOfQuestions      

		from ExamSessionItemResponseTable as ESIRT

		inner join ExamSessionTable as EST
		on EST.ID = ESIRT.ExamSessionID
		
		inner join #tempTable as TT
        on TT.ID2 = ESIRT.ExamSessionID
		
		where KeyCode = @Keycode
		
	group by ExamSessionID, TT.TotalNumberOfQuestions 


	update ExamSessionTable
	set previousExamState = examState,
		examState = 10
	where KeyCode = @Keycode

	and ID not in
	
	(
	select ExamSessionID
		from ExamSessionItemResponseTable
		)

select
         ID
         ,examState
      from ExamSessionTable
     where KeyCode = @Keycode
     

drop table #tempTable
