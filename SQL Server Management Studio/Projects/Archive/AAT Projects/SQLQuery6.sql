BEGIN TRAN

update WAREHOUSE_ExamSessionTable_Shreded
set resultData = 
'<exam passMark="70" passType="1" originalPassMark="70" originalPassType="1" totalMark="150" userMark="131.000" userPercentage="87.333" passValue="1" oiginalPassValue="0" totalTimeSpent="5378563" closeBoundaryType="0" closeBoundaryValue="0" grade="Pass" originalGrade="Pass">
  <gradeBoundaryData>
    <gradeBoundaries passLevelType="1" adjustedGradeBoundaries="0">
      <grade modifier="lt" value="70" description="Fail" />
      <grade modifier="gt" value="70" description="Pass" />
    </gradeBoundaries>
  </gradeBoundaryData>
  <section id="1" passMark="60" passType="1" name="1" totalMark="90" userMark="80.000" userPercentage="88.889" passValue="0" totalTimeSpent="3151096" itemsToMark="0">
    <item id="879P1324" name="P 1.1 FNST AC003" totalMark="19" userMark="0" actualUserMark="0.0000000000" markingType="0" markerUserMark="19.0000000000" viewingTime="639522" userAttempted="1" version="14" />
    <item id="879P1316" name="P 1.2 FNST AC003" totalMark="17" userMark="0" actualUserMark="0.0000000000" markingType="0" markerUserMark="17.0000000000" viewingTime="529870" userAttempted="1" version="9" />
    <item id="879P1698" name="1.3 FNST AC 010" totalMark="12" userMark="0" actualUserMark="10.0000000000" markingType="1" markerUserMark="10.0000000000" viewingTime="403885" userAttempted="1" version="22" />
    <item id="879P1828" name="1.4 FNST DO 012" totalMark="12" userMark="0" actualUserMark="0.0000000000" markingType="0" markerUserMark="8.0000000000" viewingTime="441294" userAttempted="1" version="24" />
    <item id="879P1997" name="1.5 FNST AC 013" totalMark="30" userMark="0" actualUserMark="0.0000000000" markingType="0" markerUserMark="26.0000000000" viewingTime="1136525" userAttempted="1" version="12" />
  </section>
  <section id="2" passMark="60" passType="1" name="2" totalMark="60" userMark="51.000" userPercentage="85.000" passValue="0" totalTimeSpent="2227467" itemsToMark="0">
    <item id="879P1734" name="2.1 FNST DO 010" totalMark="32" userMark="0" actualUserMark="0.0000000000" markingType="0" markerUserMark="32.0000000000" viewingTime="562007" userAttempted="1" version="21" />
    <item id="879P2089" name="2.2 FNST AC 002 2011" totalMark="18" userMark="0" actualUserMark="15.0000000000" markingType="1" markerUserMark="15.0000000000" viewingTime="1097837" userAttempted="1" version="26" />
    <item id="879P1523" name="2.3 FNST AC 005" totalMark="10" userMark="0" actualUserMark="4.0000000000" markingType="1" markerUserMark="4.0000000000" viewingTime="567623" userAttempted="1" version="15" />
  </section>
</exam>'
where examSessionId = 1383918;

ROLLBACK