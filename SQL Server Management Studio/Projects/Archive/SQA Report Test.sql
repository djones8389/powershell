DECLARE @tmp xml   
declare @ProjectID int = 11

SELECT @tmp = ProjectStructureXml
FROM dbo.ProjectListTable 
WHERE ID=@ProjectID

DECLARE @DocHandle int

EXEC sp_xml_preparedocument @DocHandle OUTPUT, @tmp

;WITH recycled AS(
	SELECT * FROM OPENXML(@DocHAndle,'//Rec//Pag')
	WITH (
		ID  int
	)
), pages AS (
	SELECT * FROM OPENXML(@DocHAndle,'//Pag[
		(not(@markingSchemeFor) or @markingSchemeFor = -1) 
		and (not(@isSourceItem) or @isSourceItem = "F")
		and (not(@Typ=6 or @Typ=14 or @Typ=15))
		]')
	WITH (
		ID  int,
		Nam NVARCHAR(max),
		lMD NVARCHAR(23),
		sta INT,
		FolID INT '../@ID'
	) p
	WHERE NOT EXISTS(SELECT TOP 1 1 FROM recycled r WHERE r.ID = p.ID)
), folders AS (
	SELECT * FROM OPENXML(@DocHAndle,'//Fol')
	WITH (
		ID  int,
		Nam NVARCHAR(max),
		ParentID INT '../@ID'
	)
),rootFolders AS (
	SELECT t.c.value('@ID','int') ID
	FROM @tmp.nodes('Pro/Fol') t(c)
), structure AS (
	SELECT f.ID, Nam, ParentID, Nam AS fPath FROM folders f
	--WHERE ParentID = @tmp.value('Pro[1]/@ID[1]', 'int')
		JOIN rootFolders rf ON rf.ID = f.ID
	UNION ALL
	SELECT f.ID, f.Nam, f.ParentID, s.fPath + '/' + f.Nam AS fPath FROM folders f JOIN structure s ON s.ID = f.ParentID
), statusList AS (
	SELECT	 
		 Sta.value('@id', 'NVARCHAR(255)') AS [ID],
		 Sta.value('@val', 'NVARCHAR(255)') AS [Sta],
		 Sta.value('.', 'NVARCHAR(255)') AS [StatusDesc]
	FROM	(
		SELECT CAST(REPLACE(CAST(ProjectDefaultXML AS NVARCHAR(MAX)), N'UTF-8', N'UTF-16') AS XML) DefaultXML
		FROM ProjectListTable
		WHERE ID=@ProjectID
	) T
	CROSS APPLY T.DefaultXML.nodes('Defaults/StatusList/Status') StatusList(Sta)	
), final AS (
	SELECT 
		1 AS ToAgg
		,P.ID
		,ISNULL(fPath, '/ROOT') FolPath
		,p.sta [PagStatID]
		,ISNULL(StatusList.StatusDesc, N'Without Status') AS [PagStat] 
		--,lMD
		,CONVERT(datetime, p.lMD, 103) AS lastModified
		
	FROM pages p
		LEFT JOIN structure s ON s.ID = p.FolID
		LEFT JOIN StatusList ON p.sta = StatusList.ID 
)
SELECT * FROM final
--date filtering is implemented on report side instead of DB
--WHERE lastModified&gt;=@StartDate

--EXEC sp_xml_removedocument @DocHandle SELECT ID, Name
--FROM ProjectListTable
--ORDER BY Name