DECLARE @Location nvarchar(100) = 'T:\Backup'

SELECT N'BACKUP DATABASE ['+name+N'] TO DISK = N''' +  @Location + '\' + 'DJ_STG_SANDBOX_SQA_CPProjectAdmin' + CONVERT(VARCHAR(10), GETDATE(), 102) + '.bak'' WITH NAME = N'''+name+N'- Database Backup'', COPY_ONLY, COMPRESSION, NOFORMAT, NOINIT, SKIP, COMPRESSION, NOREWIND, NOUNLOAD, STATS = 10;'  
FROM sys.databases 
where name like '%SAND%'
	AND database_id > 4


BACKUP DATABASE [STG_SANDBOX_SQA_CPProjectAdmin] 
	TO DISK = N'T:\Backup\DJ_STG_SANDBOX_SQA_CPProjectAdmin2015.10.26.bak' 
		WITH NAME = N'STG_SANDBOX_SQA_CPProjectAdmin- Database Backup'
		, COPY_ONLY, COMPRESSION, NOFORMAT, NOINIT, SKIP, COMPRESSION, NOREWIND, NOUNLOAD, STATS = 10;