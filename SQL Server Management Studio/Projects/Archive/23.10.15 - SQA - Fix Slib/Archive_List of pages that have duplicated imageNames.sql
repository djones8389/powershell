USE STG_SANDBOX_SQA_CPProjectAdmin

IF OBJECT_ID ('tempdb..#Pages') IS NOT NULL DROP TABLE #Pages;

select A.id			
	 , A.name
	 , A.Projectid
	 , A.DL
	 , Items.ID as ItemID
INTO #Pages
from (

select id, name, projectid, ISNULL(DATALENGTH(IMAGE), 0) as DL				
from ProjectManifestTable as PMT with (NOLOCK)
--where name in (
--		select name
--		from projectManifestTable with (NOLOCK)
--		group by name, projectID
--			having count(*) > 1
--	)
where location not like '%background%'

) A

LEFT JOIN								
(
      SELECT ID, ali
      FROM ItemCustomQuestionTable with (NOLOCK)
      UNION
      SELECT ID, ali
      FROM ItemGraphicTable with (NOLOCK)
      UNION
      SELECT ID, ali
      FROM ItemVideoTable with (NOLOCK)
      UNION
      SELECT ID, ali
      FROM ItemHotSpotTable with (NOLOCK)
)  as Items
ON 
      Items.ID LIKE CONVERT(NVARCHAR(10), A.ProjectId) + 'P%' 
      AND 
      (
            CONVERT(NVARCHAR(50), A.ID) = ali
            OR
            SUBSTRING(A.name, 0, LEN(A.name) - 3) = ali
      )

SELECT * FROM #Pages

--/*Replace ALL broken images with a working one, with the same name*/

--	UPDATE ProjectManifestTable 
--	SET IMAGE =
--			(
--			SELECT TOP 1 IMAGE
--			FROM ProjectManifestTable A

--			INNER JOIN #Pages B
--			on B.name = A.Name
				
--			WHERE B.DL = 0
--				AND DATALENGTH(A.IMAGE) > 0
--				)
--	FROM ProjectManifestTable A
--	INNER JOIN #Pages B
--	on B.name = A.Name

--	WHERE B.DL = 0;

--/*Set delCol on unused images that are broken*/


--	UPDATE ProjectManifestTable 
--	SET DelCol = 1
--	FROM ProjectManifestTable A
--	INNER JOIN #Pages B
--	on B.name = A.Name
--		AND A.ID = B.ID
--	WHERE B.DL = 0
--		AND B.itemID is null;

--/*Set delCol on all unused images*/

	--UPDATE ProjectManifestTable
	--set delcol = 1	
	--FROM ProjectManifestTable A
	--INNER JOIN
	--	(
	--		SELECT name
	--			, projectID
	--			, itemID
	--		FROM #PAGES  

	--	) B
	--On A.Name = B.Name
	--	and A.projectID = B.ProjectID	
	--WHERE B.itemID  IS NULL;



		--select name, projectID
		--from #PAGES  
		--group by name, projectID
		--	having count(*) > 1
			
		--select name, projectID
		--from ProjectManifestTable  
		--group by name, projectID
		--	having count(*) > 1
			

;--@MyItems;			

/*

DECLARE SharedLibrary CURSOR FOR 
SELECT ID
	  , Name
	  , ProjectID
FROM @MyItems
Where SLProjectID IS NOT NULL		--Only include in the cursor where entry is in the sharedLibraryTable--

select projectid, cast(Structurexml as xml) as Structurexml
into #SharedLibStructure
from SharedLibraryTable
where projectid in (select ProjectID from @MyItems)

select * from #SharedLibStructure	--Select out your SLT to store for backup purposes

DECLARE @ID int, @Name nvarchar(max), @ProjectID int;

OPEN SharedLibrary;

FETCH NEXT FROM SharedLibrary INTO  @ID, @Name , @ProjectID

WHILE @@Fetch_Status = 0

BEGIN

Update #SharedLibStructure
set Structurexml.modify('delete(/sharedLibrary/item[@id=sql:variable("@ID")]  [@name=sql:variable("@Name")]) ')
where projectid = @ProjectID

FETCH NEXT FROM SharedLibrary INTO  @ID, @Name , @ProjectID

END
CLOSE SharedLibrary;
DEALLOCATE SharedLibrary;


update SharedLibraryTable
set structureXML = CAST(b.Structurexml as nvarchar(MAX))
from SharedLibraryTable a
inner join #SharedLibStructure b
on a.ProjectId = b.ProjectId
where a.ProjectId = b.ProjectId;


DROP TABLE #SharedLibStructure;


delete from ProjectManifestTable 
	where (projectid in (select ProjectID from @MyItems) 
				and name in (select name from @MyItems) 
					and ID in (select ID from @MyItems)
					);
*/















--INNER join   

--(
--select b.projectid						
--	, a.b.value ('@id', 'int') as  ItemID
--from
--(
--	select projectid
--		, cast(Structurexml as xml) as Structurexml
--	from SharedLibraryTable as SLT with (NOLOCK)
--	where projectid = @MyProjectID
--	) B
--	cross apply Structurexml.nodes('//item') a(b)
--) B 

--on A.ID = b.Itemid
--	and a.projectid = b.projectid
