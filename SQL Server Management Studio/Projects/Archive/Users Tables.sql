select 
	 AssignedUserRolesTable.UserID
	, AssignedUserRolesTable.CentreID
	, CentreTable.CentreName
	, AssignedUserRolesTable.RoleID
	, RolesTable.Name
	, UserTable.Forename
	, UserTable.Surname
	, UserTable.CandidateRef
	, UserTable.Username
	, UserTable.IsCandidate
	, UserTable.Password
	, UserTable.Retired
from UserTable 

inner join AssignedUserRolesTable
on AssignedUserRolesTable.UserID = UserTable.ID

inner join RolesTable
on RolesTable.ID = AssignedUserRolesTable.RoleID

inner join CentreTable
on CentreTable.ID = AssignedUserRolesTable.CentreID

where UserID in 

(
	select AssignedUserRolesTable.UserID--, AssignedUserRolesTable.RoleID

	from UserTable 

	inner join AssignedUserRolesTable
	on AssignedUserRolesTable.UserID = UserTable.ID

		where IsCandidate = 1
		and Password != 'password'
		and RoleID != 2
)

and RolesTable.Name != 'Base'
	
		order by Username asc





