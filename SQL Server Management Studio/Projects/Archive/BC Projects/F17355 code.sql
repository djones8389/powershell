 SET NOCOUNT ON;
    
    DECLARE @TempTable TABLE
  (
   CEV_Id INT,
   CEV_ExternalSessionID INT,
   CEV_Keycode NVARCHAR(12),
   CEV_VIP BIT,
   CEV_DateSubmitted DATETIME
  )
  
 INSERT INTO @TempTable
  select TOP 1
  CEV.ID,
  CEV.ExternalSessionID,
  CEV.Keycode,
  CEV.VIP,
  CEV.DateSubmitted
 FROM   dbo.CandidateExamVersions CEV
 WHERE CEV.KeyCode IN ('25MWYT01')



    
    SELECT TT.CEV_ID              AS 'ID'
          ,TT.CEV_ExternalSessionID           AS 'ExternalSessionID'
          ,TT.CEV_Keycode             AS 'Keycode'
          ,TT.CEV_DateSubmitted            AS 'DateSubmitted'
          ,I.ExternalItemID             AS 'ExternalItemID'
          ,MS.version              AS 'MarkSchemeVersion'      
          ,ISNULL(M.Mark, AIM.AssignedMark)         AS 'confirmedMark'
          ,ISNULL(M.markedMetadataResponse, AIM.markedMetadataResponse)  AS 'MarkedMetadataResponse'
          ,ISNULL(M.Timestamp, ISNULL(AGM.timestamp, TT.CEV_DateSubmitted)) AS 'dateMarked'
          ,ISNULL(U1.Username ,ISNULL(U.Username ,'unknown'))    AS 'Username'
          ,(CASE 
    WHEN M.ID IS NOT NULL THEN ISNULL(M.AnnotationData, '')
    ELSE ISNULL(AIM.AnnotationData, '')
   END)               AS 'annotationData'
          
    FROM   @TempTable TT
           INNER JOIN dbo.CandidateGroupResponses CGR ON  CGR.CandidateExamVersionID = TT.CEV_ID
           ----------                    
           INNER JOIN dbo.UniqueGroupResponses UGR ON  UGR.ID = CGR.UniqueGroupResponseID
           INNER JOIN dbo.GroupDefinitionItems GDI ON  GDI.GroupID = UGR.GroupDefinitionID
                    AND GDI.ViewOnly = 0 -- only for marking
           ----------                        
           INNER JOIN dbo.CandidateResponses CR ON  CR.CandidateExamVersionID = TT.CEV_ID
           INNER JOIN dbo.UniqueResponses UR ON  UR.id = CR.UniqueResponseId AND UR.itemId = GDI.ItemID
           INNER JOIN dbo.Items I ON  I.ID = UR.itemId
           LEFT JOIN dbo.AssignedGroupMarks AGM ON  AGM.UniqueGroupResponseId = CGR.UniqueGroupResponseID AND AGM.IsConfirmedMark = 1
           LEFT JOIN dbo.AssignedItemMarks AIM ON  AIM.UniqueResponseId = CR.UniqueResponseId AND AIM.GroupMarkId = AGM.ID           
           ----------
           LEFT JOIN dbo.Moderations M ON M.UniqueResponseId = UR.id AND M.CandidateExamVersionId = TT.CEV_ID AND M.IsActual = 1
           ----------                          
           LEFT JOIN dbo.Users U ON  AGM.userId = U.id
           LEFT JOIN dbo.Users U1 ON  U1.id = M.UserId
           LEFT JOIN dbo.MarkSchemes MS ON  UR.markSchemeId = MS.id                         
    WHERE  UGR.GroupDefinitionID IN 
   (
    SELECT GDSCR.GroupDefinitionID 
    FROM   dbo.GroupDefinitionStructureCrossRef GDSCR 
     INNER JOIN dbo.ExamVersionStructures EVS ON  EVS.ID = GDSCR.ExamVersionStructureID
    WHERE  GDSCR.Status = 0 -- 0 = Active
     AND EVS.StatusID = 0 -- 0 = Released
   )
     AND UGR.GroupDefinitionID IN 
   (
    SELECT  MAX(GroupDefinitionID) FROM ..CandidateGroupResponses CGR2
    INNER JOIN UniqueGroupResponses UGR2 ON UGR2.ID = CGR2.UniqueGroupResponseID
    INNER JOIN UniqueGroupResponseLinks UGRL2 ON UGRL2.UniqueGroupResponseID = UGR2.ID
    WHERE  CGR2.CandidateExamVersionID = TT.CEV_ID
    GROUP BY UGRL2.UniqueResponseId
   )
   
   
   --2014-05-30 17:59:00.000
   
   select * from CandidateExamVersions where KeyCode IN ('25MWYT01') --2014-05-30 18:02:50.787--
   
  and  DateSubmitted > '2014-05-30 17:02:50.787'
  and DateSubmitted < '2014-05-30 19:02:50.787'
   -- Keycode = 'QNZKD801'
   
   
   
   
   --_secureAssessIntegrationService.UploadResultPendingExamSessions