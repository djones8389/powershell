 WITH T AS ( SELECT ROW_NUMBER() OVER( ORDER BY timestamp DESC, AssignedMarkID ASC ) 
 AS RowNum, AssignedMarkId, UniqueGroupResponseID, GroupName, FullName AS Marker, timestamp AS Date, assignedMark AS Mark, confirmedMark AS CIMark, CIMarkingTolerance AS Tolerance
 , TotalMark, IsOutOfTolerance, MarkingMethod, IsEscalated, MarkingDeviation 
 
 FROM MarkedCIGroups_view WHERE (GroupID = 1336 AND UserId = 93) AND (ExamVersionID = 85) ) 
 
 SELECT *  FROM T 
 
 inner join MarkingMethodLookup as MML
 on MML.id = T.MarkingMethod
 
 ORDER BY Date DESC, AssignedMarkID ASC;
 
 
 SELECT *
  FROM [BRITISHCOUNCIL_SecureMarker].[dbo].[SuspendedReasons]
  
  inner join UserCompetenceStatus
  on UserCompetenceStatus.UserID = SuspendedReasons.UserID
  
  where SuspendedReasons.UserID = 93
  and Date > '01 Nov 2014'
  and GroupDefinitionID = 1336
  ORDER BY Date DESC;