--find other affected scripts--


SELECT top 20 Keycode
FROM CandidateExamVersions as CEV

INNER JOIN CandidateResponses as CR
ON CR.CandidateExamVersionID = CEV.ID

INNER JOIN UniqueResponses as UR
ON UR.ID = UniqueResponseID 

left join UniqueResponseDocuments as URD
ON URD.UniqueResponseID = UR.id 
	
Inner Join Items as I
on I.ID = ur.itemId

INNER JOIN ExamVersions as EV
ON EV.ID = I.ExamVersionID

INNER JOIN Exams as E
ON E.ID = EV.ExamID

where ur.id not in (
		select UniqueResponseID
			from UniqueResponseDocuments
	)
and DateSubmitted > '01 Jan 2014'
and e.Name = 'Speaking'
and cev.PercentageMarkingComplete = '100'
and cev.ExamSessionState = 7
order by DateSubmitted desc
--where Keycode = 'u27ag601'