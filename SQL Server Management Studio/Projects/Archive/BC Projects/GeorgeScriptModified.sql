--Original--

DECLARE @considerableAgms TABLE (
	AgmId BIGINT,
	UgrId BIGINT
)

DECLARE @prevAgm BIGINT
DECLARE @ugrId BIGINT
DECLARE @agmTimestamp DATETIME
DECLARE @prevAgmMarkingMethod DATETIME

DECLARE @agmId BIGINT
DECLARE @agms CURSOR
SET @agms = CURSOR FOR 
		SELECT agm.ID 
		FROM AssignedGroupMarks agm 
		WHERE agm.IsConfirmedMark = 1
		AND agm.MarkingMethodId = 4
		AND agm.[Timestamp] > '01 Nov 2014'

OPEN @agms
       FETCH NEXT
       FROM @agms INTO @agmId
       WHILE @@FETCH_STATUS = 0
             BEGIN
                 
				  SET @ugrId = (SELECT agm.UniqueGroupResponseId
				                FROM AssignedGroupMarks agm WHERE agm.ID = @agmId)
				                
				  SET @agmTimestamp = (SELECT agm.[Timestamp]
				                FROM AssignedGroupMarks agm WHERE agm.ID = @agmId)
				              
                  SET @prevAgmMarkingMethod = (SELECT TOP 1 MarkingMethodId 
                                  FROM AssignedGroupMarks 
                                  WHERE UniqueGroupResponseId = @ugrId 
                                  AND Id != @agmId 
                                  AND [Timestamp] < @agmTimestamp 
                                  ORDER BY Timestamp DESC, Id DESC)
                                  
                  SET @prevAgm = (SELECT TOP 1 Id 
                                  FROM AssignedGroupMarks 
                                  WHERE UniqueGroupResponseId = @ugrId 
                                  AND Id != @agmId 
                                  AND [Timestamp] < @agmTimestamp 
                                  ORDER BY Timestamp DESC, Id DESC)
                              
                   -- if prev agm is NOT 6	(Actual CI Review) or 2(Control Item) or 3(Competency Control Item) it means that it was NOT Revoke CI, so we add to the Table
				   -- if prev agm is NOT 11 (Escalated) it means that it was NOT De-Escalate             
                   IF(@prevAgmMarkingMethod IS NULL OR (@prevAgmMarkingMethod != 6 and @prevAgmMarkingMethod != 2 and @prevAgmMarkingMethod != 3 and @prevAgmMarkingMethod != 11))
                   BEGIN
                   
					INSERT INTO @considerableAgms
					(
						AgmId,
						UgrId
					)
					VALUES
					(
						@agmId,
						@ugrId
					)
                   
                   END   
                   
                   -- if prev agm = 11 (Escalated)
					IF(@prevAgmMarkingMethod = 11)
						BEGIN
							-- and IsEscalated = 0 (it was de-escalate) => add to the Table
							IF((SELECT TOP 1 IsEscalated FROM AssignedGroupMarks WHERE id = @prevAgm) = 0) 
								BEGIN
									
									INSERT INTO @considerableAgms
									(
										AgmId,
										UgrId
									)
									VALUES
									(
										@agmId,
										@ugrId
									)
								END
						END            

                   FETCH NEXT FROM @agms INTO @agmId
             END
CLOSE @agms
DEALLOCATE @agms


SELECT 
	agm.UserId, 
	users.Username,
	users.Forename,
	users.surname,
	cev.Keycode, 
	Exams.Name,	
	COUNT(agm.ID) as 'Count'
FROM CandidateExamVersions cev
INNER JOIN CandidateGroupResponses cgr ON cgr.CandidateExamVersionID = cev.ID
INNER JOIN AssignedGroupMarks agm ON agm.UniqueGroupResponseID = cgr.UniqueGroupResponseID
INNER JOIN GroupDefinitions ON GroupDefinitions.ID = agm.GroupDefinitionID
INNER JOIN ExamVersions ev ON ev.ID = GroupDefinitions.ExamVersionID
inner join Exams on Exams.ID = ev.ExamID
inner join Users on Users.ID = agm.UserId
WHERE ev.IsMarkOneItemPerScript = 1
AND EXISTS (SELECT *
               FROM @considerableAgms ca
               WHERE ca.AgmId = agm.ID)
  and UserId not in ( 
  select UserID from AssignedUserRoles where RoleID = 30
 )
 
GROUP BY agm.UserId, cev.Keycode,users.Username,
	users.Forename,
	users.surname,
	Exams.Name
having COUNT(agm.ID) > 1