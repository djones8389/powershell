with cte as (

SELECT EST.ID
  FROM ExamSessionTable as EST

  Inner Join ScheduledExamsTable as SCET
  on SCET.ID = EST.ScheduledExamID
  
  Inner Join UserTable as UT
  on UT.ID = EST.UserID

where attemptAutoSubmitForAwaitingUpload is null 
		
)


SELECT EST.ID, EST.KeyCode, EST.examState, UT.Forename, UT.Surname, CT.CentreName, SCET.examName, StructureXML
	,scet.CreatedDateTime, SCET.ScheduledStartDateTime, SCET.ScheduledEndDateTime, attemptAutoSubmitForAwaitingUpload, AwaitingUploadGracePeriodInDays
		, AwaitingUploadStateChangingTime

  FROM ExamSessionTable as EST

  Inner Join ScheduledExamsTable as SCET
  on SCET.ID = EST.ScheduledExamID
  
  Inner Join UserTable as UT
  on UT.ID = EST.UserID

  Inner Join CentreTable as CT
  on CT.ID = SCET.CentreID
  
  inner join cte
  on cte.ID = est.ID 
  
  where est.ID = cte.ID
  
order by scet.CreatedDateTime