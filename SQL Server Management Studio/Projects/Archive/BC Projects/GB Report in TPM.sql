select distinct
	SPC.IsCompleted
	, P.Name
	, FirstName
	, LastName
	, SurpassCandidateRef
	, PackageScore
--	, WESTS.centreName
	--,CentreName?
	, SPC.DateCompleted
	--,*
 from ScheduledPackageCandidates as SPC
 
	left join ScheduledPackageCandidateExams as SPCE
	on SPCE.Candidate_ScheduledPackageCandidateId = SPC.ScheduledPackageCandidateId
 
	inner join PackageExams as PE
	on PE.PackageExamId = SPCE.PackageExamId
	
	inner join Packages as P
	on P.PackageId = PE.PackageId
	
	--inner join BRITISHCOUNCIL_SecureAssess..WAREHOUSE_ExamSessionTable_Shreded as WESTS
	--on WESTS.KeyCode = SurpassExamRef
	
	where SurpassCandidateRef = 'filip-browser2'
	--where SPC.ScheduledPackageCandidateId = 163548

/*
select top 50 
	* from ScheduledPackageCandidateExams 

where ScheduledExamRef in ('NZCSFN01','LXASRP01')


select top 50 * from PackageExams 

	inner join Packages as P
	on P.PackageId = PackageExams.PackageId

where PackageExamId in (525,526)
*/


/*
select top 50 * from PackageExams where PackageId = 525
select top 50 * from PackageExams where SurpassExamId = 525


select top 50 * from ScheduledPackages where ScheduledPackageId = 525
select top 50 * from ScheduledPackages where PackageId = 525*/