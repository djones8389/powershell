USE [BRITISHCOUNCIL_SecureMarker]
GO
/****** Object:  StoredProcedure [dbo].[sm_MARKINGSERVICE_GetMarkingProgressData_sp]    Script Date: 05/08/2014 14:11:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Anton Burdyugov
-- Date:		20/06/2013
-- Description:	Gets the information about how many Unique Responses and Control Items across all Exam Versions was marked.
-- =============================================

--ALTER PROCEDURE [dbo].[sm_MARKINGSERVICE_GetMarkingProgressData_sp]
	-- Add the parameters for the stored procedure here
	
	declare @fromDate DATETIME ='2014-03-01 23:00:00',@toDate DATETIME ='2014-04-30 23:59:59',@userId INT =73
	--@fromDate DATETIME,
	--@toDate DATETIME,
	--@userId INT
--AS 
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;
	
        SELECT  COUNT(AGM.GroupDefinitionID) AS TotalMarked ,
				ISNULL(SUM(
				CASE
					WHEN AGM.MarkingMethodId IN (2, 3) THEN 1
					ELSE 0
				END
				), 0) AS TotalCGsMarked
        FROM    dbo.AssignedGroupMarks AGM
				INNER JOIN dbo.GroupDefinitionItems GDI ON GDI.GroupId = AGM.GroupDefinitionID AND GDI.ViewOnly = 0
        WHERE   EXISTS ( SELECT *
                         FROM   GroupDefinitionStructureCrossRef GDSCR
                                INNER JOIN ExamVersionStructures EVS ON EVS.ID = GDSCR.ExamVersionStructureID
                         WHERE  GDSCR.Status IN (0, 2) 
                                AND GDSCR.GroupDefinitionID = AGM.GroupDefinitionID
                                AND EVS.StatusID = 0 )
                AND AGM.GroupDefinitionID > 0
                AND AGM.UserId = @userId
                AND Timestamp BETWEEN @fromDate AND @toDate
                AND MarkingMethodId != 5
				AND MarkingMethodId != 6
				AND MarkingMethodId != 11
				AND ViewOnly = 0
				AND AGM.IsEscalated = 0
    END
