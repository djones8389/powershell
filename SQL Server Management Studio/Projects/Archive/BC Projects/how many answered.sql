create table #tempTable

(
      ID2 int,
      TotalNumberOfQuestions int,
      Keycode2 nvarchar(10)
)

insert into #tempTable

      select
                  ID,
                  StructureXML.value('count(/assessmentDetails/assessment/section/item)[1]', 'int'),
                  KeyCode
      from ExamSessionTable
     where KeyCode in ('2Y33PX99','CERWJN99','4EX82M99','UYGJPB99','JDMPED99','VS58SK99','5B7S4M99','CPHXFX99','P63AA599','3P5S9W99','24CBNV99','4XHAG499','AEDXQX99','XESPKG99','4AL8FP99','K9PPTF99','7YV8DL99','WTJLLN99','3LMMVZ99','7TGJBY99','LA62F799','GAQWWY99','MRRDRT99','MUMM4C99','CNQHZ499','GJ7P8W99','JS764X99','BDMKW899','RBD6KY99','G9BGUT99','K9594K99','R8SY3799','FCADBS99','N8TR5C99','QUNH4H99','FKEUAM99','SDJDY699','G72KEM99','M3QDSV99','W2FD6W99','TMNL7B99','3J87J599','JU6WQX99','PUNYXF99','2U7J3999','49JPNH99','ZE3YME99','7B9Q5V99','S5VNX799','YAMP5T99','UJJ2GV99','LM6JA399','FSLUN299','P6A6L799','LMXYVA99','HVHPTV99','BSREBC99','QHC24X99','G5YY7D99')
      
      
	select 				
		   COUNT (ExamSessionID) as NumberOfQuestionsAnswered
		   ,  TT.TotalNumberOfQuestions      
		   , Keycode2
			--into #compare
		from ExamSessionItemResponseTable as ESIRT

		inner join ExamSessionTable as EST
		on EST.ID = ESIRT.ExamSessionID
		
		inner join #tempTable as TT
        on TT.ID2 = ESIRT.ExamSessionID
		
		where KeyCode in ('2Y33PX99','CERWJN99','4EX82M99','UYGJPB99','JDMPED99','VS58SK99','5B7S4M99','CPHXFX99','P63AA599','3P5S9W99','24CBNV99','4XHAG499','AEDXQX99','XESPKG99','4AL8FP99','K9PPTF99','7YV8DL99','WTJLLN99','3LMMVZ99','7TGJBY99','LA62F799','GAQWWY99','MRRDRT99','MUMM4C99','CNQHZ499','GJ7P8W99','JS764X99','BDMKW899','RBD6KY99','G9BGUT99','K9594K99','R8SY3799','FCADBS99','N8TR5C99','QUNH4H99','FKEUAM99','SDJDY699','G72KEM99','M3QDSV99','W2FD6W99','TMNL7B99','3J87J599','JU6WQX99','PUNYXF99','2U7J3999','49JPNH99','ZE3YME99','7B9Q5V99','S5VNX799','YAMP5T99','UJJ2GV99','LM6JA399','FSLUN299','P6A6L799','LMXYVA99','HVHPTV99','BSREBC99','QHC24X99','G5YY7D99')
		
	group by ExamSessionID, TT.TotalNumberOfQuestions, Keycode2


drop table #tempTable;
/*
select #compare.* from #compare 

	 inner join ExamSessionTable
	 on ExamSessionTable.KeyCode = Keycode2

where NumberOfQuestionsAnswered <> TotalNumberOfQuestions;

drop table #compare;
*/