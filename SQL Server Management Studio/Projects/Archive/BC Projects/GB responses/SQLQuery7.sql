--Speaking--
DECLARE @InstanceName SYSNAME = N'430088-BC-SQL\SQL1',
            @DatabaseName SYSNAME = N'BRITISHCOUNCIL_SecureAssess',
            @Directory NVARCHAR(100) = N'C:',
            @FileExtension NVARCHAR(4) = N'mp3';

SELECT N'bcp "SELECT document FROM ['+@DatabaseName+N'].[dbo].[WAREHOUSE_ExamSessionDocumentTable] WHERE ItemID = '''+WAREHOUSE_ExamSessionDocumentTable.ItemID+N''';" queryout '+@Directory+N'\'+WAREHOUSE_ExamSessionTable.KeyCode+N'_'+WAREHOUSE_ExamSessionDocumentTable.ItemID+N'.'+@FileExtension+N' -S '+@InstanceName+N' -T -N'
FROM dbo.WAREHOUSE_ExamSessionTable
INNER JOIN dbo.WAREHOUSE_ExamSessionDocumentTable
ON WAREHOUSE_ExamSessionTable.ID = WAREHOUSE_ExamSessionDocumentTable.WarehouseExamSessionID
WHERE WAREHOUSE_ExamSessionTable.KeyCode IN ('NG93RA01','6201665534','3602444388','7155407230','4BAFRR01','EPLUV701','U89YYH01','5EGS8J01','CATG4X01','LLDTAJ01','43KYKK01','5DUCZC01','BLKCLU01');
