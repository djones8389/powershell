SELECT UGR.ID, Keycode, CandidateRef, ExternalItemName, CentreName
FROM CandidateExamVersions as CEV

INNER JOIN CandidateResponses as CR
ON CR.CandidateExamVersionID = CEV.ID

INNER JOIN UniqueResponses as UR
ON UR.ID = CR.UniqueResponseID 

INNER JOIN UniqueGroupResponseLinks as UGRL
on UGRL.UniqueResponseId = UR.id

INNER JOIN UniqueGroupResponses as UGR
on UGR.ID = UGRL.UniqueGroupResponseID

INNER JOIN Items as I
on I.ID = UR.itemId

where ugr.ID in (601063,590567,578583,469808,452727,392036,332513,225723,42901,9945,43541,43905,113458,9890,9113,211363,207626,189841,179180,113454)
      order by ugr.ID asc
