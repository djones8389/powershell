use BRITISHCOUNCIL_SecureAssess

select count(qualificationName) as CountofExams, qualificationName, examName, examVersionRef, examVersionId
	 from WAREHOUSE_ExamSessionTable_Shreded
	where examname = '' 
		--and examVersionId in (240,241,242)
	 group by qualificationName, examName, examVersionRef, examVersionId
		
	order by examVersionId asc


BEGIN TRAN
		
DECLARE IDs CURSOR FOR
	select examVersionId
		from BRITISHCOUNCIL_SecureAssess..WAREHOUSE_ExamSessionTable_Shreded
		where examname = ''
			--and examVersionId in (240,241,242)
		
OPEN IDs;

DECLARE @ID int;

FETCH NEXT FROM IDs INTO @ID;

WHILE @@FETCH_STATUS = 0
BEGIN

update BRITISHCOUNCIL_SecureAssess..WAREHOUSE_ExamSessionTable_Shreded
set examName = 
	( 
		select AGT.Name	
			from BRITISHCOUNCIL_ItemBank..AssessmentGroupTable as AGT
			
			inner join QualificationTable as QT
			on QT.ID = AGT.QualificationID			
			
			inner join AssessmentTable as AT
			on AT.AssessmentGroupID = AGT.ID
			
			where AT.ID = @ID
			)
			
where examVersionId = @ID
	and examName = ''


FETCH NEXT FROM IDs INTO @ID;
END

CLOSE IDs;
DEALLOCATE IDs;

ROLLBACK


