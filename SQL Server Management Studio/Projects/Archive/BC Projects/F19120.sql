--update UniqueGroupResponses 
--set IsEscalated = 1
--where id in (647883,647881,647882)

-- WITH T AS ( SELECT ROW_NUMBER() OVER( ORDER BY DateEscalated DESC, ID ASC ) AS RowNum, ID, Qualification, QualificationId, Exam, ExamId, ExamVersion, ExamVersionId, ItemName, ItemId, Examiner, ExaminerId, DateEscalated, Issues, ReasonIds, Centres, CentresCodes, CountOfScripts, ScriptId, CandidateName, CandidateRef, CentreName, CentreCode, IsAllowViewWholeScript FROM sm_EscalatedUGRs_fn(118) WHERE (ID > 0) ) SELECT *  FROM T WHERE RowNum BETWEEN 0 * 35 + 1  AND ( 0 + 1 ) * 35 ORDER BY DateEscalated DESC, ID ASC
 
--update UniqueGroupResponses 
--set IsEscalated = 0
--where id in (647883,647881,647882)
 
 
 SELECT UGR.ID, Keycode, CandidateRef, ExternalItemName, CentreName
FROM CandidateExamVersions as CEV

INNER JOIN CandidateResponses as CR
ON CR.CandidateExamVersionID = CEV.ID

INNER JOIN UniqueResponses as UR
ON UR.ID = CR.UniqueResponseID 

INNER JOIN UniqueGroupResponseLinks as UGRL
on UGRL.UniqueResponseId = UR.id

INNER JOIN UniqueGroupResponses as UGR
on UGR.ID = UGRL.UniqueGroupResponseID

INNER JOIN Items as I
on I.ID = UR.itemId

where ugr.ID in (647883,647881,647882)


select * from UniqueGroupResponses where id in (647883,647881,647882)

