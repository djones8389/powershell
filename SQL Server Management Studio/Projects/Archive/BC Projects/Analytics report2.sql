exec sm_MARKINGSERVICE_GetAllGroupsForExamVersion_sp @UserID=73,@ExamVersionID=88,@TokenStoreID=2892515

--exec sm_MARKINGSERVICE_GetMarkingProgressData_sp @fromDate='2014-02-28 23:00:00',@toDate='2014-04-30 23:59:59',@userId=73

 
 
 WITH [OrderedView]
AS (
	SELECT ROW_NUMBER() OVER (
			ORDER BY Qualification ASC
				,Id ASC
			) AS RowNum
		,Id
		,Qualification
		,Exam
		,ExamVersion
		,NAME
		,UniqueResponse
		,ControlItems
		,Total
	FROM sm_getMarkingReportItems_fn(73, '2014-02-28', '2014-04-30 23:59:59')
	WHERE (id > 0)
	)
SELECT *
FROM (
	SELECT COUNT(*) AS [TotalCount]
	FROM [OrderedView]
	) tCount
LEFT JOIN [OrderedView] ON RowNum BETWEEN 0 * 35 + 1
		AND (0 + 1) * 35
ORDER BY Qualification ASC
	,Id ASC
	
	--W_Ver_7_Task_1