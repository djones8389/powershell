SELECT CEV.ExternalSessionID, E.Name, URD.Blob
FROM CandidateExamVersions as CEV

INNER JOIN CandidateResponses as CR
ON CR.CandidateExamVersionID = CEV.ID

INNER JOIN UniqueResponses as UR
ON UR.ID = UniqueResponseID 

left join UniqueResponseDocuments as URD
ON URD.UniqueResponseID = UR.id 
	
Inner Join Items as I
on I.ID = ur.itemId

inner join ExamVersions as EV
on EV.ID = I.ExamVersionID

INNER JOIN Exams as E
ON E.ID = EV.ExamID
 
INNER JOIN Qualifications as Q
ON E.QualificationID = Q.ID

--where Keycode = 'KU43PB99'
where --Keycode = 'T8RN2U99'
	E.Name = 'Speaking'
	and URD.Blob is null
	group by CEV.ExternalSessionID, E.Name, URD.Blob
	having COUNT(*) > 1
	
	order by cev.DateSubmitted desc
	

/*
WarehouseExamSessionID	ExamState	Date
674068	3	2014-11-12 02:09:59.630
674068	3	2014-11-12 12:11:02.880
*/