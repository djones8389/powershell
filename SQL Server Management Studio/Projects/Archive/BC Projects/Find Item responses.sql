SELECT --GroupDefinitions.ID, --tokenID, parkeduserid, 
Candidateexamversions.keycode, COUNT(UniqueGroupResponses.ID), Qualifications.Name AS QualName, Exams.Name AS ExamName, ExamVersions.Name AS VersionName
FROM Candidateexamversions 
INNER JOIN ExamVersions
ON ExamVersions.ID = CandidateExamVersions.ExamVersionID
INNER JOIN Exams
ON Exams.ID = ExamVersions.ExamID
INNER JOIN Qualifications
ON QualificationID = Qualifications.ID
LEFT JOIN CandidateResponses
ON CandidateExamVersionID = Candidateexamversions.ID
LEFT JOIN UniqueResponses
ON UniqueResponseID = UniqueResponses.ID
LEFT JOIN UniqueGroupResponseLinks
ON UniqueGroupResponseLinks.UniqueResponseID = UniqueResponses.id
LEFT JOIN UniqueGroupResponses
ON UniqueGroupResponses.ID = UniqueGroupResponseLinks.UniqueGroupResponseID
where ExamSessionState = 1--keycode in ('4DXS4H01','EBYF5G01','29R74Q01','L2HT9801','VQY3W401','CBPWT301','7FLXHK01','GLFC8701','ZJYYNQ01','6ZL3HM01','RR2WDJ01','6V5R3V01','5LBLTS01','P265KF01','TLQAAB01','4MAL7X01','GC6AVQ01','3WMJUS01','6VYTXD01','4H2JNC01','76SJDS01','YMARYU01','GPFQBX01','RSSP2H01','5157551518','H7NNDD01','34TDNJ01','4GY4GM01','5XPLMJ01','YZXN5A01','RBP2BA01','QVAWYR01','K8B9RY01','G43ULB01')
GROUP BY ExternalSessionID, keycode, Qualifications.Name, Exams.Name, ExamVersions.Name
