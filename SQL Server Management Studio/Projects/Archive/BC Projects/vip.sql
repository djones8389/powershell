SELECT *
FROM Items
inner join examversions
on examversions.ID = examversionid
WHERE ExternalItemName = 'SPVer_9_Task_2'

SELECT UniqueGroupResponses.VIP, CandidateExamVersions.VIP, *
FROM UniqueResponses
inner join UniqueGroupResponseLinks
on UniqueGroupResponseLinks.UniqueResponseId = UniqueResponses.id
INNER JOIN UniqueGroupResponses
on UniqueGroupResponses.ID = UniqueGroupResponseLinks.UniqueGroupResponseID
INNER JOIN CandidateResponses
ON CandidateResponses.UniqueResponseID = UniqueResponses.id
INNER JOIN CandidateExamVersions
ON CandidateExamVersions.ID = CandidateResponses.CandidateExamVersionID
where itemId = 800 and UniqueGroupResponses.confirmedMark IS NULL
order by UniqueGroupResponses.VIP desc, UniqueGroupResponses.insertionDate ASC

SELECT TOP 200 UniqueGroupResponses.vip, AssignedGroupMarks.*
FROM UniqueResponses
inner join UniqueGroupResponseLinks
on UniqueGroupResponseLinks.UniqueResponseId = UniqueResponses.id
INNER JOIN UniqueGroupResponses
on UniqueGroupResponses.ID = UniqueGroupResponseLinks.UniqueGroupResponseID
INNER JOIN CandidateResponses
ON CandidateResponses.UniqueResponseID = UniqueResponses.id
INNER JOIN CandidateExamVersions
ON CandidateExamVersions.ID = CandidateResponses.CandidateExamVersionID
INNER JOIN AssignedGroupMarks
ON AssignedGroupMarks.UniqueGroupResponseId = UniqueGroupResponses.ID
where itemId = 800 and MarkingMethodId = 4-- and UniqueGroupResponses.confirmedMark IS NULL
ORDER BY Timestamp DESC
