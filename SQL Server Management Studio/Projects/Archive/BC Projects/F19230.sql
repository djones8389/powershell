use BritishCouncil_SecureAssess


select WAREHOUSE_ExamSessionTable.id, KeyCode, ItemID, warehouseTime
	 from WAREHOUSE_ExamSessionItemResponseTable

	inner join WAREHOUSE_ExamSessionTable
	on WAREHOUSE_ExamSessionTable.ID = WAREHOUSE_ExamSessionItemResponseTable.WAREHOUSEExamSessionID
	
	--inner join WAREHOUSE_ExamSessionTable_ShrededItems
	--on WAREHOUSE_ExamSessionTable_ShrededItems.examSessionId = WAREHOUSE_ExamSessionTable.ID

where ItemID in (

	SELECT  CONVERT(NVARCHAR(10), ProjectID) + 'P' + CONVERT(NVARCHAR(10), ItemRef) AS ItemID
	FROM BRITISHCOUNCIL_ItemBank..ItemTable

	GROUP BY ProjectID, ItemRef
	HAVING COUNT(DISTINCT TotalMark) > 1

)

	and ExportToSecureMarker = 1
	and warehouseTime > '01 Nov 2014'
		ORDER BY warehouseTime desc
		
--146744--
--724916--
--select top 5 * from WAREHOUSE_ExamSessionItemResponseTable where WAREHOUSEExamSessionID = 686025
--select top 5 * from WAREHOUSE_ExamSessionTable_ShrededItems where ExamSessionID = 686025