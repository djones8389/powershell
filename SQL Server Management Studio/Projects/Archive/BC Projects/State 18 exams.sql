SELECT EST.ID, EST.KeyCode, EST.examState, UT.Forename, UT.Surname, CT.CentreName, SCET.examName
  FROM ExamSessionTable as EST

  Inner Join ScheduledExamsTable as SCET
  on SCET.ID = EST.ScheduledExamID
  
  Inner Join UserTable as UT
  on UT.ID = EST.UserID

  Inner Join CentreTable as CT
  on CT.ID = SCET.CentreID

where ExamState = 18

order by ID asc



update ExamSessionTable 
set AwaitingUploadGracePeriodInDays = 14, attemptAutoSubmitForAwaitingUpload = 1
where KeyCode = 'ZAWTRG01'

select * from ExamSessionTable where KeyCode = 'ZAWTRG01'
select * from WAREHOUSE_ExamSessionTable where KeyCode = 'ZAWTRG01'


TLP7XY01
ZAWTRG01
EQRT4Q01
TT48CT01
JS5JKT01
3XMKCQ01
2AM7GB01
3FJ5YY01
JUD79C01
JHDV7801
MLURDT01
WVUE4Y01
LMTP5T01
6LMEHG01
VDCD3C01
USAZFV01
GCVH2F01
QVA84B01
VVAC3501
MG84G201

declare @Keycode nvarchar(10) = 'ZAWTRG01'

declare @ID int = (
select ID
	from ExamSessionTable
		where KeyCode = @Keycode
	)


create table #tempTable

(
      ID2 int,
      TotalNumberOfQuestions int
)

insert into #tempTable

      select
                  ID,
                  StructureXML.value('count(/assessmentDetails/assessment/section/item)[1]', 'int')
      from ExamSessionTable
     where KeyCode = @Keycode
      
      
	select 				
		   COUNT (ExamSessionID) as NumberOfQuestionsAnswered
		   ,  TT.TotalNumberOfQuestions      

		from ExamSessionItemResponseTable as ESIRT

		inner join ExamSessionTable as EST
		on EST.ID = ESIRT.ExamSessionID
		
		inner join #tempTable as TT
        on TT.ID2 = ESIRT.ExamSessionID
		
		where KeyCode = @Keycode
		
	group by ExamSessionID, TT.TotalNumberOfQuestions 

drop table #tempTable