select top 5 * from BRITISHCOUNCIL_SecureAssess..WAREHOUSE_ExamSessionTable_Shreded where  examName = '' 
	
	--where KeyCode = 'HGHWDK01'

select * from BRITISHCOUNCIL_ItemBank..AssessmentGroupTable where ID = 119
select * from BRITISHCOUNCIL_ItemBank..AssessmentTable where ID = 86



--This tells you how many records you expect to update

use BRITISHCOUNCIL_SecureAssess

select count(qualificationName), qualificationName, examName, examVersionRef, examVersionId
	 from WAREHOUSE_ExamSessionTable_Shreded
	where examname = '' 
	 group by qualificationName, examName, examVersionRef, examVersionId
		
	order by qualificationName asc



--Declare your ExamVersionID

use BRITISHCOUNCIL_ItemBank

declare @examversion int = 304

--begin tran

update BRITISHCOUNCIL_SecureAssess..WAREHOUSE_ExamSessionTable_Shreded
set examName = 
	( 
		select AGT.Name	
			from BRITISHCOUNCIL_ItemBank..AssessmentGroupTable as AGT
			
			inner join QualificationTable as QT
			on QT.ID = AGT.QualificationID			
			
			inner join AssessmentTable as AT
			on AT.AssessmentGroupID = AGT.ID
			
			where AT.ID = @examversion
			)
			
where examVersionId = @examversion
	and examName = ''

--rollback


/*
select * from AssessmentTable where ID = 86
select top 10 * from BRITISHCOUNCIL_SecureAssess..WAREHOUSE_ExamSessionTable_Shreded where examVersionId = 86
*/
