use BritishCouncil_TestPackage

select --120 batch for Kuwait--
	distinct SurpassQualificationId, SurpassExamId, PE.PackageId, PE.Name, SurpassExamRef
		, P.Name, PE.PackageExamId, PE.SurpassExamRef--, SurpassCandidateRef

 from ScheduledPackageCandidateExams as SPCE

	inner join ScheduledPackageCandidates as SPC
	on SPC.ScheduledPackageCandidateId =  SPCE.Candidate_ScheduledPackageCandidateId
	
	inner join PackageExams as PE
	on PE.PackageExamId = SPCE.PackageExamId

	inner join Packages as P
	on P.PackageId = PE.PackageId
	
	where SurpassExamId in (42,45,44,43,59)
		and SurpassQualificationId in (119)
		and p.Name = 'Aptis Listening Reading Speaking and Writing Package'
		order by SurpassQualificationId, SurpassExamId;
		
		
select --120 batch for Kuwait--
	distinct SurpassQualificationId, SurpassExamId, PE.PackageId, PE.Name, SurpassExamRef
		, P.Name, PE.PackageExamId, PE.SurpassExamRef--, SurpassCandidateRef

 from ScheduledPackageCandidateExams as SPCE

	inner join ScheduledPackageCandidates as SPC
	on SPC.ScheduledPackageCandidateId =  SPCE.Candidate_ScheduledPackageCandidateId
	
	inner join PackageExams as PE
	on PE.PackageExamId = SPCE.PackageExamId

	inner join Packages as P
	on P.PackageId = PE.PackageId
	
	where SurpassExamId in (42,43,44,45,59)
		and SurpassQualificationId in (119)
		and p.Name = 'Aptis Listening Reading Speaking and Writing Package'
		order by SurpassQualificationId, SurpassExamId;
		