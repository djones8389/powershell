select
	KeyCode
	,foreName
	,surName
	,ItemResponseData.p.value('@id', 'nvarchar(15)') as itemId
	,c.i.value('.[1]', 'nvarchar(max)') as userData
	, WAREHOUSE_ExamSessionTable_Shreded.examSessionId
from WAREHOUSE_ExamSessionTable_Shreded 


inner join WAREHOUSE_ExamSessionItemResponseTable 
on WAREHOUSE_ExamSessionItemResponseTable.WAREHOUSEExamSessionID = WAREHOUSE_ExamSessionTable_Shreded.examSessionId

cross apply ItemResponseData.nodes('/p') ItemResponseData(p)
cross apply ItemResponseData.nodes('/p/s/c/i') c(i)

where KeyCode in ('R7G4YJ01','U9N94F01','FFQV2201','273UDZ01','2XA2RN01','8BTCEQ99','4RA7PK99','SHWRPA99','J5JJWQ99','U26VJG99','W6GLK599','QSQ24D99','AQPV2T99','3W3WB399','3HN8S799','PBQS6G99','XH5UG599','EZTY2899','X4ML2W99','JYJZ9F99','ST843K99','GZEK9T99','FSWZ6T99','9N937F99','33TSE699','AEVVHE99','PUTVF699','8VH6LC99','4LY35T99','24LLMU99','VAJVDE99','UY85D999')
	and itemid in ('3523P3607 ','3523P3939 ','3523P3945 ','3523P3754 ','3523P4096 ','3523P4220 ','3523P4232 ','3523P4232 ','3523P4219 ','3523P4223 ','3523P4223 ','3523P3771 ','3523P3939 ','3523P3783 ','3523P3779 ','3523P3949 ','3523P4296 ','3523P3783 ','3523P4298 ','3523P3605 ','3523P3771 ','3523P4296 ','3523P4298 ','3523P3939 ','3523P4284 ','3523P3948 ','3523P4282 ','3523P4096 ','3523P4094 ','3523P3754 ','3523P4294 ','3523P3725 ')
	order by Keycode asc;