 SELECT    --GD.ID AS ID ,
            --GD.Name AS GroupName ,
            --GD.ExamVersionID AS ExamVersionID ,
            COUNT(UGR.ID) AS NumToMark 
    FROM    dbo.QuotaManagement QM
            INNER JOIN dbo.GroupDefinitions GD ON GD.ID = QM.GroupId
   INNER JOIN dbo.GroupDefinitionStructureCrossRef GDSCR ON GDSCR.GroupDefinitionID = GD.ID
            INNER JOIN dbo.UniqueGroupResponses UGR ON UGR.GroupDefinitionID = QM.GroupId
            INNER JOIN dbo.ExamVersions EV ON GD.ExamVersionID = EV.ID

   -- Added
   INNER JOIN dbo.ExamVersionStructures EVS ON EVS.ID = GDSCR.ExamVersionStructureID


    WHERE    
                QM.AssignedQuota = -1
            AND GD.IsReleased = 1
   AND GDSCR.Status = 0
   AND UGR.ParkedUserID IS NULL
            AND UGR.confirmedMark IS NULL
   AND UGR.CI = 0

   -- Added
   AND EVS.StatusID = 0 -- 0 = Released