USE [BRITISHCOUNCIL_SecureMarker]
GO
/****** Object:  StoredProcedure [dbo].[sm_QUOTAMANAGEMENTSERVICE_GetMarkedResponseForMarkedCG_sp]    Script Date: 05/22/2014 10:44:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Tom Gomersall
-- Create date: 27/06/2011
-- Description:	Gets the marked response for a marked CI.
-- Modified:	Tom Gomersall
-- Date:		04/09/2012
-- Description:	Added marked metadata response to select.
-- Modified:	Vitaly Bevzik
-- Date:		03/04/2013
-- Description:	Added ScriptType
-- Modified:	Vitaly Bevzik
-- Date:		10/04/2013
-- Description:	Removed cheking Is item a CI, to allow se most recently Ci after it was discarded or 
-- Modified:	Anton Burdyugov
-- Date:		31/05/2013
-- Description:	Make work with group level data.
-- Modified:	Anton Burdyugov
-- Date:		17/06/2013
-- Description:	Confirmed mark for (Actual CI Review) added

-- Modified:	Roman Tseyko
-- Date:		21/06/2013
-- Description:	MarkingTolerance and ConfirmedMark for unique response added
-- =============================================
--ALTER PROCEDURE [dbo].[sm_QUOTAMANAGEMENTSERVICE_GetMarkedResponseForMarkedCG_sp]
	-- Add the parameters for the stored procedure here
  declare
	  @assignedGroupMarkId INT = 52190
--AS 
    --BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;
	
        DECLARE @uniqueGroupResponseId INT
        SET @uniqueGroupResponseId = ( SELECT TOP 1
                                                AGM.UniqueGroupResponseId
                                       FROM     dbo.AssignedGroupMarks AGM
                                       WHERE    AGM.ID = @assignedGroupMarkID
                                     )

--Select Group information
        SELECT TOP 100
                GD.ID ,
                GD.TotalMark ,
                GD.Name ,
                AGM.AssignedMark ,
				ISNULL(UGR.ConfirmedMark,
							  (SELECT     TOP (1) AssignedMark
								FROM          dbo.AssignedGroupMarks AGM
								WHERE      (AGM.UniqueGroupResponseId = UGR.id) AND (markingMethodId = 6)
								ORDER BY timestamp DESC))	AS ConfirmedMark,
				GD.CIMarkingTolerance AS Tolerance,
                ISNULL(EV.AnnotationSchema, '') AS AnnotationSchema ,
                CEV.ScriptType ,
                UGR.ID AS UniqueGroupResponseId,
                dbo.sm_checkGroupAndItemsMarkedInTolerance_fn(AGM.id, UGR.id, AGM.MarkingDeviation, GD.CIMarkingTolerance) AS IsOutOfTolerance,
                AGM.IsEscalated,
                AGM.MarkingDeviation,
                
                AGM.id, UGR.id, AGM.MarkingDeviation, GD.CIMarkingTolerance
                
        FROM    dbo.UniqueGroupResponses UGR
                INNER JOIN dbo.GroupDefinitions GD ON GD.ID = UGR.GroupDefinitionID
                INNER JOIN dbo.AssignedGroupMarks AGM ON AGM.UniqueGroupResponseId = UGR.ID
                INNER JOIN dbo.ExamVersions EV ON EV.ID = GD.ExamVersionID
                INNER JOIN dbo.CandidateGroupResponses CGR ON CGR.UniqueGroupResponseID = UGR.ID
                INNER JOIN CandidateExamVersions CEV ON CEV.ID = CGR.CandidateExamVersionID
                
        WHERE   AGM.ID = 52190--@assignedGroupMarkID
                --AND 
                --GD.TotalMark = AGM.AssignedMark 
				
				and
                
                ( AGM.markingMethodId = 2
                      OR AGM.markingMethodId = 3
                    )
                and IsOutOfTolerance = 1
                 
/*
-- Select unique responses				
        SELECT  UR.id AS id ,
                UR.responseData AS response ,
                AIM.assignedMark AS assignedMark ,
                AIM.annotationData AS annotationData ,
                AIM.markedMetadataResponse AS markedMetadataResponse ,
                I.ID AS ItemId ,
                I.Version AS ItemVersion ,
                I.LayoutName AS LayoutName ,
                I.AllowAnnotations AS AllowAnnotations ,
                I.TotalMark AS TotalMark ,
                I.ExternalItemID AS ExternalID ,
                GDI.ViewOnly AS ViewOnly,
				I.CIMarkingTolerance AS MarkingTolerance,
				L.confirmedMark AS ConfirmedMark,
				AIM.MarkingDeviation AS MarkingDeviation
        FROM    dbo.UniqueGroupResponses UGR
                INNER JOIN dbo.UniqueGroupResponseLinks L ON UGR.Id = L.UniqueGroupResponseID
                INNER JOIN dbo.UniqueResponses UR ON UR.id = L.UniqueResponseId
                INNER JOIN dbo.Items I ON i.id = UR.itemId
                INNER JOIN dbo.GroupDefinitionItems GDI ON GDI.GroupID = UGR.GroupDefinitionID
                                                           AND GDI.ItemID = UR.itemId
                LEFT JOIN dbo.AssignedItemMarks AIM ON AIM.UniqueResponseId = UR.id
                                                        AND AIM.GroupMarkId = @assignedGroupMarkId
        WHERE   UGR.ID = @uniqueGroupResponseId
        ORDER BY GDI.GroupDefinitionItemOrder ASC
    END
*/