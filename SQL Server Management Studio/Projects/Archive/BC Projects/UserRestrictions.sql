select Users.UserName, Keycode, DateSubmitted

      FROM CandidateExamVersions
      INNER JOIN CandidateResponses
      ON CandidateResponses.CandidateExamVersionID = CandidateExamVersions.ID
      INNER JOIN AssignedItemMarks
      ON AssignedItemMarks.UniqueResponseId = CandidateResponses.UniqueResponseID
      INNER JOIN AssignedGroupMarks
      ON AssignedGroupMarks.ID = AssignedItemMarks.GroupMarkId AND AssignedGroupMarks.IsConfirmedMark = 1
      INNER JOIN Users
      ON Users.ID = AssignedGroupMarks.UserId
      INNER JOIN UniqueResponses
      ON UniqueResponses.ID = CandidateResponses.UniqueResponseID
      INNER JOIN Items
      ON Items.ID = UniqueResponses.itemId
      INNER JOIN MarkingMethodLookup
      ON MarkingMethodLookup.id = MarkingMethodId
      inner join ExamVersions
      on ExamVersions.ID = Items.ExamVersionID

WHERE Users.UserName != 'automarker'
      and users.ID not in (17,26,31,94,115,151,169,170,176,177,177,178,180,195)  --Omit users with RoleID 30--
      and IsMarkOneItemPerScript = 1  
      and MarkingMethodId = 4      
	  and Timestamp > '01 Nov 2014'
      group by Users.Username, Keycode, DateSubmitted
      
      having COUNT(*) > 1
      order by DateSubmitted desc
