USE [BRITISHCOUNCIL_SecureMarker]
GO
/****** Object:  StoredProcedure [dbo].[sm_STATEMANAGEMENT_GetMarkingCompleteExamSessionsForExport_sp]    Script Date: 05/06/2014 11:22:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:		Dave Dixon
-- Create date: 27/05/2011
-- Description:	sproc to select Candidate Exam Versions (sessions) to export to SecureAssess
--				as Result Pending (State 3 - Marking Complete)
-- Modified:	Tom Gomersall
-- Date:		04/09/2012
-- Description:	Added marked metadata response.
-- Modified:	Anton Burdyugov
-- Date:		01/10/2013
-- Description:	Moderator changes support added. Corrected marked metadata response determination.
-- =================================================================================
--ALTER PROCEDURE [dbo].[sm_STATEMANAGEMENT_GetMarkingCompleteExamSessionsForExport_sp]
--AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
    SET NOCOUNT ON;
    
    SELECT TOP 100 
           CEV.ID							AS 'ID'
          ,CEV.ExternalSessionID			AS 'ExternalSessionID'
          ,CEV.Keycode						AS 'Keycode'
          ,CEV.DateSubmitted				AS 'DateSubmitted'
          ,I.ExternalItemID					AS 'ExternalItemID'
          ,I.Version AS ItemVersion
          ,UR.id AS UniqueResponseID
          ,MS.version						AS 'MarkSchemeVersion'      
          ,ISNULL(M.Mark, AIM.AssignedMark) AS 'confirmedMark'
          ,ISNULL(M.markedMetadataResponse, AIM.markedMetadataResponse) AS 'MarkedMetadataResponse'
          ,ISNULL(M.Timestamp, ISNULL(AGM.timestamp, CEV.DateSubmitted)) AS 'dateMarked'
          ,ISNULL(U1.Username ,ISNULL(U.Username ,'unknown'))  AS 'Username'
          ,(CASE 
				WHEN M.ID IS NOT NULL THEN ISNULL(M.AnnotationData, '')
				ELSE ISNULL(AIM.AnnotationData, '')
			END)							AS 'annotationData'
          
    FROM   dbo.CandidateExamVersions CEV
           INNER JOIN dbo.CandidateGroupResponses CGR ON  CGR.CandidateExamVersionID = CEV.ID
           ----------                    
           INNER JOIN dbo.UniqueGroupResponses UGR ON  UGR.ID = CGR.UniqueGroupResponseID
           INNER JOIN dbo.GroupDefinitionItems GDI ON  GDI.GroupID = UGR.GroupDefinitionID
                    AND GDI.ViewOnly = 0 -- only for marking
           ----------                        
           INNER JOIN dbo.CandidateResponses CR ON  CR.CandidateExamVersionID = CEV.ID
           INNER JOIN dbo.UniqueResponses UR ON  UR.id = CR.UniqueResponseId AND UR.itemId = GDI.ItemID
           INNER JOIN dbo.Items I ON  I.ID = UR.itemId
           LEFT JOIN dbo.AssignedGroupMarks AGM ON  AGM.UniqueGroupResponseId = CGR.UniqueGroupResponseID AND AGM.IsConfirmedMark = 1
           LEFT JOIN dbo.AssignedItemMarks AIM ON  AIM.UniqueResponseId = CR.UniqueResponseId AND AIM.GroupMarkId = AGM.ID           
           ----------
           LEFT JOIN dbo.Moderations M ON M.UniqueResponseId = UR.id AND M.CandidateExamVersionId = CEV.ID AND M.IsActual = 1
           ----------		                        
           LEFT JOIN dbo.Users U ON  AGM.userId = U.id
           LEFT JOIN dbo.Users U1 ON  U1.id = M.UserId
           LEFT JOIN dbo.MarkSchemes MS ON  UR.markSchemeId = MS.id
    WHERE  CEV.Keycode = 'a3ybtp01' OR CEV.Keycode = '2jc4hu01'
    ORDER BY
           CEV.VIP DESC
          ,CEV.DateSubmitted ASC
END
