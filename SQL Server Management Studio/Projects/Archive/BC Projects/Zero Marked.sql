/****** Script for SelectTopNRows command from SSMS  ******/
SELECT TOP 1000 [Key]
      ,[Value]
  FROM [BritishCouncil_TestPackage].[dbo].[CommonSettings]
  
  
  
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

SELECT TOP 1000 WAREHOUSE_ExamSessionTable.ID
	,WAREHOUSE_ExamSessionTable.KeyCode
	,resultDataFull
	,WAREHOUSE_ExamSessionTable.PreviousExamState
	, WAREHOUSE_ExamSessionTable.warehouseTime
	,WAREHOUSE_ExamSessionTable.ExportToSecureMarker
FROM WAREHOUSE_ExamSessionTable
WHERE ID IN (
		SELECT DISTINCT examSessionId
		FROM [BRITISHCOUNCIL_SecureAssess].[dbo].[WAREHOUSE_ExamSessionTable_ShrededItems] AS WESTSI
		WHERE ExamSessionID IN (
				SELECT DISTINCT WEST.ID
				FROM WAREHOUSE_ExamSessionTable_ShrededItems WESTSI
				INNER JOIN WAREHOUSE_ExamSessionTable AS WEST ON WESTSI.examSessionId = WEST.ID
				INNER JOIN WAREHOUSE_UserTable AS WUT ON WUT.ID = WEST.WAREHOUSEUserID
				INNER JOIN WAREHOUSE_ExamSessionTable_Shreded AS WES ON WES.examSessionId = WEST.ID
				WHERE WESTSI.userMark = 0
					AND examPercentage = 0
					AND userAttempted = 0
					AND correctAnswerCount > 0
					AND WEST.warehouseTime > '2014-07-14'
					AND WEST.PreviousExamState <> 10
					AND WES.examVersionRef NOT LIKE '%Practice'
					--and wes.KeyCode in ('HSPKQQ99','TRD5AE99')
				)
		);