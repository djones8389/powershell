/****** Script for SelectTopNRows command from SSMS  ******/
SELECT TOP 1000 [GroupDefinitionID]
      ,[ExamVersionStructureID]
      ,[Status]
  FROM [BRITISHCOUNCIL_SecureMarker].[dbo].[GroupDefinitionStructureCrossRef]
  
  where GroupDefinitionID = 898 
  and ExamVersionStructureID = 471
  
  
 
SELECT *
FROM UniqueGroupResponses
INNER JOIN AssignedGroupMarks
ON AssignedGroupMarks.UniqueGroupResponseId = UniqueGroupResponses.ID
INNER JOIN GroupDefinitions
ON UniqueGroupResponses.GroupDefinitionID = GroupDefinitions.ID
where UniqueGroupResponses.GroupDefinitionID = 898 