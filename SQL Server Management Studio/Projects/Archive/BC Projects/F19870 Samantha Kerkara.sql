 WITH T AS ( SELECT ROW_NUMBER() OVER( ORDER BY timestamp DESC, AssignedMarkID ASC ) 
 AS RowNum, AssignedMarkId, UniqueGroupResponseID, GroupName, FullName AS Marker, timestamp AS Date, assignedMark AS Mark, confirmedMark AS CIMark, CIMarkingTolerance AS Tolerance
 , TotalMark, IsOutOfTolerance, MarkingMethod, IsEscalated, MarkingDeviation 
 
 FROM MarkedCIGroups_view WHERE (UserId = 92) AND (ExamVersionID = 89) ) 
 
 SELECT *  FROM T 
 
 inner join MarkingMethodLookup as MML
 on MML.id = T.MarkingMethod
 
 ORDER BY Date DESC, AssignedMarkID ASC;
 
 
 SELECT *
  FROM [BRITISHCOUNCIL_SecureMarker].[dbo].[SuspendedReasons]
  
  inner join UserCompetenceStatus
  on UserCompetenceStatus.UserID = SuspendedReasons.UserID
  
  where SuspendedReasons.UserID = 92
  and Date > '01 Nov 2014'
  and GroupDefinitionID = 1353
  ORDER BY Date DESC;