declare 
    @UserID INT = 35,
    @ExamVersionID INT = 17,
    @TokenStoreID NVARCHAR(30) = NULL


      -- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements. 
        SET NOCOUNT ON; 
      
        SELECT  G.id AS ID ,
                G.name AS Name ,
                SUM(QM.[assignedquota]) AS AssignedQuota ,
                SUM(QM.[numbermarked]) AS NumberMarked ,
                QM.MarkingSuspended AS MarkingSuspended ,
                ( SELECT    COUNT(UGR.[id])
                  FROM      UniqueGroupResponses UGR
                            INNER JOIN GroupDefinitions GD ON UGR.GroupDefinitionID = GD.ID
                  WHERE     ( UGR.[tokenid] IS NULL
                              OR UGR.[tokenid] = @TokenStoreID
                            )
     -- unecessary check as we are passing in item id which determines this 
                            AND UGR.[parkeduserid] IS NULL
                            AND UGR.[confirmedmark] IS NULL
                            AND UGR.CI = 0
                            AND GD.ID = G.ID
                ) AS UniqueResponsesAvailableToMark,
                
                -- Count of unique Group Group Responses which affected with 'One Item Per Script' principle
                -- except Unique Group Responses for current Group
                (
     SELECT COUNT(CGR.UniqueGroupResponseID)
      from CandidateGroupResponses CGR
      INNER JOIN UniqueGroupResponses UGR ON UGR.ID = CGR.UniqueGroupResponseID
      INNER JOIN 
       (
        select CGR.CandidateExamVersionID
         from CandidateGroupResponses CGR
         INNER JOIN UniqueGroupResponses UGR ON UGR.ID = CGR.UniqueGroupResponseID
         where UGR.GroupDefinitionID != G.ID 
         AND exists 
         (
          select *
          from 
          (
           select TOP (1) 
            AGM.MarkingMethodId AS MM, 
            AGM.UserId AS UI,
            AGM.IsEscalated AS IE
           from AssignedGroupMarks AGM
           where AGM.UniqueGroupResponseId = CGR.UniqueGroupResponseID
           order by AGM.Timestamp desc
          ) AS T
          where ((T.MM != 14 AND T.MM != 11) OR (T.MM = 11 AND T.IE = 1)) AND T.UI = @UserID
         )
       ) AS AffectedScriptIds ON CGR.CandidateExamVersionID = AffectedScriptIds.CandidateExamVersionID
     WHERE UGR.GroupDefinitionID = G.ID
    )  AS AffectedUniqueResponses
                
        FROM    dbo.GroupDefinitions G
                INNER JOIN [dbo].[QuotaManagement] QM ON QM.[groupid] = G.[id]
                INNER JOIN [dbo].[ExamVersions] EV ON G.examversionid = EV.[id]
        WHERE   --structures with status released 
                EXISTS ( SELECT *
                         FROM   GroupDefinitionStructureCrossRef CR
                                INNER JOIN ExamVersionStructures EVS ON EVS.ID = CR.ExamVersionStructureID
                         WHERE  CR.Status = 0
                                AND CR.GroupDefinitionID = G.ID
                                AND EVS.StatusID = 0 )
                AND EV.id = @ExamVersionID
                AND QM.UserId = @UserID
                --Remove automatic groups with Marking Type = automatic
                AND NOT EXISTS ( SELECT GD1.ID
                                 FROM   dbo.Items I1
                                        INNER JOIN dbo.GroupDefinitionItems GDI1 ON I1.ID = GDI1.ItemID
                                        INNER JOIN dbo.GroupDefinitions GD1 ON GDI1.GroupID = GD1.ID
                                 WHERE  I1.MarkingType = 0
                                        AND GD1.IsAutomaticGroup = 1 AND GD1.ID = g.ID)   
   
    ---------------------------------------------------------------------------------
        GROUP BY G.id ,
                G.name ,
                QM.MarkingSuspended