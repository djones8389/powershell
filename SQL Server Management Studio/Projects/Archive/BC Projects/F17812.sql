SELECT UGR.ID, Keycode, CandidateRef, ExternalItemName, CentreName
FROM CandidateExamVersions as CEV

left JOIN CandidateResponses as CR
ON CR.CandidateExamVersionID = CEV.ID

left JOIN UniqueResponses as UR
ON UR.ID = CR.UniqueResponseID 

left JOIN UniqueGroupResponseLinks as UGRL
on UGRL.UniqueResponseId = UR.id

left JOIN UniqueGroupResponses as UGR
on UGR.ID = UGRL.UniqueGroupResponseID

inner join Items as I
on I.ID = UR.itemId

where ugr.ID in (538775)
	order by ugr.ID asc
--where Keycode = 'ERKQQK01'
