USE [BRITISHCOUNCIL_SecureMarker]
GO
/****** Object:  StoredProcedure [dbo].[sm_SCRIPTREVIEW_getMarkingHistoryItems_sp]    Script Date: 04/25/2014 14:35:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Vitaly Bevzik
-- Create date: 10/04/2013
-- Description:	Get items for marking history for current script Id uniqueResponse Id
-- =============================================
--ALTER PROCEDURE [dbo].[sm_SCRIPTREVIEW_getMarkingHistoryItems_sp]
-- Add the parameters for the stored procedure here
DECLARE	@scriptId INT = 100436,
	@uniqueResponseId INT = 163390
--AS
BEGIN
    SET NOCOUNT ON;
    
    DECLARE @resultTable TABLE(
                DateOfMarking DATETIME
               ,Firstname NVARCHAR(50)
               ,Surname NVARCHAR(50)
               ,MARK DECIMAL(18 ,0)
               ,TotalMark DECIMAL(18 ,0)
               ,Comment NVARCHAR(MAX)
               ,TYPE INT
              /* ,CI_Review BIT NULL
			   ,ID INT NULL*/
            );
    
    
    -- get last valid group for a script
    DECLARE @lastValidGroup INT = (
						SELECT TOP 1 GroupDefinitionID FROM ..CandidateGroupResponses CGR
						INNER JOIN UniqueGroupResponses UGR ON UGR.ID = CGR.UniqueGroupResponseID
						INNER JOIN UniqueGroupResponseLinks UGRL ON UGRL.UniqueGroupResponseID = UGR.ID
						WHERE  CGR.CandidateExamVersionID = @scriptId
						AND UGRL.UniqueResponseId = @uniqueResponseId
						ORDER BY GroupDefinitionID DESC
	)
    
    INSERT INTO @resultTable
    SELECT AGM.Timestamp     AS DateOfMarking
          ,U.Forename        AS Firstname
          ,U.Surname         AS Surname
          ,AIM.AssignedMark  AS MARK
          ,I.TotalMark       AS TotalMark
          ,NULL              AS Comment
          ,(
               CASE 
                    WHEN AGM.MarkingMethodId=4 THEN 2 -- Actual marking = LiveMarking
                    WHEN AGM.MarkingMethodId=5 THEN 1 -- Control Item = CreateCI
                    WHEN AGM.MarkingMethodId=9 THEN 3 -- Discarded = Discarded
                    WHEN AGM.MarkingMethodId=12 THEN 5 -- Auto = Auto
                    WHEN AGM.MarkingMethodId=14 THEN 3 -- LiveMark Discard = Discard
                    WHEN AGM.MarkingMethodId=8 THEN 3 -- Discard Awating Review CI = Discard
               END
           )                 AS TYPE
          /*,UGR.CI_Review     AS CI_Review
			,AGM.ID*/
    FROM   dbo.AssignedGroupMarks AGM
           INNER JOIN dbo.AssignedItemMarks AIM
                ON  AGM.ID = AIM.GroupMarkId
           INNER JOIN dbo.Users U
                ON  U.ID = AGM.UserId
           INNER JOIN dbo.UniqueResponses UR
                ON  UR.id = AIM.UniqueResponseId
           INNER JOIN dbo.Items I
                ON  UR.itemId = I.ID
           /*INNER JOIN dbo.UniqueGroupResponses UGR
				ON  UGR.ID = AGM.UniqueGroupResponseId*/
    WHERE  UR.id = @uniqueResponseId
           AND AGM.MarkingMethodId IN (4 ,5 ,9 ,12 ,14, 8)
           --AND AGM.UniqueGroupResponseId in (SELECT CGR.UniqueGroupResponseID FROM dbo.CandidateGroupResponses CGR WHERE CGR.CandidateExamVersionID = @scriptId)
           AND AGM.GroupDefinitionID = @lastValidGroup
       --SELECT *
       --FROM @resultTable    
           
    /*----------- 
    --Here we find and delete CI that have not yet been reviewed 
    DECLARE @latestCITable TABLE(CI_Review BIT ,id INT);
    
	INSERT INTO @latestCITable
	  (
		CI_Review
	   ,id
	  )
	SELECT TOP 1             CI_Review
		  ,id
	FROM   @resultTable
	WHERE  [TYPE] = 1
	ORDER BY
		   DateOfMarking     DESC
    
	IF (
		   (
			   SELECT TOP 1 CI_Review
			   FROM   @latestCITable
		   )=0
	   )
	BEGIN
		DELETE 
		FROM   @resultTable
		WHERE  id = (
				   SELECT TOP 1 id
				   FROM   @latestCITable
			   )
	END    	
    -------------*/
    
    INSERT INTO @resultTable
    SELECT M.Timestamp  AS DateOfMarking
          ,U.Forename   AS Firstname
          ,U.Surname    AS Surname
          ,M.Mark       AS MARK
          ,I.TotalMark  AS TotalMark
          ,M.Comment    AS Comment
          ,4            AS TYPE -- Moderated
         /* ,NULL
		  ,NULL*/
    FROM   dbo.Moderations M
           INNER JOIN dbo.Users U
                ON  U.ID = M.UserId
           INNER JOIN dbo.UniqueResponses UR
                ON  UR.id = M.UniqueResponseId
           INNER JOIN dbo.Items I
                ON  UR.itemId = I.ID
    WHERE  UR.id = @uniqueResponseId
           AND M.CandidateExamVersionId = @scriptId
    
    SELECT DateOfMarking
          ,Firstname
          ,Surname
          ,MARK
          ,TotalMark
          ,Comment
          ,TYPE
    FROM   @resultTable
    ORDER BY
           DateOfMarking
END
