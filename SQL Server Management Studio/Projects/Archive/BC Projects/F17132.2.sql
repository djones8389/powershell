USE [BRITISHCOUNCIL_SecureMarker]
GO
/****** Object:  UserDefinedFunction [dbo].[sm_checkGroupAndItemsMarkedInTolerance_fn]    Script Date: 05/22/2014 10:52:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Anton Burdyugov
-- Create date: 28/06/2013
-- Description:	Check all items in some group marked in tolerance
-- =============================================
--ALTER FUNCTION [dbo].[sm_checkGroupAndItemsMarkedInTolerance_fn]
--(
declare
	-- Add the parameters for the function here
	@assignedGroupMarkId     INT = 52190
   ,@uniqueGroupResponse     INT = 59645
   ,@groupDeviation          DECIMAL = 2.0000000000
   ,@groupTolerance          DECIMAL = 1.0000000000
--)
--RETURNS BIT
--AS
--BEGIN
--    -- Declare the return variable here
--    DECLARE @myCheck BIT=0
--    --52190	59645	2.0000000000	1.0000000000--
--    -- Add the T-SQL statements to compute the return value here
    
    
--    declare @isAutoGroup BIT = (SELECT GD.IsAutomaticGroup FROM dbo.GroupDefinitions GD 
--		INNER JOIN UniqueGroupResponses UGR ON UGR.ID = @uniqueGroupResponse AND UGR.GroupDefinitionID = GD.ID)
	
--    IF (@groupDeviation>@groupTolerance) -- group marked out of tolerance
--    BEGIN
--        SET @myCheck = 1
--    END
--    ELSE 
--    IF (
--			-- if autogroup check only group tolerance
--			@isAutoGroup = 0 AND
--           -- group marked within tolerance, so we need the same check on item level
--           EXISTS
--           (
               SELECT *
               FROM   dbo.AssignedItemMarks AIM
                      INNER JOIN dbo.UniqueResponses UR
                           ON  UR.ID = AIM.UniqueResponseId
                      INNER JOIN dbo.Items I
                           ON  I.ID = UR.itemId
                      INNER JOIN dbo.UniqueGroupResponseLinks UGRL
                           ON  UGRL.UniqueResponseId = UR.id
                      INNER JOIN dbo.UniqueGroupResponses UGR
                           ON  UGR.Id = UGRL.UniqueGroupResponseID
                      INNER JOIN dbo.GroupDefinitionItems GDI
                           ON  GDI.itemId = UR.itemId
                               AND GDI.GroupID = UGR.GroupDefinitionID
                               AND GDI.ViewOnly = 0
               WHERE  AIM.MarkingDeviation>I.CIMarkingTolerance -- the main check
                      AND AIM.GroupMarkId = @assignedGroupMarkId
                      AND UGRL.UniqueGroupResponseID = @uniqueGroupResponse
--           )
--       )
--    BEGIN
--        SET @myCheck = 1
--    END
    
--    -- Return the result of the function
--    --RETURN @myCheck
--END
