SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

;with cte as (

SELECT ExternalSessionID, Keycode
FROM CandidateExamVersions
WHERE NOT EXISTS(
      SELECT CandidateExamVersions.ID
		FROM CandidateResponses
		  
		  INNER JOIN UniqueResponses
		  ON UniqueResponses.id = CandidateResponses.UniqueResponseID
			  
			  WHERE CandidateResponses.CandidateExamVersionID = CandidateExamVersions.ID
					AND CONVERT(NVARCHAR(MAX), responseData) <> ''
	)
)


SELECT CEV.Keycode, ExamSessionState, DateSubmitted, cev.Mark, CEV.TotalMark, I.ExternalItemID, UR.responseData
FROM CandidateExamVersions as CEV

INNER JOIN CandidateResponses as CR
ON CR.CandidateExamVersionID = CEV.ID

INNER JOIN UniqueResponses as UR
ON UR.ID = UniqueResponseID 

left join UniqueResponseDocuments as URD
ON URD.UniqueResponseID = UR.id 
	
Inner Join Items as I
on I.ID = ur.itemId

inner join ExamVersions as EV
on EV.ID = I.ExamVersionID

INNER JOIN Exams as E
ON E.ID = EV.ExamID

INNER JOIN Qualifications
ON E.QualificationID = Qualifications.ID

where cev.ExternalSessionID In (select ExternalSessionID from cte)
 order by  cev.DateSubmitted desc;