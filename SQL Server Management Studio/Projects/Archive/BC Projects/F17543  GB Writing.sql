select

	KeyCode
	,foreName
	,surName
	,ItemResponseData.p.value('@id', 'nvarchar(15)') as itemId
	,c.i.value('.[1]', 'nvarchar(max)') as userData

from WAREHOUSE_ExamSessionTable_Shreded 


inner join WAREHOUSE_ExamSessionItemResponseTable 
on WAREHOUSE_ExamSessionItemResponseTable.WAREHOUSEExamSessionID = WAREHOUSE_ExamSessionTable_Shreded.examSessionId

cross apply ItemResponseData.nodes('/p') ItemResponseData(p)
cross apply ItemResponseData.nodes('/p/s/c/i') c(i)

where KeyCode in ('K523AU01','ED4BB601','RT89FF01','7JSC6901','ZNKQQL01','TNJP6V01','DGL69Q01','VLSG4R01','86HWNK01','7WQ89G01','MVW3YU01','T629RN01','C94SQM01','QGC33H01','6HTMU601','9B7N8J01','FW74TK01','DVMAAL01','DBD9UC01','L9DSTG01','YNXWAN01','2BLCA801','VHYV8K01','M2KTXD01','NS9SAC01','65LLE701','2FF7X301','TMRR2A01','8L7HP401','VBQSDY01','GYHYPC01','RE3J5401','4XJPLX01','3Y3QBW01','62NUDS01','C6UB7S01','FHMH5J01','EQ8SX301','KD4HRK01','93CT9501')
