--select warehousetime, centreCode, * from WAREHOUSE_ExamSessionTable_Shreded where KeyCode = 'QNZKD801'
--select * from UserTable where Username = 'superuser'
--2014-06-01 07:53:29.747--


USE [BRITISHCOUNCIL_SecureAssess]
GO
/****** Object:  StoredProcedure [dbo].[sa_SECUREASSESSINTEGRATION_getCandidateResults_sp]    Script Date: 06/10/2014 15:28:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- 20/09/2012 DS				Added ExamName, ExamVersionName and ItemDataFull
-- 27/09/2013 RB				Use Shredded table WAREHOUSE_ExamSessionTable_Shreded
declare
--ALTER PROCEDURE [dbo].[sa_SECUREASSESSINTEGRATION_getCandidateResults_sp]    
 @startDate      nvarchar(max) = '01/06/2014 07:00:01',    
 @endDate      nvarchar(max) = '01/06/2014 09:00:01',    
 @examReference     nvarchar(max) = '%',    
 @centreReference    nvarchar(max) = '%',    
 @useSubmissionTime    bit = 0,    
 @pageNumber      decimal = 1,  
 @userID       decimal =1,
 @warehouseStates	nvarchar(20) = 1
  
      
--WITH_ENCRYPTION_REPLACE_ME_FOR_LOCAL--    
--AS    
BEGIN    
    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
 SET DATEFORMAT dmy;    
    
 DECLARE @errorReturnString nvarchar(max)    
 DECLARE @errorNum  nvarchar(100)    
 DECLARE @errorMess nvarchar(max)    
    
    
 DECLARE @pageSize   decimal    
 SET @pageSize = 100    
    
 DECLARE @mySelect nvarchar(max)    
 SET @mySelect = null    
    
    
 DECLARE @myWhere nvarchar(max)    
 SET @myWhere = null    
 DECLARE @myRest nvarchar(max)    
 SET @myRest = null    
 DECLARE @myFinalQuery nvarchar(max)    
 SET @myFinalQuery  = null    
     
     
    
    
    
  BEGIN TRY    
    
   SET @mySelect = '    
    WITH MyCTE AS    
    (    
    
    SELECT x.* FROM (    
     SELECT    
      ROW_NUMBER() OVER(ORDER BY [WAREHOUSE_ExamSessionTable_Shreded].[warehouseTime]) AS ''RowNumber'',    
      [WAREHOUSE_ExamSessionTable_Shreded].[examSessionid] AS ''ID'',    
      [WAREHOUSE_ScheduledExamsTable].[ExamID] AS ''ExamID'',    
      [WAREHOUSE_ExamSessionTable_Shreded].[warehouseTime] AS ''WarehousedTime'',    
      [WAREHOUSE_ExamSessionTable_Shreded].[examRef] AS ''ExamRef'', 
      [WAREHOUSE_ExamSessionTable_Shreded].[examVersionRef] AS ''ExamVersionRef'',          
      [WAREHOUSE_ScheduledExamsTable].[examVersionId] AS ''ExamVersionID'',    
      [WAREHOUSE_ExamSessionTable_Shreded].[qualificationRef] AS ''qualificationReference'',        
      [WAREHOUSE_ExamSessionTable_Shreded].[validFromDate] AS ''start'',        
      [WAREHOUSE_ExamSessionTable_Shreded].[expiryDate] AS ''end'',      
      [WAREHOUSE_ExamSessionTable_Shreded].[totalTimeSpent] AS ''ActualDuration'',     
      Convert(nvarchar(max),[WAREHOUSE_ExamSessionTable_Shreded].[scheduledDurationValue]) AS ''allowedDuration'',        
      [WAREHOUSE_ExamSessionTable_Shreded].[defaultDuration] AS ''defaultDuration'',        
      [WAREHOUSE_ExamSessionTable_Shreded].[scheduledDurationReason] AS ''reason'',            
      [WAREHOUSE_CentreTable].[CentreID] AS ''CentreId'',    
      [WAREHOUSE_CentreTable].[CentreName] AS ''CentreName'',    
      [WAREHOUSE_UserTable].[Userid] AS ''CandidateID'',    
      [WAREHOUSE_UserTable].[Gender] AS ''CandidateGender'',    
      [WAREHOUSE_UserTable].[DOB] AS ''CandidateDOB'',    
      [WAREHOUSE_UserTable].[EthnicOrigin] AS ''CandidateEthnicOriginID'',    
      [WAREHOUSE_UserTable].[CandidateRef] AS ''CandidateRef'',    
      [WAREHOUSE_UserTable].[Surname] AS ''CandidateSurname'',    
      [WAREHOUSE_UserTable].[Forename] AS ''CandidateForename'',    
      [WAREHOUSE_ExamSessionTable_Shreded].[userMark] AS ''CandidateMark'',    
      [WAREHOUSE_ExamSessionTable_Shreded].[passMark] AS ''PassMark'',    
      [WAREHOUSE_ExamSessionTable_Shreded].[totalMark] AS ''TotalMark'',    
      [WAREHOUSE_ExamSessionTable_Shreded].[previousExamState] As ''PreviousExamState'',    
      [VoidJustificationLookupTable].[name] AS ''VoidReason'',    
      Convert(nvarchar(max),[WAREHOUSE_ExamSessionTable_Shreded].[passValue]) AS ''PassValue'',    
      [WAREHOUSE_ExamSessionTable_Shreded].[automaticVerification] AS ''AutoVerify'',    
      [WAREHOUSE_ExamSessionTable_Shreded].[resultSampled] AS ''resultSampled'',
      SUBSTRING(CONVERT(nvarchar(max),[WAREHOUSE_ExamSessionTable_Shreded].[started],113),0,24) AS ''started'',    
      [WAREHOUSE_ExamSessionTable_Shreded].[submitted] AS ''submitted'',    
      Convert(xml,Convert(nvarchar(max),''<sectionDataRaw>'' + Convert(nvarchar(max),[WAREHOUSE_ExamSessionTable_Shreded].[resultData].query(''//section'')) + ''</sectionDataRaw>'')) AS ''sectionDataRaw'' ,    
      dbo.fn_getMarkers([WAREHOUSE_ExamSessionTable_Shreded].[examSessionId]) AS ''Marker'',           
      [WAREHOUSE_ScheduledExamsTable].[ScheduledStartDateTime] AS ''WindowsStartTime'',  
	  [WAREHOUSE_ExamSessionTable_Shreded].[TakenThroughLocalScan] AS ''TakenThroughLocalScan'',   
      [WAREHOUSE_CentreTable].[CentreCode] AS ''CentreRef'',    
      [WAREHOUSE_ExamSessionTable_Shreded].[originalGrade] AS ''Grade'' ,   
	  [WAREHOUSE_ExamSessionTable_Shreded].[KeyCode] AS ''keyCode'' ,  
      [WAREHOUSE_UserTable].[ULN] AS ''ULN'',
      [WAREHOUSE_ExamSessionTable_Shreded].[reMarkStatus] AS ''ReMarkStatus'',
      [WAREHOUSE_ExamSessionTable_Shreded].[CQN] as ''CQN'',
      [WAREHOUSE_ExamSessionTable_Shreded].[passType] AS ''PassType'',
      [WAREHOUSE_ExamSessionTable_Shreded].[WarehouseExamState] AS ''WarehouseExamState'',
	  [WAREHOUSE_ExamSessionTable_Shreded].[examName]  AS ''ExamName'',
	  [WAREHOUSE_ExamSessionTable_Shreded].[examVersionName] AS ''ExamVersionName'',								
	  [WAREHOUSE_ExamSessionTable_Shreded].[itemDataFull] AS ''itemDataFull''
      FROM [dbo].[WAREHOUSE_ExamSessionTable_Shreded]    
      INNER JOIN [dbo].[WAREHOUSE_ScheduledExamsTable]    
       ON [dbo].[WAREHOUSE_ScheduledExamsTable].[ID] = [WAREHOUSE_ExamSessionTable_Shreded].[WAREHOUSEScheduledExamID]    
      INNER JOIN [dbo].[WAREHOUSE_CentreTable]    
       ON [dbo].[WAREHOUSE_CentreTable].[ID] = [WAREHOUSE_ScheduledExamsTable].[WAREHOUSECentreID]    
      INNER JOIN [dbo].[WAREHOUSE_UserTable]    
       ON [dbo].[WAREHOUSE_UserTable].[ID] = [WAREHOUSE_ExamSessionTable_Shreded].[WAREHOUSEUserID]
      LEFT JOIN [dbo].[VoidJustificationLookupTable]    
       ON [dbo].[WAREHOUSE_ExamSessionTable_Shreded].[voidJustificationLookupTableId] = [VoidJustificationLookupTable].[ID]
        
     '    
    
    
    
  DECLARE @myDateClause nvarchar(max)  
  DECLARE @myTestCentreClause nvarchar(max)  
  DECLARE @myPracticeTestClause nvarchar(max)  
  DECLARE @myCentreRefClause nvarchar(max)  
  DECLARE @myExamRefClause nvarchar(max)  
  DECLARE @myOmrClause nvarchar(max)  
  DECLARE @myPermissionClause nvarchar(max)  
    
  IF (@useSubmissionTime = 0)  
   BEGIN  
    SET @myDateClause = '(  
           [WAREHOUSE_ExamSessionTable_Shreded].[warehouseTime] >= ''' +@startDate+ '''  
           AND   
           [WAREHOUSE_ExamSessionTable_Shreded].[warehouseTime] <= ''' +@endDate+ '''  
           )'  
   END  
  ELSE  
   BEGIN  
    SET @myDateClause = '(  
           [WAREHOUSE_ExamSessionTable_Shreded].[submittedDate] >= ''' +@startDate+ '''  
           AND   
           [WAREHOUSE_ExamSessionTable_Shreded].[submittedDate] <= ''' +@endDate+ '''  
           )'  
   END  
    
  SET @myTestCentreClause = 'AND ([WAREHOUSE_CentreTable].[CentreName] NOT LIKE ''%BTL Test Centre%'')'  
    
  SET @myPracticeTestClause = 'AND ([WAREHOUSE_ExamSessionTable_Shreded].[examRef] NOT LIKE ''%/PRACTICE%'')'  
    
  IF(@centreReference like '%')  
   BEGIN  
    SET @myCentreRefClause = ''  
   END  
  ELSE  
   BEGIN  
    SET @myCentreRefClause = 'AND ([WAREHOUSE_CentreTable].[CentreCode]  = ''' + convert(nvarchar(max),@centreReference) +''')'  
   END  
     
  IF(@examReference like '%')  
   BEGIN  
    SET @myExamRefClause = ''  
   END  
  ELSE  
   BEGIN  
    SET @myExamRefClause = 'AND ([WAREHOUSE_ExamSessionTable_Shreded].[examRef]  = ''' + convert(nvarchar(max),@examReference) +''')'  
   END  
      
  SET @myOmrClause = 'AND (not WAREHOUSE_ExamSessionTable_Shreded.previousexamstate = 17)'  
   
  SET @myPermissionClause = ' AND   
         (  
          (  
           '+convert(nvarchar(max),@userID)+ ' = 1   
           OR   
           (  
            [WAREHOUSE_CentreTable].[CentreID] IN   
            (  
             Select   
              [AssignedUserRolesTable].CentreID   
             FROM AssignedUserRolesTable   
             INNER JOIN RolePermissionsTable   
              ON AssignedUserRolesTable.RoleID = RolePermissionsTable.RoleID  
             WHERE       
             (  
              (   
               RolePermissionsTable.PermissionID = 29)   
               AND    
               [AssignedUserRolesTable].UserID = '+convert(nvarchar(max),@userID)+ '  
              )  
              AND   
              (  
                [WAREHOUSE_ExamSessionTable_Shreded].[qualificationid]
               IN   
               (  
                Select   
                 QualificationID   
                From UserQualificationsTable   
                Where UserID = '+convert(nvarchar(max),@userID)+ '  
               )  
              )  
             )  
            )  
           )  
           OR  
           (  
            EXISTS   
            (  
             SELECT   
              UserID   
             FROM AssignedUserRolesTable   
             INNER JOIN RolePermissionsTable   
              ON AssignedUserRolesTable.RoleID = RolePermissionsTable.RoleID  
             WHERE   
              PermissionID = 29   
              AND   
              UserID = '+convert(nvarchar(max),@userID)+ '   
              AND   
              CentreID = 1  
            )  
           )  
          )'  
  DECLARE @myWarehouseStates	nvarchar(200)
  SET @myWarehouseStates = ' AND [WAREHOUSE_ExamSessionTable_Shreded].WarehouseExamState IN (' + @warehouseStates + ')'
  SET @myWhere = 'WHERE ' + @myDateClause + @myTestCentreClause + @myPracticeTestClause + @myCentreRefClause + @myExamRefClause + @myOmrClause + @myPermissionClause + @myWarehouseStates
    
  Set @myRest = '  
       ) As X  
      )  
  
      SELECT  
       *,   
       (SELECT COUNT(RowNumber) FROM MyCTE) As ''TotalResultsAvailable'',  
       '+ convert(nvarchar(max),@pageSize) + ' AS ''PageSize''   
      FROM MyCTE   
      WHERE   
       RowNumber  > (' + convert(nvarchar(max),@pageSize) + ' * (' + convert(nvarchar(max), @pageNumber) + ' - 1))  
       AND   
       RowNumber  <= (' + convert(nvarchar(max),@pageSize) + ' *' +  convert(nvarchar(max), @pageNumber) + ')  
      ORDER BY   
        [RowNumber] ASC  
       ,[ExamVersionRef] ASC  
       ,[CentreId] ASC  
       ,[CandidateSurname] ASC  
       ,[ID] ASC  
    '  
      
  Set @myFinalQuery =  @mySelect + @myWhere + @myRest    
      
    --print @myFinalQuery    
    SELECT @myFinalQuery     as test
    exec sp_executesql @myFinalQuery, N'@startDate nvarchar(max), @endDate nvarchar(max), @centreReference nvarchar(max), @examReference nvarchar(max),    
      @pageSize nvarchar(max), @pageNumber decimal',     
     @startDate, @endDate, @centreReference, @examReference, @pageSize, @pageNumber    
    
      
 END TRY    
 BEGIN CATCH    
  DECLARE @ErrorMessage NVARCHAR(4000);    
  DECLARE @ErrorSeverity INT;    
    
  SELECT @ErrorMessage = ERROR_MESSAGE(),    
      @ErrorSeverity = ERROR_SEVERITY();    
    
  -- Use RAISERROR inside the CATCH block to return     
  -- error information about the original error that     
  -- caused execution to jump to the CATCH block.    
  RAISERROR (@ErrorMessage, -- Message text.    
       @ErrorSeverity, -- Severity.    
       1 -- State.    
       );    
 END CATCH    
    
END
