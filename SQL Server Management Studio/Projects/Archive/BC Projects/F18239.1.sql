SELECT --GroupDefinitions.ID, --tokenID, parkeduserid, 
*
FROM Candidateexamversions 
INNER JOIN CandidateResponses
ON CandidateExamVersionID = Candidateexamversions.ID
INNER JOIN UniqueResponses
ON UniqueResponseID = UniqueResponses.ID
INNER JOIN UniqueGroupResponseLinks
ON UniqueGroupResponseLinks.UniqueResponseID = UniqueResponses.id
INNER JOIN UniqueGroupResponses
ON UniqueGroupResponses.ID = UniqueGroupResponseLinks.UniqueGroupResponseID
where keycode in ('5XPLMJ01','YZXN5A01')

SELECT *
FROM UniqueResponses
WHERE NOT EXISTS(SELECT * FROM UniqueGroupResponseLinks WHERE UniqueGroupResponseLinks.UniqueResponseId = UniqueResponses.id)