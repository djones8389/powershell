SELECT urd.* --responseData.exist('p'), *--CEV.ExternalSessionID
FROM CandidateExamVersions as CEV

left JOIN CandidateResponses as CR
ON CR.CandidateExamVersionID = CEV.ID

left JOIN UniqueResponses as UR
ON UR.ID = CR.UniqueResponseID 

left join UniqueResponseDocuments as URD
ON URD.UniqueResponseID = UR.id 

 where KeyCode = 'HK3XUA01'