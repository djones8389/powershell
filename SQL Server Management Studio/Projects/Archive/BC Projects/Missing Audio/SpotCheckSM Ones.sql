SELECT 
	cev.Keycode, urd.*
		FROM CandidateExamVersions as CEV

		INNER JOIN CandidateResponses as CR
		ON CR.CandidateExamVersionID = CEV.ID

		INNER JOIN UniqueResponses as UR
		ON UR.ID = UniqueResponseID 

		left join UniqueResponseDocuments as URD
		ON URD.UniqueResponseID = UR.id 
			
		Inner Join Items as I
		on I.ID = ur.itemId

		inner join ExamVersions as EV
		on EV.ID = I.ExamVersionID

		INNER JOIN Exams as E
		ON E.ID = EV.ExamID
		 
		INNER JOIN Qualifications as Q
		ON E.QualificationID = Q.ID

where KeyCode in ('T8RN2U99','AV8VG201','RQXBLZ01','TN2MAN01','KN5RWB01','8PDKPV01','DJAX5501','3L8AQ701','HK3XUA01','Z5ZG2D01','G3TB8B01','WNPJTB01','Q8UQUV01','SXGS4R01','QGUHCQ01','QZWAU601','75RFQF01','T3GM2E01','7756UW01')
	order by cev.ExternalSessionID asc

where urd.Blob is null
 and DateSubmitted > '01 Jan 2014'
 and E.Name = 'Speaking'