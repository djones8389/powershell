SELECT EST.ID, EST.KeyCode, EST.examState--, UT.Forename, UT.Surname--, CT.CentreName, SCET.examName, ExportToSecureMarker
	--, StructureXML
	--, est.AwaitingUploadGracePeriodInDays
	--, attemptAutoSubmitForAwaitingUpload
	, resultData.query('data(/exam/@userMark)') as userMark
	, ExportToSecureMarker
	, AwaitingUploadStateChangingTime
	, attemptAutoSubmitForAwaitingUpload
	, AwaitingUploadGracePeriodInDays
  FROM ExamSessionTable as EST

  Inner Join ScheduledExamsTable as SCET
  on SCET.ID = EST.ScheduledExamID
  
  Inner Join UserTable as UT
  on UT.ID = EST.UserID

  Inner Join CentreTable as CT
  on CT.ID = SCET.CentreID


--update ExamSessionTable
--set AwaitingUploadGracePeriodInDays = 2 
----where ID = 758267
Where KeyCode in ('2Y33PX99','CERWJN99','4EX82M99','UYGJPB99','JDMPED99','VS58SK99','5B7S4M99','CPHXFX99','P63AA599','3P5S9W99','24CBNV99','4XHAG499','AEDXQX99','XESPKG99','4AL8FP99','K9PPTF99','7YV8DL99','WTJLLN99','3LMMVZ99','7TGJBY99','LA62F799','GAQWWY99','MRRDRT99','MUMM4C99','CNQHZ499','GJ7P8W99','JS764X99','BDMKW899','RBD6KY99','G9BGUT99','K9594K99','R8SY3799','FCADBS99','N8TR5C99','QUNH4H99','FKEUAM99','SDJDY699','G72KEM99','M3QDSV99','W2FD6W99','TMNL7B99','3J87J599','JU6WQX99','PUNYXF99','2U7J3999','49JPNH99','ZE3YME99','7B9Q5V99','S5VNX799','YAMP5T99','UJJ2GV99','LM6JA399','FSLUN299','P6A6L799','LMXYVA99','HVHPTV99','BSREBC99','QHC24X99','G5YY7D99')



--select * from ExamSessionItemResponseTable where ExamSessionID in ('768056','783053','767981','783055','771033','768041','783068','770311','767978','768036','768035','783054','771034','770319','770315','771036','783612','767976','771025','768039','772766','772764','783614','783615','774886','771026','771029','771045','783628','768037','771031','783070','772771','770314','771039','783047','783613','774866','771030','771035','772765','768054','783629','783050','783627','772772','772776','771046','783049','774871','774878','783067','768042','768040','774887','770309','770310','783052','783048')