SELECT CEV.CandidateForename + ' ' + CEV.CandidateSurname
FROM   UniqueGroupResponses UGR

       INNER JOIN GroupDefinitions GD
       ON  GD.ID = UGR.GroupDefinitionID
       
       LEFT JOIN Users U
       ON  U.ID = UGR.ParkedUserID
       
       Inner join CandidateGroupResponses as CGR
       on CGR.UniqueGroupResponseID = UGR.GroupDefinitionID
       
       Inner join CandidateResponses as CR
       on CR.UniqueResponseID = CGR.CandidateExamVersionID
       
       inner join CandidateExamVersions as CEV
       on CEV.ID = CR.CandidateExamVersionID
       
       where ugr.ID = 563630
       
order by cev.id desc