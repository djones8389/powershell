USE [BRITISHCOUNCIL_SecureMarker]
GO

/****** Object:  View [dbo].[EscalatedGroups_view]    Script Date: 07/23/2014 10:36:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Anton Burdyugov
-- Create date: 2013-06-26
-- Description:	Data about escalated(parked) groups.
-- Author:		Anton Burdyugov
-- Create date: 2013-07-03
-- Description:	Data from deleted user
-- =============================================
--ALTER VIEW [dbo].[EscalatedGroups_view]
--AS
 
SELECT --GD.ExamVersionID              AS ExamVersionID
      --,UGR.Id                        AS ID
      --,GD.Name                       AS NAME
      --,ISNULL(U.Surname+', '+U.Forename ,'Unknown') AS Examiner
      --,UGR.ParkedDate                AS ParkedDate
      --,UGR.ParkedAnnotation          AS ParkedAnnotation
      --,ISNULL(U.Surname ,'Unknown')  AS Surname
      --,ISNULL(U.Id ,-1)              AS UserID
      ugr.ID
      ,cev.CandidateForename
      ,Cev.CandidateSurname
      ,cev.Keycode
FROM   UniqueGroupResponses UGR

       INNER JOIN GroupDefinitions GD
       ON  GD.ID = UGR.GroupDefinitionID
       
       LEFT JOIN Users U
       ON  U.ID = UGR.ParkedUserID
       
       Inner join CandidateGroupResponses as CGR
       on CGR.UniqueGroupResponseID = UGR.GroupDefinitionID
       
       Inner join CandidateResponses as CR
       on CR.UniqueResponseID = CGR.CandidateExamVersionID
       
       inner join CandidateExamVersions as CEV
       on CEV.ID = CR.CandidateExamVersionID

       
WHERE  UGR.ParkedUserID IS NOT NULL
       AND (U.InternalUser=0 OR U.Username = 'superuser' OR U.ID IS NULL)
       AND EXISTS (
               SELECT *
               FROM   GroupDefinitionStructureCrossRef GDSCR
                      INNER JOIN ExamVersionStructures EVS
                           ON  EVS.ID = GDSCR.ExamVersionStructureID
               WHERE  GDSCR.Status = 0
                      AND GDSCR.GroupDefinitionID = GD.ID
                      AND EVS.StatusID = 0
           )
and ugr.ID = 563630

GO