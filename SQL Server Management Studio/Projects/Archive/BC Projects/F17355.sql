SELECT WEST.ID
	,WEST.KeyCode
	,WUT.Forename
	,WUT.Surname
	,WUT.CandidateRef
	,WCT.CentreName
	,WSCET.examName
	,wests.warehousetime
	,WESTS.ExternalReference
	,wests.userMark
	,WESTS.userPercentage
	,WEST.WarehouseExamState
FROM WAREHOUSE_ExamSessionTable AS WEST
INNER JOIN WAREHOUSE_ScheduledExamsTable AS WSCET ON WSCET.ID = WEST.WAREHOUSEScheduledExamID
INNER JOIN WAREHOUSE_UserTable AS WUT ON WUT.ID = WEST.WAREHOUSEUserID
INNER JOIN WAREHOUSE_CentreTable AS WCT ON WCT.ID = WSCET.WAREHOUSECentreID
INNER JOIN WAREHOUSE_ExamSessionTable_Shreded AS WESTS ON WESTS.examSessionId = WEST.ID
WHERE west.ExportToSecureMarker = 1
	AND west.KeyCode collate Latin1_General_CI_AS IN (
		SELECT ScheduledExamRef
		FROM [BritishCouncil_TestPackage]..ScheduledPackageCandidateExams
		WHERE IsCompleted = 1
			AND Score = '0.00'
		)
	AND west.KeyCode collate Latin1_General_CI_AS IN (
		SELECT cev.KeyCode
		FROM [430326-BC-SQL2\SQL2].BRITISHCOUNCIL_SecureMarker.dbo.CandidateExamVersions as CEV
	
		where PercentageMarkingComplete = '100'
			and cev.examsessionstate = 7
			and Mark > 0
		)
	 AND WESTS.userMark = 0 		
	 
ORDER BY west.ID desc


/*

GKNTDU01
25MWYT01
QNZKD801

*/

select  userMark, userPercentage,* from WAREHOUSE_ExamSessionTable_Shreded where KeyCode in ('QNZKD801')
SELECT * FROM [430326-BC-SQL2\SQL2].BRITISHCOUNCIL_SecureMarker.dbo.CandidateExamVersions where KeyCode  = 'QNZKD801'
select * from BritishCouncil_TestPackage..ScheduledPackageCandidateExams where ScheduledExamRef = 'QNZKD801'

SELECT lastexportdate, *
FROM [430326-BC-SQL2\SQL2].BRITISHCOUNCIL_SecureMarker.dbo.CandidateExamVersions
WHERE keycode IN ('2837146822',
'5718351043',
'GKNTDU01',
'3205384745',
'7781154747',
'25MWYT01',
'QNZKD801',
'TS6AVU01')

select * from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID in (508851) order by DATE asc
select * from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID in (10) order by DATE asc

SELECT *
FROM WAREHOUSE_ExamStateAuditTable
INNER JOIN WAREHOUSE_ExamSessionTable
ON WarehouseExamSessionID = WAREHOUSE_ExamSessionTable.ID
WHERE KeyCode IN ('2837146822','5718351043','GKNTDU01','3205384745','7781154747','25MWYT01','QNZKD801','TS6AVU01')
ORDER BY KeyCode, Date

SELECT TOP 100 *
FROM WAREHOUSE_ExamSessionTable
WHERE EXISTS (SELECT WarehouseExamSessionID FROM WAREHOUSE_ExamStateAuditTable WHERE WarehouseExamSessionID = WAREHOUSE_ExamSessionTable.ID AND ExamState = 1)
AND NOT EXISTS (SELECT WarehouseExamSessionID FROM WAREHOUSE_ExamStateAuditTable WHERE WarehouseExamSessionID = WAREHOUSE_ExamSessionTable.ID AND ExamState = 4)
AND EXISTS (SELECT WarehouseExamSessionID FROM WAREHOUSE_ExamStateAuditTable WHERE WarehouseExamSessionID = WAREHOUSE_ExamSessionTable.ID AND ExamState = 2)
 AND ExportToSecureMarker = 1

select * from WAREHOUSE_ExamSessionTable_Shreded where Examsessionid in (508623,
508668,
508671,
508851,
508852,
508876,
509442)


select top 50 userMark, userPercentage, ExportToSecureMarker, ExportedToIntegration , KeyCode, previousExamState, WarehouseExamState, * from WAREHOUSE_ExamSessionTable_Shreded where examId = 182 and previousExamState != 10 order by examSessionId desc

select * from WAREHOUSE_ExamSessionItemResponseTable where WAREHOUSEExamSessionID in (508623,
508668,
508671,
508851,
508852,
508876,
509442)