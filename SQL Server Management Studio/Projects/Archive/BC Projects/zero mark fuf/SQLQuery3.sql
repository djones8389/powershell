select ID, resultData, resultDatafull, KeyCode, warehousetime from WAREHOUSE_ExamSessionTable, exporttosecuremarker	where Keycode in   ('HSPKQQ99','TRD5AE99','TTEPHU99')

select MarkerResponseData.value('(/entries/entry/assignedMark)[1]', 'nvarchar(1000)') as AssignedMark
	, ItemID
	,*
 from WAREHOUSE_ExamSessionItemResponseTable where WAREHOUSEExamSessionID = 607378

begin tran 
select MarkerResponseData.value('(/entries/entry/assignedMark)[1]', 'nvarchar(1000)') as AssignedMark
	, ItemID
	,*
 from WAREHOUSE_ExamSessionItemResponseTable where WAREHOUSEExamSessionID = 607380
 
update WAREHOUSE_ExamSessionItemResponseTable
set MarkerResponseData = '<entries>
  <entry>
    <comment>Computer marked</comment>
    <userId>-1</userId>
    <userForename>Computer</userForename>
    <userSurname>Marked</userSurname>
    <assignedMark>5</assignedMark>
    <version>0</version>
    <dateCreated>05/09/2014</dateCreated>
  </entry>
</entries>'
where id = 5495618;

update WAREHOUSE_ExamSessionItemResponseTable
set MarkerResponseData = '<entries>
  <entry>
    <comment>Computer marked</comment>
    <userId>-1</userId>
    <userForename>Computer</userForename>
    <userSurname>Marked</userSurname>
    <assignedMark>3</assignedMark>
    <version>0</version>
    <dateCreated>05/09/2014</dateCreated>
  </entry>
</entries>'
where id = 5495619;

update WAREHOUSE_ExamSessionItemResponseTable
set MarkerResponseData = '<entries>
  <entry>
    <comment>Computer marked</comment>
    <userId>-1</userId>
    <userForename>Computer</userForename>
    <userSurname>Marked</userSurname>
    <assignedMark>5</assignedMark>
    <version>0</version>
    <dateCreated>05/09/2014</dateCreated>
  </entry>
</entries>'
where id = 5495620;

update WAREHOUSE_ExamSessionItemResponseTable
set MarkerResponseData = '<entries>
  <entry>
    <comment>Computer marked</comment>
    <userId>-1</userId>
    <userForename>Computer</userForename>
    <userSurname>Marked</userSurname>
    <assignedMark>5</assignedMark>
    <version>0</version>
    <dateCreated>05/09/2014</dateCreated>
  </entry>
</entries>'
where id = 5495621;

update WAREHOUSE_ExamSessionItemResponseTable
set MarkerResponseData = '<entries>
  <entry>
    <comment>Computer marked</comment>
    <userId>-1</userId>
    <userForename>Computer</userForename>
    <userSurname>Marked</userSurname>
    <assignedMark>4</assignedMark>
    <version>0</version>
    <dateCreated>05/09/2014</dateCreated>
  </entry>
</entries>'
where id = 5495622;
select MarkerResponseData.value('(/entries/entry/assignedMark)[1]', 'nvarchar(1000)') as AssignedMark
	, ItemID
	,*
 from WAREHOUSE_ExamSessionItemResponseTable where WAREHOUSEExamSessionID = 607378
rollback

--in (5495618,5495619,5495620,5495621,5495622)


25	3520P3701
20	3520P3704
25	3520P3709
15	3520P3715
15	3520P3720

update WAREHOUSE_ExamSessionTable
set resultDataFull = '<assessmentDetails>
  <assessmentID>83</assessmentID>
  <qualificationID>119</qualificationID>
  <qualificationName>Aptis General</qualificationName>
  <qualificationReference>Aptis General</qualificationReference>
  <assessmentGroupName>Reading</assessmentGroupName>
  <assessmentGroupID>43</assessmentGroupID>
  <assessmentGroupReference>Reading</assessmentGroupReference>
  <assessmentName>Version 2</assessmentName>
  <validFromDate>07 Mar 2012</validFromDate>
  <expiryDate>08 Mar 2022</expiryDate>
  <startTime>00:00:00</startTime>
  <endTime>23:59:59</endTime>
  <duration>30</duration>
  <defaultDuration>30</defaultDuration>
  <scheduledDuration>
    <value>30</value>
    <reason />
  </scheduledDuration>
  <sRBonus>30</sRBonus>
  <sRBonusMaximum>30</sRBonusMaximum>
  <externalReference>August 2012_V2R</externalReference>
  <passLevelValue>0</passLevelValue>
  <passLevelType>1</passLevelType>
  <status>2</status>
  <testFeedbackType>
    <passFail>0</passFail>
    <percentageMark>0</percentageMark>
    <allowSummaryFeedback>0</allowSummaryFeedback>
    <summaryFeedbackType>1</summaryFeedbackType>
    <itemSummary>0</itemSummary>
    <itemReview>0</itemReview>
    <itemReviewCorrectAnswer>0</itemReviewCorrectAnswer>
    <itemFeedback>0</itemFeedback>
    <printableSummary>0</printableSummary>
    <candidateDetails>0</candidateDetails>
    <feedbackByReference>0</feedbackByReference>
    <allowFeedbackDuringExam>0</allowFeedbackDuringExam>
    <allowFeedbackDuringSummary>0</allowFeedbackDuringSummary>
  </testFeedbackType>
  <itemFeedback>0</itemFeedback>
  <allowCalculator>0</allowCalculator>
  <lastInUseDate>Sep 16 2014  2:57PM</lastInUseDate>
  <completedScriptReview>0</completedScriptReview>
  <assessment currentSection="0" totalTime="0" currentTime="0" originalPassMark="0" originalPassType="1" passMark="0" passType="1" totalMark="25" userMark="25" userPercentage="100" passValue="1" originalGrade="Pass">
    <intro id="0" name="Introduction" currentItem="">
      <item id="3521P3580" name="Reading_Welcome" totalMark="0" version="12" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
    </intro>
    <outro id="0" name="Finish" currentItem="" />
    <tools id="0" name="Tools" currentItem="" />
    <section id="1" name="Section 1" passLevelValue="0" passLevelType="1" itemsToMark="0" currentItem="" fixed="1" totalMark="25" userMark="159" userPercentage="636" passValue="1">
      <item id="3521P3615" name="Copy of Reading_V2_Task1_1.09.12" totalMark="5" version="39" markingType="0" markingState="0" userMark="5" markerUserMark="" userAttempted="1" viewingTime="" flagged="0" quT="12">
        <p cs="1" um="1" id="3521P3615" ua="1">
          <s id="1" ua="1" um="1">
            <c wei="1" id="50" um="1" ie="1" typ="12" ua="1">
              <i id="1" sl="2" />
            </c>
            <c wei="1" id="49" um="1" ie="1" typ="12" ua="1">
              <i id="1" sl="2" />
            </c>
            <c wei="1" id="46" um="1" ie="1" typ="12" ua="1">
              <i id="1" sl="3" />
            </c>
            <c wei="1" id="45" um="1" ie="1" typ="12" ua="1">
              <i id="1" sl="4" />
            </c>
            <c wei="1" id="44" um="1" ie="1" typ="12" ua="1">
              <i id="1" sl="3" />
            </c>
          </s>
        </p>
      </item>
      <item id="3521P3617" name="Copy of Reading_V2_Task_2_1.09.12" totalMark="6" version="34" markingType="0" markingState="0" userMark="6" markerUserMark="" userAttempted="1" viewingTime="" flagged="0" quT="13">
        <p cs="1" um="1" id="3521P3617" ua="1">
          <s id="1" ua="1" um="1">
            <c wei="1" id="46" um="1" ie="1" typ="13" ua="1">
              <i x0="315" id="1" y0="385" x="315" y="275" />
              <i x0="315" id="2" y0="495" x="315" y="330" />
              <i x0="315" id="3" y0="275" x="315" y="385" />
              <i x0="315" id="4" y0="550" x="315" y="440" />
              <i x0="315" id="5" y0="330" x="315" y="495" />
              <i x0="315" id="6" y0="440" x="315" y="550" />
            </c>
          </s>
        </p>
      </item>
      <item id="3521P3623" name="Copy of Reading_V2_Task_3_1.09.12" totalMark="7" version="45" markingType="0" markingState="0" userMark="7" markerUserMark="" userAttempted="1" viewingTime="" flagged="0" quT="16">
        <p cs="1" um="1" id="3521P3623" ua="1">
          <s id="1" ua="1" um="1">
            <c wei="1" id="8" um="1" ie="1" typ="16" ua="1">
              <z id="10" />
              <z id="11">
                <i x="605" y="585" ID="11" />
              </z>
              <z id="12">
                <i x="495" y="555" ID="12" />
              </z>
              <z id="5">
                <i x="122" y="272" ID="4" />
              </z>
              <z id="6">
                <i x="452" y="497" ID="3" />
              </z>
              <z id="7">
                <i x="547" y="332" ID="14" />
              </z>
              <z id="8">
                <i x="562" y="272" ID="2" />
              </z>
              <z id="1">
                <i x="605" y="555" ID="1" />
              </z>
              <z id="16">
                <i x="722" y="367" ID="9" />
              </z>
              <z id="13" />
              <z id="17">
                <i x="167" y="462" ID="10" />
              </z>
              <z id="19" />
              <z id="14" />
              <z id="15">
                <i x="512" y="402" ID="13" />
              </z>
              <z id="9" />
              <z id="4" />
              <z id="2" />
              <z id="3" />
              <z id="18">
                <i x="115" y="205" ID="18" />
              </z>
            </c>
          </s>
        </p>
      </item>
      <item id="3521P3621" name="Reading_V2_Task_4_1.09.12" totalMark="7" version="38" markingType="0" markingState="0" userMark="7" markerUserMark="" userAttempted="1" viewingTime="" flagged="0" quT="12">
        <p cs="1" um="1" id="3521P3621" ua="1">
          <s id="1" ua="1" um="1">
            <c wei="1" id="54" um="1" ie="1" typ="12" ua="1">
              <i id="1" sl="7" />
            </c>
            <c wei="1" id="58" um="1" ie="1" typ="12" ua="1">
              <i id="1" sl="6" />
            </c>
            <c wei="1" id="57" um="1" ie="1" typ="12" ua="1">
              <i id="1" sl="4" />
            </c>
            <c wei="1" id="56" um="1" ie="1" typ="12" ua="1">
              <i id="1" sl="5" />
            </c>
            <c wei="1" id="53" um="1" ie="1" typ="12" ua="1">
              <i id="1" sl="9" />
            </c>
            <c wei="1" id="55" um="1" ie="1" typ="12" ua="1">
              <i id="1" sl="3" />
            </c>
            <c wei="1" id="20" um="1" ie="1" typ="12" ua="1">
              <i id="1" sl="16" />
            </c>
          </s>
        </p>
      </item>
      <item id="3521P3581" name="Reading_Submit_Finish" totalMark="0" version="15" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0">
        <p cs="1" um="0" id="3521P3581" ua="0">
          <s id="1" ua="0" um="0" />
        </p>
      </item>
    </section>
  </assessment>
  <isValid>1</isValid>
  <isPrintable>0</isPrintable>
  <qualityReview>0</qualityReview>
  <numberOfGenerations>1</numberOfGenerations>
  <requiresInvigilation>1</requiresInvigilation>
  <advanceContentDownloadTimespan>120</advanceContentDownloadTimespan>
  <automaticVerification>1</automaticVerification>
  <offlineMode>0</offlineMode>
  <requiresValidation>0</requiresValidation>
  <requiresSecureClient>1</requiresSecureClient>
  <examType>0</examType>
  <advanceDownload>0</advanceDownload>
  <gradeBoundaryData>
    <gradeBoundaries passLevelType="1" adjustedGradeBoundaries="0">
      <grade modifier="lt" value="0" description="Fail" />
      <grade modifier="gt" value="0" description="Pass" />
    </gradeBoundaries>
  </gradeBoundaryData>
  <autoViewExam>0</autoViewExam>
  <autoProgressItems>0</autoProgressItems>
  <confirmationText>
    <confirmationText>By ticking this box you confirm that the above details are correct and you will adhere to the Awarding Organisation''s code of conduct.</confirmationText>
  </confirmationText>
  <requiresConfirmationCheck>1</requiresConfirmationCheck>
  <allowCloseWithoutSubmit>0</allowCloseWithoutSubmit>
  <useSecureMarker>0</useSecureMarker>
  <annotationVersion>0</annotationVersion>
  <allowPackagingDelivery>1</allowPackagingDelivery>
  <scoreBoundaryData>
    <scoreBoundaries showScoreBoundaryColumn="0">
      <score modifier="lt" value="40" description="Not met" higherBoundarySet="1" />
      <score modifier="lt" value="40" description="Not met" higherBoundarySet="0" />
      <score modifier="gt" value="40" description="Close to met" higherBoundarySet="1" />
      <score modifier="gt" value="40" description="Met" higherBoundarySet="0" />
      <score modifier="gt" value="45" description="Met" higherBoundarySet="1" />
      <score modifier="gt" value="80" description="Exceeded" higherBoundarySet="1" />
    </scoreBoundaries>
  </scoreBoundaryData>
  <showCandidateReportResultColumn>0</showCandidateReportResultColumn>
  <showCandidateReportPercentageColumn>1</showCandidateReportPercentageColumn>
  <certifiedAccessible>0</certifiedAccessible>
  <markingProgressVisible>1</markingProgressVisible>
  <markingProgressMode>1</markingProgressMode>
  <secureClientOperationMode>1</secureClientOperationMode>
  <examDeliverySystemStyleType>delivery</examDeliverySystemStyleType>
  <markingAutoVoidPeriod>365</markingAutoVoidPeriod>
  <showPageRequiresScrollingAlert>0</showPageRequiresScrollingAlert>
  <project ID="3507" />
  <project ID="3519" />
  <project ID="3520" />
  <project ID="3521" />
  <project ID="3522" />
  <project ID="3523" />
  <project ID="3524" />
</assessmentDetails>', resultData = ''<exam passMark="0" passType="1" originalPassMark="0" originalPassType="1" totalMark="50" userMark="35" userPercentage="70.00" passValue="1" originalPassValue="1" totalTimeSpent="0" closeBoundaryType="0" closeBoundaryValue="0" grade="Pass" originalGrade="Pass">
  <gradeBoundaryData>
    <gradeBoundaries passLevelType="1" adjustedGradeBoundaries="0">
      <grade modifier="lt" value="0" description="Fail" />
      <grade modifier="gt" value="0" description="Pass" />
    </gradeBoundaries>
  </gradeBoundaryData>
  <section id="1" passMark="0" passType="1" name="Grammar" totalMark="25" userMark="25" userPercentage="100" passValue="1" totalTimeSpent="0" itemsToMark="0">
    <item id="3519P4085" name="37" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="31" />
    <item id="3519P4093" name="29" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="31" />
    <item id="3519P4052" name="30" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="31" />
    <item id="3519P4102" name="20" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="31" />
    <item id="3519P4081" name="1" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="33" />
    <item id="3519P4118" name="4" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="31" />
    <item id="3519P4091" name="31" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="31" />
    <item id="3519P4071" name="11" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="31" />
    <item id="3519P4120" name="2" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="31" />
    <item id="3519P4096" name="26" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="31" />
    <item id="3519P4055" name="27" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="31" />
    <item id="3519P4092" name="30" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="31" />
    <item id="3519P4048" name="34" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="31" />
    <item id="3519P4097" name="25" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="31" />
    <item id="3519P4121" name="1" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="31" />
    <item id="3519P4083" name="39" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="31" />
    <item id="3519P4106" name="16" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="32" />
    <item id="3519P4063" name="19" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="31" />
    <item id="3519P4110" name="12" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="31" />
    <item id="3519P4084" name="38" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="31" />
    <item id="3519P4082" name="40" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="31" />
    <item id="3519P4088" name="34" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="31" />
    <item id="3519P4090" name="32" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="31" />
    <item id="3519P4100" name="22" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="31" />
    <item id="3519P4099" name="23" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="31" />
  </section>
  <section id="2" passMark="0" passType="1" name="Vocabulary" totalMark="25" userMark="10" userPercentage="40.00" passValue="1" totalTimeSpent="0" itemsToMark="0">
    <item id="3520P3709" name="V2_A1_Synonym_2" totalMark="5" userMark="5" actualUserMark="2.5" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="48" />
    <item id="3520P3701" name="V1_A2_Definition_3" totalMark="5" userMark="5" actualUserMark="2.5" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="37" />
    <item id="3520P3715" name="V2_B1_Synonym_2" totalMark="5" userMark="3" actualUserMark="1.5" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="49" />
    <item id="3520P3704" name="V1_B1_Usage_1" totalMark="5" userMark="4" actualUserMark="2.0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="42" />
    <item id="3520P3720" name="V2_B2_Collocation" totalMark="5" userMark="3" actualUserMark="1.5" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="46" />
    <item id="3520P3604" name="GV_Click Finish page" totalMark="0" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="21" />
  </section>
</exam>'
where ID = 607378;

update WAREHOUSE_ExamSessionTable_shreded
set resultData = '<exam passMark="0" passType="1" originalPassMark="0" originalPassType="1" totalMark="25" userMark="25" userPercentage="100" passValue="1" originalPassValue="1" totalTimeSpent="0" closeBoundaryType="0" closeBoundaryValue="0" grade="Pass" originalGrade="Pass">
  <gradeBoundaryData>
    <gradeBoundaries passLevelType="1" adjustedGradeBoundaries="0">
      <grade modifier="lt" value="0" description="Fail" />
      <grade modifier="gt" value="0" description="Pass" />
    </gradeBoundaries>
  </gradeBoundaryData>
  <section id="1" passMark="0" passType="1" name="Section 1" totalMark="25" userMark="25" userPercentage="100" passValue="1" totalTimeSpent="0" itemsToMark="0">
    <item id="3521P3615" name="Copy of Reading_V2_Task1_1.09.12" totalMark="5" userMark="5" actualUserMark="5" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="39" />
    <item id="3521P3617" name="Copy of Reading_V2_Task_2_1.09.12" totalMark="6" userMark="6" actualUserMark="6" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="34" />
    <item id="3521P3623" name="Copy of Reading_V2_Task_3_1.09.12" totalMark="7" userMark="7" actualUserMark="7" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="45" />
    <item id="3521P3621" name="Reading_V2_Task_4_1.09.12" totalMark="7" userMark="7" actualUserMark="7" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="38" />
    <item id="3521P3581" name="Reading_Submit_Finish" totalMark="0" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="15" />
  </section>
</exam>'
where examsessionid = 607380;