SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;


DECLARE  @subject VARCHAR(30),
		 @query VARCHAR(MAX);

--SELECT	 @subject = CAST(COUNT(CandidateExamVersions.ExternalSessionID) AS NVARCHAR(11)) + ' Exams.'  FROM	 CandidateExamVersions
--WHERE ExamSessionTable.ExamState = 99;
SELECT	 @subject = COUNT(ExternalSessionID)
						  FROM CandidateExamVersions
						  WHERE NOT EXISTS (
									  SELECT CandidateExamVersions.ID
									  FROM CandidateResponses
									  INNER JOIN UniqueResponses ON UniqueResponses.id = CandidateResponses.UniqueResponseID
									  WHERE CandidateResponses.CandidateExamVersionID = CandidateExamVersions.ID
											AND CONVERT(NVARCHAR(MAX), responseData) <> ''''
									  )
								and DateSubmitted > DATEADD(hour, -1, SYSDATETIMEOFFSET())
									--+ ' Exams with their item responses missing.'

SET @query = 

'
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

      SELECT ExternalSessionID
      FROM CandidateExamVersions
      WHERE NOT EXISTS (
                  SELECT CandidateExamVersions.ID
                  FROM CandidateResponses
                  INNER JOIN UniqueResponses ON UniqueResponses.id = CandidateResponses.UniqueResponseID
                  WHERE CandidateResponses.CandidateExamVersionID = CandidateExamVersions.ID
                        AND CONVERT(NVARCHAR(MAX), responseData) <> ''''
                  )
            and DateSubmitted > DATEADD(hour, -1, SYSDATETIMEOFFSET())

            ';
            
if @@ROWCOUNT > 0

EXECUTE	 msdb.dbo.sp_send_dbmail
		 @profile_name = N'WEB1 SMTP Virtual Server',
		 @recipients = N'dave.jones@btl.com; gary.barnes@btl.com',
		 @subject = @subject,
		 @body = N'See attached for the ExamSessionID of an exam with blank responseData',
		 @execute_query_database = N'BRITISHCOUNCIL_SecureMarker',
		 @query = @query,
		 @attach_query_result_as_file = 1,
		 @query_attachment_filename = N'British Council Missing Responses.csv',
		 @query_result_separator = N',',
		 @query_result_no_padding = 1;
		 
		 --@recipients = N'liveservices@btl.com; support@btl.com',
		 --@recipients = N'dave.jones@btl.com',
		 --@recipients = N'liveservices@btl.com; dave.jones@btl.com; gary.barnes@btl.com',
		 
		 
--select * from CandidateExamVersions where ExternalSessionID = '560086'		 
--2014-07-28 14:58:54.563
--2014-11-28 14:58:54.563

--update CandidateExamVersions 
--set DateSubmitted = '2014-07-28 14:58:54.563'
--where ExternalSessionID = '560086'		 