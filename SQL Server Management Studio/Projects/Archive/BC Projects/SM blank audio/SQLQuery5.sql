USE [BRITISHCOUNCIL_SecureAssess]
GO
/****** Object:  StoredProcedure [dbo].[sa_SECUREMARKERINTEGRATION_GetExamSessionsForExport_sp]    Script Date: 06/24/2014 13:55:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Rathin Sundar	
-- Create date: 25/02/2011
-- Description:	Sproc to get ExamVersions based on ExamState Audit Table.
-- Modified:	Tom Gomersall
-- Date:		15/01/2013
-- Description:	Added new columns to the select so that they can be sent to SecureMarker.
-- =============================================
--ALTER PROCEDURE [dbo].[sa_SECUREMARKERINTEGRATION_GetExamSessionsForExport_sp]
	-- Add the parameters for the stored procedure here
	--@MaxExamSessions int = 10
--WITH_ENCRYPTION_REPLACE_ME_FOR_LOCAL--
--AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	--SET ROWCOUNT @MaxExamSessions
	
    -- Insert statements for procedure here
	SELECT 
		[dbo].[WAREHOUSE_ExamSessionTable].[ID]	AS ExamSessionID, 
		KeyCode									AS Keycode,
		CentreName								AS CentreName,
		CentreCode								AS CentreCode,
		[dbo].[WAREHOUSE_CentreTable].Country	AS Country,
		Forename								AS Forename,
		Surname									AS Surname,
		CandidateRef							AS CandidateRef,
		completionDate							AS CompletionDate,
		[resultDataFull],
		(
			SELECT ItemResponseData
			FROM WAREHOUSE_ExamSessionItemResponseTable WITH (NOLOCK)
			WHERE WAREHOUSEExamSessionID = [WAREHOUSE_ExamSessionTable].ID
			FOR XML PATH(''), TYPE
		) AS 'Items',
		[resultData]
	FROM [dbo].[WAREHOUSE_ExamSessionTable] WITH (NOLOCK)
	INNER JOIN [dbo].[WAREHOUSE_ScheduledExamsTable] WITH (NOLOCK)
	ON [dbo].[WAREHOUSE_ScheduledExamsTable].[ID] = [dbo].[WAREHOUSE_ExamSessionTable].[warehouseScheduledExamID]
	INNER JOIN [dbo].[WAREHOUSE_CentreTable] WITH (NOLOCK)
	ON [dbo].[WAREHOUSE_CentreTable].[ID] = [dbo].[WAREHOUSE_ScheduledExamsTable].[WAREHOUSECentreId]
	INNER JOIN [dbo].[WAREHOUSE_UserTable] WITH (NOLOCK)
	ON [dbo].[WAREHOUSE_UserTable].[ID] = [dbo].[WAREHOUSE_ExamSessionTable].[WAREHOUSEUserID]
	WHERE KeyCode = 'Z5ZG2D01'
	FOR XML PATH ('ExamSession'), root ('UnmarkedExamSessions')
	
	--SET ROWCOUNT 0
END
