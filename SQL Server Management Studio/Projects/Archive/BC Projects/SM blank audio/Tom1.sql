SELECT 
            [dbo].[WAREHOUSE_ExamSessionTable].[ID]   AS ExamSessionID, 
            KeyCode                                                     AS Keycode,
            CentreName                                            AS CentreName,
            CentreCode                                            AS CentreCode,
            [dbo].[WAREHOUSE_CentreTable].Country     AS Country,
            Forename                                              AS Forename,
            Surname                                                     AS Surname,
            CandidateRef                                          AS CandidateRef,
            completionDate                                        AS CompletionDate,
            [resultDataFull],
            (
                  SELECT ItemResponseData
                  FROM WAREHOUSE_ExamSessionItemResponseTable WITH (NOLOCK)
                  WHERE WAREHOUSEExamSessionID = [WAREHOUSE_ExamSessionTable].ID
                  FOR XML PATH(''), TYPE
            ) AS 'Items',
            [resultData]
      FROM [dbo].[WAREHOUSE_ExamSessionTable] WITH (NOLOCK)
      INNER JOIN [dbo].[WAREHOUSE_ScheduledExamsTable] WITH (NOLOCK)
      ON [dbo].[WAREHOUSE_ScheduledExamsTable].[ID] = [dbo].[WAREHOUSE_ExamSessionTable].[warehouseScheduledExamID]
      INNER JOIN [dbo].[WAREHOUSE_CentreTable] WITH (NOLOCK)
      ON [dbo].[WAREHOUSE_CentreTable].[ID] = [dbo].[WAREHOUSE_ScheduledExamsTable].[WAREHOUSECentreId]
      INNER JOIN [dbo].[WAREHOUSE_UserTable] WITH (NOLOCK)
      ON [dbo].[WAREHOUSE_UserTable].[ID] = [dbo].[WAREHOUSE_ExamSessionTable].[WAREHOUSEUserID]
     WHERE KeyCode in ('T3GM2E01','HTXZ9701','WZTU2301','Z5ZG2D01','M5KE2T01')
      FOR XML PATH ('ExamSession'), root ('UnmarkedExamSessions')
