
--SELECT WEST.ID
--	,WEST.KeyCode
--	,WUT.Forename
--	,WUT.Surname
--	,WUT.CandidateRef
--	,WCT.CentreName
--	,WSCET.examName
--	,wests.warehousetime
--	,WESTS.ExternalReference
--	,wests.userMark
--	,WESTS.userPercentage
--	,WEST.WarehouseExamState
select
   WCT.CentreName 
	, wests.foreName
	, wests.surName
	, wests.candidateRef
FROM WAREHOUSE_ExamSessionTable AS WEST
INNER JOIN WAREHOUSE_ScheduledExamsTable AS WSCET ON WSCET.ID = WEST.WAREHOUSEScheduledExamID
INNER JOIN WAREHOUSE_UserTable AS WUT ON WUT.ID = WEST.WAREHOUSEUserID
INNER JOIN WAREHOUSE_CentreTable AS WCT ON WCT.ID = WSCET.WAREHOUSECentreID
INNER JOIN WAREHOUSE_ExamSessionTable_Shreded AS WESTS ON WESTS.examSessionId = WEST.ID
WHERE west.ExportToSecureMarker = 1
	AND west.KeyCode collate Latin1_General_CI_AS IN (
		SELECT ScheduledExamRef
		FROM [BritishCouncil_TestPackage]..ScheduledPackageCandidateExams
		WHERE IsCompleted = 1
			AND Score = '0.00'
		)
	AND west.KeyCode collate Latin1_General_CI_AS IN (
		SELECT KeyCode
		FROM [430326-BC-SQL2\SQL2].BRITISHCOUNCIL_SecureMarker.dbo.CandidateExamVersions
		WHERE ExamSessionState = 7
			AND PercentageMarkingComplete = '100'
			AND Mark >= '1.0000000000'
		)
	 AND WESTS.userMark = 0 
	 and WCT.CentreName != 'BC Commercial - Malaysia' 
	 and wct.CentreName != 'British Council - Malaysia'
	
order by WCT.CentreName asc



