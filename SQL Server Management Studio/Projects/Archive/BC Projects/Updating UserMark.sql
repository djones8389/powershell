begin tran

--update AssignedItemMarks
--set AssignedMark = '6.0000000000'
--where AssignedMark in (

SELECT --ExternalItemID--, CEV.TotalMark, I.TotalMark--, *
	AIM.AssignedMark
FROM CandidateExamVersions as CEV

INNER JOIN CandidateResponses as CR
ON CR.CandidateExamVersionID = CEV.ID

INNER JOIN UniqueResponses as UR
ON UR.ID = UniqueResponseID 

inner join UniqueResponseDocuments as URD
ON URD.UniqueResponseID = UR.id 
	
Inner Join Items as I
on I.ID = ur.itemId

inner join AssignedItemMarks as AIM
on AIM.UniqueResponseId = UR.id

where Keycode = '5E8PUZ99'
	and ExternalItemID = '3524P4371'

)


SELECT AIM.AssignedMark, AIM.AssignedMark + 1, I.TotalMark, I.TotalMark + 1
	, AIM.UniqueResponseId, UR.id
FROM CandidateExamVersions as CEV

INNER JOIN CandidateResponses as CR
ON CR.CandidateExamVersionID = CEV.ID

INNER JOIN UniqueResponses as UR
ON UR.ID = UniqueResponseID 

inner join UniqueResponseDocuments as URD
ON URD.UniqueResponseID = UR.id 
	
Inner Join Items as I
on I.ID = ur.itemId

inner join AssignedItemMarks as AIM
on AIM.UniqueResponseId = UR.id

where Keycode = '5E8PUZ99'
	and ExternalItemID = '3524P4371'
	
rollback


--Update AssignedItemMarks 
--set AssignedMark = '6.0000000000'
--where GroupMarkId = 702469

--select * from AssignedItemMarks where UniqueResponseId = 599061