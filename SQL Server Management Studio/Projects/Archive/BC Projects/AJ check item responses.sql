drop table #tempTable

declare @Keycode nvarchar(10) = 'u3le6u01'

declare @ID int = (
select ID
	from ExamSessionTable
		where KeyCode = @Keycode
	)


create table #tempTable

(
      ID2 int,
      TotalNumberOfQuestions int
)

insert into #tempTable

      select
                  ID,
                  StructureXML.value('count(/assessmentDetails/assessment/section/item)[1]', 'int')
      from ExamSessionTable
     where KeyCode = @Keycode
      
      
	select 	EST.ID			
		   ,COUNT (ExamSessionID) as NumberOfQuestionsAnswered
		   ,  TT.TotalNumberOfQuestions      

		from ExamSessionItemResponseTable as ESIRT

		inner join ExamSessionTable as EST
		on EST.ID = ESIRT.ExamSessionID
		
		inner join #tempTable as TT
        on TT.ID2 = ESIRT.ExamSessionID
		
		where KeyCode = @Keycode
	group by EST.ID,ExamSessionID, TT.TotalNumberOfQuestions 
	
drop table #tempTable