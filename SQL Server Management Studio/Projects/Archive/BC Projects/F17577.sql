select * from WAREHOUSE_ExamSessionTable where KeyCode = 'TE5XDQ01'

select * from WAREHOUSE_ExamSessionItemResponseTable where WAREHOUSEExamSessionID = 536658


declare @keycode nvarchar(12) =  'TE5XDQ01'

declare @ID int = (
select ID
	from WAREHOUSE_ExamSessionTable
		where KeyCode = @Keycode
	)


create table #ItemResponseTempTable

(
      ID2 int,
      TotalNumberOfQuestions int
)

insert into #ItemResponseTempTable

      select
                  ID,
                  StructureXML.value('count(/assessmentDetails/assessment/section/item)[1]', 'int')
      from WAREHOUSE_ExamSessionTable
     where KeyCode = @keycode
      
      
	select 				
		   COUNT (ExamSessionID) as NumberOfQuestionsAnswered
		   ,  TT.TotalNumberOfQuestions      

		from WAREHOUSE_ExamSessionItemResponseTable as ESIRT

		inner join WAREHOUSE_ExamSessionTable as EST
		on EST.ID = ESIRT.WAREHOUSEExamSessionID
		
		inner join #ItemResponseTempTable as TT
        	on TT.ID2 = ESIRT.WAREHOUSEExamSessionID
		
		   where KeyCode = @keycode
		
	group by ExamSessionID, TT.TotalNumberOfQuestions 
	
	drop table #ItemResponseTempTable
	--drop procedure BTL_DaveJ_HowManyItemResponses_Live
