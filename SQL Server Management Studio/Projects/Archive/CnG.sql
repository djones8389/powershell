/****** Script for SelectTopNRows command from SSMS  ******/
--SELECT *
--  FROM [UAT_BCGuilds_SecureAssess_11_7_223_0_IP].[dbo].AssignedUserRolesTable
--  WHERE UserID = 1

SELECT *
  FROM [UAT_BCGuilds_SecureAssess_11_7_223_0_IP].[dbo].RolePermissionsTable
  WHERE RoleID IN(1,2)

SELECT * 
FROM UAT_EAL_SecureAssess.[dbo].RolePermissionsTable
  WHERE RoleID IN(1,2)

  SELECT * FROM UAT_EAL_SecureAssess..CentreTable
  WHERE UserID = 1

  --12 = board administration


  --GrantableRolesTable
  --WAREHOUSE_ExamStateLookupTable



  --SET IDENTITY_INSERT [AssignedUserRolesTable] ON 
  --INSERT [AssignedUserRolesTable]
  --VALUES(1,2,1)
  --SET IDENTITY_INSERT [AssignedUserRolesTable] OFF

  --INSERT [CohortUsersTable]
  --VALUES(1,1)



--  --exec sa_CENTREUSERSMANAGEMENT_modifyUser_sp @userDetailsXml=N'<user><id>395889</id><forename>EXPIRED</forename><surname>1</surname><middleName>1</middleName><dob>31/10/1982</dob><gender>M</gender><addressLine1>W0048CW_KRAVAL</addressLine1><addressLine2 /><town>Sidcup</town><county>196</county><country>208</country><postCode>DA14 4RH</postCode><telephone>123456789</telephone><email>test@eal.org.uk</email><ethnicOrigin>0</ethnicOrigin><accountExpiryDate>31/12/2025</accountExpiryDate><username autoGenerate="0">W5029EW_EXP1</username><candidateRef autoGenerate="0" /><centreRoles><centre id="1" ref="-1"><added><i>12</i></added><removed /></centre></centreRoles><centres><added /><removed /></centres><qualifications><added /><removed /></qualifications><retired>1</retired><uln /></user>',@isCandidate=0

--  exec sa_CENTREUSERSMANAGEMENT_modifyUser_sp @userDetailsXml=N'<user>
--    <id>1</id>
--    <forename>Forename</forename>
--    <surname>Surname</surname>
--    <middleName>MiddleName</middleName>
--    <dob>31/10/1982</dob>
--    <gender>M</gender>
--    <addressLine1>BTL Group Ltd</addressLine1>
--    <addressLine2/>
--    <town>Shipley</town>
--    <county>7</county>
--    <country>0</country>
--    <postCode>BD17 7DB</postCode>
--    <telephone>01274 203250</telephone>
--    <email>liveservices@btl.com</email>
--    <ethnicOrigin>0</ethnicOrigin>
--    <accountExpiryDate>31/12/2047</accountExpiryDate>
--    <username autoGenerate="0">superuser</username>
--    <candidateRef autoGenerate="0"/>
--    <centreRoles>
--        <centre id="1" ref="-1">
--            <added>
--                <i>1</i>
--            </added>
--            <removed/>
--        </centre>
--    </centreRoles>
--    <centres>
--        <added/>
--        <removed/>
--    </centres>
--    <qualifications>
--        <added/>
--        <removed/>
--    </qualifications>
--    <retired>0</retired>
--    <uln/>
--</user>',@isCandidate=0

  SELECT * FROM dbo.UserTable WHERE id = 1