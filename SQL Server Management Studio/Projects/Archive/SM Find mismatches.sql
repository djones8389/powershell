select top 100 cev.ExternalSessionID, I.ExternalItemID, ExternalItemName, AssignedMark
from CandidateExamVersions as CEV

inner join CandidateResponses as CR
on CR.CandidateExamVersionID = CEV.ID

inner join UniqueResponses as UR
on UR.id = CR.UniqueResponseID

Inner join AssignedItemMarks as AIM
on AIM.UniqueResponseId = UR.id

inner join Items as I
on I.ID = UR.itemId

where cev.ExternalSessionID = 1564278
	--and AssignedMark > 0
	
EXCEPT

select top 50
	WAREHOUSE_ExamSessionTable.ID
   -- , WAREHOUSE_ExamSessionItemResponseTable.MarkerResponseData.value('(entries/entry/assignedMark)[1]', 'nvarchar(max)') as Mark
	, WAREHOUSE_ExamSessionTable_ShrededItems.ItemRef
--	, resultData
	, WAREHOUSE_ExamSessionTable_ShrededItems.ItemName
	--, WAREHOUSE_ExamSessionTable_ShrededItems.userMark
	--, WAREHOUSE_ExamSessionTable_ShrededItems.TotalMark
	, WAREHOUSE_ExamSessionTable_ShrededItems.userMark * WAREHOUSE_ExamSessionTable_ShrededItems.TotalMark as 'SAMark'
from AAT_SecureAssess_LIVE_28_01_2015..WAREHOUSE_ExamSessionTable (NOLOCK)

left join AAT_SecureAssess_LIVE_28_01_2015..WAREHOUSE_ExamSessionItemResponseTable (NOLOCK)
on WAREHOUSE_ExamSessionItemResponseTable.WAREHOUSEExamSessionID = WAREHOUSE_ExamSessionTable.ID

left join AAT_SecureAssess_LIVE_28_01_2015..WAREHOUSE_ExamSessionTable_ShrededItems  (NOLOCK)
on WAREHOUSE_ExamSessionTable_ShrededItems.examSessionId = WAREHOUSE_ExamSessionTable.id
		and WAREHOUSE_ExamSessionTable_ShrededItems.ItemRef = WAREHOUSE_ExamSessionItemResponseTable.ItemID

where WAREHOUSE_ExamSessionTable.ExportToSecureMarker = 1
	and WAREHOUSE_ExamSessionTable.ID = 1564278
	--and WAREHOUSE_ExamSessionTable_ShrededItems.userMark * WAREHOUSE_ExamSessionTable_ShrededItems.TotalMark > 0
	--order by AAT_SecureAssess_LIVE_28_01_2015..WAREHOUSE_ExamSessionTable.ID DESC;	