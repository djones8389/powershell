USE [NCFE_SecureAssess_LIVE]
GO

/****** Object:  View [dbo].[sa_CandidateExamAudit_View]    Script Date: 07/24/2014 15:04:28 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


























ALTER VIEW [dbo].[sa_CandidateExamAudit_View]    
AS    
    
SELECT     WAREHOUSE_ExamSessionTable.ID AS examSessionId, WAREHOUSE_ExamSessionTable.StructureXML, CONVERT(nvarchar(MAX),     
                      CAST(WAREHOUSE_ExamSessionTable.StructureXML AS XML).query('/assessmentDetails/assessmentName/text()')) AS examVersionName, CONVERT(nvarchar(MAX),
                      CAST(WAREHOUSE_ExamSessionTable.StructureXML AS XML).query('/assessmentDetails/externalReference/text()')) AS examVersionRef,     
                      WAREHOUSE_ScheduledExamsTable.examVersionId, CONVERT(nvarchar(MAX),     
                      CAST(WAREHOUSE_ExamSessionTable.StructureXML AS XML).query('/assessmentDetails/assessmentGroupName/text()')) AS examName, CONVERT(nvarchar(MAX),     
                      CAST(WAREHOUSE_ExamSessionTable.StructureXML AS XML).query('/assessmentDetails/assessmentGroupReference/text()')) AS examRef,
                      WAREHOUSE_ScheduledExamsTable.language,     
                      WAREHOUSE_ScheduledExamsTable.qualificationId, WAREHOUSE_ScheduledExamsTable.qualificationName, CONVERT(nvarchar(MAX),     
                      CAST(WAREHOUSE_ExamSessionTable.StructureXML AS XML).query('/assessmentDetails/qualificationReference/text()')) AS qualificationRef,     
                     
                      WAREHOUSE_ExamSessionTable.resultData, CONVERT(datetime, WAREHOUSE_ExamSessionTable.completionDate, 113) AS submittedDate,     
                      WAREHOUSE_ScheduledExamsTable.OriginatorID, WAREHOUSE_CentreTable.CentreName, WAREHOUSE_CentreTable.CentreCode,     
                      WAREHOUSE_UserTable.Forename, WAREHOUSE_UserTable.DOB AS dateOfBirth, WAREHOUSE_UserTable.Gender,     
                      WAREHOUSE_UserTable.CandidateRef, WAREHOUSE_UserTable.Surname,     
                      CAST(CAST(CAST(WAREHOUSE_ExamSessionTable.StructureXML AS XML).query('//assessmentDetails/scheduledDuration/value/text()[1]') AS nvarchar(100)) AS decimal)     
                      AS scheduledDurationValue, WAREHOUSE_ExamSessionTable.PreviousExamState,     
                      WAREHOUSE_ExamSessionTable.ExamStateChangeAuditXml.query('//stateChange/newStateID[text()=10][1]/../information/stateChangeInformation')     
                      AS examStateInformation, CAST(CAST(WAREHOUSE_ExamSessionTable.resultData.query('data(/exam/@userPercentage)') AS nvarchar(100)) AS float)     
                      AS examResult, CAST(WAREHOUSE_ExamSessionTable.resultData.query('data(/exam/@passValue)') AS nvarchar(MAX)) AS passValue,     
                      CAST(WAREHOUSE_ExamSessionTable.resultData.query('data(/exam/@closeValue)') AS nvarchar(MAX)) AS closeValue,     
                      WAREHOUSE_ScheduledExamsTable.ExternalReference,     
                      CAST(CAST(CAST(WAREHOUSE_ExamSessionTable.StructureXML AS XML).query('//assessmentDetails/examType/text()[1]') AS NVARCHAR(MAX)) AS int) AS examType,     
                      WAREHOUSE_ExamSessionTable.warehouseTime, WAREHOUSE_ExamSessionTable.CQN,     
                      CAST(CAST(CAST(WAREHOUSE_ExamSessionTable.StructureXML AS XML).query('//duration/text()') AS NVARCHAR(MAX)) AS int) AS actualDuration,     
                      WAREHOUSE_ExamSessionTable.appeal, WAREHOUSE_ExamSessionTable.reMarkStatus, WAREHOUSE_ScheduledExamsTable.qualificationLevel,     
                      WAREHOUSE_CentreTable.CentreID, WAREHOUSE_UserTable.ULN,WAREHOUSE_ExamSessionTable.AllowPackageDelivery,
                      WAREHOUSE_ExamSessionTable.KeyCode,    
                      WAREHOUSE_ExamSessionTable.ExportToSecureMarker, 
                      WAREHOUSE_ExamSessionTable.ContainsBTLOffice as ContainsBTLOffice,
                      WAREHOUSE_ExamSessionTable.ExportedToIntegration,
                      WAREHOUSE_ExamSessionTable.WarehouseExamState,
                      WAREHOUSE_ExamSessionTable.TargetedForVoid,
                      WAREHOUSE_ExamSessionTable.EnableOverrideMarking,
                      WAREHOUSE_CentreTable.AddressLine1,
                      WAREHOUSE_CentreTable.AddressLine2,
                      WAREHOUSE_CentreTable.County,
                      WAREHOUSE_CentreTable.Country,
                      WAREHOUSE_CentreTable.Town,
                      WAREHOUSE_CentreTable.Postcode,
                      CAST(WAREHOUSE_ExamSessionTable.StructureXML AS XML).query('/assessmentDetails/scoreBoundaryData/scoreBoundaries') AS ScoreBoundaryData,
						WAREHOUSE_ExamSessionTable.itemMarksUploaded,
                      CAST(WAREHOUSE_ExamSessionTable.resultData AS XML).query('data(/exam/@passMark)').value('.', 'DECIMAL(6, 3)') AS passMark,
                      CAST(WAREHOUSE_ExamSessionTable.resultData AS XML).query('data(/exam/@totalMark)').value('.', 'DECIMAL(6, 3)') AS totalMark,
                      CAST(WAREHOUSE_ExamSessionTable.resultData AS XML).query('data(/exam/@passType)').value('.', 'INT') AS passType,
                      CAST(WAREHOUSE_ExamSessionTable.ExamStateChangeAuditXml AS XML).query('//stateChange[newStateID=10][1]/information/stateChangeInformation/reason/text()').value('.', 'INT') AS voidJustificationLookupTableId,
                      CAST(WAREHOUSE_ExamSessionTable.StructureXML AS XML).query('data(//automaticVerification)').value('.', 'BIT') AS automaticVerification,
                      CASE CAST(WAREHOUSE_ExamSessionTable.ExamStateChangeAuditXml AS XML).query('data(//stateChange[newStateID=12]/information/stateChangeInformation/type/text())').value('.', 'NVARCHAR(MAX)') WHEN 'release' THEN 1 ELSE 0 END AS resultSampled,
                      CAST(WAREHOUSE_ExamSessionTable.ExamStateChangeAuditXml AS XML).query('data(//stateChange[newStateID=6][1]/changeDate/text())').value('.', 'DATETIME') AS [started],
                      CAST(WAREHOUSE_ExamSessionTable.ExamStateChangeAuditXml AS XML).query('data(//stateChange[newStateID=9 or newStateID=10][last()]/changeDate/text())').value('.', 'DATETIME') AS submitted,
                      CAST(WAREHOUSE_ExamSessionTable.StructureXML AS XML).query('data(/assessmentDetails/validFromDate)').value('.', 'DATETIME') AS validFromDate,
                      CAST(WAREHOUSE_ExamSessionTable.StructureXML AS XML).query('data(/assessmentDetails/expiryDate)').value('.', 'DATETIME') AS expiryDate,
                      CAST(WAREHOUSE_ExamSessionTable.resultData AS XML).query('data(/exam/@totalTimeSpent)').value('.', 'INT') AS totalTimeSpent,
                      CAST(WAREHOUSE_ExamSessionTable.StructureXML AS XML).query('data(/assessmentDetails/defaultDuration)').value('.', 'INT') AS defaultDuration,
                      CAST(WAREHOUSE_ExamSessionTable.StructureXML AS XML).query('data(/assessmentDetails/scheduledDuration/reason)').value('.', 'VARCHAR(MAX)') AS scheduledDurationReason,
                      WAREHOUSEScheduledExamID,
                      WAREHOUSEUserID,
                      Convert(xml,Convert(nvarchar(max),'<itemData>' + Convert(nvarchar(max),[WAREHOUSE_ExamSessionTable].[resultDataFull].query('//item')) + '</itemData>')) AS itemDataFull,
                      UserId,
                      ExamID,
                      WAREHOUSE_ExamSessionTable.TakenThroughLocalScan AS TakenThroughLocalScan,
					  WAREHOUSE_ExamSessionTable.LocalScanDownloadDate AS LocalScanDownloadDate,
					  WAREHOUSE_ExamSessionTable.LocalScanUploadDate AS LocalScanUploadDate,
		              WAREHOUSE_ExamSessionTable.LocalScanNumPages AS LocalScanNumPages
FROM         WAREHOUSE_ScheduledExamsTable INNER JOIN    
                      WAREHOUSE_ExamSessionTable ON     
                      WAREHOUSE_ExamSessionTable.WAREHOUSEScheduledExamID = WAREHOUSE_ScheduledExamsTable.ID INNER JOIN    
                      WAREHOUSE_CentreTable ON WAREHOUSE_CentreTable.ID = WAREHOUSE_ScheduledExamsTable.WAREHOUSECentreID INNER JOIN    
                      WAREHOUSE_UserTable ON WAREHOUSE_ExamSessionTable.WAREHOUSEUserID = WAREHOUSE_UserTable.ID    
WHERE     (WAREHOUSE_ScheduledExamsTable.IsExternal <> 1)

GO


