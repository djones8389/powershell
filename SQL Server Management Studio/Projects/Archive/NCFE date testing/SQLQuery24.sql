USE [NCFE_SecureAssess_LIVE]
GO
/****** Object:  StoredProcedure [dbo].[sa_SHARED_warehouseExam_sp]    Script Date: 07/24/2014 15:38:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================      
-- Authors:  David Naylor / Dave Dixon      
-- Create date: 17/07/2007 / 30/08/2007      
-- Description: A stored procedure to warehouse an exam using a passed in instance id      
-- this is an internal version that can be changed without affecting the publically visible SP      
-- Updated to send all uploaded documents from ExamSessionDocumentTable to WAREHOUSE_ExamSessionDocumentTable -- AWAIS    
-- Modified 08/02/2011 Umair Added ExportToSecureMarker     
-- Modified 18/04/2011 Umair Added ContainsBTLOffice in Warehouse_ExamSessionTable and WAREHOUSE_ExamSessionTable_Shreded    
-- Modified 20/04/2011 Ali,	updated the appeal with autoviewexam from structurexml of ExamSessionTable
-- Modified 09/05/2011 JC, added the marking ignored item flag when updating the WAREHOUSE_ExamSessionItemResponseTable table from its live equivalent
-- Modified 02/06/2011 Awais, added [WarehouseExamState] in Warehouse_ExamSessionTable and WAREHOUSE_ExamSessionTable_Shreded
-- Modified 24/11/2011 JC, added shredding of structure xml item data into WAREHOUSE_ExamSessionAvailableItemsTable - ItemID, ItemVersion, ItemXML
-- Modified 24/11/2011 Umair, modified resultDatafull column for LastInUseDate
-- Modified 20/12/2011 Tom Gomersall Added username to the usertable inserts.
-- Modified 20/02/2012 TH Added language to the WAREHOUSE_ExamSessionShreddedTable for use in ExamAudit and ReportingService filtering
-- Modified 22/02/2012 Umair Added TargetedForVoid column in Warehouse_ExamSessionTable and WAREHOUSE_ExamSessionTable_Shreded
-- Modified 03/07/2012 Modifications so it still works with typed data.
-- Modified 21/08/2012 Added explicit field list to the insert.
-- Modified 14/03/2012 Added new shreded columns to shreded items table.
-- Modified:	Tom Gomersall
-- Date:		25/04/2013
-- Description:	Added new itemType column to the insertion to the WAREHOUSE_examSessionTable_shrededItems table.
-- Modified:	Tom Gomersall
-- Date:		26/04/2013
-- Description:	Added new FriendItems column to the insertion to the WAREHOUSE_examSessionTable_shrededItems table.
-- Modified:	Danny Jimmison
-- Date:		16/05/2013
-- Description:	(Line 1098) Added a section to shred the mark data into the WAREHOUSE_examSessionTable_shreddedItems_Mark table. 
-- Modified 10/06/2013 JC, Shredded the ScoreBoundary xml data from the structure xml into the WAREHOUSE_ExamSessionTable
-- Modified:	Tom Gomersall
-- Date:		27/11/2012 
-- Description:	Added ItemMarksUploaded column to inserts and added it to the checks for whether to set exams to be awaiting export to SM.
-- Modifed:		Robert Barnes
-- Date:		27/09/2013
-- Description:	Added new columns from view so that warehouse is updated
-- =============================================      
ALTER PROCEDURE [dbo].[sa_SHARED_warehouseExam_sp]      
 @examInstanceId  int,      
 @OriginatorID  int,      
 @returnString  nvarchar(max) output      
--WITH_ENCRYPTION_REPLACE_ME_FOR_LOCAL--      
AS      
       
BEGIN      
 -- SET NOCOUNT ON added to prevent extra result sets from      
 -- interfering with SELECT statements.      
      
      
SET NOCOUNT ON;      
          
          
DECLARE @errorReturnString nvarchar(max)      
DECLARE @errorNum  nvarchar(100)      
DECLARE @errorMess nvarchar(max)      
DECLARE @ReturnVal nvarchar(max)      
 BEGIN TRY      
        
  -- get all the required variables that will be searched on for the affected exam instance and scheduled exam and initialise them      
  DECLARE @mySAScheduledExamId int, @mySACentreId int, @mySACreatedById int, @mySAUserId int, @myKeyCode nvarchar(12)      
  Declare @AvailableForCentreReview bit -- Centre Direct     
  
  Declare @ResultDataFull xml
  Declare @TodaysDate NVarchar(50)
       
  SELECT       
  @mySAScheduledExamId = ExamSessionTable.ScheduledExamID,      
  @mySACentreId = ScheduledExamsTable.CentreID,      
  @mySACreatedById = ScheduledExamsTable.CreatedBy,      
  @mySAUserId = ExamSessionTable.UserID,      
  @myKeyCode = ExamSessionTable.KeyCode,    
  @AvailableForCentreReview = scheduledForCentreDirect     
  FROM ExamSessionTable      
  INNER JOIN ScheduledExamsTable ON      
   ScheduledExamsTable.ID = ExamSessionTable.ScheduledExamID      
  WHERE ExamSessionTable.ID = @examInstanceId      
      
  IF (EXISTS(SELECT [KeyCode] FROM Warehouse_ExamSessionTable WHERE KeyCode=@myKeyCode AND ExamSessionID=@examInstanceId))      
   BEGIN      
    -- exam has already been archived, report the error      
    SET @returnString = '<result errorCode="1"><return>The exam session already exists in the archive</return></result>'      
    RETURN      
   END      
      
  --Get the secure Assess version number for the current centre into a var      
  DECLARE @mySACentreVersion int      
  SELECT @mySACentreVersion = version from CentreTable      
  WHERE ID = @mySACentreId      
      
  --Get the secure Assess version number for the createdBy User into a var      
  DECLARE @mySACreatedByIdVersion int      
  SELECT @mySACreatedByIdVersion = version from Usertable      
  WHERE ID = @mySACreatedById      
      
  --Get the secure Assess version number for the User taking the exam into a var      
  DECLARE @mySAUserIdVersion int      
  SELECT @mySAUserIdVersion = version from Usertable      
  WHERE ID = @mySAUserId      
      
  /*############################################################################################      
  SET UP A LINK TO THE WAREHOUSE SERVER, THIS WILL BE IMPLEMENTED LATER      
  SEE PAGE 300 OF SERVER BIBLE      
  ##############################################################################################*/      
      
  /*Check whther the schuled exam already exists in the warehouse tables      
  If it doesnt we must create a record for and select the wareHouseScheduledExam ID      
  If it already exists, we just need to select the wareHouseScheduledExam ID*/      
        
  IF NOT EXISTS       
  (SELECT [ID] FROM WAREHOUSE_ScheduledExamsTable      
  WHERE ScheduledExamID = @mySAScheduledExamId      
  AND OriginatorID = @OriginatorID)      
  BEGIN      
   -- there is no record of the schduled exam data for the current exam instance in the warehouse      
   -- this must be created here.      
      
   /*      
   Now check the centre that owns the scheduled exam exists in the warehouse      
   If it does       
    check the version numbers between the warehouse and secure assess.      
     If they differ       
      insert a new centre record into the warehouse and select the new id,       
     else      
      select the warehouse version of the centre id      
   */      
   -- declare a var to flag if the centre needs inserting      
   DECLARE @myCentreInsertRequiredFlag int      
   SET @myCentreInsertRequiredFlag = 0      
      
   IF EXISTS      
   (SELECT [ID] FROM WAREHOUSE_CentreTable      
   WHERE CentreID = @mySACentreId      
   AND OriginatorID = @OriginatorID)      
   BEGIN      
    -- get the centre version number from the warehouse      
    DECLARE @myWarehouseCentreVersion int      
    SELECT TOP(1) @myWarehouseCentreVersion = version      
    FROM WAREHOUSE_CentreTable      
    WHERE CentreID = @mySACentreId      
    AND OriginatorID = @OriginatorID      
      
    -- check the version numbers match      
    IF @myWarehouseCentreVersion < @mySACentreVersion      
     BEGIN      
      -- the centre in the warehouse is out of sync with SA      
      -- insert a record for this centre      
      SET @myCentreInsertRequiredFlag = 1      
     END      
   END      
   ELSE      
   BEGIN      
    -- centre does not exist so we must make it      
    SET @myCentreInsertRequiredFlag = 1      
   END      
         
   IF @myCentreInsertRequiredFlag = 1      
    BEGIN      
     --insert a new centre record      
     INSERT [dbo].[WAREHOUSE_CentreTable]      
         ([CentreID]      
         ,[CentreName]      
         ,[CentreCode]      
         ,[AddressLine1]      
         ,[AddressLine2]      
         ,[County]      
         ,[Country]      
         ,[PostCode]      
         ,[version]      
         ,[OriginatorID]      
         ,[Town])      
     SELECT [dbo].[CentreTable].[ID]      
        ,[dbo].[CentreTable].[CentreName]      
        ,[dbo].[CentreTable].[CentreCode]      
        ,[dbo].[CentreTable].[AddressLine1]      
        ,[dbo].[CentreTable].[AddressLine2]      
        ,[dbo].[CountyLookupTable].[County]      
        ,[dbo].[CountryLookupTable].[Country]      
        ,[dbo].[CentreTable].[PostCode]      
        ,[dbo].[CentreTable].[version]      
        ,@OriginatorID      
        ,[dbo].[CentreTable].[Town]       
     FROM [dbo].[CentreTable],[dbo].[CountryLookupTable],[dbo].[CountyLookupTable]      
     WHERE [dbo].[CentreTable].[ID] = @mySACentreId      
     AND [dbo].[CentreTable].[County] = [dbo].[CountyLookupTable].[ID]      
     AND [dbo].[CentreTable].[Country] = [dbo].[CountryLookupTable].[ID]       
    END      
         
   -- get the latest id for the affected centre from the warehouse table      
   DECLARE @myWarehouseCentreId  int      
   SELECT @myWarehouseCentreId = [ID]       
   FROM WAREHOUSE_CentreTable      
   WHERE CentreID = @mySACentreId      
   AND OriginatorID = @OriginatorID      
   AND version = @mySACentreVersion      
      
   /*      
   Now check the user that is in the createdBy record of the scheduled exams table      
   exists in the warehouse user table      
   If they does       
    check the version numbers between the warehouse and secure assess.      
     If they differ       
      insert a new user record into the warehouse and select the new id,       
     else      
      select the warehouse version of the user id      
   */      
      
   DECLARE @myCreatedByUserInsertRequiredFlag int      
   SET @myCreatedByUserInsertRequiredFlag = 0      
      
   IF EXISTS      
   (SELECT [ID] FROM WAREHOUSE_UserTable      
   WHERE UserId = @mySACreatedById      
   AND OriginatorID = @OriginatorID)      
   BEGIN      
    -- get the user version number from the warehouse      
    DECLARE @myWarehouseCreatedByUserVersion int      
    SELECT TOP(1) @myWarehouseCreatedByUserVersion = version      
    FROM WAREHOUSE_UserTable      
    WHERE UserId = @mySACreatedById      
    AND OriginatorID = @OriginatorID      
      
    -- check the version numbers match      
    IF @myWarehouseCreatedByUserVersion < @mySACreatedByIdVersion      
     BEGIN      
      -- the user in the warehouse is out of sync with SA      
      -- insert a record for this user       
      SET @myCreatedByUserInsertRequiredFlag = 1      
     END      
   END      
   ELSE      
   BEGIN      
    -- user does not exist so we must make it      
    SET @myCreatedByUserInsertRequiredFlag = 1      
   END      
         
   IF @myCreatedByUserInsertRequiredFlag = 1      
    BEGIN      
     --insert a new user record      
     INSERT [dbo].[WAREHOUSE_UserTable]      
         ([UserId]      
         ,[CandidateRef]      
         ,[Forename]      
         ,[Surname]      
         ,[Middlename]      
         ,[DOB]      
         ,[Gender]      
         ,[SpecialRequirements]      
         ,[AddressLine1]      
         ,[AddressLine2]      
         ,[Town]      
         ,[County]      
         ,[Country]      
         ,[PostCode]      
         ,[Telephone]      
         ,[Email]      
         ,[EthnicOrigin]      
         ,[AccountCreationDate]      
         ,[AccountExpiryDate]      
         ,[ExtraInfo]      
         ,[version]      
         ,[OriginatorID]    
         ,[ULN]
         ,[Username])      
     SELECT      
       [UserTable].[ID]      
       ,[UserTable].[CandidateRef]      
       ,[UserTable].[Forename]      
       ,[UserTable].[Surname]      
       ,[UserTable].[Middlename]      
       ,[UserTable].[DOB]      
       ,[UserTable].[Gender]      
       ,[UserTable].[SpecialRequirements]      
       ,[UserTable].[AddressLine1]      
       ,[UserTable].[AddressLine2]      
       ,[UserTable].[Town]      
       ,[CountyLookupTable].[County]      
       ,[CountryLookupTable].[Country]      
       ,[UserTable].[PostCode]      
       ,[UserTable].[Telephone]      
       ,[UserTable].[Email]      
       ,[EnthnicOriginLookupTable].[Name]      
       ,[UserTable].[AccountCreationDate]      
       ,[UserTable].[AccountExpiryDate]      
       ,[UserTable].[ExtraInfo]      
       ,[UserTable].[version]      
       ,@OriginatorID     
       ,[UserTable].[ULN] 
       ,[UserTable].[Username]   
     FROM [dbo].[UserTable],[dbo].[CountyLookupTable],[dbo].[CountryLookupTable],[dbo].[EnthnicOriginLookupTable]      
     WHERE [UserTable].[ID] = @mySACreatedById      
     AND [dbo].[UserTable].[County] = [dbo].[CountyLookupTable].[ID]      
     AND [dbo].[UserTable].[Country] = [dbo].[CountryLookupTable].[ID]      
     AND [dbo].[EnthnicOriginLookupTable].[ID] = [UserTable].[EthnicOriginID]      
    END      
         
   -- get the latest id for the affected createdByUser from the warehouse table      
   DECLARE @myWarehouseCreatedById  int      
   SELECT @myWarehouseCreatedById = [ID]       
   FROM [WAREHOUSE_UserTable]      
   WHERE [UserId] = @mySACreatedById      
   AND OriginatorID = @OriginatorID      
   AND version = @mySACreatedByIdVersion      
         
      
   -- we are now in a position to insert the scheduled exam into the warehouse table      
   INSERT [dbo].[WAREHOUSE_ScheduledExamsTable]      
       ([ScheduledExamID]      
       ,[ExamID]      
       ,[WAREHOUSECentreID]      
       ,[WAREHOUSECreatedBy]      
       ,[CreatedDateTime]      
       ,[ScheduledStartDateTime]      
       ,[ScheduledEndDateTime]      
       ,[ActiveStartTime]      
       ,[ActiveEndTime]      
       ,[qualificationId]      
       ,[examName]      
       ,[groupState]      
       ,[invigilated]      
       ,[humanMarked]      
       ,[AdvanceContentDownloadTimespanInHours]      
       ,[OriginatorID]      
       ,[qualificationName]      
       ,[qualificationRef]      
       ,[examVersionId]      
       ,[ExternalReference]      
       ,[language]      
       ,[qualificationLevel]  
       ,[purchaseOrder]    
       )      
   SELECT [dbo].[ScheduledExamsTable].[ID]      
       ,[dbo].[ScheduledExamsTable].[ExamID]      
       ,@myWarehouseCentreId      
       ,@myWarehouseCreatedById      
       ,[dbo].[ScheduledExamsTable].[CreatedDateTime]      
       ,[dbo].[ScheduledExamsTable].[ScheduledStartDateTime]      
       ,[dbo].[ScheduledExamsTable].[ScheduledEndDateTime]      
       ,[dbo].[ScheduledExamsTable].[ActiveStartTime]      
       ,[dbo].[ScheduledExamsTable].[ActiveEndTime]      
       ,[dbo].[ScheduledExamsTable].[qualificationId]      
       ,[dbo].[ScheduledExamsTable].[examName]      
       ,[dbo].[ScheduledExamsTable].[groupState]      
       ,[dbo].[ScheduledExamsTable].[invigilated]      
       ,[dbo].[ScheduledExamsTable].[humanMarked]      
       ,[dbo].[ScheduledExamsTable].[AdvanceContentDownloadTimespanInHours]      
       ,@OriginatorID      
       ,[dbo].[IB3QualificationLookup].[QualificationName]      
       ,[dbo].[IB3QualificationLookup].[QualificationRef]      
       ,[dbo].[ScheduledExamsTable].[examVersionId]      
       ,Convert(varchar(200),CAST([ExamSessionTable].StructureXml AS XML).query('/assessmentDetails/externalReference/text()')) -- Fix by DD - 15/01/09      
       ,[dbo].[ScheduledExamsTable].[language]      
       ,[dbo].[IB3QualificationLookup].[QualificationLevel]  
       ,[dbo].[ScheduledExamsTable].[purchaseOrder]     
   FROM [dbo].[ScheduledExamsTable]      
    INNER JOIN [dbo].[IB3QualificationLookup] ON      
     [dbo].[IB3QualificationLookup].[ID] = [dbo].[ScheduledExamsTable].[qualificationId]      
    INNER JOIN [dbo].[ExamSessionTable] ON [dbo].[ScheduledExamsTable].[ID] = [dbo].[ExamSessionTable].[ScheduledExamID]      
   WHERE [dbo].[ScheduledExamsTable].[ID] = @mySAScheduledExamId      
      
  END      
      
  -- get the warehouse scheduled exam id version into a var      
  DECLARE @myWarehouseScheduledExamId  int      
  SELECT @myWarehouseScheduledExamId = [ID]       
  FROM WAREHOUSE_ScheduledExamsTable    WHERE ScheduledExamID = @mySAScheduledExamId      
  AND OriginatorID = @OriginatorID      
      
  /*      
  The Scheduled Exam that the current exam session belongs to is now in the warehouse,      
  We must now insert the exam session and item response data into the warehouse.      
        
  Firstly check the user that has taken the exam is in the warehouse user table      
       
  If they do      
   check the version numbers between the warehouse and secure assess.      
    If they differ       
     insert a new user record into the warehouse and select the new id,       
    else      
     select the warehouse version of the user id      
  */      
  DECLARE @myUserInsertRequiredFlag int      
  SET @myUserInsertRequiredFlag = 0      
      
  IF EXISTS      
  (SELECT [ID] FROM WAREHOUSE_UserTable      
  WHERE UserId = @mySAUserId      
  AND OriginatorID = @OriginatorID)      
  BEGIN      
   -- get the user version number from the warehouse      
   DECLARE @myWarehouseUserVersion int      
   SELECT TOP(1) @myWarehouseUserVersion = version      
   FROM WAREHOUSE_UserTable      
   WHERE UserId = @mySAUserId      
   AND OriginatorID = @OriginatorID      
      
   -- check the version numbers match      
   IF @myWarehouseUserVersion < @mySAUserIdVersion      
    BEGIN      
     -- the user in the warehouse is out of sync with SA      
     -- insert a record for this user       
     SET @myUserInsertRequiredFlag = 1      
    END      
  END      
  ELSE      
   BEGIN      
    -- user does not exist so we must make it      
    SET @myUserInsertRequiredFlag = 1      
   END      
        
  IF @myUserInsertRequiredFlag = 1      
   BEGIN      
    --insert a new user record      
    INSERT [dbo].[WAREHOUSE_UserTable]      
        ([UserId]      
        ,[CandidateRef]      
        ,[Forename]      
        ,[Surname]      
        ,[Middlename]      
        ,[DOB]      
        ,[Gender]      
        ,[SpecialRequirements]      
        ,[AddressLine1]      
        ,[AddressLine2]      
        ,[Town]      
        ,[County]      
        ,[Country]      
        ,[PostCode]      
        ,[Telephone]      
        ,[Email]      
        ,[EthnicOrigin]      
        ,[AccountCreationDate]      
        ,[AccountExpiryDate]      
        ,[ExtraInfo]      
        ,[version]      
        ,[OriginatorID]    
        ,[ULN]
        ,[Username])      
    SELECT      
      [UserTable].[ID]      
      ,[UserTable].[CandidateRef]      
      ,[UserTable].[Forename]      
      ,[UserTable].[Surname]      
      ,[UserTable].[Middlename]      
      ,[UserTable].[DOB]      
      ,[UserTable].[Gender]      
      ,[UserTable].[SpecialRequirements]      
      ,[UserTable].[AddressLine1]      
      ,[UserTable].[AddressLine2]      
      ,[UserTable].[Town]      
      ,[CountyLookupTable].[County]      
      ,[CountryLookupTable].[Country]      
      ,[UserTable].[PostCode]      
      ,[UserTable].[Telephone]      
      ,[UserTable].[Email]      
      ,[EnthnicOriginLookupTable].[Name]      
      ,[UserTable].[AccountCreationDate]      
      ,[UserTable].[AccountExpiryDate]      
      ,[UserTable].[ExtraInfo]      
      ,[UserTable].[version]      
      ,@OriginatorID     
      ,[UserTable].[ULN]  
      ,[UserTable].[Username]   
    FROM [dbo].[UserTable],[dbo].[CountyLookupTable],[dbo].[CountryLookupTable], [dbo].[EnthnicOriginLookupTable]      
    WHERE [UserTable].[ID] = @mySAUserId      
    AND [dbo].[UserTable].[County] = [dbo].[CountyLookupTable].[ID]      
    AND [dbo].[UserTable].[Country] = [dbo].[CountryLookupTable].[ID]      
    AND [dbo].[EnthnicOriginLookupTable].[ID] = [dbo].[UserTable].[EthnicOriginID]      
   END      
      
   -- get the latest id for the affected user from the warehouse table      
   DECLARE @myWarehouseUserId  int      
   SELECT @myWarehouseUserId = [ID]       
   FROM [WAREHOUSE_UserTable]      
   WHERE [UserId] = @mySAUserId      
   AND OriginatorID = @OriginatorID      
   AND version = @mySAUserIdVersion      
        
   -- select the all entries in the ExamStateChangeAuditTable for the current exam session      
   -- into an xml variable      
   DECLARE @examStateChangeAuditXml xml      
   SET @examStateChangeAuditXml =       
   (SELECT TAG, PARENT, [exam!1!Element], [exam!1!id],       
   [stateChange!2!newStateID!Element], [stateChange!2!newState!Element],       
   [stateChange!2!changeDate!Element], [stateChange!2!information!xml]      
         
   FROM      
   (SELECT 1 AS TAG,      
     NULL  AS  Parent,      
     NULL  as  [exam!1!Element],      
     NULL  as  [exam!1!id],      
     NULL  AS  [stateChange!2!newStateID!Element],      
     NULL  AS  [stateChange!2!newState!Element],      
     NULL  AS  [stateChange!2!changeDate!Element],      
     NULL  AS  [stateChange!2!information!xml]      
           
     UNION ALL      
     SELECT      
     2   AS Tag,      
     1   AS Parent,      
     NULL,      
     [ExamStateChangeAuditTable].[ExamSessionID],      
     [ExamStateChangeAuditTable].[NewState],      
     [ExamStateLookupTable].[StateName],      
     CONVERT(VARCHAR(23), [ExamStateChangeAuditTable].[StateChangeDate], 113),      
     [ExamStateChangeAuditTable].[StateInformation]      
           
     FROM [dbo].[ExamStateChangeAuditTable]      
      INNER JOIN [dbo].[ExamStateLookupTable] ON      
       [ExamStateLookupTable].[ID] = [ExamStateChangeAuditTable].[NewState]      
           
     WHERE [ExamStateChangeAuditTable].[ExamSessionID] = @examInstanceId)      
    AS stateChangeXml      
    FOR XML EXPLICIT)      
          
   /*######################D Naylor 08/06/09#########################      
   * Code select to retrive the exam audit trial as xml and store this in the warehouse      
   */      
    DECLARE @IpAuditXml xml      
    SET @IpAuditXml =      
    (SELECT       
     ID     AS  '@id',      
     examInstanceID  AS  'examInstanceId',      
     IPAddress   AS  'ipAddress',      
     methodCalled  AS  'methodCalled'      
    FROM ExamSessionAuditTrail      
    WHERE examInstanceID = @examInstanceId      
    FOR XML PATH('entry'), ROOT('ipData')      
    )      
         
   -- place if not exists on here for concurrency reasons      
   IF NOT EXISTS (SELECT ID FROM [dbo].[WAREHOUSE_ExamSessionTable]      
       WHERE [ExamSessionID] = @examInstanceId AND WAREHOUSEScheduledExamID = @myWarehouseScheduledExamId)      
    BEGIN      
     /*DAVID NAYLOR 08/05/08 R4      
     * made changes to read the completion date from the statechange audit trial.      
     * This is in place to improve the performance of the rerporting UI      
     */      
     -- get the completionDate into a variable      
     DECLARE @completionDate smalldatetime      
     SET @completionDate = CONVERT(smalldatetime, CONVERT(varchar(max), @examStateChangeAuditXml.query('//stateChange[newState[text()=''Exam Submitted'']][1]/changeDate/text()')))      
           
     -- if the completion date is  1 Jan 1900 00:00:00, the exam has been voided      
     -- the completion date needs to be set from this voided date instead.      
     IF @completionDate = convert(smalldatetime, '1 Jan 1900 00:00:00')      
     BEGIN      
      SET @completionDate = CONVERT(smalldatetime,CONVERT(nvarchar(max), @examStateChangeAuditXml.query('//stateChange[newState[text()=''Exam Voided'']][1]/changeDate/text()')))      
     END      
      
     -- if the completion date still hasn't been set, just set it to now      
     IF @completionDate = convert(smalldatetime, '1 Jan 1900 00:00:00')      
     BEGIN      
      SET @completionDate = GETDATE()      
     END      
     
     ---Modify LastInUseDate in ResultData    
     SELECT @ResultDataFull = ResultDataFull FROM [dbo].[ExamSessionTable] WHERE [ID] = @examInstanceId      
     SET @TodaysDate = GETDATE()
     SET @ResultDataFull.modify('replace value of (//assessmentDetails/lastInUseDate/text())[1]  with (sql:variable("@TodaysDate"))')  
     
      
     -- now insert the exam session from the SA db into the warehouse version      
     INSERT [dbo].[WAREHOUSE_ExamSessionTable]      
       ( [ExamSessionID]      
          ,[WAREHOUSEScheduledExamID]      
          ,[WAREHOUSEUserID]      
          ,[StructureXML]      
          ,[MarkerData]      
          ,[KeyCode]      
          ,[PreviousExamState]      
          ,[pinNumber]      
          ,[duration]      
          ,[ExamStateChangeAuditXml]      
          ,[resultData]      
          ,[warehouseTime]      
          ,[resultDataFull]      
          ,[completionDate]      
          ,[CQN]      
          ,[examIPAuditData]      
          ,[downloadInformation]      
          ,[clientInformation]      
		  ,[availableForCentreReview]    
		  ,[ExportToSecureMarker]    
		  ,WarehouseExamState
		  ,[AllowPackageDelivery]  
		  ,[ContainsBTLOffice]      
		  ,[appeal]   
		  ,[submissionExported]
		  ,[TargetedForVoid]
		  ,[EnableOverrideMarking]
		  ,[itemMarksUploaded]
		  ,[TakenThroughLocalScan]
		  ,[LocalScanDownloadDate]
		  ,[LocalScanUploadDate]
		  ,[LocalScanNumPages]
         )      
         SELECT  [ExamSessionTable].[ID]      
         ,@myWarehouseScheduledExamId      
         ,@myWarehouseUserId      
         ,[StructureXML]      
         ,[MarkerData]      
         ,[KeyCode]      
         ,[examState]      
         ,[pinNumber]      
         ,[duration]      
         ,@examStateChangeAuditXml      
         ,[resultData]      
         ,GETDATE()      
         ,@ResultDataFull       
         ,@completionDate      
         ,[UserQualificationsTable].[CQN]      
         ,@IpAuditXml      
         ,[downloadInformation]      
         ,[clientInformation]      
         ,@availableForCentreReview    
         ,[ExportToSecureMarker]    
         ,CASE WHEN ExportToSecureMarker = 1 AND examState <> 10 AND [ItemMarksUploaded] = 0 THEN 2 ELSE 1 END 
         ,[AllowPackageDelivery]    
         ,[ContainsBTLOffice]  
         ,ISNULL(StructureXml.value('(assessmentDetails/autoViewExam)[1]','INT'), 0)
         ,[submissionExported]
         ,[TargetedForVoid]
         ,[ExamSessionTable].[EnableOverrideMarking]
         ,[ItemMarksUploaded]
         ,[TakenThroughLocalScan]
         ,[LocalScanDownloadDate]
         ,[LocalScanUploadDate]
         ,[LocalScanNumPages]
     FROM [dbo].[ExamSessionTable]   
     INNER JOIN [WAREHOUSE_ScheduledExamsTable]      
      ON [WAREHOUSE_ScheduledExamsTable].[ID] = @myWarehouseScheduledExamId      
     INNER JOIN [UserQualificationsTable]      
      ON (      
       [UserQualificationsTable].[UserId] = [ExamSessionTable].[UserId]      
       AND      
       [UserQualificationsTable].[QualificationId] = [WAREHOUSE_ScheduledExamsTable].[QualificationId]      
       )      
     WHERE [ExamSessionTable].[ID] = @examInstanceId      
      
     DECLARE @didInsertWork int      
     SET @didInsertWork = @@rowcount      
      
     -- get the new id for the exam session in the warehouse      
     DECLARE @myNewWarehouseExamSessionId int      
     SET @myNewWarehouseExamSessionId = (select scope_identity())      
      
         -- confirm @myNewWarehouseExamSessionId is valid      
     IF (@didInsertWork = 0)      
      BEGIN      
       -- now insert the exam session from the SA db into the warehouse version      
       -- but with placeholder CQN this time      
--dfdf      

	 ---Modify LastInUseDate in ResultData
	 SELECT @ResultDataFull = ResultDataFull FROM [dbo].[ExamSessionTable] WHERE [ID] = @examInstanceId      
     SET @TodaysDate = GETDATE()
     SET @ResultDataFull .modify('replace value of (//assessmentDetails/lastInUseDate/text())[1]  with (sql:variable("@TodaysDate"))')  
    
       INSERT [dbo].[WAREHOUSE_ExamSessionTable]      
         (   [ExamSessionID]      
            ,[WAREHOUSEScheduledExamID]      
            ,[WAREHOUSEUserID]      
            ,[StructureXML]      
            ,[MarkerData]      
            ,[KeyCode]      
            ,[PreviousExamState]      
            ,[pinNumber]      
            ,[duration]      
            ,[ExamStateChangeAuditXml]      
            ,[resultData]      
            ,[warehouseTime]      
            ,[resultDataFull]      
            ,[completionDate]      
            ,[CQN]      
            ,[examIPAuditData]      
            ,[appeal]      
            ,[availableForCentreReview]    
            ,[ExportToSecureMarker]
            ,[WarehouseExamState]    
            ,[AllowPackageDelivery]    
            ,[ContainsBTLOffice]  
            ,[submissionExported]          
            ,[TargetedForVoid]
            ,[EnableOverrideMarking]
            ,[ItemMarksUploaded] 
           )      
           SELECT  [ExamSessionTable].[ID]      
           ,@myWarehouseScheduledExamId      
           ,@myWarehouseUserId      
           ,[StructureXML]      
           ,[MarkerData]      
           ,[KeyCode]      
           ,[examState]      
           ,[pinNumber]      
           ,[duration]      
           ,@examStateChangeAuditXml      
           ,[resultData]      
           ,GETDATE()      
           ,@ResultDataFull      
           ,@completionDate      
           ,404      
           ,@IpAuditXml      
           ,ISNULL(StructureXml.value('(assessmentDetails/autoViewExam)[1]','INT'), 0)
           ,@availableForCentreReview    
           ,[ExportToSecureMarker]    
           ,CASE WHEN ExportToSecureMarker = 1 AND examState <> 10 AND [ItemMarksUploaded] = 0 THEN 2 ELSE 1 END 
           ,[AllowPackageDelivery]    
           ,[ContainsBTLOffice]    
           ,[submissionExported]       
           ,[TargetedForVoid]
           ,[ExamSessionTable].[EnableOverrideMarking]
           ,[ItemMarksUploaded]
       FROM [dbo].[ExamSessionTable]      
       INNER JOIN [WAREHOUSE_ScheduledExamsTable]      
        ON [WAREHOUSE_ScheduledExamsTable].[ID] = @myWarehouseScheduledExamId      
       WHERE [ExamSessionTable].[ID] = @examInstanceId      
      
       SET @myNewWarehouseExamSessionId = (select scope_identity())      
      END      
      
     --insert the exam session item response from sa into the warehouse      
     INSERT [dbo].[WAREHOUSE_ExamSessionItemResponseTable]      
         ([WAREHOUSEExamSessionID]      
         ,[ItemID]      
         ,[ItemVersion]      
         ,[ItemResponseData]      
         ,[MarkerResponseData]      
         ,[ItemMark]
         ,[MarkingIgnored])      
     SELECT       
       @myNewWarehouseExamSessionId      
       ,[ItemID]      
       ,[ItemVersion]      
       ,[ItemResponseData]      
       ,[MarkerResponseData]      
       ,[ItemMark]
       ,[MarkingIgnored]      
     FROM [dbo].[ExamSessionItemResponseTable]      
     WHERE ExamSessionID = @examInstanceId      
           
    
    
           
     /*FIX for bug id 2560; Add the marker into the warehouse user table if they       
     a; dont exist or      
     b; have been updated since their last entry      
     */      
         
     --get the marker data into a variable      
     DECLARE @markerData xml      
     SELECT @markerData = [MarkerResponseData] FROM [dbo].[ExamSessionItemResponseTable]      
     WHERE ExamSessionID = @examInstanceId      
      
     --create a user table to hold the user id of the markers in the MarkerResponseData      
     CREATE TABLE #markerUserTable      
      (userId   int)      
            
     --loop the marker response xml and insert into the temp user table if the user does not exist      
     -- Total count of <Entry> Nodes      
     DECLARE @max INT, @i INT      
     SELECT      
     @max = @markerData.query('<e>      
         { count(/entries/entry) }      
         </e>').value('e[1]','int')      
     -- Set counter variable to 1      
     SET @i = 1      
     -- variable to store marker id      
     DECLARE @markerId int      
     -- loop starts      
     WHILE @i <= @max       
     BEGIN      
      -- select "ID" to the variable      
      SELECT      
       @markerId = x.value('userId[1]', 'int')      
      FROM      
      @markerData.nodes('/entries/entry[position()=sql:variable("@i")]')      
      e(x)      
            
      --check the user id is not in the temp #markerUserTable and insert if not      
      IF NOT EXISTS(SELECT userId FROM #markerUserTable      
          WHERE userId = @markerId)      
      BEGIN      
       INSERT INTO #markerUserTable(userId)      
       values(@markerId)      
      END      
      ---- increment counter      
      SET @i = @i + 1             
     END      
           
     --update the warehouse users table with markers that are not already in there      
     --loop the users table      
     WHILE (SELECT COUNT(userId) FROM #markerUserTable) > 0      
     BEGIN      
      DECLARE @newMarkerID int      
      SELECT TOP(1) @newMarkerID = userId from #markerUserTable      
            
      DECLARE @mySAMarkerIdVersion int      
      SELECT @mySAMarkerIdVersion = version from Usertable      
      WHERE ID = @newMarkerID      
            
            
            
      DECLARE @myMarkerUserInsertRequiredFlag int      
      SET @myMarkerUserInsertRequiredFlag = 0      
      
      IF EXISTS      
      (SELECT [ID] FROM WAREHOUSE_UserTable      
      WHERE UserId = @newMarkerID      
      AND OriginatorID = @OriginatorID)      
      BEGIN      
       -- get the user version number from the warehouse      
       DECLARE @myWarehouseMarkerUserVersion int      
       SELECT TOP(1) @myWarehouseMarkerUserVersion = version      
       FROM WAREHOUSE_UserTable      
       WHERE UserId = @newMarkerID      
       AND OriginatorID = @OriginatorID      
      
       -- check the version numbers match      
       IF @myWarehouseMarkerUserVersion < @mySAMarkerIdVersion      
        BEGIN      
         -- the user in the warehouse is out of sync with SA      
         -- insert a record for this user       
         SET @myMarkerUserInsertRequiredFlag = 1      
        END      
      END      
      ELSE      
      BEGIN      
       -- user does not exist so we must make it      
       SET @myMarkerUserInsertRequiredFlag = 1      
      END      
            
      IF @myMarkerUserInsertRequiredFlag = 1      
       BEGIN      
        --insert a new user record      
        INSERT [dbo].[WAREHOUSE_UserTable]      
            ([UserId]      
            ,[CandidateRef]      
            ,[Forename]      
            ,[Surname]      
            ,[Middlename]      
            ,[DOB]      
            ,[Gender]      
            ,[SpecialRequirements]      
            ,[AddressLine1]      
            ,[AddressLine2]      
            ,[Town]      
            ,[County]      
            ,[Country]      
            ,[PostCode]      
            ,[Telephone]      
            ,[Email]      
            ,[EthnicOrigin]      
            ,[AccountCreationDate]      
            ,[AccountExpiryDate]      
            ,[ExtraInfo]      
            ,[version]      
            ,[OriginatorID]    
            ,[ULN]
            ,[Username])      
        SELECT      
          [UserTable].[ID]      
          ,[UserTable].[CandidateRef]      
          ,[UserTable].[Forename]      
          ,[UserTable].[Surname]      
          ,[UserTable].[Middlename]      
          ,[UserTable].[DOB]      
          ,[UserTable].[Gender]      
          ,[UserTable].[SpecialRequirements]      
          ,[UserTable].[AddressLine1]      
          ,[UserTable].[AddressLine2]      
          ,[UserTable].[Town]      
          ,[CountyLookupTable].[County]      
          ,[CountryLookupTable].[Country]      
          ,[UserTable].[PostCode]      
          ,[UserTable].[Telephone]      
          ,[UserTable].[Email]      
          ,[EnthnicOriginLookupTable].[Name]      
          ,[UserTable].[AccountCreationDate]      
          ,[UserTable].[AccountExpiryDate]      
          ,[UserTable].[ExtraInfo]      
          ,[UserTable].[version]      
          ,@OriginatorID     
          ,[UserTable].[ULN] 
          ,[UserTable].[Username]    
        FROM [dbo].[UserTable],[dbo].[CountyLookupTable],[dbo].[CountryLookupTable],[dbo].[EnthnicOriginLookupTable]      
        WHERE [UserTable].[ID] = @newMarkerID      
        AND [dbo].[UserTable].[County] = [dbo].[CountyLookupTable].[ID]      
        AND [dbo].[UserTable].[Country] = [dbo].[CountryLookupTable].[ID]      
        AND [dbo].[EnthnicOriginLookupTable].[ID] = [UserTable].[EthnicOriginID]      
       END      
    
      DELETE TOP (1) FROM #markerUserTable      
     END--end loop      
      
     --CLEAN UP      
      
     DROP TABLE #markerUserTable      
           
     /*===========================================================================*/      
           
/*D NAYLOR - R7 05/09#################################################################      
Performance improvements, new shredded item tables have been created in order to remove the need       
for X-Query within the warehouse data requests. These new tables are populated below using the data      
just inserted into the warehouse and making use of the existing views.      
*/      
      
           
      
     --Insert into the Shredded data version of the candidate exam audit view      
     insert into WAREHOUSE_ExamSessionTable_Shreded  
     (examSessionId,      
      structureXml,      
      examVersionName,      
      examVersionRef,      
      examVersionId,      
      examName,      
      examRef,      
      qualificationid,      
      qualificationName,      
      qualificationRef,      
      resultData,      
      submittedDate,      
      originatorId,      
      centreName,      
      centreCode,      
      foreName,      
      dateOfBirth,      
      gender,      
      candidateRef,      
      surName,      
      scheduledDurationValue,      
      previousExamState,      
      examStateInformation,      
      examResult,      
      passValue,      
      closeValue,      
      --0,      
      externalReference,      
      examType,      
      warehouseTime,      
      CQN,      
      actualDuration,      
      appeal,      
      reMarkStatus,        
      qualificationLevel,   
      centreId,     
      uln,  
      AllowPackageDelivery,  
      ExportToSecureMarker,  
      ContainsBTLOffice,
      ExportedToIntegration,
      WarehouseExamState,
      [language],
      KeyCode,
      TargetedForVoid,
      EnableOverrideMarking,
      AddressLine1,
      AddressLine2,
      Town,
      County,
      Country,
      Postcode,
      ScoreBoundaryData,
      itemMarksUploaded,
      passMark,
      totalMark,
      passType,
      voidJustificationLookupTableId,
      automaticVerification,
      resultSampled,
      [started],
      submitted,
      validFromDate,
      expiryDate,
      totalTimeSpent,
      defaultDuration,
      scheduledDurationReason,
      WAREHOUSEScheduledExamID,
      WAREHOUSEUserID,
      itemDataFull,
      examId,
      userId,
      TakenThroughLocalScan,
	  LocalScanDownloadDate,
      LocalScanUploadDate,
	  LocalScanNumPages)    
     SELECT      
      examSessionId,      
      structureXml,      
      examVersionName,      
      examVersionRef,      
      examVersionId,      
      examName,      
      examRef,      
      qualificationid,      
      qualificationName,      
      qualificationRef,      
      resultData,      
      submittedDate,      
      originatorId,      
      centreName,      
      centreCode,      
      foreName,      
      dateOfBirth,      
      gender,      
      candidateRef,      
      surName,      
      scheduledDurationValue,      
      previousExamState,      
      examStateInformation,      
      examResult,      
      passValue,      
      closeValue,      
      --0,      
      externalReference,      
      examType,      
      warehouseTime,      
      CQN,      
      actualDuration,      
      appeal,      
      reMarkStatus,        
      qualificationLevel,   
      centreId,     
      uln,  
      AllowPackageDelivery,  
      ExportToSecureMarker,  
      ContainsBTLOffice,
      ExportedToIntegration,
      WarehouseExamState,
      [language],
      KeyCode,
      TargetedForVoid,
      EnableOverrideMarking,
      AddressLine1,
      AddressLine2,
      Town,
      County,
      Country,
      Postcode,
      ScoreBoundaryData,
      itemMarksUploaded,
      passMark,
      totalMark,
      passType,
      voidJustificationLookupTableId,
      automaticVerification,
      resultSampled,
      [started],
      submitted,
      validFromDate,
      expiryDate,
      totalTimeSpent,
      defaultDuration,
      scheduledDurationReason,
      WAREHOUSEScheduledExamID,
      WAREHOUSEUserID,
      itemDataFull,
      examId,
      userId,
      TakenThroughLocalScan,
      LocalScanDownloadDate,
      LocalScanUploadDate,
	  LocalScanNumPages
     FROM sa_CandidateExamAudit_View      
     WHERE examSessionId = @myNewWarehouseExamSessionId                 
	 	
	/* New insert to shred new columns */
	BEGIN TRANSACTION;
		INSERT INTO WAREHOUSE_ExamSessionTable_ShrededItems
		 (	 ExamSessionID
			,ItemRef
			,ItemName
			,UserMark
			,MarkerUserMark
			,UserAttempted
			,ItemVersion
			,MarkingIgnored
			,MarkedMetadataCount
			,ExamPercentage
			,TotalMark
			,ResponseXML
			,OptionsChosen
			,SelectedCount
			,CorrectAnswerCount
			,itemType
			,FriendItems)
			SELECT	 @myNewWarehouseExamSessionId AS [examSessionId]
					,Result.Item.value('@id', 'nvarchar(15)') AS [itemRef]
					,Result.Item.value('@name', 'nvarchar(200)') AS [itemName]
					,Result.Item.value('@userMark', 'decimal(6, 3)') AS [userMark]
					,Result.Item.value('@markerUserMark', 'nvarchar(max)') AS [markerUserMark]
					,Result.Item.value('@userAttempted', 'bit') AS [userAttempted]
					,Result.Item.value('@version', 'int') AS [itemVersion]
					,Result.Item.value('@markingIgnored', 'tinyint') AS [markingIgnored]
					,Result.Item.value('count(mark)', 'int') AS [markedMetadataCount]
					,WAREHOUSE_ExamSessionTable.ResultData.value('(exam/@userPercentage)[1]', 'decimal(6, 3)') AS [examPercentage]
					,Result.Item.value('@totalMark', 'decimal(6, 3)') AS [totalMark]
					,WAREHOUSE_ExamSessionItemResponseTable.ItemResponseData AS [responseXml]
					,CAST(ISNULL(WAREHOUSE_ExamSessionItemResponseTable.ItemResponseData.query('data(p/s/c[@typ = "10"]/i[@sl = "1"]/@ac)'), ' UA ') AS nvarchar(200)) AS [optionsChosen] --Why?
					,WAREHOUSE_ExamSessionItemResponseTable.ItemResponseData.value('count(p/s/c/i[@sl = "1"])', 'int') AS [selectedCount]
					,WAREHOUSE_ExamSessionItemResponseTable.ItemResponseData.value('count(p/s/c/i[@ca = "1"])', 'int') AS [correctAnswerCount]
					,Result.Item.value('@type', 'int') AS ItemType
					,Result.Item.value('@SurpassFriendItems', 'NVARCHAR(MAX)') AS FriendItems
			 FROM	 WAREHOUSE_ExamSessionTable
			 CROSS APPLY WAREHOUSE_ExamSessionTable.ResultData.nodes('exam/section/item') Result(Item)
			 LEFT JOIN WAREHOUSE_ExamSessionItemResponseTable -- we want to include nulls in the the item response table so that non attempted items are included
					 ON WAREHOUSE_ExamSessionItemResponseTable.WAREHOUSEExamSessionID = @myNewWarehouseExamSessionId
					AND WAREHOUSE_ExamSessionItemResponseTable.ItemID = Result.Item.value('@id', 'nvarchar(15)')
			WHERE	 WAREHOUSE_ExamSessionTable.ID = @myNewWarehouseExamSessionId;
	COMMIT TRANSACTION;

	BEGIN TRANSACTION;
		INSERT INTO WAREHOUSE_ExamSessionAvailableItemsTable
		 (	 ExamSessionID
			,ItemID
			,ItemVersion
			,ItemXML	)
			SELECT	 @myNewWarehouseExamSessionId AS [examSessionId]
					,StructureXML.Item.value('(@id)[1]', 'nvarchar(15)') AS [itemId]
					,StructureXML.Item.value('(@version)[1]', 'int') AS [itemVersion]
					,StructureXML.Item.query('.') AS [itemXml]
			 FROM	 WAREHOUSE_ExamSessionTable
			 CROSS APPLY WAREHOUSE_ExamSessionTable.StructureXML.nodes('assessmentDetails/assessment/section/item') StructureXML(Item)
			WHERE	 WAREHOUSE_ExamSessionTable.ID = @myNewWarehouseExamSessionId;
	COMMIT TRANSACTION;        
      
     -- update userMark where it has been overridden by a human marker      
     UPDATE WAREHOUSE_ExamSessionTable_ShrededItems      
     SET [userMark] = CONVERT(decimal(6,3),[markerUserMark]) / (CASE [TotalMark] WHEN 0 THEN 1 ELSE [TotalMark] END)
     WHERE examSessionId = @myNewWarehouseExamSessionId      
     AND [markerUserMark] != ''      
           
     SET @returnString = '<success/>'     
    
     --insert the exam session uploaded documents from sa into the warehouse      
     INSERT INTO [dbo].[WAREHOUSE_ExamSessionDocumentTable]    
           ([ID]    
           ,[warehouseExamSessionID]    
           ,[itemId]    
           ,[documentName]    
           ,[Document]    
           ,[uploadDate])    
     SELECT [ID]    
      ,@myNewWarehouseExamSessionId    
      ,[itemId]    
      ,[documentName]    
      ,[Document]    
      ,[uploadDate]    
     FROM [dbo].[ExamSessionDocumentTable]    
     WHERE ([ExamSessionDocumentTable].[examSessionId]=@examInstanceId)    
     
     --Shred the ScoreBoundary data from the structure xml
    
    END      
   ELSE      
    BEGIN      
     -- exam has already been archived, report the error      
      SET @returnString = '<result errorCode="1"><return>The exam session already exists in the archive</return></result>'      
     END  

--DJ 16/05/2013 - Insert into dbo.WAREHOUSE_ExamSessionTable_ShreddedItems_Mark table the MarkedMetadata mark node details.
	BEGIN TRANSACTION;
		INSERT INTO WAREHOUSE_ExamSessionTable_ShreddedItems_Mark
		(	ExamSessionID
		   ,ItemID
		   ,Mark
		   ,LearningOutcome
		   ,DisplayText
		   ,MaxMark)
			SELECT  WAREHOUSE_ExamSessionTable.ID AS [ExamSessionID]
				   ,Result.Item.value('@id', 'varchar(15)') AS [ItemID]
				   ,Item.Mark.value('@mark', 'decimal(6, 3)') AS [Mark]
				   ,Item.Mark.value('@learningOutcome', 'int') AS [LearningOutcome]
				   ,Item.Mark.value('@displayName', 'nvarchar(200)') AS [DisplayName]
				   ,Item.Mark.value('@maxMark', 'decimal(6, 3)') AS [MaxMark]
			FROM   WAREHOUSE_ExamSessionTable
			CROSS APPLY WAREHOUSE_ExamSessionTable.ResultData.nodes('exam/section/item') Result(Item)
			CROSS APPLY Result.Item.nodes('mark') Item(Mark)
			WHERE   WAREHOUSE_ExamSessionTable.ID = @myNewWarehouseExamSessionId;
	COMMIT TRANSACTION; 

 END TRY      
 BEGIN CATCH    
	WHILE @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;
		  
  SET @errorNum = (SELECT ERROR_NUMBER() AS ErrorNumber)      
  SET @errorMess = (SELECT ERROR_MESSAGE() AS ErrorMessage)      
  SET @returnString = '<result errorCode="2"><return>SQL Error, Number: ' + @errorNum + ' MESSAGE: ' + @errorMess + '</return></result>'      
       
 END CATCH      
END
