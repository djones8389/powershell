/****** Script for SelectTopNRows command from SSMS  ******/
SELECT TOP 1000 [Id]
      ,[SourceId]
      ,[Version]
      ,[ExecutionGUID]
      ,[OriginalExecutionGUID]
      ,[PeriodStart]
      ,[PeriodEnd]
      ,[Result]
      ,[Comment]
  FROM [UAT_BC2_SurpassDataWarehouse].[ETL].[ExecutionLog]

  UPDATE [UAT_BC2_SurpassDataWarehouse].[ETL].[ExecutionLog]
  set PeriodEnd = (	
					SELECT MAX(WarehouseTime)
					from UAT_BC2_SurpassDataWarehouse..FactExamSessions
	)
  where id=  1

  --2015-08-18 20:00:03.857

SELECT MAX(WarehouseTime)
from UAT_BC2_SurpassDataWarehouse..FactExamSessions