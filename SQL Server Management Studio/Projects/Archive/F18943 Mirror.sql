--MIRROR--

USE Rcpch_SurpassManagement

SELECT  ItemBankSubjectId, UserCentreSubjectPermissions.CentreId as 'EditionsCentreID' , SecureAssessCentreId as 'SACentreID'
	, QualificationName, IsValid, status
  FROM UserCentreSubjectPermissions
  
  inner join Centres
  on Centres.Id = UserCentreSubjectPermissions.CentreId
  
  inner join Subjects
  on Subjects.Id = UserCentreSubjectPermissions.SubjectId
  
  inner join Rcpch_ItemBank..QualificationTable
  on Rcpch_ItemBank..QualificationTable.ID = Subjects.ItemBankSubjectId
  
  inner join Rcpch_SecureAssess..CentreQualificationsTable
  on Rcpch_SecureAssess..CentreQualificationsTable.CentreID =  Centres.SecureAssessCentreId
  
  where-- Status = 0 and IsValid = 1 
   centres.Name like 'Bri%'
  
   

  
use Rcpch_SecureAssess
  
  
  select CentreID, QualificationID, CentreName, QualificationName
	 from CentreQualificationsTable
  
  inner join CentreTable
  on CentreTable.ID = CentreQualificationsTable.CentreID
  
  inner join Rcpch_ItemBank..QualificationTable
  on Rcpch_ItemBank..QualificationTable.ID = CentreQualificationsTable.QualificationID
  
  where CentreTable.CentreName like '%Bri%'