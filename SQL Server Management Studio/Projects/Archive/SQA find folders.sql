USE SQA_CPProjectAdmin

select CAST(structurexml as xml) as myXML
	into #XML1
	from SharedLibraryTable where ProjectId = 942;
	
select 
	a.b.value('@name','nvarchar(1000)') as 'FolderName'
	,myXML
from #XML1	
cross apply myXML.nodes('//fol') a(b)	
	
USE SQA_CPProjectAdmin_LIVE

select CAST(structurexml as xml) as myXML
	into #XML2
	from SharedLibraryTable where ProjectId = 942;
	
select 
	a.b.value('@name','nvarchar(1000)') as 'FolderName'
	,myXML
from #XML2	
cross apply myXML.nodes('//fol') a(b)		
	
	drop table #XML1
	drop table #XML2
	