SELECT X.*
	,X.ProjectStructureXml.value('(//Pag[@ID = sql:column("X.PageNo")]/@sta)[1]', 'int')
	,S.StatusName
FROM (
SELECT TOP 10 
	 ProjectListTable.ID ProjectID
	,ProjectListTable.ProjectStructureXml
	,PageTable.ID
	,SUBSTRING(PageTable.ID, CHARINDEX(N'P', PageTable.ID) + 1, LEN(PageTable.ID)) PageNo
FROM ProjectListTable
INNER JOIN PageTable
ON PageTable.ParentID = ProjectListTable.ID
INNER JOIN SceneTable
ON SceneTable.ParentID = PageTable.ID
INNER JOIN ComponentTable
ON ComponentTable.ParentID = SceneTable.ID
INNER JOIN ItemGraphicTable
ON ItemGraphicTable.ParentID = ComponentTable.ID
INNER JOIN ProjectManifestTable
ON ProjectListTable.ID = ProjectManifestTable.ProjectId
AND ItemGraphicTable.ali = CAST(ProjectManifestTable.ID AS NVARCHAR(50))
WHERE ProjectManifestTable.name = N'table_highlight_table.swf'
) X
INNER JOIN (
	SELECT ProjectListTable.ID
		,d.s.value('@id', 'int') StatusID
		,d.s.value('(.)[1]', 'nvarchar(60)') StatusName
	FROM ProjectListTable
	CROSS APPLY ProjectListTable.ProjectDefaultXml.nodes('/Defaults/StatusList/Status') d(s)
) S
ON X.ProjectID = S.ID
AND X.ProjectStructureXml.value('(//Pag[@ID = sql:column("X.PageNo")]/@sta)[1]', 'int') = S.StatusID