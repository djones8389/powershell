--112 before

select X.* FROM (

select ID, name, projectid, DATALENGTH(Image) as Image, sharedLib, delcol
from ProjectManifestTable as PMT
where PMT.projectID = 942
 ) A


 right JOIN 
 (
	SELECT X.ProjectID
		,sl.i.value('@id', 'int') ItemID
	FROM (
		SELECT SharedLibraryTable.ProjectID
			,CAST(SharedLibraryTable.structureXML AS XML) AS [StructureXML]
		FROM dbo.SharedLibraryTable
		where SharedLibraryTable.projectID = 942
		) X
	CROSS APPLY X.StructureXML.nodes('//item') sl(i)

	where sl.i.value('@visible', 'bit')  = 0

	) X 
ON A.ProjectId = X.ProjectID
  AND A.ID = X.ItemID
  
  where a.Name is null


