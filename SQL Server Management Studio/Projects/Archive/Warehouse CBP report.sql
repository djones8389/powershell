SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
--171 before doing Xpath on statechangeAuditXML--

;with cte as (

	select distinct examName 
	from WAREHOUSE_ScheduledExamsTable 
	where examName like '%ICAS%'
	or examName like '%ISYS%'
	or examName like '%DSSK%'
	or examName like '%WKTM%'
	or examName like '%PDBC%'
	or examName like '%SDST5%'
	or examName like '%PRSW%'
	or examName like '%DLPR%'
	or examName like '%OFMW%'
	or examName like '%DVCA%'
)


SELECT west.[WarehouseExamState]
	 , pinNumber
	 , wests.examName
	 , WEST.KeyCode
	 , WCT.CentreName
	 , wests.qualificationName
	 , cast(ScheduledStartDateTime as date) as [Start Date], cast(ScheduledEndDateTime as date) as [End Date]
	 , WUT.Forename + ' ' + WUT.Surname as [Candidate]
	 , Wut.CandidateRef
	 , invigilated
	 , cast(DATEADD(minute, ActiveStartTime, 0) as time(0)) as [StartTime]
	 , cast(DATEADD(minute, ActiveEndTime, 0) as time(0)) as [EndTime]
	-- , started as [Started]
	 , ExamStateChangeAuditXml.query('data(exam/stateChange[newStateID=6]/changeDate)[1]') as [Started]
	 , west.warehouseTime
	 , west.PreviousExamState 
FROM WAREHOUSE_ExamSessionTable as WEST

  Inner Join WAREHOUSE_ScheduledExamsTable as WSCET
  on WSCET.ID = WEST.WAREHOUSEScheduledExamID
  
  Inner Join WAREHOUSE_UserTable as WUT
  on WUT.ID = WEST.WAREHOUSEUserID

  Inner Join WAREHOUSE_CentreTable as WCT
  on WCT.ID = WSCET.WAREHOUSECentreID

  Inner Join WAREHOUSE_ExamSessionTable_Shreded as WESTS
  on WESTS.examSessionId = WEST.ID

 where 
	wests.examName in (select examName from cte)
	and west.warehouseTime > '2014-09-01 00:00:01.000'
	and west.warehouseTime < '2014-12-31 23:59:59.000'
	
	
	--order by examState asc;
	
	--SELECT top 1 warehouseTime from WAREHOUSE_ExamSessionTable