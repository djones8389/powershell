EXECUTE sp_MSforeachdb N'
SET QUOTED_IDENTIFIER ON;
IF N''?'' LIKE N''EAL[_]CP[_]%''
BEGIN

SELECT N''?'' AS [ProjectName]
, ProjectStructureXML


FROM [?].dbo.[StructureTable]
      --where name like ''%fla%''
      --and sharedLib = 1
END
'



select 
	CONVERT(xml, Replace(CONVERT(nvarchar(max),(ProjectStructureXml)),'UTF-8','UTF-16')) as ProjectStructureXml
	--ProjectStructureXml
	, Fol.Pag.value('(@ID)[1]', 'nvarchar(12)') PageID
	, Fol.Pag.value('(@CH02)[1]', 'nvarchar(12)') CheckedOut
 from EAL_CP_Adv_Dip_Engineering_and_Technology_LIVE..StructureTable
 
 cross apply ProjectStructureXml.nodes('Pro/Fol/Pag')  Fol(Pag)
 --cross apply ProjectStructureXml.nodes('Pro/Pag')  Pro(Pag)
 --cross apply Fol.Pag.nodes('@ID') Pag(Fol)
 --cross apply Pro.Pag.nodes('@ID') Pag(Pro)
 
 
 
 select 
	--CONVERT(xml, Replace(CONVERT(nvarchar(max),(ProjectStructureXml)),'UTF-8','UTF-16')) as ProjectStructureXml.query('data(/Pro/Fol/Pag/[@chO2])'
	--CONVERT(xml, Replace(CONVERT(nvarchar(max),(ProjectStructureXml)),'UTF-8','UTF-16')).query('data(/Pro/Fol/Pag/@chO2)')
	ID
	--,CONVERT(xml, Replace(CONVERT(nvarchar(max),(ProjectStructureXml)),'UTF-8','UTF-16'))
	--,CONVERT(xml, Replace(CONVERT(nvarchar(max),(ProjectStructureXml)),'UTF-8','UTF-16')).value('count(//Pag[@chO2])', 'nvarchar(100)') as CheckedOut
 from EAL_CP_Adv_Dip_Engineering_and_Technology_LIVE..StructureTable
 
 --where CONVERT(xml, Replace(CONVERT(nvarchar(max),(ProjectStructureXml)),'UTF-8','UTF-16')).exist('/Pro/Fol/Pag[@chO2]') = 1
 
 
use [EAL_SecureAssess_10.0] 
 select top 2
	ResultData.query('data(/exam/@userMark)')	
 from WAREHOUSE_ExamSessionTable
