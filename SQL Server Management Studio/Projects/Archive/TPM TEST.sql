USE BritishCouncil_SecureAssess_LIVE
GO

BEGIN TRAN

select * from BritishCouncil_TPM..ScheduledPackageCandidateExams as SPCE

	inner join BritishCouncil_TPM..ScheduledPackageCandidates as SPC
	on SPC.ScheduledPackageCandidateId =  SPCE.Candidate_ScheduledPackageCandidateId

where ScheduledExamRef in ('V2N6WJ01')

      -- [TODO1] Specify test package ID 
      DECLARE @testPackageId int = 206

      -- [TODO2] Specify list of exam sessions that have been scheduled in SecureAssess, but not in TestPackage
      DECLARE @examSessions table(id int, candidateId int)
      INSERT @examSessions(id, candidateId)
            SELECT WEST.ID, WEST.WAREHOUSEUserID
                  FROM WAREHOUSE_ExamSessionTable WEST
                  WHERE ID in (538436)
	             
	             --Omitted 399901,399940,
	                          
      -- How many exams the test package contains
      DECLARE @testPackageExamCount int
      SELECT @testPackageExamCount = COUNT(*)
         FROM BritishCouncil_TPM..PackageExams PE
            WHERE PE.PackageId = @testPackageId
                  AND StatusValue = 0 -- Live

      DECLARE @candidates TABLE(candidateId int)
      INSERT @candidates(candidateId)
            SELECT candidateId
				FROM @examSessions
            GROUP BY candidateId

     -- Find candidate exams that have been scheduled in SecureAssess, but not in TestPackage
      DECLARE @candidateId int
      DECLARE @intersectedCount int

      WHILE EXISTS (SELECT TOP 1 * FROM @candidates)
      BEGIN
            SELECT TOP 1 @candidateId = candidateId FROM @candidates
            DELETE FROM @candidates WHERE candidateId = @candidateId

            -- Check if all exams from the package have been scheduled in SecureAssess
            SELECT @intersectedCount = COUNT(*)
                  FROM 
                  (
                        SELECT PE.SurpassExamId
                              FROM BritishCouncil_TPM..PackageExams PE
                              WHERE PE.PackageId = @testPackageId
                        INTERSECT
                        SELECT SEXT.ExamID
                              FROM @examSessions ES
                                    JOIN WAREHOUSE_ExamSessionTable EST ON EST.ID = ES.id
                                    JOIN WAREHOUSE_ScheduledExamsTable SEXT ON SEXT.ID = EST.WAREHOUSEScheduledExamID
                              WHERE ES.candidateId = @candidateId
                  ) T

            IF (@intersectedCount <> @testPackageExamCount)
            BEGIN
                  PRINT CAST(@candidateId AS nvarchar(100)) + ' DO NOT PROCEED'
                  CONTINUE
            END


            -- Add one row to ScheduledPackages
            DECLARE @scheduledPackageId int
            INSERT INTO BritishCouncil_TPM.[dbo].[ScheduledPackages]
                           ([CenterId]
                           ,[CenterRef]
                           ,[CenterName]
                           ,[QualificationId]
                           ,[QualificationRef]
                           ,[QualificationName]
                           ,[StartDate]
                           ,[EndDate]
                           ,[StartTime]
                           ,[EndTime]
                           ,[DateCreated]
                           ,[CreatedBy]
                           ,[StatusValue]
                           ,[PackageId]
                           ,[PackageDate])
                  SELECT TOP 1
                              CT.ID
                              ,CT.CentreID
                              ,CT.CentreName
                              ,Q.ID
                              ,Q.QualificationRef
                              ,Q.QualificationName
                              ,SEXT.ScheduledStartDateTime
                              ,SEXT.ScheduledEndDateTime
                              ,'00:01' AS [StartTime] -- TODO: check 
                              ,'23:59' AS [EndTime] -- TODO: check 
                              ,SEXT.CreatedDateTime
                              ,'aptismanagermorocco' AS [CreatedBy] -- TODO: check
                              ,0 AS [StatusValue] -- Editable 
                              ,@testPackageId AS [PackageId]
                              ,GETDATE() AS [PackageDate]
                        FROM @examSessions ES
                              JOIN WAREHOUSE_ExamSessionTable EST ON EST.ID = ES.id
                              JOIN WAREHOUSE_ScheduledExamsTable SEXT ON SEXT.ID = EST.WAREHOUSEScheduledExamID
                              JOIN WAREHOUSE_CentreTable CT ON CT.ID = SEXT.WAREHOUSECentreID
                              JOIN IB3QualificationLookup Q ON Q.ID = SEXT.qualificationId
                        WHERE ES.candidateId = @candidateId

            SELECT @scheduledPackageId = SCOPE_IDENTITY();

            -- Add one row to ScheduledPackageCandidates
            DECLARE @scheduledPackageCandidateId int
            INSERT INTO BritishCouncil_TPM.[dbo].[ScheduledPackageCandidates]
                           ([SurpassCandidateId]
                           ,[SurpassCandidateRef]
                           ,[FirstName]
                           ,[LastName]
                           ,[MiddleName]
                           ,[BirthDate]
                           ,[IsCompleted]
                           ,[DateCompleted]
                           ,[PackageScore]
                           ,[ScheduledPackage_ScheduledPackageId]
                           ,[IsVoided])
                  SELECT 
                               U.id
                              ,U.CandidateRef
                              ,U.Forename
                              ,U.Surname
                              ,U.Middlename
                              ,U.DOB
                              ,0 AS [IsCompleted]
                              ,NULL AS [DateCompleted]
                              ,NULL AS [PackageScore]
                              ,@scheduledPackageId AS [ScheduledPackage_ScheduledPackageId]
                              ,0 AS [IsVoided]
                        FROM WAREHOUSE_UserTable U
                        WHERE U.ID = @candidateId
            
            SELECT @scheduledPackageCandidateId = SCOPE_IDENTITY();

            -- ScheduledPackageCandidateExams - add as many rows as exams in the package
            
            INSERT INTO BritishCouncil_TPM.[dbo].[ScheduledPackageCandidateExams]
                           (
                           [ScheduledExamRef]
                           ,[PackageExamId]
                           ,[IsCompleted]
                           ,[DateCompleted]
                           ,[Score]
                           ,[Candidate_ScheduledPackageCandidateId]
                           ,[IsVoided]
                           ,[ShouldIncludeScale]
                           ,[ShouldIncludeCEFR]
                           ,[ShouldIncludeFinalScore]
                           ,[IsRescheduled]  --3DRAH401
                           )
                                                      
                      SELECT 
							EST.KeyCode
							   -- SEXT.ExamRef
							  ,/*PE.PackageExamId*/ (SELECT PackageExamId FROM BritishCouncil_TPM.[dbo].PackageExams PE WHERE PE.SurpassExamId = SEXT.ExamID AND PE.PackageId = @TestPackageId) AS PackageExamId
							  ,0 AS [IsCompleted]
							  ,NULL AS [DateCompleted]
							  ,NULL AS [Score]
							  ,@scheduledPackageCandidateId AS [Candidate_ScheduledPackageCandidateId]
							  ,0 AS [IsVoided]
							  ,/*PE.ShouldIncludeScale*/ (SELECT ShouldIncludeScale FROM BritishCouncil_TPM.[dbo].PackageExams PE WHERE PE.SurpassExamId = SEXT.ExamID AND PE.PackageId = @TestPackageId) AS ShouldIncludeScale
							  ,/*PE.ShouldIncludeCEFR*/ (SELECT ShouldIncludeCEFR FROM BritishCouncil_TPM.[dbo].PackageExams PE WHERE PE.SurpassExamId = SEXT.ExamID AND PE.PackageId = @TestPackageId) AS ShouldIncludeCEFR
							  ,/*PE.ShouldIncludeFinalScore*/ (SELECT ShouldIncludeFinalScore FROM BritishCouncil_TPM.[dbo].PackageExams PE WHERE PE.SurpassExamId = SEXT.ExamID AND PE.PackageId = @TestPackageId) AS ShouldIncludeFinalScore
							  ,0 AS [IsRescheduled]
						FROM @examSessions ES
                              JOIN WAREHOUSE_ExamSessionTable EST ON EST.ID = ES.id
                              JOIN WAREHOUSE_ScheduledExamsTable SEXT ON SEXT.ID = EST.WAREHOUSEScheduledExamID
                              --JOIN BritishCouncil_TPM.[dbo].PackageExams PE ON PE.SurpassExamId = SEXT.ExamID
                        WHERE ES.candidateId = @candidateId
						
      END
      
select * from BritishCouncil_TPM..ScheduledPackageCandidateExams as SPCE

	inner join BritishCouncil_TPM..ScheduledPackageCandidates as SPC
	on SPC.ScheduledPackageCandidateId =  SPCE.Candidate_ScheduledPackageCandidateId

where ScheduledExamRef in ('V2N6WJ01')

ROLLBACK TRAN
