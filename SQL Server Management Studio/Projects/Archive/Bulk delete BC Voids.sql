--Select last 6 months' worth of voids--
--Ensure they have a 1:1 relationship, as not to inadvertently delete schedules we don't want to delete--

DECLARE @examCounter int;

DECLARE @SchdulesToDelete TABLE 
	(
		ID int
		, warehouseTime datetime
	);
	
INSERT @SchdulesToDelete(ID, warehouseTime)	
SELECT  WSCET.ID, warehouseTime
FROM WAREHOUSE_ExamSessionTable as WEST (NOLOCK)

  Inner Join WAREHOUSE_ScheduledExamsTable as WSCET (NOLOCK)
  on WSCET.ID = WEST.WAREHOUSEScheduledExamID
  
 where PreviousExamState = 10 
	and warehouseTime <= DATEADD (month , -6 , GETDATE())
	
group by WSCET.ID, warehouseTime
HAVING count(WEST.ID) = 1
order by warehouseTime asc


SET @examCounter = (SELECT COUNT(WSCET.ID)
FROM WAREHOUSE_ExamSessionTable as WEST (NOLOCK)

  Inner Join WAREHOUSE_ScheduledExamsTable as WSCET (NOLOCK)
  on WSCET.ID = WEST.WAREHOUSEScheduledExamID
  
 where PreviousExamState = 10 
	and warehouseTime <= DATEADD (month , -6 , GETDATE())
)


WHILE @examCounter <= 100000
BEGIN

Delete top (500) from WAREHOUSE_ScheduledExamsTable
Where ID IN (Select ID FROM @SchdulesToDelete)

SET @examCounter+=500

	WAITFOR DELAY '00:10:00'
END
GO




--Delete top (250) from WAREHOUSE_ScheduledExamsTable
--Where ID IN (Select ID FROM @SchdulesToDelete)