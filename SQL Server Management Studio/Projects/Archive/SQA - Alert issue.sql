SELECT ProjectDefaults.*
, AlertDefinition.*
FROM
(

select 
   ID as ProjectID	
    ,a.b.value('@val','smallint') as Val
	,a.b.value('@id','smallint') as ID
	, a.b.value('(.)[1]', 'nvarchar(100)') as Status
from ProjectListTable 

cross apply ProjectDefaultXml.nodes('Defaults/StatusList/Status') a(b)

) ProjectDefaults

INNER JOIN (


select Projectid
	,c.d.value('@id','smallint') as ID
	,  c.d.value('@name','nvarchar(100)') as Status
from AlertDefinitionTable 

CROSS APPLY ItemStates.nodes('ItemStates/States/State') c(d)

) AlertDefinition

On ProjectDefaults.ProjectID = AlertDefinition.ProjectID
	and ProjectDefaults.Status = AlertDefinition.Status
	and ProjectDefaults.Val != AlertDefinition.ID	

where ProjectDefaults.projectID = 1014

--Need to change AlertDefinitionTable.ItemStates State id="3" to = StatusID = "6"