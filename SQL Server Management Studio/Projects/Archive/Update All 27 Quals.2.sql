--DECLARE IDs CURSOR FOR
--	select ID from IB3QualificationLookup
--	where ID IN  ('160','161','162','163','164','165','166','167','168','169','170','171','172','173','174','175','176','177','184','185','186','187','188','189','190','192')


--OPEN IDs;


--DECLARE @ID int;

--FETCH NEXT FROM IDs INTO @ID;

--WHILE @@FETCH_STATUS = 0
--BEGIN



DECLARE @QualificationID int = 160-- @ID --@ID


select CentreID
	into #OmitThese
	from CentreQualificationsTable
		where QualificationID = @QualificationID;
		
		
create table #DJTable
(
	CentreID int,
	QualificationID int
);


with cte  as (

SELECT distinct CentreID as 'CentreID'
  FROM [AAT_SecureAssess].[dbo].[CentreQualificationsTable]
      where CentreID not IN (select CentreID from #OmitThese)
)
    
--select * from cte

insert into #DJTable(CentreID, QualificationID)

SELECT distinct cte.CentreID
	    , @QualificationID
  from cte
  
--select * from #DJTable order by CentreID

--begin tran

merge into CentreQualificationsTable as TGT
using #DJTable as SRC
	on 1 = 2 -- TGT.CentreID = SRC.CentreID
when not matched then 
	insert (CentreID, QualificationID)
	Values (SRC.CentreID,  SRC.QualificationID);


--rollback

drop table #DJTable;
drop table #OmitThese;

--FETCH NEXT FROM IDs INTO @ID;
--END

--CLOSE IDs;
--DEALLOCATE IDs;
