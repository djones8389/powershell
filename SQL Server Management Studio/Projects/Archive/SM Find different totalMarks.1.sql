USE BritishCouncil_SecureAssess_LIVE

select top 10 WEST.ID, WEST.Keycode, WESTSI.ItemRef, WESTSI.TotalMark as 'SA TotalMark', I.TotalMark as 'SM TotalMark'
	from WAREHOUSE_ExamSessionTable as WEST
	
	inner join WAREHOUSE_ExamSessionTable_ShrededItems as WESTSI
	on WESTSI.examSessionId = WEST.ID

	inner join BritishCouncil_SecureMarker_LIVE..CandidateExamVersions as CEV
	on CEV.Keycode = WEST.KeyCode

	INNER JOIN BritishCouncil_SecureMarker_LIVE..CandidateResponses as CR
	ON CR.CandidateExamVersionID = CEV.ID

	INNER JOIN BritishCouncil_SecureMarker_LIVE..UniqueResponses as UR
	ON UR.ID = CR.UniqueResponseID 

	--inner join BritishCouncil_SecureMarker_LIVE..UniqueResponseDocuments as URD
	--ON URD.UniqueResponseID = UR.id 
		
	Inner Join BritishCouncil_SecureMarker_LIVE..Items as I
	on I.ID = ur.itemId and I.ExternalItemID = WESTSI.ItemRef

	inner join BritishCouncil_SecureMarker_LIVE..AssignedItemMarks as AIM
	on AIM.UniqueResponseId = UR.id


where ExportToSecureMarker = 1
	and WESTSI.TotalMark <> I.TotalMark
	and WESTSI.TotalMark <> 0
	order by WEST.ID DESC	
	
	