--DECLARE @DefaultLocation TABLE (Value nvarchar(MAX), Data nvarchar(MAX))
--INSERT @DefaultLocation(Value, Data)
--EXECUTE xp_instance_regread N'HKEY_LOCAL_MACHINE', N'Software\Microsoft\MSSQLServer\MSSQLServer',N'BackupDirectory'

--SELECT N'BACKUP DATABASE ['+name+N'] TO  DISK = N''' +  (SELECT DATA FROM @DefaultLocation) + '\' + name + '.bak'' WITH NAME = N'''+name+N'-Full Database Backup'', COPY_ONLY, NOFORMAT, NOINIT, SKIP, COMPRESSION;' 
--FROM sys.databases 
--where name like N'%[_][ACIS]%[^0-9]';



DECLARE @DefaultLocation TABLE (Value nvarchar(MAX), Data nvarchar(MAX))
INSERT @DefaultLocation(Value, Data)
EXECUTE xp_instance_regread N'HKEY_LOCAL_MACHINE', N'Software\Microsoft\MSSQLServer\MSSQLServer',N'BackupDirectory'

SELECT N'BACKUP DATABASE ['+name+N'] TO DISK = N''' +  (SELECT DATA FROM @DefaultLocation) + '\' + name + '_DB_' + CONVERT(VARCHAR(10), GETDATE(), 102) + '.bak'' WITH NAME = N'''+name+N'- Database Backup'', COPY_ONLY, NOFORMAT, NOINIT, SKIP, COMPRESSION, NOREWIND, NOUNLOAD, STATS = 10;'  
FROM sys.databases 
where recovery_model_desc = 'FULL'	
	and name = 'SecureAssess'
	
	UNION
	
SELECT N'BACKUP LOG ['+name+N'] TO DISK = N''' +  (SELECT DATA FROM @DefaultLocation) + '\' + name + '_LOG_' + CONVERT(VARCHAR(10), GETDATE(), 102) + '.bak'' WITH NAME = N'''+name+N'- Log Backup'', RECOVERY, NOREWIND, NOUNLOAD, SKIP, STATS = 10;'  
FROM sys.databases 
where recovery_model_desc = 'FULL'		
	and name = 'SecureAssess'
	
	
	
	

BACKUP DATABASE [SecureAssess] TO DISK = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.SADB\MSSQL\Backup\SecureAssess_DB_2015.05.18.bak' WITH NAME = N'SecureAssess- Database Backup', COPY_ONLY, NOFORMAT, NOINIT, SKIP, COMPRESSION, NOREWIND, NOUNLOAD, STATS = 10;
BACKUP LOG [SecureAssess] TO DISK = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.SADB\MSSQL\Backup\SecureAssess_LOG_2015.05.18.bak' WITH NAME = N'SecureAssess- Log Backup',  NOREWIND, NOUNLOAD, SKIP, STATS = 10;




