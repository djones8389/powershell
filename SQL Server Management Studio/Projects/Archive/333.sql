SELECT EST.ID, EST.KeyCode, EST.examState, UT.Forename, UT.Surname, CT.CentreName, SCET.examName, est.StructureXML
  FROM ExamSessionTable as EST

  Inner Join ScheduledExamsTable as SCET
  on SCET.ID = EST.ScheduledExamID
  
  Inner Join UserTable as UT
  on UT.ID = EST.UserID

  Inner Join CentreTable as CT
  on CT.ID = SCET.CentreID


	--inner join ExamSessionItemResponseTable as ESIRT
	--on ESIRT.ExamSessionID = EST.ID

where examState = 15
	and est.ID IN (
		select ExamSessionID
			from ExamSessionItemResponseTable
				where ItemResponseData.exist('/p/s/c[@ua="1"]') = 1
	)
	
	--and est.StructureXML.exist('assessmentDetails/assessment/section/item[@userMark="1"]') = 0
	
	order by est.ID desc
	
	
	select * from ExamStateChangeAuditTable where ExamSessionID  = 71160

