SELECT top 20 
	WEST.ID
	, west.ExamSessionID
    --, WEST.KeyCode
    --, WUT.CandidateRef
    , WSCET.examName
    , WEST.completionDate
	, WESTS.ExternalReference
	, AGT.AllowVersionRecycling
	, west.StructureXML
	, ExamStateChangeAuditXml
	, Track.*
	, Comp.*
	, west.StructureXML.query('data(/assessmentDetails/assessmentID)') as assessmentID
	, west.StructureXML.value('(/assessmentDetails/assessmentID)[1]', 'int')  as assessmentID2
FROM WAREHOUSE_ExamSessionTable as WEST

  Inner Join WAREHOUSE_ScheduledExamsTable as WSCET
  on WSCET.ID = WEST.WAREHOUSEScheduledExamID
  
  Inner Join WAREHOUSE_UserTable as WUT
  on WUT.ID = WEST.WAREHOUSEUserID

  Inner Join WAREHOUSE_CentreTable as WCT
  on WCT.ID = WSCET.WAREHOUSECentreID

  Inner Join WAREHOUSE_ExamSessionTable_Shreded as WESTS
  on WESTS.examSessionId = WEST.ID

  Inner Join UAT_BC_ItemBank..AssessmentTable as AT
  on AT.ID = wests.examVersionId

  Inner join UAT_BC_ItemBank..AssessmentGroupTable as AGT
  on AGT.ID = AT.AssessmentGroupID 

  inner join UserExamHistoryVersionTrackerTable as Track
  on Track.examSessionID = WEST.ExamSessionID
 
  inner join UserExamHistoryCompletionDateTrackerTable as Comp
  on Comp.examID = Track.examID and Comp.userID = Track.userID
  
--where west.KeyCode in ('G35887B8','L8N23KB8')
--where west.ID in (65906,95667)

--where west.ID in (646930,646351,645731,645629,642987,639290,639288,639285,639273,639175,639168,639160,639156,639152,639147,639143,639138,639134,639123,639121)
where west.StructureXML.value('(/assessmentDetails/assessmentID)[1]', 'int') <> Track.versionID
order by west.id desc;