declare @MyTable table (UserID int, CandidateRef nvarchar(100), ExamID int,  ExamVersionID int)
insert into @MyTable
SELECT  U.UserID
		,u.CandidateRef
        ,S.ExamID
        ,S.ExamVersionID
        --,E.ExamSessionID
        --, AGT.AllowVersionRecycling
        --, AGT.MinResitTimeInDays
FROM  WAREHOUSE_ExamSessionTable E
	INNER JOIN WAREHOUSE_ScheduledExamsTable S
	ON E.WAREHOUSEScheduledExamID = S.ID
	INNER JOIN WAREHOUSE_UserTable U
	ON E.WAREHOUSEUserID = U.ID
	Inner Join [OCR_ItemBank_11.2]..AssessmentTable as AT
	on AT.ID = examVersionId
	Inner join [OCR_ItemBank_11.2]..AssessmentGroupTable as AGT
	on AGT.ID = AT.AssessmentGroupID
	
WHERE E.ExamSessionID  IN
                (
                SELECT  ExamSessionID
                FROM  UserExamHistoryVersionTrackerTable
                )
AND	E.ExamStateChangeAuditXML.exist('/exam/stateChange[newStateID = 6]') = 1	--Where exam has definitely been presented to a candidate--
and AGT.AllowVersionRecycling = 0	--Where IB does not allow duplicates to be sat--
and E.warehouseTime > '20 Feb 2014' --The date on which I started the job running again--
and U.CandidateRef <> ''			--Omit Users--
	group by userID, S.ExamVersionID, CandidateRef,S.ExamID
		having COUNT(*) > 1 
		
order by UserId


select wests.structureXml, wests.candidateRef, wests.structureXml.query('data(//assessmentGroupID)[1]') as assessmentGroupID
	, examVersionId, wests.warehouseTime, west.completionDate, west.ExamSessionID, ExamStateChangeAuditXml
	from WAREHOUSE_ExamSessionTable_Shreded as wests
	
	inner join WAREHOUSE_ExamSessionTable as west
	on west.ID = wests.examSessionId
		
	inner join UserTable as ut
	on ut.CandidateRef = wests.candidateRef		
	
where wests.candidateRef in (Select candidateRef from @MyTable)--'006fa234ccc394e5b8f378d8ca911e661' 
	and examVersionId in (Select ExamVersionID from @MyTable)
	and ut.ID in (Select UserID from @MyTable)
	and examVersionId in (Select ExamVersionID from @MyTable)
	order by wests.CandidateRef, wests.examVersionId