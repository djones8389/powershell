/*
UserID	CandidateRef	ExamVersionID
2673	006fa234ccc394e5b8f378d8ca911e661	2247
*/



declare @userid int = 19766
declare @examversionid int = 1482
declare @candidateRef nvarchar(100) = 'AUTOd619af7aefc8455f89064a1c963677cd'

select wests.structureXml, wests.structureXml.query('data(//assessmentGroupID)[1]') as assessmentGroupID, examVersionId, wests.warehouseTime
	, west.completionDate, west.ExamSessionID, ExamStateChangeAuditXml, ut.ID
	from WAREHOUSE_ExamSessionTable_Shreded as wests
	
	inner join WAREHOUSE_ExamSessionTable as west
	on west.ID = wests.examSessionId
		
	inner join UserTable as ut
	on ut.CandidateRef = wests.candidateRef		
	
where wests.candidateRef = @candidateRef --'006fa234ccc394e5b8f378d8ca911e661' 
	and examVersionId = @examversionid
	and ut.ID = @userid;
			

select * from UserExamHistoryCompletionDateTrackerTable where userID = 	@userid and examID = 225;
select * from UserExamHistoryVersionTrackerTable where userID = @userid and VersionId = @examversionid;