declare @ESID int = 68961;

create table #Marks
(
	ID int identity(1,1) 
	,resultData xml
	,itemID nvarchar(50)
	,CurrentTotalMark decimal
	,NewTotalMark float
	, ESID int
);

insert into #Marks(resultData, itemID, CurrentTotalMark, NewTotalMark, ESID)
select 
	resultData
	,s.e.value('@id', 'nvarchar(50)') as ItemID
	,0 as CurrentTotalMark
	,s.e.value('sum(mark/@mark)', 'float') as NewTotalMark
	, @ESID
from WAREHOUSE_ExamSessionTable
cross apply resultData.nodes('exam/section/item') s(e)
where ID = @ESID;


DECLARE @UpdatedMarks Table (
	 resultData xml
	,itemID nvarchar(40)
	, CurrentTotalMark decimal
	, NewTotalMarkD decimal
	, SumTotal decimal
);

insert into @UpdatedMarks(resultData, itemID, CurrentTotalMark, NewTotalMarkD, SumTotal)
select 
	resultData
	,s.e.value('@id', 'nvarchar(50)')
	,0 
	,s.e.value('sum(mark/@mark)', 'float') 
	,s.e.value('sum(//@mark)', 'float') 
	--into #Marks
from WAREHOUSE_ExamSessionTable
cross apply resultData.nodes('exam/section/item') s(e)
where ID = @ESID;

select * from @UpdatedMarks

update #Marks
SET ResultData.modify('replace value of (/exam/section/item[1]/@userMark)[1] with sql:column("NewTotalMarkD")')
from #Marks
inner join @UpdatedMarks A
on A.itemId = #Marks.itemid
 and ID = 1; 
 
update #Marks
set resultData = (select resultData from #Marks where id = 1)
where ID = 2;

update #Marks
SET ResultData.modify('replace value of (/exam/section/item[2]/@userMark)[1] with sql:column("NewTotalMarkD")')
from #Marks
inner join @UpdatedMarks A
on A.itemId = #Marks.itemid
 and ID = 2;

update #Marks
set resultData = (select resultData from #Marks where id = 2)
where ID = 3;

update #Marks
SET ResultData.modify('replace value of (/exam/section/item[3]/@userMark)[1] with sql:column("NewTotalMarkD")')
from #Marks
inner join @UpdatedMarks A
on A.itemId = #Marks.itemid
 and ID = 3;


update #Marks
set ResultData.modify('replace value of (/exam/@userMark)[1] with sql:column("SumTotal")')
from #Marks
inner join @UpdatedMarks A
on A.itemId = #Marks.itemid
 and ID = 3;
 
update #Marks
set ResultData.modify('replace value of (/exam/section/@userMark)[1] with sql:column("SumTotal")')
from #Marks
inner join @UpdatedMarks A
on A.itemId = #Marks.itemid
 and ID = 3;
 
 
DECLARE @ExamUserPercentage [decimal] (6, 3);

SELECT @ExamUserPercentage = CAST(((#Marks.resultdata.value('(/exam/@userMark)[1]', 'decimal(13, 10)') 
											/ #Marks.resultData.value('(/exam/@totalMark)[1]', 'decimal(13, 10)')) * 100) AS [decimal](6, 3))
	FROM #Marks
where ID = 3;



UPDATE #Marks
SET resultdata.modify('replace value of (/exam/@userPercentage)[1] with sql:variable("@ExamUserPercentage")')
where ID = 3;

UPDATE #Marks
SET resultdata.modify('replace value of (/exam/section/@userPercentage)[1] with sql:variable("@ExamUserPercentage")')
where ID = 3;

delete from #Marks where ID IN (1,2);

select resultData from WAREHOUSE_ExamSessionTable where ID = @ESID;
select resultData from WAREHOUSE_ExamSessionTable_Shreded where examSessionId = @ESID;

update WAREHOUSE_ExamSessionTable
set resultData = b.resultData
from WAREHOUSE_ExamSessionTable a
inner join #Marks b
on b.ESID = a.ID
where a.ID = @ESID;


update WAREHOUSE_ExamSessionTable
set resultData = b.resultData
from WAREHOUSE_ExamSessionTable_Shreded a
inner join #Marks b
on b.ESID = a.examSessionId
where a.examSessionId = @ESID;

select top 100 resultData from WAREHOUSE_ExamSessionTable-- where ID = @ESID;
select top 100 resultData from WAREHOUSE_ExamSessionTable_Shreded --where examSessionId = @ESID;


drop table #Marks;


select top 100 resultData from WAREHOUSE_ExamSessionTable where ID = 68961
/*

(3 row(s) affected)

(3 row(s) affected)

(3 row(s) affected)

(1 row(s) affected)

(1 row(s) affected)

(1 row(s) affected)

(1 row(s) affected)

(1 row(s) affected)

(1 row(s) affected)

(1 row(s) affected)

(1 row(s) affected)

(1 row(s) affected)

(2 row(s) affected)

(1 row(s) affected)

(1 row(s) affected)

(1 row(s) affected)

(76632 row(s) affected)

(1 row(s) affected)

(1 row(s) affected)

*/