SELECT WEST.ExamSessionID, WEST.KeyCode, WUT.Forename, WUT.Surname, WUT.CandidateRef,  WSCET.examName, WESTS.ExternalReference
	, WEST.completionDate, ExamID, wests.examVersionId
	, ExamStateChangeAuditXml.query('data(//stateChange[newStateID=1]/changeDate)[1]') as State_1  
	, ExamStateChangeAuditXml.query('data(//stateChange[newStateID=6]/changeDate)[1]') as State_6  
	, ExamStateChangeAuditXml.query('data(//stateChange[newStateID=9]/changeDate)[1]') as State_9 
	, ExamStateChangeAuditXml 
FROM WAREHOUSE_ExamSessionTable as WEST

  Inner Join WAREHOUSE_ScheduledExamsTable as WSCET
  on WSCET.ID = WEST.WAREHOUSEScheduledExamID
  
  Inner Join WAREHOUSE_UserTable as WUT
  on WUT.ID = WEST.WAREHOUSEUserID

  Inner Join WAREHOUSE_CentreTable as WCT
  on WCT.ID = WSCET.WAREHOUSECentreID

  Inner Join WAREHOUSE_ExamSessionTable_Shreded as WESTS
  on WESTS.examSessionId = WEST.ID

where wests.candidateRef = 'AUTOd619af7aefc8455f89064a1c963677cd'
	and wests.examVersionId in (1482)
--wests.KeyCode in ('XLHP29B8','PRPPZNB8')
	order by west.ID
	
Select * from UserExamHistoryCompletionDateTrackerTable where userID = 19766 and examID in (225)
Select UserExamHistoryVersionTrackerTable.*, KeyCode from UserExamHistoryVersionTrackerTable inner join WAREHOUSE_ExamSessionTable on WAREHOUSE_ExamSessionTable.ExamSessionID = UserExamHistoryVersionTrackerTable.examSessionID  where userID = 19766 and examID in (225) and VersionId in (1482)