--select west.ID, west.KeyCode, wests.Forename, wests.Surname, wests.CandidateRef 
--	, qualificationName, examName, examVersionName, ExternalReference
--		, examID, examVersionId, completionDate, ExamStateChangeAuditXml
--from UserExamHistoryVersionTrackerTable as UTT

--inner join WAREHOUSE_ExamSessionTable as west
--on west.ID = utt.examSessionID

--inner join WAREHOUSE_ExamSessionTable_Shreded as wests
--on wests.examSessionId = west.ID


--where wests.CandidateRef in  ('AUTOd619af7aefc8455f89064a1c963677cd')
--	order by examName asc;
/*
<exam>
  <stateChange>
    <newStateID>1</newStateID>
    <newState>Exam Scheduled (but not created)</newState>
    <changeDate>12 Aug 2014 14:06:39:22</changeDate>
  </stateChange>
  <stateChange>
  */

select west.ID, west.KeyCode, wests.Forename, wests.Surname, wests.CandidateRef 
	, wests.qualificationName, wests.examName, examVersionName, wests.ExternalReference
		, examID, wests.examVersionId, completionDate, ExamStateChangeAuditXml.query('data(//stateChange[newStateID=1]/changeDate)[1]') as State_1
			 , wscet.WAREHOUSECreatedBy, CreatedDateTime
from WAREHOUSE_ExamSessionTable_Shreded as wests

inner join WAREHOUSE_ExamSessionTable as west
on west.ID = wests.examSessionID

inner join WAREHOUSE_ScheduledExamsTable as wscet
on wscet.ID = west.WAREHOUSEScheduledExamID

where WAREHOUSECreatedBy = 13672-- wests.CandidateRef in  ('AUTO26bfcc705f1b4a369bfcf56f2965b11a')
	--order by examID asc;

select * from WAREHOUSE_UserTable where ID = 13672 or ID = 1575

select * from AssignedUserRolesTable where UserID = 13672


select distinct UCT.userID, uct.examID, uct.completionDate, wests.qualificationName, wests.examName, examVersionName, wests.ExternalReference
		, wscet.examID, wests.examVersionId
from UserExamHistoryCompletionDateTrackerTable as UCT

	inner join UserTable as UT
	 on UT.ID = UCT.userID
	
	inner join WAREHOUSE_ExamSessionTable_Shreded as wests
	on wests.candidateRef = UT.CandidateRef
	
	inner join WAREHOUSE_ExamSessionTable as west
	on west.ID = wests.examSessionId	
	
	inner join WAREHOUSE_ScheduledExamsTable as wscet
	on wscet.ID = west.WAREHOUSEScheduledExamID

where UT.CandidateRef in  ('AUTO26bfcc705f1b4a369bfcf56f2965b11a')
	and uct.examID = wscet.examID
	order by uct.examID;
		