SELECT ID
      ,AssessmentName
      ,CommentsHistory--c.t.value('(.)[1]', 'nvarchar(max)') AS [CommentHistoryCommentText]
            
FROM AssessmentTable
CROSS APPLY CommentsHistory.nodes('/commentHistory/comment/text') c(t)
WHERE CommentsHistory IS NOT NULL
AND c.t.value('(.)[1]', 'nvarchar(max)') LIKE N'%&%'


UPDATE AssessmentTable
SET commentshistory = REPLACE(CAST(commentshistory AS NVARCHAR(MAX)), N'&amp;', N'and')
WHERE CAST(commentshistory AS NVARCHAR(MAX)) LIKE N'%&amp;%'