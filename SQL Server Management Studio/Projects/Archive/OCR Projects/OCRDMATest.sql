-- Query to pull out required data for Unnamed Marksheet export

-- 3 parameters that calling user will pass in
DECLARE @BatchName nvarchar(10)
SET @BatchName = 'FSUM'
DECLARE @SessionId nvarchar(10)
SET @SessionId = 'ONDEMAND'
DECLARE @SessionYear nvarchar(10)
SET @SessionYear = '0000';

WITH temp AS (
	SELECT 
		row_number() over (partition by [ExamSessionTable].[ID] ORDER BY [ExamSessionTable].[ID]) as rownum
		,[ExamSessionTable].[ID] AS 'ExamSessionId'
		,[CentreTable].CentreCode AS 'CentreCode'
		,[CentreTable].CentreName AS 'CentreName'
		,[ScheduledExamsTable].examVersionRef AS 'ExamVersionRef'
		,[ScheduledExamsTable].examVersionName AS 'ExamVersionName'
		,[ExamSessionItemResponseTable].MarkerResponseData.query('//userId[1]/text()').value('.', 'int') AS ExaminerUserId
		,REPLACE(CONVERT(varchar(8),[UserTable].DOB,3), '/', '') AS CandidateDOB
		,[ExamSessionTable].ResultData.query('data(/exam/@userMark)').value('.', 'varchar(3)') as TotalMark
		,[ExamSessionTable].ResultData
		,[UserTable].Forename
		,[UserTable].Surname
		,SUBSTRING(Usertable.Forename, 1, 1) + ISNULL(SUBSTRING(Usertable.Middlename, 1, 1),'') + SUBSTRING(Usertable.Surname, 1, 1) AS Initials
		,[UserTable].Gender
		,REPLACE(CONVERT(varchar(8),[ExamStateChangeAuditTable].StateChangeDate,3), '/', '') AS TestDate
		,[UserTable].ULN AS ULN
		, StateChangeDate
		, NewState
		FROM [ExamSessionTable]
			INNER JOIN [ScheduledExamsTable]
				ON ScheduledExamsTable.ID = [ExamSessionTable].ScheduledExamID
			INNER JOIN CentreTable
				ON CentreTable.ID = [ScheduledExamsTable].CentreID
			INNER JOIN ExamSessionItemResponseTable
				ON ExamSessionItemResponseTable.ExamSessionID = [ExamSessionTable].ID
				AND ExamSessionItemResponseTable.ItemID = [ExamSessionTable].resultData.query('data(//item[1]/@id)').value('.', 'varchar(15)')
			INNER JOIN UserTable
				ON UserTable.ID = ExamSessionTable.UserID
			INNER JOIN [ExamStateChangeAuditTable]
				ON ExamStateChangeAuditTable.ExamSessionId = ExamSessionTable.Id
				AND ExamStateChangeAuditTable.NewState = 9
							
		WHERE
			[examState] = 16
			AND
			[ScheduledExamsTable].examVersionRef LIKE @BatchName + '_' + @SessionId + '_' + @SessionYear + '_%'
			and
			examsessiontable.Id in ('86067','82999','82509','87749','87026','86680','86969','86831','84960','83295','86890','86118','88147','86826','86688','83434','83206','83575','82147','82889','82501','85140','82323','87751','83440','86073','82417','87027','86687','83173','83576','86825','86691','86677','88146','85340','88190','87043','82455','86673','82329','82831','82927','83143','83360','82130','86489','88312','83000','82295','83146','86493','86967','88388','86968','88152','84973','83478','86068','86679','83320','87750','86671','84947','87754','84534','83480','88116','85072','86670','88245','87982','87764','84943','85212','86692','86895','82668','87744','86689','86123','83476','83477','84130','85059','88647','87745','88145','85073','83147','88153','86926','82553','87741','86945','83137','84308','82285','82430','86066','87742','82371','84165','87168','86071','88120','88104','87746','88108','84535','87756','84429','88187','88643','87103','88114','82427','87969','89420','88144','85339','85203','88135','82146','86119','82294','87086','82362','88156','86678','86115','82131','85094','84366','87761','86894','84941','83323','82998','89419','88164','84536','85007','84128','87357','87762','86116','87763','83437','87748','88138','87759','86065','87747','85202','86683','88122','88539','83016','82428','87337','86120','84934','84942','82286','82372','85716','86070','82630','85095','83577','87752')
	)

SELECT TOP 1000 

temp.ExamSessionId,
temp.CentreCode,
temp.CentreName,
temp.ExamVersionRef,
temp.ExamVersionName,
ISNULL(SUBSTRING(UserTable.Username,3,4), '0000') AS ExaminerNo,
temp.CandidateDOB,
temp.TotalMark,
temp.resultData,
temp.Forename, 
temp.Surname,
temp.Initials,
temp.Gender,
temp.TestDate, 
temp.ULN,
temp.StateChangeDate,
temp.NewState
 
FROM temp
	LEFT JOIN UserTable
ON UserTable.id = temp.ExaminerUserId 
WHERE rownum = 1
ORDER BY CentreCode, ExamVersionRef





/*

DECLARE @BatchName nvarchar(10)
SET @BatchName = 'PQUM'
DECLARE @SessionId nvarchar(10)
SET @SessionId = '00CBT'
DECLARE @SessionYear nvarchar(10)
SET @SessionYear = '2013';

select --distinct examVersionRef 
top 10 *
from ScheduledExamsTable 

	inner join ExamSessionTable on ExamSessionTable.ScheduledExamID = ScheduledExamsTable.ID
	--inner join ExamStateChangeAuditTable on ExamStateChangeAuditTable.ExamSessionID = ExamSessionTable.ID

where examVersionRef  LIKE '%Practice%'  --= 'FSUM_00CBT_2012_m_09498_4/Practice'
	and examState = 13
	--and StateChangeDate like '%2012-08-20%'
	order by StateChangeDate
	*/
	
	
	
	--ESHNJAB8
	
	/*
declare @Keycode nvarchar(10) = '6VUCCSB8'--'ESHNJAB8'

SELECT EST.ID, scet.id ,EST.KeyCode, EST.examState, UT.Forename, UT.Surname, CT.CentreName, SCET.examName, examVersionRef
  FROM ExamSessionTable as EST

  Inner Join ScheduledExamsTable as SCET
  on SCET.ID = EST.ScheduledExamID
  
  Inner Join UserTable as UT
  on UT.ID = EST.UserID

  Inner Join CentreTable as CT
  on CT.ID = SCET.CentreID

where Keycode = @Keycode;

update ScheduledExamsTable
set examVersionRef = 'IanP_Annual_2013_1234_m_05755_2_15CBT'
where ID = 57347
--Installation_Test_01/Practice--

--update ExamStateChangeAuditTable 
--set StateChangeDate = '2014-11-28 09:00:09.270'
--where ExamSessionID = 70764 and NewState = 9
--2014-06-12 09:00:09.270--

--select * from ExamStateChangeAuditTable where ExamSessionID = 70764
_m_05755_2_15CBT



SELECT EST.ID, scet.id ,EST.KeyCode, EST.examState, UT.Forename, UT.Surname, CT.CentreName, SCET.examName, examVersionRef
  FROM ExamSessionTable as EST

  Inner Join ScheduledExamsTable as SCET
  on SCET.ID = EST.ScheduledExamID
  
  Inner Join UserTable as UT
  on UT.ID = EST.UserID

  Inner Join CentreTable as CT
  on CT.ID = SCET.CentreID
  
   --where EST.id IN (6133,6134)
  Where KeyCode = '6VUCCSB8' 
  
--needs to be state 16 and in this format   PQUM_ONDEMAND_0000_m_05755_2_15CBT --  

update ExamSessionTable
set examState = 16
where id = 93461

update ScheduledExamsTable
set examVersionRef = 'IanP_Annual_2013_1234_m_05755_2_15CBT'
where ID = 78245
*/