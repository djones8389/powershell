USE OcrDataManagement

exec sp_executesql N'SELECT 
[Extent1].[ExportId] AS [ExportId], 
[Extent1].[ExamSessionId] AS [ExamSessionId]
FROM  [dbo].[ExportDetails] AS [Extent1]
INNER JOIN [dbo].[Exports] AS [Extent2] ON [Extent1].[ExportId] = [Extent2].[ExportId]
WHERE ([Extent2].[BatchName] = @p__linq__0) AND ([Extent2].[SessionId] = @p__linq__1) AND ([Extent2].[SessionYear] = @p__linq__2) AND ([Extent2].[SessionStatus] = @p__linq__3)',N'@p__linq__0 nvarchar(4000),@p__linq__1 nvarchar(4000),@p__linq__2 nvarchar(4000),@p__linq__3 int',@p__linq__0=N'IanP',@p__linq__1=N'Annual',@p__linq__2=N'2013',@p__linq__3=16

SELECT TOP 50 *
  FROM Exports
  
	inner join ExportDetails
	on ExportDetails.ExportId = Exports.ExportId
where exports.ExportId = 58


USE OCR_SecureAssess_11_2
select * from ExamSessionTable where ID IN (6133,6134)



/*

58	IanP	Annual	2013	16	2013-05-05 13:10:27.090	UnnamedMarksheet_IanP_Annual_2013_2013-05-05 131027.dat	1900-01-01 00:00:00.000	58	6133
58	IanP	Annual	2013	16	2013-05-05 13:10:27.090	UnnamedMarksheet_IanP_Annual_2013_2013-05-05 131027.dat	1900-01-01 00:00:00.000	58	6134



begin tran

select * from  Exports where ExportId = 58

update Exports
set Timestamp = '2014-11-05 13:10:27.090' --RecentWarehouseTime = '1900-01-01 00:00:00.000'
where ExportId = 58

select * from  Exports where ExportId = 58

rollback



begin tran

select * from ExportDetails where examsessionid in ('86067','82999','82509','87749','87026','86680','86969','86831','84960','83295','86890','86118','88147','86826','86688','83434','83206','83575','82147','82889','82501','85140','82323','87751','83440','86073','82417','87027','86687','83173','83576','86825','86691','86677','88146','85340','88190','87043','82455','86673','82329','82831','82927','83143','83360','82130','86489','88312','83000','82295','83146','86493','86967','88388','86968','88152','84973','83478','86068','86679','83320','87750','86671','84947','87754','84534','83480','88116','85072','86670','88245','87982','87764','84943','85212','86692','86895','82668','87744','86689','86123','83476','83477','84130','85059','88647','87745','88145','85073','83147','88153','86926','82553','87741','86945','83137','84308','82285','82430','86066','87742','82371','84165','87168','86071','88120','88104','87746','88108','84535','87756','84429','88187','88643','87103','88114','82427','87969','89420','88144','85339','85203','88135','82146','86119','82294','87086','82362','88156','86678','86115','82131','85094','84366','87761','86894','84941','83323','82998','89419','88164','84536','85007','84128','87357','87762','86116','87763','83437','87748','88138','87759','86065','87747','85202','86683','88122','88539','83016','82428','87337','86120','84934','84942','82286','82372','85716','86070','82630','85095','83577','87752')

update ExportDetails 
set examsessionID = examSessionID * 2
where ExportId = 58

select * from ExportDetails where ExportId = 58

update ExportDetails 
set examsessionID = examSessionID / 2
where ExportId = 58

select * from ExportDetails where ExportId = 58

rollback

*/