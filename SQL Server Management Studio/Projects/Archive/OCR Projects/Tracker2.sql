--INSERT INTO UserExamHistoryVersionTrackerTable

SELECT		 U.UserID
			,S.ExamID
			,S.ExamVersionID
			,E.ExamSessionID
	 FROM	 WAREHOUSE_ExamSessionTable E
	 INNER JOIN WAREHOUSE_ScheduledExamsTable S
			 ON E.WAREHOUSEScheduledExamID = S.ID
	 INNER JOIN WAREHOUSE_UserTable U
			 ON E.WAREHOUSEUserID = U.ID
	WHERE	 E.ExamSessionID NOT IN
			(
			SELECT	 ExamSessionID
			 FROM	 UserExamHistoryVersionTrackerTable
			)
	 AND	 E.ExamStateChangeAuditXML.exist('/exam/stateChange[newStateID = 6]') = 1;
	 
	 
--INSERT INTO UserExamHistoryVersionTrackerTable

SELECT		U.ID as UserID
			,S.ExamID
			,S.ExamVersionID
			,E.ID as ExamSessionID
	 FROM	 ExamSessionTable E
	 INNER JOIN ScheduledExamsTable S
			 ON E.ScheduledExamID = S.ID
	 INNER JOIN UserTable U
			 ON E.UserID = U.ID
	WHERE	 E.ID NOT IN
			(
			SELECT	 ExamSessionID
			 FROM	 UserExamHistoryVersionTrackerTable
			)
AND E.examState !=10 AND E.previousExamState !=10;