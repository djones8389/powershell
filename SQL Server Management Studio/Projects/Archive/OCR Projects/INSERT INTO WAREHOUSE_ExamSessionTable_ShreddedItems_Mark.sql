	BEGIN TRANSACTION;
		INSERT INTO WAREHOUSE_ExamSessionTable_ShreddedItems_Mark
		(	ExamSessionID
		   ,ItemID
		   ,Mark
		   ,LearningOutcome
		   ,DisplayText
		   ,MaxMark)
			SELECT  WAREHOUSE_ExamSessionTable.ID AS [ExamSessionID]
				   ,Result.Item.value('@id', 'varchar(15)') AS [ItemID]
				   ,Item.Mark.value('@mark', 'decimal(6, 3)') AS [Mark]
				   ,Item.Mark.value('@learningOutcome', 'int') AS [LearningOutcome]
				   ,Item.Mark.value('@displayName', 'nvarchar(200)') AS [DisplayName]
				   ,Item.Mark.value('@maxMark', 'decimal(6, 3)') AS [MaxMark]
			FROM   WAREHOUSE_ExamSessionTable
			CROSS APPLY WAREHOUSE_ExamSessionTable.ResultData.nodes('exam/section/item') Result(Item)
			CROSS APPLY Result.Item.nodes('mark') Item(Mark)
			WHERE   WAREHOUSE_ExamSessionTable.ID = 76308;
	COMMIT TRANSACTION; 