select *, ID, resultData, resultDataFull from WAREHOUSE_ExamSessionTable where id in (33579,33581)
select  examSessionId, resultData, userMark, userPercentage,grade, * from WAREHOUSE_ExamSessionTable_Shreded where candidateRef = '0c7484875be7d41dbb4f235aca122e03d' and ExternalReference in ('FSUM_33CBT_2013_m_09498_5','FSUM_33CBT_2013_m_09865_2')-- examSessionid in (33579,33581)

declare @NewValue nvarchar(20) = 'Pass'
--declare @esid int = 22349
declare @passValue int = 1
declare @originalPassValue int = 1
declare @originalCloseValue int = 0
declare @closeValue int = 0
declare @closeBoundaryValue int = 0

--WEST.ResultData--

update WAREHOUSE_ExamSessionTable
set resultData.modify('replace value of (/exam/@grade)[1] with sql:variable("@NewValue") ')    
where id in (33579,33581)

update WAREHOUSE_ExamSessionTable
set resultData.modify('replace value of (/exam/@originalGrade)[1] with sql:variable("@NewValue") ')    
where id in (33579,33581)

update WAREHOUSE_ExamSessionTable
set resultData.modify('replace value of (/exam/@passValue)[1] with sql:variable("@passValue") ')    
where id in (33579,33581)

update WAREHOUSE_ExamSessionTable
set resultData.modify('replace value of (/exam/@closeValue)[1] with sql:variable("@closeValue") ')    
where id in (33579,33581)

update WAREHOUSE_ExamSessionTable
set resultData.modify('replace value of (/exam/@originalCloseValue)[1] with sql:variable("@originalCloseValue") ')    
where id in (33579,33581)

--WESTS.ResultData--

update WAREHOUSE_ExamSessionTable_Shreded
set resultData.modify('replace value of (/exam/@grade)[1] with sql:variable("@NewValue") ')    
where examSessionid in (33579,33581)

update WAREHOUSE_ExamSessionTable_Shreded
set resultData.modify('replace value of (/exam/@originalGrade)[1] with sql:variable("@NewValue") ')    
where examSessionid in (33579,33581)

update WAREHOUSE_ExamSessionTable_Shreded
set resultData.modify('replace value of (/exam/@passValue)[1] with sql:variable("@passValue") ')    
where examSessionid in (33579,33581)

update WAREHOUSE_ExamSessionTable_Shreded
set resultData.modify('replace value of (/exam/@closeValue)[1] with sql:variable("@closeValue") ')    
where examSessionid in (33579,33581)

update WAREHOUSE_ExamSessionTable_Shreded
set resultData.modify('replace value of (/exam/@originalCloseValue)[1] with sql:variable("@originalCloseValue") ')    
where examSessionid in (33579,33581)


--WEST.ResultDataFull--

update WAREHOUSE_ExamSessionTable
set resultDataFull.modify('replace value of (/assessmentDetails/assessment/@originalGrade)[1] with sql:variable("@NewValue") ')    
where id in (33579,33581)

update WAREHOUSE_ExamSessionTable
set resultDataFull.modify('replace value of (/assessmentDetails/assessment/@passValue)[1] with sql:variable("@passValue") ')    
where id in (33579,33581)

select ID, resultData, resultDataFull from WAREHOUSE_ExamSessionTable where id in (33579,33581)
select  examSessionId, resultData, userMark, userPercentage,grade, * from WAREHOUSE_ExamSessionTable_Shreded where candidateRef = '0c7484875be7d41dbb4f235aca122e03d' and ExternalReference in ('FSUM_33CBT_2013_m_09498_5','FSUM_33CBT_2013_m_09865_2')-- examSessionid in (33579,33581)
