
SELECT EST.ID, EST.KeyCode, EST.examState, UT.Forename, UT.Surname, CT.CentreName, SCET.examName, examVersionRef
  FROM ExamSessionTable as EST

  Inner Join ScheduledExamsTable as SCET
  on SCET.ID = EST.ScheduledExamID
  
  Inner Join UserTable as UT
  on UT.ID = EST.UserID

  Inner Join CentreTable as CT
  on CT.ID = SCET.CentreID

where KeyCode in ('ZWNB5BB8','MAL7S4B8','8EPMP6B8','7TFFEMB8')

/*


Keycode - ZWNB5BB8
--------------------------------------------------------------------------------
Qualification - CPC for Transport Managers (Road Haulage) Level 3

Candidate - 14840719

Keycode - MAL7S4B8
--------------------------------------------------------------------------------
Qualification - CPC for Transport Managers (Road Haulage) Level 3

Candidate - 14840717

Keycode - 8EPMP6B8
--------------------------------------------------------------------------------
Qualification - CPC for Transport Managers (Road Haulage) Level 3

Candidate - 14840718

Keycode - 7TFFEMB8
----------------------------------------------*/