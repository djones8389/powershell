--SELECT EST.ID, EST.KeyCode, EST.examState, UT.Forename, UT.Surname, CT.CentreName, SCET.examName
--	, qualificationId, ExamID, est.UserID
--  FROM ExamSessionTable as EST

--  Inner Join ScheduledExamsTable as SCET
--  on SCET.ID = EST.ScheduledExamID
  
--  Inner Join UserTable as UT
--  on UT.ID = EST.UserID

--  Inner Join CentreTable as CT
--  on CT.ID = SCET.CentreID

--where candidateRef = 'AUTO068f011c468b414782110919770ab629' and examState <> 13
--	order by qualificationid, examid;


SELECT WEST.ID, WEST.KeyCode, wests.Forename, wests.Surname, wests.CandidateRef, WCT.CentreName, wests.qualificationName, WSCET.examName
	, WEST.completionDate, WESTS.ExternalReference, CreatedDateTime, west.ExamStateChangeAuditXml
	 , west.StructureXML, Wut.Forename + '' + wut.Surname, wests.qualificationId, ExamID, userMark, userPercentage
FROM WAREHOUSE_ExamSessionTable as WEST

  Inner Join WAREHOUSE_ScheduledExamsTable as WSCET
  on WSCET.ID = WEST.WAREHOUSEScheduledExamID
		
  Inner Join WAREHOUSE_UserTable as WUT
  on WUT.ID = WSCET.WAREHOUSECreatedBy
  		
  Inner Join WAREHOUSE_CentreTable as WCT
  on WCT.ID = WSCET.WAREHOUSECentreID

  Inner Join WAREHOUSE_ExamSessionTable_Shreded as WESTS
  on WESTS.examSessionId = WEST.ID

where west.ID IN (83060,83075)--KeyCode in ('rqfcchb8','4b3rueb8')
	--or wests.candidateRef = 'AUTO068f011c468b414782110919770ab629'
	order by wests.qualificationId, examid;	
	
--2014-11-07 00:00:00.000
--2014-11-11 00:00:00.000
--where CreatedDateTime = '2014-11-11 00:00:00.000' and wests.qualificationid = 161 and ExamID = 119--
--select * from WAREHOUSE_UserTable where ID = 37292



select UserExamHistoryVersionTrackerTable.*, QualificationName,AssessmentName, KeyCode, warehouseTime, completionDate, ExamStateChangeAuditXml
	 from UserExamHistoryVersionTrackerTable 

	inner join [OCR_ItemBank_11.2]..AssessmentTable as AT
	on AT.ID = versionID
	
	inner join [OCR_ItemBank_11.2]..AssessmentGroupTable as AGT
	on AGT.ID = AT.AssessmentGroupID

	inner join [OCR_ItemBank_11.2]..QualificationTable as QT
	on QT.ID = AGT.QualificationID

	inner join WAREHOUSE_ExamSessionTable as WEST
	on WEST.examSessionID = UserExamHistoryVersionTrackerTable.examSessionID

where userID = 14981 and examID = 119;


select * from UserExamHistoryCompletionDateTrackerTable where userID = 14981 and examID = 119;