select 
	CentreName
	, CentreCode
	, Forename
	, Surname
	, Email
 
 from CentreTable
	
	inner join UserTable
	on usertable.ID = CentreTable.PrimaryContactID
		
order by CentreName, Forename;