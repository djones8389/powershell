IF OBJECT_ID('tempdb..#Before') IS NOT NULL DROP TABLE #Before
IF OBJECT_ID('tempdb..#After') IS NOT NULL DROP TABLE #After

create table #Before

(
	ProjectName nvarchar(200)
	, CP_ProjectAndPage nvarchar(200)
	, CP_Version  nvarchar(200)
	, IB_ProjectAndPage nvarchar(200)
	, IB_Version  nvarchar(200)
);

create table #After

(
	ProjectName nvarchar(200)
	, CP_ProjectAndPage nvarchar(200)
	, CP_Version  nvarchar(200)
	, IB_ProjectAndPage nvarchar(200)
	, IB_Version  nvarchar(200)
);


bulk insert #Before
from 'C:\Users\DaveJ\Desktop\Before.csv'
with (fieldterminator = ',', rowterminator = '\n')
go

bulk insert #After
from 'C:\Users\DaveJ\Desktop\After.csv'
with (fieldterminator = ',', rowterminator = '\n')
go


select 
	a.ProjectName
	,a.CP_ProjectAndPage
	,a.CP_Version as CurrentCPVersion
	,a.IB_Version as CurrentIBVersion

 from #After as a

inner join #Before as b
on a.CP_ProjectAndPage = b.CP_ProjectAndPage

where a.CP_ProjectAndPage = b.CP_ProjectAndPage
	and a.CP_Version > A.IB_Version
	--and a.IB_Version not like '%[0-9]%'

	select * from #After where CP_ProjectAndPage = '6050P6448'




/*
ProjectName	CP_ProjectAndPage						CP_Version	IB_Version
OCR Cambridge Awards Maths On Demand	6057P7671	10			9
*/