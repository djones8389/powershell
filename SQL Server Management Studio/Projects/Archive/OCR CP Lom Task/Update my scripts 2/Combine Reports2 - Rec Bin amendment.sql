USE UAT_OCR_ContentProducer_IP

IF OBJECT_ID('tempdb..#AffectedPages') IS NOT NULL DROP TABLE #AffectedPages
IF OBJECT_ID('tempdb..#Projects') IS NOT NULL DROP TABLE #Projects


/*REPORT -  Find pages that:

- Have images
- Are not in the recycle bin
- Are within the publishable state
- Are not BTL Test projects

Store this data,  this is what our cursor will run against

*/

	SELECT 
		 A.ProjectName 
		, cast(A.ProjectID as nvarchar(10)) + 'P' +  cast(A.PageID  as nvarchar(10)) as CP_ProjectAndPage
		,  A.Version as CP_Version
		,  cast(B.ProjectID as nvarchar(10)) + 'P' + cast(ItemRef as nvarchar(10)) as IB_ProjectAndPage
		,  B.Version as IB_Version
	INTO #AffectedPages
	FROM 
	(
	select distinct
		  PLT.ID as [ProjectID]
		 , Name as [ProjectName]
		 , SUBSTRING(PT.ID, CHARINDEX('P', PT.ID) + 1, LEN(PT.ID)) as PageID
		 , PT.Ver as Version
		from dbo.ProjectListTable as PLT with (NOLOCK)
	
	cross apply ProjectStructureXml.nodes('Pro/*[local-name(.)!="Rec"]//Pag') p(r) 
	
	INNER JOIN dbo.ItemGraphicTable IGT with (NOLOCK) on SUBSTRING(IGT.ID, CHARINDEX('P', IGT.ID) + 1, CHARINDEX('S',IGT.ID) - CHARINDEX('P', IGT.ID) - 2 + Len('S')) =  p.r.value('@ID','nvarchar(12)')
	INNER JOIN dbo.ComponentTable CT with (NOLOCK) ON IGT.ParentID = CT.ID
	INNER JOIN dbo.SceneTable ST with (NOLOCK) ON  ST.ID = CT.ParentID
	INNER JOIN dbo.PageTable PT with (NOLOCK) ON PT.ParentID = PLT.ID AND PT.ID = ST.ParentID AND PT.ID = cast(PLT.ID as nvarchar(10)) + 'P' + cast(SUBSTRING(PT.ID, CHARINDEX('P', PT.ID) + 1, LEN(PT.ID)) as nvarchar(10))

	where p.r.value('@sta','int') Between PublishSettingsXML.value('(/PublishConfig/StatusLevelFrom)[1]','int') and PublishSettingsXML.value('(/PublishConfig/StatusLevelTo)[1]','int')

	) A

	LEFT JOIN (

	select  ProjectID, ItemRef, max(Version) as Version 
		from UAT_OCR_ItemBank_IP.dbo.ItemTable  with (NOLOCK)
			group by ProjectID, ItemRef 
	) B

	ON A.ProjectID = B.ProjectID 
		and A.PageID = B.ItemRef		

	--where A.Version = B.Version
	where (A.ProjectName not like '%BTL%' and A.ProjectName != 'cbtest11102013 openassess bank')
	Order by 1,2;

--Full List > Save this--	
SELECT * from #AffectedPages;

/*Run cursor*/

DECLARE  @ProjectsToTarget table (ProjectID int, PageID int)
INSERT @ProjectsToTarget(ProjectID, PageID)
SELECT 
       SUBSTRING(#AffectedPages.CP_ProjectAndPage, 0 , CHARINDEX(N'P', #AffectedPages.CP_ProjectAndPage)) 
	   , SUBSTRING(#AffectedPages.CP_ProjectAndPage, CHARINDEX('P', #AffectedPages.CP_ProjectAndPage) + 1, LEN(#AffectedPages.CP_ProjectAndPage)) as PageID
    FROM #AffectedPages;

SELECT ID as ProjectID 
      ,ProjectStructureXML
INTO #Projects
FROM ProjectListTable
Where ID IN (SELECT ProjectID from @ProjectsToTarget);

--select * from #Projects 

DECLARE PageXML CURSOR FOR 
	select ProjectID, PageID
		from @ProjectsToTarget

DECLARE @ProjectID int, @PageID int;

OPEN PAGEXML;

FETCH NEXT FROM PageXML into @ProjectID, @PageID;

WHILE @@FETCH_STATUS = 0

BEGIN

Update #Projects
set ProjectStructureXML.modify('replace value of (/Pro//Pag[@ID = sql:variable("@PageID")]/@alF)[1] with ("1")')
where ProjectID = @ProjectID;

FETCH NEXT FROM PageXML into @ProjectID, @PageID

END
CLOSE PageXML;
DEALLOCATE PageXML;


--select * from #Projects; 


update ProjectListTable
set ProjectStructureXML = b.ProjectStructureXml
from ProjectListTable a

inner join #Projects as b
on b.ProjectID = a.ID

where b.ProjectID = a.ID;


DROP TABLE #Projects;


UPDATE PageTable
SET ver = ver + 1 
where ID in (SELECT CP_ProjectAndPage FROM #AffectedPages)


UPDATE dbo.ItemGraphicTable 
SET loM = 1
WHERE loM IS NULL;


DROP TABLE #AffectedPages;
