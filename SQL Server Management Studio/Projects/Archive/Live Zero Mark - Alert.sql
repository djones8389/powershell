select
	est.ID 
	, KeyCode
	, Forename
	, Surname
	, esirt.ItemResponseData.query('data(/p/@id)') as ItemID
	, esirt.ItemResponseData.query('data(/p/@um)') as userMark
	, MarkerResponseData
	
	from ExamSessionTable as est WITH(NOLOCK)
	
	inner join ExamSessionItemResponseTable as esirt WITH(NOLOCK)
	on esirt.ExamSessionID = est.ID
	
	inner join UserTable as ut WITH(NOLOCK)
	on ut.id = est.UserID
	
	where examState = 15
		 and esirt.ItemResponseData.exist('p[@um> 0]') = 1
		 and MarkerResponseData.exist('entries/entry[assignedMark=0.000]') = 1;
		
		
		
		 
/*DECLARE	@return_value int

EXEC	@return_value = [dbo].[sa_CANDIDATEEXAMSTATEMANAGEMENTSERVICE_UpdateCumulativeMarkingProgress_sp]
		@ExamSessionId = 97648

SELECT	'Return Value' = @return_value

GO
*/		 