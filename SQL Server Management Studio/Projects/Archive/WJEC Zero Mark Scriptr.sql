/*
DROP TABLE #ExamXML;
*/


DECLARE @ExamSessions table
(
	ESID INT
)

insert @ExamSessions(ESID)
select distinct est.ID	
	from ExamSessionTable as est WITH(NOLOCK)
	
	inner join ExamSessionItemResponseTable as esirt WITH(NOLOCK)
	on esirt.ExamSessionID = est.ID
	
	where examState = 15
		 and esirt.ItemResponseData.exist('p[@um!=0]') = 1
		 and esirt.ItemResponseData.exist('p[@um!=-1]') = 1
		 --and MarkerResponseData.exist('entries/entry[assignedMark=0.000]') = 1;
		 and est.ID in (1430881,1430872)

SELECT ID, StructureXML
FROM ExamSessionTable 
WHERE ID IN (Select ESID from @ExamSessions);


SELECT	 ID
		,CAST(StructureXML AS XML) AS [StructureXML]
INTO	 #ExamXML
FROM	 ExamSessionTable 
WHERE	 ID IN (Select ESID from @ExamSessions);


DECLARE STRUCTUREXML CURSOR FOR

select
	 ExamSessionID
	,ItemID
	,ItemResponseData.value('(/p/@um)[1]', 'decimal(6,4)') as userMark
	,ItemResponseData.value('(/p/@ua)[1]', 'tinyint') as userAttempted
	From ExamSessionItemResponseTable
	
WHERE	 ExamSessionID IN (Select ESID from @ExamSessions);


DECLARE @ESID int, @ItemID nvarchar(12), @userMark decimal(6,4), @userAttempted tinyint;

OPEN STRUCTUREXML;

FETCH NEXT FROM STRUCTUREXML INTO @ESID, @ItemID, @userMark, @userAttempted;

WHILE @@FETCH_STATUS = 0

BEGIN

update #ExamXML
set StructureXML.modify('replace value of (/assessmentDetails/assessment/section/item[@id = sql:variable("@ItemID")]/@userMark)[1] with sql:variable("@userMark")')
where ID = @ESID

update #ExamXML
set StructureXML.modify('replace value of (/assessmentDetails/assessment/section/item[@id = sql:variable("@ItemID")]/@userAttempted)[1] with sql:variable("@userAttempted")')
where ID = @ESID


FETCH NEXT FROM STRUCTUREXML INTO @ESID, @ItemID, @userMark, @userAttempted

END

CLOSE STRUCTUREXML;
DEALLOCATE STRUCTUREXML;

SELECT ID, StructureXML
FROM #ExamXML 
WHERE ID IN  (Select ESID from @ExamSessions);

/*
update ExamSessionTable
set StructureXML = B.StructureXML
from ExamSessionTable as A
inner join #ExamXML as B
on A.ID = B.ID
where A.ID = B.ID
	and A.ID IN (Select ESID from @ExamSessions);
*/

DROP TABLE #ExamXML;