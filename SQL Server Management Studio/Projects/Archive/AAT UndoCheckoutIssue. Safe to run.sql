declare @ItemXML xml = 
 '<ProjectDuration>90</ProjectDuration>
  <ProjectSRBonus>0</ProjectSRBonus>
  <ProjectSRBonusMaximum>5</ProjectSRBonusMaximum>';
  
 create table #AffectedAssessments

(
      ID int,
      AssessmentID int,
      Assessment xml

);

create table #AffectedAssessments_backup

(
      ID int,
      AssessmentID int,
      Assessment xml

);

insert into #AffectedAssessments
SELECT ID, AssessmentID, Assessment
      FROM AssessmentAuditTable
            WHERE Assessment.exist('Assessment/ProjectDuration') = 0;
            
insert into #AffectedAssessments_backup
SELECT ID, AssessmentID, Assessment
      FROM #AffectedAssessments;            

--Select before applying--
select * from #AffectedAssessments;
      
  UPDATE #AffectedAssessments 
  SET Assessment.modify('insert sql:variable("@ItemXML") as last into ((/Assessment)[1])')
  
  From #AffectedAssessments as A
  Inner join AssessmentAuditTable as B
  on B.ID = A.ID
        and B.AssessmentID = A.AssessmentID
        
  WHERE B.ID = A.ID;
  
    
  Update AssessmentAuditTable
  set Assessment = A.Assessment
  from AssessmentAuditTable
  inner join #AffectedAssessments A
  on A.AssessmentID = A.AssessmentID  
        and A.ID = A.ID
  where A.ID = AssessmentAuditTable.ID
	and A.AssessmentID = AssessmentAuditTable.AssessmentID;


  --BackupPlan--
  /*
  Update AssessmentAuditTable
  set Assessment = A.Assessment
  from AssessmentAuditTable
  inner join #AffectedAssessments_backup A
  on A.AssessmentID = A.AssessmentID  
        and A.ID = A.ID
  where A.ID = AssessmentAuditTable.ID
	and A.AssessmentID = AssessmentAuditTable.AssessmentID;
   */

select * from #AffectedAssessments order by ID, AssessmentID;
select * from #AffectedAssessments_backup order by ID, AssessmentID;

rollback 
--drop table #AffectedAssessments;
--drop table #AffectedAssessments_backup;