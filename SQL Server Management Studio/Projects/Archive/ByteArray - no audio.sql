select  

--top 20 
--	west.id
--	, wesdt.itemId
--	, ExamSessionID
	 resultDataFull
	, d.h.value('@v','nvarchar(max)') as ByteArray
	, s.i.value('@id', 'nvarchar(20)') as ItemID
	, SUBSTRING(wesdt.itemId, 0, CHARINDEX('S', wesdt.itemId)) 
	
      from WAREHOUSE_ExamSessionTable as west

      cross apply resultDataFull.nodes('assessmentDetails/assessment/section/item') s(i)
      cross apply s.i.nodes('p/s/c/i/data/h') d(h)

      left join WAREHOUSE_ExamSessionDocumentTable as wesdt
      on wesdt.warehouseExamSessionID = west.ID
		and SUBSTRING(wesdt.itemId, 0, CHARINDEX('S', wesdt.itemId)) = s.i.value('@id', 'nvarchar(20)')
      
      
  where west.ID = 639168
  order by d.h.value('@v','nvarchar(max)')
  
   --order by west.ID desc
    
    
  --    where wesdt.ID is not null
		--and  SUBSTRING(wesdt.itemId, 0, CHARINDEX('S', wesdt.itemId))  = s.i.value('@id', 'nvarchar(20)') 
		
