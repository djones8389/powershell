USE MASTER; 
GO

IF OBJECT_ID('tempdb..#dbusers') IS NOT NULL DROP TABLE tempdb..#dbusers;


DECLARE @loginName varchar(max)
DECLARE @SQL varchar(max)

CREATE TABLE #dbusers ( 
  sid VARBINARY(85)
  ) 

EXEC sp_MSforeachdb 
  'insert #dbusers select sid from [?].sys.database_principals where type != ''R''' 


DECLARE loginCursor CURSOR FOR

SELECT name 
FROM   sys.server_principals 
WHERE  sid IN (SELECT sid 
               FROM   sys.server_principals 
               WHERE  TYPE != 'R' 
                      AND name NOT LIKE ('##%##') 
               EXCEPT 
               SELECT DISTINCT sid 
               FROM   #dbusers) 
AND type_desc = 'SQL_LOGIN'

and name not in (
select  p.name
		--, p.type_desc
		--, pp.name
		--, pp.type_desc
		
from  sys.server_role_members roles

inner join sys.server_principals p 
on roles.member_principal_id = p.principal_id

left  join sys.server_principals pp 
on roles.role_principal_id = pp.principal_id
	
	where pp.name = 'sysadmin'

	)

OPEN loginCursor  

FETCH NEXT FROM loginCursor into @loginName   

WHILE @@FETCH_STATUS=0

BEGIN

	PRINT 'DROP LOGIN ' + @loginName + ';'
    FETCH NEXT FROM loginCursor into @loginName 
    
END
CLOSE loginCursor
DEALLOCATE loginCursor

GO 
DROP TABLE #dbusers;