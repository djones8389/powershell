SELECT EST.ID, EST.KeyCode, EST.examState, UT.Forename, UT.Surname, CT.CentreName, SCET.examName
	, Scet.ScheduledStartDateTime, ScheduledEndDateTime
	,	DATEDIFF(DAY, Scet.ScheduledStartDateTime, ScheduledEndDateTime)
	,  ScheduledDuration, ScheduledDurationXml
  FROM ExamSessionTable as EST (NOLOCK)

  Inner Join ScheduledExamsTable as SCET (NOLOCK)
  on SCET.ID = EST.ScheduledExamID
   
  Inner Join UserTable as UT (NOLOCK)
  on UT.ID = EST.UserID

  Inner Join CentreTable as CT (NOLOCK)
  on CT.ID = SCET.CentreID

where Keycode =  'PVNZVXA6'
 --or ut.CandidateRef = '20019420'
 or ut.CandidateRef = '20019423'
 order by est.ID desc