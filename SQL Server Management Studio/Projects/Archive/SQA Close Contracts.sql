IF OBJECT_ID('tempdb..#SQAContracts') IS NOT NULL DROP TABLE #SQAContracts;

CREATE TABLE #SQAContracts (

	username nvarchar(150)
	, userID smallint
	, forename nvarchar(150)
	, surname nvarchar(150)
	, projectName nvarchar(150)
	, projectID smallint
	, contractID smallint
	, specificationID smallint
	, Role nvarchar(20)
)

bulk insert #SQAContracts
from 'C:\Temp\DaveJ-DoNoDelete\SQAAppointeeClosedContracts.csv'
with (fieldterminator = ',', rowterminator = '\n')
go


--SELECT  userID, projectID, contractID FROM #SQAContracts  order by contractID

select * 
from #SQAContracts  as TC
INNER JOIN Contract as C
on C.ContractID	 = TC.contractID
	and C.UserID = TC.userID


select top 500 * from Contract  

--1 = Writer
--2 = Checker

/*

UPDATE Contract 
Set CompletionDate = ''
where ContractID = 180
	and userID = 2354
	and projectID = 1217
	and itemSpecificationID IN (4,8)
	and Type = 2

*/


DECLARE CloseContracts CURSOR FOR
select contractID
		, userID
		, projectID
		, specificationID
		, case #SQAContracts.Role when 'Writer' then '1' else '2' end as 'Role'
from #SQAContracts
where contractID = 378


DECLARE @ContractID int, @UserID smallint, @ProjectID smallint, @itemSpecificationID smallint, @Type tinyint;

OPEN CloseContracts

FETCH NEXT FROM CloseContracts INTO @ContractID, @UserID, @ProjectID, @itemSpecificationID, @Type

WHILE @@FETCH_STATUS = 0

BEGIN

PRINT @ContractID 
PRINT @UserID 
PRINT @ProjectID 
PRINT @itemSpecificationID 
PRINT @Type

BEGIN TRAN

select contractID
		, userID
		, projectID
		, ItemSpecificationLookup.id
		, case #SQAContracts.Role when 'Writer' then '1' else '2' end as 'Role'
from #SQAContracts
inner join ItemSpecificationLookup
on ItemSpecificationLookup.SpecificationName = specificationID
where contractID = 378

UPDATE Contract 
Set CompletionDate = '2015-06-26 15:35:30.897'
where  ContractID = @ContractID
	and userID = @UserID
	and projectID = @ProjectID
	and itemSpecificationID = @itemSpecificationID
	and Type = @Type

select contractID
		, userID
		, projectID
		, ItemSpecificationID
		, type
		, Contract.Type
		, CompletionDate
FROM Contract
where contractID = 378

ROLLBACK
	
FETCH NEXT FROM CloseContracts INTO @ContractID, @UserID, @ProjectID, @itemSpecificationID, @Type

END
CLOSE CloseContracts;
DEALLOCATE CloseContracts;















select C.contractID
		, C.userID
		, C.projectID
		, #SQAContracts.specificationID
		, C.ItemSpecificationID
		, case #SQAContracts.Role when 'Writer' then '1' else '2' end as 'Role'
from #SQAContracts
inner join Contract as C
on C.UserID = #SQAContracts.userID
	and C.ContractID = #SQAContracts.ContractID
where C.userid = '1391'
	and C.ItemSpecificationID in (select ID FROM ItemSpecificationLookup where SpecificationName = 1234)


DECLARE CloseContracts CURSOR FOR
select contractID
		, userID
		, projectID
		, ItemSpecificationLookup.id
		, case #SQAContracts.Role when 'Writer' then '1' else '2' end as 'Role'
from #SQAContracts
inner join ItemSpecificationLookup
on ItemSpecificationLookup.SpecificationName = specificationID
where userid = '1391'

DECLARE @ContractID int, @UserID smallint, @ProjectID smallint, @itemSpecificationID smallint, @Type tinyint;

OPEN CloseContracts

FETCH NEXT FROM CloseContracts INTO @ContractID, @UserID, @ProjectID, @itemSpecificationID, @Type

WHILE @@FETCH_STATUS = 0

BEGIN


--PRINT  @itemSpecificationID
select * from Contract
where ContractID = @ContractID
	and userID = @UserID
	and projectID = @ProjectID
	and Type = @Type
	and itemSpecificationID = @itemSpecificationID
	

FETCH NEXT FROM CloseContracts INTO @ContractID, @UserID, @ProjectID, @itemSpecificationID, @Type

END
CLOSE CloseContracts;
DEALLOCATE CloseContracts;



--select 
-- top 5
--	PLT.Name
--	,PLT.ID
--	,Forename + ' ' + Surname
--	,UT.Username
--	,C.ContractID
--	,CP.*
--	,CTL.*
--	,ISL.SpecificationName
--	, C.CompletionDate
--from Contract as C

--INNER JOIN ProjectListTable AS PLT ON PLT.ID = C.ProjectID
--INNER JOIN UserTable AS UT ON UT.UserID = C.UserID
--INNER JOIN ContractPage AS CP ON CP.ContractID = C.ID
--INNER JOIN ContractTypeLookup AS CTL ON CTL.ID = C.Type
--INNER JOIN ItemSpecificationLookup AS ISL ON ISL.ID = C.ItemSpecificationID

-- where PLT.ID = 1109
--	and ut.UserID = 1934
--	order by Username, ISL.SpecificationName


