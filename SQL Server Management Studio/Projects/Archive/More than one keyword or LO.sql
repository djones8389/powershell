IF ('tempdb..##IBResults') IS NOT NULL DROP TABLE ##IBResults

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

CREATE TABLE ##IBResults (ClientIB nvarchar(MAX), ClientCA nvarchar(MAX), ID smallint, Keywords bit, LO bit)
INSERT ##IBResults
exec sp_msforeachdb '

use [?]

if (''?'' like ''%_ItemBank'')

SET QUOTED_IDENTIFIER ON;

DECLARE @myAssessments TABLE(ID INT, AssessmentName NVARCHAR(200), Rules XML)
DECLARE @myRules TABLE(ID INT, Parameter NVARCHAR(1000), Projects XML)
DECLARE @myRulesWithType TABLE(ID INT, Parameter NVARCHAR(1000), Projects XML, keywords BIT, lo BIT)
DECLARE @myProjectIDs TABLE(ClientIB nvarchar(MAX), ClientCA nvarchar(MAX), ID smallint, Keywords bit, LO bit)


INSERT INTO @myAssessments
SELECT ID, AssessmentName, AssessmentRules
FROM AssessmentTable
WHERE 
      CONVERT(NVARCHAR(MAX), AssessmentRules) LIKE ''%@Keywords%''
      OR
      CONVERT(NVARCHAR(MAX), AssessmentRules) LIKE ''%@LO%''

INSERT INTO @myRules     
SELECT exams.ID, c.value(''(./XPath)[1]'', ''NVARCHAR(MAX)''), c.query(''ProjectIDList'')
FROM @myAssessments exams
CROSS APPLY exams.Rules.nodes(''PaperRules/Section/Rule/Param'') as x(c)
      
DELETE @myRules
WHERE NOT(Parameter LIKE ''%@Keywords%'' OR Parameter LIKE ''%@LO%'')

INSERT INTO @myRulesWithType
SELECT ID, Parameter, Projects, 
      CASE WHEN Parameter LIKE ''%@Keywords%'' THEN 1 ELSE 0 END,
      CASE WHEN Parameter LIKE ''%@LO%'' THEN 1 ELSE 0 END
FROM @myRules


INSERT INTO @myProjectIDs(ClientIB, ID, Keywords, LO)
SELECT ''?'', ProjectID, MAX(CONVERT(INT, keywords)), MAX(CONVERT(INT, lo))
FROM   (
		SELECT c.value(''.'', ''INT'') AS ProjectId, keywords, lo
		FROM @myRulesWithType Rules
		CROSS APPLY Rules.Projects.nodes(''ProjectIDList/ID'') AS x(c)
		) ProjectIDs
GROUP BY ProjectID


SELECT *
FROM @myProjectIDs
'

UPDATE ##IBResults 
set ClientCA = SUBSTRING(ClientIB, 0, CHARINDEX('_', ClientIB)) + '_ContentAuthor'


SELECT '

SELECT SUBSTRING(ClientIB, 0, CHARINDEX(''_'', ClientIB)), SubjectTagTypes.Id, Item_Id, COUNT(Item_Id) as ''Count'', TagTypeKey
from ##IBResults projects
inner join ' + ClientCA + '..Subjects on projects.ID = Subjects.ProjectId
inner join ' + ClientCA + '..SubjectTagTypes on subjects.Id = SubjectTagTypes.SubjectId
INNER JOIN ' + ClientCA + '..SubjectTagValues on SubjectTagTypes.Id = SubjectTagTypeId
INNER JOIN ' + ClientCA + '..SubjectTagValueItems ON SubjectTagValueItems.SubjectTagValue_Id = SubjectTagValues.Id
WHERE (SubjectTagTypes.TagTypeKey = 1 and LO = 1) or (SubjectTagTypes.TagTypeKey = 3 and Keywords = 1)
GROUP BY SubjectTagTypes.Id, Item_Id, TagTypeKey,SUBSTRING(ClientIB, 0, CHARINDEX(''_'', ClientIB))
HAVING COUNT(Item_Id) > 1
UNION
'
FROM ##IBResults