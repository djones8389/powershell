USE [AAT_ContentProducer_DI] 

--DECLARE @MyProjectID int = 880;

IF OBJECT_ID ('tempdb..#SharedLibStructure') IS NOT NULL DROP TABLE #ProjectStructure;

DECLARE @MyItems TABLE (id int, name nvarchar(200), projectid int, SLProjectID int)
insert into @MyItems(ID, name, projectid, SLProjectID)

select A.id			
	 , A.name
	 , A.Projectid
	 , B.ProjectID as 'SLProjectID'
from (

select id, name, projectid				
from ProjectManifestTable as PMT with (NOLOCK)
where  (name like '[%0-9%]' + '.swf' 
            or name like '[%0-9%][%0-9%]' + '.swf' 
            or name like '[%0-9%][%0-9%][%0-9%]' + '.swf')
	and location not like '%background%'
	--and projectid = @MyProjectID
) A

INNER join   

(
select b.projectid						
	, a.b.value ('@id', 'int') as  ItemID
from
(
	select projectid
		, cast(Structurexml as xml) as Structurexml
	from SharedLibraryTable as SLT with (NOLOCK)
	--where projectid = @MyProjectID
	) B
	cross apply Structurexml.nodes('//item') a(b)
) B 

on A.ID = b.Itemid
	and a.projectid = b.projectid

LEFT JOIN								
(
      SELECT ID, ali
      FROM ItemCustomQuestionTable with (NOLOCK)
      UNION
      SELECT ID, ali
      FROM ItemGraphicTable with (NOLOCK)
      UNION
      SELECT ID, ali
      FROM ItemVideoTable with (NOLOCK)
      UNION
      SELECT ID, ali
      FROM ItemHotSpotTable with (NOLOCK)
) Items
ON 
      Items.ID LIKE CONVERT(NVARCHAR(10), A.ProjectId) + 'P%' 
      AND 
      (
            CONVERT(NVARCHAR(50), A.ID) = ali
            OR
            SUBSTRING(A.name, 0, LEN(A.name) - 3) = ali
      )

WHERE  Items.id IS NULL				--Where image is not on any pages
	--and A.ProjectID = @MyProjectID;


SELECT * FROM @MyItems;			--Select out which PMT + SLT Records you are going to delete

--DECLARE SharedLibrary CURSOR FOR 
--SELECT ID
--	  , Name
--	  , ProjectID
--FROM @MyItems
--Where SLProjectID IS NOT NULL		--Only include in the cursor where entry is in the sharedLibraryTable--

--select projectid, cast(Structurexml as xml) as Structurexml
--into #SharedLibStructure
--from SharedLibraryTable
--where projectid in (select ProjectID from @MyItems)

--select * from #SharedLibStructure	--Select out your SLT to store for backup purposes

--DECLARE @ID int, @Name nvarchar(max), @ProjectID int;

--OPEN SharedLibrary;

--FETCH NEXT FROM SharedLibrary INTO  @ID, @Name , @ProjectID

--WHILE @@Fetch_Status = 0

--BEGIN

--Update #SharedLibStructure
--set Structurexml.modify('delete(/sharedLibrary/item[@id=sql:variable("@ID")]  [@name=sql:variable("@Name")]) ')
--where projectid = @ProjectID

--FETCH NEXT FROM SharedLibrary INTO  @ID, @Name , @ProjectID

--END
--CLOSE SharedLibrary;
--DEALLOCATE SharedLibrary;


--update SharedLibraryTable
--set structureXML = CAST(b.Structurexml as nvarchar(MAX))
--from SharedLibraryTable a
--inner join #SharedLibStructure b
--on a.ProjectId = b.ProjectId
--where a.ProjectId = b.ProjectId;


--DROP TABLE #SharedLibStructure;


--delete from ProjectManifestTable 
--	where (projectid in (select ProjectID from @MyItems) 
--				and name in (select name from @MyItems) 
--					and ID in (select ID from @MyItems)
--					);
