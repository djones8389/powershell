USE AAT_ContentProducer_DI

DECLARE @MyProjectID INT = 886;

DECLARE @MyItems TABLE (projectid int, id int, name nvarchar(200))
insert into @MyItems(projectid, ID, name)

select 
	 Projectid
	 , B.id			
	 , name	  

from (

	select projectid
			, id
			, name				--Find where image is null--
	from ProjectManifestTable as PMT with (NOLOCK)
	where  (name like '[%0-9%]' + '.swf' 
		        or name like '[%0-9%][%0-9%]' + '.swf' 
				    or name like '[%0-9%][%0-9%][%0-9%]' + '.swf')
	and location not like '%background%'
		and projectid = @MyProjectID
) B

		
LEFT JOIN								--Find on any pages
(
      SELECT ID, ali
      FROM ItemCustomQuestionTable with (NOLOCK)
      UNION
      SELECT ID, ali
      FROM ItemGraphicTable with (NOLOCK)
      UNION
      SELECT ID, ali
      FROM ItemVideoTable with (NOLOCK)
      UNION
      SELECT ID, ali
      FROM ItemHotSpotTable with (NOLOCK)
) Items
ON 
      Items.ID LIKE CONVERT(NVARCHAR(10), B.ProjectId) + 'P%' 
      AND 
      (
            CONVERT(NVARCHAR(50), B.ID) = ali
            OR
            SUBSTRING(B.name, 0, LEN(B.name) - 3) = ali
      )

WHERE  Items.id IS NULL				--Where image is not on any pages
	and B.ProjectID = @MyProjectID;


SELECT * FROM @MyItems order by name; --Keep for backup purposes


delete from ProjectManifestTable 
	where (projectid in (select ProjectID from @MyItems) 
				and name in (select name from @MyItems) 
					and ID in (select ID from @MyItems)
					);
