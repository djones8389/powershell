USE [AAT_ContentProducer_DI] 

;with cte as (

select id as ID
		, name as Name
		, projectid	as ProjectID
from ProjectManifestTable as PMT with (NOLOCK)
where location not like '%background%'

UNION

select 	 a.b.value ('@id', 'int') as  ID
		, a.b.value ('@name', 'nvarchar(200)') as Name
		, b.projectid	  as ProjectID				
from
(
	select projectid
		, cast(Structurexml as xml) as Structurexml
	from SharedLibraryTable as SLT with (NOLOCK)
	) B
	cross apply Structurexml.nodes('//item') a(b)
 )
 

select cte.ID
	 ,  Name
	 , ProjectID
	
 from cte 
	
LEFT JOIN								--Find on any pages
(
      SELECT ID, ali
      FROM ItemCustomQuestionTable with (NOLOCK)
      UNION
      SELECT ID, ali
      FROM ItemGraphicTable with (NOLOCK)
      UNION
      SELECT ID, ali
      FROM ItemVideoTable with (NOLOCK)
      UNION
      SELECT ID, ali
      FROM ItemHotSpotTable with (NOLOCK)
) Items
ON 

      Items.ID LIKE CONVERT(NVARCHAR(10), cte.ProjectID) + 'P%' 
      AND 
      (
            CONVERT(NVARCHAR(50), cte.ID) = ali
            OR
            SUBSTRING(cte.Name, 0, LEN(cte.Name) - 3) = ali
      )


WHERE Items.id IS NOT NULL				--Where image is on pages
	and (name like '[%0-9%]' + '.swf'	-- Where image is numeric
            or name like '[%0-9%][%0-9%]' + '.swf' 
            or name like '[%0-9%][%0-9%][%0-9%]' + '.swf')





select ROW_NUMBER() over (order by name) rownum
                  , name
                  , Image
      into #TempTable
      from ProjectManifestTable

where Image is not null
      and name in (
                    select name
                        from ProjectManifestTable
                                where ProjectId = @ProjectID
                                and Image is null
                  );


delete
from #TempTable
where rownum not in (   
      
      select rownum
      from 
      #TempTable as R
      inner join
            (
            select  name, MAX(rownum) as RN --Max(DATALENGTH(image)) as 'Myimage'
                  from #TempTable               
                  
                  group by name
                  
            ) X
      on r.rownum = X.RN
);


select * from #MyTable where pmtname in (select name from #TempTable); -- Report to show you affected pages that we can fix



SELECT * FROM #TempTable;  --Find images we can fix



update ProjectManifestTable
set Image = (select Image from #TempTable where #TempTable.name = ProjectManifestTable.name)
where ProjectId = @ProjectID
		and Image is null;
