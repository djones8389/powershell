--SELECT WEST.ID, WEST.KeyCode, WUT.Forename, WUT.Surname, WUT.CandidateRef, WCT.CentreName, WSCET.examName, WEST.completionDate
--	, WESTS.ExternalReference, wests.examId, wests.centreId
select west.KeyCode, wests.centreCode
FROM WAREHOUSE_ExamSessionTable as WEST

  Inner Join WAREHOUSE_ScheduledExamsTable as WSCET
  on WSCET.ID = WEST.WAREHOUSEScheduledExamID
  
  Inner Join WAREHOUSE_UserTable as WUT
  on WUT.ID = WEST.WAREHOUSEUserID

  Inner Join WAREHOUSE_CentreTable as WCT
  on WCT.ID = WSCET.WAREHOUSECentreID

  Inner Join WAREHOUSE_ExamSessionTable_Shreded as WESTS
  on WESTS.examSessionId = WEST.ID

where west.KeyCode = '3DRAH401'
or wests.centreCode  = 'AhlconIS'