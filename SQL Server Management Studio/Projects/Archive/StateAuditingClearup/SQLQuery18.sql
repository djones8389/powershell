--IF OBJECT_ID('tempdb..#StateAuditing') IS NOT NULL DROP TABLE #StateAuditing

DECLARE @StateToDelete tinyint = 1;

--select ROW_NUMBER() over (partition by WarehouseExamSessionID, examState ORDER BY WarehouseExamSessionID, examState, [date]) as test
--	, WarehouseExamSessionID
--	, ExamState
--	, Date
--INTO #StateAuditing
--from Warehouse_ExamStateAuditTable_DJTEST (NOLOCK)
--group by WarehouseExamSessionID, ExamState, [date]

select top 10 A.*
FROM Warehouse_ExamStateAuditTable_DJTEST A
INNER JOIN #StateAuditing B
ON B.WarehouseExamSessionID = A.WarehouseExamSessionID
	AND B.examState = A.examState
	  AND b.Date = A.Date
where A.examstate = @StateToDelete
	
	and test NOT IN (
			
	select test
	FROM #StateAuditing 
	where date = (

		SELECT MIN(DATE)
		FROM #StateAuditing
		where examstate = @StateToDelete
			
		)
	)

--OPTION (MAXDOP 1)

	--select * from #StateAuditing where warehouseexamsessionid = 2512
	--select * from Warehouse_ExamStateAuditTable_DJTEST where warehouseexamsessionid = 2512
	--2015-04-03 11:27:38.113
/*
DROP TABLE  [WAREHOUSE_ExamStateAuditTable_DJTEST];

CREATE TABLE [dbo].[WAREHOUSE_ExamStateAuditTable_DJTEST](
	[WarehouseExamSessionID] [int] NOT NULL,
	[ExamState] [int] NOT NULL,
	[Date] [datetime] NOT NULL
);

INSERT INTO [WAREHOUSE_ExamStateAuditTable_DJTEST]
	SELECT * FROM WAREHOUSE_ExamStateAuditTable;
*/


--select WarehouseExamSessionID, count(ExamState), examState
--from Warehouse_ExamStateAuditTable_DJTEST (NOLOCK)
--group by WarehouseExamSessionID, ExamState
--order by 2 DESC




select * 
from Warehouse_ExamStateAuditTable_DJTEST
where WarehouseExamSessionID = 1654730 --and examState=  4

--select * 
--from Warehouse_ExamStateAuditTable
--where WarehouseExamSessionID = 1654730 --and examState=  4

----SELECT * FROM Warehouse_ExamStateAuditTable_DJTEST WHERE WarehouseExamSessionID =  1654730


--select WarehouseExamSessionID, count(ExamState), examState
----FROM #StateAuditing
--from Warehouse_ExamStateAuditTable_DJTEST
--group by WarehouseExamSessionID, ExamState
--order by 2 DESC


--DELETE from Warehouse_ExamStateAuditTable_DJTEST
--where WarehouseExamSessionID = 1654730	
--	and 


	--select * FROM #StateAuditing 
	--where WarehouseExamSessionID = 1654730
	--and examstate = 4