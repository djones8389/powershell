--select * from PageTable where ID = '4114p4331'--their page--
----select * from PageTable where ID = '4114p4303'--normal one--

--select * from ProjectListTable where ID = '4114'

----drop table #myTemp



select  
	ProjectListTable.id as ProjectID
	, pro.pag.value('@ID','nvarchar(100)') as PageID
	, pag.value('local-name((..)[1])', 'nvarchar(1000)')
	, pro.pag.value('local-name((.)[1])', 'nvarchar(1000)')
	, ProjectStructureXml
from ProjectListTable 

	cross apply ProjectStructureXml.nodes('//Pag') pro(pag)
	
		left join PageTable
		on PageTable.ID = cast(ProjectListTable.ID as nvarchar(100)) + N'P' + pro.pag.value('@ID','nvarchar(100)')
					AND ProjectListTable.ID = PageTable.ParentID
					
WHERE pagetable.id is null
	and pag.value('local-name((..)[1])', 'nvarchar(1000)') <> 'Rec';




where ID = '4114';


select ProjectID + 'P' + PageID as Combined 
		into #jointable
	from #myTemp
	
select * from #jointable order by Combined 

;with cte as (

	select cast(id as nvarchar(100)) as CTEID
		 from PageTable 
		where ID like '4114%'
)

select * from cte order by CTEID



drop table #myTemp;
drop table #jointable;