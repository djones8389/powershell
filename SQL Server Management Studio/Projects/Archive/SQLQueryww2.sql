USE [SADB]
GO

DECLARE	@return_value int

EXEC	@return_value = [dbo].[sa_SECUREASSESSINTEGRATION_getCandidateResults_sp]
		@startDate = N'03/07/2012',
		@endDate = N'03/07/2012',
		@examReference = N'*',
		@centreReference = N'*',
		@useSubmissionTime = false,
		@pageNumber = 100,
		@userID = 26,
		@warehouseStates = 12
		
SELECT	'Return Value' = @return_value

GO

--select * from UserTable where Surname = 'User'

select top 100 * from WAREHOUSE_ExamSessionTable_Shreded order by warehouseTime desc