DECLARE @SingleProject int = '6054'

--SpotCheck

SELECT PT.ParentID
	 , SUBSTRING(IGT.ID, CHARINDEX('P', IGT.ID) + 1, CHARINDEX('S',IGT.ID) - CHARINDEX('P', IGT.ID) - 2 + Len('S'))  as PageID
	 , PT.Ver
	 , IGT.lom
--INTO #ManualCheck
FROM dbo.ItemGraphicTable IGT
INNER JOIN dbo.ComponentTable CT ON IGT.ParentID = CT.ID
INNER JOIN dbo.SceneTable ST ON ST.ID = CT.ParentID
INNER JOIN dbo.PageTable PT ON PT.ID = ST.ParentID
where PT.ParentID = @SingleProject
order by 1,2,3;

select ItemRef, max(Version) as ver
from UAT_OCR_ItemBank_IP..ItemTable 
where ProjectID = @SingleProject group by ItemRef;


SELECT ID ProjectID, 
      ProjectStructureXML
INTO #Projects
FROM ProjectListTable
WHERE ID NOT IN (6020,6005,6015,6014,6009,6038,6010,6042,334,327,340,6007,6012,6051,6011,6041,6044,6045,6016, 6072)  --Omit Test projects--
AND ID IN (		--Only run this for projects, with pages with graphics--
      SELECT PT.ParentID
      FROM dbo.ItemGraphicTable IGT
      INNER JOIN dbo.ComponentTable CT ON IGT.ParentID = CT.ID
      INNER JOIN dbo.SceneTable ST ON ST.ID = CT.ParentID
      INNER JOIN dbo.PageTable PT ON PT.ID = ST.ParentID
	  where PT.ParentID = @SingleProject
);

--select * from #Projects;

DECLARE PageXML CURSOR FOR  --Only do this for pages which need it - not ALL pages--
	SELECT PT.ParentID  as ProjectID	
		  , SUBSTRING(IGT.ID, CHARINDEX('P', IGT.ID) + 1, CHARINDEX('S',IGT.ID) - CHARINDEX('P', IGT.ID) - 2 + Len('S'))  as PageID

	FROM dbo.ItemGraphicTable IGT
	INNER JOIN dbo.ComponentTable CT ON IGT.ParentID = CT.ID
	INNER JOIN dbo.SceneTable ST ON ST.ID = CT.ParentID
	INNER JOIN dbo.PageTable PT ON PT.ID = ST.ParentID
	where PT.ParentID =  @SingleProject;

DECLARE @ProjectID int, @PageID int;

OPEN PAGEXML;

FETCH NEXT FROM PageXML into @ProjectID, @PageID;

WHILE @@FETCH_STATUS = 0

BEGIN

Update #Projects
set ProjectStructureXML.modify('replace value of (/Pro//Pag[@ID = sql:variable("@PageID")]/@alF)[1] with ("1")')
where ProjectID = @ProjectID;


FETCH NEXT FROM PageXML into @ProjectID, @PageID

END
CLOSE PageXML;
DEALLOCATE PageXML;


--select * from #Projects; 


update ProjectListTable
set ProjectStructureXML = b.ProjectStructureXml
from ProjectListTable a

inner join #Projects as b
on b.ProjectID = a.ID

where b.ProjectID = a.ID;

DROP TABLE #Projects;


UPDATE PT
SET ver = ver + 1 
FROM dbo.ItemGraphicTable IGT
INNER JOIN dbo.ComponentTable CT ON IGT.ParentID = CT.ID
INNER JOIN dbo.SceneTable ST ON ST.ID = CT.ParentID 
INNER JOIN dbo.PageTable PT ON PT.ID = ST.ParentID
WHERE IGT.loM IS NULL-- OR IGT.loM = 0
	and PT.ParentID = @SingleProject;

UPDATE dbo.ItemGraphicTable 
SET loM = 1
WHERE SUBSTRING(ItemGraphicTable.ParentID, 0 , CHARINDEX(N'P', ItemGraphicTable.ParentID)) = @SingleProject;
	
--SpotCheck

SELECT PT.ParentID
	 , SUBSTRING(IGT.ID, CHARINDEX('P', IGT.ID) + 1, CHARINDEX('S',IGT.ID) - CHARINDEX('P', IGT.ID) - 2 + Len('S'))  as PageID
	 , PT.Ver
	 , IGT.lom
--INTO #ManualCheck
FROM dbo.ItemGraphicTable IGT
INNER JOIN dbo.ComponentTable CT ON IGT.ParentID = CT.ID
INNER JOIN dbo.SceneTable ST ON ST.ID = CT.ParentID
INNER JOIN dbo.PageTable PT ON PT.ID = ST.ParentID
where PT.ParentID = @SingleProject
order by 1,2,3;

--select ItemRef, max(Version) as ver
--from UAT_OCR_ItemBank_IP..ItemTable 
--where ProjectID = @SingleProject group by ItemRef;






/*

DEBUG



UPDATE PT
SET ver = ver + 1 
FROM dbo.ItemGraphicTable IGT
INNER JOIN dbo.ComponentTable CT ON IGT.ParentID = CT.ID
INNER JOIN dbo.SceneTable ST ON ST.ID = CT.ParentID 
INNER JOIN dbo.PageTable PT ON PT.ID = ST.ParentID
WHERE IGT.loM IS NULL-- OR IGT.loM = 0
	and PT.ParentID = 6017

Go

UPDATE dbo.ItemGraphicTable 
SET loM = 1
WHERE loM IS NULL-- OR loM = 0



select top 5 SUBSTRING(ItemGraphicTable.ParentID, 0 , CHARINDEX(N'P', ItemGraphicTable.ParentID))
, SUBSTRING(ItemGraphicTable.ID, CHARINDEX('P', ItemGraphicTable.ID) + 1, CHARINDEX('S',ItemGraphicTable.ID) - CHARINDEX('P', ItemGraphicTable.ID) - 2 + Len('S')) 
from ItemGraphicTable 

inner join ProjectListtable as plt
on plt.id  =  SUBSTRING(ItemGraphicTable.ParentID, 0 , CHARINDEX(N'P', ItemGraphicTable.ParentID))    --where ParentID=  6017

where plt.id = 6017

*/