USE UAT_AAT_ContentProducer

--DECLARE @MyProjectID int = 942;

IF OBJECT_ID ('tempdb..#HoldingTable') IS NOT NULL DROP TABLE #HoldingTable;

--DECLARE @MyItems TABLE (id int, name nvarchar(200), projectid int, SLProjectID int)
--insert into @MyItems(ID, name, projectid, SLProjectID)

select A.id			
	 , A.name
	 , A.Projectid
	 , B.ProjectID as 'SLProjectID'
	 , Items.ID as PageID
	 , DataLength(A.Image) as DL
into #HoldingTable
from (

select id, name, projectid, image
from ProjectManifestTable as PMT with (NOLOCK)
where  (name like '[%0-9%]' + '.swf' 
            or name like '[%0-9%][%0-9%]' + '.swf' 
            or name like '[%0-9%][%0-9%][%0-9%]' + '.swf')
	and location not like '%background%'
	
) A

INNER join   

(
select b.projectid						
	, a.b.value ('@id', 'int') as  ItemID
from
(
	select projectid
		, cast(Structurexml as xml) as Structurexml
	from SharedLibraryTable as SLT with (NOLOCK)
	) B
	cross apply Structurexml.nodes('//item') a(b)
) B 

on A.ID = b.Itemid
	and a.projectid = b.projectid
	--and a.projectid = 880
LEFT JOIN								
(
      SELECT ID, ali
      FROM ItemCustomQuestionTable with (NOLOCK)
      UNION
      SELECT ID, ali
      FROM ItemGraphicTable with (NOLOCK)
      UNION
      SELECT ID, ali
      FROM ItemVideoTable with (NOLOCK)
      UNION
      SELECT ID, ali
      FROM ItemHotSpotTable with (NOLOCK)
) Items
ON 
      Items.ID LIKE CONVERT(NVARCHAR(10), A.ProjectId) + 'P%' 
      AND 
      (
            CONVERT(NVARCHAR(50), A.ID) = ali
            OR
            SUBSTRING(A.name, 0, LEN(A.name) - 3) = ali
      )
where items.id is null
Order by Items.ID



select #HoldingTable.*, ProjectListTable.Name as ProjectName from #HoldingTable

inner join ProjectListTable
on ProjectListTable.ID = Projectid

