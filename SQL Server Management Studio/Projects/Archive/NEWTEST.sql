--NEWTEST--

USE BritishCouncil_SecureAssess_LIVE

BEGIN TRAN


select * from BritishCouncil_TPM..ScheduledPackageCandidateExams as SPCE

	inner join BritishCouncil_TPM..ScheduledPackageCandidates as SPC
	on SPC.ScheduledPackageCandidateId =  SPCE.Candidate_ScheduledPackageCandidateId

where ScheduledExamRef in ('ZUF2BE01','MNTCAG01','N2URWJ01','V2N6WJ01','DFEW4601')

      -- [TODO1] Specify test package ID 
      DECLARE @testPackageId int = 206

      -- [TODO2] Specify list of exam sessions that have been scheduled in SecureAssess, but not in TestPackage
      DECLARE @examSessions table(id int, candidateId int)
      INSERT @examSessions(id, candidateId)
            SELECT WEST.ID, WEST.WAREHOUSEUserID
                  FROM WAREHOUSE_ExamSessionTable WEST
                  WHERE ID in (539916,539917,539918,538436,539919)
	             
	             --Omitted 399901,399940,
	                          
      -- How many exams the test package contains
      DECLARE @testPackageExamCount int
      SELECT @testPackageExamCount = COUNT(*)
         FROM BritishCouncil_TPM..PackageExams PE
            WHERE PE.PackageId = @testPackageId
                  AND StatusValue = 0 -- Live

      DECLARE @candidates TABLE(candidateId int)
      INSERT @candidates(candidateId)
            SELECT candidateId
				FROM @examSessions
            GROUP BY candidateId

     -- Find candidate exams that have been scheduled in SecureAssess, but not in TestPackage
      DECLARE @candidateId int
      DECLARE @intersectedCount int

      WHILE EXISTS (SELECT TOP 1 * FROM @candidates)
      BEGIN
            SELECT TOP 1 @candidateId = candidateId FROM @candidates
            DELETE FROM @candidates WHERE candidateId = @candidateId

            -- Check if all exams from the package have been scheduled in SecureAssess
            SELECT @intersectedCount = COUNT(*)
                  FROM 
                  (--@intersectedCount is > = works--
                        SELECT PE.SurpassExamId
                              FROM BritishCouncil_TPM..PackageExams PE
                              WHERE PE.PackageId = @testPackageId
                        INTERSECT
                        SELECT SEXT.ExamID
                              FROM @examSessions ES
                                    JOIN WAREHOUSE_ExamSessionTable EST ON EST.ID = ES.id
                                    JOIN WAREHOUSE_ScheduledExamsTable SEXT ON SEXT.ID = EST.WAREHOUSEScheduledExamID
                              WHERE ES.candidateId = @candidateId
                  ) T

            IF (@intersectedCount <> @testPackageExamCount)
            BEGIN
                  PRINT CAST(@candidateId AS nvarchar(100)) + ' DO NOT PROCEED'
                  CONTINUE
            END


            -- Add one row to ScheduledPackages
            DECLARE @scheduledPackageId int
            INSERT INTO BritishCouncil_TPM.[dbo].[ScheduledPackages]
                           ([CenterId]
                           ,[CenterRef]
                           ,[CenterName]
                           ,[QualificationId]
                           ,[QualificationRef]
                           ,[QualificationName]
                           ,[StartDate]
                           ,[EndDate]
                           ,[StartTime]
                           ,[EndTime]
                           ,[DateCreated]
                           ,[CreatedBy]
                           ,[StatusValue]
                           ,[PackageId]
                           ,[PackageDate])
                  SELECT TOP 1
                              CT.CentreID
                              ,CT.CentreCode
                              ,CT.CentreName
                              ,Q.ID
                              ,Q.QualificationRef
                              ,Q.QualificationName
                              ,SEXT.ScheduledStartDateTime
                              ,SEXT.ScheduledEndDateTime
                              ,'00:01' AS [StartTime] -- TODO: check 
                              ,'23:59' AS [EndTime] -- TODO: check 
                              ,SEXT.CreatedDateTime
                              ,'aptismanagermorocco' AS [CreatedBy] -- TODO: check
                              ,0 AS [StatusValue] -- Editable 
                              ,@testPackageId AS [PackageId]
                              ,GETDATE() AS [PackageDate]
                        FROM @examSessions ES
                              JOIN WAREHOUSE_ExamSessionTable EST ON EST.ID = ES.id
                              JOIN WAREHOUSE_ScheduledExamsTable SEXT ON SEXT.ID = EST.WAREHOUSEScheduledExamID
                              JOIN WAREHOUSE_CentreTable CT ON CT.ID = SEXT.WAREHOUSECentreID
                              JOIN IB3QualificationLookup Q ON Q.ID = SEXT.qualificationId
                        WHERE ES.candidateId = @candidateId

            SELECT @scheduledPackageId = SCOPE_IDENTITY();

            -- Add one row to ScheduledPackageCandidates
            DECLARE @scheduledPackageCandidateId int
            INSERT INTO BritishCouncil_TPM.[dbo].[ScheduledPackageCandidates]
                           ([SurpassCandidateId]
                           ,[SurpassCandidateRef]
                           ,[FirstName]
                           ,[LastName]
                           ,[MiddleName]
                           ,[BirthDate]
                           ,[IsCompleted]
                           ,[DateCompleted]
                           ,[PackageScore]
                           ,[ScheduledPackage_ScheduledPackageId]
                           ,[IsVoided])
                  SELECT 
                               U.id
                              ,U.CandidateRef
                              ,U.Forename
                              ,U.Surname
                              ,U.Middlename
                              ,U.DOB
                              ,0 AS [IsCompleted]
                              ,NULL AS [DateCompleted]
                              ,NULL AS [PackageScore]
                              ,@scheduledPackageId AS [ScheduledPackage_ScheduledPackageId]
                              ,0 AS [IsVoided]
                        FROM WAREHOUSE_UserTable U
                        WHERE U.ID = @candidateId
            
            SELECT @scheduledPackageCandidateId = SCOPE_IDENTITY();

            -- ScheduledPackageCandidateExams - add as many rows as exams in the package
            
            INSERT INTO BritishCouncil_TPM.[dbo].[ScheduledPackageCandidateExams]
                           (
                           [ScheduledExamRef]
                           ,[PackageExamId]
                           ,[IsCompleted]
                           ,[DateCompleted]
                           ,[Score]
                           ,[Candidate_ScheduledPackageCandidateId]
                           ,[IsVoided]
                           ,[ShouldIncludeScale]
                           ,[ShouldIncludeCEFR]
                           ,[ShouldIncludeFinalScore]
                           ,[IsRescheduled]
                           )
                                                      
                      SELECT 
							EST.KeyCode
							   -- SEXT.ExamRef
							  ,/*PE.PackageExamId*/ (SELECT PackageExamId FROM BritishCouncil_TPM.[dbo].PackageExams PE WHERE PE.SurpassExamId = SEXT.ExamID AND PE.PackageId = @TestPackageId) AS PackageExamId
							  ,0 AS [IsCompleted]
							  ,NULL AS [DateCompleted]
							  ,NULL AS [Score]
							  ,@scheduledPackageCandidateId AS [Candidate_ScheduledPackageCandidateId]
							  ,0 AS [IsVoided]
							  ,/*PE.ShouldIncludeScale*/ (SELECT ShouldIncludeScale FROM BritishCouncil_TPM.[dbo].PackageExams PE WHERE PE.SurpassExamId = SEXT.ExamID AND PE.PackageId = @TestPackageId) AS ShouldIncludeScale
							  ,/*PE.ShouldIncludeCEFR*/ (SELECT ShouldIncludeCEFR FROM BritishCouncil_TPM.[dbo].PackageExams PE WHERE PE.SurpassExamId = SEXT.ExamID AND PE.PackageId = @TestPackageId) AS ShouldIncludeCEFR
							  ,/*PE.ShouldIncludeFinalScore*/ (SELECT ShouldIncludeFinalScore FROM BritishCouncil_TPM.[dbo].PackageExams PE WHERE PE.SurpassExamId = SEXT.ExamID AND PE.PackageId = @TestPackageId) AS ShouldIncludeFinalScore
							  ,0 AS [IsRescheduled]
						FROM @examSessions ES
                              JOIN WAREHOUSE_ExamSessionTable EST ON EST.ID = ES.id
                              JOIN WAREHOUSE_ScheduledExamsTable SEXT ON SEXT.ID = EST.WAREHOUSEScheduledExamID
                              --JOIN BritishCouncil_TPM.[dbo].PackageExams PE ON PE.SurpassExamId = SEXT.ExamID
                        WHERE ES.candidateId = @candidateId
						
      END
      
select SP.* from BritishCouncil_TPM..ScheduledPackageCandidateExams as SPCE

	inner join BritishCouncil_TPM..ScheduledPackageCandidates as SPC
	on SPC.ScheduledPackageCandidateId =  SPCE.Candidate_ScheduledPackageCandidateId
	
	inner join BritishCouncil_TPM..ScheduledPackages as SP
	on SP.ScheduledPackageId = SPC.ScheduledPackage_ScheduledPackageId
	
where ScheduledExamRef in ('ZUF2BE01','MNTCAG01','N2URWJ01','V2N6WJ01','DFEW4601')

use BritishCouncil_TPM
exec sp_executesql N'SELECT 
    [GroupBy1].[A1] AS [C1]
    FROM ( SELECT 
        COUNT(1) AS [A1]
        FROM   (SELECT [Extent1].[ScheduledPackageCandidateId] AS [ScheduledPackageCandidateId], [Extent1].[SurpassCandidateRef] AS [SurpassCandidateRef], [Extent1].[FirstName] AS [FirstName], [Extent1].[LastName] AS [LastName], [Extent1].[ScheduledPackage_ScheduledPackageId] AS [ScheduledPackage_ScheduledPackageId], [Extent2].[CenterName] AS [CenterName], [Extent3].[Name] AS [Name]
            FROM   [dbo].[ScheduledPackageCandidates] AS [Extent1]
            INNER JOIN [dbo].[ScheduledPackages] AS [Extent2] ON [Extent1].[ScheduledPackage_ScheduledPackageId] = [Extent2].[ScheduledPackageId]
            INNER JOIN [dbo].[Packages] AS [Extent3] ON [Extent2].[PackageId] = [Extent3].[PackageId]
            WHERE ([Extent1].[IsCompleted] = 0) AND ([Extent1].[IsVoided] <> cast(1 as bit)) ) AS [Filter1]
        LEFT OUTER JOIN [dbo].[ScheduledPackages] AS [Extent4] ON [Filter1].[ScheduledPackage_ScheduledPackageId] = [Extent4].[ScheduledPackageId]
        WHERE ( EXISTS (SELECT 
            1 AS [C1]
            FROM    [dbo].[UserCenterAccesses] AS [Extent5]
            INNER JOIN [dbo].[ScheduledPackages] AS [Extent6] ON [Extent5].[CenterId] = [Extent6].[CenterId]
            INNER JOIN [dbo].[ScheduledPackages] AS [Extent7] ON 1 = 1
            LEFT OUTER JOIN  (SELECT 
                [Extent8].[ScheduledPackageId] AS [ScheduledPackageId]
                FROM [dbo].[ScheduledPackages] AS [Extent8]
                WHERE [Filter1].[ScheduledPackage_ScheduledPackageId] = [Extent8].[ScheduledPackageId] ) AS [Project1] ON 1 = 1
            WHERE ([Filter1].[ScheduledPackage_ScheduledPackageId] = [Extent6].[ScheduledPackageId]) AND ([Filter1].[ScheduledPackage_ScheduledPackageId] = [Extent7].[ScheduledPackageId]) AND ([Extent5].[UserName] = @p__linq__0) AND (@p__linq__0 IS NOT NULL)
        )) AND ( EXISTS (SELECT 
            1 AS [C1]
            FROM    [dbo].[UserQualificationAccesses] AS [Extent9]
            INNER JOIN [dbo].[ScheduledPackages] AS [Extent10] ON [Extent9].[QualificationId] = [Extent10].[QualificationId]
            INNER JOIN [dbo].[ScheduledPackages] AS [Extent11] ON 1 = 1
            LEFT OUTER JOIN  (SELECT 
                [Extent12].[ScheduledPackageId] AS [ScheduledPackageId]
                FROM [dbo].[ScheduledPackages] AS [Extent12]
                WHERE [Filter1].[ScheduledPackage_ScheduledPackageId] = [Extent12].[ScheduledPackageId] ) AS [Project3] ON 1 = 1
            WHERE ([Filter1].[ScheduledPackage_ScheduledPackageId] = [Extent10].[ScheduledPackageId]) AND ([Filter1].[ScheduledPackage_ScheduledPackageId] = [Extent11].[ScheduledPackageId]) AND ([Extent9].[UserName] = @p__linq__1) AND (@p__linq__1 IS NOT NULL)
        )) AND (([Filter1].[SurpassCandidateRef] LIKE N''%ZUF2BE01%'') OR ([Filter1].[FirstName] LIKE N''%ZUF2BE01%'') OR ([Filter1].[LastName] LIKE N''%ZUF2BE01%'') OR ([Filter1].[Name] LIKE N''%ZUF2BE01%'') OR ([Filter1].[CenterName] LIKE N''%ZUF2BE01%'') OR ([Extent4].[QualificationName] LIKE N''%ZUF2BE01%'') OR ( EXISTS (SELECT 
            1 AS [C1]
            FROM [dbo].[ScheduledPackageCandidateExams] AS [Extent13]
            WHERE ([Filter1].[ScheduledPackageCandidateId] = [Extent13].[Candidate_ScheduledPackageCandidateId]) AND ([Extent13].[ScheduledExamRef] LIKE N''%ZUF2BE01%'')
        )))
    )  AS [GroupBy1]',N'@p__linq__0 nvarchar(4000),@p__linq__1 nvarchar(4000)',@p__linq__0=N'superuser',@p__linq__1=N'superuser'
    
    
ROLLBACK TRAN