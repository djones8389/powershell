SET	TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

Select
	EST.ID
	, KeyCode
	, CandidateRef
	, CentreCode
	, purchaseOrder
	, examName
	, StateChangeDate as 'State 6 time'
	, sum(s.i.value('@viewingTime[1]','int')) as ViewingTimes
	from ExamSessionTable as EST

	inner join ScheduledExamsTable as SCET
	on SCET.ID = EST.ScheduledExamID
	
	LEFT join ExamStateChangeAuditTable as ESCAT
	on ESCAT.ExamSessionID = EST.ID
	
	Inner join UserTable as UT
	on UT.ID = EST.UserID
	
	inner join CentreTable as CT
	on CT.ID = SCET.CentreID
  
	cross apply StructureXML.nodes('assessmentDetails/assessment/section/item') s(i)

	where newState = 6
		GROUP BY 	EST.ID	, KeyCode	, CandidateRef	, CentreCode	, purchaseOrder	, examName	, StateChangeDate	
				 having (sum(s.i.value('@viewingTime[1]','int'))) = 0

	UNION


select 	
	west.ExamSessionID as ID
	, west.KeyCode
	, candidateRef
	, centreCode
	, purchaseOrder
	, wests.examName
	, west.warehouseTime
	, sum(s.i.value('@viewingTime[1]','int')) as ViewingTimes

	from  Warehouse_ExamSessionTable as WEST 
		
	inner join WAREHOUSE_ScheduledExamsTable as WSCET 
	on WSCET.ID = WEST.WAREHOUSEScheduledExamID

	inner join WAREHOUSE_ExamSessionTable_Shreded as WESTS 
	on WESTS.examSessionId = WEST.ID

	
	cross apply west.resultData.nodes('exam/section/item') s(i)

	where  west.warehousetime > '14 Apr 2015 00:00:01'
		and examStateChangeAuditXMl.exist('exam/stateChange[newStateID=6]') = 1
	  group by WEST.ExamSessionID, west.KeyCode, candidateRef, centreCode, purchaseOrder, wests.examName, west.warehouseTime
	  
	  having (sum(s.i.value('@viewingTime[1]','int'))) = 0;
