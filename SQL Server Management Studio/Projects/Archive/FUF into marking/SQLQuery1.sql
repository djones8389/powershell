USE AAT_SecureAssess_LIVE_28_01_2015

--Populate @ID and @ItemESID accordingly once exam is in Warehouse--

declare @Keycode nvarchar(10) = 'LWNDSBA6';
declare @ID int = 1568808;
declare @ItemESID int = 1563194;

--Find Exam in Live--

select ExamSessionTable.id, keycode, examstate, StructureXML, resultData, resultDataFull 
, Forename, Surname
      from  ExamSessionTable 

inner join UserTable on usertable.id = ExamSessionTable.UserID

where KeyCode =  @Keycode;


--Find Exam in Warehouse--

select id, ExamSessionID ,keycode, StructureXML, resultData, resultDataFull  
      from  Warehouse_ExamSessionTable 
            where ExamSessionID = (Select ID from ExamSessionTable where KeyCode = @Keycode);

--Void exam for FUF--

update ExamSessionTable
set previousExamState = examState, examState = 10
where ID =  @ID;

--Run FUF,  then replace XML's

update ExamSessionTable 
set StructureXML = (select StructureXML from Warehouse_ExamSessionTable where ExamSessionID = @ID)
      , resultData = (select resultData from Warehouse_ExamSessionTable where ExamSessionID = @ID)
      , resultDataFull = (select resultDataFull from Warehouse_ExamSessionTable where ExamSessionID = @ID)
where ID = @ID;


--Put exam to State 9 > will go to State 15

update ExamSessionTable
set previousExamState = examState, examState = 9
where ID =  @ID;


--Update ItemResponses

set identity_insert ExamSessionItemResponseTable ON

Insert into ExamSessionItemResponseTable(ID, ExamSessionID, ItemID, ItemVersion, ItemResponseData, MarkerResponseData, ItemMark, MarkingIgnored)
select ID, @ID, ItemID, ItemVersion, ItemResponseData, MarkerResponseData, ItemMark, MarkingIgnored
from WAREHOUSE_ExamSessionItemResponseTable  where WAREHOUSEExamSessionID = @ItemESID
      
set identity_insert ExamSessionItemResponseTable OFF        
            
select * from ExamSessionItemResponseTable where ExamSessionID = @ID