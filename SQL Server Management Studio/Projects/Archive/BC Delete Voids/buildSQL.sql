

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[temp_WAREHOUSE_ExamSessionAvailableItemsTable]') AND type in (N'U'))
DROP TABLE [dbo].[temp_WAREHOUSE_ExamSessionAvailableItemsTable];

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[temp_WAREHOUSE_ExamSessionDocumentTable]') AND type in (N'U'))
DROP TABLE [dbo].[temp_WAREHOUSE_ExamSessionDocumentTable];

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[temp_WAREHOUSE_ExamSessionItemResponseTable]') AND type in (N'U'))
DROP TABLE [dbo].[temp_WAREHOUSE_ExamSessionItemResponseTable];

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[temp_WAREHOUSE_ExamSessionTable]') AND type in (N'U'))
DROP TABLE [dbo].[temp_WAREHOUSE_ExamSessionTable];

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[temp_WAREHOUSE_ExamSessionTable_ShreddedItems_Mark]') AND type in (N'U'))
DROP TABLE [dbo].[temp_WAREHOUSE_ExamSessionTable_ShreddedItems_Mark];

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[temp_WAREHOUSE_ExamSessionTable_Shreded]') AND type in (N'U'))
DROP TABLE [dbo].[temp_WAREHOUSE_ExamSessionTable_Shreded];

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[temp_WAREHOUSE_ExamSessionTable_ShrededItems]') AND type in (N'U'))
DROP TABLE [dbo].[temp_WAREHOUSE_ExamSessionTable_ShrededItems];

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[temp_WAREHOUSE_ExamStateAuditTable]') AND type in (N'U'))
DROP TABLE [dbo].[temp_WAREHOUSE_ExamStateAuditTable];

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[temp_WAREHOUSE_ScheduledExamsTable]') AND type in (N'U'))
DROP TABLE [dbo].[temp_WAREHOUSE_ScheduledExamsTable];

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[temp_WAREHOUSE_ExamSessionResultHistoryTable]') AND type in (N'U'))
DROP TABLE [dbo].[temp_WAREHOUSE_ExamSessionResultHistoryTable];


CREATE TABLE [dbo].[temp_WAREHOUSE_ExamSessionAvailableItemsTable](
	[ID] [int] NOT NULL,
	[ExamSessionID] [int] NOT NULL,
	[ItemID] [nvarchar](50) NOT NULL,
	[ItemVersion] [int] NOT NULL,
	[ItemXML] [xml] NOT NULL
);

CREATE TABLE [dbo].[temp_WAREHOUSE_ExamSessionDocumentTable](
	[ID] [int] NOT NULL,
	[warehouseExamSessionID] [int] NOT NULL,
	[itemId] [nvarchar](20) NOT NULL,
	[documentName] [nvarchar](200) NOT NULL,
	[Document] [image] NOT NULL,
	[uploadDate] [datetime] NOT NULL
);

CREATE TABLE [dbo].[temp_WAREHOUSE_ExamSessionItemResponseTable](
	[ID] [int] NOT NULL,
	[WAREHOUSEExamSessionID] [int] NOT NULL,
	[ItemID] [nvarchar](15) NOT NULL,
	[ItemVersion] [int] NOT NULL,
	[ItemResponseData] [xml] NULL,
	[MarkerResponseData] [xml] NULL,
	[ItemMark] [int] NULL,
	[MarkingIgnored] [tinyint] NULL,
	[DerivedResponse] [nvarchar](max) NULL,
	[ShortDerivedResponse] [nvarchar](500) NULL
);

CREATE TABLE [dbo].[temp_WAREHOUSE_ExamSessionResultHistoryTable](
	[ID] [int] NOT NULL,
	[resultData] [xml] NULL,
	[resultDataFull] [xml] NULL,
	[warehouseTime] [datetime] NULL,
	[WareHouseExamSessionTableID] [int] NOT NULL,
	[UserID] [int] NULL
);

CREATE TABLE [dbo].[temp_WAREHOUSE_ExamSessionTable](
	[ID] [int] NOT NULL,
	[ExamSessionID] [int] NOT NULL,
	[WAREHOUSEScheduledExamID] [int] NOT NULL,
	[WAREHOUSEUserID] [int] NOT NULL,
	[StructureXML] [xml](CONTENT [dbo].[structureXmlXsd]) NULL,
	[MarkerData] [xml] NULL,
	[KeyCode] [varchar](12) NOT NULL,
	[PreviousExamState] [int] NOT NULL,
	[pinNumber] [nvarchar](15) NULL,
	[ExamStateChangeAuditXml] [xml] NOT NULL,
	[resultData] [xml] NULL,
	[resultDataFull] [xml] NULL,
	[warehouseTime] [datetime] NULL,
	[completionDate] [datetime] NULL,
	[CQN] [nvarchar](20) NULL,
	[examIPAuditData] [xml] NULL,
	[appeal] [bit] NULL,
	[reMarkStatus] [int] NOT NULL,
	[reMarkUser] [int] NULL,
	[downloadInformation] [xml] NULL,
	[clientInformation] [xml] NULL,
	[availableForCentreReview] [bit] NULL,
	[ExportToSecureMarker] [bit] NOT NULL,
	[WarehouseExamState] [int] NOT NULL,
	[AllowPackageDelivery] [bit] NOT NULL,
	[ContainsBTLOffice] [bit] NULL,
	[ExportedToIntegration] [bit] NULL,
	[copiedFromExamSessionID] [int] NULL,
	[submissionExported] [bit] NOT NULL,
	[EnableOverrideMarking] [bit] NOT NULL,
	[TargetedForVoid] [xml] NULL,
	[itemMarksUploaded] [bit] NOT NULL,
	[TakenThroughLocalScan] [bit] NOT NULL,
	[LocalScanDownloadDate] [datetime] NULL,
	[LocalScanUploadDate] [datetime] NULL,
	[LocalScanNumPages] [int] NULL,
	[CertifiedForTablet] [bit] NOT NULL,
	[IsProjectBased] [bit] NOT NULL,
	[AssignedMarkerUserID] [int] NULL,
	[AssignedModeratorUserID] [int] NULL
);

CREATE TABLE [dbo].[temp_WAREHOUSE_ExamSessionTable_ShreddedItems_Mark](
	[ExamSessionID] [int] NOT NULL,
	[ItemID] [varchar](15) NOT NULL,
	[Mark] [decimal](6, 3) NOT NULL,
	[LearningOutcome] [int] NOT NULL,
	[DisplayText] [nvarchar](max) NOT NULL,
	[MaxMark] [decimal](6, 3) NOT NULL
);

CREATE TABLE [dbo].[temp_WAREHOUSE_ExamSessionTable_Shreded](
	[examSessionId] [int] NOT NULL,
	[structureXml] [xml](CONTENT [dbo].[structureXmlXsd]) NULL,
	[examVersionName] [nvarchar](max) NULL,
	[examVersionRef] [nvarchar](max) NULL,
	[examVersionId] [int] NULL,
	[examName] [nvarchar](max) NULL,
	[examRef] [nvarchar](max) NULL,
	[qualificationid] [int] NULL,
	[qualificationName] [nvarchar](max) NULL,
	[qualificationRef] [nvarchar](max) NULL,
	[resultData] [xml] NULL,
	[submittedDate] [datetime] NULL,
	[originatorId] [int] NULL,
	[centreName] [nvarchar](max) NULL,
	[centreCode] [nvarchar](max) NULL,
	[foreName] [nvarchar](max) NULL,
	[dateOfBirth] [smalldatetime] NULL,
	[gender] [char](1) NULL,
	[candidateRef] [nvarchar](max) NULL,
	[surName] [nvarchar](max) NULL,
	[scheduledDurationValue] [int] NULL,
	[previousExamState] [int] NULL,
	[examStateInformation] [xml] NULL,
	[examResult] [float] NULL,
	[passValue] [bit] NULL,
	[closeValue] [bit] NULL,
	[ExternalReference] [nvarchar](100) NULL,
	[examType] [int] NULL,
	[warehouseTime] [datetime] NULL,
	[CQN] [nvarchar](20) NULL,
	[actualDuration] [int] NULL,
	[appeal] [bit] NULL,
	[reMarkStatus] [int] NULL,
	[qualificationLevel] [int] NULL,
	[centreId] [int] NULL,
	[ULN] [nvarchar](10) NULL,
	[AllowPackageDelivery] [bit] NOT NULL,
	[ExportToSecureMarker] [bit] NOT NULL,
	[ContainsBTLOffice] [bit] NULL,
	[ExportedToIntegration] [bit] NULL,
	[WarehouseExamState] [int] NOT NULL,
	[grade]  AS (case when [previousExamState]=(10) then 'Voided' when NOT isnull([dbo].[fn_getValueFromResultData]([resultData],'GRADE'),'')='' then [dbo].[fn_getValueFromResultData]([resultData],'GRADE') when CONVERT([int],[dbo].[fn_getValueFromResultData]([resultData],'PASSVALUE'),(0))=(1) then 'Pass' when CONVERT([int],[dbo].[fn_getValueFromResultData]([resultData],'PASSVALUE'),(0))=(0) AND CONVERT([int],[dbo].[fn_getValueFromResultData]([resultData],'CLOSEVALUE'),(0))=(1) then 'Close' else 'Fail' end) PERSISTED,
	[userMark]  AS (CONVERT([float],[dbo].[fn_getValueFromResultData]([resultData],'USERMARK'),(0))) PERSISTED,
	[userPercentage]  AS (CONVERT([float],[dbo].[fn_getValueFromResultData]([resultData],'USERPERCENTAGE'),(0))) PERSISTED,
	[adjustedGrade]  AS (case when NOT isnull([dbo].[fn_getValueFromResultData]([resultData],'ORIGINALGRADE'),[dbo].[fn_getValueFromResultData]([resultData],'GRADE'))=[dbo].[fn_getValueFromResultData]([resultData],'GRADE') then case when NOT isnull([dbo].[fn_getValueFromResultData]([resultData],'GRADE'),'')='' then [dbo].[fn_getValueFromResultData]([resultData],'GRADE') when CONVERT([int],[dbo].[fn_getValueFromResultData]([resultData],'PASSVALUE'),(0))=(1) then 'Pass' when CONVERT([int],[dbo].[fn_getValueFromResultData]([resultData],'PASSVALUE'),(0))=(0) AND CONVERT([int],[dbo].[fn_getValueFromResultData]([resultData],'CLOSEVALUE'),(0))=(1) then 'Close' else 'Fail' end else '' end) PERSISTED,
	[language] [nchar](10) NULL,
	[KeyCode] [varchar](12) NOT NULL,
	[TargetedForVoid] [xml] NULL,
	[EnableOverrideMarking] [bit] NOT NULL,
	[originalGrade]  AS (case when NOT isnull([dbo].[fn_getValueFromResultData]([resultData],'ORIGINALGRADE'),[dbo].[fn_getValueFromResultData]([resultData],'GRADE'))=[dbo].[fn_getValueFromResultData]([resultData],'GRADE') then [dbo].[fn_getValueFromResultData]([resultData],'ORIGINALGRADE') else case when [previousExamState]=(10) then 'Voided' when NOT isnull([dbo].[fn_getValueFromResultData]([resultData],'GRADE'),'')='' then [dbo].[fn_getValueFromResultData]([resultData],'GRADE') when CONVERT([int],[dbo].[fn_getValueFromResultData]([resultData],'PASSVALUE'),(0))=(1) then 'Pass' when CONVERT([int],[dbo].[fn_getValueFromResultData]([resultData],'PASSVALUE'),(0))=(0) AND CONVERT([int],[dbo].[fn_getValueFromResultData]([resultData],'CLOSEVALUE'),(0))=(1) then 'Close' else 'Fail' end end),
	[IsExternal] [bit] NOT NULL,
	[AddressLine1] [nvarchar](100) NULL,
	[AddressLine2] [nvarchar](100) NULL,
	[Town] [nvarchar](100) NULL,
	[County] [nvarchar](70) NULL,
	[Country] [nvarchar](50) NULL,
	[Postcode] [nvarchar](12) NULL,
	[ScoreBoundaryData] [xml] NULL
);

CREATE TABLE [dbo].[temp_WAREHOUSE_ExamSessionTable_ShrededItems](
	[examSessionId] [int] NOT NULL,
	[ItemRef] [varchar](15) NULL,
	[ItemName] [nvarchar](200) NULL,
	[userMark] [decimal](6, 3) NULL,
	[markerUserMark] [nvarchar](max) NULL,
	[examPercentage] [decimal](6, 3) NULL,
	[candidateId] [int] IDENTITY(1,1) NOT NULL,
	[ItemVersion] [int] NULL,
	[responsexml] [xml] NULL,
	[OptionsChosen] [varchar](200) NULL,
	[selectedCount] [int] NULL,
	[correctAnswerCount] [int] NULL,
	[TotalMark] [decimal](6, 3) NOT NULL,
	[userAttempted] [bit] NULL,
	[markingIgnored] [tinyint] NULL,
	[markedMetadataCount] [int] NULL,
	[ItemType] [int] NULL,
	[FriendItems] [nvarchar](max) NULL
);

CREATE TABLE [dbo].[temp_WAREHOUSE_ExamStateAuditTable](
	[WarehouseExamSessionID] [int] NOT NULL,
	[ExamState] [int] NOT NULL,
	[Date] [datetime] NOT NULL
);

CREATE TABLE [dbo].[temp_WAREHOUSE_ScheduledExamsTable](
	[ID] [int] NOT NULL,
	[ScheduledExamID] [int] NOT NULL,
	[ExamID] [int] NOT NULL,
	[WAREHOUSECentreID] [int] NOT NULL,
	[WAREHOUSECreatedBy] [int] NOT NULL,
	[CreatedDateTime] [datetime] NOT NULL,
	[ScheduledStartDateTime] [datetime] NOT NULL,
	[ScheduledEndDateTime] [datetime] NOT NULL,
	[ActiveStartTime] [int] NOT NULL,
	[ActiveEndTime] [int] NOT NULL,
	[qualificationId] [int] NOT NULL,
	[examName] [nvarchar](200) NULL,
	[groupState] [int] NOT NULL,
	[invigilated] [bit] NOT NULL,
	[humanMarked] [bit] NOT NULL,
	[AdvanceContentDownloadTimespanInHours] [int] NOT NULL,
	[OriginatorID] [int] NOT NULL,
	[qualificationName] [nvarchar](100) NULL,
	[examVersionId] [int] NULL,
	[ExternalReference] [nvarchar](100) NULL,
	[QualificationRef] [nvarchar](100) NULL,
	[IsExternal] [bit] NOT NULL,
	[language] [nchar](10) NULL,
	[qualificationLevel] [int] NULL,
	[purchaseOrder] [nvarchar](100) NULL
);


IF OBJECT_ID('tempdb..#AffectedExams') IS NOT NULL DROP TABLE #AffectedExams;

SELECT  WEST.ID as ExamSessionID, WSCET.ID as ScheduledID, '' as BeenDeleted
INTO #AffectedExams
FROM WAREHOUSE_ExamSessionTable as WEST (NOLOCK)

  Inner Join WAREHOUSE_ScheduledExamsTable as WSCET (NOLOCK)
  on WSCET.ID = WEST.WAREHOUSEScheduledExamID
  
 where PreviousExamState = 10 
	and warehouseTime <= DATEADD (month , -6 , GETDATE())
	and reMarkStatus != 2		--Check this
  order by warehouseTime asc;

--02:18 up to here--


INSERT INTO  [dbo].[temp_WAREHOUSE_ExamSessionAvailableItemsTable]
select * FROM [WAREHOUSE_ExamSessionAvailableItemsTable]
	where ExamSessionID not in (
		select ExamSessionID FROM #AffectedExams
		);	--05:16
		
INSERT INTO  [dbo].[temp_WAREHOUSE_ExamSessionDocumentTable]
select * FROM [WAREHOUSE_ExamSessionDocumentTable]
	where warehouseExamSessionID not in (
		select ExamSessionID FROM #AffectedExams
		);

INSERT INTO  [dbo].[temp_WAREHOUSE_ExamSessionItemResponseTable]
select * FROM [WAREHOUSE_ExamSessionItemResponseTable]
	where warehouseExamSessionID not in (
		select ExamSessionID FROM #AffectedExams
		);

INSERT INTO  [dbo].[temp_WAREHOUSE_ExamSessionResultHistoryTable]
select * FROM [WAREHOUSE_ExamSessionResultHistoryTable]
	where WareHouseExamSessionTableID not in (
		select ExamSessionID FROM #AffectedExams
		);
		
		
INSERT INTO  [dbo].[temp_WAREHOUSE_ExamSessionTable]
select * FROM [WAREHOUSE_ExamSessionTable]
	where ExamSessionID not in (
		select ExamSessionID FROM #AffectedExams
		);
		
INSERT INTO  [dbo].[temp_WAREHOUSE_ExamSessionTable_ShreddedItems_Mark]
select * FROM [WAREHOUSE_ExamSessionTable_ShreddedItems_Mark]
	where ExamSessionID not in (
		select ExamSessionID FROM #AffectedExams
		);

INSERT INTO  [dbo].[temp_WAREHOUSE_ExamSessionTable_Shreded]
select * FROM [WAREHOUSE_ExamSessionTable_Shreded]
	where examSessionId not in (
		select ExamSessionID FROM #AffectedExams
		);

INSERT INTO  [dbo].[temp_WAREHOUSE_ExamSessionTable_ShrededItems]
select * FROM [WAREHOUSE_ExamSessionTable_ShrededItems]
	where examSessionId not in (
		select ExamSessionID FROM #AffectedExams
		);
		
INSERT INTO  [dbo].[temp_WAREHOUSE_ExamStateAuditTable]
select * FROM [WAREHOUSE_ExamStateAuditTable]
	where WarehouseExamSessionID not in (
		select ExamSessionID FROM #AffectedExams
		);

INSERT INTO  [dbo].[temp_WAREHOUSE_ScheduledExamsTable]
select * FROM [WAREHOUSE_ScheduledExamsTable]
	where ID not in (
		select ScheduledID FROM #AffectedExams
		);
		