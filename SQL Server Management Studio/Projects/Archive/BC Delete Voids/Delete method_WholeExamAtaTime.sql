/*
CREATE TABLE ##ExamsToDelete (

	ESID int
	, SCETID int
	, CentreID int
	, Deleted bit
)

CREATE INDEX [IX_DeleteExams] ON ##ExamsToDelete
(
	ESID ASC
)
INCLUDE (
   DELETED
)

INSERT INTO ##ExamsToDelete
select west.ID 
	, wscet.ID 
	, wscet.WAREHOUSECentreID 		
	, '0' 
from WAREHOUSE_ExamSessionTable as west
inner join WAREHOUSE_ScheduledExamsTable as wscet
on west.WAREHOUSEScheduledExamID = wscet.ID
where warehouseTime <= DATEADD(Month, -6, GETDATE())
	and PreviousExamState = 10;
*/

--CREATE TABLE Deleted_Exams (
--	ESID int
--	, WAREHOUSE_ExamSessionDocumentTable bit
--)


DECLARE @ExamsToDelete smallint = 1000;	
	
DECLARE DeleteExam CURSOR FOR
SELECT TOP(@ExamsToDelete) ESID, SCETID, CENTREID
FROM ##ExamsToDelete
where Deleted = 0
Order by ESID;

OPEN DeleteExam;

DECLARE @ESID int, @SCETID int, @CentreID int;

FETCH NEXT FROM DeleteExam INTO @ESID, @SCETID, @CentreID;

WHILE @@FETCH_STATUS = 0

BEGIN

		WHILE @ExamsToDelete <= 100
		 BEGIN
			
			SET @ExamsToDelete = @ExamsToDelete + 1
		
		 END
		 
PRINT 'DELETE from WAREHOUSE_ExamSessionDocumentTable where ID IN (SELECT ID FROM WAREHOUSE_ExamSessionDocumentTable where warehouseExamSessionID = ' + CONVERT(nvarchar(10), @ESID) +')' + '; ' 
PRINT 'DELETE from WAREHOUSE_ExamStateAuditTable where WAREHOUSEExamSessionID = ' + CONVERT(nvarchar(10), @ESID) + ';'
PRINT 'DELETE from WAREHOUSE_ExamSessionDurationAuditTable where ID IN (SELECT ID FROM WAREHOUSE_ExamSessionDurationAuditTable where Warehoused_ExamSessionID = ' + CONVERT(nvarchar(10), @ESID) +')' + ';'
PRINT 'DELETE from WAREHOUSE_CentreTable where ID = ' + CONVERT(nvarchar(10), @CentreID) + ';'
PRINT 'DELETE from WAREHOUSE_ScheduledExamsTable where ID = ' + CONVERT(nvarchar(10), @SCETID) + ';'
PRINT 'DELETE from WAREHOUSE_ExamSessionAvailableItemsTable where ID IN (SELECT ID FROM WAREHOUSE_ExamSessionAvailableItemsTable where ExamSessionID = ' + CONVERT(nvarchar(10), @ESID) +')' + ';'
PRINT 'DELETE from WAREHOUSE_ExamSessionResultHistoryTable where ID IN (SELECT ID FROM WAREHOUSE_ExamSessionResultHistoryTable where WareHouseExamSessionTableID = ' + CONVERT(nvarchar(10), @ESID) +')' + ';'
PRINT 'DELETE from WAREHOUSE_ExamSessionTable_ShrededItems where examSessionID = ' + CONVERT(nvarchar(10), @ESID) + ';'
PRINT 'DELETE from WAREHOUSE_ExamSessionTable_Shreded where examSessionID = ' + CONVERT(nvarchar(10), @ESID) + ';'
PRINT 'DELETE from WAREHOUSE_ExamSessionTable where ID = ' + CONVERT(nvarchar(10), @ESID) + ';'

UPDATE ##ExamsToDelete
set Deleted = 1
where ESID = @ESID;
		 
FETCH NEXT FROM DeleteExam INTO @ESID, @SCETID, @CentreID;

END
CLOSE DeleteExam;
DEALLOCATE DeleteExam;
 


SELECT * FROM ##ExamsToDelete order by 1;


--UPDATE ##ExamsToDelete
--SET DELETED = 0



--SELECT * FROM Deleted_Exams


--PRINT 'OUTPUT DELETED.ID, ''WAREHOUSE_ExamSessionDocumentTable'' INTO Deleted_Exams VALUES('''+CONVERT(nvarchar(10), @ESID) +''',''1'')'

--BEGIN TRAN

--DECLARE @WAREHOUSE_ExamSessionDocumentTable TABLE (ESID INT)

--DELETE from WAREHOUSE_ExamSessionDocumentTable where ID IN (SELECT ID FROM WAREHOUSE_ExamSessionDocumentTable where warehouseExamSessionID = 1)
--OUTPUT DELETED.ID
--	INTO @WAREHOUSE_ExamSessionDocumentTable 
--	  VALUES('1')
--ROLLBACK
--(1 row(s) affected)

--DECLARE DeleteExam CURSOR FOR
--SELECT ESID, SCETID, CENTREID
--FROM ##ExamsToDelete

--OPEN DeleteExam;

--DECLARE @ESID int, @SCETID int, @CentreID int;

--FETCH NEXT FROM DeleteExam INTO @ESID, @SCETID, @CentreID;

--WHILE @@FETCH_STATUS = 0

--BEGIN

--PRINT 'DELETE FROM WAREHOUSE_ExamSessionDocumentTable WHERE ID IN (

--SELECT ID
--from WAREHOUSE_ExamSessionDocumentTable
--where warehouseExamSessionID = ' + CONVERT(nvarchar(15), @ESID) + '

--)'

--FETCH NEXT FROM DeleteExam INTO @ESID, @SCETID, @CentreID;

--END
--CLOSE DeleteExam
--DEALLOCATE DeleteExam





DELETE from WAREHOUSE_ExamSessionDocumentTable where ID IN (
	select top 1 ID
	from WAREHOUSE_ExamSessionDocumentTable 
	where warehouseExamSessionID in (SELECT ESID from ##ExamsToDelete)
	GROUP BY ID		
	ORDER BY ID

)



BEGIN TRAN

DELETE from WAREHOUSE_ExamSessionDocumentTable WHERE ID IN (

SELECT ID
from WAREHOUSE_ExamSessionDocumentTable
where warehouseExamSessionID in (SELECT ESID from ##ExamsToDelete)

)

ROLLBACK


select 'DELETE from WAREHOUSE_ExamSessionDocumentTable where ID = ' + CONVERT(nvarchar(10), ID) + ';'
from WAREHOUSE_ExamSessionDocumentTable 
where warehouseExamSessionID in (SELECT ESID from ##ExamsToDelete)
GROUP BY ID		
ORDER BY ID;

select 'DELETE from WAREHOUSE_ExamStateAuditTable where WAREHOUSEExamSessionID = ' + CONVERT(nvarchar(10), WAREHOUSEExamSessionID) + ';'
from WAREHOUSE_ExamStateAuditTable where WAREHOUSEExamSessionID in (SELECT ESID from ##ExamsToDelete)
GROUP BY warehouseExamSessionID
ORDER BY warehouseExamSessionID;

select 'DELETE from WAREHOUSE_ExamSessionDurationAuditTable where ID = ' + CONVERT(nvarchar(10), ID) + ';'
from WAREHOUSE_ExamSessionDurationAuditTable 
where Warehoused_ExamSessionID in (SELECT ESID from ##ExamsToDelete)
GROUP BY ID
ORDER BY ID;

select 'DELETE from WAREHOUSE_CentreTable where ID = ' + CONVERT(nvarchar(10), ID) + ';'
from WAREHOUSE_CentreTable 
where ID in (SELECT CentreID from ##ExamsToDelete)
GROUP BY ID
ORDER BY ID;

select 'DELETE from WAREHOUSE_ScheduledExamsTable where ID = ' + CONVERT(nvarchar(10), ID) + ';'
from WAREHOUSE_ScheduledExamsTable 
where ID IN (SELECT SCETID from ##ExamsToDelete)
GROUP BY ID
ORDER BY ID;

select 'DELETE from WAREHOUSE_ExamSessionAvailableItemsTable where ID = ' + CONVERT(nvarchar(10), ID) + ';'
from WAREHOUSE_ExamSessionAvailableItemsTable
where ExamSessionID in (SELECT ESID from ##ExamsToDelete)
GROUP BY ID
ORDER BY ID;

select  'DELETE from WAREHOUSE_ExamSessionItemResponseTable where ID = '  + CONVERT(nvarchar(10), ID) + ';'
FROM WAREHOUSE_ExamSessionItemResponseTable
where WAREHOUSEExamSessionID in (SELECT ESID from ##ExamsToDelete)
group by ID
ORDER by ID;

select 'DELETE from WAREHOUSE_ExamSessionResultHistoryTable where ID = ' + CONVERT(nvarchar(10), ID) + ';'
from WAREHOUSE_ExamSessionResultHistoryTable 
where  WareHouseExamSessionTableID in (SELECT ESID from ##ExamsToDelete)
GROUP BY ID
ORDER BY ID;

select 'DELETE from WAREHOUSE_ExamSessionTable_ShrededItems where examSessionID = ' + CONVERT(nvarchar(10), examSessionID) + ';'
from WAREHOUSE_ExamSessionTable_ShrededItems 
where examSessionID in (SELECT ESID from ##ExamsToDelete)
GROUP BY examSessionID
ORDER BY examSessionID;

select 'DELETE from WAREHOUSE_ExamSessionTable_Shreded where examSessionID = ' + CONVERT(nvarchar(10), examSessionID) + ';'
from WAREHOUSE_ExamSessionTable_Shreded 
where examSessionID in (SELECT ESID from ##ExamsToDelete)
GROUP BY examSessionID
ORDER BY examSessionID;

select 'DELETE from WAREHOUSE_ExamSessionTable where ID = ' + CONVERT(nvarchar(10), ID) + ';'
from WAREHOUSE_ExamSessionTable where ID in (SELECT ESID from ##ExamsToDelete)
GROUP BY ID
ORDER BY ID;











/*
select count(*) from WAREHOUSE_ExamSessionItemResponseTable where WAREHOUSEExamSessionID in (SELECT ESID from ##ExamsToDelete);
select count(*) from WAREHOUSE_ExamSessionAvailableItemsTable where ExamSessionID in (SELECT ESID from ##ExamsToDelete);
select count(*) from WAREHOUSE_ExamSessionDocumentTable where warehouseExamSessionID in (SELECT ESID from ##ExamsToDelete);
select count(*) from WAREHOUSE_ExamSessionTable where ID in (SELECT ESID from ##ExamsToDelete);
select count(*) from WAREHOUSE_ExamSessionTable_Shreded where examSessionID in (SELECT ESID from ##ExamsToDelete);
select count(*) from WAREHOUSE_ExamSessionTable_ShrededItems where examSessionID in (SELECT ESID from ##ExamsToDelete);
select count(*) from WAREHOUSE_ExamStateAuditTable where WAREHOUSEExamSessionID in (SELECT ESID from ##ExamsToDelete);
select count(*) from WAREHOUSE_ScheduledExamsTable where ID IN (SELECT SCETID from ##ExamsToDelete);
select count(*) from WAREHOUSE_ExamSessionDurationAuditTable where Warehoused_ExamSessionID in (SELECT ESID from ##ExamsToDelete);
select count(*) from WAREHOUSE_ExamSessionResultHistoryTable where  WareHouseExamSessionTableID in (SELECT ESID from ##ExamsToDelete);
select count(*) from WAREHOUSE_CentreTable where ID in (SELECT CentreID from ##ExamsToDelete);


USE SANDBOX_BC_SecureAssess

select west.ID as ESID, wscet.ID as SCETID, wscet.WAREHOUSECentreID as CentreID				--320713 Exams
into ##ExamsToDelete
from WAREHOUSE_ExamSessionTable as west
inner join WAREHOUSE_ScheduledExamsTable as wscet
on west.WAREHOUSEScheduledExamID = wscet.ID
where warehouseTime <= DATEADD(Month, -6, GETDATE())
	and PreviousExamState = 10
*/
--DROP TABLE ##ExamsToDelete

*/