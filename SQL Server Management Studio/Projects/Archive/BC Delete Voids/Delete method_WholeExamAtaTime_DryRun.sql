IF OBJECT_ID ('tempdb..##ExamsToDelete') IS NOT NULL DROP TABLE ##ExamsToDelete;


CREATE TABLE ##ExamsToDelete (

	ESID int
	, SCETID int
	, CentreID int
	, Deleted bit
)

CREATE INDEX [IX_DeleteExams] ON ##ExamsToDelete
(
	ESID ASC
)
INCLUDE (
   DELETED
)

INSERT INTO ##ExamsToDelete
select west.ID 
	, wscet.ID 
	, wscet.WAREHOUSECentreID 		
	, '0' 
from WAREHOUSE_ExamSessionTable as west
inner join WAREHOUSE_ScheduledExamsTable as wscet
on west.WAREHOUSEScheduledExamID = wscet.ID
where warehouseTime <= DATEADD(Month, -6, GETDATE())
	and PreviousExamState = 10;


--CREATE TABLE Deleted_Exams (
--	ESID int
--	, WAREHOUSE_ExamSessionDocumentTable bit
--)


DECLARE @ExamsToDelete smallint = 1;	
	
DECLARE DeleteExam CURSOR FOR
SELECT TOP(@ExamsToDelete) ESID, SCETID, CENTREID
FROM ##ExamsToDelete
where Deleted = 0
Order by ESID;

OPEN DeleteExam;

DECLARE @ESID int, @SCETID int, @CentreID int;

FETCH NEXT FROM DeleteExam INTO @ESID, @SCETID, @CentreID;

WHILE @@FETCH_STATUS = 0

BEGIN

		WHILE @ExamsToDelete <= 10
		 BEGIN
			
			SET @ExamsToDelete = @ExamsToDelete + 1
		
		 END

DELETE from WAREHOUSE_ExamSessionDocumentTable where ID IN (SELECT ID FROM WAREHOUSE_ExamSessionDocumentTable where warehouseExamSessionID = CONVERT(nvarchar(10), @ESID));
DELETE from WAREHOUSE_ExamStateAuditTable where WAREHOUSEExamSessionID =  + CONVERT(nvarchar(10), @ESID);
DELETE from WAREHOUSE_ExamSessionDurationAuditTable where ID IN (SELECT ID FROM WAREHOUSE_ExamSessionDurationAuditTable where Warehoused_ExamSessionID =  CONVERT(nvarchar(10), @ESID)) ;
DELETE from WAREHOUSE_CentreTable where ID = CONVERT(nvarchar(10), @CentreID);
DELETE from WAREHOUSE_ScheduledExamsTable where ID =  CONVERT(nvarchar(10), @SCETID);
DELETE from WAREHOUSE_ExamSessionAvailableItemsTable where ID IN (SELECT ID FROM WAREHOUSE_ExamSessionAvailableItemsTable where ExamSessionID = CONVERT(nvarchar(10), @ESID));
DELETE from WAREHOUSE_ExamSessionResultHistoryTable where ID IN (SELECT ID FROM WAREHOUSE_ExamSessionResultHistoryTable where WareHouseExamSessionTableID =  CONVERT(nvarchar(10), @ESID));
DELETE from WAREHOUSE_ExamSessionTable_ShrededItems where examSessionID =CONVERT(nvarchar(10), @ESID);
DELETE from WAREHOUSE_ExamSessionTable_Shreded where examSessionID = CONVERT(nvarchar(10), @ESID);
DELETE from WAREHOUSE_ExamSessionTable where ID = CONVERT(nvarchar(10), @ESID);

UPDATE ##ExamsToDelete
set Deleted = 1
where ESID = @ESID;
		 
FETCH NEXT FROM DeleteExam INTO @ESID, @SCETID, @CentreID;

END
CLOSE DeleteExam;
DEALLOCATE DeleteExam;
 


--SELECT * FROM ##ExamsToDelete order by 1;


--UPDATE ##ExamsToDelete
--SET DELETED = 0


SELECT name, delete_referential_action_desc
FROM sys.foreign_keys
where name like '%Warehouse%'
	and delete_referential_action_desc = 'CASCADE'