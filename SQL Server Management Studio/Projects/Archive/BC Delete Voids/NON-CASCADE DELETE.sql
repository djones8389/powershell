/*
IF OBJECT_ID ('tempdb..##ExamsToDelete') IS NOT NULL DROP TABLE ##ExamsToDelete;


CREATE TABLE ##ExamsToDelete (

	ESID int
	--, SCETID int
	--, CentreID int
	, Deleted bit
)

INSERT INTO ##ExamsToDelete
select west.ID 
	--, wscet.ID 
	--, wscet.WAREHOUSECentreID 		
	, '0' 
from WAREHOUSE_ExamSessionTable as west
inner join WAREHOUSE_ScheduledExamsTable as wscet
on west.WAREHOUSEScheduledExamID = wscet.ID
where warehouseTime <= DATEADD(Month, -6, GETDATE())
	and PreviousExamState = 10;

CREATE INDEX [IX_DeleteExams] ON ##ExamsToDelete
(
	ESID ASC
)
INCLUDE (
	DELETED
)
*/

SET NOCOUNT ON

DECLARE @ExamsToDelete smallint = 500;	
	
DECLARE DeleteExam CURSOR FOR
SELECT TOP(@ExamsToDelete) ESID--, SCETID, CENTREID
FROM ##ExamsToDelete
where Deleted = 0
Order by ESID;

OPEN DeleteExam;

DECLARE @ESID int;--, @SCETID int, @CentreID int;

FETCH NEXT FROM DeleteExam INTO @ESID;--, @SCETID, @CentreID;

WHILE @@FETCH_STATUS = 0

BEGIN

		--WHILE @ExamsToDelete <= 100
		-- BEGIN
			
		--	SET @ExamsToDelete = @ExamsToDelete + 1
		
		-- END
		 
--PRINT @ESID

--------DELETE FROM Warehouse_ExamSessionTable where ID = @ESID;
DELETE from WAREHOUSE_ExamSessionDocumentTable where ID IN (SELECT ID FROM WAREHOUSE_ExamSessionDocumentTable where warehouseExamSessionID = CONVERT(nvarchar(10), @ESID));
DELETE from WAREHOUSE_ExamStateAuditTable where WAREHOUSEExamSessionID =  + CONVERT(nvarchar(10), @ESID);
DELETE from WAREHOUSE_ExamSessionDurationAuditTable where ID IN (SELECT ID FROM WAREHOUSE_ExamSessionDurationAuditTable where Warehoused_ExamSessionID =  CONVERT(nvarchar(10), @ESID)) ;
--------DELETE from WAREHOUSE_CentreTable where ID = CONVERT(nvarchar(10), @CentreID);
--------DELETE from WAREHOUSE_ScheduledExamsTable where ID =  CONVERT(nvarchar(10), @SCETID);
DELETE from WAREHOUSE_ExamSessionAvailableItemsTable where ID IN (SELECT ID FROM WAREHOUSE_ExamSessionAvailableItemsTable where ExamSessionID = CONVERT(nvarchar(10), @ESID));
DELETE from WAREHOUSE_ExamSessionResultHistoryTable where ID IN (SELECT ID FROM WAREHOUSE_ExamSessionResultHistoryTable where WareHouseExamSessionTableID =  CONVERT(nvarchar(10), @ESID));
DELETE from WAREHOUSE_ExamSessionTable_ShrededItems where examSessionID =CONVERT(nvarchar(10), @ESID);
DELETE from WAREHOUSE_ExamSessionTable_Shreded where examSessionID = CONVERT(nvarchar(10), @ESID);
DELETE from WAREHOUSE_ExamSessionTable where ID = CONVERT(nvarchar(10), @ESID);


	     
UPDATE ##ExamsToDelete
set Deleted = 1
where ESID = @ESID;
		 
FETCH NEXT FROM DeleteExam INTO @ESID;--, @SCETID, @CentreID;

END
CLOSE DeleteExam;
DEALLOCATE DeleteExam;


--SELECT * FROM ##ExamsToDelete where ESID = 2726 order by 1



--SELECT ID FROM WAREHOUSE_ScheduledExamsTable 
--	where ID NOT IN (SELECT WAREHOUSEScheduledExamID FROM WAREHOUSE_ExamSessionTable)

--SELECT WAREHOUSE_ScheduledExamsTable.ID, CreatedDateTime, ScheduledStartDateTIme, ScheduledEndDateTIme, examName, qualificationName
--FROM WAREHOUSE_ScheduledExamsTable 
--LEFT JOIN WAREHOUSE_ExamSessionTable
--on WAREHOUSE_ExamSessionTable.WAREHOUSEScheduledExamID = WAREHOUSE_ScheduledExamsTable.ID
--WHERE WAREHOUSE_ExamSessionTable.ID IS NULL

--DELETE FROM WAREHOUSE_ExamSessionTable where ID = CONVERT(nvarchar(10), @ESID);

--SELECT * FROM WAREHOUSE_ScheduledExamsTable 
--	where ID =  CONVERT(nvarchar(10), @SCETID)
		--AND ID NOT IN (SELECT WAREHOUSEScheduledExamID FROM WAREHOUSE_ExamSessionTable);
		
--SELECT * FROM WAREHOUSE_CentreTable 
--	where ID = CONVERT(nvarchar(10), @CentreID)
	    --AND ID NOT IN (SELECT WAREHOUSECentreID FROM WAREHOUSE_ScheduledExamsTable);
































--SELECT * FROM ##ExamsToDelete order by 1;

--DECLARE @ESID int = 718-- (SELECT TOP 1 ESID FROM ##ExamsToDelete where DELETED = 0 order by ESID)
--DECLARE @SCETID INT = 830-- (SELECT TOP 1 SCETID FROM ##ExamsToDelete where DELETED = 0 order by ESID)
--DECLARE @CentreID INT = 149-- (SELECT TOP 1 CentreID FROM ##ExamsToDelete where DELETED = 0 order by ESID)

--SELECT @ESID
--SELECT @SCETID
--SELECT @CentreID

--SELECT * FROM WAREHOUSE_ExamSessionDocumentTable where ID IN (SELECT ID FROM WAREHOUSE_ExamSessionDocumentTable where warehouseExamSessionID = CONVERT(nvarchar(10), @ESID));
--SELECT * FROM WAREHOUSE_ExamStateAuditTable where WAREHOUSEExamSessionID =  + CONVERT(nvarchar(10), @ESID);
--SELECT * FROM WAREHOUSE_ExamSessionDurationAuditTable where ID IN (SELECT ID FROM WAREHOUSE_ExamSessionDurationAuditTable where Warehoused_ExamSessionID =  CONVERT(nvarchar(10), @ESID)) ;
--SELECT * FROM WAREHOUSE_CentreTable where ID = CONVERT(nvarchar(10), @CentreID);
--SELECT * FROM WAREHOUSE_ScheduledExamsTable where ID =  CONVERT(nvarchar(10), @SCETID);
--SELECT * FROM WAREHOUSE_ExamSessionAvailableItemsTable where ID IN (SELECT ID FROM WAREHOUSE_ExamSessionAvailableItemsTable where ExamSessionID = CONVERT(nvarchar(10), @ESID));
--SELECT * FROM WAREHOUSE_ExamSessionResultHistoryTable where ID IN (SELECT ID FROM WAREHOUSE_ExamSessionResultHistoryTable where WareHouseExamSessionTableID =  CONVERT(nvarchar(10), @ESID));
--SELECT * FROM WAREHOUSE_ExamSessionTable_ShrededItems where examSessionID =CONVERT(nvarchar(10), @ESID);
--SELECT * FROM WAREHOUSE_ExamSessionTable_Shreded where examSessionID = CONVERT(nvarchar(10), @ESID);
--SELECT * FROM WAREHOUSE_ExamSessionTable where ID = CONVERT(nvarchar(10), @ESID);


--DELETE FROM WAREHOUSE_ExamSessionDocumentTable where ID IN (SELECT ID FROM WAREHOUSE_ExamSessionDocumentTable where warehouseExamSessionID = CONVERT(nvarchar(10), @ESID));
--DELETE FROM WAREHOUSE_ExamStateAuditTable where WAREHOUSEExamSessionID =  + CONVERT(nvarchar(10), @ESID);
--DELETE FROM WAREHOUSE_ExamSessionDurationAuditTable where ID IN (SELECT ID FROM WAREHOUSE_ExamSessionDurationAuditTable where Warehoused_ExamSessionID =  CONVERT(nvarchar(10), @ESID)) ;
--DELETE FROM WAREHOUSE_CentreTable where ID = CONVERT(nvarchar(10), @CentreID);

--DELETE FROM WAREHOUSE_ExamSessionAvailableItemsTable where ID IN (SELECT ID FROM WAREHOUSE_ExamSessionAvailableItemsTable where ExamSessionID = CONVERT(nvarchar(10), @ESID));
--DELETE FROM WAREHOUSE_ExamSessionResultHistoryTable where ID IN (SELECT ID FROM WAREHOUSE_ExamSessionResultHistoryTable where WareHouseExamSessionTableID =  CONVERT(nvarchar(10), @ESID));
--DELETE FROM WAREHOUSE_ExamSessionTable_ShrededItems where examSessionID =CONVERT(nvarchar(10), @ESID);
--DELETE FROM WAREHOUSE_ExamSessionTable_Shreded where examSessionID = CONVERT(nvarchar(10), @ESID);

--DELETE FROM WAREHOUSE_ExamSessionTable where ID = CONVERT(nvarchar(10), @ESID);

--SELECT * FROM WAREHOUSE_ScheduledExamsTable 
--	where ID =  CONVERT(nvarchar(10), @SCETID)
		--AND ID NOT IN (SELECT WAREHOUSEScheduledExamID FROM WAREHOUSE_ExamSessionTable);
		
--SELECT * FROM WAREHOUSE_CentreTable 
--	where ID = CONVERT(nvarchar(10), @CentreID)
	    --AND ID NOT IN (SELECT WAREHOUSECentreID FROM WAREHOUSE_ScheduledExamsTable);

--SELECT * FROM WAREHOUSE_CentreTable where ID = CONVERT(nvarchar(10), @CentreID);

--PRINT @ESID

--SELECT * FROM WAREHOUSE_ScheduledExamsTable WHERE ID = 830


--SELECT * FROM WAREHOUSE_ScheduledExamsTable WHERE ID NOT IN (SELECT WAREHOUSEScheduledExamID FROM WAREHOUSE_ExamSessionTable) 

--DELETE WEST
--DELETE WSCET WHERE ID = ID 
	--AND ID NOT IN (SELECT WAREHOUSEScheduledExamID FROM WAREHOUSE_ExamSessionTable) 