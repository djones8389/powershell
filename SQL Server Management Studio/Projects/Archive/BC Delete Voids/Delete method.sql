/*
select count(*) from WAREHOUSE_ExamSessionItemResponseTable where WAREHOUSEExamSessionID in (SELECT ESID from ##ExamsToDelete);
select count(*) from WAREHOUSE_ExamSessionAvailableItemsTable where ExamSessionID in (SELECT ESID from ##ExamsToDelete);
select count(*) from WAREHOUSE_ExamSessionDocumentTable where warehouseExamSessionID in (SELECT ESID from ##ExamsToDelete);
select count(*) from WAREHOUSE_ExamSessionTable where ID in (SELECT ESID from ##ExamsToDelete);
select count(*) from WAREHOUSE_ExamSessionTable_Shreded where examSessionID in (SELECT ESID from ##ExamsToDelete);
select count(*) from WAREHOUSE_ExamSessionTable_ShrededItems where examSessionID in (SELECT ESID from ##ExamsToDelete);
select count(*) from WAREHOUSE_ExamStateAuditTable where WAREHOUSEExamSessionID in (SELECT ESID from ##ExamsToDelete);
select count(*) from WAREHOUSE_ScheduledExamsTable where ID IN (SELECT SCETID from ##ExamsToDelete);
select count(*) from WAREHOUSE_ExamSessionDurationAuditTable where Warehoused_ExamSessionID in (SELECT ESID from ##ExamsToDelete);
select count(*) from WAREHOUSE_ExamSessionResultHistoryTable where  WareHouseExamSessionTableID in (SELECT ESID from ##ExamsToDelete);
select count(*) from WAREHOUSE_CentreTable where ID in (SELECT CentreID from ##ExamsToDelete);
*/

USE SANDBOX_BC_SecureAssess

select west.ID as ESID, wscet.ID as SCETID, wscet.WAREHOUSECentreID as CentreID				--320713 Exams
into ##ExamsToDelete
from WAREHOUSE_ExamSessionTable as west
inner join WAREHOUSE_ScheduledExamsTable as wscet
on west.WAREHOUSEScheduledExamID = wscet.ID
where warehouseTime <= DATEADD(Month, -6, GETDATE())
	and PreviousExamState = 10


select 'DELETE from WAREHOUSE_ExamSessionDocumentTable where ID =' + CONVERT(nvarchar(10), ID) + ';'
from WAREHOUSE_ExamSessionDocumentTable 
where warehouseExamSessionID in (SELECT ESID from ##ExamsToDelete)
GROUP BY ID		
ORDER BY ID;

select 'DELETE from WAREHOUSE_ExamStateAuditTable where WAREHOUSEExamSessionID =' + CONVERT(nvarchar(10), WAREHOUSEExamSessionID) + ';'
from WAREHOUSE_ExamStateAuditTable where WAREHOUSEExamSessionID in (SELECT ESID from ##ExamsToDelete)
GROUP BY warehouseExamSessionID
ORDER BY warehouseExamSessionID;

select 'DELETE from WAREHOUSE_ExamSessionDurationAuditTable where ID =' + CONVERT(nvarchar(10), ID) + ';'
from WAREHOUSE_ExamSessionDurationAuditTable 
where Warehoused_ExamSessionID in (SELECT ESID from ##ExamsToDelete)
GROUP BY ID
ORDER BY ID;

select 'DELETE from WAREHOUSE_CentreTable where ID =' + CONVERT(nvarchar(10), ID) + ';'
from WAREHOUSE_CentreTable 
where ID in (SELECT CentreID from ##ExamsToDelete)
GROUP BY ID
ORDER BY ID;

select 'DELETE from WAREHOUSE_ScheduledExamsTable where ID =' + CONVERT(nvarchar(10), ID) + ';'
from WAREHOUSE_ScheduledExamsTable 
where ID IN (SELECT SCETID from ##ExamsToDelete)
GROUP BY ID
ORDER BY ID;

select 'DELETE from WAREHOUSE_ExamSessionAvailableItemsTable where ID =' + CONVERT(nvarchar(10), ID) + ';'
from WAREHOUSE_ExamSessionAvailableItemsTable
where ExamSessionID in (SELECT ESID from ##ExamsToDelete)
GROUP BY ID
ORDER BY ID;

select  'DELETE from WAREHOUSE_ExamSessionItemResponseTable where ID ='  + CONVERT(nvarchar(10), ID) + ';'
FROM WAREHOUSE_ExamSessionItemResponseTable
where WAREHOUSEExamSessionID in (SELECT ESID from ##ExamsToDelete)
group by ID
ORDER by ID;

select 'DELETE from WAREHOUSE_ExamSessionResultHistoryTable where ID =' + CONVERT(nvarchar(10), ID) + ';'
from WAREHOUSE_ExamSessionResultHistoryTable 
where  WareHouseExamSessionTableID in (SELECT ESID from ##ExamsToDelete)
GROUP BY ID
ORDER BY ID;

select 'DELETE from WAREHOUSE_ExamSessionTable_ShrededItems where examSessionID =' + CONVERT(nvarchar(10), examSessionID) + ';'
from WAREHOUSE_ExamSessionTable_ShrededItems 
where examSessionID in (SELECT ESID from ##ExamsToDelete)
GROUP BY examSessionID
ORDER BY examSessionID;

select 'DELETE from WAREHOUSE_ExamSessionTable_Shreded where examSessionID =' + CONVERT(nvarchar(10), examSessionID) + ';'
from WAREHOUSE_ExamSessionTable_Shreded 
where examSessionID in (SELECT ESID from ##ExamsToDelete)
GROUP BY examSessionID
ORDER BY examSessionID;

select 'DELETE from WAREHOUSE_ExamSessionTable where ID =' + CONVERT(nvarchar(10), ID) + ';'
from WAREHOUSE_ExamSessionTable where ID in (SELECT ESID from ##ExamsToDelete)
GROUP BY ID
ORDER BY ID;