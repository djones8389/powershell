IF OBJECT_ID ('tempdb..##ExamsToDelete') IS NOT NULL DROP TABLE ##ExamsToDelete;
IF OBJECT_ID ('tempdb..#Counters') IS NOT NULL DROP TABLE #Counters;


CREATE TABLE ##ExamsToDelete (

	ESID int
	, SCETID int
	, CentreID int
	, Deleted bit
)

INSERT INTO ##ExamsToDelete
select west.ID 
	 ,Wscet.ID 
	 ,Wscet.WAREHOUSECentreID 		
	, '0' 
from WAREHOUSE_ExamSessionTable as west
inner join WAREHOUSE_ScheduledExamsTable as wscet
on west.WAREHOUSEScheduledExamID = wscet.ID
where warehouseTime <= DATEADD(Month, -6, GETDATE())
	and PreviousExamState = 10;

CREATE INDEX [IX_DeleteExams] ON ##ExamsToDelete
(
	ESID ASC
)
INCLUDE (
	DELETED
)

DECLARE @Counters TABLE  (
	TableName nvarchar(150)
	, examCounter int
)
	
DECLARE @WAREHOUSE_ExamStateAuditTable int = (SELECT COUNT(*) FROM WAREHOUSE_ExamStateAuditTable where  warehouseExamSessionID IN (SELECT ESID FROM ##ExamsToDelete) )
DECLARE @WAREHOUSE_CentreTable int= (SELECT COUNT(*) FROM WAREHOUSE_CentreTable where ID IN (SELECT CentreID FROM ##ExamsToDelete))
DECLARE @WAREHOUSE_ScheduledExamsTable int= (SELECT COUNT(*) FROM WAREHOUSE_ScheduledExamsTable where ID IN (SELECT SCETID FROM ##ExamsToDelete))
DECLARE @WAREHOUSE_ExamSessionAvailableItemsTable int = (SELECT COUNT(*) FROM WAREHOUSE_ScheduledExamsTable where ID IN (SELECT SCETID FROM ##ExamsToDelete))
DECLARE @WAREHOUSE_ExamSessionResultHistoryTable int= (SELECT COUNT(*) FROM WAREHOUSE_ExamSessionResultHistoryTable where  WareHouseExamSessionTableID IN (SELECT ESID FROM ##ExamsToDelete)  )
DECLARE @WAREHOUSE_ExamSessionTable_ShrededItems int= (SELECT COUNT(*) FROM WAREHOUSE_ExamSessionTable_ShrededItems where examSessionID IN (SELECT ESID FROM ##ExamsToDelete))
DECLARE @WAREHOUSE_ExamSessionTable_Shreded int= (SELECT COUNT(*) FROM WAREHOUSE_ExamSessionTable_Shreded where examSessionID IN (SELECT ESID FROM ##ExamsToDelete))
DECLARE @WAREHOUSE_ExamSessionTable int= (SELECT COUNT(*) FROM WAREHOUSE_ExamSessionTable where ID IN (SELECT ESID FROM ##ExamsToDelete))
DECLARE @WAREHOUSE_ExamSessionItemResponseTable int= (SELECT COUNT(*) FROM WAREHOUSE_ExamSessionItemResponseTable where WAREHOUSEExamSessionID IN (SELECT ESID FROM ##ExamsToDelete))
DECLARE @WAREHOUSE_ExamSessionDurationAuditTable int= (SELECT COUNT(*)  FROM WAREHOUSE_ExamSessionDurationAuditTable where Warehoused_ExamSessionID IN (SELECT ESID FROM ##ExamsToDelete))
DECLARE @WAREHOUSE_ExamSessionDocumentTable int= (SELECT COUNT(*) FROM WAREHOUSE_ExamSessionDocumentTable where warehouseExamSessionID IN (SELECT ESID FROM ##ExamsToDelete)) 

INSERT @Counters (TableName, examCounter)
VALUES('WAREHOUSE_ExamStateAuditTable', @WAREHOUSE_ExamStateAuditTable)
 , ('WAREHOUSE_CentreTable', @WAREHOUSE_CentreTable)
 , ('WAREHOUSE_ScheduledExamsTable', @WAREHOUSE_ScheduledExamsTable)
 , ('WAREHOUSE_ExamSessionAvailableItemsTable', @WAREHOUSE_ExamSessionAvailableItemsTable)
 , ('WAREHOUSE_ExamSessionResultHistoryTable', @WAREHOUSE_ExamSessionResultHistoryTable)
 , ('WAREHOUSE_ExamSessionTable_ShrededItems', @WAREHOUSE_ExamSessionTable_ShrededItems)
 , ('WAREHOUSE_ExamSessionTable_Shreded', @WAREHOUSE_ExamSessionTable_Shreded)
 , ('WAREHOUSE_ExamSessionTable', @WAREHOUSE_ExamSessionTable)
 , ('WAREHOUSE_ExamSessionItemResponseTable', @WAREHOUSE_ExamSessionItemResponseTable)
 , ('WAREHOUSE_ExamSessionDurationAuditTable', @WAREHOUSE_ExamSessionDurationAuditTable)
 , ('WAREHOUSE_ExamSessionDocumentTable', @WAREHOUSE_ExamSessionDocumentTable)



 SELECT * FROM @Counters


select X.*
	--, c.examcounter --/ TotalUsedSpaceMB as MB_Per_Row
	--, ((TotalRowCounts / TotalUsedSpaceMB) * RowsAffected) / 1000 as MB_To_Delete_From_Table
FROM
(
SELECT 
 t.NAME AS TableName
 , SUM(p.rows) AS TotalRowCounts
 , (SUM(a.used_pages) * 8) /  1024 AS TotalUsedSpaceMB
 --, (SUM(a.data_pages) * 8) /  1024 AS TotalDataSpaceMB
-- , C.examCounter as RowsAffected
FROM 
 sys.tables t
INNER JOIN  sys.indexes i ON t.OBJECT_ID = i.object_id
INNER JOIN  sys.partitions p ON i.object_id = p.OBJECT_ID AND i.index_id = p.index_id
INNER JOIN  sys.allocation_units a ON p.partition_id = a.container_id
--INNER JOIN  @Counters C ON C.TableName = t.name
WHERE 
 t.NAME NOT LIKE 'dt%' --AND
 --i.OBJECT_ID > 255 AND  
 --i.index_id <= 1
  --and C.examCounter > 0
GROUP BY 
 t.NAME--, i.object_id, i.index_id, i.name--, C.examCounter

 ) X

 ORDER BY 1 DESC








	--INSERT #Counters(WAREHOUSE_ExamStateAuditTable) SELECT COUNT(*) FROM WAREHOUSE_ExamSessionDurationAuditTable where  Warehoused_ExamSessionID IN (SELECT ESID FROM ##ExamsToDelete) 
	--INSERT #Counters(WAREHOUSE_CentreTable) SELECT COUNT(*) FROM WAREHOUSE_CentreTable where ID IN (SELECT CentreID FROM ##ExamsToDelete)
	--INSERT #Counters(WAREHOUSE_ScheduledExamsTable) SELECT COUNT(*) FROM WAREHOUSE_ScheduledExamsTable where ID IN (SELECT SCETID FROM ##ExamsToDelete)
	--INSERT #Counters(WAREHOUSE_ExamSessionAvailableItemsTable) SELECT COUNT(*) FROM WAREHOUSE_ExamSessionAvailableItemsTable where ExamSessionID IN (SELECT ESID FROM ##ExamsToDelete) 
	--INSERT #Counters(WAREHOUSE_ExamSessionResultHistoryTable) SELECT COUNT(*) FROM WAREHOUSE_ExamSessionResultHistoryTable where  WareHouseExamSessionTableID IN (SELECT ESID FROM ##ExamsToDelete)  
	--INSERT #Counters(WAREHOUSE_ExamSessionTable_ShrededItems) SELECT COUNT(*) FROM WAREHOUSE_ExamSessionTable_ShrededItems where examSessionID IN (SELECT ESID FROM ##ExamsToDelete)
	--INSERT #Counters(WAREHOUSE_ExamSessionTable_Shreded) SELECT COUNT(*) FROM WAREHOUSE_ExamSessionTable_Shreded where examSessionID IN (SELECT ESID FROM ##ExamsToDelete)
	--INSERT #Counters(WAREHOUSE_ExamSessionTable) SELECT COUNT(*) FROM WAREHOUSE_ExamSessionTable where ID IN (SELECT ESID FROM ##ExamsToDelete)
	--INSERT #Counters(WAREHOUSE_ExamSessionItemResponseTable) SELECT COUNT(*) FROM WAREHOUSE_ExamSessionItemResponseTable where WAREHOUSEExamSessionID IN (SELECT ESID FROM ##ExamsToDelete) 

