IF OBJECT_ID ('tempdb..##ExamsToDelete') IS NOT NULL DROP TABLE ##ExamsToDelete;
IF OBJECT_ID ('tempdb..#Counters') IS NOT NULL DROP TABLE #Counters;


CREATE TABLE ##ExamsToDelete (

	ESID int
	, SCETID int
	, CentreID int
	, Deleted bit
)

INSERT INTO ##ExamsToDelete
select west.ID 
	DECLARE @Wscet.ID 
	DECLARE @Wscet.WAREHOUSECentreID 		
	, '0' 
from WAREHOUSE_ExamSessionTable as west
inner join WAREHOUSE_ScheduledExamsTable as wscet
on west.WAREHOUSEScheduledExamID = wscet.ID
where warehouseTime <= DATEADD(Month, -6, GETDATE())
	and PreviousExamState = 10;

CREATE INDEX [IX_DeleteExams] ON ##ExamsToDelete
(
	ESID ASC
)
INCLUDE (
	DELETED
)

CREATE TABLE #Counters (
	--WAREHOUSE_CentreTable int,
	--WAREHOUSE_ExamSessionAvailableItemsTable int,
	--WAREHOUSE_ExamSessionDocumentTable int,
	--WAREHOUSE_ExamSessionDurationAuditTable int,
	--WAREHOUSE_ExamSessionItemResponseTable int,
	--WAREHOUSE_ExamSessionResultHistoryTable int,
	--WAREHOUSE_ExamSessionTable int,
	--WAREHOUSE_ExamSessionTable_Shreded int,
	--WAREHOUSE_ExamSessionTable_ShrededItems int,
	--WAREHOUSE_ExamStateAuditTable int,
	--WAREHOUSE_ScheduledExamsTable int,
	TableName nvarchar(150)
)

--Creates a placeholder--

INSERT #Counters(WAREHOUSE_ExamStateAuditTable) 
SELECT COUNT(*)
FROM WAREHOUSE_ExamSessionDurationAuditTable 
where  Warehoused_ExamSessionID IN (SELECT ESID FROM ##ExamsToDelete)


	
DECLARE @WAREHOUSE_ExamStateAuditTable int = (SELECT COUNT(*) FROM WAREHOUSE_ExamSessionDurationAuditTable where  Warehoused_ExamSessionID IN (SELECT ESID FROM ##ExamsToDelete) )
DECLARE @WAREHOUSE_CentreTable int= (SELECT COUNT(*) FROM WAREHOUSE_CentreTable where ID IN (SELECT CentreID FROM ##ExamsToDelete))
DECLARE @WAREHOUSE_ScheduledExamsTable int= (SELECT COUNT(*) FROM WAREHOUSE_ScheduledExamsTable where ID IN (SELECT SCETID FROM ##ExamsToDelete))
DECLARE @WAREHOUSE_ExamSessionAvailableItemsTable int = (SELECT COUNT(*) FROM WAREHOUSE_ScheduledExamsTable where ID IN (SELECT SCETID FROM ##ExamsToDelete))
DECLARE @WAREHOUSE_ExamSessionResultHistoryTable int= (SELECT COUNT(*) FROM WAREHOUSE_ExamSessionResultHistoryTable where  WareHouseExamSessionTableID IN (SELECT ESID FROM ##ExamsToDelete)  )
DECLARE @WAREHOUSE_ExamSessionTable_ShrededItems int= (SELECT COUNT(*) FROM WAREHOUSE_ExamSessionTable_ShrededItems where examSessionID IN (SELECT ESID FROM ##ExamsToDelete))
DECLARE @WAREHOUSE_ExamSessionTable_Shreded int= (SELECT COUNT(*) FROM WAREHOUSE_ExamSessionTable_Shreded where examSessionID IN (SELECT ESID FROM ##ExamsToDelete))
DECLARE @WAREHOUSE_ExamSessionTable int= (SELECT COUNT(*) FROM WAREHOUSE_ExamSessionTable where ID IN (SELECT ESID FROM ##ExamsToDelete))
DECLARE @WAREHOUSE_ExamSessionItemResponseTable int= (SELECT COUNT(*) FROM WAREHOUSE_ExamSessionItemResponseTable where WAREHOUSEExamSessionID IN (SELECT ESID FROM ##ExamsToDelete))
DECLARE @WAREHOUSE_ExamSessionDurationAuditTable int= (SELECT COUNT(*)  FROM WAREHOUSE_ExamSessionDurationAuditTable where Warehoused_ExamSessionID IN (SELECT ESID FROM ##ExamsToDelete))
DECLARE @WAREHOUSE_ExamSessionDocumentTable int= (SELECT COUNT(*)  FROM WAREHOUSE_ExamSessionDocumentTable where warehouseExamSessionID IN (SELECT ESID FROM ##ExamsToDelete)) 


SELECT * FROM #Counters

SELECT 
 t.NAME AS TableName,
 --i.name AS indexName,
 SUM(p.rows) AS RowCounts,
 --SUM(a.total_pages) AS TotalPages, 
 --SUM(a.used_pages) AS UsedPages, 
 --SUM(a.data_pages) AS DataPages,
 --(SUM(a.total_pages) * 8) / 1024 AS TotalSpaceMB, 
 (SUM(a.used_pages) * 8) / 1024 AS UsedSpaceMB, 
 (SUM(a.data_pages) * 8) / 1024 AS DataSpaceMB
FROM 
 sys.tables t
INNER JOIN   sys.indexes i ON t.OBJECT_ID = i.object_id
INNER JOIN  sys.partitions p ON i.object_id = p.OBJECT_ID AND i.index_id = p.index_id
INNER JOIN  sys.allocation_units a ON p.partition_id = a.container_id

WHERE 
 t.NAME NOT LIKE 'dt%' AND
 i.OBJECT_ID > 255 AND  
 i.index_id <= 1
GROUP BY 
 t.NAME, i.object_id, i.index_id, i.name 
ORDER BY 
 OBJECT_NAME(i.object_id) 
















	--INSERT #Counters(WAREHOUSE_ExamStateAuditTable) SELECT COUNT(*) FROM WAREHOUSE_ExamSessionDurationAuditTable where  Warehoused_ExamSessionID IN (SELECT ESID FROM ##ExamsToDelete) 
	--INSERT #Counters(WAREHOUSE_CentreTable) SELECT COUNT(*) FROM WAREHOUSE_CentreTable where ID IN (SELECT CentreID FROM ##ExamsToDelete)
	--INSERT #Counters(WAREHOUSE_ScheduledExamsTable) SELECT COUNT(*) FROM WAREHOUSE_ScheduledExamsTable where ID IN (SELECT SCETID FROM ##ExamsToDelete)
	--INSERT #Counters(WAREHOUSE_ExamSessionAvailableItemsTable) SELECT COUNT(*) FROM WAREHOUSE_ExamSessionAvailableItemsTable where ExamSessionID IN (SELECT ESID FROM ##ExamsToDelete) 
	--INSERT #Counters(WAREHOUSE_ExamSessionResultHistoryTable) SELECT COUNT(*) FROM WAREHOUSE_ExamSessionResultHistoryTable where  WareHouseExamSessionTableID IN (SELECT ESID FROM ##ExamsToDelete)  
	--INSERT #Counters(WAREHOUSE_ExamSessionTable_ShrededItems) SELECT COUNT(*) FROM WAREHOUSE_ExamSessionTable_ShrededItems where examSessionID IN (SELECT ESID FROM ##ExamsToDelete)
	--INSERT #Counters(WAREHOUSE_ExamSessionTable_Shreded) SELECT COUNT(*) FROM WAREHOUSE_ExamSessionTable_Shreded where examSessionID IN (SELECT ESID FROM ##ExamsToDelete)
	--INSERT #Counters(WAREHOUSE_ExamSessionTable) SELECT COUNT(*) FROM WAREHOUSE_ExamSessionTable where ID IN (SELECT ESID FROM ##ExamsToDelete)
	--INSERT #Counters(WAREHOUSE_ExamSessionItemResponseTable) SELECT COUNT(*) FROM WAREHOUSE_ExamSessionItemResponseTable where WAREHOUSEExamSessionID IN (SELECT ESID FROM ##ExamsToDelete) 

