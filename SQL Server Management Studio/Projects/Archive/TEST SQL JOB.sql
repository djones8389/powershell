USE AQA_SALocal

DECLARE  @subject VARCHAR(30),
		 @query VARCHAR(MAX);

SELECT	 @subject = CAST(COUNT(ExamSessionTable.ID) AS NVARCHAR(11)) + ' Exams in Limbo.'
 FROM	 ExamSessionTable
WHERE ExamSessionTable.ExamState = 99;

SET @query = 

'
SET QUOTED_IDENTIFIER ON


select ''HI''
--select ID, Keycode as Keycode_For_All_Limbo_Exams from ExamSessionTable where examState = 99
            ';

EXECUTE	 msdb.dbo.sp_send_dbmail
		 --@profile_name = '',--N'Local SMTP Virtual Server',
		 @recipients = N'liveservices@btl.com',
		 @subject = @subject,
		 @body = N'See attached for Limbo exams.',
		 @execute_query_database = N'AQA_SALocal',
		 @query = @query,
		 @attach_query_result_as_file = 1,
		 @query_attachment_filename = N'AQA_SALocal Limbo Exams.csv',
		 @query_result_separator = N',',
		 @query_result_no_padding = 1;
		 

--USE master;
--GO
--EXEC sp_configure 'Database Mail XPs', '1';


--RECONFIGURE;
--EXEC sp_configure;

use msdb;
sysmail_help_queue_sp @queue_type = 'Mail' ;

EXECUTE msdb.dbo.sysmail_help_queue_sp ;
GO