USE [BIS_SecureAssess_11.0]

select examsessionid
	--, userMark
	--, userPercentage
	, resultData
	--, resultData.value('(@userMark)','tinyint') as total
	--, SUM(a.b.value('(@actualUserMark)','tinyint')) as totalActual
	--, SUM(a.b.value('(@userMark)','decimal(6,3)')) as totalUser
from WAREHOUSE_ExamSessionTable_Shreded (NOLOCK)
--cross apply resultdata.nodes('/exam//section/item') a(b)
where examSessionID IN (8561,8402,8404)	
   GROUP BY examSessionID
	
	--order by userPercentage DESC
/*	
	
UPDATE WAREHOUSE_ExamSessionTable_Shreded
set resultData = '
<exam passMark="0" passType="1" originalPassMark="0" originalPassType="1" totalMark="61" userMark="45" userPercentage="73.770" passValue="1" originalPassValue="1" totalTimeSpent="0" closeBoundaryType="0" closeBoundaryValue="0" grade="Pass" originalGrade="Pass">
  <gradeBoundaryData>
    <gradeBoundaries passLevelType="1" adjustedGradeBoundaries="0">
      <grade modifier="lt" value="0" description="Fail" />
      <grade modifier="gt" value="0" description="Pass" />
    </gradeBoundaries>
  </gradeBoundaryData>
  <section id="1" passMark="0" passType="1" name="A" totalMark="52" userMark="42" userPercentage="80.769" passValue="1" totalTimeSpent="0" itemsToMark="0">
    <item id="368P981" name="question zero" totalMark="0" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="14" />
    <item id="368P702" name="MAPBE1_10" totalMark="3" userMark="3" actualUserMark="9" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="33" />
    <item id="368P704" name="MAPBE1 18" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="14" />
    <item id="368P767" name="MAPBE1_33" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="29" />
    <item id="368P711" name="MAPBE1_39" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="39" />
    <item id="368P804" name="MAPBE2_16" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="29" />
    <item id="368P953" name="MAPBE2_14" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="49" />
    <item id="368P799" name="MAPBE2_11" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="30" />
    <item id="368P802" name="MAPBE2_15" totalMark="2" userMark="2" actualUserMark="4" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="34" />
    <item id="368P822" name="MAPBE2_27" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="47" />
    <item id="368P789" name="MAPBE2_3" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="36" />
    <item id="368P826" name="MAPBE2_29" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="34" />
    <item id="368P563" name="MARCE3_14" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="21" />
    <item id="368P517" name="MARCE3_5" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="25" />
    <item id="368P652" name="MARCE3_21" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="25" />
    <item id="368P1017" name="MARCE3_45" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="30" />
    <item id="368P1015" name="MARCE3_43" totalMark="2" userMark="1" actualUserMark="2" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="34" />
    <item id="368P1014" name="MARCE3_42" totalMark="2" userMark="1" actualUserMark="2" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="32" />
    <item id="368P1022" name="MARCE3_50" totalMark="2" userMark="2" actualUserMark="4" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="40" />
    <item id="368P1020" name="MARCE3_48" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="24" />
    <item id="368P433" name="E1 72 maths" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="10" />
    <item id="368P419" name="E2 53 maths" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="10" />
    <item id="368P428" name="E2 63 maths" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="14" />
    <item id="368P434" name="E2 73 maths" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="10" />
    <item id="368P420" name="E3 54 maths" totalMark="4" userMark="2" actualUserMark="8" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="12" />
    <item id="368P1391" name="Calculator screen" totalMark="0" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="10" />
    <item id="368P696" name="MAPBE1 15" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="19" />
    <item id="368P693" name="MAPBE1 12" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="18" />
    <item id="368P707" name="MAPBE1 21" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="9" />
    <item id="368P689" name="MAPBE1_3" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="34" />
    <item id="368P717" name="MAPBE1 24" totalMark="3" userMark="3" actualUserMark="9" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="22" />
    <item id="368P745" name="MAPBE1_30" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="29" />
    <item id="368P713" name="MAPBE1_41" totalMark="3" userMark="3" actualUserMark="9" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="31" />
    <item id="368P792" name="MAPBE2_6" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="22" />
    <item id="368P820" name="MAPBE2_25" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="44" />
    <item id="368P835" name="MAPBE2_33" totalMark="3" userMark="3" actualUserMark="9" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="31" />
    <item id="368P866" name="MAPBE2_38" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="41" />
    <item id="368P901" name="MAPBE2_47" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="38" />
    <item id="368P657" name="MARCE3_26" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="19" />
  </section>
  <section id="2" passMark="0" passType="1" name="B" totalMark="9" userMark="3" userPercentage="33.3" passValue="1" totalTimeSpent="0" itemsToMark="0">
    <item id="368P1337" name="MBRCE3 1_1" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="32" />
    <item id="368P1338" name="MBRCE3 1_2" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="31" />
    <item id="368P1340" name="MBRCE3 1_3" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="34" />
    <item id="368P1341" name="MBRCE3 1_4" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="34" />
    <item id="368P1342" name="MBRCE3 1_5" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="35" />
    <item id="368P1350" name="MBRCE3 4 1" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="9" />
    <item id="368P1352" name="MBRCE3 4 2" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="10" />
    <item id="368P1357" name="MBRCE3 4 4" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="9" />
    <item id="368P1358" name="MBRCE3 4 5" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="11" />
  </section>
</exam>'
where examsessionID = 8561;

UPDATE WAREHOUSE_ExamSessionTable
set resultData = '
<exam passMark="0" passType="1" originalPassMark="0" originalPassType="1" totalMark="61" userMark="45" userPercentage="73.770" passValue="1" originalPassValue="1" totalTimeSpent="0" closeBoundaryType="0" closeBoundaryValue="0" grade="Pass" originalGrade="Pass">
  <gradeBoundaryData>
    <gradeBoundaries passLevelType="1" adjustedGradeBoundaries="0">
      <grade modifier="lt" value="0" description="Fail" />
      <grade modifier="gt" value="0" description="Pass" />
    </gradeBoundaries>
  </gradeBoundaryData>
  <section id="1" passMark="0" passType="1" name="A" totalMark="52" userMark="42" userPercentage="80.769" passValue="1" totalTimeSpent="0" itemsToMark="0">
    <item id="368P981" name="question zero" totalMark="0" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="14" />
    <item id="368P702" name="MAPBE1_10" totalMark="3" userMark="3" actualUserMark="9" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="33" />
    <item id="368P704" name="MAPBE1 18" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="14" />
    <item id="368P767" name="MAPBE1_33" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="29" />
    <item id="368P711" name="MAPBE1_39" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="39" />
    <item id="368P804" name="MAPBE2_16" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="29" />
    <item id="368P953" name="MAPBE2_14" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="49" />
    <item id="368P799" name="MAPBE2_11" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="30" />
    <item id="368P802" name="MAPBE2_15" totalMark="2" userMark="2" actualUserMark="4" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="34" />
    <item id="368P822" name="MAPBE2_27" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="47" />
    <item id="368P789" name="MAPBE2_3" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="36" />
    <item id="368P826" name="MAPBE2_29" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="34" />
    <item id="368P563" name="MARCE3_14" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="21" />
    <item id="368P517" name="MARCE3_5" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="25" />
    <item id="368P652" name="MARCE3_21" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="25" />
    <item id="368P1017" name="MARCE3_45" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="30" />
    <item id="368P1015" name="MARCE3_43" totalMark="2" userMark="1" actualUserMark="2" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="34" />
    <item id="368P1014" name="MARCE3_42" totalMark="2" userMark="1" actualUserMark="2" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="32" />
    <item id="368P1022" name="MARCE3_50" totalMark="2" userMark="2" actualUserMark="4" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="40" />
    <item id="368P1020" name="MARCE3_48" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="24" />
    <item id="368P433" name="E1 72 maths" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="10" />
    <item id="368P419" name="E2 53 maths" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="10" />
    <item id="368P428" name="E2 63 maths" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="14" />
    <item id="368P434" name="E2 73 maths" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="10" />
    <item id="368P420" name="E3 54 maths" totalMark="4" userMark="2" actualUserMark="8" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="12" />
    <item id="368P1391" name="Calculator screen" totalMark="0" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="10" />
    <item id="368P696" name="MAPBE1 15" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="19" />
    <item id="368P693" name="MAPBE1 12" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="18" />
    <item id="368P707" name="MAPBE1 21" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="9" />
    <item id="368P689" name="MAPBE1_3" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="34" />
    <item id="368P717" name="MAPBE1 24" totalMark="3" userMark="3" actualUserMark="9" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="22" />
    <item id="368P745" name="MAPBE1_30" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="29" />
    <item id="368P713" name="MAPBE1_41" totalMark="3" userMark="3" actualUserMark="9" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="31" />
    <item id="368P792" name="MAPBE2_6" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="22" />
    <item id="368P820" name="MAPBE2_25" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="44" />
    <item id="368P835" name="MAPBE2_33" totalMark="3" userMark="3" actualUserMark="9" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="31" />
    <item id="368P866" name="MAPBE2_38" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="41" />
    <item id="368P901" name="MAPBE2_47" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="38" />
    <item id="368P657" name="MARCE3_26" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="19" />
  </section>
  <section id="2" passMark="0" passType="1" name="B" totalMark="9" userMark="3" userPercentage="33.3" passValue="1" totalTimeSpent="0" itemsToMark="0">
    <item id="368P1337" name="MBRCE3 1_1" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="32" />
    <item id="368P1338" name="MBRCE3 1_2" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="31" />
    <item id="368P1340" name="MBRCE3 1_3" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="34" />
    <item id="368P1341" name="MBRCE3 1_4" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="34" />
    <item id="368P1342" name="MBRCE3 1_5" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="35" />
    <item id="368P1350" name="MBRCE3 4 1" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="9" />
    <item id="368P1352" name="MBRCE3 4 2" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="10" />
    <item id="368P1357" name="MBRCE3 4 4" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="9" />
    <item id="368P1358" name="MBRCE3 4 5" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="11" />
  </section>
</exam>'
where ID = 8561;


	
UPDATE WAREHOUSE_ExamSessionTable_Shreded
set resultData = '<exam passMark="0" passType="1" originalPassMark="0" originalPassType="1" totalMark="70" userMark="46.666" userPercentage="67.143" passValue="1" originalPassValue="1" totalTimeSpent="0" closeBoundaryType="0" closeBoundaryValue="0" grade="Pass" originalGrade="Pass">
  <gradeBoundaryData>
    <gradeBoundaries passLevelType="1" adjustedGradeBoundaries="0">
      <grade modifier="lt" value="0" description="Fail" />
      <grade modifier="gt" value="0" description="Pass" />
    </gradeBoundaries>
  </gradeBoundaryData>
  <section id="1" passMark="0" passType="1" name="A" totalMark="46" userMark="40" userPercentage="86.957" passValue="1" totalTimeSpent="0" itemsToMark="0">
    <item id="368P981" name="question zero" totalMark="0" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="14" />
    <item id="368P953" name="MAPBE2_14" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="49" />
    <item id="368P820" name="MAPBE2_25" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="44" />
    <item id="368P822" name="MAPBE2_27" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="47" />
    <item id="368P648" name="MARCE3_18" totalMark="3" userMark="3" actualUserMark="9" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="24" />
    <item id="368P605" name="MARCE3_15" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="29" />
    <item id="368P563" name="MARCE3_14" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="21" />
    <item id="368P520" name="MARCE3_8" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="20" />
    <item id="368P517" name="MARCE3_5" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="25" />
    <item id="368P655" name="MARCE3_24" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="29" />
    <item id="368P652" name="MARCE3_21" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="25" />
    <item id="368P657" name="MARCE3_26" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="19" />
    <item id="368P1017" name="MARCE3_45" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="30" />
    <item id="368P1015" name="MARCE3_43" totalMark="2" userMark="2" actualUserMark="4" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="34" />
    <item id="368P982" name="MARCE3_32" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="28" />
    <item id="368P1022" name="MARCE3_50" totalMark="2" userMark="1.666" actualUserMark="3" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="40" />
    <item id="368P1020" name="MARCE3_48" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="24" />
    <item id="368P492" name="MAMCL1 5" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="23" />
    <item id="368P660" name="MAMCL1 11" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="14" />
    <item id="368P1014" name="MARCE3_42" totalMark="2" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="32" />
    <item id="368P491" name="MAMCL1 4" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="29" />
    <item id="368P489" name="MAMCL1 1" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="32" />
    <item id="368P682" name="MAMCL1 26" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="13" />
    <item id="368P749" name="MAMCL1 33" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="13" />
    <item id="368P775" name="MAMCL1 53" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="15" />
    <item id="368P428" name="E2 63 maths" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="14" />
    <item id="368P420" name="E3 54 maths" totalMark="4" userMark="4" actualUserMark="16" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="12" />
    <item id="368P429" name="E3 64 maths" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="15" />
    <item id="368P435" name="E3 74 maths" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="8" />
    <item id="368P421" name="L1 55 maths" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="11" />
    <item id="368P1391" name="Calculator screen" totalMark="0" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="10" />
    <item id="368P792" name="MAPBE2_6" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="22" />
    <item id="368P835" name="MAPBE2_33" totalMark="3" userMark="3" actualUserMark="9" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="31" />
    <item id="368P866" name="MAPBE2_38" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="41" />
    <item id="368P901" name="MAPBE2_47" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="38" />
    <item id="368P611" name="MARCE3_16" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="18" />
    <item id="368P679" name="MAMCL1 23" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="13" />
    <item id="368P780" name="MAMCL1 58" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="10" />
  </section>
  <section id="2" passMark="0" passType="1" name="B" totalMark="24" userMark="7" userPercentage="29.2" passValue="1" totalTimeSpent="0" itemsToMark="0">
    <item id="368P1337" name="MBRCE3 1_1" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="32" />
    <item id="368P1338" name="MBRCE3 1_2" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="31" />
    <item id="368P1340" name="MBRCE3 1_3" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="34" />
    <item id="368P1341" name="MBRCE3 1_4" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="34" />
    <item id="368P1342" name="MBRCE3 1_5" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="35" />
    <item id="368P1350" name="MBRCE3 4 1" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="9" />
    <item id="368P1352" name="MBRCE3 4 2" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="10" />
    <item id="368P1354" name="MBRCE3 4 3" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="12" />
    <item id="368P1357" name="MBRCE3 4 4" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="9" />
    <item id="368P1358" name="MBRCE3 4 5" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="11" />
    <item id="368P1343" name="MBRCE3 5_1" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="30" />
    <item id="368P1344" name="MBRCE3 5_2" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="31" />
    <item id="368P1345" name="MBRCE3 5_3" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="38" />
    <item id="368P1347" name="MBRCE3 5_4" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="34" />
    <item id="368P1348" name="MBRCE3 5_5" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="35" />
    <item id="368P1332" name="MBMCL1 1" totalMark="3" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="22" />
    <item id="368P1331" name="MBMCL1 1 2" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="16" />
    <item id="368P1325" name="MBMCL1 4" totalMark="5" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="21" />
  </section>
</exam>'
where examSessionId = 8404;

UPDATE WAREHOUSE_ExamSessionTable
set resultData = '<exam passMark="0" passType="1" originalPassMark="0" originalPassType="1" totalMark="70" userMark="46.666" userPercentage="67.143" passValue="1" originalPassValue="1" totalTimeSpent="0" closeBoundaryType="0" closeBoundaryValue="0" grade="Pass" originalGrade="Pass">
  <gradeBoundaryData>
    <gradeBoundaries passLevelType="1" adjustedGradeBoundaries="0">
      <grade modifier="lt" value="0" description="Fail" />
      <grade modifier="gt" value="0" description="Pass" />
    </gradeBoundaries>
  </gradeBoundaryData>
  <section id="1" passMark="0" passType="1" name="A" totalMark="46" userMark="40" userPercentage="86.957" passValue="1" totalTimeSpent="0" itemsToMark="0">
    <item id="368P981" name="question zero" totalMark="0" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="14" />
    <item id="368P953" name="MAPBE2_14" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="49" />
    <item id="368P820" name="MAPBE2_25" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="44" />
    <item id="368P822" name="MAPBE2_27" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="47" />
    <item id="368P648" name="MARCE3_18" totalMark="3" userMark="3" actualUserMark="9" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="24" />
    <item id="368P605" name="MARCE3_15" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="29" />
    <item id="368P563" name="MARCE3_14" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="21" />
    <item id="368P520" name="MARCE3_8" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="20" />
    <item id="368P517" name="MARCE3_5" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="25" />
    <item id="368P655" name="MARCE3_24" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="29" />
    <item id="368P652" name="MARCE3_21" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="25" />
    <item id="368P657" name="MARCE3_26" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="19" />
    <item id="368P1017" name="MARCE3_45" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="30" />
    <item id="368P1015" name="MARCE3_43" totalMark="2" userMark="2" actualUserMark="4" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="34" />
    <item id="368P982" name="MARCE3_32" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="28" />
    <item id="368P1022" name="MARCE3_50" totalMark="2" userMark="1.666" actualUserMark="3" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="40" />
    <item id="368P1020" name="MARCE3_48" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="24" />
    <item id="368P492" name="MAMCL1 5" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="23" />
    <item id="368P660" name="MAMCL1 11" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="14" />
    <item id="368P1014" name="MARCE3_42" totalMark="2" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="32" />
    <item id="368P491" name="MAMCL1 4" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="29" />
    <item id="368P489" name="MAMCL1 1" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="32" />
    <item id="368P682" name="MAMCL1 26" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="13" />
    <item id="368P749" name="MAMCL1 33" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="13" />
    <item id="368P775" name="MAMCL1 53" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="15" />
    <item id="368P428" name="E2 63 maths" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="14" />
    <item id="368P420" name="E3 54 maths" totalMark="4" userMark="4" actualUserMark="16" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="12" />
    <item id="368P429" name="E3 64 maths" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="15" />
    <item id="368P435" name="E3 74 maths" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="8" />
    <item id="368P421" name="L1 55 maths" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="11" />
    <item id="368P1391" name="Calculator screen" totalMark="0" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="10" />
    <item id="368P792" name="MAPBE2_6" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="22" />
    <item id="368P835" name="MAPBE2_33" totalMark="3" userMark="3" actualUserMark="9" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="31" />
    <item id="368P866" name="MAPBE2_38" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="41" />
    <item id="368P901" name="MAPBE2_47" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="38" />
    <item id="368P611" name="MARCE3_16" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="18" />
    <item id="368P679" name="MAMCL1 23" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="13" />
    <item id="368P780" name="MAMCL1 58" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="10" />
  </section>
  <section id="2" passMark="0" passType="1" name="B" totalMark="24" userMark="7" userPercentage="29.2" passValue="1" totalTimeSpent="0" itemsToMark="0">
    <item id="368P1337" name="MBRCE3 1_1" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="32" />
    <item id="368P1338" name="MBRCE3 1_2" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="31" />
    <item id="368P1340" name="MBRCE3 1_3" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="34" />
    <item id="368P1341" name="MBRCE3 1_4" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="34" />
    <item id="368P1342" name="MBRCE3 1_5" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="35" />
    <item id="368P1350" name="MBRCE3 4 1" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="9" />
    <item id="368P1352" name="MBRCE3 4 2" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="10" />
    <item id="368P1354" name="MBRCE3 4 3" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="12" />
    <item id="368P1357" name="MBRCE3 4 4" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="9" />
    <item id="368P1358" name="MBRCE3 4 5" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="11" />
    <item id="368P1343" name="MBRCE3 5_1" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="30" />
    <item id="368P1344" name="MBRCE3 5_2" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="31" />
    <item id="368P1345" name="MBRCE3 5_3" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="38" />
    <item id="368P1347" name="MBRCE3 5_4" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="34" />
    <item id="368P1348" name="MBRCE3 5_5" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="35" />
    <item id="368P1332" name="MBMCL1 1" totalMark="3" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="22" />
    <item id="368P1331" name="MBMCL1 1 2" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="16" />
    <item id="368P1325" name="MBMCL1 4" totalMark="5" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="21" />
  </section>
</exam>'
where ID = 8404;




UPDATE WAREHOUSE_ExamSessionTable_Shreded
set resultData = '<exam passMark="0" passType="1" originalPassMark="0" originalPassType="1" totalMark="70" userMark="49.581" userPercentage="70.83" passValue="1" originalPassValue="1" totalTimeSpent="0" closeBoundaryType="0" closeBoundaryValue="0" grade="Pass" originalGrade="Pass">
  <gradeBoundaryData>
    <gradeBoundaries passLevelType="1" adjustedGradeBoundaries="0">
      <grade modifier="lt" value="0" description="Fail" />
      <grade modifier="gt" value="0" description="Pass" />
    </gradeBoundaries>
  </gradeBoundaryData>
  <section id="1" passMark="0" passType="1" name="A" totalMark="46" userMark="34" userPercentage="73.913" passValue="1" totalTimeSpent="0" itemsToMark="0">
    <item id="368P981" name="question zero" totalMark="0" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="14" />
    <item id="368P953" name="MAPBE2_14" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="49" />
    <item id="368P820" name="MAPBE2_25" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="44" />
    <item id="368P822" name="MAPBE2_27" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="47" />
    <item id="368P648" name="MARCE3_18" totalMark="3" userMark="3" actualUserMark="9" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="24" />
    <item id="368P605" name="MARCE3_15" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="29" />
    <item id="368P563" name="MARCE3_14" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="21" />
    <item id="368P520" name="MARCE3_8" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="20" />
    <item id="368P517" name="MARCE3_5" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="25" />
    <item id="368P655" name="MARCE3_24" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="29" />
    <item id="368P652" name="MARCE3_21" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="25" />
    <item id="368P657" name="MARCE3_26" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="19" />
    <item id="368P1017" name="MARCE3_45" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="30" />
    <item id="368P1015" name="MARCE3_43" totalMark="2" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="34" />
    <item id="368P982" name="MARCE3_32" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="28" />
    <item id="368P1022" name="MARCE3_50" totalMark="2" userMark="1.666" actualUserMark="3" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="40" />
    <item id="368P1020" name="MARCE3_48" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="24" />
    <item id="368P492" name="MAMCL1 5" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="23" />
    <item id="368P660" name="MAMCL1 11" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="14" />
    <item id="368P1014" name="MARCE3_42" totalMark="2" userMark="2" actualUserMark="4" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="32" />
    <item id="368P491" name="MAMCL1 4" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="29" />
    <item id="368P489" name="MAMCL1 1" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="32" />
    <item id="368P682" name="MAMCL1 26" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="13" />
    <item id="368P749" name="MAMCL1 33" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="13" />
    <item id="368P775" name="MAMCL1 53" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="15" />
    <item id="368P428" name="E2 63 maths" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="14" />
    <item id="368P420" name="E3 54 maths" totalMark="4" userMark="4" actualUserMark="16" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="12" />
    <item id="368P429" name="E3 64 maths" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="15" />
    <item id="368P435" name="E3 74 maths" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="8" />
    <item id="368P421" name="L1 55 maths" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="11" />
    <item id="368P1391" name="Calculator screen" totalMark="0" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="10" />
    <item id="368P792" name="MAPBE2_6" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="22" />
    <item id="368P835" name="MAPBE2_33" totalMark="3" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="31" />
    <item id="368P866" name="MAPBE2_38" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="41" />
    <item id="368P901" name="MAPBE2_47" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="38" />
    <item id="368P611" name="MARCE3_16" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="18" />
    <item id="368P679" name="MAMCL1 23" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="13" />
    <item id="368P780" name="MAMCL1 58" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="10" />
  </section>
  <section id="2" passMark="0" passType="1" name="B" totalMark="24" userMark="17" userPercentage="70.833" passValue="1" totalTimeSpent="0" itemsToMark="0">
    <item id="368P1337" name="MBRCE3 1_1" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="32" />
    <item id="368P1338" name="MBRCE3 1_2" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="31" />
    <item id="368P1340" name="MBRCE3 1_3" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="34" />
    <item id="368P1341" name="MBRCE3 1_4" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="34" />
    <item id="368P1342" name="MBRCE3 1_5" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="35" />
    <item id="368P1350" name="MBRCE3 4 1" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="9" />
    <item id="368P1352" name="MBRCE3 4 2" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="10" />
    <item id="368P1354" name="MBRCE3 4 3" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="12" />
    <item id="368P1357" name="MBRCE3 4 4" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="9" />
    <item id="368P1358" name="MBRCE3 4 5" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="11" />
    <item id="368P1343" name="MBRCE3 5_1" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="30" />
    <item id="368P1344" name="MBRCE3 5_2" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="31" />
    <item id="368P1345" name="MBRCE3 5_3" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="38" />
    <item id="368P1347" name="MBRCE3 5_4" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="34" />
    <item id="368P1348" name="MBRCE3 5_5" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="35" />
    <item id="368P1332" name="MBMCL1 1" totalMark="3" userMark="2.25" actualUserMark="7" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="22" />
    <item id="368P1331" name="MBMCL1 1 2" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="16" />
    <item id="368P1325" name="MBMCL1 4" totalMark="5" userMark="3.665" actualUserMark="18" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="21" />
  </section>
</exam>'
where examSessionId = 8402;

UPDATE WAREHOUSE_ExamSessionTable
set resultData = '<exam passMark="0" passType="1" originalPassMark="0" originalPassType="1" totalMark="70" userMark="49.581" userPercentage="70.83" passValue="1" originalPassValue="1" totalTimeSpent="0" closeBoundaryType="0" closeBoundaryValue="0" grade="Pass" originalGrade="Pass">
  <gradeBoundaryData>
    <gradeBoundaries passLevelType="1" adjustedGradeBoundaries="0">
      <grade modifier="lt" value="0" description="Fail" />
      <grade modifier="gt" value="0" description="Pass" />
    </gradeBoundaries>
  </gradeBoundaryData>
  <section id="1" passMark="0" passType="1" name="A" totalMark="46" userMark="34" userPercentage="73.913" passValue="1" totalTimeSpent="0" itemsToMark="0">
    <item id="368P981" name="question zero" totalMark="0" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="14" />
    <item id="368P953" name="MAPBE2_14" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="49" />
    <item id="368P820" name="MAPBE2_25" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="44" />
    <item id="368P822" name="MAPBE2_27" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="47" />
    <item id="368P648" name="MARCE3_18" totalMark="3" userMark="3" actualUserMark="9" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="24" />
    <item id="368P605" name="MARCE3_15" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="29" />
    <item id="368P563" name="MARCE3_14" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="21" />
    <item id="368P520" name="MARCE3_8" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="20" />
    <item id="368P517" name="MARCE3_5" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="25" />
    <item id="368P655" name="MARCE3_24" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="29" />
    <item id="368P652" name="MARCE3_21" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="25" />
    <item id="368P657" name="MARCE3_26" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="19" />
    <item id="368P1017" name="MARCE3_45" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="30" />
    <item id="368P1015" name="MARCE3_43" totalMark="2" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="34" />
    <item id="368P982" name="MARCE3_32" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="28" />
    <item id="368P1022" name="MARCE3_50" totalMark="2" userMark="1.666" actualUserMark="3" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="40" />
    <item id="368P1020" name="MARCE3_48" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="24" />
    <item id="368P492" name="MAMCL1 5" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="23" />
    <item id="368P660" name="MAMCL1 11" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="14" />
    <item id="368P1014" name="MARCE3_42" totalMark="2" userMark="2" actualUserMark="4" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="32" />
    <item id="368P491" name="MAMCL1 4" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="29" />
    <item id="368P489" name="MAMCL1 1" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="32" />
    <item id="368P682" name="MAMCL1 26" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="13" />
    <item id="368P749" name="MAMCL1 33" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="13" />
    <item id="368P775" name="MAMCL1 53" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="15" />
    <item id="368P428" name="E2 63 maths" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="14" />
    <item id="368P420" name="E3 54 maths" totalMark="4" userMark="4" actualUserMark="16" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="12" />
    <item id="368P429" name="E3 64 maths" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="15" />
    <item id="368P435" name="E3 74 maths" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="8" />
    <item id="368P421" name="L1 55 maths" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="11" />
    <item id="368P1391" name="Calculator screen" totalMark="0" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="10" />
    <item id="368P792" name="MAPBE2_6" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="22" />
    <item id="368P835" name="MAPBE2_33" totalMark="3" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="31" />
    <item id="368P866" name="MAPBE2_38" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="41" />
    <item id="368P901" name="MAPBE2_47" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="38" />
    <item id="368P611" name="MARCE3_16" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="18" />
    <item id="368P679" name="MAMCL1 23" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="13" />
    <item id="368P780" name="MAMCL1 58" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="10" />
  </section>
  <section id="2" passMark="0" passType="1" name="B" totalMark="24" userMark="17" userPercentage="70.833" passValue="1" totalTimeSpent="0" itemsToMark="0">
    <item id="368P1337" name="MBRCE3 1_1" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="32" />
    <item id="368P1338" name="MBRCE3 1_2" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="31" />
    <item id="368P1340" name="MBRCE3 1_3" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="34" />
    <item id="368P1341" name="MBRCE3 1_4" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="34" />
    <item id="368P1342" name="MBRCE3 1_5" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="35" />
    <item id="368P1350" name="MBRCE3 4 1" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="9" />
    <item id="368P1352" name="MBRCE3 4 2" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="10" />
    <item id="368P1354" name="MBRCE3 4 3" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="12" />
    <item id="368P1357" name="MBRCE3 4 4" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="9" />
    <item id="368P1358" name="MBRCE3 4 5" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="11" />
    <item id="368P1343" name="MBRCE3 5_1" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="30" />
    <item id="368P1344" name="MBRCE3 5_2" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="31" />
    <item id="368P1345" name="MBRCE3 5_3" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="38" />
    <item id="368P1347" name="MBRCE3 5_4" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="34" />
    <item id="368P1348" name="MBRCE3 5_5" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="35" />
    <item id="368P1332" name="MBMCL1 1" totalMark="3" userMark="2.25" actualUserMark="7" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="22" />
    <item id="368P1331" name="MBMCL1 1 2" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="16" />
    <item id="368P1325" name="MBMCL1 4" totalMark="5" userMark="3.665" actualUserMark="18" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="21" />
  </section>
</exam>'
where ID = 8402;


*/


select examsessionid
	, userMark
	, userPercentage
	, resultData
	
from WAREHOUSE_ExamSessionTable_Shreded (NOLOCK)
where examSessionID IN (8561,8402,8404)