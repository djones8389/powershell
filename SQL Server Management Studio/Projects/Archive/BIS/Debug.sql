USE [BIS_SecureAssess_11.0_restore];

SELECT	 E.ID
		,CAST(E.StructureXML AS XML) AS [StructureXML]
		,CAST(E.ResultData AS XML) AS [ResultData]
		,CAST(E.ResultDataFull AS XML) AS [ResultDataFull]
INTO	 #ExamXML
FROM	 dbo.WAREHOUSE_ExamSessionTable E
WHERE	 E.ID IN (3771,5449,9086,9052,6025,9103,9109,9111,9122,9114,9119,5698,6528,6529,6532,6534,6536,6535,7266,7593,7916,7834,7995,8188,7914,8189,8190,8117,8061,8191,8060,8119,8122,8400,7984,8194,8401,8139,8402,8197,8403,8499,8404,8358,8561,8200,8199,8406,8405,8388,8407,8201,8408,8430,8434,8437,8438,8439,8440,8442,8443,8444,8445,8446); --<< Exam(s) you want to update

--SELECT * FROM #ExamXML

SELECT	 E.ID
		,E.ResultData.value('(/exam/@passMark)[1]', 'int') AS [ExamPassMark]
		,E.ResultData.value('(/exam/@passType)[1]', 'tinyint') AS [ExamPassType]
		,E.ResultData.value('(/exam/@totalMark)[1]', 'int') AS [ExamTotalMark]
		,E.ResultData.value('(/exam/@userMark)[1]', 'decimal(6,3)') AS [ExamUserMark]
		,E.ResultData.value('(/exam/@userPercentage)[1]', 'decimal(6,3)') AS [ExamUserPercentage]
		,E.ResultData.value('(/exam/@passValue)[1]', 'tinyint') AS [ExamPassValue]
		,E.ResultData.value('(/exam/@originalPassValue)[1]', 'tinyint') AS [ExamOriginalPassValue]
		,E.ResultData.value('(/exam/@grade)[1]', 'nvarchar(100)') AS [ExamGrade]
		,E.ResultData.value('(/exam/@originalGrade)[1]', 'nvarchar(100)') AS [ExamOriginalGrade]
		--,Exam.Section.query('.')
		,Exam.Section.value('@id', 'int') AS [SectionID]
		,Exam.Section.value('@passMark', 'int') AS [SectionPassMark]
		,Exam.Section.value('@passType', 'tinyint') AS [SectionPassType]
		,Exam.Section.value('@totalMark', 'int') AS [SectionTotalMark]
		,Exam.Section.value('@userMark', 'decimal(6,3)') AS [SectionUserMark]
		,Exam.Section.value('@userPercentage', 'decimal(6,3)') AS [SectionUserPercentage]
		,Exam.Section.value('@passValue', 'tinyint') AS [SectionPassValue]
		--,Section.Item.query('.')
		,Section.Item.value('@id', 'nvarchar(50)') AS [ItemID]
		,Section.Item.value('@totalMark', 'int') AS [ItemTotalMark]
		,Section.Item.value('@userMark', 'decimal(6,3)') AS [ItemUserMark]
		,Section.Item.value('@actualUserMark', 'decimal(6,3)') AS [ItemActualUserMark]
		,Section.Item.value('@userAttempted', 'tinyint') AS [ItemUserAttempted]
--INTO	 #Item
FROM	 #ExamXML E
CROSS APPLY E.ResultData.nodes('/exam/section') Exam(Section)
CROSS APPLY Exam.Section.nodes('item') Section(Item);


SELECT * FROM WAREHOUSE_ExamSessionItemResponseTable where WAREHOUSEExamSessionID = 6025 and ItemID = '368P648'

DECLARE @@HoldingTable TABLE (
    ID SMALLINT
   , ItemID Nvarchar(12)
   , NewValue float
);

INSERT @@HoldingTable
SELECT *
FROM ##HoldingTable;


UPDATE WAREHOUSE_ExamSessionItemResponseTable
SET MarkerResponseData.modify('replace value of (/entries/entry[last()]/assignedMark/text())[1] with sql:column ("NewValue")')
FROM WAREHOUSE_ExamSessionItemResponseTable A
INNER JOIN @@HoldingTable B
On A.WAREHOUSEExamSessionID = B.ID
	AND A.ItemID = B.ItemID;
	
SELECT * FROM WAREHOUSE_ExamSessionItemResponseTable where WAREHOUSEExamSessionID = 6025 and ItemID = '368P648'