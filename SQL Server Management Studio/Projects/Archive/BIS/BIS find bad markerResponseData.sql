USE [BIS_SecureAssess_11.0_restore]
/*
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

SELECT  --resultData
	WEST.ID
	, WESIRT.ItemID
	, resultData
	, a.b.value('@totalMark','decimal(6,3)') as TotalMark
	, MarkerResponseData.value('(entries/entry[last()]/assignedMark)[1]','decimal(6,3)') as ItemResponseXMLMark
	--, NewValue = (a.b.value('@totalMark','decimal(6,3)')  * ItemResponseData.value('(/p/@um)[1]','float'))
	--, UMValue = ItemResponseData.value('(/p/@um)[1]','float')
	, NewValueRounded = ROUND((a.b.value('@totalMark','decimal(6,3)')  * ItemResponseData.value('(/p/@um)[1]','float')), 3)
	--, MarkerResponseData
	--, ItemResponseData
	--, ExamStateChangeAuditXml
FROM WAREHOUSE_ExamSessionItemResponseTable as WESIRT

INNER JOIN WAREHOUSE_ExamSessionTable as WEST
on WEST.ID = WESIRT.WAREHOUSEExamSessionID

CROSS APPLY ResultData.nodes('exam/section//item') a(b)

where west.id in (3771,5449,9086,9052,6025,9103,9109,9111,9122,9114,9119,5698,6528,6529,6532,6534,6536,6535,7266,7593,7916,7834,7995,8188,7914,8189,8190,8117,8061,8191,8060,8119,8122,8400,7984,8194,8401,8139,8402,8197,8403,8499,8404,8358,8561,8200,8199,8406,8405,8388,8407,8201,8408,8430,8434,8437,8438,8439,8440,8442,8443,8444,8445,8446)
	 AND a.b.value('@id','nvarchar(12)') = ItemID
	 --AND WAREHOUSEExamSessionID = 6529
	 AND MarkerResponseData.value('(entries/entry[last()]/assignedMark)[1]','decimal(6,3)') >  a.b.value('@totalMark','decimal(6,3)')
	 --and WEST.ID IN (9125,9122,9121,9119,9117,9116,9114,9113,9112,9111,9110,9109,9108,9103,9098,9097,9091,9088,9087,9086,9085,9084,9078,9077,9076,9070,9069,9064,9063,9062,9060,9059,9057,9053,9052,9051,9050,9049,9048,9047,8974,8583,8561,8499,8484,8446,8445,8444,8443,8442,8440,8439,8438,8437,8436,8434,8431,8430,8408,8407,8406,8405,8404,8403,8402,8401,8400,8388,8359,8358,8201,8200,8199,8197,8196,8195,8194,8193,8192,8191,8190,8189,8188,8139,8122,8119,8117,8116,8109,8108,8107,8106,8061,8060,7995,7984,7925,7924,7916,7914,7885,7840,7593,7382,7380,7266,7258,6539,6536,6535,6534,6533,6532,6529,6528,6026,6025,5698,5532,5449,4014,4013,3778,3771,3766,3729)
	 and ExamStateChangeAuditXml.exist('exam/stateChange[newStateID="99"][1]') = 1
	 --AND MarkerResponseData.value('(entries/entry[last()]/assignedMark)[1]','decimal(6,3)')  <> ROUND((a.b.value('@totalMark','decimal(6,3)')  * ItemResponseData.value('(/p/@um)[1]','float')), 0)
*/

--/*


SELECT  
	WEST.ID
	, WESIRT.ItemID
	, NewValueRounded = ROUND((a.b.value('@totalMark','decimal(6,3)')  * ItemResponseData.value('(/p/@um)[1]','float')), 3)
INTO ##HoldingTable
FROM WAREHOUSE_ExamSessionItemResponseTable as WESIRT

INNER JOIN WAREHOUSE_ExamSessionTable as WEST
on WEST.ID = WESIRT.WAREHOUSEExamSessionID

CROSS APPLY ResultData.nodes('exam/section//item') a(b)

where 
	 a.b.value('@id','nvarchar(12)') = ItemID
	 --AND MarkerResponseData.value('(entries/entry[last()]/assignedMark)[1]','decimal(6,3)') >  a.b.value('@totalMark','decimal(6,3)')
	 and ExamStateChangeAuditXml.exist('exam/stateChange[newStateID="99"][1]') = 1
 ORDER BY 1;
 
 
DECLARE MarkerResponse CURSOR FOR
 
 SELECT
	ID
	, ItemID
	, NewValueRounded
 FROM ##HoldingTable
 
 
 DECLARE @ID SMALLINT, @ItemID Nvarchar(12), @NewValue float;
 
 OPEN MarkerResponse;
 
 FETCH NEXT FROM MarkerResponse INTO @ID, @ItemID, @NewValue
 
 WHILE @@FETCH_STATUS = 0
 
 BEGIN
 
 UPDATE WAREHOUSE_ExamSessionItemResponseTable
 SET MarkerResponseData.modify('replace value of (/entries/entry[last()]/assignedMark/text())[1] with sql:variable ("@NewValue")')
 WHERE WAREHOUSE_ExamSessionItemResponseTable.WAREHOUSEExamSessionID = @ID
	AND ItemID = @ItemID

FETCH NEXT FROM MarkerResponse INTO @ID, @ItemID, @NewValue
 
 END
 CLOSE MarkerResponse;
 DEALLOCATE MarkerResponse;
 
-- SELECT DISTINCT ID FROM ##HoldingTable
--*/