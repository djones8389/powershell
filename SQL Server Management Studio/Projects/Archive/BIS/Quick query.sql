USE [BIS_SecureAssess_11.0_restore]

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

SELECT  distinct
	WEST.ID
	--, WESIRT.ItemID
	--, NewValueRounded = ROUND((a.b.value('@totalMark','decimal(6,3)')  * ItemResponseData.value('(/p/@um)[1]','float')), 3)
	--, a.b.value('@totalMark','decimal(6,3)') as TotalMark
	--, MarkerResponseData.value('(entries/entry[last()]/assignedMark)[1]','decimal(6,3)') as ItemResponseXMLMark
	, userMark
	, userPercentage
	--, west.resultData
FROM WAREHOUSE_ExamSessionItemResponseTable as WESIRT 

INNER JOIN WAREHOUSE_ExamSessionTable as WEST
on WEST.ID = WESIRT.WAREHOUSEExamSessionID

INNER JOIN WAREHOUSE_ExamSessionTable_Shreded as WESTS
on WEST.ID = WESTS.examSessionId


--CROSS APPLY west.resultData.nodes('exam/section//item') a(b)

where west.id in (3771,5449,9086,9052,6025,9103,9109,9111,9122,9114,9119,5698,6528,6529,6532,6534,6536,6535,7266,7593,7916,7834,7995,8188,7914,8189,8190,8117,8061,8191,8060,8119,8122,8400,7984,8194,8401,8139,8402,8197,8403,8499,8404,8358,8561,8200,8199,8406,8405,8388,8407,8201,8408,8430,8434,8437,8438,8439,8440,8442,8443,8444,8445,8446)
	order by 1--,2;
	
	
	
SELECT ID, resultData	
FROM	 dbo.WAREHOUSE_ExamSessionTable E
WHERE	 E.ID IN (3771,5449,9086,9052,6025,9103,9109,9111,9122,9114,9119,5698,6528,6529,6532,6534,6536,6535,7266,7593,7916,7834,7995,8188,7914,8189,8190,8117,8061,8191,8060,8119,8122,8400,7984,8194,8401,8139,8402,8197,8403,8499,8404,8358,8561,8200,8199,8406,8405,8388,8407,8201,8408,8430,8434,8437,8438,8439,8440,8442,8443,8444,8445,8446); --<< Exam(s) you want to update


SELECT *
FROM ##HoldingTable;