SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

declare @setAlf nvarchar(MAX) = '';
declare @myVar varbinary(MAX);

DECLARE @AffectedItems TABLE (
	 projectID smallint
	 , ProjectName nvarchar(100)
	 , ItemID nvarchar(15)
	 , Version tinyint
)

/*Find a known bad SWF*/

select @myVar =  CAST(image as varbinary(MAX))
from ProjectManifestTable
where ProjectId = 908
	and ID = 73;


INSERT @AffectedItems
SELECT DISTINCT
	 FindImage.ProjectId
	 , ProjectName
	  , Items.id
      , Items.ver
      
FROM
(
      Select i.id, i.ver, i.moD, it.aLI
      from ItemGraphicTable it 
            INNER JOIN ComponentTable c ON c.ID = it.ParentID 
            INNER JOIN SceneTable s ON s.ID = c.ParentID 
            INNER JOIN PageTable i ON i.id = S.ParentID
      UNION
      Select i.id, i.ver, i.moD, it.aLI  
      from ItemVideoTable it 
            INNER JOIN ComponentTable c ON c.ID = it.ParentID 
            INNER JOIN SceneTable s ON s.ID = c.ParentID 
            INNER JOIN PageTable i ON i.id = S.ParentID
      UNION
      Select i.id, i.ver, i.moD, it.aLI  
            from ItemHotSpotTable it 
            INNER JOIN ComponentTable c ON c.ID = it.ParentID 
            INNER JOIN SceneTable s ON s.ID = c.ParentID 
            INNER JOIN PageTable i ON i.id = S.ParentID
      UNION
      Select i.id, i.ver, i.moD, it.aLI 
      from ItemCustomQuestionTable it 
            INNER JOIN ComponentTable c ON c.ID = it.ParentID 
            INNER JOIN SceneTable s ON s.ID = c.ParentID 
            INNER JOIN PageTable i ON i.id = S.ParentID
) AS Items

INNER JOIN 
(
	SELECT 
		plt.Name as ProjectName
		,pmt.ProjectID
		,pmt.ID
		,pmt.NAME
	FROM ProjectManifestTable (NOLOCK) pmt
	      
	inner join ProjectListTable as plt 
	on plt.ID = pmt.ProjectId

	where CAST(image as varbinary(MAX)) = @myVar
		 

) AS FindImage

ON (Items.aLI = SUBSTRING(FindImage.NAME , 0, CHARINDEX('.',FindImage.NAME )) or Items.ali = convert(varchar(10), FindImage.ID))
AND SUBSTRING (Items.ID, 0, CHARINDEX('P', Items.ID)) = FindImage.ProjectID

INNER JOIN PageTable as PT
ON PT.ID = Items.ID and PT.ver = Items.ver  
      
     Where Items.id = '908P1363'
      
ORDER BY Items.ID, Items.ver; 


--Keep this so we know which pages are affected--

SELECT * FROM @AffectedItems

/*Replace SWF in projectManifestTable where needed, with known working one*/

UPDATE ProjectManifestTable
set image = (Select image from ProjectManifestTable where ID = 597 and projectID = 898 )
where name = 'table_journal.swf'
	and projectID = 908;

/*Increment page versions*/

Update PageTable
set Ver = Ver + 1
where ID in (select ItemID from @AffectedItems);

/*Set alF flags to = 1*/

select @setAlf += CHAR(13) + 'Update ProjectListTable Set projectStructureXML.modify(''replace value of (//Pag[@ID='+ CAST(SUBSTRING(ItemID, CHARINDEX('P', ItemID) + 1, 10)as nvarchar(MAX)) +'][1]/@alF)[1] with ("1")'') where id = ' + CAST(projectID as nvarchar(MAX)) + ';'
from @AffectedItems

EXEC(@setAlf);
