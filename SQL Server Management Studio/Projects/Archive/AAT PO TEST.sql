USE AAT_SecureAssess_LIVE_28_01_2015

select  
	west.ID
	, west.KeyCode
	, wests.WarehouseExamState
	, wests.reMarkStatus
	, wests.ExportToSecureMarker
	, wests.ExportedToIntegration
	, west.submissionExported
	, purchaseOrder
	, west.PreviousExamState
	, centreName
	, wests.qualificationName
	, wests.examName
from WAREHOUSE_ExamSessionTable_Shreded as wests

	inner join WAREHOUSE_ExamSessionTable as west
	on west.ID = wests.examSessionId
	
	inner join WAREHOUSE_ScheduledExamsTable as wscet
	on wscet.ID = west.WAREHOUSEScheduledExamID

where west.KeyCode in ('EYWNGPA6','F3ULR9A6','QTX4FTA6'); 

select  
	west.ID
	, west.KeyCode
	, wests.WarehouseExamState
	, wests.reMarkStatus
	, wests.ExportToSecureMarker
	, wests.ExportedToIntegration
	, west.submissionExported
	, purchaseOrder
	, west.PreviousExamState
	, centreName
	, wests.qualificationName
	, wests.examName
from WAREHOUSE_ExamSessionTable_Shreded as wests

	inner join WAREHOUSE_ExamSessionTable as west
	on west.ID = wests.examSessionId
	
	inner join WAREHOUSE_ScheduledExamsTable as wscet
	on wscet.ID = west.WAREHOUSEScheduledExamID

where west.KeyCode in ('XNRR24A6','6458LVA6');