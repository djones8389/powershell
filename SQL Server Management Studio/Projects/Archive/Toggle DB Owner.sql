SET NOCOUNT ON

DECLARE @@DatabaseRoleMemberShip TABLE (
	Username NVARCHAR(100)
	,Rolename NVARCHAR(100)
	,Databasename NVARCHAR(100)
)
use [UAT_EAL_ContentProducer]; EXEC sp_droprolemember 'db_owner', 'UAT_EAL_ContentProducer_User';

INSERT @@DatabaseRoleMemberShip
EXEC sp_MSforeachdb  '
USE [?];

select u.name 
		,r.name 
		,''?'' 
from sys.database_role_members RM 
	inner join sys.database_principals U on U.principal_id = RM.member_principal_id
	inner join sys.database_principals R on R.principal_id = RM.role_principal_id
where u.type<>''R''
	and u.name != ''dbo''
	and u.name  like ''%itembank%'' or u.name  like ''%secureassess%'' or u.name  like ''%content%''
	and u.name like ''UAT%''
	and r.name IN (''db_owner'')
	and ''?'' not IN (''msdb'', ''master'', ''tempdb'')
'

SELECT * FROM @@DatabaseRoleMemberShip


--Assign schema permissions

SELECT 'use [' + DatabaseName + ']; GRANT SELECT, EXECUTE ON SCHEMA::dbo to ' + Username + ';'
FROM @@DatabaseRoleMemberShip
order by Databasename ASC;

--Drop DBOwner from it

SELECT 'use [' + DatabaseName + ']; EXEC sp_droprolemember ''db_owner'',' + '''' + username + '''' + ';'
FROM @@DatabaseRoleMemberShip
order by Databasename ASC;
--Readd DBOwner

SELECT 'use [' + DatabaseName + ']; EXEC sp_addrolemember ''db_owner'',' + '''' + username + '''' + ';'
FROM @@DatabaseRoleMemberShip
order by Databasename ASC;