--select *
--From ProjectListTable
--where ID  = 871


Select * from ProjectManifestTable where ProjectId = 1057;
Select CONVERT(XML, structurexml) from SharedLibraryTable where ProjectId = 1057;

Select ID, name, delCol, sharedLib, publishable, hotspot, source from ProjectManifestTable where ProjectId = 871;
--Select CONVERT(XML, structurexml),ProjectId from SharedLibraryTable where ProjectId = 871;


SELECT
	--SUM(DATALENGTH(IMAGE))/1000as MB
	PLT.Name as ProjectName
	, PLT.ID as ProjectID
	-------, PMT.ID
	, PMT.name as ImageName
	--, delCol
	--, sharedLib
from ProjectManifestTable as PMT

INNER JOIN ProjectListTable as PLT
on PLT.ID = PMT.ProjectId

where delCol = 1
	and sharedLib = 1
	order by PLT.Name, PMT.name
