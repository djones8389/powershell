use BritishCouncil_SecureAssess_LIVE



--WORKS--



begin tran

DECLARE @ExamUserPercentage [decimal] (6, 3);
DECLARE @ESID int = 542815

select ID, resultData, resultDatafull from WAREHOUSE_ExamSessionTable	where id = @ESID;

	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @ESID
	--Percentage--
	SELECT @ExamUserPercentage = CAST(((resultdata.value('(/exam/@userMark)[1]', 'decimal(13, 10)') / resultdata.value('(/exam/@totalMark)[1]', 'decimal(13, 10)')) * 10) AS [decimal](6, 3))
	FROM WAREHOUSE_ExamSessionTable_Shreded
	WHERE examSessionId = @ESID;	
	
	UPDATE WAREHOUSE_ExamSessionTable_Shreded
	SET resultdata.modify('replace value of (/exam/@userPercentage)[1] with sql:variable("@ExamUserPercentage")')
	WHERE examSessionId = @ESID;

	UPDATE WAREHOUSE_ExamSessionTable
	SET resultdata.modify('replace value of (/exam/@userPercentage)[1] with sql:variable("@ExamUserPercentage")')
	WHERE Id = @ESID;
	
	UPDATE WAREHOUSE_ExamSessionTable
	SET resultdata.modify('replace value of (/exam/@userPercentage)[1] with sql:variable("@ExamUserPercentage")')
	WHERE Id = @ESID;

	UPDATE WAREHOUSE_ExamSessionTable
	SET resultDataFull.modify('replace value of (/assessmentDetails/assessment/@userPercentage)[1] with sql:variable("@ExamUserPercentage")')
	WHERE ID = @ESID;


	--Mark--
	--SELECT @ExamUserPercentage = CAST(((resultdata.value('(/exam/@userMark)[1]', 'decimal(13, 10)') / resultdata.value('(/exam/@totalMark)[1]', 'decimal(13, 10)')) * 10) AS [decimal](6, 3))
	--FROM WAREHOUSE_ExamSessionTable_Shreded
	--WHERE examSessionId = @ESID;	
	DECLARE @ESID int = 542815
	DECLARE  @ExamUserMark [decimal](6, 3)
	select  @ExamUserMark  = CAST(((resultdata.value('(/exam/@userMark)[1]', 'decimal(13, 10)')))) 
	FROM WAREHOUSE_ExamSessionTable_Shreded
	WHERE examSessionId = @ESID;	
	
	
	UPDATE WAREHOUSE_ExamSessionTable_Shreded
	SET resultdata.modify('replace value of (/exam/@userMark)[1] with sql:variable("@ExamUserPercentage")')
	WHERE examSessionId = @ESID;




select ID, resultData, resultDatafull from WAREHOUSE_ExamSessionTable	where id = @ESID;

rollback

