DECLARE @DBName TABLE ( DBNAME nvarchar(100) )
INSERT @DBName
select Name
from sys.databases
where name like '%SurpassDataWarehouse'

DECLARE @ExecutionLog nvarchar(MAX) = '';
DECLARE @Package nvarchar(MAX) = '';
DECLARE @MinTime datetime = (SELECT DATEADD(Second, -1, max(warehouseTime))  from FactExamSessions)
DECLARE @MaxTime datetime = (select max(warehouseTime) from FactExamSessions)


SELECT @ExecutionLog += CHAR(13) +  'INSERT INTO [' + DBName + '].[ETL].[ExecutionLog] (sourceID, Version, executionGUID, originalexecutionGUID, PeriodStart, PeriodEnd, Result, comment)
VALUES (''B95406AA-C983-4321-BCBB-ABB563791FA5'',''3.1.1562'',''3CEFEE0B-2E34-43E8-9117-3DDEB4238C51'', ''3CEFEE0B-2E34-43E8-9117-3DDEB4238C51'',''2005-01-01 00:00:01.393'', ''2005-01-01 00:00:01.393'',''0'', ''NULL'');
'
FROM @DBName
EXEC(@ExecutionLog)

UPDATE [ETL].[ExecutionLog]
set PeriodStart = @MinTime, PeriodEnd = @MaxTime 
where PeriodStart = '2005-01-01 00:00:01.393' and PeriodEnd = '2005-01-01 00:00:01.393';

SELECT @Package += CHAR(13) +  '
IF NOT EXISTS (SELECT * FROM [ETL].[Packages] where PackageName = ''SecureAssessETL'')
INSERT INTO [' + DBName + '].[ETL].[Packages] 
VALUES (''B95406AA-C983-4321-BCBB-ABB563791FA5'',''SecureAssessETL'',''100'')
	'
FROM @DBName
EXEC(@Package)
