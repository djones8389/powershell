SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

USE UAT_BC2_SecureAssess

SELECT ItemResponseData.value('(/p/s/c/@id)[1]','tinyint') as CID
	, ItemResponseData.value('(/p/s/c/@typ)[1]','tinyint') as CType
	,  ItemVersion--, *
	, ItemResponseData
	, Keycode
	--, examStatechangeauditxml
	--, resultData
	, warehouse_examSessionTable.ID
from Warehouse_ExamSessionItemResponseTable (NOLOCK)
inner join warehouse_examSessionTable
on Warehouse_ExamSessionItemResponseTable.warehouseexamsessionid = warehouse_examSessionTable.id
where itemID = '3520P3865'
	and ItemVersion = 51
ORDER BY Warehouse_ExamSessionItemResponseTable.ID DESC;
	
	--select *
	--from uat_bc2_contentproducer..projectlisttable where id = 3520
	
Select responseXML.value('(/p/s/c/@id)[1]','tinyint') as CID
	, responseXML.value('(/p/s/c/@typ)[1]','tinyint') as CType
	, totalMark
	, itemType
	, ItemVersion
	, responseXML
	, itemName
	, Keycode
	, warehouse_examSessionTable.id
from Warehouse_ExamSessionTable_ShrededItems
inner join warehouse_examSessionTable
on Warehouse_ExamSessionTable_ShrededItems.examsessionid = warehouse_examSessionTable.id
	where ItemRef = '3520P3865'
		and ItemVersion = 51
		AND responseXML IS NOT NULL
ORDER BY Warehouse_ExamSessionTable_ShrededItems.examSessionID DESC;

--UPDATE Warehouse_ExamSessionTable_ShrededItems
--set responseXML = '<p id="3520P3865" cs="1" ua="1" um="0.4">
--  <s ua="1" id="1" um="0.4">
--    <c id="20" typ="12" ie="1" ua="1" um="0.4" wei="1">
--      <i id="1" sl="8" />
--      <i id="2" sl="10" />
--      <i id="3" sl="11" />
--      <i id="4" sl="5" />
--      <i id="5" sl="2" />
--    </c>
--  </s>
--</p>'
--WHERE examsessionid IN (1086441,1082736,1082651,1082645);

--DELETE FROM Warehouse_ExamSessionTable_ShrededItems WHERE examsessionid IN (1086441,1082736,1082651,1082645);

--update Warehouse_ExamSessionItemResponseTable
--set ItemResponseData = ItemResponseData 
--from Warehouse_ExamSessionItemResponseTable a
--inner join Warehouse_ExamSessionTable_ShrededItems b
--on a.WAREHOUSEExamSessionID = b.examSessionId
--	and a.ItemID = b.itemRef
--WHERE warehouseexamsessionid IN (1086441,1082736,1082651,1082645);

--Update Warehouse_ExamSessionItemResponseTable
--set ItemResponseData = '<p id="3520P3865" cs="1" ua="1" um="0.4">
--  <s ua="1" id="1" um="0.4">
--    <c id="20" typ="12" ie="1" ua="1" um="0.4" wei="1">
--      <i id="1" sl="8" />
--      <i id="2" sl="10" />
--      <i id="3" sl="11" />
--      <i id="4" sl="5" />
--      <i id="5" sl="2" />
--    </c>
--  </s>
--</p>'
--WHERE warehouseexamsessionid IN (1086441,1082736,1082651,1082645);

select forename, surname, centrename, warehousetime, resultData
from WAREHOUSE_ExamSessionTable_Shreded
where examsessionid in (1086441,1082736,1082651,1082645);

