USE UAT_BC2_SurpassDataWarehouse

SELECT TOP 1000 [Id]
      ,[SourceId]
      ,[Version]
      ,[ExecutionGUID]
      ,[OriginalExecutionGUID]
      ,[PeriodStart]
      ,[PeriodEnd]
      ,[Result]
      ,[Comment]
  FROM [UAT_BC2_SurpassDataWarehouse].[ETL].[ExecutionLog]

  --SET IDENTITY_INSERT [ETL].[ExecutionLog] ON

  --INSERT [UAT_BC2_SurpassDataWarehouse].[ETL].[ExecutionLog] (Id, SourceId, Version, ExecutionGUID, OriginalExecutionGUID, PeriodStart, PeriodEnd, Result, Comment)
  --VALUES('14',	'B95406AA-C983-4321-BCBB-ABB563791FA5',	'3.1.1562', 'CBEE0236-B842-4832-B2AA-19E52AAB438F','CBEE0236-B842-4832-B2AA-19E52AAB438F','2015-08-01 00:00:00.001','2015-08-02 23:59:59.001',	'0','NULL')

  --SET IDENTITY_INSERT [ETL].[ExecutionLog] OFF