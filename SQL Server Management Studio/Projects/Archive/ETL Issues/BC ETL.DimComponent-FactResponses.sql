DROP TABLE #Comps

SELECT CID, CPVersion,ExamSessionKey
INTO #Comps
FROM etl.stg_FactComponentResponses

CREATE INDEX IX_1 ON #Comps (CID, CPVersion)

SELECT  S.ExamSessionKey
 ,S.CID
 , S.CPVersion
    , D.CID
    , D.CPVersion
FROM #Comps S
LEFT JOIN DimComponents D
ON S.CID = D.CID 
 AND S.CPVersion = D.CPVersion
WHERE D.CID IS NULL

--sp_stgPopulateStgFactComponentResponses
--(1086441,1082736,1082651,1082645);

--exec [ETL].[sp_stgMergeFactComponentResponses]

