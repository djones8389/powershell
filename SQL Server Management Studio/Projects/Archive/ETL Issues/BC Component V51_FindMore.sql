Select 
	a.b.value('@id','nvarchar(12)')	 as ItemID
	, c.d.value('@typ','nvarchar(12)') as ItemType
	, west.ID
	--, StructureXML
	,e.f.value('@quT[1]','nvarchar(12)')
from Warehouse_ExamSessionTable as west

cross apply StructureXML.nodes('assessmentDetails/assessment/section/item') e(f)

INNER JOIN Warehouse_ExamSessionItemResponseTable as wesirt
on wesirt.warehouseExamSessionID = west.id
	and wesirt.ItemID = e.f.value('@id[1]','nvarchar(12)')

cross apply ItemResponseData.nodes('p') a(b)
cross apply a.b.nodes('s//c') c(d)

where 
	c.d.value('@typ','nvarchar(12)') != e.f.value('@quT[1]','nvarchar(12)')
	and west.id = 297749
--select a.b.value('@typ','tinyint') 
--from Warehouse_ExamSessionItemResponseTable

--CROSS APPLY itemresponsedata.nodes('/p//c') a(b)



SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;


Select 
	west.ID
	, e.f.value('@id[1]','nvarchar(12)')
	, e.f.value('@quT[1]','nvarchar(12)')
	--,StructureXML
from Warehouse_ExamSessionTable as west

cross apply StructureXML.nodes('assessmentDetails/assessment/section/item') e(f)

where   
	e.f.value('@quT[1]','nvarchar(12)') IS NOT NULL

EXCEPT

select 
	west.id
	, itemID	
	, a.b.value('@typ[1]','nvarchar(12)')
	
from Warehouse_ExamSessionTable as west

INNER JOIN Warehouse_ExamSessionItemResponseTable as wesirt
on wesirt.warehouseExamSessionID = west.id	

CROSS APPLY itemresponsedata.nodes('/p//c') a(b)

--where west.Keycode = 'HTCW8G89'
	




	--and a.b.value('@id[1]','nvarchar(20)') = e.f.value('@id[1]','nvarchar(20)')