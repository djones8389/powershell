/****** Script for SelectTopNRows command from SSMS  ******/
SELECT ExamSessionKey
      ,FR.Section
      ,FR.OID
      ,FR.CID
      ,FR.SID
      ,FR.CPID
      ,FR.CPVersion
      ,FR.Mark
      ,FR.RawResponse
      ,FR.ResponseKey
      ,FR.DerivedResponse
      ,FR.ShortDerivedResponse
	  ,FR.x
	  ,FR.y
	  ,DQT.*
  FROM UAT_SQA2_SurpassDataWarehouse.etl.stg_FactOptionResponses FR

    INNER JOIN DimOptions DO 
     ON DO.OID = FR.OID AND DO.CPVersion = FR.CPVersion
  
     INNER JOIN [dbo].[DimQuestionTypes] DQT
	 on DQT.QuestionTypeKey = DO.OptionType

  where cast (FR.Section as nvarchar(100)) = '1,3' 
  or  cast (FR.OID as nvarchar(100)) = '1,3' 
  or cast (FR.CID as nvarchar(100)) = '1,3'
	  or      cast (FR.SID as nvarchar(100)) = '1,3' 
	  or      cast (FR.CPID as nvarchar(100))= '1,3' 
	  or      cast (FR.CPVersion as nvarchar(100))= '1,3' 
	  or      cast (FR.Mark as nvarchar(100))= '1,3' 
	  or      cast (FR.RawResponse  as nvarchar(max))= '1,3' 
	  or      cast (FR.ResponseKey  as nvarchar(100)) = '1,3' 
	  or      cast (FR.DerivedResponse as nvarchar(100))= '1,3' 
	  or      cast (FR.ShortDerivedResponse as nvarchar(100)) = '1,3' 
	  or      cast (FR.x as nvarchar(100)) = '1,3' 
	  or      cast (FR.y as nvarchar(100)) = '1,3' 