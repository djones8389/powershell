/****** Script for SelectTopNRows command from SSMS  ******/
SELECT TOP 1000 [Id]
      ,[SourceId]
      ,[Version]
      ,[ExecutionGUID]
      ,[OriginalExecutionGUID]
      ,[PeriodStart]
      ,[PeriodEnd]
      ,[Result]
      ,[Comment]
  FROM [UAT_NCFE_SurpassDataWarehouse].[ETL].[ExecutionLog]

  select max(WAREHOUSETIME)
  from FactExamSessions

--SET IDENTITY_INSERT  [UAT_NCFE_SurpassDataWarehouse].[ETL].[ExecutionLog] ON 
  
--INSERT [UAT_NCFE_SurpassDataWarehouse].[ETL].[ExecutionLog](Id, SourceId, Version, ExecutionGUID, OriginalExecutionGUID, PeriodStart, PeriodEnd, Result, Comment)
--VALUES('1', 'B95406AA-C983-4321-BCBB-ABB563791FA5', '3.1.1562','3CEFEE0B-2E34-43E8-9117-3DDEB4238C51','3CEFEE0B-2E34-43E8-9117-3DDEB4238C51','2015-06-16 00:00:19.793', '2015-06-16 00:00:20.793', '0', 'NULL')
  
--SET IDENTITY_INSERT  [UAT_NCFE_SurpassDataWarehouse].[ETL].[ExecutionLog] OFF