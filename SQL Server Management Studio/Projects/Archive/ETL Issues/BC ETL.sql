USE UAT_BC2_SurpassDataWarehouse
 
select DISTINCT CandidateKey from ETL.stg_FactExamSessions
WHERE CandidateKey NOT IN (SELECT CandidateKey FROM DimCandidate)

--SELECT * FROM DIMCANDIDATE WHERE CANDIDATEKEY IN ('272433','272430','272434','272432','272431','272433','272430','272434','272432','272431','274449','274221','274204','274205','274206','274207','274226','274231','274221','274204','274205','274206','274207','274226','274231','274158','274387','274387','274792','274767','274770','274795','274776','274788','274813','274812','274895','274895','274897','274936','274216','274217','274218','274219','274224','274234','274229','274196','274198','274259','274260','274261','274262','274263','274265','274266','274267','274268','274894','275153','270219','274449','274449','274222','274208','274209','274227','274210','274211','274232','274214','274215','274223','274212','274228','274213','274233','274216','274217','274218','274219','274224','274234','274229','274196','274199','274792','274767','274770','274795','274776','274788','274792','274767','274770','274795','274776','274788','274813','274812','274813','274812','274936','275152','275153','272433','272430','272434','272432','272431','272083','260018','260045','260040','260020','260043','260042','271291','274308','269665','269664','274222','274208','274209','274227','274210','274211','274232','274153','274158','271179','271181','274387','274896','274896','274898','272433','272430','272434','272432','272431','274214','274215','274223','274212','274228','274213','274233','274198','274199','274387','274894','274897','275152','275153','270219','142276','142261','142260','142266','142262','142270','142264','142257','142265','142277','142258','142267','142259','142273','142263','142468','142467','142461','142469','142463','142465','143244','143243','143242','143247','143245','143248','143246','272433','272430','272434','272432','272431','272083','260018','260045','260040','260020','260043','260042','271291','274308','269665','269664','269665','274200','274201','274202','274220','274203','274225','274230','274200','274201','274202','274220','274203','274225','274230','274221','274204','274205','274206','274207','274226','274231','274307','274307','274153','271179','271181','271179','271181','274387','274894','274896','274898','271291','270219','269664','272083','260018','260045','260040','260020','260043','260042','274308','274449','274214','274215','274223','274212','274228','274213','274233','274216','274217','274218','274219','274224','274234','274229','274196','274198','274199','274792','274767','274770','274795','274776','274788','274813','274812','275152','272083','272083','260018','260045','260040','260020','260043','260042','260018','260045','260040','260020','260043','260042','271291','271291','274308','274308','269665','269664','269665','269664','273811','273811','274449','274200','274201','274202','274220','274203','274225','274230','274222','274208','274209','274227','274210','274211','274232','274307','274153','274158','271179','271181','271179','271181','274792','274767','274770','274795','274776','274788','274813','274812','274895','274897','274898','274936','275247','275249','275250','275252','275251','275253','275254','275255','275256','275246','275257','275258','275259','275260','275261','275263','275265','275264','275262','275266','275289','270219')

--SELECT
-- MAX(COALESCE(DC.LatestWHCandidateKey,0)) LastWHUserID
--FROM [dbo].[FactExamSessions] FES
-- LEFT OUTER JOIN dbo.DimCandidate DCr
-- ON FES.CandidateKey = DC.CandidateKey

--USE UAT_BC2_SECUREASSESS



SELECT SRC.CandidateKey, TRG.CandidateKey 
FROM ETL.stg_FactExamSessions as SRC

INNER JOIN FactExamSessions as TRG
on TRG.ExamSessionKey = SRC.ExamKey

WHERE SRC.CompletionDateKey IS NOT NULL 
AND SRC.GradeKey IS NOT NULL 
AND SRC.AdjustedGradeKey IS NOT NULL 
AND SRC.ClientMachineKey IS NOT NULL 
AND (
		trg.[OriginatorKey] <> src.[OriginatorKey]
		or trg.[WarehouseTime] <> src.[WarehouseTime]
		or trg.[CompletionDateKey] <> src.[CompletionDateKey]
		or trg.[CompletionTime] <> src.[CompletionTime]
		or trg.[KeyCode] <> src.[KeyCode]
		or trg.[ExamKey] <> src.[ExamKey]
		or trg.[QualificationKey] <> src.[QualificationId]
		or trg.[CentreKey] <> src.[CentreKey]
		or trg.[CandidateKey] <> src.[CandidateKey]
		or trg.[GradeKey] <> src.[GradeKey]
		or trg.[Pass] <> src.[Pass]
		or trg.[TimeTaken] <> src.[TimeTaken]
		or trg.[UserMarks] <> src.[UserMarks]
		or trg.[TotalMarksAvailable] <> src.[TotalMarksAvailable]
		or trg.[PassMark] <> src.[PassMark]
		or trg.[Invigilated] <> src.[Invigilated]
		or trg.[ClientMachineKey] <> src.[ClientMachineKey]
		or trg.[FinalExamState] <> src.[FinalExamState]
		or trg.[VoidReasonKey] <> src.[VoidReasonKey]
		or trg.[VoidReason] <> src.[VoidReason]
		or trg.[ExamVersionKey] <> src.[ExamVersionKey]
		or trg.[IsMysteryShopper] <> src.[IsMysteryShopper]
		or trg.[ExamStatus] <> src.[ExamStatus]
		or trg.[Duration] <> src.[Duration]
		or trg.[AdjustedGradeKey] <> src.[AdjustedGradeKey]
		)
AND (
     SRC.CandidateKey in ('274767','275254','142273','274267','274218','274201','274265','275152','271291','275252','274233','274199','274216','274231','260042','274214','142267','143246','272434','270219','274770','274813','275263','274227','274788','275249','269665','274213','274202','272430','142260','274795','274263','274220','275256','142467','274223','274209','274206','260020','142264','275260','274224','142463','274259','142263','275259','274792','274895','274229','142276','273811','275265','142265','143244','274261','274204','272432','274897','274222','275258','142469','274211','275247','142258','274896','272083','274221','275257','142259','142468','275266','275289','274230','274387','274196','274219','271179','275255','142257','142266','275264','274153','274262','142461','274898','274260','274228','260043','274215','274266','142270','275251','274232','274936','274198','143242','274268','274217','274200','274449','260045','275153','275253','274234','274776','275262','274226','271181','274307','274894','260018','269664','142262','143247','274207','274158','142465','275261','274225','274308','142261','143248','274208','260040','274212','143243','274203','275246','274210','272433','274205','142277','274812','275250','143245','272431')
	 OR
     TRG.CandidateKey in ('274767','275254','142273','274267','274218','274201','274265','275152','271291','275252','274233','274199','274216','274231','260042','274214','142267','143246','272434','270219','274770','274813','275263','274227','274788','275249','269665','274213','274202','272430','142260','274795','274263','274220','275256','142467','274223','274209','274206','260020','142264','275260','274224','142463','274259','142263','275259','274792','274895','274229','142276','273811','275265','142265','143244','274261','274204','272432','274897','274222','275258','142469','274211','275247','142258','274896','272083','274221','275257','142259','142468','275266','275289','274230','274387','274196','274219','271179','275255','142257','142266','275264','274153','274262','142461','274898','274260','274228','260043','274215','274266','142270','275251','274232','274936','274198','143242','274268','274217','274200','274449','260045','275153','275253','274234','274776','275262','274226','271181','274307','274894','260018','269664','142262','143247','274207','274158','142465','275261','274225','274308','142261','143248','274208','260040','274212','143243','274203','275246','274210','272433','274205','142277','274812','275250','143245','272431')
)



/*
MERGE dbo.FactExamSessions WITH (TABLOCK) trg
	USING (SELECT * FROM ETL.stg_FactExamSessions WHERE CompletionDateKey IS NOT NULL AND GradeKey IS NOT NULL AND AdjustedGradeKey IS NOT NULL AND ClientMachineKey IS NOT NULL )src
	ON 
		trg.[ExamSessionKey] = src.[ExamSessionKey]
	WHEN MATCHED AND (
		trg.[OriginatorKey] <> src.[OriginatorKey]
		or trg.[WarehouseTime] <> src.[WarehouseTime]
		or trg.[CompletionDateKey] <> src.[CompletionDateKey]
		or trg.[CompletionTime] <> src.[CompletionTime]
		or trg.[KeyCode] <> src.[KeyCode]
		or trg.[ExamKey] <> src.[ExamKey]
		or trg.[QualificationKey] <> src.[QualificationId]
		or trg.[CentreKey] <> src.[CentreKey]
		or trg.[CandidateKey] <> src.[CandidateKey]
		or trg.[GradeKey] <> src.[GradeKey]
		or trg.[Pass] <> src.[Pass]
		or trg.[TimeTaken] <> src.[TimeTaken]
		or trg.[UserMarks] <> src.[UserMarks]
		or trg.[TotalMarksAvailable] <> src.[TotalMarksAvailable]
		or trg.[PassMark] <> src.[PassMark]
		or trg.[Invigilated] <> src.[Invigilated]
		or trg.[ClientMachineKey] <> src.[ClientMachineKey]
		or trg.[FinalExamState] <> src.[FinalExamState]
		or trg.[VoidReasonKey] <> src.[VoidReasonKey]
		or trg.[VoidReason] <> src.[VoidReason]
		or trg.[ExamVersionKey] <> src.[ExamVersionKey]
		or trg.[IsMysteryShopper] <> src.[IsMysteryShopper]
		or trg.[ExamStatus] <> src.[ExamStatus]
		or trg.[Duration] <> src.[Duration]
		or trg.[AdjustedGradeKey] <> src.[AdjustedGradeKey]
		)
	THEN 
		UPDATE SET  
		[OriginatorKey] = src.[OriginatorKey]
		, [WarehouseTime] = src.[WarehouseTime]
		, [CompletionDateKey] = src.[CompletionDateKey]
		, [CompletionTime] = src.[CompletionTime]
		, [KeyCode] = src.[KeyCode]
		, [ExamKey] = src.[ExamKey]
		, [QualificationKey] = src.[QualificationId]
		, [CentreKey] = src.[CentreKey]
		, [CandidateKey] = src.[CandidateKey]
		, [GradeKey] = src.[GradeKey]
		, [Pass] = src.[Pass]
		, [TimeTaken] = src.[TimeTaken]
		, [UserMarks] = src.[UserMarks]
		, [TotalMarksAvailable] = src.[TotalMarksAvailable]
		, [PassMark] = src.[PassMark]
		, [Invigilated] = src.[Invigilated]
		, [ClientMachineKey] = src.[ClientMachineKey]
		, [FinalExamState] = src.[FinalExamState]
		, [VoidReasonKey] = src.[VoidReasonKey]
		, [VoidReason] = src.[VoidReason]
		, [ExamVersionKey] = src.[ExamVersionKey]
		, [IsMysteryShopper] = src.[IsMysteryShopper]
		, [ExamStatus] = src.[ExamStatus]
		, [Duration] = src.[Duration]
		, [AdjustedGradeKey] = src.[AdjustedGradeKey]
		
	WHEN NOT MATCHED THEN 
		INSERT ([ExamSessionKey], [OriginatorKey], [WarehouseTime], [CompletionDateKey], [CompletionTime], [KeyCode], [ExamKey], [QualificationKey], [CentreKey], [CandidateKey], [GradeKey], [Pass], [TimeTaken], [UserMarks], [TotalMarksAvailable], [PassMark], [Invigilated], [ClientMachineKey], [FinalExamState], [VoidReasonKey], [VoidReason], [ExamVersionKey], [IsMysteryShopper], [ExamStatus],[Duration], [AdjustedGradeKey])
		VALUES ([ExamSessionKey], [OriginatorKey], [WarehouseTime], [CompletionDateKey], [CompletionTime], [KeyCode], [ExamKey], [QualificationId], [CentreKey], [CandidateKey], [GradeKey], [Pass], [TimeTaken], [UserMarks], [TotalMarksAvailable], [PassMark], [Invigilated], [ClientMachineKey], [FinalExamState], [VoidReasonKey], [VoidReason], [ExamVersionKey], [IsMysteryShopper], [ExamStatus],[Duration], src.[AdjustedGradeKey])
	;
*/