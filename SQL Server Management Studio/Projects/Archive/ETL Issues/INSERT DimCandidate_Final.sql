USE BRITISHCOUNCIL_SecureAssess


DECLARE @UsersToInsert TABLE
(

    CandidateKey int
	, LatestWHCandidateKey int
	, Version tinyint
	, Forename nvarchar(100)
	, Surname nvarchar(100)
	, EthnicOrigin nvarchar(70)
	, DOB smalldatetime
	, CandidateRef nvarchar(300)
	, ULN nvarchar(10)
	, MiddleName nvarchar(50)
	, Gender char(1)
	, SpecialRequirements NVARCHAR(MAX)--XML
	, Addressline1 nvarchar(50)
	, AddressLine2 nvarchar(50)
	, Town nvarchar(100)
	, County nvarchar(100)
	, Country nvarchar(100)
	, Postcode nvarchar(12)
	, Telephone nvarchar(15)
	, Email nvarchar(max)
	, IsExaminer bit
	, UserName nvarchar(50)
)

INSERT @UsersToInsert

SELECT 
	CandidateKey
	,LatestWHCandidateKey
	,Version
	, Forename
	, Surname
	, EthnicOrigin
	, DOB
	, CandidateRef
	, ULN
	, MiddleName
	, Gender
	, SpecialRequirements
	, AddressLine1
	, AddressLine2
	, Town
	, County
	, Country
	, PostCode
	, Telephone
	, Email
	, IsExaminer
	, UserName
FROM
(
	SELECT
		ROW_NUMBER() OVER(PARTITION BY UserId ORDER BY ID DESC) N
		,WUT.UserId CandidateKey
		,WUT.[ID] LatestWHCandidateKey
		,WUT.[version] [Version]
		,WUT.[Forename]
		,WUT.[Surname]
		,(SELECT [EthnicOriginKey] FROM [430326-BC-SQL2\SQL2].BritishCouncil_SurpassDataWarehouse.[dbo].[DimEthnicOrigins] where EthnicOrigin = WUT.[EthnicOrigin]) EthnicOrigin
		,WUT.[DOB]
		,WUT.[CandidateRef]
		,CONVERT(nvarchar(10),'') ULN
		,WUT.[Middlename]
		,WUT.[Gender]
		,CONVERT(NVARCHAR(MAX),WUT.[SpecialRequirements]) SpecialRequirements
		,WUT.[AddressLine1]
		,WUT.[AddressLine2]
		,WUT.[Town]
		,(Select CountyKey from [430326-BC-SQL2\SQL2].BritishCouncil_SurpassDataWarehouse.[dbo].[DimCounty] where County = WUT.[County]) County
		,(Select CountryKey from [430326-BC-SQL2\SQL2].BritishCouncil_SurpassDataWarehouse.[dbo].[DimCountry] where Country = WUT.[Country]) Country
		,WUT.[PostCode]
		,WUT.[Telephone]
		,CONVERT(NVARCHAR(100),WUT.[Email]) Email
		,CONVERT(BIT, 0) IsExaminer
		,UserName
	FROM [dbo].[WAREHOUSE_UserTable] WUT
	WHERE ID > 0
		AND ID <= 2147483647
) A	
		
		WHERE N=1	--Filter duplicates
		AND A.CandidateKey IN (
		
		select CandidateKey	
		from [430326-BC-SQL2\SQL2].BritishCouncil_SurpassDataWarehouse.[dbo].FactExamSessions
		WHERE CandidateKey NOT IN 
								(SELECT CandidateKey 
								FROM [430326-BC-SQL2\SQL2].BritishCouncil_SurpassDataWarehouse.[dbo].DimCandidate
								)
		UNION
	    		
		select CandidateKey	
		from [430326-BC-SQL2\SQL2].BritishCouncil_SurpassDataWarehouse.ETL.stg_FactExamSessions
		WHERE CandidateKey NOT IN 
								(SELECT CandidateKey 
								FROM [430326-BC-SQL2\SQL2].BritishCouncil_SurpassDataWarehouse.[dbo].DimCandidate
								)			
								
	)
	
	
INSERT [430326-BC-SQL2\SQL2].BritishCouncil_SurpassDataWarehouse.[dbo].DimCandidate(CandidateKey, LatestWHCandidateKey, Version, Forename, Surname, EthnicOriginKey, DOB, CandidateRef, Username, ULN, Middlename, Gender, SpecialRequirements, AddressLine1, AddressLine2, Town, PostCode, Telephone, Email, CountryKey, CountyKey, IsExaminer)
SELECT CandidateKey, LatestWHCandidateKey, Version, Forename, Surname, EthnicOrigin, DOB, CandidateRef,  Username, ULN,  Middlename, Gender, SpecialRequirements, AddressLine1, AddressLine2, Town, PostCode, Telephone, Email, Country, County, IsExaminer   FROM @UsersToInsert

	
	
	
	