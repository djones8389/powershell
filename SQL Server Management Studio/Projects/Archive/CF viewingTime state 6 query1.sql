DECLARE @IDs TABLE (ID int)
INSERT INTO @IDs(ID)
	select id
		from  Warehouse_ExamSessionTable as WEST (NOLOCK)
		
		cross apply west.structurexml.nodes('assessmentDetails/assessment/section/item') s(i)

			where  west.warehousetime > '01 Apr 2015 00:00:01'
				and examStateChangeAuditXMl.exist('exam/stateChange[newStateID=6]') = 1
				group by ID
				having (sum(s.i.value('@viewingTime[1]','int'))) = 0;


select west.ExamSessionID as ID
	, west.KeyCode
	, candidateRef
	, centreCode
	, purchaseOrder
	, wests.examName
	, sum(s.i.value('@viewingTime[1]','int')) as ViewingTimes
from  Warehouse_ExamSessionTable as WEST (NOLOCK)

inner join WAREHOUSE_ScheduledExamsTable as WSCET (NOLOCK)
on WSCET.ID = WEST.WAREHOUSEScheduledExamID

inner join WAREHOUSE_ExamSessionTable_Shreded as WESTS (NOLOCK)
on WESTS.examSessionId = WEST.ID

cross apply west.structurexml.nodes('assessmentDetails/assessment/section/item') s(i)

where west.id in (Select id from @IDs)
  group by WEST.ExamSessionID, west.KeyCode, candidateRef, centreCode, purchaseOrder, wests.examName
	having (sum(s.i.value('@viewingTime[1]','int'))) = 0;


--select top 1 examstatechangeauditxml
--from Warehouse_ExamSessionTable
--where examStateChangeAuditXMl.exist('exam/stateChange/newStateID') = 1