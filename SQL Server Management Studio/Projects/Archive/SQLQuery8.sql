;with cte as
(
SELECT ExternalSessionID
	FROM Candidateexamversions 
		INNER JOIN CandidateResponses
		ON CandidateExamVersionID = Candidateexamversions.ID
		INNER JOIN UniqueResponses
		ON UniqueResponseID = UniqueResponses.ID

where CONVERT(NVARCHAR(MAX), responseData) = ''

)

--Select WEST.ID, KeyCode, ItemID, ItemResponseData
--	 from BritishCouncil_SecureAssess_LIVE..Warehouse_ExamSessionTable as WEST

--	inner join BritishCouncil_SecureAssess_LIVE..Warehouse_ExamSessionItemResponseTable as WESIRT
--	on WESIRT.WarehouseExamSessionID = WEST.ID

--where WEST.ID IN (Select ExternalSessionID from cte)
--	and  ItemResponseData.exist('p[@ua=1]') = 1
	
	
	
	
SELECT CEV.ExternalSessionID, Keycode, Ur.responseData, i.ExternalItemID
FROM CandidateExamVersions as CEV

INNER JOIN CandidateResponses as CR
ON CR.CandidateExamVersionID = CEV.ID

INNER JOIN UniqueResponses as UR
ON UR.ID = UniqueResponseID 

left join UniqueResponseDocuments as URD
ON URD.UniqueResponseID = UR.id 
	
Inner Join Items as I
on I.ID = ur.itemId

inner join ExamVersions as EV
on EV.ID = I.ExamVersionID

INNER JOIN Exams as E
ON E.ID = EV.ExamID

INNER JOIN Qualifications
ON E.QualificationID = Qualifications.ID


where CEV.ExternalSessionID in (Select ExternalSessionID from cte)  --Keycode in ('BAKBUE01','HPMXJY01','VQVL2D01','7A3JR401','4WHNHR01','A74GGX01','QFE6JU01','6JJC2X99','CT7W4Y99','F2PF7V99','V4TEMK99','7YSHYB99','RSQNU599','FAQSLN99','3WLK5M99','6JJQEH99','DLY9KV99','5Z4P3P99','NG6GT601','EY5HS401','Q83NS399','MESZUL99','LBTDZE99')
	order by Keycode
	