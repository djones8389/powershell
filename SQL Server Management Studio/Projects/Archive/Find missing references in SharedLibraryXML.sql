select * from SharedLibraryTable where ProjectId in (931)
select * from ProjectManifestTable where ProjectId in (931)


--DECLARE IDs CURSOR FOR
--      select distinct ProjectId 
--            from ProjectManifestTable 
  

--OPEN IDs;


--DECLARE @ID int;

--FETCH NEXT FROM IDs INTO @ID;

--WHILE @@FETCH_STATUS = 0
--BEGIN



declare @ProjectId int = 931;

select
	CAST(structurexml as xml) as SharedLibraryXML
		into #DJTemp
			from SharedLibraryTable
				where ProjectId = @ProjectId;


create table #newTable 
	( 

		itemID int

	);
	
insert into #newTable

select
	 s.i.value('@id','int') as ItemID
	 
	from #DJTemp
	
	cross apply SharedLibraryXML.nodes('/sharedLibrary/item') s(i);
		
drop table #DJTemp;


select itemID as 'SharedLibraryXMLID', ID as 'ProjectManifestTableID', name, version, location, delCol, sharedLib, publishable, hotspot, source, ProjectId, Image, isPreview
	 from #newTable

right join ProjectManifestTable
on ProjectManifestTable.ID = #newTable.itemID

where ProjectId = @ProjectId
	and itemID is null
	and sharedLib = 1;

drop table #newTable;

--FETCH NEXT FROM IDs INTO @ID;
--END

--CLOSE IDs;
--DEALLOCATE IDs;

