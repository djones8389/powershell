--select id, name, projectid
--from ProjectManifestTable
--where name in (
--		select name
--			from projectManifestTable with (NOLOCK)
--			where projectid =  942
--			group by name
--			having count(*) > 1
--	)
--	and location not like '%background%'
--	and projectid = 942



;with cte as (

select A.*
FROM
(

select id as ID
              , name as Name
              , projectid   as ProjectID
from ProjectManifestTable as PMT with (NOLOCK)
where name in (
		select name
			from projectManifestTable with (NOLOCK)
			where projectid =  942
			group by name
			having count(*) > 1
	)
and location not like '%background%'
and projectid = 942
) A
--UNION
INNER JOIN
(
select  a.b.value ('@id', 'int') as  ID
        , a.b.value ('@name', 'nvarchar(200)') as Name
        , b.projectid   as ProjectID                           
from
(
       select projectid
              , cast(Structurexml as xml) as Structurexml
       from SharedLibraryTable as SLT with (NOLOCK)
	   where projectid = 942
       ) B
       cross apply Structurexml.nodes('//item') a(b)
	   --where a.b.value ('@name', 'nvarchar(200)') = '10.jpg'
		--or a.b.value ('@id', 'int') = 10
) C

on A.ID = C.ID
	and A.Name = C.Name
		And A.ProjectID = C.ProjectID
)



--select * from cte

select cte.ID
       ,  Name
       , ProjectID
       , Items.id as ItemID
	   , items.ali
from cte 
       
LEFT JOIN                                                     --Find on any pages
(
      SELECT ID, ali
      FROM ItemCustomQuestionTable with (NOLOCK)
      UNION
      SELECT ID, ali
      FROM ItemGraphicTable with (NOLOCK)
      UNION
      SELECT ID, ali
      FROM ItemVideoTable with (NOLOCK)
      UNION
      SELECT ID, ali
      FROM ItemHotSpotTable with (NOLOCK)
) Items
ON 

      Items.ID LIKE CONVERT(NVARCHAR(10), cte.ProjectID) + 'P%' 
      AND 
      (
            CONVERT(NVARCHAR(50), cte.ID) = ali
            OR
            SUBSTRING(cte.Name, 0, LEN(cte.Name) - 3) = ali
      )
	  --and cast(cte.id as nvarchar(MAX)) = cast(items.id as nvarchar(MAX))


