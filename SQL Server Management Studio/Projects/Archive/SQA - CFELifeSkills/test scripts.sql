
SELECT top 10 * 
from SQA_CPProjectAdmin_DI..ProjectManifestTable
where ProjectId = 942 and datalength(IMAGE) IS NULL

SELECT * 
from SQA_CPProjectAdmin_DI..ProjectListTable
where ID = 260

update SQA_CPProjectAdmin_DI..ProjectManifestTable
set sharedLib = 1
where ProjectId = 260 and name IN ('background_with_line.jpg')

SELECT * 
from UAT_SQA_ContentProducer_BeforeFix..ProjectManifestTable
where  name IN ('background_with_line.jpg')

SELECT id, name, projectid, image
from UAT_SQA_ContentProducer..ProjectManifestTable
where ProjectId = 260

SELECT id, name, projectid, image
from SQA_CPProjectAdmin_LIVE..ProjectManifestTable
where ProjectId = 260

begin tran
UPDATE SQA_CPProjectAdmin_DI..ProjectManifestTable
SET image = (select image from SQA_CPProjectAdmin_DI..ProjectManifestTable where projectid = 942 and name = 'lefthandtemplate1030x700.jpg')
where name = 'lefthandtemplate1030x700.jpg' and image is  null

rollback


select projectid, cast(Structurexml as xml) as Structurexml

from SharedLibraryTable
where projectid in (942)