--IF OBJECT_ID ('tempdb..#SharedLibStructure') IS NOT NULL DROP TABLE #ProjectStructure;
IF OBJECT_ID ('tempdb..#MyTable') IS NOT NULL DROP TABLE #MyTable;
IF object_ID ('tempdb..#TempTable') IS NOT NULL DROP TABLE #TempTable;
--DECLARE @MyItems TABLE (ItemID nvarchar(max), PMTID int, PMTName nvarchar(200), PMTProjectID int, SLItemID int, SLItemName nvarchar(200), SLProjectID int)
--insert into @MyItems(ItemID, PMTID, PMTName, PMTProjectID, SLItemID, SLItemName, SLProjectID)

DECLARE @ProjectID int = 942


select  Items.id as ItemID
	 , A.PMTID			
	 , A.PMTName
	 , A.PMTProjectID
	 , B.SLItemID
	 , B.SLItemName
	 , B.SLProjectID
  into #MyTable
from (

select id as PMTID
		, name as PMTName
		, projectid	as PMTProjectID
from ProjectManifestTable as PMT with (NOLOCK)
where image is null
	--and location not like '%background%'
	and projectid = @ProjectID
) A

LEFT join   

(
select 	 a.b.value ('@id', 'int') as  SLItemID
		, a.b.value ('@name', 'nvarchar(200)') as  SLItemName
		, b.projectid	  as 'SLProjectID'					
from
(
	select projectid
		, cast(Structurexml as xml) as Structurexml
	from SharedLibraryTable as SLT with (NOLOCK)
	where projectid = @ProjectID
	) B
	cross apply Structurexml.nodes('//item') a(b)
) B 

on A.PMTID = b.SLItemID
	and a.PMTProjectID = b.SLProjectID

		
LEFT JOIN								--Find on any pages
(
      SELECT ID, ali
      FROM ItemCustomQuestionTable with (NOLOCK)
      UNION
      SELECT ID, ali
      FROM ItemGraphicTable with (NOLOCK)
      UNION
      SELECT ID, ali
      FROM ItemVideoTable with (NOLOCK)
      UNION
      SELECT ID, ali
      FROM ItemHotSpotTable with (NOLOCK)
) Items
ON 

      Items.ID LIKE CONVERT(NVARCHAR(10), A.PMTProjectID) + 'P%' 
      AND 
      (
            CONVERT(NVARCHAR(50), A.PMTID) = ali
            OR
            SUBSTRING(A.PMTName, 0, LEN(A.PMTName) - 3) = ali
      )


WHERE Items.id IS NOT NULL;				--Where image is on pages


select ROW_NUMBER() over (order by name) rownum
                  , name
                  , Image
      into #TempTable
      from ProjectManifestTable

where Image is not null
      and name in (
                    select name
                        from ProjectManifestTable
                                where ProjectId = @ProjectID
                                and Image is null
                  );


delete
from #TempTable
where rownum not in (   
      
      select rownum
      from 
      #TempTable as R
      inner join
            (
            select  name, MAX(rownum) as RN --Max(DATALENGTH(image)) as 'Myimage'
                  from #TempTable               
                  
                  group by name
                  
            ) X
      on r.rownum = X.RN
);


select * from #MyTable where pmtname in (select name from #TempTable); -- Report to show you affected pages that we can fix



SELECT * FROM #TempTable;  --Find images we can fix



update ProjectManifestTable
set Image = (select Image from #TempTable where #TempTable.name = ProjectManifestTable.name)
where ProjectId = @ProjectID
		and Image is null;
