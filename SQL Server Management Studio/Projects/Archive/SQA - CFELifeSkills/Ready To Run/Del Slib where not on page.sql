IF OBJECT_ID ('tempdb..#SharedLibStructure') IS NOT NULL DROP TABLE #SharedLibStructure;

DECLARE @MyProjectID INT = 942;

DECLARE @MyItems TABLE (projectid int, id int, name nvarchar(200))
insert into @MyItems(projectid, ID, name)

select 
	 B.ProjectID as 'SLProjectID'
	 , b.ItemID as 'SLItemID'
	 , B.ItemName as 'SLItemName'
from
(
select b.projectid						--Where exists in SLib
	, a.b.value ('@id', 'int') as  ItemID
	, a.b.value ('@name', 'nvarchar(max)') as  ItemName
from
(
	select projectid
		, cast(Structurexml as xml) as Structurexml
	from SharedLibraryTable as SLT with (NOLOCK)
	where projectid = @MyProjectID
	) B
	cross apply Structurexml.nodes('//item') a(b)
) B 

		
LEFT JOIN								--Find on any pages
(
      SELECT ID, ali
      FROM ItemCustomQuestionTable with (NOLOCK)
      UNION
      SELECT ID, ali
      FROM ItemGraphicTable with (NOLOCK)
      UNION
      SELECT ID, ali
      FROM ItemVideoTable with (NOLOCK)
      UNION
      SELECT ID, ali
      FROM ItemHotSpotTable with (NOLOCK)
) Items
ON 
      Items.ID LIKE CONVERT(NVARCHAR(10), B.ProjectId) + 'P%' 
      AND 
      (
            CONVERT(NVARCHAR(50), B.ItemID) = ali 
            OR
            SUBSTRING(B.ItemName, 0, LEN(B.ItemName) - 3) = ali
      )

WHERE  Items.id IS  NULL				--Where image is not on any pages
	and	 B.ProjectID = @MyProjectID;


SELECT * FROM @MyItems order by name; --Keep for backup purposes

select projectid, cast(Structurexml as xml) as Structurexml
into #SharedLibStructure
from SharedLibraryTable
where projectid in (select ProjectID from @MyItems)

select * from #SharedLibStructure;	--Keep for backup purposes

DECLARE SharedLibrary CURSOR FOR 
SELECT ID
	  , Name
	  , ProjectID
FROM @MyItems

DECLARE @ID int, @Name nvarchar(max), @ProjectID int;

OPEN SharedLibrary;

FETCH NEXT FROM SharedLibrary INTO  @ID, @Name , @ProjectID

WHILE @@Fetch_Status = 0

BEGIN

Update #SharedLibStructure
set Structurexml.modify('delete(/sharedLibrary/item[@id=sql:variable("@ID")]  [@name=sql:variable("@Name")]) ')
where projectid = @ProjectID

FETCH NEXT FROM SharedLibrary INTO  @ID, @Name , @ProjectID

END
CLOSE SharedLibrary;
DEALLOCATE SharedLibrary;


update SharedLibraryTable
set structureXML = CAST(b.Structurexml as nvarchar(MAX))
from SharedLibraryTable a
inner join #SharedLibStructure b
on a.ProjectId = b.ProjectId
where a.ProjectId = b.ProjectId;

select * from #SharedLibStructure;

DROP TABLE #SharedLibStructure;


