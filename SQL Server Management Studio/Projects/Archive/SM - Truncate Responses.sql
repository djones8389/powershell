SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

SELECT TOP 50 ur.id
	,Keycode
	,DATALENGTH(UR.responseData)
	,UR.responseData
FROM CandidateExamVersions AS CEV WITH (NOLOCK)
INNER JOIN CandidateResponses AS CR WITH (NOLOCK) ON CR.CandidateExamVersionID = CEV.ID
INNER JOIN UniqueResponses AS UR WITH (NOLOCK) ON UR.ID = UniqueResponseID
INNER JOIN UniqueGroupResponseLinks AS UGRL WITH (NOLOCK) ON UGRL.UniqueGroupResponseID = UR.id
INNER JOIN UniqueGroupResponses AS UGR WITH (NOLOCK) ON UGR.ID = UGRL.UniqueGroupResponseID
INNER JOIN AssignedGroupMarks AS AGM WITH (NOLOCK) ON AGM.UniqueGroupResponseId = UGR.ID
LEFT JOIN UniqueResponseDocuments AS URD WITH (NOLOCK) ON URD.UniqueResponseID = UR.id
INNER JOIN Items AS I WITH (NOLOCK) ON I.ID = ur.itemId
INNER JOIN ExamVersions AS EV WITH (NOLOCK) ON EV.ID = I.ExamVersionID
INNER JOIN Exams AS E WITH (NOLOCK) ON E.ID = EV.ExamID
INNER JOIN Qualifications AS Q WITH (NOLOCK) ON E.QualificationID = Q.ID
WHERE e.NAME = 'Writing'
	AND CentreName IS NOT NULL
ORDER BY DATALENGTH(responseData) DESC;


select responseData
--, responseData.query('/p/s/c[1]/i')
, responseData.value('(/p/s/c[1]/i)[1]', 'nvarchar(max)')
, LEN(responseData.value('(/p/s/c[1]/i)[1]', 'nvarchar(max)')) as CharLength
, responseData.value('(/p/s/c[2]/i)[1]', 'nvarchar(max)')
, LEN(responseData.value('(/p/s/c[2]/i)[1]', 'nvarchar(max)')) as CharLength
--, LEN(responseData.query('/p/s/c[1]/i'))
	from UniqueResponses where ID = 162994