USE UAT_BCGuilds_TestPackage_11_6_0_241_IP

SELECT *
FROM dbo.ScheduledPackageCandidateExams
WHERE ScheduledExamRef = 'YJMTWR99'

USE UAT_BCGuilds_SecureAssess_11_7_223_0_IP

SELECT EST.ID, EST.KeyCode, EST.examState, UT.Forename, UT.Surname, CT.CentreName, SCET.examName, SCET.CreatedBy
  FROM ExamSessionTable as EST (NOLOCK)

  Inner Join ScheduledExamsTable as SCET (NOLOCK)
  on SCET.ID = EST.ScheduledExamID
   
  Inner Join UserTable as UT (NOLOCK)
  on UT.ID = EST.UserID

  Inner Join CentreTable as CT (NOLOCK)
  on CT.ID = SCET.CentreID

WHERE KeyCode = 'YJMTWR99'

--SELECT * FROM dbo.UserTable WHERE id  = 23
--SELECT * FROM dbo.ExamStateChangeAuditTable WHERE ExamSessionID = 27
SELECT warehouseTime, WarehouseExamState ,* FROM dbo.WAREHOUSE_ExamSessionTable WHERE KeyCode = 'YJMTWR99'
SELECT * FROM UAT_BCGuilds_TestPackage_11_6_0_241_IP..CommonSettings
--08/04/2015 13:34:12  COMMONSETTINGS
--2015-08-04 14:51:38.917  warehouseTime

--UPDATE dbo.WAREHOUSE_ExamSessionTable
--SET warehouseTime = '2015-08-05 10:00:00.917'

--UPDATE dbo.WAREHOUSE_ExamSessionTable_Shreded
--SET warehouseTime = '2015-08-05 10:00:00.917'

--SELECT ID, KeyCode, warehouseTime
--INTO #TempTable
--FROM dbo.WAREHOUSE_ExamSessionTable 

--UPDATE dbo.WAREHOUSE_ExamSessionTable
--SET warehouseTime = b.warehouseTime
--FROM dbo.WAREHOUSE_ExamSessionTable A
--INNER JOIN #TempTable AS B
--ON A.ID = B.ID

--UPDATE dbo.WAREHOUSE_ExamSessionTable_Shreded
--SET warehouseTime = b.warehouseTime
--FROM dbo.WAREHOUSE_ExamSessionTable_Shreded A
--INNER JOIN #TempTable AS B
--ON A.examSessionId = B.ID





SELECT WEST.ID, WEST.KeyCode, WUT.Forename, WUT.Surname, WUT.CandidateRef, WCT.CentreName, WSCET.examName, WEST.completionDate
	, WESTS.ExternalReference, WESTS.examRef, WCT.CentreName
FROM WAREHOUSE_ExamSessionTable as WEST (NOLOCK)

  Inner Join WAREHOUSE_ScheduledExamsTable as WSCET (NOLOCK)
  on WSCET.ID = WEST.WAREHOUSEScheduledExamID
  
  Inner Join WAREHOUSE_UserTable as WUT (NOLOCK)
  on WUT.ID = WEST.WAREHOUSEUserID

  Inner Join WAREHOUSE_CentreTable as WCT (NOLOCK)
  on WCT.ID = WSCET.WAREHOUSECentreID

  Inner Join WAREHOUSE_ExamSessionTable_Shreded as WESTS (NOLOCK)
  on WESTS.examSessionId = WEST.ID
