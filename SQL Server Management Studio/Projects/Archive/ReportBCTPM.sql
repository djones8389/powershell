USE BRITISHCOUNCIL_TPM

;with cte as (

	select Keycode, CreatedDateTime 
		from BritishCouncil_SecureAssess_LIVE..WAREHOUSE_ExamSessionTable as WEST
			
			Inner join BritishCouncil_SecureAssess_LIVE..WAREHOUSE_ScheduledExamsTable as WSCET
			on WSCET.ID = WEST.WAREHOUSEScheduledExamID
		
		where CreatedDateTime > '15 Nov 2014'	

)


select distinct
	SPC.IsVoided
	, P.Name
	, FirstName
	, LastName
	, SurpassCandidateRef
	, PackageScore
	, SP.CenterName
	, SPC.DateCompleted
	--,*
	, CreatedDateTime
from ScheduledPackageCandidates as SPC

	left join ScheduledPackageCandidateExams as SPCE
	on SPCE.Candidate_ScheduledPackageCandidateId = SPC.ScheduledPackageCandidateId

	left join PackageExams as PE
	on PE.PackageExamId = SPCE.PackageExamId
	
	left join Packages as P
	on P.PackageId = PE.PackageId	

	inner join ScheduledPackages as SP
	on SP.ScheduledPackageId = SPC.ScheduledPackage_ScheduledPackageId

	inner join cte
	on cte.KeyCode collate Latin1_General_CI_AS  = SPCE.ScheduledExamRef

where ScheduledExamRef in (select Keycode collate Latin1_General_CI_AS from cte)
	order by DateCompleted asc