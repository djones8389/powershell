DISABLE TRIGGER [dbo].[WarehouseExamState_Trigger] ON  [dbo].[WAREHOUSE_ExamSessionTable];

CREATE TABLE [#TMP_Marks_Table](
	[ExamSessionId] [int] NOT NULL,
	[ItemID] [nvarchar](50) NULL,
	[TotalMark] [float] NULL,
	[UserMark] [float] NULL
);

INSERT INTO #TMP_Marks_Table(ExamSessionId, ItemID, totalMark, UserMark )	
SELECT ID, 
			item.node.value('@id[1]','nvarchar(50)') as itemID,
			item.node.value('@totalMark[1]','float') as totalMark,
			item.node.value('@userMark[1]','float') as userMark
FROM dbo.WAREHOUSE_ExamSessionTable
CROSS APPLY StructureXML.nodes('/assessmentDetails/assessment/section/item') item(node)
WHERE ID IN (1530080,1530081,1530082,1530083,1530084,1530085,1530086,1530087,1530088,1530089,1530090,1530091,1530092,1530093,1530094,1530095,1530096,1530097,1530219,1530220,1530221)

DECLARE @ExamsessionId INT;
DECLARE @SectionId INT;
DECLARE @StructureXML XML;
DECLARE @ResultData XML;
DECLARE @ResultDataFULL XML;
DECLARE @ItemID VARCHAR(50);
DECLARE @ItemTotalMark float = 0;
DECLARE @SectionTotalMark float = 0;
DECLARE @SectionUserMarks float = 0;
DECLARE @SectionUserPercentage float;
DECLARE @ExamTotalMark float = 0;
DECLARE @ExamUserMarks float = 0;
DECLARE @ExamUserPercentage float = 0;

DECLARE Exams_cursor CURSOR FAST_FORWARD FOR 
	SELECT DISTINCT ExamsessionId 
	FROM #TMP_Marks_Table;

OPEN Exams_cursor;

FETCH NEXT FROM Exams_cursor INTO @ExamsessionId;

WHILE @@FETCH_STATUS = 0
BEGIN
	SELECT @StructureXML = StructurexML
		,@ResultData = resultData
		,@ResultDataFull = resultDataFull
	FROM dbo.WAREHOUSE_ExamSessionTable
	WHERE ID = @ExamsessionId;
	
	DECLARE Items_cursor CURSOR FAST_FORWARD FOR
		SELECT ItemID
			, totalMark 
		FROM #TMP_Marks_Table
		WHERE ExamSessionId = @ExamsessionId 
			AND userMark > totalMark;		

	OPEN Items_cursor;

	FETCH NEXT FROM Items_cursor INTO @ItemID, @ItemTotalMark;

	WHILE @@FETCH_STATUS = 0
	BEGIN
	
		--PRINT '@ItemID'  + @ItemID +  ' ,@ItemTotalMark: ' + Convert(Varchar, @ItemTotalMark)

		--SET @StructureXML.modify('replace value of (//item[@id=sql:variable("@ItemID")]/@userMark )[1] with sql:variable("@ItemTotalMark") ')
		--SET @ResultData.modify('replace value of (//item[@id=sql:variable("@ItemID")]/@userMark )[1] with sql:variable("@ItemTotalMark") ')		
		--SET @ResultDataFull.modify('replace value of (//item[@id=sql:variable("@ItemID")]/@userMark )[1] with sql:variable("@ItemTotalMark") ')
				
   		SET @StructureXML.modify('replace value of (//item[@id=sql:variable("@ItemID")]/@userMark)[1] with ("1")')
		SET @ResultData.modify('replace value of (//item[@id=sql:variable("@ItemID")]/@userMark)[1] with ("1")')		
		SET @ResultDataFull.modify('replace value of (//item[@id=sql:variable("@ItemID")]/@userMark)[1] with ("1")')

		FETCH NEXT FROM Items_cursor INTO @ItemID, @ItemTotalMark;
	END

	CLOSE Items_cursor;
	DEALLOCATE Items_cursor;
		
	DECLARE Sections_cursor CURSOR FAST_FORWARD FOR
		SELECT section.node.value('@id[1]','int') as sectionID
		FROM @StructureXML.nodes('//section') section(node);
	OPEN Sections_cursor;	
	FETCH NEXT FROM Sections_cursor INTO @sectionId;	
	
	WHILE @@FETCH_STATUS = 0
	BEGIN
		
		SELECT @SectionUserMarks = @ResultData.value('sum(/exam/section[@id=sql:variable("@sectionId")]/item/@userMark)', 'float');
		SELECT @SectionTotalMark = @ResultData.value('(/exam/section[@id=sql:variable("@sectionId")]/@totalMark)[1]', 'float');		
		
		
		SET @SectionUserPercentage = (@SectionUserMarks / @SectionTotalMark) * 100;
		
		--------------- ResultData		
		SET @ResultData.modify('replace value of (/exam/section[@id=sql:variable("@sectionId")]/@userMark )[1] with  sql:variable("@SectionUserMarks")');
		SET @ResultData.modify('replace value of (/exam/section[@id=sql:variable("@sectionId")]/@userPercentage )[1] with  sql:variable("@SectionUserPercentage")');
				
		--------------- ResultData Full
		SET @ResultDataFull.modify('replace value of (/assessmentDetails/assessment/section[@id=sql:variable("@sectionId")]/@userMark )[1] with  sql:variable("@SectionUserMarks")');
		SET @ResultDataFull.modify('replace value of (/assessmentDetails/assessment/section[@id=sql:variable("@sectionId")]/@userPercentage )[1] with  sql:variable("@SectionUserPercentage")');		
		FETCH NEXT FROM Sections_cursor INTO @sectionId;	
	END

	CLOSE Sections_cursor;
	DEALLOCATE Sections_cursor;
	
	SELECT @ExamTotalMark = @ResultData.value('(/exam[1]/@totalMark)[1]', 'float');
	SELECT @ExamUserMarks = @ResultData.value('sum(/exam/section/@userMark)', 'float');		
	SET @ExamUserPercentage = (@ExamUserMarks / @ExamTotalMark) * 100;

	--------------- ResultData	
	SET @ResultData.modify('replace value of (/exam/@userMark)[1] with  sql:variable("@ExamUserMarks")');
	SET @ResultData.modify('replace value of (/exam/@userPercentage )[1] with  sql:variable("@ExamUserPercentage")');
	
	--------------- ResultData Full
	SET @ResultDataFull.modify('replace value of (assessmentDetails/assessment/@userMark )[1] with  sql:variable("@ExamUserMarks")');
	SET @ResultDataFull.modify('replace value of (assessmentDetails/assessment/@userPercentage )[1] with  sql:variable("@ExamUserPercentage")');
	
	UPDATE dbo.WAREHOUSE_ExamSessionTable
	SET StructureXML = @StructureXML
		,resultData = @ResultData
		,resultDataFull = @ResultDataFULL
	WHERE ID = @ExamsessionId;

	UPDATE dbo.WAREHOUSE_ExamSessionTable_Shreded
	SET structureXml = @StructureXml
		,resultData = @ResultData
	WHERE examSessionId = @ExamsessionId;
	
	FETCH NEXT FROM Exams_cursor INTO @ExamsessionId;
END

CLOSE Exams_cursor;
DEALLOCATE Exams_cursor;

DROP TABLE #TMP_Marks_Table;

ENABLE TRIGGER [dbo].[WarehouseExamState_Trigger] ON  [dbo].[WAREHOUSE_ExamSessionTable];

PRINT 'COMPLETED!!'