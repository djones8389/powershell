use msdb

DECLARE @To NVARCHAR(100),
		@body NVARCHAR(MAX), 
		@profile_name SYSNAME,
		@query nvarchar(MAX)

/* Get email address */
	SELECT @To = email_address FROM msdb.dbo.sysoperators WHERE name = 'Dave Jones';

	SET @body = ERROR_MESSAGE();

	SELECT TOP(1) @profile_name = name FROM msdb.dbo.sysmail_profile


SELECT @query = '

if OBJECT_ID(''tempdb..#IndexResults'') IS NOT NULL DROP TABLE #IndexResults;

CREATE TABLE #IndexResults (
	[DB_Name] sysname
	, TableName nvarchar(200)
	, IndexName nvarchar(200)
	, Avg_fragmentation_in_percent decimal(15,13)
	, page_count bigint
)

INSERT #IndexResults
EXECUTE sp_MSforeachdb N''
		SET QUOTED_IDENTIFIER ON;
		USE [?];
		IF DB_ID() > 4
		BEGIN
			SELECT	db_NAME() [DB]
					,OBJECT_NAME(ix.object_id)
					,QUOTENAME(ix.name) [Index]
					, avg_fragmentation_in_percent 
					, ix_phy.page_count
				FROM	 sys.dm_db_index_physical_stats(DB_ID(''''?''''), NULL, NULL, NULL, NULL) ix_phy
				INNER JOIN sys.indexes ix
						ON ix_phy.object_id = ix.object_id
					AND ix_phy.index_id = ix.index_id
			WHERE	 avg_fragmentation_in_percent > 5
			 AND	 ix.index_id <> 0
			
		END'';


SELECT *
FROM #IndexResults
ORDER BY 1,2,3,4,5
'



EXECUTE	 msdb.dbo.sp_send_dbmail
		@profile_name = @profile_name,
		@recipients = @To,
		@subject = N'Index Test - Before Maintenance',
		@body = @body,
		@execute_query_database = N'master',
		@query = @query,
		@attach_query_result_as_file = 1,
		@query_attachment_filename = N'IndexBefore.csv',
		@query_result_separator = N',',
		@query_result_no_padding = 1,
		@body_format = 'HTML';



--SELECT * FROM msdb.dbo.sysoperators WHERE name = 'Dave Jones';


/*
SELECT @query = '

SELECT
	db_NAME() [DB]
	,OBJECT_NAME(ix.object_id)
	,QUOTENAME(ix.name) [Index]
	, avg_fragmentation_in_percent 
	, ix_phy.page_count
FROM	 sys.dm_db_index_physical_stats(DB_ID(), NULL, NULL, NULL, NULL) ix_phy
INNER JOIN sys.indexes ix
ON ix_phy.object_id = ix.object_id
	AND ix_phy.index_id = ix.index_id
WHERE  avg_fragmentation_in_percent > 5
AND	 ix.index_id <> 0
	ORDER BY 1,2,3,4,5;
'
*/