--RESTORE DATABASE [OCR_AnalyticsManagement] FROM DISK = N'T:\BACKUP\R12.5 KH DBs\OCR_AnalyticsManagement.bak' WITH FILE = 1, NOUNLOAD, NOREWIND, REPLACE, STATS = 10, MOVE 'OCR_AnalyticsManagement' TO N'S:\DATA\OCR_AnalyticsManagement.mdf', MOVE 'OCR_AnalyticsManagement_log' TO N'L:\LOGS\OCR_AnalyticsManagement.ldf'; 
--RESTORE DATABASE [OCR_ContentAuthor] FROM DISK = N'T:\BACKUP\R12.5 KH DBs\OCR_ContentAuthor.bak' WITH FILE = 1, NOUNLOAD, NOREWIND, REPLACE, STATS = 10, MOVE 'OCR_ContentAuthor' TO N'S:\DATA\OCR_ContentAuthor.mdf', MOVE 'OCR_ContentAuthor_log' TO N'L:\LOGS\OCR_ContentAuthor.ldf'; 
--RESTORE DATABASE [OCR_SurpassDataWarehouse] FROM DISK = N'T:\BACKUP\R12.5 KH DBs\OCR_SurpassDataWarehouse.bak' WITH FILE = 1, NOUNLOAD, NOREWIND, REPLACE, STATS = 10, MOVE 'OCR_SurpassDataWarehouse' TO N'S:\DATA\OCR_SurpassDataWarehouse.mdf', MOVE 'OCR_SurpassDataWarehouse_log' TO N'L:\LOGS\OCR_SurpassDataWarehouse.ldf'; 
--RESTORE DATABASE [OCR_SurpassManagement] FROM DISK = N'T:\BACKUP\R12.5 KH DBs\OCR_SurpassManagement.bak' WITH FILE = 1, NOUNLOAD, NOREWIND, REPLACE, STATS = 10, MOVE 'OCR_SurpassManagement' TO N'S:\DATA\OCR_SurpassManagement.mdf', MOVE 'OCR_SurpassManagement_log' TO N'L:\LOGS\OCR_SurpassManagement.ldf'; 


USE [Ocr_AnalyticsManagement];  --needed

CREATE USER [Ocr_AnalyticsManagementUser] FOR LOGIN [Ocr_AnalyticsManagementUser]

exec sp_addrolemember 'db_owner','Ocr_AnalyticsManagementUser'


USE [Ocr_ContentAuthor];  --needed

CREATE USER [Ocr_ContentAuthorUser] FOR LOGIN [Ocr_ContentAuthorUser]
CREATE USER [Ocr_ETLUser] FOR LOGIN [Ocr_ETLUser]

exec sp_addrolemember 'db_owner','Ocr_ContentAuthorUser'
exec sp_addrolemember 'db_datareader', 'Ocr_ETLUser'


--USE [Ocr_ItemBank]; --not needed

--CREATE USER [Ocr_ItemBankUser] FOR LOGIN [Ocr_ItemBankUser]
--CREATE USER [Ocr_ETLUser] FOR LOGIN [Ocr_ETLUser]

--exec sp_addrolemember 'db_owner','Ocr_ItemBankUser'
--exec sp_addrolemember 'ETLRole','Ocr_ETLUser'
--exec sp_addrolemember 'db_datawriter','Ocr_ETLUser'
--exec sp_addrolemember 'db_datareader','Ocr_ETLUser'

USE [Ocr_SecureAssess]; --needed

CREATE USER [Ocr_SecureAssessUser] FOR LOGIN [Ocr_SecureAssessUser]
CREATE USER [Ocr_ETLUser] FOR LOGIN [Ocr_ETLUser]

exec sp_addrolemember 'db_owner','Ocr_SecureAssessUser'
exec sp_addrolemember 'db_datareader','Ocr_ETLUser'

USE [Ocr_SurpassDataWarehouse];  --needed

CREATE USER [OCR_ETLUser] FOR LOGIN [OCR_ETLUser]

exec sp_addrolemember 'db_owner','OCR_ETLUser'

USE [Ocr_SurpassManagement]; --needed

CREATE USER [Ocr_SurpassManagementUser] FOR LOGIN [Ocr_SurpassManagementUser]
CREATE USER [Ocr_ETLUser] FOR LOGIN [Ocr_ETLUser]
CREATE USER [Ocr_SecureAsessUser] FOR LOGIN [Ocr_SecureAsessUser]

exec sp_addrolemember 'db_owner','Ocr_SurpassManagementUser'
exec sp_addrolemember 'db_owner','Ocr_SecureAsessUser'
exec sp_addrolemember 'db_datareader','Ocr_ETLUser'