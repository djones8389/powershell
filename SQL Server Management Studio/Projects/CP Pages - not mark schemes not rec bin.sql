SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

select 
	  PLT.ID as [ProjectID]
	 , PLT.[Name] as [ProjectName]
	 , pt.ID
from ProjectListTable PLT
	
cross apply ProjectStructureXml.nodes('Pro//Pag') p(r)

INNER JOIN PageTable PT
on pt.id = convert(nvarchar(10),plt.id) + 'P' +  convert(nvarchar(10),p.r.value('@ID','smallint'))

where p.r.value('@markingSchemeFor','smallint')  = '-1'
	 
EXCEPT		
		
select 
	  PLT.ID as [ProjectID]
	 , PLT.[Name] as [ProjectName]
	, pt.ID
from ProjectListTable PLT
	
cross apply ProjectStructureXml.nodes('Pro/Rec//Pag') p(r)
	
INNER JOIN PageTable PT
on pt.id = convert(nvarchar(10),plt.id) + 'P' +  convert(nvarchar(10),p.r.value('@ID','smallint'))

where p.r.value('@markingSchemeFor','smallint')  = '-1'