select count(a.examsessionid) [ExamCount]
	, Statechangedate	
from (
select examsessionid
		, cast(statechangedate as smalldatetime) statechangedate
from examstatechangeaudittable
where newstate = 9
) a
where statechangedate between '2018-05-17 00:00:00' and '2018-05-17 23:59:59'
group by statechangedate
order by 2;