use master

USE [SANDBOX_Saxion_SurpassDataWarehouse]

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS where table_Name = 'DJ_TABLE') 
	DROP TABLE [DJ_TABLE];

CREATE TABLE [DJ_TABLE] (
		CPID nvarchar(100)
		, CPVersion int
		, DerivedCorrectAnswer nvarchar(max)
		, ShortDerivedCorrectAnswer nvarchar(max)
		, updated bit
	);

DECLARE @DJ_TABLE TABLE (
		CPID nvarchar(100)
		, CPVersion int
		, DerivedCorrectAnswer nvarchar(max)
		, ShortDerivedCorrectAnswer nvarchar(max)

);

INSERT [DJ_TABLE]
SELECT 
	CPID
	, CPVersion
	, DerivedCorrectAnswer
	, ShortDerivedCorrectAnswer
	, 0
FROM DimQuestions
WHERE DerivedCorrectAnswer IS NULL
	OR ShortDerivedCorrectAnswer IS NULL;

UPDATE DQ	
	SET 
		DerivedCorrectAnswer = FullCorrect
		,ShortDerivedCorrectAnswer = ShortCorrect 
	OUTPUT
			inserted.CPID
			, inserted.CPVersion
			, inserted.DerivedCorrectAnswer
			, inserted.ShortDerivedCorrectAnswer
		INTO @DJ_TABLE
	FROM DimQuestions DQ 
	INNER JOIN [DJ_TABLE]
	on [DJ_TABLE].CPID = DQ.CPID
		and [DJ_TABLE].CPVersion = DQ.CPVersion
	JOIN (
		SELECT 
			CPID
			,DC.CPVersion
			, dbo.ag_CLR_Concat(CASE WHEN DerivedCorrectAnswer = '' THEN NULL ELSE DerivedCorrectAnswer END, ', ') FullCorrect
			, dbo.ag_CLR_Concat(CASE WHEN ShortDerivedCorrectAnswer = '' THEN NULL ELSE ShortDerivedCorrectAnswer END, ', ') ShortCorrect
		FROM DimComponents DC	
		WHERE (DerivedCorrectAnswer<>'' OR ShortDerivedCorrectAnswer<>'')
			AND PackagePriority=(SELECT PackagePriority FROM ETL.Packages WHERE PackageId = 'CBD380C5-224D-49BA-B59A-289BE55B6469')
		GROUP BY CPID,DC.CPVersion
	)
		A ON A.CPID = DQ.CPID 
		AND A.CPVersion = DQ.CPVersion

UPDATE [DJ_TABLE]
set updated = 1
FROM [DJ_TABLE] A
INNER JOIN @DJ_TABLE B
on A.cpID = b.CPID
	and a.cpversion = b.CPVersion;

