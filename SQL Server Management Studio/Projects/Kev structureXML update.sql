DECLARE @Table TABLE (
	ID INT
	,resultDataFull XML
	,structureXML XML
)

INSERT @Table(ID, resultDataFull,structureXML)
select ID
	, resultDataFull
	, structurexml
from WAREHOUSE_ExamSessionTable
WHERE ID = 127501

UPDATE @Table
set  resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item[@id="6076P6418"]/@markingType)[1] with "0"') 
	,  structureXML.modify('replace value of (/assessmentDetails/assessment/section/item[@id="6076P6418"]/@markingType)[1] with "0"')


UPDATE WAREHOUSE_ExamSessionTable
set resultDataFull = a.resultDataFull
	, StructureXML = a.StructureXML
FROM WAREHOUSE_ExamSessionTable
INNER JOIN @Table A
on A.ID = WAREHOUSE_ExamSessionTable.ID







/*
DECLARE @XML XML = (select CONVERT(xml,structurexml) structurexml
					from WAREHOUSE_ExamSessionTable
					WHERE ID = 127501
					)
					
SET @XML.modify('replace value of (/assessmentDetails/assessment/section/item/@markingType)[2] with "0"')
*/