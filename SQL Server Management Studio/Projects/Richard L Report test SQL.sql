select CONVERT(nvarchar(MAX),C.f) + CONVERT(nvarchar(MAX),C.Modifier) + CONVERT(nvarchar(MAX),C.g)

	
FROM (

select -- GradeBoundaries
	 a.b.value('@description','nvarchar(20)') f
	, case a.b.value('@modifier','char(2)') when 'lt' then '<' when 'gt' then '>' END AS [Modifier]
	, a.b.value('@value','tinyint')	  g
from AssessmentTable
CROSS APPLY GradeBoundaries.nodes('GradeBoundaries/grade') a(b)
where ExternalReference = 'CAABC/PRACTICE'
	and AssessmentName = 'AAT Conference 2010 Exemplar'

	
) C

for xml path ('root') 

--RESTORE HEADERONLY FROM DISK = 'T:\Backup\PreMigration-BritishCouncil_SecureMarker.bak'





select 
 	 (a.b.value('@description','nvarchar(20)') + ' '
	+ a.b.value('@modifier','char(2)')  + ' '
	+ a.b.value('@value','nvarchar(20)') + ' ')
from AssessmentTable
CROSS APPLY GradeBoundaries.nodes('GradeBoundaries/grade') a(b)
where ExternalReference = 'CAABC/PRACTICE'
	and AssessmentName = 'AAT Conference 2010 Exemplar'
	FOR XML PATH('')





--exec ib3_GetDetailsForAssessmentGroup_SP @userName=N'superuser',@assessmentGroupID=58



--select ID,

--	(select 
--			a.b.value('@description','nvarchar(20)') + ' '
--			+  a.b.value('@modifier','char(2)')  + ' '
--			+ a.b.value('@value','nvarchar(20)') + ' '					
--			from AssessmentTable L
--			CROSS APPLY GradeBoundaries.nodes('GradeBoundaries/grade') a(b)
--			where l.id = m.ID
--				FOR XML PATH(''), type
--				) as GradeBoundary
--From AssessmentTable M
--group by ID


	--(select gb.O FOR XML PATH('') )
	--,		
		--(
		--select 
 	--		 (a.b.value('@description','nvarchar(20)') + ' '
		--	+ a.b.value('@modifier','char(2)')  + ' '
		--	+ a.b.value('@value','nvarchar(20)') + ' ')
		--from AssessmentTable
		--CROSS APPLY GradeBoundaries.nodes('GradeBoundaries/grade') a(b)
		----where ExternalReference = 'CAABC/PRACTICE'
		----	and AssessmentName = 'AAT Conference 2010 Exemplar'
		--	FOR XML PATH('')
		--	) [Grade Boundaries]