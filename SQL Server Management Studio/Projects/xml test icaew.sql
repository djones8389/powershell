--Have data on the 8th Col (H)

--SELECT MAX(R)
--FROM (
--select  c.d.query('.') a
--	, e.f.query('.') b
--	, ROW_NUMBER() OVER (PARTITION BY cast(c.d.query('.') as nvarchar(MAX)) ORDER BY (SELECT 1)) R
--from [dbo].[xmlholder]
--cross apply myxml.nodes('p/s/c/i/t/r') c(d)
--CROSS APPLY c.d.nodes('c') e(f)
--) X
--where b.value('data(c/text)[1]','nvarchar(MAX)') IS NOT NULL

--ColWidths

SELECT SUM(ColWidth)
FROM (
SELECT   --sum(a.b.value('@w','int')) [ColWidth]
	a.b.value('@w','int') [ColWidth]
	, ROW_NUMBER() OVER(ORDER BY (SELECT 1)) [ColNo] --OVER (PARTITION BY a.b.value('@w','int') ORDER BY (SELECT 1))
FROM  [xmlholder] ESIRT  
			
CROSS APPLY ESIRT.myxml.nodes('p/s/c/i/t/r/c[@w]') a(b)
) B
where ColNo <= (
	SELECT MAX(R)
	FROM (
	select  c.d.query('.') a
		, e.f.query('.') b
		, ROW_NUMBER() OVER (PARTITION BY cast(c.d.query('.') as nvarchar(MAX)) ORDER BY (SELECT 1)) R
	from [dbo].[xmlholder]
	cross apply myxml.nodes('p/s/c/i/t/r') c(d)
	CROSS APPLY c.d.nodes('c') e(f)
	) X
	where b.value('data(c/text)[1]','nvarchar(MAX)') IS NOT NULL
)


	/*DECLARE @XML XML = '<p um="0" id="368P1041" cs="1" ua="1">
  <s um="0" id="1" ua="1">
    <c id="8" typ="4">
      <i cc="1" id="1">null</i>
    </c>
    <c id="9" typ="1">
      <i id="1" />
    </c>
    <c um="0" ie="1" wei="1" typ="20" id="10" ua="1">
      <i id="1">
        <t bd="1001" bc="0x0" bgc="16777215">
          <r h="22">
            <c w="70">
              <text>Q3.1</text>
            </c>
            <c w="386" />
            <c w="70" />
            <c w="70" />
            <c w="236" />
            <c w="140" />
            <c w="70" />
            <c w="70" />
            <c w="70" />
            <c w="70" />
            <c w="70" />
          </r>
          <r h="22">
            <c />
            <c bld="1">
              <text>Painting</text>
            </c>
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c>
              <text>Proceeds</text>
            </c>
            <c dp="0">
              <text>5000</text>
            </c>
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c>
              <text>Less%20Auctioneer%27s%20Fees%20%282%25*5000%29</text>
            </c>
            <c bd="0010" res="-100">
              <text>%3D-.02*5000</text>
            </c>
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c bd="1000" typ="3" res="4900">
              <text>%3DSUM%28C3%3AC4%29</text>
            </c>
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c>
              <text>As%20Proceeds%20Is%20less%20than%20cost%2C%20we%20can%20use%20a%20proceed%20of%20max%206000</text>
            </c>
            <c dp="0">
              <text>6000</text>
            </c>
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c>
              <text>Less%20Cost</text>
            </c>
            <c bd="0010" dp="0">
              <text>-6500</text>
            </c>
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c>
              <text>Allowable%20Loss</text>
            </c>
            <c bd="1000" typ="3" res="-500">
              <text>%3DSUM%28C6%3AC7%29</text>
            </c>
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c bld="1">
              <text>Shares%20In%20Snart%20Ltd</text>
            </c>
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c>
              <text>Chargeable%20Business%20Asset</text>
            </c>
            <c />
            <c />
            <c>
              <text>Chargeable%20Asset</text>
            </c>
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c>
              <text>Factory</text>
            </c>
            <c dp="0">
              <text>600000</text>
            </c>
            <c />
            <c>
              <text>Factory</text>
            </c>
            <c dp="0">
              <text>600000</text>
            </c>
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c>
              <text>Plant%20%26amp%3B%20Machinery%20%2840000+30000%29</text>
            </c>
            <c dp="0">
              <text>70000</text>
            </c>
            <c />
            <c>
              <text>Goodwill</text>
            </c>
            <c dp="0">
              <text>250000</text>
            </c>
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c bd="0010" />
            <c />
            <c>
              <text>Plant%20%26amp%3B%20Machinery%20%2840000+30000%29</text>
            </c>
            <c bd="0010" dp="0">
              <text>70000</text>
            </c>
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c bd="1000" typ="3" res="670000">
              <text>%3DSUM%28C12%3AC14%29</text>
            </c>
            <c />
            <c />
            <c bd="1000" typ="3" res="920000">
              <text>%3DSUM%28F12%3AF14%29</text>
            </c>
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c>
              <text>Gift%20Relief%20%28670000/920000%29*254000</text>
            </c>
            <c dp="0">
              <text>184978</text>
            </c>
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c>
              <text>Proceeds%20At%20Market%20Value</text>
            </c>
            <c dp="0">
              <text>254000</text>
            </c>
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c>
              <text>Less%20COst</text>
            </c>
            <c bd="0010" dp="0">
              <text>-35000</text>
            </c>
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c bd="1000" typ="3" res="219000">
              <text>%3DSUM%28C19%3AC20%29</text>
            </c>
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c>
              <text>Gift%20Relief</text>
            </c>
            <c bd="0010" dp="0">
              <text>-184978</text>
            </c>
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c>
              <text>Chargeable%20Gain</text>
            </c>
            <c bd="1030" typ="3" res="34022">
              <text>%3DSUM%28C21%3AC22%29</text>
            </c>
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c>
              <text>Gain%20Eligible%20For%20Entrepreneur%20Relief%20%28ER%29</text>
            </c>
            <c bd="1000" />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c>
              <text>No%20Of%20Shares</text>
            </c>
            <c>
              <text>Price</text>
            </c>
            <c>
              <text>Total%20Cost</text>
            </c>
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c bld="1">
              <text>Ramon%20Shares</text>
            </c>
            <c />
            <c />
            <c>
              <text>Purchase%20Of%20Shares</text>
            </c>
            <c dp="0">
              <text>4000</text>
            </c>
            <c dp="0">
              <text>1</text>
            </c>
            <c res="4000">
              <text>%3DF27*G27</text>
            </c>
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c>
              <text>Proceeds</text>
            </c>
            <c dp="0">
              <text>24000</text>
            </c>
            <c />
            <c>
              <text>Right%20Issue%20%281%20For%202%29%20%284000/2*1%29</text>
            </c>
            <c bd="0010" res="2000">
              <text>%3D4000/2*1</text>
            </c>
            <c dp="0">
              <text>3.2</text>
            </c>
            <c bd="0010" res="6400">
              <text>%3D2000*3.2</text>
            </c>
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c>
              <text>Less%20Cost</text>
            </c>
            <c bd="0010" dp="0">
              <text>-8667</text>
            </c>
            <c />
            <c />
            <c bd="1000" typ="3" res="6000">
              <text>%3DSUM%28F27%3AF28%29</text>
            </c>
            <c />
            <c bd="1000" typ="3" res="10400">
              <text>%3DSUM%28H27%3AH28%29</text>
            </c>
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c>
              <text>Chargeable%20Gain</text>
            </c>
            <c bd="1000" typ="3" res="15333">
              <text>%3DSUM%28C28%3AC29%29</text>
            </c>
            <c />
            <c>
              <text>Disposal%20Of%205000%20Shares</text>
            </c>
            <c dp="0">
              <text>-5000</text>
            </c>
            <c />
            <c res="8667">
              <text>%3D5000/6000*H29</text>
            </c>
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c dp="0" />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c>
              <text>ER</text>
            </c>
            <c>
              <text>Non%20ER</text>
            </c>
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c>
              <text>Painting</text>
            </c>
            <c />
            <c dp="0">
              <text>-500</text>
            </c>
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c>
              <text>Shares%20In%20Snart%20Ltd</text>
            </c>
            <c dp="0">
              <text>34022</text>
            </c>
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c>
              <text>Shares%20In%20Ramon%20PLC</text>
            </c>
            <c bd="0010" />
            <c bd="0010" dp="0">
              <text>15333</text>
            </c>
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c bd="1000" typ="3" res="34022">
              <text>%3DSUM%28C34%3AC36%29</text>
            </c>
            <c bd="1000" typ="3" res="14833">
              <text>%3DSUM%28D34%3AD36%29</text>
            </c>
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c>
              <text>Less%20AEA</text>
            </c>
            <c bd="0010" dp="0">
              <text>-11100</text>
            </c>
            <c bd="0010" />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c>
              <text>Taxable%20Gain</text>
            </c>
            <c bd="1030" typ="3" res="22922">
              <text>%3DSUM%28C37%3AC38%29</text>
            </c>
            <c bd="1030" dp="0">
              <text>14833</text>
            </c>
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c bd="1000" />
            <c bd="1000" />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c>
              <text>Amount%20Left%20In%20BRB%20%2832000-27800%29</text>
            </c>
            <c res="4200">
              <text>%3D32000-27800</text>
            </c>
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c>
              <text>4200%20@%2010%25</text>
            </c>
            <c dp="0">
              <text>420</text>
            </c>
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c>
              <text>18722%20@%2010%25</text>
            </c>
            <c dp="0">
              <text>1872</text>
            </c>
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c>
              <text>14833%20@%2020%25</text>
            </c>
            <c bd="0010" dp="0">
              <text>2967</text>
            </c>
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c>
              <text>Capital%20Gains%20Tax%20Liability</text>
            </c>
            <c bd="1030" typ="3" res="5259">
              <text>%3DSUM%28C44%3AC46%29</text>
            </c>
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c bd="1000" />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <r h="22">
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
            <c />
          </r>
          <merge />
        </t>
      </i>
    </c>
  </s>
</p>'
insert [xmlholder]
select @xml
create database  [xmlholder]
go
use [xmlholder]
go
create table [xmlholder] (myxml xml)


	select *
		, ROW_NUMBER() OVER (PARTITION BY M,N ORDER BY (SELECT cast(@XML as nvarchar(MAX)))) R
	from (
	SELECT cast(c.d.query('.') as nvarchar(MAX)) M
		,cast(e.f.query('.') as nvarchar(MAX)) N
		,e.f.value('.','nvarchar(MAX)') O
	FROM (VALUES (@XML)) a(b)
	CROSS APPLY a.b.nodes('p/s/c/i/t/r') c(d)
	CROSS APPLY c.d.nodes('c') e(f)
	) X

	--288
	--<c bld="1"><text>Ramon%20Shares</text></c>	Ramon%20Shares
	
	*/

