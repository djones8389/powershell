use master

select 'kill ' + convert(varchar(5),spid)
	,db_name(dbid)
from sys.sysprocesses s
where dbid > 4
	--and  db_name(dbid) = 'SaxionDefect_ItemBank'
order by 2;


SELECT name, collation_Name
FROM sys.Databases
where name = 'SANDBOX_PRV_DEMO_SecureAssess'

ALTER DATABASE [SANDBOX_PRV_DEMO_SecureAssess] COLLATE SQL_Latin1_General_CP1_CI_AS;

use [SANDBOX_PRV_DEMO_SecureAssess]

select 'drop table ' + quotename(NAME)
from sys.tables
where name like 'temp%'

drop table [temp__MigrationScripts]
drop table [temp_ExamSessionTable]
drop table [temp_ReMarkExamSessionTable]
drop table [temp_WAREHOUSE_ExamSessionTable_Shreded]

select top 1 * 
into [ExamSessionTable2]
from [ExamSessionTable]




CREATE TABLE [dbo].[temp_ExamSessionTable](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ScheduledExamID] [int] NOT NULL,
	[UserID] [int] NOT NULL,
	[StructureXML] [xml](CONTENT [dbo].[structureXmlXsd]) NULL,
	[MarkerData] [xml] NULL,
	[KeyCode] [varchar](12)  NOT NULL,
	[examState] [int] NOT NULL,
	[pinNumber] [nvarchar](15)  NULL,
	[resultData] [xml] NULL,
	[previousExamState] [int] NULL,
	[resultDataFull] [xml] NULL,
	[IsOffline] [bit] NULL,
	[currentSection] [int] NULL,
	[LastInteractionDate] [datetime] NULL,
	[toolsXML] [xml] NULL,
	[examStateInformation] [xml] NULL,
	[ScheduledDurationXml] [xml] NULL,
	[isPrintable] [varchar](1)  NULL,
	[ScheduledDuration] [int] NULL,
	[srDefault] [int] NULL,
	[srMaximum] [int] NULL,
	[defaultDuration] [int] NULL,
	[advanceDownload] [bit] NULL,
	[downloadInformation] [xml] NULL,
	[clientInformation] [xml] NULL,
	[ExportToSecureMarker] [bit] NOT NULL,
	[AllowPackageDelivery] [bit] NOT NULL,
	[ContainsBTLOffice] [bit] NULL,
	[LocalState] [int] NULL,
	[AwaitingUploadStateChangingTime] [datetime] NULL,
	[attemptAutoSubmitForAwaitingUpload] [bit] NULL,
	[AwaitingUploadGracePeriodInDays] [int] NOT NULL,
	[automaticCreatePin] [bit] NULL,
	[submissionExported] [bit] NOT NULL,
	[grade]  AS (case when [previousExamState]=(10) then 'Voided' when NOT isnull([dbo].[fn_getValueFromResultData]([resultData],'GRADE'),'')='' then [dbo].[fn_getValueFromResultData]([resultData],'GRADE') when CONVERT([int],[dbo].[fn_getValueFromResultData]([resultData],'PASSVALUE'),(0))=(1) then 'Pass' when CONVERT([int],[dbo].[fn_getValueFromResultData]([resultData],'PASSVALUE'),(0))=(0) AND CONVERT([int],[dbo].[fn_getValueFromResultData]([resultData],'CLOSEVALUE'),(0))=(1) then 'Close' else 'Fail' end) COLLATE SQL_Latin1_General_CP1_CI_AS PERSISTED,
	[userMark]  AS (CONVERT([float],[dbo].[fn_getValueFromResultData]([resultData],'USERMARK'),(0))) COLLATE SQL_Latin1_General_CP1_CI_AS PERSISTED,
	[userPercentage]  AS (CONVERT([float],[dbo].[fn_getValueFromResultData]([resultData],'USERPERCENTAGE'),(0))) COLLATE SQL_Latin1_General_CP1_CI_AS PERSISTED,
	[originalGrade]  AS (case when NOT isnull([dbo].[fn_getValueFromResultData]([resultData],'ORIGINALGRADE'),[dbo].[fn_getValueFromResultData]([resultData],'GRADE'))=[dbo].[fn_getValueFromResultData]([resultData],'GRADE') then [dbo].[fn_getValueFromResultData]([resultData],'ORIGINALGRADE') else case when [previousExamState]=(10) then 'Voided' when NOT isnull([dbo].[fn_getValueFromResultData]([resultData],'GRADE'),'')='' then [dbo].[fn_getValueFromResultData]([resultData],'GRADE') when CONVERT([int],[dbo].[fn_getValueFromResultData]([resultData],'PASSVALUE'),(0))=(1) then 'Pass' when CONVERT([int],[dbo].[fn_getValueFromResultData]([resultData],'PASSVALUE'),(0))=(0) AND CONVERT([int],[dbo].[fn_getValueFromResultData]([resultData],'CLOSEVALUE'),(0))=(1) then 'Close' else 'Fail' end end) COLLATE SQL_Latin1_General_CP1_CI_AS PERSISTED,
	[adjustedGrade]  AS (case when NOT isnull([dbo].[fn_getValueFromResultData]([resultData],'ORIGINALGRADE'),[dbo].[fn_getValueFromResultData]([resultData],'GRADE'))=[dbo].[fn_getValueFromResultData]([resultData],'GRADE') then case when NOT isnull([dbo].[fn_getValueFromResultData]([resultData],'GRADE'),'')='' then [dbo].[fn_getValueFromResultData]([resultData],'GRADE') when CONVERT([int],[dbo].[fn_getValueFromResultData]([resultData],'PASSVALUE'),(0))=(1) then 'Pass' when CONVERT([int],[dbo].[fn_getValueFromResultData]([resultData],'PASSVALUE'),(0))=(0) AND CONVERT([int],[dbo].[fn_getValueFromResultData]([resultData],'CLOSEVALUE'),(0))=(1) then 'Close' else 'Fail' end else '' end) COLLATE SQL_Latin1_General_CP1_CI_AS PERSISTED,
	[TargetedForVoid] [xml] NULL,
	[MarkingAutoVoidPeriod] [int] NULL,
	[AutoVoidDate] [datetime] NULL,
	[EnableOverrideMarking] [bit] NOT NULL,
	[ComputedTotalDuration]  AS ([dbo].[fn_ComputeTotalDuration]([StructureXML])) COLLATE SQL_Latin1_General_CP1_CI_AS PERSISTED,
	[HumanMarkingComplete] [bit] NOT NULL,
	[itemMarksUploaded] [bit] NOT NULL,
	[LocalScanState] [int] NULL,
	[InvalidRulesCount] [int] NOT NULL,
	[DeliveryMechanism] [int] NOT NULL,
	[TakenThroughLocalScan] [bit] NOT NULL,
	[LocalScanUploadDate] [datetime] NULL,
	[LocalScanDownloadDate] [datetime] NULL,
	[LocalScanNumPages] [int] NULL,
	[CertifiedForTablet] [bit] NOT NULL,
	[IsProjectBased] [bit] NOT NULL,
	[ExpiryDate] [datetime] NULL,
	[UserAssociationEnabled] [bit] NOT NULL,
	[UserAssociationMarkerAssignable] [bit] NOT NULL,
	[UserAssociationMarkerRequired] [bit] NOT NULL,
	[UserAssociationModeratorAssignable] [bit] NOT NULL,
	[UserAssociationModeratorRequired] [bit] NOT NULL,
	[UserAssociationRestrictUsers] [bit] NOT NULL,
	[PreCachingDefault] [bit] NOT NULL,
	[PreCachingAvailable] [bit] NOT NULL,
	[ValidationCount] [int] NULL,
	[SyncStructureXmlCompleted] [bit] NOT NULL,
	[ItemBankStyleProfileID] [int] NULL,
	[ItemBankStyleProfileVersion] [int] NULL,
	[SecureAssessStyleProfileID] [int] NULL,
	[sendToWarehouseAfterDelivery] [bit] NOT NULL,
	[EnableLogging] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
