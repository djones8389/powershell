use master

IF OBJECT_ID('tempdb..##ClientTableMetrics') IS NOT NULL DROP TABLE ##ClientTableMetrics;

CREATE TABLE ##ClientTableMetrics (
	
	ID uniqueIdentifier
	,server_name nvarchar(100)
	, database_name nvarchar(100)
	, database_id int
	, table_name nvarchar(100)
	, rows int
	, reserved_kb nvarchar(20)
	, data_kb nvarchar(20)
	, index_size nvarchar(20)
	, unused_kb nvarchar(20)
);

exec sp_MSforeachdb '

USE [?];

if (''?'' in (select name from sys.databases where state_desc = ''ONLINE'' and database_id > 4 and name like ''ICAEW%''))

BEGIN

	declare @dynamic nvarchar(MAX) = '''';

	select @dynamic +=CHAR(13) +
		''exec sp_spaceused @objname = ''''''+s.name+''.''+t.name + '''''' ''
	from sys.tables T
	inner join sys.schemas S
	on s.schema_id = t.schema_id
	where type = ''u''
	order by t.name;

	INSERT ##ClientTableMetrics (table_name, [rows], reserved_kb, data_kb, index_size, unused_kb)
	EXEC(@dynamic);

	UPDATE ##ClientTableMetrics
	SET database_id = db_id()
	where database_id IS NULL;

	UPDATE ##ClientTableMetrics
	SET server_name = @@SERVERNAME
		, database_name = DB_Name()
	where db_id() = database_id;
END
'

SELECT
  database_name [Database]
	, table_name [Table]
	, rows [No Of Records]
    ,cast(replace([data_kb], 'KB','') as float) [Data (KB)]
	,cast(replace([data_kb], 'KB','') as float)/1024 [Data (MB)]
FROM ##ClientTableMetrics
where database_name in ('Icaewmock_ItemBank','ICAEW_ItemBank')
	and table_name = 'ItemTable'
order by 1,2


use Icaewmock_ItemBank
select ProjectID
	, ItemRef
	, [Version]
	, DATALENGTH(ItemFile) [Size (bytes)]
	, cast(DATALENGTH(ItemFile) as float)/1024 [Size (KB)]
	, cast(DATALENGTH(ItemFile) as float)/1024/1024 [Size (MB)]
from [dbo].[ItemTable] NOLOCK
order by 1,2,3