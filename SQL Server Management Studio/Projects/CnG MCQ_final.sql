SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
SET NOCOUNT ON;
 
IF OBJECT_ID('tempdb..#Questions') IS NOT NULL DROP TABLE #Questions;
IF OBJECT_ID('tempdb..#Answers') IS NOT NULL DROP TABLE #Answers;
 
select 
       substring(Q.ID, 0, charindex('S', Q.ID)+2) UpToScene
       , SUBSTRING(Q.ID,charindex('C',Q.ID)+1, (Len(Q.ID)-charindex('I',Q.ID)))  C
       , cast(Q.Question.query('ItemValue/TEXTFORMAT/P//FONT/.//text()') as nvarchar(MAX)) Question 
INTO #Questions
from (
            select ITT.ID 
                    , CAST(ItemValue AS xml)  Question
            from [ItemTextBoxTable] ITT WITH (READUNCOMMITTED)
) Q
 
CREATE NONCLUSTERED INDEX [IX_NC] on #QUESTIONS (UpToScene,C) INCLUDE(Question);
 
SELECT  ROW_NUMBER() OVER(PARTITION BY ParentID ORDER BY ID, [asC]) AS [N],
       ParentID,
       [asC] AS [AnswerAlias], 
       CAST(ItemXml.query('.//text()') AS nvarchar(max)) AS [AnswerText]
INTO #Answers
FROM (
SELECT ID,
         ParentID,
         [asC],
         CAST(ItemValue AS xml) AS [ItemXml]
FROM dbo.ItemMultipleChoiceTable WITH (READUNCOMMITTED)
) AS [A];
 
CREATE NONCLUSTERED INDEX [IX] ON [dbo].[#Answers] ([ParentID]) INCLUDE ([N],[AnswerAlias],[AnswerText]);
 
 
declare @n table (
       n float 
);
 
INSERT @n
SELECT  DISTINCT N
FROM  #Answers
order by N;
 
DECLARE @OptionList nvarchar(500) = (
 
       SELECT 
              distinct
              substring((
                     SELECT ', ' + QUOTENAME(cast([N] as varchar(10)))
                     FROM @n 
                     order by [N]
                     FOR XML PATH('')
              ), 2,1000)
       FROM @n 
);
 
DECLARE @ISNULL Nvarchar(500) = (SELECT REPLACE(REPLACE(@OptionList, '[','ISNULL(['), ']','],'''') [Answer Option]'))
 
DECLARE @DynamicString nvarchar(MAX) = '';
SELECT @DynamicString += '
 
SELECT  
       B.parentID
       ,(
              SELECT Question + ''  ''
              from #QUESTIONS Q2
              where q2.UpToScene = b.UpToScene
                     and (cast(b.c as int) - cast(q2.c as int)) in (1,2)
              for xml path ('''')
       ) Question
       ,'+@ISNULL+'
FROM (
              SELECT p.ParentID
                     , substring(ParentID, 0, charindex(''S'',ParentID)+2) UpToScene
                     , SUBSTRING(ParentID,charindex(''C'',ParentID)+1, (Len(ParentID)-charindex(''I'',ParentID))) C
              ,'+@OptionList+'
                     
              FROM (
                     SELECT ParentID, N, AnswerAlias + N'': '' + AnswerText AS [AnswerText]
                     FROM #Answers
              ) AS [S]
              PIVOT (
                     MAX(AnswerText)
                     FOR N IN ('+@OptionList+')
              ) AS [P]
)  B
'
EXEC(@DynamicString);
