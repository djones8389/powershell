declare @XML xml = '
<assessmentDetails>
  <assessmentID>5461</assessmentID>
  <qualificationID>644</qualificationID>
  <qualificationName>Functional Skills English Writing L1</qualificationName>
  <qualificationReference>AST3267</qualificationReference>
  <assessmentGroupName>Functional Skill English Writing L1 Paper Based</assessmentGroupName>
  <assessmentGroupID>679</assessmentGroupID>
  <assessmentGroupReference>196</assessmentGroupReference>
  <assessmentName>FSW149</assessmentName>
  <validFromDate>06 Jan 2015</validFromDate>
  <expiryDate>07 Jan 2025</expiryDate>
  <startTime>00:01:00</startTime>
  <endTime>23:59:00</endTime>
  <duration>60</duration>
  <defaultDuration>60</defaultDuration>
  <scheduledDuration>
    <value>60</value>
    <reason />
  </scheduledDuration>
  <sRBonus>0</sRBonus>
  <sRBonusMaximum>15</sRBonusMaximum>
  <externalReference>FSW149-59-PB
</externalReference>
  <passLevelValue>36</passLevelValue>
  <passLevelType>0</passLevelType>
  <status>3</status>
  <testFeedbackType>
    <passFail>0</passFail>
    <percentageMark>0</percentageMark>
    <allowSummaryFeedback>0</allowSummaryFeedback>
    <summaryFeedbackType>1</summaryFeedbackType>
    <itemSummary>0</itemSummary>
    <itemReview>0</itemReview>
    <itemReviewCorrectAnswer>0</itemReviewCorrectAnswer>
    <itemFeedback>0</itemFeedback>
    <printableSummary>0</printableSummary>
    <candidateDetails>0</candidateDetails>
    <feedbackByReference>0</feedbackByReference>
    <allowFeedbackDuringExam>0</allowFeedbackDuringExam>
    <allowFeedbackDuringSummary>0</allowFeedbackDuringSummary>
  </testFeedbackType>
  <itemFeedback>0</itemFeedback>
  <allowCalculator>0</allowCalculator>
  <lastInUseDate>19 Jan 2017 14:27:04</lastInUseDate>
  <completedScriptReview>0</completedScriptReview>
  <assessment currentSection="1" totalTime="0" currentTime="0" originalPassMark="36" originalPassType="0" passMark="36" passType="0" totalMark="60" userMark="52.000" userPercentage="86.667" passValue="1" originalGrade="Pass">
    <intro id="0" name="Introduction" currentItem="" />
    <outro id="0" name="Finish" currentItem="" />
    <tools id="0" name="Tools" currentItem="" />
    <section id="1" name="Task 1" passLevelValue="0" passLevelType="1" itemsToMark="0" currentItem="4156P4225" fixed="1" totalMark="60" userMark="52.000" userPercentage="86.667" passValue="1">
      <item id="4156P4225" name="Paper Based Exam" totalMark="60" version="5" markingType="0" markingState="0" userMark="0.86666666" markerUserMark="" userAttempted="1" viewingTime="5" flagged="0" type="1" quT="10">
        <p id="4156P4225" um="0.866666666666667" cs="1" ua="1" uploaded="1">
          <!--This response has been generated from a CSV mark import-->
        </p>
      </item>
    </section>
  </assessment>
  <isValid>1</isValid>
  <isPrintable>0</isPrintable>
  <qualityReview>0</qualityReview>
  <numberOfGenerations>1</numberOfGenerations>
  <requiresInvigilation>1</requiresInvigilation>
  <advanceContentDownloadTimespan>120</advanceContentDownloadTimespan>
  <automaticVerification>0</automaticVerification>
  <offlineMode>2</offlineMode>
  <requiresValidation>0</requiresValidation>
  <requiresSecureClient>1</requiresSecureClient>
  <examType>0</examType>
  <advanceDownload>0</advanceDownload>
  <gradeBoundaryData>
    <gradeBoundaries passLevelType="0" adjustedGradeBoundaries="0">
      <grade modifier="lt" value="36" description="Fail" />
      <grade modifier="gt" value="36" description="Pass" />
    </gradeBoundaries>
  </gradeBoundaryData>
  <autoViewExam>0</autoViewExam>
  <autoProgressItems>1</autoProgressItems>
  <confirmationText>
    <confirmationText />
  </confirmationText>
  <requiresConfirmationCheck>0</requiresConfirmationCheck>
  <allowCloseWithoutSubmit>0</allowCloseWithoutSubmit>
  <useSecureMarker>0</useSecureMarker>
  <annotationVersion>0</annotationVersion>
  <allowPackagingDelivery>1</allowPackagingDelivery>
  <scoreBoundaryData>
    <scoreBoundaries showScoreBoundaryColumn="0">
      <score modifier="lt" value="40" description="Not met" higherBoundarySet="1" />
      <score modifier="lt" value="40" description="Not met" higherBoundarySet="0" />
      <score modifier="gt" value="40" description="Close to met" higherBoundarySet="1" />
      <score modifier="gt" value="40" description="Met" higherBoundarySet="0" />
      <score modifier="gt" value="45" description="Met" higherBoundarySet="1" />
      <score modifier="gt" value="80" description="Exceeded" higherBoundarySet="1" />
    </scoreBoundaries>
  </scoreBoundaryData>
  <showCandidateReportResultColumn>0</showCandidateReportResultColumn>
  <showCandidateReportPercentageColumn>1</showCandidateReportPercentageColumn>
  <certifiedAccessible>0</certifiedAccessible>
  <markingProgressVisible>1</markingProgressVisible>
  <markingProgressMode>1</markingProgressMode>
  <secureClientOperationMode>1</secureClientOperationMode>
  <examDeliverySystemStyleType>delivery_horizontal</examDeliverySystemStyleType>
  <markingAutoVoidPeriod>60</markingAutoVoidPeriod>
  <showPageRequiresScrollingAlert>1</showPageRequiresScrollingAlert>
  <project ID="869">
    <value display="1.1 Write clearly and coherently, including an appropriate level of detail.">1</value>
    <value display="1.2 Present information in a logical sequence.">2</value>
    <value display="1.3 Use language, format and structure suitable for purpose and audience.">3</value>
    <value display="1.4 Use correct grammar, including correct and consistent use of tense.">4</value>
    <value display="1.5 Ensure written work includes generally accurate spelling and that meaning is clear.">5</value>
    <value display="1.6 Ensure written work includes generally accurate punctuation and that meaning is clear.">6</value>
    <value display="L1. Identify the main points and ideas and how they are presented in a variety of texts.">7</value>
	</project>
</assessmentDetails>'



if OBJECT_ID('tempdb..#AsciiCheck') IS NOT NULL DROP TABLE #AsciiCheck;

CREATE TABLE #AsciiCheck (
	
	[Character] char(1)
	, [ASCII Val] tinyint
)


declare @mystring nvarchar(MAX) = CAST(@XML as nvarchar(MAX))
declare @min int = 1
declare @max int = (select LEN(@mystring)+1)

while (@min < @max)

BEGIN

	INSERT #AsciiCheck
	select substring(@mystring, @min,1), ASCII((SELECT substring(@mystring, @min,1)))

	select @min = @min + 1

END

select * from #AsciiCheck
where [ASCII Val] < 32;


DELETE #AsciiCheck
where [ASCII Val] < 32;


Declare @newString NVARCHAR(max) =  (

select CAST 
	(
		(
			select CHAR([ASCII Val])
			from #AsciiCheck
			for xml path(''), type
		)  as nvarchar(MAX)
	)
)

select cast(replace(replace(@newString, '&lt;','<'), '&gt;','>') as xml)

declare @XML xml = '
<assessmentDetails>
  <assessmentID>5461</assessmentID>
  <qualificationID>644</qualificationID>
  <qualificationName>Functional Skills English Writing L1</qualificationName>
  <qualificationReference>AST3267</qualificationReference>
  <assessmentGroupName>Functional Skill English Writing L1 Paper Based</assessmentGroupName>
  <assessmentGroupID>679</assessmentGroupID>
  <assessmentGroupReference>196</assessmentGroupReference>
  <assessmentName>FSW149</assessmentName>
  <validFromDate>06 Jan 2015</validFromDate>
  <expiryDate>07 Jan 2025</expiryDate>
  <startTime>00:01:00</startTime>
  <endTime>23:59:00</endTime>
  <duration>60</duration>
  <defaultDuration>60</defaultDuration>
  <scheduledDuration>
    <value>60</value>
    <reason />
  </scheduledDuration>
  <sRBonus>0</sRBonus>
  <sRBonusMaximum>15</sRBonusMaximum>
  <externalReference>FSW149-59-PB
</externalReference>
  <passLevelValue>36</passLevelValue>
  <passLevelType>0</passLevelType>
  <status>3</status>
  <testFeedbackType>
    <passFail>0</passFail>
    <percentageMark>0</percentageMark>
    <allowSummaryFeedback>0</allowSummaryFeedback>
    <summaryFeedbackType>1</summaryFeedbackType>
    <itemSummary>0</itemSummary>
    <itemReview>0</itemReview>
    <itemReviewCorrectAnswer>0</itemReviewCorrectAnswer>
    <itemFeedback>0</itemFeedback>
    <printableSummary>0</printableSummary>
    <candidateDetails>0</candidateDetails>
    <feedbackByReference>0</feedbackByReference>
    <allowFeedbackDuringExam>0</allowFeedbackDuringExam>
    <allowFeedbackDuringSummary>0</allowFeedbackDuringSummary>
  </testFeedbackType>
  <itemFeedback>0</itemFeedback>
  <allowCalculator>0</allowCalculator>
  <lastInUseDate>19 Jan 2017 14:27:04</lastInUseDate>
  <completedScriptReview>0</completedScriptReview>
  <assessment currentSection="1" totalTime="0" currentTime="0" originalPassMark="36" originalPassType="0" passMark="36" passType="0" totalMark="60" userMark="52.000" userPercentage="86.667" passValue="1" originalGrade="Pass">
    <intro id="0" name="Introduction" currentItem="" />
    <outro id="0" name="Finish" currentItem="" />
    <tools id="0" name="Tools" currentItem="" />
    <section id="1" name="Task 1" passLevelValue="0" passLevelType="1" itemsToMark="0" currentItem="4156P4225" fixed="1" totalMark="60" userMark="52.000" userPercentage="86.667" passValue="1">
      <item id="4156P4225" name="Paper Based Exam" totalMark="60" version="5" markingType="0" markingState="0" userMark="0.86666666" markerUserMark="" userAttempted="1" viewingTime="5" flagged="0" type="1" quT="10">
        <p id="4156P4225" um="0.866666666666667" cs="1" ua="1" uploaded="1">
          <!--This response has been generated from a CSV mark import-->
        </p>
      </item>
    </section>
  </assessment>
  <isValid>1</isValid>
  <isPrintable>0</isPrintable>
  <qualityReview>0</qualityReview>
  <numberOfGenerations>1</numberOfGenerations>
  <requiresInvigilation>1</requiresInvigilation>
  <advanceContentDownloadTimespan>120</advanceContentDownloadTimespan>
  <automaticVerification>0</automaticVerification>
  <offlineMode>2</offlineMode>
  <requiresValidation>0</requiresValidation>
  <requiresSecureClient>1</requiresSecureClient>
  <examType>0</examType>
  <advanceDownload>0</advanceDownload>
  <gradeBoundaryData>
    <gradeBoundaries passLevelType="0" adjustedGradeBoundaries="0">
      <grade modifier="lt" value="36" description="Fail" />
      <grade modifier="gt" value="36" description="Pass" />
    </gradeBoundaries>
  </gradeBoundaryData>
  <autoViewExam>0</autoViewExam>
  <autoProgressItems>1</autoProgressItems>
  <confirmationText>
    <confirmationText />
  </confirmationText>
  <requiresConfirmationCheck>0</requiresConfirmationCheck>
  <allowCloseWithoutSubmit>0</allowCloseWithoutSubmit>
  <useSecureMarker>0</useSecureMarker>
  <annotationVersion>0</annotationVersion>
  <allowPackagingDelivery>1</allowPackagingDelivery>
  <scoreBoundaryData>
    <scoreBoundaries showScoreBoundaryColumn="0">
      <score modifier="lt" value="40" description="Not met" higherBoundarySet="1" />
      <score modifier="lt" value="40" description="Not met" higherBoundarySet="0" />
      <score modifier="gt" value="40" description="Close to met" higherBoundarySet="1" />
      <score modifier="gt" value="40" description="Met" higherBoundarySet="0" />
      <score modifier="gt" value="45" description="Met" higherBoundarySet="1" />
      <score modifier="gt" value="80" description="Exceeded" higherBoundarySet="1" />
    </scoreBoundaries>
  </scoreBoundaryData>
  <showCandidateReportResultColumn>0</showCandidateReportResultColumn>
  <showCandidateReportPercentageColumn>1</showCandidateReportPercentageColumn>
  <certifiedAccessible>0</certifiedAccessible>
  <markingProgressVisible>1</markingProgressVisible>
  <markingProgressMode>1</markingProgressMode>
  <secureClientOperationMode>1</secureClientOperationMode>
  <examDeliverySystemStyleType>delivery_horizontal</examDeliverySystemStyleType>
  <markingAutoVoidPeriod>60</markingAutoVoidPeriod>
  <showPageRequiresScrollingAlert>1</showPageRequiresScrollingAlert>
  <project ID="869">
    <value display="1.1 Write clearly and coherently, including an appropriate level of detail.">1</value>
    <value display="1.2 Present information in a logical sequence.">2</value>
    <value display="1.3 Use language, format and structure suitable for purpose and audience.">3</value>
    <value display="1.4 Use correct grammar, including correct and consistent use of tense.">4</value>
    <value display="1.5 Ensure written work includes generally accurate spelling and that meaning is clear.">5</value>
    <value display="1.6 Ensure written work includes generally accurate punctuation and that meaning is clear.">6</value>
    <value display="L1. Identify the main points and ideas and how they are presented in a variety of texts.">7</value>
	</project>
</assessmentDetails>'



if OBJECT_ID('tempdb..#AsciiCheck') IS NOT NULL DROP TABLE #AsciiCheck;

CREATE TABLE #AsciiCheck (
	
	[Character] char(1)
	, [ASCII Val] tinyint
)


declare @mystring nvarchar(MAX) = CAST(@XML as nvarchar(MAX))
declare @min int = 1
declare @max int = (select LEN(@mystring)+1)

while (@min < @max)

BEGIN

	INSERT #AsciiCheck
	select substring(@mystring, @min,1), ASCII((SELECT substring(@mystring, @min,1)))

	select @min = @min + 1

END

select * from #AsciiCheck
where [ASCII Val] < 32;


DELETE #AsciiCheck
where [ASCII Val] < 32;


Declare @newString NVARCHAR(max) =  (

select CAST 
	(
		(
			select CHAR([ASCII Val])
			from #AsciiCheck
			for xml path(''), type
		)  as nvarchar(MAX)
	)
)

select cast(replace(replace(@newString, '&lt;','<'), '&gt;','>') as xml)