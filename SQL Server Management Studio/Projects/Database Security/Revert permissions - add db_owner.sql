--Put this into a SQL job pointing at one of the DB's

use Aatpreview_AnalyticsManagement

DECLARE @UserLogins TABLE (DBName nvarchar(150), userName nvarchar(150), loginName nvarchar(150), RoleName nvarchar(50))
DECLARE @@ClientName nvarchar(20) = (select SUBSTRING(db_name(),0,charindex('_',db_name())))
DECLARE @Dynamic nvarchar(MAX)=''

DECLARE @command1 nvarchar(1000) = ' 

USE [?];

if(db_name() like ''%'+@@ClientName+'%'')

select ''?''  as DBName
		, u.name as ''User''
		, (select SUSER_SNAME (u.sid)) as ''Login''
		, r.name  as RoleName
from sys.database_role_members RM 
	right join sys.database_principals U on U.principal_id = RM.member_principal_id
	left join sys.database_principals R on R.principal_id = RM.role_principal_id
where u.type<>''R''
	and U.type = ''S''
	and r.name IS NOT NULL
	and ''?'' not in (''msdb'',''tempdb'',''model'',''master'')
'
INSERT @UserLogins
EXEC sp_MSforeachdb  @command1


SELECT @Dynamic += CHAR(13) +
   'USE ' + QUOTENAME(DBNAME) + ';'	
	+ ' exec sp_addrolemember ''' + rolename + ''', '''  +userName+ ''';' --sp_droprolemember
FROM @UserLogins
where DBName like '%' + @@ClientName + '%'
	and username != 'dbo'
	and RoleName = 'db_owner'

PRINT(@Dynamic)
