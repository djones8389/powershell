--Set new, secure permissions

	 IF EXISTS (select 1 from sys.databases where name = 'Windesheim_AnalyticsManagement') use [Windesheim_AnalyticsManagement]       BEGIN       exec sp_addrolemember 'db_datareader','Windesheim_AnalyticsManagementUser'       GRANT EXECUTE ON SCHEMA :: dbo TO [Windesheim_AnalyticsManagementUser]       END       
	 IF EXISTS (select 1 from sys.databases where name = 'Windesheim_ContentAuthor') use [Windesheim_ContentAuthor]       BEGIN       exec sp_addrolemember 'db_datareader','Windesheim_ContentAuthorUser'       GRANT EXECUTE ON SCHEMA :: dbo TO [Windesheim_ContentAuthorUser]           END       
	 IF EXISTS (select 1 from sys.databases where name = 'Windesheim_ContentAuthor') use [Windesheim_ContentAuthor]       BEGIN        exec sp_addrolemember 'db_datareader','Windesheim_ETLUser'         END    
	 IF EXISTS (select 1 from sys.databases where name = 'Windesheim_ItemBank') use [Windesheim_ItemBank]       BEGIN       exec sp_addrolemember 'db_datareader','Windesheim_ItemBankUser'       GRANT EXECUTE ON SCHEMA :: dbo TO [Windesheim_ItemBankUser]           END       
	 IF EXISTS (select 1 from sys.databases where name = 'Windesheim_ItemBank') use [Windesheim_ItemBank]       BEGIN        exec sp_addrolemember 'db_datareader','Windesheim_ETLUser'         END    
	 IF EXISTS (select 1 from sys.databases where name = 'Windesheim_SecureAssess') use [Windesheim_SecureAssess]       BEGIN               GRANT EXECUTE ON SCHEMA :: dbo TO [Windesheim_SecureAssessUser]      GRANT VIEW DEFINITION ON XML SCHEMA COLLECTION::[dbo].[itemResponseXmlXsd] TO [Windesheim_SecureAssessUser]      GRANT VIEW DEFINITION ON XML SCHEMA COLLECTION::[dbo].[structureXmlXsd] TO [Windesheim_SecureAssessUser]         END       
	 IF EXISTS (select 1 from sys.databases where name = 'Windesheim_SecureAssess') use [Windesheim_SecureAssess]       BEGIN        exec sp_addrolemember 'db_datareader','Windesheim_ETLUser'         END    
	 IF EXISTS (select 1 from sys.databases where name = 'Windesheim_SurpassDataWarehouse') use [Windesheim_SurpassDataWarehouse]       BEGIN        exec sp_addrolemember 'db_datareader','Windesheim_ETLUser'         END    
	 IF EXISTS (select 1 from sys.databases where name = 'Windesheim_SurpassDataWarehouse') use [Windesheim_SurpassDataWarehouse]       BEGIN        GRANT EXECUTE ON SCHEMA :: ETL TO Windesheim_ETLUser;        exec sp_addrolemember 'db_datawriter','Windesheim_ETLUser'         END       
	 IF EXISTS (select 1 from sys.databases where name = 'Windesheim_SurpassManagement') use [Windesheim_SurpassManagement]       BEGIN       exec sp_addrolemember 'db_datawriter','Windesheim_SurpassManagementUser'       END       
	 IF EXISTS (select 1 from sys.databases where name = 'Windesheim_SurpassManagement') use [Windesheim_SurpassManagement]       BEGIN        exec sp_addrolemember 'db_datareader','Windesheim_ETLUser'         END    

--Check they are in place

	DECLARE @command1 nvarchar(1000) = '

	USE [?];

	select ''?''  as DBName
			, u.name as ''User''
			, (select SUSER_SNAME (u.sid)) as ''Login''
			, r.name  as RoleName
			--, u.*
	from sys.database_role_members RM 
		right join sys.database_principals U on U.principal_id = RM.member_principal_id
		left join sys.database_principals R on R.principal_id = RM.role_principal_id
	where u.type<>''R''
		and U.type = ''S''
		and r.name IS NOT NULL
		and ''?'' not in (''msdb'',''tempdb'',''model'',''master'')
	'


	DECLARE @UserLogins TABLE (DBName nvarchar(150), userName nvarchar(150), loginName nvarchar(150), RoleName nvarchar(50))
	INSERT @UserLogins
	EXEC sp_MSforeachdb  @command1

	select a.*
	FROM (
	SELECT 
		 'USE ' + QUOTENAME(DBNAME) + ';'
		+ ' IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = '''+userName+''') CREATE USER ' + QUOTENAME(userName) + 'FOR LOGIN ' + QUOTENAME(loginName) + ';'
		+ ' exec sp_addrolemember''' + rolename + ''', '''  +userName+ ''' ' [create]
		, DBName
		, rolename
		, userName
	FROM @UserLogins
	where DBName like 'Windesheim%'
		and username != 'dbo'
	) a
	where a.[create] is not null;


--Drop db_owner
 
	USE [Windesheim_AnalyticsManagement]; exec sp_droprolemember 'db_owner', 'Windesheim_AnalyticsManagementUser';
	USE [Windesheim_ContentAuthor]; exec sp_droprolemember 'db_owner', 'Windesheim_ContentAuthorUser';
	USE [Windesheim_ItemBank]; exec sp_droprolemember 'db_owner', 'Windesheim_ItemBankUser';
	USE [Windesheim_SecureAssess]; exec sp_droprolemember 'db_owner', 'Windesheim_SecureAssessUser';
	USE [Windesheim_SurpassManagement]; exec sp_droprolemember 'db_owner', 'Windesheim_SurpassManagementUser';
	USE [Windesheim_SurpassDataWarehouse]; exec sp_droprolemember 'db_owner', 'Windesheim_ETLUser';


--Revert if you encounter errors

	USE [Windesheim_AnalyticsManagement]; exec sp_addrolemember 'db_owner', 'Windesheim_AnalyticsManagementUser';	USE [Windesheim_ContentAuthor]; exec sp_addrolemember 'db_owner', 'Windesheim_ContentAuthorUser';
	USE [Windesheim_ItemBank]; exec sp_addrolemember 'db_owner', 'Windesheim_ItemBankUser';	USE [Windesheim_SecureAssess]; exec sp_addrolemember 'db_owner', 'Windesheim_SecureAssessUser';	USE [Windesheim_SurpassManagement]; exec sp_addrolemember 'db_owner', 'Windesheim_SurpassManagementUser';	USE [Windesheim_SurpassDataWarehouse]; exec sp_addrolemember 'db_owner', 'Windesheim_ETLUser';

