--USE [Demo_SurpassDataWarehouse]
--GO

--/****** Object:  Index [PK_stg_FactComponentResponses]    Script Date: 23/11/2016 11:23:39 ******/
--ALTER TABLE [ETL].[stg_FactComponentResponses] ADD  CONSTRAINT [PK_stg_FactComponentResponses] PRIMARY KEY CLUSTERED 
--(
--	[ExamSessionKey] ASC,
--	[CID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--GO



USE Demo_SecureAssess

if OBJECT_ID('tempdb..#IDs') is not null drop table #IDs;

select ID
into #IDs
from WAREHOUSE_ExamSessionTable
where warehouseTime > DATEADD(MONTH, -6, getDATE());

create clustered index [ix] on #IDs (ID);

select c.*
FROM (

select  WAREHOUSEExamSessionID
	,   ItemID
	,	a.b.value('@id','tinyint') CID
from WAREHOUSE_ExamSessionItemResponseTable
INNER JOIN #IDs d
on d.ID = WAREHOUSE_ExamSessionItemResponseTable.WAREHOUSEExamSessionID
cross apply ItemResponseData.nodes('p/s//c') a(b)
where a.b.value('@id','tinyint') is not null
 ) C
group by WAREHOUSEExamSessionID
	,   ItemID
	,	CID
having count(*) > 1

	--5001P3820S1C6

--SELECT *
--from WAREHOUSE_ExamSessionItemResponseTable
--where WAREHOUSEExamSessionID = 4006
--	and itemid = '5195P15819'