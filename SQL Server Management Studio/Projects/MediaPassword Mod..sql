DECLARE @BackupFile NVARCHAR(255),
		@MediaPassword NVARCHAR(100);

--SET @BackupFile ='T:\Backup\STG_SQA2_CP.2016.02.24.bak'	  --Doesnt need P/W
SET @BackupFile ='T:\FTP\BC\BC.CPSM.ALL2015.12.09.bak'   --Needs P/w
SET @MediaPassword = 'WITH MEDIAPASSWORD = ''s_!85WrenukEtrut+4f5a#-a&ac*aKUs'''


BEGIN TRY
	RESTORE FILELISTONLY FROM DISK = @BackupFile 
	SET @MediaPassword = ''
END TRY
BEGIN CATCH
	SET @MediaPassword = 'WITH MEDIAPASSWORD = ''s_!85WrenukEtrut+4f5a#-a&ac*aKUs'''
END CATCH




--SET @BackupFile ='T:\Backup\STG_SQA2_CP.2016.02.24.bak'	  --Doesnt need P/W
--SET @BackupFile ='T:\FTP\BC\BC.CPSM.ALL2015.12.09.bak'   --Needs P/w
--SET @MediaPassword = 'WITH MEDIAPASSWORD = ''s_!85WrenukEtrut+4f5a#-a&ac*aKUs'''

DECLARE @Header TABLE (
      BackupName NVARCHAR(128)
      ,BackupDescription NVARCHAR(255)
      ,BackupType SMALLINT
      ,ExpirationDate DATETIME
      ,Compressed BIT
      ,Position SMALLINT
      ,DeviceType TINYINT
      ,UserName NVARCHAR(128)
      ,ServerName NVARCHAR(128)
      ,DatabaseName NVARCHAR(128)
      ,DatabaseVersion INT
      ,DatabaseCreationDate DATETIME
      ,BackupSize NUMERIC(20, 0)
      ,FirstLSN NUMERIC(25, 0)
      ,LastLSN NUMERIC(25, 0)
      ,CheckpointLSN NUMERIC(25, 0)
      ,DatabaseBackupLSN NUMERIC(25, 0)
      ,BackupStartDate DATETIME
      ,BackupFinishDate DATETIME
      ,SortOrder SMALLINT
      ,CodePage SMALLINT
      ,UnicodeLocaleId INT
      ,UnicodeComparisonStyle INT
      ,CompatibilityLevel TINYINT
      ,SoftwareVendorId INT
      ,SoftwareVersionMajor INT
      ,SoftwareVersionMinor INT
      ,SoftwareVersionBuild INT
      ,MachineName NVARCHAR(128)
      ,Flags INT
      ,BindingID UNIQUEIDENTIFIER
      ,RecoveryForkID UNIQUEIDENTIFIER
      ,Collation NVARCHAR(128)
      ,FamilyGUID UNIQUEIDENTIFIER
      ,HasBulkLoggedData BIT
      ,IsSnapshot BIT
      ,IsReadOnly BIT
      ,IsSingleUser BIT
      ,HasBackupChecksums BIT
      ,IsDamaged BIT
      ,BeginsLogChain BIT
      ,HasIncompleteMetaData BIT
      ,IsForceOffline BIT
      ,IsCopyOnly BIT
      ,FirstRecoveryForkID UNIQUEIDENTIFIER
      ,ForkPointLSN NUMERIC(25, 0) NULL
      ,RecoveryModel NVARCHAR(60)
      ,DifferentialBaseLSN NUMERIC(25, 0) NULL
      ,DifferentialBaseGUID UNIQUEIDENTIFIER
      ,BackupTypeDescription NVARCHAR(60)
      ,BackupSetGUID UNIQUEIDENTIFIER NULL
      ,CompressedBackupSize NUMERIC(20, 0)
      );
	  
INSERT INTO @Header
EXECUTE (N'RESTORE HEADERONLY FROM DISK = N''' + @BackupFile + ''' ' +@MediaPassword+'');

SELECT * FROM @Header
