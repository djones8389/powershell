SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

SELECT wests.examsessionid
	, wests.keycode
	, wests.warehouseTime
	, ExamStateChangeAuditXml.value('data(exam/stateChange[newStateID=1]/changeDate)[1]','datetime') [State1XML]
	, CreatedDateTime
FROM WAREHOUSE_ExamSessionTable_Shreded wests
inner join WAREHOUSE_ExamSessionTable west
on west.id = wests.examSessionId
inner join WAREHOUSE_ScheduledExamsTable wscet
on wscet.id = west.WAREHOUSEScheduledExamID
where centrename in (

	SELECT CentreName
	FROM CentreTable
	WHERE ID NOT IN (
				  SELECT DISTINCT CentreID
				  FROM WAREHOUSE_CentreTable WCT
				  INNER JOIN WAREHOUSE_ScheduledExamsTable WSCET
						 ON WCT.ID = WSCET.WAREHOUSECentreID
				  INNER JOIN WAREHOUSE_ExamSessionTable WEST
						 ON WEST.WAREHOUSEScheduledExamID = WSCET.ID
				  WHERE WEST.completionDate > DATEADD(year, - 2, GETDATE())
				  )
)
