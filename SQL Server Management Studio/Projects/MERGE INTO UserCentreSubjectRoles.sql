  DROP TABLE UserCentreSubjectRoles_15092017;

  SELECT *
  INTO UserCentreSubjectRoles_15092017_1
  FROM UserCentreSubjectRoles;

  SET IDENTITY_INSERT UserCentreSubjectRoles ON

  MERGE INTO UserCentreSubjectRoles AS TGT
  USING (
  SELECT distinct ID,UserId, CentreId, SubjectId, SubjectLinkType, Assignable, RoleId
  FROM [UserCentreSubjectRoles_DeleteAudit] READUNCOMMITTED
        where Timestamp > '2017-09-15 09:59:01.880'
  ) AS SRC
  ON TGT.ID = SRC.ID
	AND TGT.UserID = SRC.UserID
	AND TGT.CentreID = SRC.UserID
	AND TGT.SubjectID = SRC.UserID
	AND TGT.SubjectLinkType = SRC.UserID
	AND TGT.Assignable = SRC.UserID
	AND TGT.RoleId = SRC.UserID
	WHEN NOT MATCHED THEN 
	
	INSERT (Id, UserId, CentreId, SubjectId, SubjectLinkType, Assignable, RoleId)
	VALUES (SRC.Id, SRC.UserId, SRC.CentreId, SRC.SubjectId, SRC.SubjectLinkType, SRC.Assignable, SRC.RoleId);

	SET IDENTITY_INSERT UserCentreSubjectRoles OFF