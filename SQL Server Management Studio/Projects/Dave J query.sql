--Use TagGroups

if exists(select 1 from sys.objects where name = 'vw_TagIndexDJ' and type = 'V')
	DROP VIEW dbo.vw_TagIndexDJ 
GO

CREATE VIEW dbo.vw_TagIndexDJ 
WITH SCHEMABINDING  
AS  
SELECT 
	i.SubjectId
	, i.Id as ItemId
	, stg.TagTypeName TagGroup
	, stv.text as TagValue
	, stg.Id as STT_ID
FROM dbo.Items i
  INNER JOIN dbo.SubjectTagValueItems si ON si.Item_Id = i.id
  INNER JOIN dbo.SubjectTagValues stv ON si.SubjectTagValue_Id = stv.Id
  INNER JOIN dbo.SubjectTagTypes stg on stv.SubjectTagTypeId = stg.Id AND stg.SubjectId = i.SubjectId
GO  

CREATE UNIQUE CLUSTERED INDEX IDX_V1   
    ON dbo.vw_TagIndexDJ (ItemId,STT_ID);  
GO  
CREATE NONCLUSTERED INDEX IDX_V2   
	ON dbo.vw_TagIndexDJ (TagValue,TagGroup) 
	INCLUDE (SubjectId,ItemId)
GO  




Declare @Start datetime = getDate() 

SET STATISTICS IO, TIME ON

	declare @TagTypeName1 NVARCHAR(100) = 'TAG_TYPE_10'
				  ,@TagTypeName2 NVARCHAR(100) = 'TAG_TYPE_1'
				  ,@TagTypeName3 NVARCHAR(100) = 'TAG_TYPE_2'
				  ,@StvText NVARCHAR(100) = 'One'
				  ,@count tinyint

	SELECT @count = (count(@TagTypeName1) + count(@TagTypeName2) + count(@TagTypeName3));

	SELECT 
		subjectId
		, ItemId
	FROM dbo.vw_TagIndexDJ (NOEXPAND)
	WHERE TagValue = @StvText
		and TagGroup in (@TagTypeName1,@TagTypeName2,@TagTypeName3)
	group by subjectId, ItemId
	having count(*) = @count

Declare @End datetime = getDate() 

SELECT cast(DATEDIFF(MILLISECOND, @Start, @End) as varchar(10)) + ' Milliseconds runtime'