use master

select loginame, d.name, s.spid, 'KILL ' + convert(varchar(3),s.spid)
from sys.databases D
INNER JOIN sys.sysprocesses S
on D.database_id = s.dbid
where database_id > 4
	and d.name in ('SHA_BIS_SurpassDataWarehouse_RESTORE','DEMO_CP_Aspire_Demo_Content')
   
----DROP DATABASE [master_restore];
----DROP DATABASE [PPD_BCIntegration_CPProjectAdmin_21042016];
----DROP DATABASE [PRV_BritishCouncil_SurpassDataWarehouse];
--DROP DATABASE [SHA_BIS_SurpassDataWarehouse_RESTORE];
--DROP DATABASE [DEMO_CP_Aspire_Demo_Content];
----DROP DATABASE [DI_AQA_CPProjectAdmin];
----DROP DATABASE [SANDBOX_Saxion_ContentAuthor];
----DROP DATABASE [SANDBOX_Saxion_ItemBank];
----DROP DATABASE [SANDBOX_Saxion_SecureAssess];

--BACKUP DATABASE [PPD_BCIntegration_CPProjectAdmin_21042016] TO DISK = N'T:\BACKUP\PPD_BCIntegration_CPProjectAdmin_21042016.2016.08.03.bak' WITH NAME = N'PPD_BCIntegration_CPProjectAdmin_21042016- Database Backup', COPY_ONLY, NOFORMAT, NOINIT, SKIP, COMPRESSION, NOREWIND, NOUNLOAD, STATS = 10;
--BACKUP DATABASE [SHA_BIS_SurpassDataWarehouse_RESTORE] TO DISK = N'T:\BACKUP\SHA_BIS_SurpassDataWarehouse_RESTORE.2016.08.03.bak' WITH NAME = N'SHA_BIS_SurpassDataWarehouse_RESTORE- Database Backup', COPY_ONLY, NOFORMAT, NOINIT, SKIP, COMPRESSION, NOREWIND, NOUNLOAD, STATS = 10;
--BACKUP DATABASE [PRV_BritishCouncil_SurpassDataWarehouse] TO DISK = N'T:\BACKUP\PRV_BritishCouncil_SurpassDataWarehouseBadData.2016.08.03.bak' WITH NAME = N'PRV_BritishCouncil_SurpassDataWarehouse- Database Backup', COPY_ONLY, NOFORMAT, NOINIT, SKIP, COMPRESSION, NOREWIND, NOUNLOAD, STATS = 10;
--BACKUP DATABASE [DEMO_CP_Aspire_Demo_Content] TO DISK = N'T:\BACKUP\DEMO_CP_Aspire_Demo_Content.2016.08.03.bak' WITH NAME = N'DEMO_CP_Aspire_Demo_Content- Database Backup', COPY_ONLY, NOFORMAT, NOINIT, SKIP, COMPRESSION, NOREWIND, NOUNLOAD, STATS = 10;
--BACKUP DATABASE [DI_AQA_CPProjectAdmin] TO DISK = N'T:\BACKUP\DI_AQA_CPProjectAdmin.2016.08.03.bak' WITH NAME = N'DI_AQA_CPProjectAdmin- Database Backup', COPY_ONLY, NOFORMAT, NOINIT, SKIP, COMPRESSION, NOREWIND, NOUNLOAD, STATS = 10;