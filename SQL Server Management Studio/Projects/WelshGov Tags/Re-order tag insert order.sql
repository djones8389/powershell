use SandboxWelshGov_ContentAuthor

DROP TABLE IF EXISTS tempdb..#InsertTag;
DROP TABLE IF EXISTS tempdb..#LNF;
DROP TABLE IF EXISTS tempdb..#BeforePublish;

CREATE TABLE #LNF (

	itemname nvarchar(100)
	, itemid nvarchar(100)
	, LNFYear2 varchar(20)
	, LNFYear3 varchar(20)
	, LNFYear4 varchar(20)
	, LNFYear5 varchar(20)
	, LNFYear6 varchar(20)
	, LNFYear7 varchar(20)
	, LNFYear8 varchar(20)
	, LNFYear9 varchar(20)
);


BULK INSERT #LNF  
FROM 'F:\WelshGov\LNF - copy.csv'
WITH (fieldterminator=',',rowterminator='\n');

DELETE FROM #LNF WHERE ISNUMERIC(itemID) = 0;

SELECT c.itemid
		, c.Text
		, c.Deleted
		, ROW_NUMBER() OVER (PARTITION BY itemid ORDER BY len(text)) R
INTO #InsertTag
FROM (
	SELECT cast(itemid as float) itemid
		, SUBSTRING(a.LNF, 0, CHARINDEX('|', lnf)) as [Text], 0 as [Deleted]
	FROM (
	SELECT itemid
			,  COALESCE(b.LNFYear2, b.LNFYear3,b.LNFYear4,b.LNFYear5,b.LNFYear6,b.LNFYear7,b.LNFYear8,b.LNFYear9) LNF
	FROM #LNF b
	) a
	where  SUBSTRING(a.LNF, 0, CHARINDEX('|', lnf)) <> ''

	UNION

	SELECT cast(itemid as float) itemid
		, ltrim(SUBSTRING(a.LNF, CHARINDEX('|', lnf)+1, 10)) as [Text], 0 as [Deleted]
	FROM (
	SELECT itemid
			,  COALESCE(b.LNFYear2, b.LNFYear3,b.LNFYear4,b.LNFYear5,b.LNFYear6,b.LNFYear7,b.LNFYear8,b.LNFYear9) LNF
	FROM #LNF b
	) a
) c
ORDER BY itemid, r

select * from #InsertTag

select a.*
from (
select itemid
	, #InsertTag.Text
	, ROW_NUMBER() OVER (PARTITION BY itemid,#InsertTag.Text ORDER BY itemid, len(#InsertTag.Text) asc) R
from #InsertTag
LEFT JOIN SubjectTagValues STV
ON stv.Text collate SQL_Latin1_General_CP1_CI_AS = #InsertTag.Text
LEFT JOIN SubjectTagTypes stt 
ON stt.Id = stv.SubjectTagTypeId
LEFT JOIN  SubjectTagValueItems stvi 
ON stvi.Item_Id = #InsertTag.itemid
WHERE TagTypeName in ('LNF')
	and itemid = 467
) a
where r = 1
order by 1


select c.*
from (
	select b.*
		, ROW_NUMBER() OVER (PARTITION BY itemid,b.Text ORDER BY len(b.Text) asc) R
	from SubjectTagValues a
	inner join (
		SELECT itemid
			, [text]
			, Deleted
		FROM #InsertTag
		where  itemid = 467
	) b
	on a.Text  collate SQL_Latin1_General_CP1_CI_AS = b.Text
	order by len(b.Text)
) c
where r = 1