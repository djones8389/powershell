RESTORE DATABASE [STG_NCFE_CPProjectAdmin]
FROM DISK = N'T:\FTP\NCFE.ALL.2018.01.19.bak'
WITH FILE = 1
	,NOUNLOAD
	,NOREWIND
	,REPLACE
	,STATS = 10
	,MOVE 'Data' TO N'S:\DATA\STG_NCFE_CPProjectAdmin.mdf'
	,MOVE 'Log' TO N'L:\LOGS\STG_NCFE_CPProjectAdmin.ldf';

RESTORE DATABASE [STG_NCFE_Itembank]
FROM DISK = N'T:\FTP\NCFE.ALL.2018.01.19.bak'
WITH FILE = 2
	,NOUNLOAD
	,NOREWIND
	,REPLACE
	,STATS = 10
	,MOVE 'Data' TO N'S:\DATA\STG_NCFE_Itembank.mdf'
	,MOVE 'Log' TO N'L:\LOGS\STG_NCFE_Itembank.ldf';

RESTORE DATABASE [STG_NCFE_SecureAssess]
FROM DISK = N'T:\FTP\NCFE.ALL.2018.01.19.bak'
WITH FILE = 3
	,NOUNLOAD
	,NOREWIND
	,REPLACE
	,STATS = 10
	,MOVE 'Data' TO N'S:\DATA\STG_NCFE_SecureAssess.mdf'
	,MOVE 'Log' TO N'L:\LOGS\STG_NCFE_SecureAssess.ldf';

USE STG_NCFE_CPProjectAdmin
CREATE USER STG_NCFE_CPProjectAdmin_User FOR LOGIN [STG_NCFE_CPProjectAdmin_Login]
exec sp_addrolemember 'db_owner','STG_NCFE_CPProjectAdmin_User'

USE STG_NCFE_Itembank
CREATE USER STG_NCFE_Itembank_User FOR LOGIN [STG_NCFE_Itembank_Login]
exec sp_addrolemember 'db_owner','STG_NCFE_Itembank_User'

USE STG_NCFE_SecureAssess
CREATE USER STG_NCFE_SecureAssess_User FOR LOGIN [STG_NCFE_SecureAssess_Login]
exec sp_addrolemember 'db_owner','STG_NCFE_SecureAssess_User'