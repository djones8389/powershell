USE ICAEW_ItemBank

SELECT	
		QualificationName
         ,AssessmentName
		 ,AssessmentGroupTable.Name
         ,ExternalReference
         ,AssessmentStatusLookupTable.Name
		 ,LastModifiedDate
FROM AssessmentTable 

	inner join AssessmentGroupTable
	on AssessmentGroupTable.ID = AssessmentTable.AssessmentGroupID
	
	inner join QualificationTable
	on QualificationTable.ID = AssessmentGroupTable.QualificationID

	inner join AssessmentStatusLookupTable
	on AssessmentStatusLookupTable.ID = AssessmentTable.AssessmentStatus
	
WHERE  QualificationTable.QualificationName = 'Audit and Assurance'


USE ICAEW_SecureAssess

select
	QualificationName
	, examName
	, externalReference
	, keycode
	, warehousetime
from WAREHOUSE_ExamSessionTable_Shreded
where QualificationName = 'Audit and Assurance'
	and examName = 'FDEC2016PLAA2'

	order by examsessionid desc;


select 
	QualificationName
	, examName
	, examVersionRef
	, Keycode
	, examState
from ScheduledExamsTable scet
inner join Examsessiontable est
on est.scheduledexamid = scet.id
inner join IB3QualificationLookup ib 
on ib.id = scet.qualificationId
where QualificationName = 'Audit and Assurance'
	and examName = 'FDEC2016PLAA2'
	
	order by est.id desc;