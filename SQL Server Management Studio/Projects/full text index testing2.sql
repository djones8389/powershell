USE [FULL_TEXT_TEST]

declare @word varchar(100) = 'physician'
declare @type varchar(10) = 'OR'
declare @dynamic nvarchar(1000) = ''

select @dynamic += '
select question_text
	,	answer_text
from [answers] A
inner join questions q
on q.id = a.itemID
where  contains(question_text,'''+@word+''')
 '+@type+' contains(answer_text,'''+@word+''')
'
print @dynamic

exec(@dynamic)


--create nonclustered index [nc_ix] on [answers] (itemId,answer_text)

--drop index [nc_ix] on [answers]
	/*

	SELECT c.fulltext_catalog_id
		,c.NAME AS fulltext_catalog_name
		,i.change_tracking_state
		--,i.object_id
		,OBJECT_SCHEMA_NAME(i.object_id) + '.' + OBJECT_NAME(i.object_id) AS [object_name]
		,f.num_fragments
		,f.fulltext_mb
		,f.largest_fragment_mb
		,100.0 * (f.fulltext_mb - f.largest_fragment_mb) / NULLIF(f.fulltext_mb, 0) AS fulltext_fragmentation_in_percent
		, getDATE()
	FROM sys.fulltext_catalogs c
	INNER JOIN sys.fulltext_indexes i ON i.fulltext_catalog_id = c.fulltext_catalog_id
	INNER JOIN (
		-- Compute fragment data for each table with a full-text index
		SELECT table_id
			,COUNT(*) AS num_fragments
			,CONVERT(DECIMAL(9, 2), SUM(data_size / (1024. * 1024.))) AS fulltext_mb
			,CONVERT(DECIMAL(9, 2), MAX(data_size / (1024. * 1024.))) AS largest_fragment_mb
		FROM sys.fulltext_index_fragments
		GROUP BY table_id
		) f ON f.table_id = i.object_id

		*/