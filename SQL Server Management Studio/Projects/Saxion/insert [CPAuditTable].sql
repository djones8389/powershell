SELECT B.*
	, 'INSERT [Saxion_SecureAssess].[dbo].[CPAuditTable]
		VALUES('''+cast(cpid1 as nvarchar(1000))+''', '+cast(a.ItemVersion as nvarchar(20))+', '''+cast(itemxml as nvarchar(MAX))+''', '''+cast(CorrectAnswers as nvarchar(1000))+''','''+cast(QuestionTypes as nvarchar(1000))+''','''+cast(ShortCorrectAnswers  as nvarchar(1000))+''')
	
	'
FROM (
SELECT  [ItemID]
      ,MAX([ItemVersion]) [ItemVersion]
  FROM [Saxion_SecureAssess].[dbo].[CPAuditTable]
  where itemid in ('2562P2582','2562P2586','2562P2596','2562P2596','2562P2598','2562P2600','2562P2602','2562P2607','2562P2607','2562P2609','2562P2610','2562P2617','2562P2621','2562P2623','2562P2628','2562P2629','2562P2632','2562P2633','2562P2634','2562P2636','2562P2637','2562P2647','2562P2653','2562P2659','2562P2664','2562P2668','2562P2670','2562P2673','2562P2674','2562P2678','2562P2680','2562P2685','2562P2687','2562P2688','2562P2696','2562P2698','2562P2698','2562P2702','2562P2702','2562P2703','2562P2708','2562P2710','2562P2714','2562P2716','2562P2978','2562P3040','2562P3041','2562P3046','2562P3051','2562P3087','2562P3095','2562P3097','2562P3153','2562P3156','2562P3229','2562P3232','2562P3234','2562P3237','2562P3241','2562P3243')
	GROUP BY [ItemID]
) A
inner join [CPAuditTable] b on a.ItemID = b.ItemID
	and b.ItemVersion = a.ItemVersion
inner join (
select cpid1,'2562p' + cpid2 [cpid2]
from #cpitems
) c
on c.cpid2 = a.ItemID
--	create table #cpitems (cpid1 nvarchar(max),cpid2 nvarchar(max))
--bulk insert #cpitems
--from 'T:\book1.csv'
--with (fieldterminator = ',', rowterminator = '\n')
--go
--(
--select '2562p' + cpid2
--from #cpitems)
select distinct b.*
 from [CPAuditTable] a
 inner join #cpitems b
 on a.ItemID = b.cpid1