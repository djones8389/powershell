use saxion_itembank
--drop table ##itemstoadd 
SELECT distinct 
	--SUBSTRING(REPLACE(REPLACE(cast(a.b.query('.') as NVARCHAR(MAX)), '<XPath>@ID="',''), '"</XPath>',''), 0, CHARINDEX('P', REPLACE(REPLACE(cast(a.b.query('.') as NVARCHAR(MAX)), '<XPath>@ID="',''), '"</XPath>',''))) as [ProjectID]
	 REPLACE(REPLACE(cast(a.b.query('.') as NVARCHAR(MAX)), '<XPath>@ID="',''), '"</XPath>','') ItemID
	, QualificationName
	, AssessmentName
	, ExternalReference
--into ##itemstoadd
FROM AssessmentTable AT (READUNCOMMITTED)
		
CROSS APPLY AssessmentRules.nodes('PaperRules//XPath') a(b)

INNER JOIN AssessmentGroupTable AGT  (READUNCOMMITTED)
on AGT.ID = AT.AssessmentGroupID

INNER JOIN QualificationTable QT (READUNCOMMITTED)
ON QT.ID = AGT.QualificationID

left join (
	select projectid
		 ,  a.b.value('data(@ID)[1]','nvarchar(20)') ptitemid
	from projecttable (READUNCOMMITTED)
	cross apply projectstructure.nodes('P//I') a(b)
) pt
on pt.projectid = SUBSTRING(REPLACE(REPLACE(cast(a.b.query('.') as NVARCHAR(MAX)), '<XPath>@ID="',''), '"</XPath>',''), 0, CHARINDEX('P', REPLACE(REPLACE(cast(a.b.query('.') as NVARCHAR(MAX)), '<XPath>@ID="',''), '"</XPath>','')))
	and REPLACE(REPLACE(cast(a.b.query('.') as NVARCHAR(MAX)), '<XPath>@ID="',''), '"</XPath>','') = ptitemid
--cross apply projectstructure.nodes('P//I') a(b)

--inner join Itemtable IT				-- has to exist in here!
--on cast(it.projectid as varchar(10))+ 'p' + cast(it.itemref as varchar(10)) = REPLACE(REPLACE(cast(a.b.query('.') as NVARCHAR(MAX)), '<XPath>@ID="',''), '"</XPath>','')

where --AT.IsValid = 0
	 REPLACE(REPLACE(cast(a.b.query('.') as NVARCHAR(MAX)), '<XPath>@ID="',''), '"</XPath>','') not like '<XPath>%'
	 and externalreference in ('AGZ_PTIC_48282_1.1_1717','AGZ_PTIC_her_48282_1.2_1718','AGZ_PTIC_45236_1.2_1718','AGZ_FYS_her_45226_1.2_1718')
	--and at.AssessmentStatus = 2
	and ptitemid is null
	order by QualificationName
	, AssessmentName
	, ExternalReference


	--select distinct  externalreference
	--from assessmenttable
	--where externalreference in ('AGZ_PTIC_48282_1.1_1717','AGZ_PTIC_her_48282_1.2_1718','AGZ_PTIC_45236_1.2_1718','AGZ_FYS_her_45226_1.2_1718')

--select distinct itemid 
--from ##itemstoadd 
--inner join Itemtable IT				-- has to exist in here!
--on cast(it.projectid as varchar(10))+ 'p' + cast(it.itemref as varchar(10))=itemid




--select distinct  a.b.value('data(@ID)[1]','nvarchar(20)')
--from projecttable
--cross apply projectstructure.nodes('P//I') a(b)

--declare @string nvarchar(100) = '5101P78371'
--select len(substring(@string, charindex('P',@string)+1,10))


--select f.*
--, substring(ptitemid, charindex('P',ptitemid)+1,10)
--from (

--select projectid
--		 ,  a.b.value('data(@ID)[1]','nvarchar(20)') ptitemid
--	from projecttable (READUNCOMMITTED)
--	cross apply projectstructure.nodes('P//I') a(b)
--	) f

--	select *
--	from itemtable
--	where projectid = 5701
--	and itemref = '330569'