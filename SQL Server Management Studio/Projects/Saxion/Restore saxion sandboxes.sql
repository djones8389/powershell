RESTORE DATABASE [SANDBOX_Saxion_Current_ContentAuthor]
FROM DISK = N'T:\Backup\SaxionDebug\TestDeploy3_ContentAuthor.2017.11.09.bak'
WITH FILE = 1, NOUNLOAD, NOREWIND, REPLACE, STATS = 10, MOVE 'Data' TO N'S:\DATA\Saxion_Current_ContentAuthor.mdf', MOVE 'Log' TO N'L:\LOGS\Saxion_Current_ContentAuthor.ldf';

RESTORE DATABASE [SANDBOX_Saxion_Current_ItemBank]
FROM DISK = N'T:\Backup\SaxionDebug\TestDeploy3_ItemBank.2017.11.09.bak'
WITH FILE = 1, NOUNLOAD, NOREWIND, REPLACE, STATS = 10, MOVE 'Data' TO N'S:\DATA\Saxion_Current_ItemBank.mdf', MOVE 'Log' TO N'L:\LOGS\Saxion_Current_ItemBank.ldf';

RESTORE DATABASE [SANDBOX_Saxion_PreDeploy_ItemBank]
FROM DISK = N'T:\Backup\SaxionDebug\Saxion_Itembank_2017-10-09.bak'
WITH FILE = 1, NOUNLOAD, NOREWIND, REPLACE, STATS = 10, MOVE 'Data' TO N'S:\DATA\SANDBOX_Saxion_PreDeploy_ItemBank.mdf', MOVE 'Log' TO N'L:\LOGS\SANDBOX_Saxion_PreDeploy_ItemBank.ldf';

RESTORE DATABASE [SANDBOX_Saxion_PreDeploy_2017-11-08_ContentAuthor]
FROM DISK = N'T:\Backup\SaxionDebug\Saxion_ContentAuthor_2017-10-09.bak'
WITH FILE = 1, NOUNLOAD, NOREWIND, REPLACE, STATS = 10, MOVE 'Data' TO N'S:\DATA\Saxion_PreDeploy_2017-11-08.mdf', MOVE 'Log' TO N'L:\LOGS\Saxion_PreDeploy_2017-11-08.ldf';

select 'kill ' + convert(varchar(5),spid)
	,db_name(dbid)
from sys.sysprocesses s
where dbid > 4
	--and  db_name(dbid) = 'SaxionDefect_ItemBank'
order by 2; 

kill 76