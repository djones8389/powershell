SELECT *
FROM (
		select ProjectId, ContentProducerItemId, s.Title, (cast(ProjectId as varchar(10)) + 'P' + cast(ContentProducerItemId as varchar(10))) ItemID, i.PublishStatus, i.id
		from TestDeploy3_ContentAuthor.dbo.Items i
		inner join TestDeploy3_ContentAuthor.dbo.Subjects S
		on S.id = i.subjectid
		where ProjectId in (2560) --and PublishStatus = 2 --published pages 
		order by 2
) CA
LEFT JOIN (

SELECT distinct 
	SUBSTRING(REPLACE(REPLACE(cast(a.b.query('.') as NVARCHAR(MAX)), '<XPath>@ID="',''), '"</XPath>',''), 0, CHARINDEX('P', REPLACE(REPLACE(cast(a.b.query('.') as NVARCHAR(MAX)), '<XPath>@ID="',''), '"</XPath>',''))) as [ProjectID]
	, REPLACE(REPLACE(cast(a.b.query('.') as NVARCHAR(MAX)), '<XPath>@ID="',''), '"</XPath>','') ItemID
	, QualificationName
	, AssessmentName
	, ExternalReference
FROM AssessmentTable AT (READUNCOMMITTED)
		
CROSS APPLY AssessmentRules.nodes('PaperRules//XPath') a(b)

INNER JOIN AssessmentGroupTable AGT 
on AGT.ID = AT.AssessmentGroupID

INNER JOIN QualificationTable QT
ON QT.ID = AGT.QualificationID

where AT.IsValid = 0
	and REPLACE(REPLACE(cast(a.b.query('.') as NVARCHAR(MAX)), '<XPath>@ID="',''), '"</XPath>','') not like '<XPath>%'
	--and SUBSTRING(REPLACE(REPLACE(cast(a.b.query('.') as NVARCHAR(MAX)), '<XPath>@ID="',''), '"</XPath>',''), 0, CHARINDEX('P', REPLACE(REPLACE(cast(a.b.query('.') as NVARCHAR(MAX)), '<XPath>@ID="',''), '"</XPath>',''))) = 2557
	and at.AssessmentStatus = 2 --live assessments
	and QualificationName = 'AGZ_FYS'
	order by 2
) IB
ON CA.ProjectId = IB.ProjectID
	AND CA.ItemID = IB.ItemID
WHERE IB.ItemID IS NULL
	and CA.ItemID IS NOT NULL
	--and PublishStatus = 2
order by 4;