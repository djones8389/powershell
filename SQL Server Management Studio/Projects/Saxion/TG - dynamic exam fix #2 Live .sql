declare @exams table(id int, rules xml)

--Get the exams that have item rules and are invalid and puts them into a table variable
insert into @exams

--set identity_insert [AssessmentTable_Backup] on

--INSERT [AssessmentTable_Backup](ID, AssessmentRules)
select ID, AssessmentRules
from AssessmentTable (READUNCOMMITTED)
where IsValid = 0 and AssessmentRules.exist('PaperRules/Section/Rule/Param[@Type=3]') = 1

--set identity_insert [AssessmentTable_Backup] off

declare @count int

select @count = COUNT(ID) 
from @exams

declare @id int
declare @rules xml

declare @xpaths table (val nvarchar(max), itemId nvarchar(10), projectId nvarchar(10))

declare @numFolders int
declare @rulesString nvarchar(max)
declare @currentItemId nvarchar(10)
declare @projectId nvarchar(10)
declare @currentXPath nvarchar(max)
declare @newItemId bigint
declare @newXPath nvarchar(max)

declare @changedAny bit

--set @count = 20

declare @numUpdated int = 0

--Nasty loop through the table, taking the top one each time then deleting it
while @count > 0
      begin
            select top 1 @id = ID, @rules = rules, @changedAny = 0
            from @exams
            order by id asc
            
            delete @exams
            where id = @id
            
            --Selects the item xpath values which can then be 
            insert into @xpaths
            (val)
            select y.value('(XPath/text())[1]', 'nvarchar(max)')
            from @rules.nodes('PaperRules/Section/Rule/Param[@Type=3]') x(y)
            
            --Selects the item and project IDs from the XPaths
            update @xpaths
            set itemId = SUBSTRING(val, charindex('P', val, 0) + 1, len(val) - charindex('P', val, 0) - 1),
                  projectId = 
                  SUBSTRING(val, 6, CHARINDEX('P', val, 0) - 6)
            from @xpaths
            
            select @numFolders = COUNT(val)
            from @xpaths
            
            set @rulesString = CONVERT(nvarchar(max), @rules)
            
            --Loops through the item references to fix each one
            while @numFolders > 0
                  begin
                        set @numFolders = @numFolders - 1
                        
                        select top 1 
                              @currentItemId = itemid,
                              @projectId = projectId,
                              @currentXPath = val
                        from @xpaths
                        
                        delete @xpaths
                        where
                              itemId = @currentItemId
                              and
                              projectId = @projectId
                              and
                              val = @currentXPath
                          
                           
                        --Only updates if it can find an item whose ID matches the specified ID and is in the correct subject
                        if(exists(select i.Id
                                    from Saxion_ContentAuthor..Items i (READUNCOMMITTED)
                                    inner join Saxion_ContentAuthor..Subjects s (READUNCOMMITTED)
                                    on i.SubjectId = s.Id
                                    where
                                          s.ProjectId = CONVERT(bigint, @projectId)
                                          and 
                                          i.ID = @currentItemId
                                          and
                                          i.[Type] = 2
                                          and
                                          i.ContentProducerItemId is not null))
                              begin
                                    --First check that there isn't an item whose CP ID matches the one in the XPath. If there is, we won't know which is correct
                                    if(exists(select i.Id
                                          from Saxion_ContentAuthor..Items i (READUNCOMMITTED)
                                          inner join Saxion_ContentAuthor..Subjects s (READUNCOMMITTED)
                                          on i.SubjectId = s.Id
                                          where
                                                s.ProjectId = CONVERT(bigint, @projectId)
                                                and 
                                                i.ContentProducerItemId = @currentItemId
                                                and
                                                i.[Type] = 2))
                                          begin
                                                select 'Problem with ', @id
                                          end
                                    else
                                          begin
                                                --Get the CP ID of the item it should be pointing at
                                                select @newItemId = i.ContentProducerItemId
                                                from Saxion_ContentAuthor..Items i (READUNCOMMITTED)
                                                inner join Saxion_ContentAuthor..Subjects s (READUNCOMMITTED)
                                                on i.SubjectId = s.Id
                                                where
                                                      s.ProjectId = CONVERT(int, @projectId)
                                                      and 
                                                      i.Id = @currentItemId
                                                      and
                                                      i.[Type] = 2
                                                      and
                                                      i.ContentProducerItemId is not null
                                                
                                                --Create the amended XPath, which uses the correct item ID
                                                set @newXPath = '@ID="' + @projectId + 'P' + convert(nvarchar(10), @newItemId) + '"'
                                                
                                                --Replace the XPath in the rules XML      
                                                set @rulesString = REPLACE(@rulesString, @currentXPath, @newXPath)
                                                
                                                set @changedAny = 1
                                          end
                              end
                        
                  end
            
            if @changedAny = 1
                        begin
                              --Update the rules XML for the exam, and flag it for validation
                              update AssessmentTable
                              set AssessmentRules = CONVERT(xml, @rulesString), RequiresValidation = 1
                              where @id = id
                              
                              --select @rules, CONVERT(xml, @rulesString)
                              
                              set @numUpdated = @numUpdated + 1
                        end
            
            set @count = @count - 1
      end

select @numUpdated
