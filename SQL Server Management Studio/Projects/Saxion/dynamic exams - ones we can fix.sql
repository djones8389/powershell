USE Saxion_ItemBank

IF OBJECT_ID ('tempdb..##TEMP') IS NOT NULL drop table ##TEMP;

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

SELECT DISTINCT
	A.AssessmentID
	, ExternalReference
	, A.ProjectID
	, A.FolderID
	, a.FolderName
	--, cp.FolderID
	--, cp2.[ProjectName]
	--distinct ExternalReference
into ##TEMP
FROM (
	select ID AssessmentID
	, ExternalReference
	--, AssessmentRules
	, case when
		isnumeric(replace(replace(substring(c.d.value('.','nvarchar(500)'), CHARINDEX('P/@ID',c.d.value('.','nvarchar(500)')),20),'P/@ID=',''),')','')) = 0
			then a.b.value('data(Param/ProjectIDList/ID)[1]','smallint')
			else replace(replace(substring(c.d.value('.','nvarchar(500)'), CHARINDEX('P/@ID',c.d.value('.','nvarchar(500)')),20),'P/@ID=',''),')','')
			end as  [ProjectID]
	, a.b.value('data(@Name)[1]','nvarchar(100)') [FolderName]
	, case when 
		left(c.d.value('.','nvarchar(500)'), 16) = '(ancestor::F/@ID'
		then replace(substring(c.d.value('.','nvarchar(500)'), CHARINDEX('F/@ID',c.d.value('.','nvarchar(500)')),10),'F/@ID=','')
		else replace(replace(substring(c.d.value('.','nvarchar(500)'), CHARINDEX('../@ID',c.d.value('.','nvarchar(500)')),CHARINDEX(')',c.d.value('.','nvarchar(500)'))),'../@ID=',''),')a','')
		end as [FolderID]
from AssessmentTable (READUNCOMMITTED)
CROSS APPLY AssessmentRules.nodes('PaperRules/Section/Rule') a(b)
CROSS APPLY a.b.nodes('Param/XPath') c(d)

where AssessmentRules.exist('PaperRules/Section/Rule//Param[@Type="7"]') = 1
	and Isvalid = 0
	and AssessmentStatus in (2,3)
	and ExternalReference in ('AGZ_FYS_44035_toetsmatrijs','AGZ_FYS_44036_Anatomie_toetsmatrijs','AGZ_FYS_44037_Fysio_toetsmatrijs','AGZ_MINOR_36601_toetsmatrijs','AGZ_MINOR_Research_toetsmatrijs','AGZ_PTIC_44100_TT2_Anatomy_matrijs','AGZ_PTIC_ 44101_TT2_Psycho_matrijs','AGZ_PTIC_44133_Ortho2_Pathology_mat','AGZ_PTIC_44134_ortho2_Psycho_matrij','AGZ_PTIC_44135 Ort2_research_matrij','AGZ_PTIC_46135_toetsmatrijs','AMM_SP_44883_toetsmatrijs','AMM_SP_47191_toetsmatrijs','AMM_SP_47612_toetsmatrijs','AMM_SPH_42483_toetsmatrijs','AMM SW 47351 toetsmatrijs','AMM SW 47614 Toetsmatrijs','AMM_SPHE_42452_toetsmatrijs','AMM_SPHE_42487_toetsmatrijs','AMM_SPHE_42492_toetsmatrijs','AMM_SPHE_42493_toetsmatrijs','AMM_SPHE_44998_toetsmatrijs','AMM_SPHE_45086_toetsmatrijs','AMM_SPHE_47528_toetsmatrijs','AMM_ SP-SW TZ__44885_Toetsmatrijs','44058_ toetsmatrijs','44059_toetsmatrijs','44060_toetsmatrijs','44069_toetsmatrijs','44071_ toetsmatrijs','AGZ_FYS_44050_Pathologie_matrijs','44070_CNA_Path_toetsmatrijs','AGZ_MINOR_36589_toetsmatrijs','AGZ_MINOR_36840_toetsmatrijs','AGZ_MINOR_36841_toetsmatrijs','AGZ_MINOR_40516_toetsmatrijs','43782 Zorg en Organisatie Matrijs','43783_Zorg_Beroep_lj2_toetsmatrijs','43784 Geneeskunde  Lj 2  Matrijs','AMM SW 44646 Toetsmatrijs','AMM SW 45266 Toetsmatrijs','AGZ_FYS_44061_Ort2_Ana_toetsmatrijs','AGZ_FYS_44062_Ortho2_patho_matrijs','AGZ_FYS_44063_Orth2_fysio_toetsmatr','AGZ_FYS_44072_KT_Int._Anatomie_matr','AGZ_FYS_44073_KT_int._patho_matrijs','AGZ_FYS_44074_int._fysio_toetsmatri','AMM_SP_42324_toetsmatrijs_1314','44026_toetsmatrijs','44027 KT 1 Anatomie_ toetsmatrijs','44028_toetsmatrijs','AGZ_FYS_45226_matrijs','AGZ_FYS_45227_Toetsmatrijs','48220_toetsmatrijs','AGZ_FYS_48234_toetsmatrijs','44095_toetsmatrijs','44126_toetsmatrijs','44128_toetsmatrijs','AGZ_PTIC_44094_toetsmatrijs','AGZ_PTIC_45237_Psycho Social_matrijs','AGZ_PTIC_45249_Research_matrijs')
) A

LEFT JOIN [dbo].[CPFolderPageLookup] CP
on cast(CP.ProjectID as int) = cast(A.ProjectID as int)
	and cast(CP.FolderID as int) = cast(A.FolderID as int)
	--and cp.FolderName = a.FolderName

--LEFT JOIN [dbo].[CPFolderPageLookup] CP2
--on CP2.ProjectID = a.ProjectID

where ISNUMERIC(A.FolderID) = 1
	and A.ExternalReference = 'AGZ_FYS_44035_toetsmatrijs'
	--and cp.FolderID is null
	--and cp2.[ProjectName] is not null



/*

from shared cp

select ID [ProjectID] 
	, Name [ProjectName]
	, a.b.value('data(@ID)[1]','smallint') FolderID
	, a.b.value('data(@Nam)[1]','nvarchar(200)') FolderName
	, c.d.value('data(@ID)[1]','smallint') PageID
from ProjectListTable (READUNCOMMITTED)
CROSS APPLY ProjectStructureXML.nodes('Pro[1]//Fol') a(b)
CROSS APPLY a.b.nodes('Pag') c(d)


*/
