use Saxiondefect1_ItemBank

select count(ID) [count]
	, Isvalid
from AssessmentTable
group by isvalid
order by isvalid

/*
before i run the script
count	Isvalid
3961	0
4127	1
*/

/*
after i run the script
count	Isvalid
3830	0
4258	1
*/

/*
after i run the script #2
count	Isvalid
3769	0
4319	1
*/


SELECT ExternalReference
	, IsValid
	, ID
	, AssessmentStatus
FROM AssessmentTable
where ExternalReference in ('AGZ_FYS_44035_toetsmatrijs','AGZ_FYS_44036_Anatomie_toetsmatrijs','AGZ_FYS_44037_Fysio_toetsmatrijs','AGZ_MINOR_36601_toetsmatrijs','AGZ_MINOR_Research_toetsmatrijs','AGZ_PTIC_44100_TT2_Anatomy_matrijs','AGZ_PTIC_ 44101_TT2_Psycho_matrijs','AGZ_PTIC_44133_Ortho2_Pathology_mat','AGZ_PTIC_44134_ortho2_Psycho_matrij','AGZ_PTIC_44135 Ort2_research_matrij','AGZ_PTIC_46135_toetsmatrijs','AMM_SP_44883_toetsmatrijs','AMM_SP_47191_toetsmatrijs','AMM_SP_47612_toetsmatrijs','AMM_SPH_42483_toetsmatrijs','AMM SW 47351 toetsmatrijs','AMM SW 47614 Toetsmatrijs','AMM_SPHE_42452_toetsmatrijs','AMM_SPHE_42487_toetsmatrijs','AMM_SPHE_42492_toetsmatrijs','AMM_SPHE_42493_toetsmatrijs','AMM_SPHE_44998_toetsmatrijs','AMM_SPHE_45086_toetsmatrijs','AMM_SPHE_47528_toetsmatrijs','AMM_ SP-SW TZ__44885_Toetsmatrijs','44058_ toetsmatrijs','44059_toetsmatrijs','44060_toetsmatrijs','44069_toetsmatrijs','44071_ toetsmatrijs','AGZ_FYS_44050_Pathologie_matrijs','44070_CNA_Path_toetsmatrijs','AGZ_MINOR_36589_toetsmatrijs','AGZ_MINOR_36840_toetsmatrijs','AGZ_MINOR_36841_toetsmatrijs','AGZ_MINOR_40516_toetsmatrijs','43782 Zorg en Organisatie Matrijs','43783_Zorg_Beroep_lj2_toetsmatrijs','43784 Geneeskunde  Lj 2  Matrijs','AMM SW 44646 Toetsmatrijs','AMM SW 45266 Toetsmatrijs','AGZ_FYS_44061_Ort2_Ana_toetsmatrijs','AGZ_FYS_44062_Ortho2_patho_matrijs','AGZ_FYS_44063_Orth2_fysio_toetsmatr','AGZ_FYS_44072_KT_Int._Anatomie_matr','AGZ_FYS_44073_KT_int._patho_matrijs','AGZ_FYS_44074_int._fysio_toetsmatri','AMM_SP_42324_toetsmatrijs_1314','44026_toetsmatrijs','44027 KT 1 Anatomie_ toetsmatrijs','44028_toetsmatrijs','AGZ_FYS_45226_matrijs','AGZ_FYS_45227_Toetsmatrijs','48220_toetsmatrijs','AGZ_FYS_48234_toetsmatrijs','44095_toetsmatrijs','44126_toetsmatrijs','44128_toetsmatrijs','AGZ_PTIC_44094_toetsmatrijs','AGZ_PTIC_45237_Psycho Social_matrijs','AGZ_PTIC_45249_Research_matrijs') 
	and IsValid = 0
	-- ('ACT_CMGT_ANT_45081_Ev.tech_k2_16_17','ACT_CMGT_KNT_44884_EvTec_nl_k2_1617','34658 - toetsmatrijs','34660 - toetsmatrijs','AGZ_30275_toetsmatrijs','AGZ_30277_toetsmatrijs','AGZ_FYS_34663_toetsmatrijs','AGZ_FYS_34665_Toetsmatrijs','AGZ_34670_toetsmatrijs','AGZ_34674_toetsmatrijs','AGZ_34677_toetsmatrijs','AGZ_34679_toetsmatrijs','AGZ_FYS_30266_toetsmatrijs','AGZ_FYS_30268_toetsmatrijs','AGZ_FYS_30270_toetsmatrijs','AGZ_FYS_30273_toetsmatrijs','AGZ_34656_toetsmatrijs','AGZ _FYS_34667_toetsmatrijs','AGZ_FYS_44073_KT_int._patho_matrijs','AGZ_FYS_44074_int._fysio_toetsmatri','AGZ_30284_toetsmatrijs','01 AGP01BA113111','AGZ_MINOR_36601_toetsmatrijs','AGZ_MINOR_36850/37515/37545_2.1_161','AGZ_FYS_MINOR_research_4.1_1617','AGZ_MINOR_Research_toetsmatrijs','AGZ_MINOE_36601_Matrijs_1617','01 AGF01ES150719','01 AGF01ES151055','44751_GT_Minor_operatieve_zorg','3773 Zorg 2 Deeltijd Matrijs','AGZ_43725_toetsmatrijs','43782 Zorg en Organisatie Matrijs','43783_Zorg_Beroep_lj2_toetsmatrijs','43784 Geneeskunde  Lj 2  Matrijs','AGZ_PTIC_34740_4.1_1314','AGZ_PTIC_34740_4.2_1314','AGZ_PTIC_34742_4.1_1415','AGZ_PTIC_34742_4.2_1415','AGZ_PTIC_37908_toetsmatrijs','AGZ_PTIC_37910_toetsmatrijs','AGZ_PTIC_37924_toetsmatrijs','AGZ_PTIC_37926_toetsmatrijs','AGZ_PTIC_37927_toetsmatrijs','AGZ_PTIC_37928_toetsmatrijs','AGZ_PTIC_40720_toetsmatrijs','AGZ_PTIC_40726_toetsmatrijs','AGZ_PTIC_41748_toetsmatrijs','AGZ_PTIC_41751_toetsmatrijs','AGZ_PTIC_41752_toetsmatrijs','AGZ_PTIC_41756_4.1_1516','AGZ_PTIC_41576_4.2_1516','AGZ_PTIC_42935_toetsmatrijs','AGZ_PTIC_ 44101_TT2_Psycho_matrijs','AGZ_PTIC_46135_toetsmatrijs','AMA_3PS_31903_TOETSMATRIJS','AMM_31614_toetsmatrijs_1314','AMM_3PS_31616_toetsmatrijs','AMM_3PS_31617_toetsmatrijs','AMM_3PS_31935_toetsmatrijs','AMM_3PS_38941_toetsmatrijs','AMM_3PS_40370_kw4-1','AMM_3PS_42403_toetsmatrijs','AMM_3PS_42456_IOR2_TOETSMAT','AMM 3PS 43889 CVT 1 Toetsmatrijs','AMM 3PS 43890 Meth. C  Toetsmatrijs','AMM_SP_42324_toetsmatrijs_1314','AMM_SPH_40322_Toetsmatrijs','AMM_SPH-MWD_38819_toetsmatrijs','AMM_SPH_39698_toetsmatrijs','AMM_SPH_42263_toetsmatrijs','AMM_SPH_42264_toetsmatrijs','AMM_SPH_42268_toetsmatrijs','AMM_SPH_42269_toetsmatrijs','AMM_SW_44642_Toetsmatrijs','AMM SW 44645 Toetsmatrijs','AMM SW 47351 toetsmatrijs','Toets Ruud Koopmans','AMM_SPHE_42494_toetsmatrijs','AMM_SPHE_42495_toetsmatrijs','AMM_SPHE_42649_toetsmatrijs','AMM_SPHE_42650_toetsmatrijs','APO_DEV_22505_toetsmatrijs','BTL Test','FEM_Rekentoets_toetsmatrijs','FEM_Taaltoets_toetsmatrijs','FEM_TT_toetsmatrijs_formatief','HBS_FREM_44245_Strat_Bus_Man_23aug','HBS_HBS_46390_KW1_1617','HBS_HBS_46390_KW2_1617','HBS_HBS_46390_KW3_1617','HBS_HBS_46390_KW4_1617_HERKANSING','HBS_HBS_46530_KW4_1617','HBS_HBS_46530_KW4_1617_HERKANSING','HBS_HH_46457_HH_HRM_KW4_1617','HBS_HM_46563_HM_HRM_1617_K4','HBS_HM_46563_HM_HRM_ExtraKans','HBS_HM_46563_KW1_1617','HBS_HM_46563_KW2_1617','HBS_HTRO_42967_KW4_1617','HBS_HTRO_48433_KW1_1718','LED LS 12345 demo training toetsmat','LED LS 45334 FO Tools 2017-2018 kans 1','LED LS 45335 Orientatie FO 1617','LED LS 46119 PRB 2016-2017','LED LS 46120 Bedreigingen 17/18 kans 1','LED LS 46173 Chem.f. kans 2 1617','LED LS 46173 Chem.f toetsmat 1617','LED LS 46330 Moleculaire biologie en genomics kans 1 (2017-2018)','MIM _Rekentoets_DEV _02-12-2015','MIM_Rekentoets_toetsmatrijs','MIM_Taaltoets_DEV_toetsmatrijs','MIM_Taaltoets_ENS_toetsmatrijs','FORMAT BTV')
order by 1