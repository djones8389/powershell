declare @exams table(id int, rules xml)

--Get the exams that have folder rules and are invalid and puts them into a table variable
insert into @exams
select ID, AssessmentRules
from AssessmentTable
where IsValid = 0 
	and AssessmentRules.exist('PaperRules/Section/Rule/Param[@Type=7]') = 1

declare @count int

select @count = COUNT(ID) 
from @exams

declare @id int
declare @rules xml

declare @xpaths table (val nvarchar(max), folderId nvarchar(10), projectId nvarchar(10), subFolders bit)

declare @numFolders int
declare @rulesString nvarchar(max)
declare @currentFolderId nvarchar(10)
declare @projectId nvarchar(10)
declare @currentXPath nvarchar(max)
declare @subFolders bit
declare @newFolderId bigint
declare @newXPath nvarchar(max)

--Nasty loop through the table, taking the top one each time then deleting it
while @count > 0
      begin
            select top 1 @id = ID, @rules = rules
            from @exams
            order by id asc
            
            delete @exams
            where id = @id
            
            --Selects the folder xpath values which can then be 
            insert into @xpaths
            (val)
            select y.value('(XPath/text())[1]', 'nvarchar(max)')
            from @rules.nodes('PaperRules/Section/Rule/Param[@Type=7]') x(y)
            
            --Selects the folder and project IDs from the XPaths, with a check for XPath that includes sub-folders
            update @xpaths
            set folderId = 
                  case
                        when CHARINDEX('(../', val, 0) = 1
                        then SUBSTRING(val, 9, CHARINDEX(')and(', val, 0) - 9)
                        else SUBSTRING(val, 18, CHARINDEX(')and(', val, 0) - 18)
                  end,
                  projectId = 
                  SUBSTRING(val, CHARINDEX('::P/@ID=', val, 0) + 8, LEN(val) - CHARINDEX('::P/@ID=', val, 0) - 8),
                  subFolders =
                  case 
                        when CHARINDEX('(../', val, 0) = 1
                        then 0
                        else 1
                  end
            from @xpaths
            
            select @numFolders = COUNT(val)
            from @xpaths
            
            set @rulesString = CONVERT(nvarchar(max), @rules)
            
            --Loops through the folder references to fix each one
            while @numFolders > 0
                  begin
                        set @numFolders = @numFolders - 1
                        
                        select top 1 
                              @currentFolderId = folderid,
                              @projectId = projectId,
                              @currentXPath = val,
                              @subFolders = subFolders
                        from @xpaths
                        
                        delete @xpaths
                        where
                              folderId = @currentFolderId
                              and
                              projectId = @projectId
                              and
                              val = @currentXPath
                              and
                              subFolders = @subFolders
                              
                        --Only updates if it can find a folder whose CP ID matches the specified ID and is in the correct subject
                        if(exists(select i.Id
                                    from Saxiondefect1_ContentAuthor..Items i
                                    inner join Saxiondefect1_ContentAuthor..Subjects s
                                    on i.SubjectId = s.Id
                                    where
                                          s.ProjectId = CONVERT(bigint, @projectId)
                                          and 
                                          i.ContentProducerItemId = @currentFolderId
                                          and
                                          i.[Type] = 1))
                              begin
                                    --First check that there isn't a folder whose IA ID matches the one in the XPath. If there is, we won't know which is correct
                                    if(exists(select i.Id
                                          from Saxiondefect1_ContentAuthor..Items i
                                          inner join Saxiondefect1_ContentAuthor..Subjects s
                                          on i.SubjectId = s.Id
                                          where
                                                s.ProjectId = CONVERT(bigint, @projectId)
                                                and 
                                                i.id = @currentFolderId
                                                and
                                                i.[Type] = 1))
                                          begin
                                                select 'Problem with ', @id
                                          end
                                    else
                                          begin
                                                --Get the ID of the folder it should be pointing at
                                                select @newFolderId = i.Id
                                                from Saxiondefect1_ContentAuthor..Items i
                                                inner join Saxiondefect1_ContentAuthor..Subjects s
                                                on i.SubjectId = s.Id
                                                where
                                                      s.ProjectId = CONVERT(int, @projectId)
                                                      and 
                                                      i.ContentProducerItemId = @currentFolderId
                                                      and
                                                      i.[Type] = 1
                                                
                                                --Create the amended XPath, which uses the correct folder ID
                                                if @subFolders = 1
                                                      begin
                                                            set @newXPath = '(ancestor::F/@ID=' + convert(nvarchar(10), @newFolderId) + ')and(ancestor::P/@ID=' + @projectId + ')'
                                                      end
                                                else
                                                      begin
                                                            set @newXPath = '(../@ID=' + convert(nvarchar(10), @newFolderId) + ')and(ancestor::P/@ID=' + @projectId + ')'
                                                      end
                                                
                                                --Replace the XPath in the rules XML      
                                                set @rulesString = REPLACE(@rulesString, @currentXPath, @newXPath)
                                          end
                              end
                        
                  end
            
            --Update the rules XML for the exam, and flag it for validation
            update AssessmentTable
            set AssessmentRules = CONVERT(xml, @rulesString), RequiresValidation = 1
            where @id = id
            
            
            set @count = @count - 1
      end   
