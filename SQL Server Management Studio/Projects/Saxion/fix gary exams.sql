use saxion_itembank
--drop table ##itemstoadd 
SELECT distinct 
	--SUBSTRING(REPLACE(REPLACE(cast(a.b.query('.') as NVARCHAR(MAX)), '<XPath>@ID="',''), '"</XPath>',''), 0, CHARINDEX('P', REPLACE(REPLACE(cast(a.b.query('.') as NVARCHAR(MAX)), '<XPath>@ID="',''), '"</XPath>',''))) as [ProjectID]
	 REPLACE(REPLACE(cast(a.b.query('.') as NVARCHAR(MAX)), '<XPath>@ID="',''), '"</XPath>','') ItemID
	 , ContentProducerItemId
	, QualificationName
	, AssessmentName
	, ExternalReference
--into ##itemstoadd
FROM AssessmentTable AT (READUNCOMMITTED)
		
CROSS APPLY AssessmentRules.nodes('PaperRules//XPath') a(b)

INNER JOIN AssessmentGroupTable AGT  (READUNCOMMITTED)
on AGT.ID = AT.AssessmentGroupID

INNER JOIN QualificationTable QT (READUNCOMMITTED)
ON QT.ID = AGT.QualificationID

left join (
	select projectid
		 ,  a.b.value('data(@ID)[1]','nvarchar(20)') ptitemid
	from projecttable (READUNCOMMITTED)
	cross apply projectstructure.nodes('P//I') a(b)
) pt
on pt.projectid = SUBSTRING(REPLACE(REPLACE(cast(a.b.query('.') as NVARCHAR(MAX)), '<XPath>@ID="',''), '"</XPath>',''), 0, CHARINDEX('P', REPLACE(REPLACE(cast(a.b.query('.') as NVARCHAR(MAX)), '<XPath>@ID="',''), '"</XPath>','')))
	and REPLACE(REPLACE(cast(a.b.query('.') as NVARCHAR(MAX)), '<XPath>@ID="',''), '"</XPath>','') = ptitemid
--cross apply projectstructure.nodes('P//I') a(b)

inner join (
		select ProjectId
			, ContentProducerItemId, s.Title, (cast(ProjectId as varchar(10)) + 'P' + cast(ContentProducerItemId as varchar(10))) ItemID, i.PublishStatus, i.id
		from Saxion_ContentAuthor.dbo.Items i
		inner join Saxion_ContentAuthor.dbo.Subjects S
		on S.id = i.subjectid
		where ProjectId in (2562,2569) 
		 --and i.id in (7876,7880)	
		
) CA
on cast(ca.projectid  as nvarchar(10)) + 'p' + cast(ca.id as nvarchar(10)) =  REPLACE(REPLACE(cast(a.b.query('.') as NVARCHAR(MAX)), '<XPath>@ID="',''), '"</XPath>','')
where AT.IsValid = 0
	and REPLACE(REPLACE(cast(a.b.query('.') as NVARCHAR(MAX)), '<XPath>@ID="',''), '"</XPath>','') not like '<XPath>%'
	--and at.AssessmentStatus = 2
	and ptitemid is null
	and SUBSTRING(REPLACE(REPLACE(cast(a.b.query('.') as NVARCHAR(MAX)), '<XPath>@ID="',''), '"</XPath>',''), 0, CHARINDEX('P', REPLACE(REPLACE(cast(a.b.query('.') as NVARCHAR(MAX)), '<XPath>@ID="',''), '"</XPath>',''))) = 2562
	and externalreference in ('AGZ_PTIC_48282_1.1_1717','AGZ_PTIC_her_48282_1.2_1718','AGZ_PTIC_45236_1.2_1718','AGZ_FYS_her_45226_1.2_1718')
	order by 1
