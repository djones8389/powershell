SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

use ICAEW_SecureAssess

select 
	case cast(examState as varchar(3)) when 6 then 'In Progress' when 13 then 'Archived' else cast(examState as varchar(3)) end as ExamState
    , KeyCode
	 , IB.QualificationName
	 , ExamName
	 , CASE WHEN TAG LIKE '%Additional Exam Time%' then 'Yes' else 'No' end as [UsedSTC?]
	 , StructureXML.value('data(assessmentDetails/scheduledDuration/value)[1]','smallint') - StructureXML.value('data(assessmentDetails/defaultDuration)[1]','smallint') [ExtraTime]
	 , StructureXML.value('data(assessmentDetails/defaultDuration)[1]','smallint') DefaultDuration
	 , StructureXML.value('data(assessmentDetails/scheduledDuration/value)[1]','smallint') ScheduledDuration
	 , ESCAT.TimeInExam
	 , tag [StopTheClockInfo]
	 --,UnscheduledBreakMinUsed
	 --, StructureXML
from ExamSessionTable EST
INNER JOIN ExamSessionTagReferenceTable ETR
on ETR.ExamSessionId = EST.ID
INNER JOIN ExamSessionTagLookupTable ETL 
on ETL.Id = ETR.ExamSessionTagId
--INNER JOIN ExamStateLookupTable ESLT
--on ESLT.ID = examState
INNER JOIN (
	select a.ExamSessionID
		, DATEDIFF(MINUTE, State6, State9) TimeInExam
	FROM (
	select ExamSessionID
		, min(StateChangeDate) State6
		, max(StateChangeDate) State9
	from ExamStateChangeAuditTable
	where --ExamSessionID = 20792	
		 NewState in (6,9)
		and StateChangeDate between '2017-12-05 00:00:00.001' and getdate()
		group by ExamSessionID
	) A
) ESCAT
on ESCAT.ExamSessionID = EST.ID

INNER JOIN ScheduledExamsTable SCET
on SCET.Id = EST.ScheduledExamID
INNER JOIN CentreTable CT
on CT.ID = SCET.CentreID
INNER JOIN IB3QualificationLookup IB
on IB.ID = qualificationId
--where escat.StateChangeDate between '2017-12-05 00:00:00.001' and getdate()
	--where TAG LIKE '%Additional Exam Time%'
--	and est.id = 20792
order by est.ID

	--where keycode IN ('37JBVDF6','3PDTD4F6')
--WHERE TAG LIKE '%Additional Exam Time%'


/*
select a.ExamSessionID
	, DATEDIFF(MINUTE, State6, State9)
FROM (
select ExamSessionID
	, min(StateChangeDate) State6
	, max(StateChangeDate) State9
from ExamStateChangeAuditTable
where ExamSessionID = 20792	
	and NewState in (6,9)
	group by ExamSessionID
) A
*/