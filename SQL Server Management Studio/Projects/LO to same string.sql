create table #test (
	itemid nvarchar(10)
	, LO nvarchar(10)
)

insert #test
values('123P456','MyLo1'), ('123P456','MyLo2')

declare @dynamic nvarchar(max) = ''

select @dynamic = (

select REVERSE((
	select SUBSTRING(
	reverse((select LO + ', ' 
			from #test
			for xml path(''))), CHARINDEX(',',reverse((select LO + ', ' 
			from #test
			for xml path(''))))+1,100)))

			)

select LEN(@dynamic)  --Shows as 12, accurate, not too long a string, no whitespace
SELECT(@dynamic)



--select SUBSTRING((select LO + ',' 
--		from #test
--		for xml path('')), 0, CHARINDEX(',', reverse((select LO + ',' 
--		from #test
--		for xml path('')))))

--select
--reverse((select LO + ',' 
--		from #test
--		for xml path('')))

--select SUBSTRING(
	
--		(select LO + ',' 
--		from #test
--		for xml path('')), 0, 10)