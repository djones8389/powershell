select qualificationname, AssessmentName, ExternalReference
from [Saxion_ItemBank].dbo.AssessmentTable at
inner join [Saxion_ItemBank].dbo.AssessmentGroupTable agt on agt.id = at.assessmentgroupid
inner join [Saxion_ItemBank].dbo.Qualificationtable qt on qt.id = agt.qualificationid
where at.isvalid = 0
except
select qualificationname, AssessmentName, ExternalReference
from [SANDBOX_Saxion_ItemBank].dbo.AssessmentTable at
inner join [SANDBOX_Saxion_ItemBank].dbo.AssessmentGroupTable agt on agt.id = at.assessmentgroupid
inner join [SANDBOX_Saxion_ItemBank].dbo.Qualificationtable qt on qt.id = agt.qualificationid
where  at.isvalid = 0

