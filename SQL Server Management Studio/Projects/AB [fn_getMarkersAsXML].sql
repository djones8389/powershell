USE [SurpassLocal3]
GO

/****** Object:  UserDefinedFunction [dbo].[fn_getMarkersAsXML]    Script Date: 18/07/2017 15:33:07 ******/
DROP FUNCTION [dbo].[fn_getMarkersAsXML]
GO

/****** Object:  UserDefinedFunction [dbo].[fn_getMarkersAsXML]    Script Date: 18/07/2017 15:33:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[fn_getMarkersAsXML] (@examSessionId INT)
RETURNS XML
AS
BEGIN

	-- filter out exams that have not been human marked first
	IF	(
			(SELECT COUNT(*) FROM  [WAREHOUSE_ExamSessionItemResponseTable]
			WHERE Convert(nvarchar(max),[MarkerResponseData].query('count(/entries/entry/userId)')) != '0'
			AND [WAREHOUSEExamSessionId] = @examSessionId)
			= 0
		)
		BEGIN
			RETURN NULL
		END

	DECLARE @Return nvarchar(max)

	SELECT @Return = COALESCE(@Return + '</id><id>', '<markers><id>') + CONVERT(varchar(max),[MarkerResponseData].query('/entries/entry[last()]/userId/text()'))
	FROM [WAREHOUSE_ExamSessionItemResponseTable]
	WHERE [WAREHOUSEExamSessionID] = @examSessionId
	AND Convert(nvarchar(max),[MarkerResponseData].query('count(/entries/entry/userId)')) != '0'

	SET @Return = @Return + '</id></markers>'

	DECLARE @myMarkersXml	xml
	SET @myMarkersXml = @Return

	SET @Return = NULL
 
	SELECT @Return= coalesce(@Return + '', '') + '<marker><id>' + Convert(nvarchar(max),[UserTable].[id]) + '</id><userName>' + Username + '</userName><surname>' + surname + '</surname><firstName>' + forename + '</firstName></marker>'
	FROM [UserTable]
		INNER JOIN
	(SELECT DISTINCT
	ParamValues.ID.value('.','int') as 'id'
	FROM @myMarkersXml.nodes('/markers/id') as ParamValues(ID)) AS dMarkerIds
		ON dMarkerIds.id = [UserTable].[id]
	SET @return = '<markers>' + @return + '</markers>'
	SET @myMarkersXML = @return
	RETURN (@myMarkersXML)
END
GO


