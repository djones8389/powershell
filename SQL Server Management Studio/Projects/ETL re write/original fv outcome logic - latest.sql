;WITH MarkerMarks 
AS 
	(SELECT 
		fmr.examSessionKey, 
		fmr.CPID,  
		CASE WHEN ISNULL(dq.TotalMark, 0) = 0 
			THEN 0 
			ELSE fmr.AssignedMark/dq.TotalMark 
		END 
		AS mark
	FROM
	-- the below sub query ensures record with higest seqno is returned where there are multiple seqno per examSessionKey --
		(SELECT 
			examSessionKey, 
			CPID, 
			SEQNO, 
			AssignedMark, 
			seq = ROW_NUMBER() OVER (PARTITION BY cpid, examSessionKey ORDER BY seqno DESC)
		FROM 
			dbo.FactMarkerResponse) fmr
		INNER JOIN 
			dbo.DimQuestions dq ON dq.cpid = fmr.cpid
		WHERE 
			fmr.seq = 1
			and dq.cpid = '869P3078')
select
	 ISNULL (fv.FacilityValue,0.5)
FROM 
	dbo.DimQuestions dq
INNER JOIN
	(SELECT 
		dq.CPID, 
		AVG(ISNULL(mm.Mark, fqr.Mark)) as FacilityValue
	FROM
		dbo.DimQuestions dq 
	JOIN
		dbo.FactQuestionResponses fqr ON dq.cpid = fqr.cpid
	JOIN 
		dbo.FactExamSessions fes on fes.ExamSessionKey = fqr.ExamSessionKey
	LEFT OUTER JOIN 
		MarkerMarks mm ON fqr.ExamSessionKey = mm.ExamSessionKey AND fqr.cpid = mm.cpid
	WHERE 
		fqr.RawResponse IS NOT NULL
		and dq.cpid = '869P3078'
		--AND fes.ExcludeFromReporting <> 1
	GROUP BY
		dq.cpid) fv 
ON fv.cpid = dq.cpid


SELECT FacilityValue
FROM DimQuestions
where cpid = '869P3078'