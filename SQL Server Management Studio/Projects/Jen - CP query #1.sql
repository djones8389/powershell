SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

if OBJECT_ID('tempdb..#pageIDs') is not null drop table #pageIDs;

CREATE TABLE #pageIDs (
	ItemID NVARCHAR(20)
);

INSERT #pageIDs
select 
	cast(Projectlisttable.id as varchar(5))+ 'P' + project.page.value('@ID','nvarchar(12)') as PageID
from Projectlisttable

cross apply ProjectStructureXml.nodes('Pro//Pag') project(page)

INNER JOIN PageTable 
on PageTable.ParentID = Projectlisttable.id
	AND PageTable.ID = cast(Projectlisttable.ID as nvarchar(10)) + 'P' + cast(project.page.value('@ID','nvarchar(12)') as nvarchar(10))

where project.page.value('@sta','int') Between PublishSettingsXML.value('(/PublishConfig/StatusLevelFrom)[1]','int') and PublishSettingsXML.value('(/PublishConfig/StatusLevelTo)[1]','int')

EXCEPT

select 
	cast(Projectlisttable.id as varchar(5))+ 'P' + project.page.value('@ID','nvarchar(12)') as PageID
from Projectlisttable

cross apply ProjectStructureXml.nodes('Pro/Rec//Pag') project(page)

INNER JOIN PageTable 
on PageTable.ParentID = Projectlisttable.id
	AND PageTable.ID = cast(Projectlisttable.ID as nvarchar(10)) + 'P' + cast(project.page.value('@ID','nvarchar(12)') as nvarchar(10))

where project.page.value('@sta','int') Between PublishSettingsXML.value('(/PublishConfig/StatusLevelFrom)[1]','int') and PublishSettingsXML.value('(/PublishConfig/StatusLevelTo)[1]','int')


SELECT  db_name() DBName
	, PLT.ID ProjectID
	, PLT.Name ProjectName
	, A.ext
	, COUNT(distinct A.ItemID) NumPages
	, (SELECT COUNT(1) FROM #pageIDs) TotalCount
FROM (
select  p.ID ItemID
	   , ext
from ItemCustomQuestionTable i
INNER JOIN ComponentTable c on i.ParentID = c.ID
INNER JOIN SceneTable s on s.ID = c.ParentID
INNER JOIN PageTable p on p.id = s.Parentid
INNER JOIN #pageIDs a on a.ItemID = p.id
INNER JOIN ProjectListTable pr on pr.ID = p.ParentID
where ext in ('barchart', 'piechart', 'scattergraph', 'selfDraw_lineGraph', 'proof_reading')

UNION ALL

select SUBSTRING(ID, 0, CHARINDEX('S',ID)) ItemID
	, 'TextSelector'
from ItemTextSelectorTable
where SUBSTRING(ID, 0, CHARINDEX('S',ID)) in (select itemid from #pageIDs)

UNION ALL

select SUBSTRING(ID, 0, CHARINDEX('S',ID)) ItemID
	, 'DragAndDrop'
from [dbo].[ItemDragAndDropTable]
where SUBSTRING(ID, 0, CHARINDEX('S',ID)) in (select itemid from #pageIDs)
) A
INNER JOIN ProjectListTable PLT
on PLT.ID = SUBSTRING(A.ItemID, 0, CHARINDEX('P',A.ItemID))

GROUP BY
		PLT.ID
		, PLT.Name
		, A.ext
order by 1;