use Windesheim_SecureAssess

select keycode
	, a.b.value('data(@id)[1]','nvarchar(100)') ItemID
	, a.b.value('data(@nonScored)[1]','bit') IsNonScored
from warehouse_examsessiontable_shreded wests WITH(NOLOCK)
inner join WAREHOUSE_ExamSessionTable west WITH (NOLOCK)
on west.id = wests.examsessionid
cross apply west.resultdata.nodes('exam/section/item') a(b)
where examname = '74797'
--	and a.b.value('data(@nonScored)[1]','bit') = '1'
