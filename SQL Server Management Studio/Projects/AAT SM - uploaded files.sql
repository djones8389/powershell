IF OBJECT_ID('tempdb..#FileCompare') IS NOT NULL
	DROP TABLE #FileCompare;

SELECT
	 e.NAME [Assessment Name]
	, ev.NAME [Assessment Version Name]
	, ev.ExternalExamReference [Assessment Version Reference]
	, Keycode
	, i.ExternalItemID [ItemID]
	, i.ExternalItemName [ItemName]
	, urd.FileName [Full file name]
	, RIGHT(urd.FileName, 4) [Extension]
	, DATALENGTH(Blob) DL
	, urd.UniqueResponseID
INTO #FileCompare
FROM [UploadedFilesDetails] ufd
INNER JOIN [UniqueResponseDocuments] urd ON urd.ID = ufd.UniqueResponseDocumentID
INNER JOIN UniqueResponses ur ON ur.id = urd.UniqueResponseID
INNER JOIN CandidateResponses cr ON cr.UniqueResponseID = ur.id
INNER JOIN CandidateExamVersions cev ON cev.id = cr.CandidateExamVersionID
INNER JOIN dbo.ExamVersions ev ON ev.ID = CEV.ExamVersionID
INNER JOIN dbo.Exams e ON e.ID = ev.ExamID
INNER JOIN dbo.Qualifications q ON q.ID = e.QualificationID
INNER JOIN Items I ON i.ExamID = e.ID
	AND i.ID = ur.itemId
--where Keycode = '3NX6MNA6'



select *
from #FileCompare
where [Full file name] in ('Test4doc - Copy (3).docx','Test2doc - Copy.docx')

select *
	,urd.UniqueResponseID
	, fc.UniqueResponseID
from #FileCompare fc
inner join UniqueResponseDocuments urd
on  fc.[Full file name] = urd.FileName
	and fc.DL = DATALENGTH(urd.Blob)
inner join UniqueResponses ur
on ur.id = urd.UniqueResponseID
inner join CandidateResponses cr
on cr.UniqueResponseID = ur.id
inner join CandidateExamVersions cev
on cev.ID = cr.CandidateExamVersionID
where urd.UniqueResponseID != fc.UniqueResponseID