;with cte as (
SELECT 
     [Instance]
      ,[Drive]
      ,[Type]
      ,[MB Free]
      ,[StartDate] [CollectionDate]
	  , rank () OVER (PARTITION BY Instance order by StartDate desc) R
  FROM [PSCollector].[dbo].[DiskSpace]
       Where [StartDate] > '2017-01-01'
       and Instance in ('430326-BC-SQL2\SQL2') --430088-BC-SQL\SQL1'
), cte2 as (
	select Instance
		, Drive
		, Type
		, [MB Free]
		, CollectionDate
	from cte
	where R IN (
		select top 5 R
		from (
		select distinct R from cte
		) a
	)
)

SELECT DISTINCT
	  Instance
	, Drive
	, SUBSTRING((
			SELECT ', ' + [Type]  
			FROM cte2 
			WHERE Instance = Results.Instance
				and Drive = Results.Drive
				and [MB Free] = Results.[MB Free]
				and CollectionDate = Results.CollectionDate
			FOR XML PATH('')
		), 2,1000) [Purpose]
	, [MB Free]
	, CollectionDate	
FROM cte2 Results
GROUP BY Instance
	, Drive
	, Type
	, [MB Free]
	,CollectionDate

	order by CollectionDate DESC
		, [MB Free] ASC