CREATE TABLE InstanceLookUpTable (
	InstanceName nvarchar(150)
	,ClientName nvarchar(150)
);
GO
--truncate table InstanceLookUpTable
INSERT InstanceLookUpTable
VALUES ('311619-WEB3','WJEC'),('335657-APP1','SQA'),('335658-APP2','SQA OA'),('338301-WEB4','Skillsfirst'),('343553-WEB5','AQA SA')
	,('403252-ACTVARIE','Actuaries'),('407710-WEB3','NCFE'),('408302-AQAREPOR','AQA'),('420304-EAL2-LIV','EAL')
	,('425732-OCR-SQL\SQL1','OCR DB1'),('430069-AAT-SQL\SQL1','AAT DB1'),('430088-BC-SQL\SQL1','BC DB1'),('430325-OCR-SQL2\SQL2','OCR DB2')
	,('430326-BC-SQL2\SQL2','BC DB2'),('430327-AAT-SQL2\SQL2','AAT DB2')
		,('524778-SQL','Editions Mirror'),('553092-PRDSQL','Editions Live'),('704276-PREVSTA1','Prp-Prv')
GO
create clustered index [IX_ClientName] on [InstanceLookUpTable] (ClientName);


select * from InstanceLookUpTable