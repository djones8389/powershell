USE [PSCollector]
GO

ALTER TABLE [dbo].[DBGrowthMetrics] SET (SYSTEM_VERSIONING = OFF)
GO
DROP TABLE [dbo].[DBGrowthMetricsHistory]
GO
DROP TABLE [dbo].[DBGrowthMetrics]
GO

CREATE TABLE [dbo].[DBGrowthMetrics](
	[ID] uniqueIdentifier PRIMARY KEY CLUSTERED ,
	[server_name] [nvarchar](100) NULL,
	[database_name] [nvarchar](100) NULL,
	[table_name] [nvarchar](100) NULL,
	[rows] [int] NULL,
	[reserved_kb] [nvarchar](100) NULL,
	[data_kb] [nvarchar](100) NULL,
	[index_size] [nvarchar](100) NULL,
	[unused_kb] [nvarchar](100) NULL,
	StartDate [datetime2](2) GENERATED ALWAYS AS ROW START NOT NULL,
	EndDate [datetime2](2) GENERATED ALWAYS AS ROW END NOT NULL,
	 PERIOD FOR SYSTEM_TIME (StartDate,EndDate)  
	)
WITH (
	SYSTEM_VERSIONING = ON ( HISTORY_TABLE = [dbo].[DBGrowthMetricsHistory] )
)
GO


