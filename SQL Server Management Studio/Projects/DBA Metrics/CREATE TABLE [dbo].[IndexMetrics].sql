USE [PSCollector]
GO

ALTER TABLE [dbo].[IndexMetrics] SET (SYSTEM_VERSIONING = OFF)
GO
DROP TABLE [dbo].[IndexMetricsHistory]
GO
DROP TABLE [dbo].[IndexMetrics]
GO

CREATE TABLE [dbo].[IndexMetrics](
	[ID] uniqueidentifier NOT NULL,
	[Instance] [nvarchar](500) NULL,
	[database] [nvarchar](500) NULL,
	[table] [nvarchar](500) NULL,
	[index] [nvarchar](500) NULL,
	[user_updates] [int] NULL,
	[last_user_update] [datetime] NULL,
	[user_seeks] [int] NULL,
	[user_scans] [int] NULL,
	[user_lookups] [int] NULL,
	[last_user_seek] [datetime] NULL,
	[last_user_scan] [datetime] NULL,
	[last_user_lookup] [datetime] NULL,
	[collectionDate] [datetime] NULL,
	StartDate [datetime2](2) GENERATED ALWAYS AS ROW START NOT NULL,
	EndDate [datetime2](2) GENERATED ALWAYS AS ROW END NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
	PERIOD FOR SYSTEM_TIME (StartDate, EndDate)
) ON [PRIMARY]
WITH
(
SYSTEM_VERSIONING = ON ( HISTORY_TABLE = [dbo].[IndexMetricsHistory] )
)

GO

