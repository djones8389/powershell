USE [PSCollector]
GO

ALTER TABLE [dbo].[FN_Instance_DBs_Status] SET (SYSTEM_VERSIONING = OFF)
GO
DROP TABLE [dbo].[FN_Instance_DBs_StatusHistory]
GO
DROP TABLE [dbo].[FN_Instance_DBs_Status]
GO

CREATE TABLE [FN_Instance_DBs_Status] (
	[ID] uniqueidentifier PRIMARY KEY CLUSTERED,
	[Instance] [nvarchar](500) NULL,
	[DatabaseName] [nvarchar](500) NULL,
	[status] nvarchar(100),	
	StartDate [datetime2](2) GENERATED ALWAYS AS ROW START NOT NULL,
	EndDate [datetime2](2) GENERATED ALWAYS AS ROW END NOT NULL,
	PERIOD FOR SYSTEM_TIME (StartDate,EndDate)  
)WITH (
	SYSTEM_VERSIONING = ON ( HISTORY_TABLE = [dbo].[FN_Instance_DBs_StatusHistory] )
)
GO

