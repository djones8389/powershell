USE [PSCollector]
GO

ALTER TABLE [dbo].[FN_FullGrowthStats] SET (SYSTEM_VERSIONING = OFF)
GO
DROP TABLE [dbo].[FN_FullGrowthStats_History]
GO
DROP TABLE [dbo].[FN_FullGrowthStats]
GO

CREATE TABLE [FN_FullGrowthStats] (
	[ID] uniqueidentifier PRIMARY KEY CLUSTERED,
	[Instance] [nvarchar](500) NULL,
	[DatabaseName] [nvarchar](500) NULL,
	[TableName] [nvarchar](500) NULL,
	reserved_kb float,
	data_kb float,
	index_size float,
	unused_kb float,	
	StartDate [datetime2](2) GENERATED ALWAYS AS ROW START NOT NULL,
	EndDate [datetime2](2) GENERATED ALWAYS AS ROW END NOT NULL,
	PERIOD FOR SYSTEM_TIME (StartDate,EndDate)  
)WITH (
	SYSTEM_VERSIONING = ON ( HISTORY_TABLE = [dbo].[FN_FullGrowthStats_History] )
)
GO


