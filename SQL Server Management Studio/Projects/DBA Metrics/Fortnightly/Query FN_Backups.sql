USE PSCollector

SELECT A.[Count]
	, ClientName
FROM (
SELECT count([ID]) [Count]
	, b.ClientName
FROM [PSCollector].[dbo].[FN_Backups] A
inner join [dbo].[InstanceLookUpTable] B
on a.Instance = b.InstanceName
where cast(StartDate as date) = '2017-10-30'
group by rollup(B.ClientName)
) A
order by 2


--DateLookup
select distinct cast(StartDate as date) [CollectionDate]
from [FN_Backups]