USE [PSCollector]
GO

ALTER TABLE [dbo].[FN_Editions Deployments_Client Versions] SET (SYSTEM_VERSIONING = OFF)
GO
DROP TABLE [dbo].[FN_Editions Deployments_Client Versions_History]
GO
DROP TABLE [dbo].[FN_Editions Deployments_Client Versions]
GO

CREATE TABLE [FN_Editions Deployments_Client Versions] (
	[ID] uniqueidentifier PRIMARY KEY CLUSTERED,
	[Instance] [nvarchar](500) NULL,
	[ClientName] [nvarchar](500) NULL,
	[ApplicationName] [nvarchar](500) NULL,
	DeploymentType char(2),
	[CreatedDate] datetime,
	[Error] [nvarchar](500) NULL,
	[Version] nvarchar(100),
	[IntVersion] nvarchar(100),	
	StartDate [datetime2](2) GENERATED ALWAYS AS ROW START NOT NULL,
	EndDate [datetime2](2) GENERATED ALWAYS AS ROW END NOT NULL,
	PERIOD FOR SYSTEM_TIME (StartDate,EndDate)  
)WITH (
	SYSTEM_VERSIONING = ON ( HISTORY_TABLE = [dbo].[FN_Editions Deployments_Client Versions_History] )
)
GO


