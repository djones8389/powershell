USE [PSCollector]
GO

ALTER TABLE [dbo].[FN_MB_Data_Used] SET (SYSTEM_VERSIONING = OFF)
GO
DROP TABLE [dbo].[FN_MB_Data_Used_History]
GO
DROP TABLE [dbo].[FN_MB_Data_Used]
GO

CREATE TABLE [FN_MB_Data_Used] (
	[ID] uniqueidentifier PRIMARY KEY CLUSTERED,
	[Instance] [nvarchar](500) NULL,
	[DatabaseName] [nvarchar](500) NULL,
	 UsedSpace_MB float,	
	StartDate [datetime2](2) GENERATED ALWAYS AS ROW START NOT NULL,
	EndDate [datetime2](2) GENERATED ALWAYS AS ROW END NOT NULL,
	PERIOD FOR SYSTEM_TIME (StartDate,EndDate)  
)WITH (
	SYSTEM_VERSIONING = ON ( HISTORY_TABLE = [dbo].[FN_MB_Data_Used_History] )
)
GO


