USE [PSCollector]
GO

ALTER TABLE [dbo].[FN_Restores] SET (SYSTEM_VERSIONING = OFF)
GO
DROP TABLE [dbo].[FN_RestoresHistory]
GO
DROP TABLE [dbo].[FN_Restores]
GO

CREATE TABLE [FN_Restores] (
	[ID] uniqueidentifier PRIMARY KEY CLUSTERED,
	[Instance] [nvarchar](500) NULL,
	[DatabaseName] [nvarchar](500) NULL,
	[restore_date] [datetime] NULL,
	[user_name] sysname,	
	StartDate [datetime2](2) GENERATED ALWAYS AS ROW START NOT NULL,
	EndDate [datetime2](2) GENERATED ALWAYS AS ROW END NOT NULL,
	PERIOD FOR SYSTEM_TIME (StartDate,EndDate)  
)WITH (
	SYSTEM_VERSIONING = ON ( HISTORY_TABLE = [dbo].[FN_RestoresHistory] )
)
GO


