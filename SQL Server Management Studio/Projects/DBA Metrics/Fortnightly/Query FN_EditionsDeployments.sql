use [PSCollector]
SELECT 
      b.ClientName [Instance]
      , case cast(deploymenttype as varchar(2))
		when cast(1 as varchar(2)) then 'Delete'
		when cast(2 as varchar(2)) then 'Update'
		when cast(13 as varchar(2)) then 'Clone'
		when cast(9 as varchar(2)) then 'Apply Features'
		else cast(DeploymentType as varchar(2)) 
		end as DeploymentType
      --, cast([CreatedDate] as date) [ActionDate]
	  , count([ID]) [Count]
  FROM [dbo].[FN_Editions Deployments_Client Versions] A
  INNER JOIN InstanceLookUpTable b 
  on a.Instance=b.InstanceName
  where cast(StartDate as date) = '2017-10-30'
  group by b.ClientName
      ,[DeploymentType]
	  --,cast([CreatedDate] as date)
  order by 1,2,3

