USE [PSCollector]
GO

ALTER TABLE [dbo].[Sproc Metrics] SET (SYSTEM_VERSIONING = OFF)
GO
DROP TABLE [SprocMetricsHistory]
GO
DROP TABLE [Sproc Metrics]
GO

CREATE TABLE [dbo].[Sproc Metrics] (
	[ID] uniqueidentifier PRIMARY KEY CLUSTERED,
	[Instance] [nvarchar] (500) NULL,
	[DBName] [nvarchar] (500) NULL,
	[SCHEMA_NAME] [nvarchar](500) NULL,
	[OBJECT_NAME] [nvarchar](500) NULL,
	[cached_time] [datetime] NULL,
	[last_execution_time] [datetime] NULL,
	[execution_count] [bigint] NULL,
	[CPU] [bigint] NULL,
	[ELAPSED] [bigint] NULL,
	[LOGICAL_READS] [bigint] NULL,
	[LOGICAL_WRITES] [bigint] NULL,
	[PHYSICAL_READS] [bigint] NULL,
	StartDate [datetime2](2) GENERATED ALWAYS AS ROW START NOT NULL,
	EndDate [datetime2](2) GENERATED ALWAYS AS ROW END NOT NULL,
	PERIOD FOR SYSTEM_TIME (StartDate,EndDate)  
	)
WITH (
	SYSTEM_VERSIONING = ON ( HISTORY_TABLE = [dbo].[SprocMetricsHistory] )
)
GO
