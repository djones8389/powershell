if OBJECT_ID('tempdb..#stats') is not null drop table #stats;

DECLARE @Min VARCHAR(16); 
DECLARE @Max VARCHAR(16); 

SELECT @Min = MIN(CONVERT(date,[StartDate])) 
	, @Max = MAX(CONVERT(date,[StartDate]))
FROM [PSCollector].[dbo].[DBGrowthMetrics]

create table #stats (

	server_name nvarchar(100)
	,database_name nvarchar(100)
	,table_name nvarchar(100)
	, StartDate date
	, total int
	, data_kb int
	, index_size int
)

INSERT #stats
SELECT 
	A.server_name
	, A.[database_name]
	, A.table_name
	, [StartDate]
	, SUM(a.data_kb) + SUM(a.index_size) [total]
	, SUM(a.data_kb) data_kb
	, SUM(a.index_size) index_size	

FROM (
SELECT 
	server_name
      ,[database_name]
	  ,table_name
      ,cast(replace([data_kb], 'KB','') as float) [data_kb]
      ,cast(replace([index_size], 'KB','') as float) [index_size]
      ,cast(CollectionDate as date) [StartDate]
  FROM [PSCollector].[dbo].[xxDBGrowthMetrics]
  where database_name = 'aat_SecureAssess'
) A
where server_name = '430069-AAT-SQL\SQL1'--'430326-BC-SQL2\SQL2'
group by 
	A.server_name
	, A.[database_name]
	, a.table_name
	, a.StartDate

UNION

SELECT 
	A.Instance
	,A.[database_name]
	,table_name
	,[StartDate]
	, SUM(a.data_kb) + SUM(a.index_size) [total]
	, SUM(a.data_kb) data_kb
	, SUM(a.index_size) index_size
FROM (
SELECT 
	[Instance]
      ,[database_name]
	  ,table_name
      ,cast(replace([data_kb], 'KB','') as float) [data_kb]
      ,cast(replace([index_size], 'KB','') as float) [index_size]
      ,cast([StartDate] as date) [StartDate]
  FROM [PSCollector].[dbo].[DBGrowthMetrics]
  where database_name = 'aat_SecureAssess'
) A

where StartDate in (@Min,@Max)
	and Instance  = '430069-AAT-SQL\SQL1'--'430326-BC-SQL2\SQL2'
group by 
	A.Instance
	, A.[database_name]
	,table_name
	, a.StartDate

--select distinct StartDate from #stats

SELECT [Min].server_name
	, [Min].database_name
	, [Min].table_name
	, [min].StartDate Min_StartDate
	, [max].StartDate Max_StartDate
	, DATEDIFF(day,[min].StartDate, [max].StartDate) [Days]
	, [min].total Min_Total
	, [max].total Max_Total
	, [max].total-[min].total [Diff_Kb]
	
FROM (
select 
	A.server_name
	, a.database_name
	, a.table_name
	, a.total
	, a.StartDate
from #stats a
where StartDate in (SELECT min(startdate) FROM #stats)
) [Min]

INNER JOIN (
select 
	A.server_name
	, a.database_name
	, a.table_name
	, a.total
	, a.StartDate
from #stats a
where StartDate in (SELECT max(startdate) FROM #stats)
) [Max]

on [Min].server_name = [max].server_name 
	and [Min].database_name = [max].database_name 
	and [Min].table_name = [max].table_name 
order by 
	 Diff_Kb desc
	 ,server_name
	, database_name





/*
SELECT A.server_name
	, a.database_name
	, a.table_name
	, a.Min_StartDate
	, a.Max_StartDate
	, DATEDIFF(DAY, Min_StartDate, Max_StartDate) [Days]
	, min.total [Min_Total]
	, max.total [Max_Total]
FROM (
select [server_name]
	, [database_name]
	, table_name
	, min(startDate) Min_StartDate
	, max(startDate) Max_StartDate
from #stats
group by 
	[server_name]
	,[database_name]
	, table_name
) A
INNER JOIN (	
	select [server_name]
		, [database_name]
		, table_name
		 ,startDate
		 ,total
	from #stats 
) [Min]
on [Min].[server_name] = A.[server_name]
	and [Min].[database_name] = A.[database_name]
	and [Min].startDate = A.Min_StartDate

INNER JOIN (	
	select [server_name]
		, [database_name]
		, table_name
		 ,startDate
		 , total
	from #stats 
) [Max]
on [Max].[server_name] = A.[server_name]
	and [Max].[database_name] = A.[database_name]
	and [Max].startDate = A.Max_StartDate
order by 1,2,3






select *, ROW_NUMBER() OVER (PARTITION BY StartDate ORDER BY StartDate) R
into #results
from cte

select
	StartDate
	, [1]
FROM (
        SELECT [R] as [R], data_kb as [data_kb], startdate as [StartDate]
        FROM #results
) AS [S]
PIVOT (
	MAX(data_kb)
	For [R] in ([1]) 
) P



*/
