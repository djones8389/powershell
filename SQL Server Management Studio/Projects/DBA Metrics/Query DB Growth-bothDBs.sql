SELECT 
	A.server_name
	--, A.[database_name]
	, SUM(a.data_kb) data_kb
	, SUM(a.index_size) index_size
	, SUM(a.data_kb) + SUM(a.index_size) [total]
	,[StartDate]
FROM (
SELECT 
	server_name
      ,[database_name]
      ,cast(replace([data_kb], 'KB','') as float) [data_kb]
      ,cast(replace([index_size], 'KB','') as float) [index_size]
      ,cast(CollectionDate as date) [StartDate]
  FROM [PSCollector].[dbo].[xxDBGrowthMetrics]
) A

where server_name = '430088-BC-SQL\SQL1'
	--and StartDate in ('2017-04-24 07:15','2017-04-11 07:54')
group by 
	A.server_name
	--, A.[database_name]
	, a.StartDate

UNION

SELECT 
	A.Instance
	--, A.[database_name]
	, SUM(a.data_kb) data_kb
	, SUM(a.index_size) index_size
	, SUM(a.data_kb) + SUM(a.index_size) [total]
	,[StartDate]
FROM (
SELECT 
	[Instance]
      ,[database_name]
      ,cast(replace([data_kb], 'KB','') as float) [data_kb]
      ,cast(replace([index_size], 'KB','') as float) [index_size]
      ,cast([StartDate] as nvarchar(16)) [StartDate]
  FROM [PSCollector].[dbo].[DBGrowthMetrics]
) A

where Instance = '430088-BC-SQL\SQL1'
	and StartDate in ('2017-04-24 07:15','2017-04-11 07:54')
group by 
	A.Instance
	--, A.[database_name]
	, a.StartDate

