USE [PSCollector]
GO


ALTER TABLE [MissingIndexes2] SET (SYSTEM_VERSIONING = OFF)
GO
DROP TABLE [MissingIndexesHistory2] 
GO
DROP TABLE [dbo].[MissingIndexes2];
go

CREATE TABLE [dbo].[MissingIndexes2](
	[ID] uniqueIdentifier PRIMARY KEY CLUSTERED ,
	[Instance] [nvarchar](500),
	[database_name] sysname,
	[ImprovementMeasure] [float] NULL,
	[Statement] [nvarchar](max) NULL,
	[equality_columns] [nvarchar](max) NULL,
	[inequality_columns] [nvarchar](max) NULL,
	[included_columns] [nvarchar](max) NULL,
	StartDate [datetime2](2) GENERATED ALWAYS AS ROW START NOT NULL,
	EndDate [datetime2](2) GENERATED ALWAYS AS ROW END NOT NULL,
	 PERIOD FOR SYSTEM_TIME (StartDate,EndDate)  
	)
WITH (
	SYSTEM_VERSIONING = ON ( HISTORY_TABLE = [dbo].[MissingIndexesHistory2] )
)

GO

 