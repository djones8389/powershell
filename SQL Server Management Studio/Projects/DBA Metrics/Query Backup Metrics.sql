SELECT top 10 
	[Instance]
      ,[DBName]
      ,[Pages]
      ,[compressed_backup_size]
      ,[Duration] [Duration-seconds]
	  ,[Duration]/60 [Duration-minutes]
      ,[backup_start_date]
      ,[backup_finish_date]
      ,[Backup Type]
      ,[is_compressed]
       --,cast([StartDate] as nvarchar(16)) [StartDate]
      --,[EndDate]
  FROM [PSCollector].[dbo].[Backup Metrics]
  where dbname like 'aat_secureassess'
	and cast([StartDate] as nvarchar(16))  in ('2017-04-11 07:54','2017-04-21 07:04')
	order by [backup_start_date] desc;