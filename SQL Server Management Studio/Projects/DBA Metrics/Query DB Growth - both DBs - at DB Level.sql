DECLARE @startdate DATETIME = '2016-02-16'
DECLARE @enddate DATETIME = '2016-08-09'

;with cte as (

SELECT 
	A.server_name
	, A.[database_name]
	, [StartDate]
	, SUM(a.data_kb) + SUM(a.index_size) [total]
	, SUM(a.data_kb) data_kb
	, SUM(a.index_size) index_size	
FROM (
SELECT 
	server_name
      ,[database_name]
      ,cast(replace([data_kb], 'KB','') as float) [data_kb]
      ,cast(replace([index_size], 'KB','') as float) [index_size]
      ,cast(CollectionDate as date) [StartDate]
  FROM [PSCollector].[dbo].[xxDBGrowthMetrics]
) A
where server_name in ('430069-AAT-SQL\SQL1')
	--and database_name in (@Databases)
	and [StartDate] BETWEEN @StartDate AND @EndDate
group by 
	A.server_name
	, A.[database_name]
	, a.StartDate

UNION

SELECT 
	A.Instance
	, A.[database_name]
	,[StartDate]
	, SUM(a.data_kb) + SUM(a.index_size) [total]
	, SUM(a.data_kb) data_kb
	, SUM(a.index_size) index_size
FROM (
SELECT 
	[Instance]
      ,[database_name]
      ,cast(replace([data_kb], 'KB','') as float) [data_kb]
      ,cast(replace([index_size], 'KB','') as float) [index_size]
      ,cast([StartDate] as nvarchar(16)) [StartDate]
  FROM [PSCollector].[dbo].[DBGrowthMetrics]
) A

where Instance in ('430069-AAT-SQL\SQL1')
	--and database_name in (@Databases)
	and [StartDate] BETWEEN @StartDate AND @EndDate
group by 
	A.Instance
	, A.[database_name]
	, a.StartDate
	
)
 

SELECT b.*
FROM (
select *
	, ROW_NUMBER() OVER (PARTITION BY A.server_name,A.database_name  ORDER BY  A.StartDate desc) r
from cte A
) b
WHERE R <= 10
ORDER BY r asc, b.database_name ASC;