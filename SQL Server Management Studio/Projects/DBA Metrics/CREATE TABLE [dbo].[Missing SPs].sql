USE [PSCollector]
GO


ALTER TABLE [dbo].[Missing SPs] SET (SYSTEM_VERSIONING = OFF)
GO
DROP TABLE [Missing SPs History]
GO
DROP TABLE [Missing SPs]
GO

CREATE TABLE [dbo].[Missing SPs](
	[ID] uniqueidentifier PRIMARY KEY CLUSTERED,
	[Instance] [nvarchar](500) NULL,
	[DBName] [nvarchar](500) NULL,
	[Improvement Measure] BIGINT,
    [TableName] sysname,
	[Equality Columns]  NVARCHAR(MAX),
	[Inequality Columns] NVARCHAR(MAX), 
	[Create Statement] NVARCHAR(MAX),
	StartDate [datetime2](2) GENERATED ALWAYS AS ROW START NOT NULL,
	EndDate [datetime2](2) GENERATED ALWAYS AS ROW END NOT NULL,
	PERIOD FOR SYSTEM_TIME (StartDate,EndDate)  
	)
WITH (
	SYSTEM_VERSIONING = ON ( HISTORY_TABLE = [dbo].[Missing SPs History] )
)
GO
