USE [PSCollector]
GO

ALTER TABLE [DiskSpace] SET (SYSTEM_VERSIONING = OFF)
GO
DROP TABLE [DiskSpaceHistory] 
GO
DROP TABLE [dbo].[DiskSpace];
go


CREATE TABLE [dbo].[DiskSpace](
	[ID] uniqueIdentifier PRIMARY KEY CLUSTERED ,
	[Instance] [nvarchar](100) NOT NULL ,
	[Drive] [nvarchar](max) NULL,
	[Type] [nvarchar](max) NULL,
	[MB Free] float NULL,
	StartDate [datetime2](2) GENERATED ALWAYS AS ROW START NOT NULL,
	EndDate [datetime2](2) GENERATED ALWAYS AS ROW END NOT NULL,
	 PERIOD FOR SYSTEM_TIME (StartDate,EndDate)  
	)
WITH (
	SYSTEM_VERSIONING = ON ( HISTORY_TABLE = [dbo].[DiskSpaceHistory] )
)

GO

