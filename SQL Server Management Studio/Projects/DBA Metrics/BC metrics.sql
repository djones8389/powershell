if OBJECT_ID('tempdb..#stats') is not null drop table #stats;

--select distinct cast(StartDate as date)
--from [DBGrowthMetrics]
--order by cast(StartDate as date) desc



DECLARE @Min VARCHAR(16); 
DECLARE @Max VARCHAR(16); 

SELECT @Min = MIN(CONVERT(date,[StartDate])) 
	, @Max = MAX(CONVERT(date,[StartDate]))
FROM [PSCollector].[dbo].[DBGrowthMetrics]


create table #stats (

	server_name nvarchar(100)
	,database_name nvarchar(100)
	, StartDate date
	, total int
	, data_kb int
	, index_size int
)

INSERT #stats
SELECT 
	A.server_name
	, A.[database_name]
	, [StartDate]
	, SUM(a.data_kb) + SUM(a.index_size) [total]
	, SUM(a.data_kb) data_kb
	, SUM(a.index_size) index_size	

FROM (
SELECT 
	server_name
      ,[database_name]
      ,cast(replace([data_kb], 'KB','') as float) [data_kb]
      ,cast(replace([index_size], 'KB','') as float) [index_size]
      ,cast(CollectionDate as date) [StartDate]
  FROM [PSCollector].[dbo].[xxDBGrowthMetrics]
) A
where server_name in ('430326-BC-SQL2\SQL2','430088-BC-SQL\SQL1')
	and  cast(StartDate as date) in ('2017-04-27','2017-05-02') 
group by 
	A.server_name
	, A.[database_name]
	, a.StartDate

UNION

SELECT 
	A.Instance
	,A.[database_name]
	,[StartDate]
	, SUM(a.data_kb) + SUM(a.index_size) [total]
	, SUM(a.data_kb) data_kb
	, SUM(a.index_size) index_size
FROM (
SELECT 
	[Instance]
      ,[database_name]
      ,cast(replace([data_kb], 'KB','') as float) [data_kb]
      ,cast(replace([index_size], 'KB','') as float) [index_size]
      ,cast([StartDate] as date) [StartDate]
  FROM [PSCollector].[dbo].[DBGrowthMetrics]
) A

where [Instance] in ('430326-BC-SQL2\SQL2','430088-BC-SQL\SQL1')
	and  cast(StartDate as date) in ('2017-04-27','2017-05-02') 
group by 
	A.Instance
	, A.[database_name]
	, a.StartDate




SELECT A.server_name
	, a.database_name
	, a.Min_StartDate
	, a.Max_StartDate
	, DATEDIFF(DAY, Min_StartDate, Max_StartDate) [Days]
	,(max.total - min.total) [Diff]
	, min.total [Min_Total]
	, max.total [Max_Total]
FROM (
select [server_name]
	, [database_name]
	, min(startDate) Min_StartDate
	, max(startDate) Max_StartDate
from #stats
group by 
	[server_name]
	,[database_name]
) A
INNER JOIN (	
	select [server_name]
		, [database_name]
		 ,startDate
		 ,total
	from #stats 
) [Min]
on [Min].[server_name] = A.[server_name]
	and [Min].[database_name] = A.[database_name]
	and [Min].startDate = A.Min_StartDate

INNER JOIN (	
	select [server_name]
		, [database_name]
		 ,startDate
		 , total
	from #stats 
) [Max]
on [Max].[server_name] = A.[server_name]
	and [Max].[database_name] = A.[database_name]
	and [Max].startDate = A.Max_StartDate
order by 1,2,3





/*
select *, ROW_NUMBER() OVER (PARTITION BY StartDate ORDER BY StartDate) R
into #results
from cte

select
	StartDate
	, [1]
FROM (
        SELECT [R] as [R], data_kb as [data_kb], startdate as [StartDate]
        FROM #results
) AS [S]
PIVOT (
	MAX(data_kb)
	For [R] in ([1]) 
) P



*/
