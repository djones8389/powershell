USE [PPD_AAT_ItemBank];

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'Backup_SuppressScore')
	CREATE TABLE [Backup_SuppressScore] (
		AssessmentGroupTableID int
		,[SuppressResultsScreenMark] bit
        ,[SuppressResultsScreenPercent] bit
	);

UPDATE AssessmentGroupTable
set  [SuppressResultsScreenMark] = 1
	 ,[SuppressResultsScreenPercent] = 1
output deleted.id, deleted.SuppressResultsScreenMark, deleted.SuppressResultsScreenPercent
into [Backup_SuppressScore]
where ID IN (
	SELECT 	AssessmentGroupTable.ID
	FROM AssessmentTable 
		inner join AssessmentGroupTable
		on AssessmentGroupTable.ID = AssessmentTable.AssessmentGroupID	
		inner join QualificationTable
		on QualificationTable.ID = AssessmentGroupTable.QualificationID
		inner join dbo.QualificationStatusLookupTable
		on QualificationStatusLookupTable.ID = QualificationTable.QualificationStatus
	where QualificationStatusLookupTable.Name in ('Retired','Live')
		and QualificationTable.QualificationRef not in ('AATQCF2016', 'L2FDAB2016')
);

/*Rollback*/

/*
UPDATE AssessmentGroupTable
SET [SuppressResultsScreenMark] = Backup_SuppressScore.SuppressResultsScreenMark
	 ,[SuppressResultsScreenPercent] = Backup_SuppressScore.SuppressResultsScreenPercent
FROM AssessmentGroupTable
INNER JOIN [Backup_SuppressScore]
ON [Backup_SuppressScore].ID = AssessmentGroupTable.ID;

*/

sp_whoisactive

kill 138
kill 101

USE PPD_AAT_SecureAssess

if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'Backup_LiveSuppressScore')
	CREATE TABLE [Backup_LiveSuppressScore] (
		ID int
		,[SuppressResultsScreenMark] bit
        ,[SuppressResultsScreenPercent] bit
	);


if not exists (select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'Backup_WHSuppressScore')
	CREATE TABLE [Backup_WHSuppressScore] (
		ID int
		,[SuppressResultsScreenMark] bit
        ,[SuppressResultsScreenPercent] bit
	);

UPDATE ScheduledExamsTable
set  [SuppressResultsScreenMark] = 1
	 ,[SuppressResultsScreenPercent] = 1
output deleted.id, deleted.SuppressResultsScreenMark, deleted.SuppressResultsScreenPercent
INTO [Backup_LiveSuppressScore]
from ScheduledExamsTable
inner join IB3QualificationLookup
on IB3QualificationLookup.ID = ScheduledExamsTable.qualificationId
inner join QualificationStatusLookup
on QualificationStatusLookup.Status = IB3QualificationLookup.QualificationStatus
where QualificationStatusLookup.Name in ('Retired','Live')
	and IB3QualificationLookup.QualificationRef not in ('AATQCF2016', 'L2FDAB2016');

UPDATE WAREHOUSE_ScheduledExamsTable
set  [SuppressResultsScreenMark] = 1
	 ,[SuppressResultsScreenPercent] = 1
output deleted.id, deleted.SuppressResultsScreenMark, deleted.SuppressResultsScreenPercent
INTO [Backup_WHSuppressScore]
from WAREHOUSE_ScheduledExamsTable
inner join IB3QualificationLookup
on IB3QualificationLookup.ID = WAREHOUSE_ScheduledExamsTable.qualificationId
inner join QualificationStatusLookup
on QualificationStatusLookup.Status = IB3QualificationLookup.QualificationStatus
where QualificationStatusLookup.Name in ('Retired','Live')
	and IB3QualificationLookup.QualificationRef not in ('AATQCF2016', 'L2FDAB2016');
	



/*Rollback*/

/*
UPDATE ScheduledExamsTable
SET [SuppressResultsScreenMark] = Backup_LiveSuppressScore.SuppressResultsScreenMark
	 ,[SuppressResultsScreenPercent] = Backup_LiveSuppressScore.SuppressResultsScreenPercent
FROM ScheduledExamsTable
INNER JOIN [Backup_LiveSuppressScore]
ON [Backup_LiveSuppressScore].ID = ScheduledExamsTable.ID;

UPDATE WAREHOUSE_ScheduledExamsTable
SET [SuppressResultsScreenMark] = [Backup_WHSuppressScore].SuppressResultsScreenMark
	 ,[SuppressResultsScreenPercent] = [Backup_WHSuppressScore].SuppressResultsScreenPercent
FROM WAREHOUSE_ScheduledExamsTable
INNER JOIN [Backup_WHSuppressScore]
ON [Backup_WHSuppressScore].ID = WAREHOUSE_ScheduledExamsTable.ID;

*/




--select o.name
--	, c.name
--from sys.columns c
--inner join sys.objects o
--on o.object_id = c.object_id
--where c.name like '%suppress%'
--order by 1;

