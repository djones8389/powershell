USE PPD_OCR_SecureAssess

declare @Practice table (
	examsessionid int
)
INSERT @Practice
select 
	examsessionid	
from WAREHOUSE_ExamSessionTable_Shreded 
   where examname like '%practice%' or
		   examRef like '%practice%' or
		   examname like '%qr%' or 
		   examname like '%test%' or 
		   examRef like '%qr%' or 	
		   examname like '%btl%' or 
		   examRef like '%btl%' or 			   
		   examRef like '%CF_test%' or 		
		   Centrename like '%_OCR Test Centre%' or 
		   Centrename like '%_OCR Training%' or 
		   Centrename like '%BTL%' or 
		   Centrename like '%_OCR Dummy%' or 
		   Centrename like '%_OCR Training%' 

/*Voided*/

SELECT count(*)
FROM dbo.WAREHOUSE_ExamSessionAvailableItemsTable
WHERE WAREHOUSE_ExamSessionAvailableItemsTable.ExamSessionID IN (
		SELECT examsessionid
		FROM @Practice
		)

SELECT count(*)
FROM dbo.WAREHOUSE_ExamSessionDocumentInfoTable
WHERE WAREHOUSE_ExamSessionDocumentInfoTable.Warehoused_ExamSessionDocumentID IN (
		SELECT warehouseExamSessionID 
		from WAREHOUSE_ExamSessionDocumentTable
		WHERE WAREHOUSE_ExamSessionDocumentTable.warehouseExamSessionID IN (
		SELECT examsessionid
		FROM @Practice
		)
)

SELECT count(*)
FROM dbo.WAREHOUSE_ExamSessionDocumentTable
WHERE WAREHOUSE_ExamSessionDocumentTable.warehouseExamSessionID IN  (
		SELECT examsessionid
		FROM @Practice
		)

SELECT count(*)
FROM dbo.WAREHOUSE_ExamSessionDurationAuditTable
WHERE WAREHOUSE_ExamSessionDurationAuditTable.Warehoused_ExamSessionID IN (
		SELECT examsessionid
		FROM @Practice
		)

SELECT count(*)
FROM dbo.WAREHOUSE_ExamSessionItemResponseTable
WHERE WAREHOUSE_ExamSessionItemResponseTable.WAREHOUSEExamSessionID IN (
		SELECT examsessionid
		FROM @Practice
		)

SELECT count(*)
FROM dbo.WAREHOUSE_ExamSessionResultHistoryTable
WHERE WAREHOUSE_ExamSessionResultHistoryTable.WareHouseExamSessionTableID IN (
		SELECT examsessionid
		FROM @Practice
		)

SELECT count(*)
FROM dbo.WAREHOUSE_ExamSessionTable
WHERE ID IN (
		SELECT examsessionid
		FROM @Practice
		)

SELECT count(*)
FROM dbo.WAREHOUSE_ExamSessionTable_ShreddedItems_Mark
WHERE WAREHOUSE_ExamSessionTable_ShreddedItems_Mark.examSessionID IN (
		SELECT examsessionid
		FROM @Practice
		)

SELECT count(*)
FROM dbo.WAREHOUSE_ExamSessionTable_Shreded
WHERE WAREHOUSE_ExamSessionTable_Shreded.examSessionID IN (
		SELECT examsessionid
		FROM @Practice
		)

SELECT count(*)
FROM dbo.WAREHOUSE_ExamSessionTable_ShrededItems
WHERE WAREHOUSE_ExamSessionTable_ShrededItems.ExamSessionID IN (
		SELECT examsessionid
		FROM @Practice
		)

SELECT count(*)
FROM dbo.WAREHOUSE_ExamStateAuditTable
WHERE WAREHOUSE_ExamStateAuditTable.WarehouseExamSessionID IN (
		SELECT examsessionid
		FROM @Practice
		)

SELECT count(*)
FROM dbo.WAREHOUSE_ScheduledExamsTable
WHERE ID IN (
		SELECT WAREHOUSEScheduledExamID
		FROM WAREHOUSE_ExamSessionTable
		WHERE ID IN (
			SELECT examsessionid
			FROM @Practice
		)
		)

