USE PPD_OCR_SecureAssess

select 
	year(submittedDate) [Year]
,	month(submittedDate) [Month]
,   qualificationName [Qualification]
,   examName [Exam]
,    case previousExamState when 10 then 1 else 0 end as [Voided]
,    case when examname like '%practice%' then 1 
		  when examRef like '%practice%' then 1
		  when examname like '%qr%' then 1 
		  when examname like '%test%' then 1 
		  when examRef like '%qr%' then 1 	
		  when examname like '%btl%' then 1 
		  when examRef like '%btl%' then 1 			   
		  when examRef like '%CF_test%' then 1 		
		  when Centrename like '%_OCR Test Centre%' then 1 
		  when Centrename like '%_OCR Training%' then 1 
		  when Centrename like '%BTL%' then 1 
		  when Centrename like '%_OCR Dummy%' then 1 
		  when Centrename like '%_OCR Training%' then 1 
		  else '0'
		  end as [Practice]
from WAREHOUSE_ExamSessionTable_Shreded 
