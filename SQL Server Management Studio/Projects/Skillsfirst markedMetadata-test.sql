SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

set statistics io on

if OBJECT_ID('tempdb..#test') is not null drop table #test;

select examSessionId
	, candidateRef
	, KeyCode
	, ExternalReference
into #test
FROM WAREHOUSE_ExamSessionTable_Shreded
WHERE ExternalReference in ('14M03O106','13M04O120','16M12O205','17M01O206')
			AND warehouseTime > '01 Jan 2016' -- BETWEEN '2017-02-20' AND getDATE()
			AND PreviousExamState != 10

create clustered index [IX_ExamSessionID] on #test (examsessionid);

select 
	a.examSessionId
	, candidateRef
	, KeyCode
	, ExternalReference
	, a.Section
	, a.[Question No.]
	, sum(a.mark) mark
	, sum(a.maxmark) maxMark
FROM (
	SELECT
	   WAREHOUSE_ExamSessionTable_Shreded.examSessionId
	   , exam.section.value('@id','tinyint') Section
		, section.item.value('@id','nvarchar(20)') Item
		, item.mark.value('@mark','tinyint') mark
		, item.mark.value('@maxMark','tinyint') maxMark
		,DENSE_RANK() OVER (PARTITION BY exam.section.value('@id','tinyint') ORDER BY exam.section.value('@id','tinyint'), section.item.value('@id','nvarchar(20)')) AS [Question No.]
	FROM WAREHOUSE_ExamSessionTable_Shreded
	inner join #test 
	on #test.examSessionId = WAREHOUSE_ExamSessionTable_Shreded.examSessionId
	cross apply resultData.nodes('exam/section') exam(section)
	cross apply exam.section.nodes('item') section(item)
	cross apply section.item.nodes('mark') item(mark)
	--WHERE ExternalReference in ('14M03O106','13M04O120','16M12O205','17M01O206')
	--			AND warehouseTime > '01 Jan 2016' -- BETWEEN '2017-02-20' AND getDATE()
	--			AND PreviousExamState != 10
) a
inner join #test
on #test.examSessionId = a.examSessionId

group by a.examSessionId
	, candidateRef
	, KeyCode
	, ExternalReference
	, Section
	, item
	, [Question No.]