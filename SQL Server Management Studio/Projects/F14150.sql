select title, id, projectid
from subjects
where id = 465

select distinct type
from items NOLOCK
where subjectid = 465
	and isdeleted = 0
	and type = 0
order by 1 asc

--position 1
-- type = 0



select title, i.name, cast(projectid as nvarchar(10)) + 'P' + cast(i.id as nvarchar(10)) ItemID
from subjects s (NOLOCK)
inner join items i (NOLOCK)
on s.id = i.subjectid
where i.type = 0
	and i.isdeleted = 0
order by 1

