USE [Demo_SurpassDataWarehouse]
GO
/****** Object:  StoredProcedure [ETL].[sp_stgMergeFactComponentResponses]    Script Date: 26/09/2017 15:45:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROC [ETL].[sp_stgMergeFactComponentResponses] AS BEGIN
	MERGE dbo.FactComponentResponses trg
	USING ETL.stg_FactComponentResponses src
	ON 
		trg.[CID] = src.[CID]
		AND trg.ExamSessionKey = src.ExamSessionKey
	--WHEN MATCHED
	--THEN UPDATE SET
	--	[Mark] = src.[Mark]
	--	, [RawResponse] = src.[RawResponse]
	--	, [DerivedResponse] = src.[DerivedResponse]
	--	, [ShortDerivedResponse] = src.[ShortDerivedResponse]
	--	, [ResponseKey] = src.[ResponseKey]
	WHEN NOT MATCHED THEN 
		INSERT ([ExamSessionKey], [Section], [CID], [SID], [CPID], [CPVersion], [Mark], [RawResponse], [DerivedResponse], [ShortDerivedResponse], [ResponseKey])
		VALUES ([ExamSessionKey], [Section], [CID], [SID], [CPID], [CPVersion], [Mark], [RawResponse], [DerivedResponse], [ShortDerivedResponse], [ResponseKey])
	;
END


select A.*
from DimComponents dc
RIGHT JOIN (

select src.CID
	, src.CPVersion
from ETL.stg_FactComponentResponses src
left join dbo.FactComponentResponses trg
ON  trg.[CID] = src.[CID]
AND trg.ExamSessionKey = src.ExamSessionKey
where trg.ExamSessionKey is null
) A
on A.CID = dc.CID
	and a.CPVersion = dc.CPVersion
where dc.Cid is null

/*

   Source: Merge stgFactComponentResponses 
   Description: The MERGE statement conflicted with the FOREIGN KEY constraint "FK_FactComponentResponses_DimComponents". 
   The conflict occurred in database "Demo_SurpassDataWarehouse", table "dbo.DimComponents".

   Source: Merge stgFactComponentResponses Execute SQL Task
   Description: Executing the query "exec [ETL].[sp_stgMergeFactComponentResponses]" failed with the following error: 

*/