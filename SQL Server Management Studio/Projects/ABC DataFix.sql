if OBJECT_ID ('tempdb..#LO') is not null DROP TABLE #LO;
if OBJECT_ID ('tempdb..#XMLs') is not null DROP TABLE #XMLs;
if OBJECT_ID ('tempdb..#AvailableItems') is not null DROP TABLE #AvailableItems;

CREATE TABLE  #LO (
	examSessionID smallint
	, ItemID nvarchar(20)
	, LO nVarchar(200)
);

bulk insert #LO
from 'C:\Users\977496-davej\Desktop\F6648 datafix.csv'
with (fieldterminator=',',rowterminator ='\n');

UPDATE #LO
set LO = REPLACE(LO, '*',',');

SELECT ID
	, StructureXML
	, resultDataFull
	, itemDataFull
INTO #XMLs
from WAREHOUSE_ExamSessionTable A
inner join (
	select distinct examsessionid 
	from #LO
	) B
on a.ID = B.examSessionID
inner join WAREHOUSE_ExamSessionTable_Shreded C
on C.examSessionID = a.ID;


SELECT A.[ExamSessionID]
      ,A.[ItemID]
      ,[ItemXML]
INTO #AvailableItems
FROM [WAREHOUSE_ExamSessionAvailableItemsTable] A
INNER JOIN #LO B
on A.ExamSessionID = B.examSessionID
	and a.ItemID = b.ItemID;

DECLARE LO CURSOR FOR
select ID
	, itemid	
	, LO
from WAREHOUSE_ExamSessionTable A
inner join #LO B
on a.ID = B.examSessionID;

DECLARE @ID INT, @itemID nvarchar(20), @LO Nvarchar(200)

OPEN LO;

FETCH NEXT FROM LO INTO @ID, @itemID, @LO

WHILE @@FETCH_STATUS = 0

BEGIN

	UPDATE #XMLs
	set StructureXML.modify('replace value of (/assessmentDetails/assessment/section/item[@id = sql:variable("@itemID")]/@LO)[1] with sql:variable("@LO")')
		, resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item[@id = sql:variable("@itemID")]/@LO)[1] with sql:variable("@LO")')
		, itemDataFull.modify('replace value of (/itemData/item[@id=sql:variable("@itemID")]/@LO)[1] with sql:variable("@LO")')
	where ID = @ID;

	UPDATE #AvailableItems
	set ItemXML.modify('replace value of (/item[@id=sql:variable("@itemID")]/@LO)[1] with sql:variable("@LO")')
	where examsessionid = @ID
		and ItemID = @itemID;

FETCH NEXT FROM LO INTO @ID, @itemID, @LO

END
CLOSE LO;
DEALLOCATE LO;


UPDATE WAREHOUSE_ExamSessionTable
set StructureXML = x.StructureXML
	, resultDataFull = x.resultDataFull
FROM WAREHOUSE_ExamSessionTable WEST
INNER JOIN #XMLs X
on X.ID = WEST.ID;

UPDATE WAREHOUSE_ExamSessionTable_Shreded
set itemDataFull = X.itemDataFull
FROM WAREHOUSE_ExamSessionTable_Shreded WESTS
INNER JOIN #XMLs X
on X.ID = WESTS.examSessionId;

UPDATE WAREHOUSE_ExamSessionAvailableItemsTable
set ItemXML = b.ItemXML
FROM WAREHOUSE_ExamSessionAvailableItemsTable A
INNER JOIN #AvailableItems B
ON A.ExamSessionID = b.ExamSessionID
	and a.ItemID = b.ItemID;


DROP TABLE #LO;
DROP TABLE #XMLs;

