RESTORE DATABASE [SANDBOX_BRITISHCOUNCIL_SecureAssess] 
	FROM DISK = N'T:\FTP\BC\BC.SA.bak' WITH FILE = 1, NOUNLOAD, NOREWIND, REPLACE, STATS = 1
	, MOVE 'Data' TO N'E:\DATA\SANDBOX_BRITISHCOUNCIL_SecureAssess.mdf'
	, MOVE 'BRITISHCOUNCIL_SecureAssess_1' TO N'E:\DATA\SANDBOX_BRITISHCOUNCIL_SecureAssess_1.mdf'
	, MOVE 'WHAItemsWHItemResponses' TO N'E:\DATA\SANDBOX_BRITISHCOUNCIL_SecureAssess_WHAItemsWHItemResponses.mdf'
	, MOVE 'Log' TO N'E:\DATA\SANDBOX_BRITISHCOUNCIL_SecureAssess.ldf'; 	

use master;

alter database [SANDBOX_BRITISHCOUNCIL_SecureAssess] set recovery simple with no_wait

USE [SANDBOX_BRITISHCOUNCIL_SecureAssess]

checkpoint

dbcc shrinkfile('Log',100)



