use PSCollector

select [server_name]
      --,[database_name]
	  ,[CollectionDate]
	  ,sum([size]) [sizeKB]
	  ,sum([size])/1024 [sizeMB]
	  ,sum([size])/1024/1024 [sizeGB]
from (

SELECT [server_name]
      ,[database_name]
      ,[table_name]
      ,cast([CollectionDate] as date) [CollectionDate]
	  ,cast(replace([data_kb], 'kb','')  as int) +  cast(replace([index_size], 'kb','') as int) [size]
  FROM [PSCollector].[dbo].[DBGrowthMetrics]
  --where server_name =  '430069-AAT-SQL\SQL1'
	 --and database_name != 'ASPState'
 ) a
  group by 
		[server_name]
       -- ,[database_name]
	    ,[CollectionDate]
  order by [server_name], [CollectionDate]
