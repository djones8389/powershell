use pscollector

SELECT top 3 [Instance]
	,[DBName]
	,[OBJECT_NAME]
	,[cached_time]
	--,[last_execution_time]
	,[execution_count]
	,[CPU]
	,[ELAPSED]
	,[LOGICAL_READS]
	,[LOGICAL_WRITES]
	,[PHYSICAL_READS]
	,[StartDate]
	 ,([CPU]/execution_count) AVGCPU
	--,[CPU]/[execution_count] [Avg. CPU]
	--,[ELAPSED]/[CPU] [Avg. ELAPSED]
	--,[LOGICAL_READS]/[CPU] [Avg. LOGICAL_READS]
	--, ([CPU]/1000000)/[execution_count] [secondsperexecution]
FROM [Sproc Metrics]
WHERE [OBJECT_NAME] in ('sa_ASSESSMENTSERVICE_GetExamScript_sp')--,'sa_ASSESSMENTSERVICE_SyncStructureXmlWithLiveTables_sp')
	AND startDate > '2018-01-28'
	order by cpu desc;

SELECT top 3 [Instance]
	,[DBName]
	,[OBJECT_NAME]
	,[cached_time]
	--,[last_execution_time]
	,[execution_count]
	,[CPU]
	,[ELAPSED]
	,[LOGICAL_READS]
	,[LOGICAL_WRITES]
	,[PHYSICAL_READS]
	,[StartDate]
,([CPU]/execution_count) AVGCPU
FROM [Sproc Metrics]
WHERE [OBJECT_NAME] in('sa_ASSESSMENTSERVICE_GetExamScript_sp')--,'sa_ASSESSMENTSERVICE_SyncStructureXmlWithLiveTables_sp')
	AND startDate between '2018-01-21' and '2018-01-28'
	order by cpu desc;