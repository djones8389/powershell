--RESTORE DATABASE [PRV_BCGuilds_TestPackage] 
--FROM DISK = N'T:\BACKUP\PRV_BCGuilds_TestPackage.2016.04.12.bak' 
--WITH  FILE = 1,  MOVE N'UAT_BCGuilds_TestPackage_11_6_0_241_IP' TO N'S:\Data\PRV_BCGuilds_TestPackage.mdf',  
--				MOVE N'UAT_BCGuilds_TestPackage_11_6_0_241_IP_log' TO N'L:\Logs\PRV_BCGuilds_TestPackage.ldf'
--, RECOVERY, NOUNLOAD, NOREWIND, REPLACE;

--RESTORE DATABASE [PRV_BritishCouncil_TestPackageManager] 
--FROM DISK = N'T:\BACKUP\PRV_BritishCouncil_TestPackageManager.2016.04.12.bak' 
--WITH  FILE = 1,  MOVE N'BritishCouncil_TestPackage' TO N'E:\Data\PRV_BritishCouncil_TestPackageManager.mdf',  
--				MOVE N'BritishCouncil_TestPackage_log' TO N'L:\Logs\PRV_BritishCouncil_TestPackageManager.ldf'
--, RECOVERY, NOUNLOAD, NOREWIND, REPLACE;


DECLARE @DynamicSpidKiller nvarchar(MAX) = '';

select @DynamicSpidKiller +=CHAR(13) + 
	 'KILL '  + convert(nvarchar(10),spid)
from sys.databases D
INNER JOIN sys.sysprocesses S
on D.database_id = s.dbid
where Name = 'PRV_BritishCouncil_TestPackageManager'

EXEC (@DynamicSpidKiller);


DECLARE @DynamicSpidKiller nvarchar(MAX) = '';

select @DynamicSpidKiller +=CHAR(13) + 
	 'KILL '  + convert(nvarchar(10),spid)
from sys.databases D
INNER JOIN sys.sysprocesses S
on D.database_id = s.dbid
where Name = 'PRV_BCGuilds_TestPackage'

EXEC (@DynamicSpidKiller);


--restore filelistonly from disk  = N'T:\BACKUP\PRV_BCGuilds_TestPackage.2016.04.12.bak' 
--restore filelistonly from disk  = N'T:\BACKUP\PRV_BritishCouncil_TestPackageManager.2016.04.12.bak' 

/*
WITH  FILE = 1,  MOVE N'Data' TO N'H:\Databases\UAT_StuRestore_TestPackageManager.mdf',  
			MOVE N'Log' TO N'H:\Databases\UAT_StuRestore_TestPackageManager.ldf'
, RECOVERY, NOUNLOAD, NOREWIND,  STATS = 5

GO


*/

SELECT DISTINCT

	'IF NOT EXISTS (SELECT 1 FROM Sys.sql_logins where name =''' + Name + '_Login' + ''')  CREATE LOGIN [' + Name + '_Login] WITH PASSWORD = ''Password'';' +
    'USE ' + QUOTENAME(Name) + ';  IF EXISTS (SELECT 1 FROM sys.database_principals where name = '''+Name + '_User'') DROP USER ' + Name + '_User' + ' 
	
	CREATE USER ' + Name + '_User' + ' FOR LOGIN ' + Name + '_Login' +
    '; EXEC sp_addrolemember ''db_owner'',  ''' + Name + '_User''  ' 

from sys.databases
where name like '%PRV_%';
