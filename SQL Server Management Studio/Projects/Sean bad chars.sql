SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

IF OBJECT_ID('tempdb..#Results') IS NOT NULL DROP TABLE #Results;

CREATE TABLE #Results ( 
	Customer nvarchar(MAX)
        , ASCIIChar nvarchar(max)
        , ItemID nvarchar(max)
        , ItemText nvarchar(max));

DECLARE @DynamicString NVARCHAR(MAX) = '';

SELECT @DynamicString +=CHAR(13) + '

use '+quotename(Name)+'

DECLARE @'+Name+' nvarchar(MAX) = '''';

select @'+Name+' +=CHAR(13) +
''
select '''''+Name+''''' as DB,''''CHAR1'''' AS [ASCIIChar], ItemKey, [Text] FROM '+Name+'..DimOptions WHERE [text] like ''''%'''' + CHAR(1)+''''%'''' UNION
select '''''+Name+''''' as DB,''''CHAR2'''' AS [ASCIIChar], ItemKey, [Text] FROM '+Name+'..DimOptions WHERE [text] like ''''%'''' + CHAR(2)+''''%'''' UNION
select '''''+Name+''''' as DB,''''CHAR3'''' AS [ASCIIChar], ItemKey, [Text] FROM '+Name+'..DimOptions WHERE [text] like ''''%'''' + CHAR(3)+''''%'''' UNION
select '''''+Name+''''' as DB,''''CHAR4'''' AS [ASCIIChar], ItemKey, [Text] FROM '+Name+'..DimOptions WHERE [text] like ''''%'''' + CHAR(4)+''''%'''' UNION
select '''''+Name+''''' as DB,''''CHAR5'''' AS [ASCIIChar], ItemKey, [Text] FROM '+Name+'..DimOptions WHERE [text] like ''''%'''' + CHAR(5)+''''%'''' UNION
select '''''+Name+''''' as DB,''''CHAR6'''' AS [ASCIIChar], ItemKey, [Text] FROM '+Name+'..DimOptions WHERE [text] like ''''%'''' + CHAR(6)+''''%'''' UNION
select '''''+Name+''''' as DB,''''CHAR7'''' AS [ASCIIChar], ItemKey, [Text] FROM '+Name+'..DimOptions WHERE [text] like ''''%'''' + CHAR(7)+''''%'''' UNION
select '''''+Name+''''' as DB,''''CHAR8'''' AS [ASCIIChar], ItemKey, [Text] FROM '+Name+'..DimOptions WHERE [text] like ''''%'''' + CHAR(8)+''''%'''' UNION
select '''''+Name+''''' as DB,''''CHAR9'''' AS [ASCIIChar], ItemKey, [Text] FROM '+Name+'..DimOptions WHERE [text] like ''''%'''' + CHAR(9)+''''%'''' UNION
select '''''+Name+''''' as DB,''''CHAR10'''' AS [ASCIIChar], ItemKey, [Text] FROM '+Name+'..DimOptions WHERE [text] like ''''%'''' + CHAR(10)+''''%'''' UNION
select '''''+Name+''''' as DB,''''CHAR11'''' AS [ASCIIChar], ItemKey, [Text] FROM '+Name+'..DimOptions WHERE [text] like ''''%'''' + CHAR(11)+''''%'''' UNION
select '''''+Name+''''' as DB,''''CHAR12'''' AS [ASCIIChar], ItemKey, [Text] FROM '+Name+'..DimOptions WHERE [text] like ''''%'''' + CHAR(12)+''''%'''' UNION
select '''''+Name+''''' as DB,''''CHAR13'''' AS [ASCIIChar], ItemKey, [Text] FROM '+Name+'..DimOptions WHERE [text] like ''''%'''' + CHAR(13)+''''%'''' UNION
select '''''+Name+''''' as DB,''''CHAR14'''' AS [ASCIIChar], ItemKey, [Text] FROM '+Name+'..DimOptions WHERE [text] like ''''%'''' + CHAR(14)+''''%'''' UNION
select '''''+Name+''''' as DB,''''CHAR15'''' AS [ASCIIChar], ItemKey, [Text] FROM '+Name+'..DimOptions WHERE [text] like ''''%'''' + CHAR(15)+''''%'''' UNION
select '''''+Name+''''' as DB,''''CHAR16'''' AS [ASCIIChar], ItemKey, [Text] FROM '+Name+'..DimOptions WHERE [text] like ''''%'''' + CHAR(16)+''''%'''' UNION
select '''''+Name+''''' as DB,''''CHAR17'''' AS [ASCIIChar], ItemKey, [Text] FROM '+Name+'..DimOptions WHERE [text] like ''''%'''' + CHAR(17)+''''%'''' UNION
select '''''+Name+''''' as DB,''''CHAR18'''' AS [ASCIIChar], ItemKey, [Text] FROM '+Name+'..DimOptions WHERE [text] like ''''%'''' + CHAR(18)+''''%'''' UNION
select '''''+Name+''''' as DB,''''CHAR19'''' AS [ASCIIChar], ItemKey, [Text] FROM '+Name+'..DimOptions WHERE [text] like ''''%'''' + CHAR(19)+''''%'''' UNION
select '''''+Name+''''' as DB,''''CHAR20'''' AS [ASCIIChar], ItemKey, [Text] FROM '+Name+'..DimOptions WHERE [text] like ''''%'''' + CHAR(20)+''''%'''' UNION
select '''''+Name+''''' as DB,''''CHAR21'''' AS [ASCIIChar], ItemKey, [Text] FROM '+Name+'..DimOptions WHERE [text] like ''''%'''' + CHAR(21)+''''%'''' UNION
select '''''+Name+''''' as DB,''''CHAR22'''' AS [ASCIIChar], ItemKey, [Text] FROM '+Name+'..DimOptions WHERE [text] like ''''%'''' + CHAR(22)+''''%'''' UNION
select '''''+Name+''''' as DB,''''CHAR23'''' AS [ASCIIChar], ItemKey, [Text] FROM '+Name+'..DimOptions WHERE [text] like ''''%'''' + CHAR(23)+''''%'''' UNION
select '''''+Name+''''' as DB,''''CHAR24'''' AS [ASCIIChar], ItemKey, [Text] FROM '+Name+'..DimOptions WHERE [text] like ''''%'''' + CHAR(24)+''''%'''' UNION
select '''''+Name+''''' as DB,''''CHAR25'''' AS [ASCIIChar], ItemKey, [Text] FROM '+Name+'..DimOptions WHERE [text] like ''''%'''' + CHAR(25)+''''%'''' UNION
select '''''+Name+''''' as DB,''''CHAR26'''' AS [ASCIIChar], ItemKey, [Text] FROM '+Name+'..DimOptions WHERE [text] like ''''%'''' + CHAR(26)+''''%'''' UNION
select '''''+Name+''''' as DB,''''CHAR27'''' AS [ASCIIChar], ItemKey, [Text] FROM '+Name+'..DimOptions WHERE [text] like ''''%'''' + CHAR(27)+''''%'''' UNION
select '''''+Name+''''' as DB,''''CHAR28'''' AS [ASCIIChar], ItemKey, [Text] FROM '+Name+'..DimOptions WHERE [text] like ''''%'''' + CHAR(28)+''''%'''' UNION
select '''''+Name+''''' as DB,''''CHAR29'''' AS [ASCIIChar], ItemKey, [Text] FROM '+Name+'..DimOptions WHERE [text] like ''''%'''' + CHAR(29)+''''%'''' UNION
select '''''+Name+''''' as DB,''''CHAR30'''' AS [ASCIIChar], ItemKey, [Text] FROM '+Name+'..DimOptions WHERE [text] like ''''%'''' + CHAR(30)+''''%'''' 
''
exec(@'+Name+')
'
 from (
 SELECT top 1 [name] --SUBSTRING(name, 0, CHARINDEX('_',name)) [Name]
 FROM sys.databases 
 WHERE SUBSTRING(name, 0, CHARINDEX('_',name)) != ''
 and name like '%SurpassDataWarehouse'
)c


exec (@dynamicstring);


