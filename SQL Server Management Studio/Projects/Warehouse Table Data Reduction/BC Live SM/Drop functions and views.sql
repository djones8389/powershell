SELECT 'DROP FUNCTION ' + QUOTENAME(NAME) + ';'
FROM sys.objects 
WHERE RIGHT(type_desc, 8) = 'FUNCTION'

select 'DROP VIEW '  + QUOTENAME(NAME) + ';'
from sys.views


--DROP FUNCTION [ag_CLR_Concat];
DROP FUNCTION [getExamsWithRestrictions_fn];
DROP FUNCTION [getAffectedScripts_fn];
DROP FUNCTION [getAffectedUniqueResponses_fn];
DROP FUNCTION [getAvailableUniqueGroupResponses_fn];
DROP FUNCTION [getUserExamsWithMarkingPermissions_fn];
DROP FUNCTION [getGroupDefinitionsJoined_fn];
DROP FUNCTION [getGroupSourceTable_fn];
DROP FUNCTION [getGroupAggregatedSourceForMarking_fn];
DROP FUNCTION [getGroupsWithAffectedResponsesForMarking_fn];
DROP FUNCTION [sm_MARKINGSERVICE_GetAllGroupsForUser_fn];
DROP FUNCTION [sm_getExamPermissionsForEscalations_fn];
DROP FUNCTION [fn_diagramobjects];
DROP FUNCTION [getGroupDefinitionsForEscalations_fn];
DROP FUNCTION [getCentreNameAndCodeForUniqueGroupResponse_fn];
DROP FUNCTION [sm_getExamPermissionsForScriptReview_fn];
DROP FUNCTION [getGroupAggregatedSourceForHome_fn];
DROP FUNCTION [getGroupsWithAffectedResponsesForHome_fn];
DROP FUNCTION [ParamsToList];
DROP FUNCTION [sm_checkGroupAndItemsMarkedInTolerance_fn];
DROP FUNCTION [sm_getMarkingExaminerReport_fn];
DROP FUNCTION [sm_getIsNewItem_fn];
DROP FUNCTION [sm_getAffectedScripts_fn_OLD];
DROP FUNCTION [ParamsToThreeColumnTable];
DROP FUNCTION [sm_getAffectedUGRs_fn_OLD];
DROP FUNCTION [sm_getAffectedUGRsCount_fn_OLD];
DROP FUNCTION [sm_hasUserRealMarkedResponsesForExam_fn];
DROP FUNCTION [sm_EscalatedUGRs_fn];
DROP FUNCTION [sm_getQuotaManagementInfoForUser_fn];
DROP FUNCTION [sm_checkUser1CiawayFromSuspend];
DROP FUNCTION [sm_getMarkingReportItems_fn];
DROP FUNCTION [sm_getUserQuotaForGroup_fn];
DROP FUNCTION [sm_MANAGEEXAMINERS_GetExaminers_fn];
DROP FUNCTION [sm_getQuotaForUserForExam_fn];
DROP FUNCTION [sm_checkIsUniqueGroupResponseInConfirmedScript];
DROP FUNCTION [sm_checkGroupCIsExceeded_fn];
DROP VIEW [ControlGroups_view];
DROP VIEW [CandidateExamVersionGroups_view];
DROP VIEW [ScriptToUpdate_view];
DROP VIEW [UserExamPermissions_view];
DROP VIEW [UserExamUnrestrictedExaminer_view];
DROP VIEW [MarkingReport_view_OLD];
DROP VIEW [ActiveGroups_view];
DROP VIEW [ManualMarkedGroups_view];
DROP VIEW [NotMarkedResponses_view];
DROP VIEW [MarkingReport_view_DJTEST];
DROP VIEW [UserExamExaminer_view];
DROP VIEW [EscalatedGroups_view];
DROP VIEW [GroupIdExamVersionName_view];
DROP VIEW [ExamVersionNames_view];
DROP VIEW [ExamVersionNamesConcatenated_view];
DROP VIEW [QualificationExams_view];
DROP VIEW [GroupCentreCandidate_view];
DROP VIEW [CandidateCentreResponse_view];
DROP VIEW [EscalatedUniqueGroupResponses_view];
DROP VIEW [EscalatedUniqueGroupResponsesForCandidateCentre_view];
DROP VIEW [GroupMarkedResponses_view];
DROP VIEW [ScriptReview_view];
DROP VIEW [QuotaGroupCandidateResponses_view];
DROP VIEW [ControlGroups_view_OLD];
DROP VIEW [ControlGroups_view_NEW];
DROP VIEW [MarkingReport_view];
DROP VIEW [MarkedCIGroups_view];