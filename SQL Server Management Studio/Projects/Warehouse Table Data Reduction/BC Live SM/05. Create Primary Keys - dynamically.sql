USE [PPD_BCIntegration_SecureMarker]

--ALTER TABLE [dbo].[AffectedCandidateExamVersions] ADD CONSTRAINT [PK_AffectedCandidateExamVersions] PRIMARY KEY CLUSTERED( [CandidateExamVersionId] ASC, [UserId] ASC, [UniqueGroupResponseId] ASC);
--ALTER TABLE [dbo].[AssignedGroupMarks] ADD CONSTRAINT [PK_AssignedGroupMarks] PRIMARY KEY CLUSTERED( [ID] ASC);
--ALTER TABLE [dbo].[AssignedItemMarks] ADD CONSTRAINT [PK_AssignedItemMarks] PRIMARY KEY CLUSTERED( [GroupMarkId] ASC, [UniqueResponseId] ASC);
--ALTER TABLE [dbo].[CandidateExamVersions] ADD CONSTRAINT [PK_CandidateExamVersions_1] PRIMARY KEY CLUSTERED( [ID] ASC);
--ALTER TABLE [dbo].[CandidateExamVersionStateLookup] ADD CONSTRAINT [PK_CandidateExamVersionStateLookup] PRIMARY KEY CLUSTERED( [ID] ASC);
ALTER TABLE [dbo].[CandidateExamVersionStatuses] ADD CONSTRAINT [PK_CandidateExamVersionStatuses] PRIMARY KEY CLUSTERED( [ID] ASC);
ALTER TABLE [dbo].[CandidateGroupResponses] ADD CONSTRAINT [PK_CandidateGriupResponses] PRIMARY KEY CLUSTERED( [CandidateExamVersionID] ASC, [UniqueGroupResponseID] ASC);
ALTER TABLE [dbo].[CandidateResponses] ADD CONSTRAINT [PK_CandidateResponses] PRIMARY KEY CLUSTERED( [CandidateExamVersionID] ASC, [UniqueResponseID] ASC);
ALTER TABLE [dbo].[Exams] ADD CONSTRAINT [PK_Exams_1] PRIMARY KEY CLUSTERED( [ID] ASC);
ALTER TABLE [dbo].[ExamSectionItems] ADD CONSTRAINT [PK_ExamSectionItems] PRIMARY KEY CLUSTERED( [ExamSectionID] ASC, [ItemID] ASC, [ExamStructureId] ASC);
ALTER TABLE [dbo].[ExamSections] ADD CONSTRAINT [PK_ExamSections] PRIMARY KEY CLUSTERED( [ID] ASC);
ALTER TABLE [dbo].[ExamVersionItems] ADD CONSTRAINT [PK_ExamVersionItems] PRIMARY KEY CLUSTERED( [ID] ASC);
ALTER TABLE [dbo].[ExamVersionItemsRef] ADD CONSTRAINT [PK_ExamVersionItemsRef] PRIMARY KEY CLUSTERED( [ItemID] ASC, [ExamVersionID] ASC);
ALTER TABLE [dbo].[ExamVersions] ADD CONSTRAINT [PK_ExamVersions] PRIMARY KEY CLUSTERED( [ID] ASC);
ALTER TABLE [dbo].[ExamVersionStructureItems] ADD CONSTRAINT [PK_ExamVersionStructureItems] PRIMARY KEY CLUSTERED( [ExamStructureID] ASC, [ItemID] ASC);
ALTER TABLE [dbo].[ExamVersionStructures] ADD CONSTRAINT [PK_ExamVersionStructures] PRIMARY KEY CLUSTERED( [ID] ASC);
ALTER TABLE [dbo].[GroupDefinitionItems] ADD CONSTRAINT [PK_GroupDefinitionItems] PRIMARY KEY CLUSTERED( [GroupID] ASC, [ItemID] ASC);
ALTER TABLE [dbo].[GroupDefinitions] ADD CONSTRAINT [PK_GroupDefinitions] PRIMARY KEY CLUSTERED( [ID] ASC);
ALTER TABLE [dbo].[GroupDefinitionStructureCrossRef] ADD CONSTRAINT [PK_GroupDefinitionStructureCrossRef] PRIMARY KEY CLUSTERED( [GroupDefinitionID] ASC, [ExamVersionStructureID] ASC);
ALTER TABLE [dbo].[Items] ADD CONSTRAINT [PK_Items] PRIMARY KEY CLUSTERED( [ID] ASC);
ALTER TABLE [dbo].[Moderations] ADD CONSTRAINT [PK_Moderations] PRIMARY KEY CLUSTERED( [ID] ASC);
ALTER TABLE [dbo].[QuotaManagement] ADD CONSTRAINT [PK_QuotaManagement] PRIMARY KEY CLUSTERED( [UserId] ASC, [GroupId] ASC, [ExamID] ASC);
ALTER TABLE [dbo].[UniqueGroupResponseLinks] ADD CONSTRAINT [PK_UniqueGroupResponseLinks] PRIMARY KEY CLUSTERED( [UniqueGroupResponseID] ASC, [UniqueResponseId] ASC);
--ALTER TABLE [dbo].[UniqueGroupResponseParkedReasonCrossRef] ADD CONSTRAINT [PK_UniqueGroupResponseParkedReasonCrossRef] PRIMARY KEY CLUSTERED( [UniqueGroupResponseId] ASC, [ParkedReasonId] ASC);
ALTER TABLE [dbo].[UniqueGroupResponses] ADD CONSTRAINT [PK_UniqueGroupResponses] PRIMARY KEY CLUSTERED( [ID] ASC);
ALTER TABLE [dbo].[UniqueResponseDocuments] ADD CONSTRAINT [PK_UniqueResponseDocuments] PRIMARY KEY CLUSTERED( [ID] ASC);
ALTER TABLE [dbo].[UniqueResponses] ADD CONSTRAINT [PK_UniqueResponseTable] PRIMARY KEY CLUSTERED( [id] ASC);
ALTER TABLE [dbo].[UserCompetenceStatus] ADD CONSTRAINT [PK_UserQualifiedStatus_1] PRIMARY KEY CLUSTERED( [UserID] ASC, [GroupDefinitionID] ASC);
ALTER TABLE [dbo].[UserRecentExams] ADD CONSTRAINT [PK_UserRecentExams] PRIMARY KEY CLUSTERED( [ExamId] ASC, [UserId] ASC);
ALTER TABLE [dbo].[Users] ADD CONSTRAINT [PK_UserTable] PRIMARY KEY CLUSTERED( [ID] ASC);



/*
select 
	'ALTER TABLE ' + QUOTENAME(s.Name) + '.' + QUOTENAME(O.Name) 
	+ ' ADD CONSTRAINT ' + QUOTENAME(I.name)
	+ ' PRIMARY KEY ' + I.type_desc  collate Latin1_General_CI_AS

	+ '(' + ISNULL(STUFF((SELECT ', ' + QUOTENAME(c2.name) + 
						CASE ic2.is_descending_key
						WHEN 0 THEN ' ASC'
						ELSE ' DESC'
						END						 
						FROM sys.indexes i2 INNER JOIN sys.index_columns ic2
						ON i2.object_id = ic2.object_id AND i2.index_id = ic2.index_id
						INNER JOIN  sys.columns c2 ON ic2.object_id = c2.object_id AND ic2.column_id = c2.column_id						
						WHERE ic2.object_id = i.object_id AND ic2.index_id = i.index_id
						ORDER BY ic2.object_id	 
						FOR XML PATH(N''), TYPE).value(N'.[1]', N'NVARCHAR(MAX)'), 1, 1, N''), 'HEAP') 
						+ ');'

from sys.indexes I 
inner join sys.objects O
on o.object_id = i.object_id
inner join sys.schemas S
on S.schema_id = o.schema_id
where is_primary_key = 1
 ORDER BY s.Name
 */



