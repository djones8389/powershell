ALTER TABLE [AffectedCandidateExamVersions] DROP CONSTRAINT [FK_AffectedCandidateExamVersions_CandidateExamVersions];
ALTER TABLE [AffectedCandidateExamVersions] DROP CONSTRAINT [FK_AffectedCandidateExamVersions_GroupDefinitions];
ALTER TABLE [AffectedCandidateExamVersions] DROP CONSTRAINT [FK_AffectedCandidateExamVersions_UniqueGroupResponses];
ALTER TABLE [AffectedCandidateExamVersions] DROP CONSTRAINT [FK_AffectedCandidateExamVersions_Users];
ALTER TABLE [AssignedGroupMarks] DROP CONSTRAINT [FK_AssignedGroupMarks_GroupDefinitions];
ALTER TABLE [AssignedGroupMarks] DROP CONSTRAINT [FK_AssignedGroupMarks_MarkingMethodLookup];
ALTER TABLE [AssignedGroupMarks] DROP CONSTRAINT [FK_AssignedGroupMarks_UniqueGroupResponses];
ALTER TABLE [AssignedGroupMarks] DROP CONSTRAINT [FK_AssignedGroupMarks_Users];
ALTER TABLE [AssignedItemMarks] DROP CONSTRAINT [FK_AssignedItemMarks_UniqueResponses];
ALTER TABLE [CandidateExamVersions] DROP CONSTRAINT [FK_CandidateExamVersions_CandidateExamVersionStateLookup];
ALTER TABLE [CandidateExamVersions] DROP CONSTRAINT [FK_CandidateExamVersions_ExamVersions];
ALTER TABLE [CandidateExamVersions] DROP CONSTRAINT [FK_CandidateExamVersions_ExamVersionStructures];
ALTER TABLE [CandidateExamVersionStatuses] DROP CONSTRAINT [FK_CandidateExamVersionStatuses_CandidateExamVersions];
ALTER TABLE [CandidateExamVersionStatuses] DROP CONSTRAINT [FK_CandidateExamVersionStatuses_CandidateExamVersionStateLookup];
ALTER TABLE [CandidateGroupResponses] DROP CONSTRAINT [FK_CandidateGroupResponses_CandidateExamVersions];
ALTER TABLE [CandidateGroupResponses] DROP CONSTRAINT [FK_CandidateGroupResponses_UniqueGroupResponses];
ALTER TABLE [CandidateResponses] DROP CONSTRAINT [FK_CandidateResponses_CandidateExamVersions];
ALTER TABLE [CandidateResponses] DROP CONSTRAINT [FK_CandidateResponses_UniqueResponseTable];
ALTER TABLE [Exams] DROP CONSTRAINT [FK_Exams_Qualifications1];
ALTER TABLE [ExamSectionItems] DROP CONSTRAINT [FK_ExamSectionItems_ExamSections];
ALTER TABLE [ExamSectionItems] DROP CONSTRAINT [FK_ExamSectionItems_ExamVersionStructures];
ALTER TABLE [ExamSectionItems] DROP CONSTRAINT [FK_ExamSectionItems_Items];
ALTER TABLE [ExamSections] DROP CONSTRAINT [FK_ExamSections_ExamVersions];
ALTER TABLE [ExamVersionItems] DROP CONSTRAINT [FK_ExamVersionItems_CandidateExamVersions];
ALTER TABLE [ExamVersionItems] DROP CONSTRAINT [FK_ExamVersionItems_Items];
ALTER TABLE [ExamVersionItemsRef] DROP CONSTRAINT [FK_ExamVersionItemsRef_ExamVersions];
ALTER TABLE [ExamVersionItemsRef] DROP CONSTRAINT [FK_ExamVersionItemsRef_Items];
ALTER TABLE [ExamVersions] DROP CONSTRAINT [FK_ExamVersions_Exams];
ALTER TABLE [ExamVersionStructureItems] DROP CONSTRAINT [FK_ExamVersionStructureItems_ExamVersionStructures];
ALTER TABLE [ExamVersionStructureItems] DROP CONSTRAINT [FK_ExamVersionStructureItems_Items];
ALTER TABLE [ExamVersionStructures] DROP CONSTRAINT [FK_ExamVersionStructures_ExamVersions];
ALTER TABLE [ExamVersionStructures] DROP CONSTRAINT [FK_ExamVersionStructures_ExamVersionStructuresStatuses];
ALTER TABLE [GroupDefinitionItems] DROP CONSTRAINT [FK_GroupDefinitionItems_GroupDefinitions];
ALTER TABLE [GroupDefinitionItems] DROP CONSTRAINT [FK_GroupDefinitionItems_Items];
ALTER TABLE [GroupDefinitions] DROP CONSTRAINT [FK_GroupDefinitions_Exams];
ALTER TABLE [GroupDefinitionStructureCrossRef] DROP CONSTRAINT [FK_GroupDefinitionStructureCrossRef_ExamVersionStructures];
ALTER TABLE [GroupDefinitionStructureCrossRef] DROP CONSTRAINT [FK_GroupDefinitionStructureCrossRef_GroupDefinitions];
ALTER TABLE [GroupDefinitionStructureCrossRef] DROP CONSTRAINT [FK_GroupDefinitionStructureCrossRef_GroupDefinitionStructureCrossRefStatuses];
ALTER TABLE [Items] DROP CONSTRAINT [FK_Items_Exams];
ALTER TABLE [Moderations] DROP CONSTRAINT [FK_Moderations_CandidateExamVersions];
ALTER TABLE [Moderations] DROP CONSTRAINT [FK_Moderations_Moderations];
ALTER TABLE [Moderations] DROP CONSTRAINT [FK_Moderations_Users];
ALTER TABLE [QuotaManagement] DROP CONSTRAINT [FK_QuotaManagement_Exams];
ALTER TABLE [QuotaManagement] DROP CONSTRAINT [FK_QuotaManagement_GroupDefinitions];
ALTER TABLE [QuotaManagement] DROP CONSTRAINT [FK_QuotaManagement_SuspendedReasons];
ALTER TABLE [QuotaManagement] DROP CONSTRAINT [FK_QuotaManagement_Users];
ALTER TABLE [UniqueGroupResponseLinks] DROP CONSTRAINT [FK_UniqueGroupResponseLinks_UniqueGroupResponses];
ALTER TABLE [UniqueGroupResponseLinks] DROP CONSTRAINT [FK_UniqueGroupResponseLinks_UniqueResponses];
ALTER TABLE [UniqueGroupResponses] DROP CONSTRAINT [FK_UniqueGroupResponses_GroupDefinitions];
ALTER TABLE [UniqueGroupResponses] DROP CONSTRAINT [FK_UniqueGroupResponses_TokenStore];
ALTER TABLE [UniqueGroupResponses] DROP CONSTRAINT [FK_UniqueGroupResponses_UniqueGroupResponses];
ALTER TABLE [UniqueResponseDocuments] DROP CONSTRAINT [FK_UniqueResponseDocuments_UniqueResponses];
ALTER TABLE [UniqueResponses] DROP CONSTRAINT [FK_UniqueResponseTable_Items];
ALTER TABLE [UniqueResponses] DROP CONSTRAINT [FK_UniqueResponseTable_MarkSchemeTable];
ALTER TABLE [UniqueResponses] DROP CONSTRAINT [FK_UniqueResponseTable_MarkSchemeTable1];
ALTER TABLE [UserCompetenceStatus] DROP CONSTRAINT [FK_UserCompetenceStatus_GroupDefinitions];
ALTER TABLE [UserCompetenceStatus] DROP CONSTRAINT [FK_UserQualifiedStatus_QualifiedStatusLookup];
ALTER TABLE [UserCompetenceStatus] DROP CONSTRAINT [FK_UserQualifiedStatus_Users];




/*

ALTER TABLE [dbo].[AssignedUserRoles] DROP CONSTRAINT [FK_AssignedUserRoles_Exams]
GO
ALTER TABLE [dbo].[UserRecentExams] DROP CONSTRAINT [FK_UserRecentExams_Exams]
GO


ALTER TABLE [dbo].[AssignedUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_AssignedUserRoles_Exams] FOREIGN KEY([ExamID])
REFERENCES [dbo].[Exams] ([ID])
GO

ALTER TABLE [dbo].[AssignedUserRoles] CHECK CONSTRAINT [FK_AssignedUserRoles_Exams]
GO

ALTER TABLE [dbo].[UserRecentExams]  WITH CHECK ADD  CONSTRAINT [FK_UserRecentExams_Exams] FOREIGN KEY([ExamId])
REFERENCES [dbo].[Exams] ([ID])
GO

ALTER TABLE [dbo].[UserRecentExams] CHECK CONSTRAINT [FK_UserRecentExams_Exams]
GO

DROP TABLE [Exams]








ALTER TABLE [dbo].[UserQualifiedStatusAudit] DROP CONSTRAINT [FK_UserQualifiedStatusAudit_Items]
GO

ALTER TABLE [dbo].[UserQualifiedStatusAudit]  WITH CHECK ADD  CONSTRAINT [FK_UserQualifiedStatusAudit_Items] FOREIGN KEY([ItemID])
REFERENCES [dbo].[Items] ([ID])
GO
ALTER TABLE [dbo].[UserQualifiedStatusAudit] CHECK CONSTRAINT [FK_UserQualifiedStatusAudit_Items]
GO

DROP TABLE [Items]




ALTER TABLE [dbo].[UniqueGroupResponseParkedReasonCrossRef] DROP CONSTRAINT [FK_UniqueGroupResponseParkedReasonCrossRef_UniqueGroupResponseParkedReasonCrossRef]
GO

ALTER TABLE [dbo].[UniqueGroupResponseParkedReasonCrossRef]  WITH CHECK ADD  CONSTRAINT [FK_UniqueGroupResponseParkedReasonCrossRef_UniqueGroupResponseParkedReasonCrossRef] FOREIGN KEY([UniqueGroupResponseId])
REFERENCES [dbo].[UniqueGroupResponses] ([ID])
GO

ALTER TABLE [dbo].[UniqueGroupResponseParkedReasonCrossRef] CHECK CONSTRAINT [FK_UniqueGroupResponseParkedReasonCrossRef_UniqueGroupResponseParkedReasonCrossRef]
GO





DROP TABLE [UniqueGroupResponses]

*/



--DROP TABLE [AffectedCandidateExamVersions]
--DROP TABLE [AssignedGroupMarks]
--DROP TABLE [AssignedItemMarks]
--DROP TABLE [CandidateExamVersions]
--DROP TABLE [CandidateExamVersionStatuses]
--DROP TABLE [CandidateGroupResponses]
--DROP TABLE [CandidateResponses]
--DROP TABLE [Exams]
--DROP TABLE [ExamSectionItems]
--DROP TABLE [ExamSections]
--DROP TABLE [ExamVersionItems]
--DROP TABLE [ExamVersionItemsRef]
--DROP TABLE [ExamVersions]
--DROP TABLE [ExamVersionStructureItems]
--DROP TABLE [ExamVersionStructures]
--DROP TABLE [GroupDefinitionItems]
--DROP TABLE [GroupDefinitions]
--DROP TABLE [GroupDefinitionStructureCrossRef]
--DROP TABLE [Items]
--DROP TABLE [Moderations]
--DROP TABLE [QuotaManagement]
--DROP TABLE [UniqueGroupResponseLinks]
--DROP TABLE [UniqueGroupResponses]
--DROP TABLE [UniqueResponseDocuments]
--DROP TABLE [UniqueResponses]
--DROP TABLE [UserCompetenceStatus]



exec sp_rename 'temp_AffectedCandidateExamVersions','AffectedCandidateExamVersions'
exec sp_rename 'temp_AssignedGroupMarks','AssignedGroupMarks'
exec sp_rename 'temp_AssignedItemMarks','AssignedItemMarks'
exec sp_rename 'temp_CandidateExamVersions','CandidateExamVersions'
exec sp_rename 'temp_CandidateExamVersionStatuses','CandidateExamVersionStatuses'
exec sp_rename 'temp_CandidateGroupResponses','CandidateGroupResponses'
exec sp_rename 'temp_CandidateResponses','CandidateResponses'
exec sp_rename 'temp_Exams','Exams'
exec sp_rename 'temp_ExamSectionItems','ExamSectionItems'
exec sp_rename 'temp_ExamSections','ExamSections'
exec sp_rename 'temp_ExamVersionItems','ExamVersionItems'
exec sp_rename 'temp_ExamVersionItemsRef','ExamVersionItemsRef'
exec sp_rename 'temp_ExamVersions','ExamVersions'
exec sp_rename 'temp_ExamVersionStructureItems','ExamVersionStructureItems'
exec sp_rename 'temp_ExamVersionStructures','ExamVersionStructures'
exec sp_rename 'temp_GroupDefinitionItems','GroupDefinitionItems'
exec sp_rename 'temp_GroupDefinitions','GroupDefinitions'
exec sp_rename 'temp_GroupDefinitionStructureCrossRef','GroupDefinitionStructureCrossRef'
exec sp_rename 'temp_Items','Items'
exec sp_rename 'temp_Moderations','Moderations'
exec sp_rename 'temp_QuotaManagement','QuotaManagement'
exec sp_rename 'temp_UniqueGroupResponseLinks','UniqueGroupResponseLinks'
exec sp_rename 'temp_UniqueGroupResponses','UniqueGroupResponses'
exec sp_rename 'temp_UniqueResponseDocuments','UniqueResponseDocuments'
exec sp_rename 'temp_UniqueResponses','UniqueResponses'
exec sp_rename 'temp_UserCompetenceStatus','UserCompetenceStatus'




/*
select 
	distinct
	o.name
	, fk.name
	, 'ALTER TABLE ' + QUOTENAME(o.name) + ' DROP CONSTRAINT ' + QUOTENAME(fk.name) + ';'
from sys.foreign_key_columns FKC
inner join sys.objects o
on fkc.parent_object_id = o.object_id
inner join sys.foreign_keys fk
on fk.parent_object_id = o.object_id
where 'temp_' + o.name in (
		select name
		from sys.tables
	)
order by fk.name desc

SELECT 'DROP TABLE '+ quotename(SUBSTRING(name, CHARINDEX('_', name)+1, 100)) + ''
FROM SYS.Tables
where name like 'temp%'


select  'ALTER TABLE ' + QUOTENAME(o.name) + ' DROP CONSTRAINT ' + QUOTENAME(i.name) + ';' 
from sys.indexes I
inner join sys.objects o
on i.object_id = o.object_id
where is_primary_key = 1
	order by o.name desc

*/
