/*Notes: This script is going to be the long runner. 
    It copies any data associated with script 01. into 
    temporary tables as it's quicker to drop the originals
    rather than delete from them.*/

/*TODO - I fully expect that this will need batching!*/

/*Copy data from Warehouse Scheduled Exams Table*/
SET IDENTITY_INSERT [dbo].[TEMP_WAREHOUSE_ScheduledExamsTable] ON;
INSERT INTO [dbo].[TEMP_WAREHOUSE_ScheduledExamsTable]
    ([ID]
    ,[ScheduledExamID]
    ,[ExamID]
    ,[WAREHOUSECentreID]
    ,[WAREHOUSECreatedBy]
    ,[CreatedDateTime]
    ,[ScheduledStartDateTime]
    ,[ScheduledEndDateTime]
    ,[ActiveStartTime]
    ,[ActiveEndTime]
    ,[qualificationId]
    ,[examName]
    ,[groupState]
    ,[invigilated]
    ,[humanMarked]
    ,[AdvanceContentDownloadTimespanInHours]
    ,[OriginatorID]
    ,[qualificationName]
    ,[examVersionId]
    ,[ExternalReference]
    ,[QualificationRef]
    ,[IsExternal]
    ,[language]
    ,[qualificationLevel]
    ,[purchaseOrder])
    SELECT [ID]
	   ,[ScheduledExamID]
	   ,[ExamID]
	   ,[WAREHOUSECentreID]
	   ,[WAREHOUSECreatedBy]
	   ,[CreatedDateTime]
	   ,[ScheduledStartDateTime]
	   ,[ScheduledEndDateTime]
	   ,[ActiveStartTime]
	   ,[ActiveEndTime]
	   ,[qualificationId]
	   ,[examName]
	   ,[groupState]
	   ,[invigilated]
	   ,[humanMarked]
	   ,[AdvanceContentDownloadTimespanInHours]
	   ,[OriginatorID]
	   ,[qualificationName]
	   ,[examVersionId]
	   ,[ExternalReference]
	   ,[QualificationRef]
	   ,[IsExternal]
	   ,[language]
	   ,[qualificationLevel]
	   ,[purchaseOrder]
    FROM [dbo].[WAREHOUSE_ScheduledExamsTable]
    WHERE [ID] IN (
	   SELECT WAREHOUSE_ExamSessionTable.WAREHOUSEScheduledExamID
	   FROM dbo.WAREHOUSE_ExamSessionTable
	   WHERE WAREHOUSE_ExamSessionTable.ID IN (
		  SELECT TEMP_WAREHOUSEExamsToKeep.ID
		  FROM TEMP_WAREHOUSEExamsToKeep
	   )
    );
SET IDENTITY_INSERT [dbo].[TEMP_WAREHOUSE_ScheduledExamsTable] OFF;

/*Copy data from Warehouse Exam Sessions Table*/
SET IDENTITY_INSERT [dbo].[TEMP_WAREHOUSE_ExamSessionTable] ON;
INSERT INTO [dbo].[TEMP_WAREHOUSE_ExamSessionTable]
    ([ID]
    ,[ExamSessionID]
    ,[WAREHOUSEScheduledExamID]
    ,[WAREHOUSEUserID]
    ,[StructureXML]
    ,[MarkerData]
    ,[KeyCode]
    ,[PreviousExamState]
    ,[pinNumber]
    ,[duration]
    ,[ExamStateChangeAuditXml]
    ,[resultData]
    ,[resultDataFull]
    ,[warehouseTime]
    ,[completionDate]
    ,[CQN]
    ,[examIPAuditData]
    ,[appeal]
    ,[reMarkStatus]
    ,[reMarkUser]
    ,[downloadInformation]
    ,[clientInformation]
    ,[availableForCentreReview]
    ,[ExportToSecureMarker]
    ,[WarehouseExamState]
    ,[AllowPackageDelivery]
    ,[ContainsBTLOffice]
    ,[ExportedToIntegration]
    ,[copiedFromExamSessionID]
    ,[submissionExported]
    ,[EnableOverrideMarking]
    ,[TargetedForVoid])
    SELECT [ID]
	   ,[ExamSessionID]
	   ,[WAREHOUSEScheduledExamID]
	   ,[WAREHOUSEUserID]
	   ,[StructureXML]
	   ,[MarkerData]
	   ,[KeyCode]
	   ,[PreviousExamState]
	   ,[pinNumber]
	   ,[duration]
	   ,[ExamStateChangeAuditXml]
	   ,[resultData]
	   ,[resultDataFull]
	   ,[warehouseTime]
	   ,[completionDate]
	   ,[CQN]
	   ,[examIPAuditData]
	   ,[appeal]
	   ,[reMarkStatus]
	   ,[reMarkUser]
	   ,[downloadInformation]
	   ,[clientInformation]
	   ,[availableForCentreReview]
	   ,[ExportToSecureMarker]
	   ,[WarehouseExamState]
	   ,[AllowPackageDelivery]
	   ,[ContainsBTLOffice]
	   ,[ExportedToIntegration]
	   ,[copiedFromExamSessionID]
	   ,[submissionExported]
	   ,[EnableOverrideMarking]
	   ,[TargetedForVoid]
    FROM [dbo].[WAREHOUSE_ExamSessionTable]
    WHERE [WAREHOUSE_ExamSessionTable].[ID] IN (
	   SELECT TEMP_WAREHOUSEExamsToKeep.ID
	   FROM TEMP_WAREHOUSEExamsToKeep
    );
SET IDENTITY_INSERT [dbo].[TEMP_WAREHOUSE_ExamSessionTable] OFF;

/*Copy data from Warehouse Exam Session Available Items Table*/
SET IDENTITY_INSERT [dbo].[TEMP_WAREHOUSE_ExamSessionAvailableItemsTable] ON;
INSERT INTO [dbo].[TEMP_WAREHOUSE_ExamSessionAvailableItemsTable]
    ([ID]
    ,[ExamSessionID]
    ,[ItemID]
    ,[ItemVersion]
    ,[ItemXML])
    SELECT [ID]
	   ,[ExamSessionID]
	   ,[ItemID]
	   ,[ItemVersion]
	   ,[ItemXML]
    FROM [dbo].[WAREHOUSE_ExamSessionAvailableItemsTable]
    WHERE [WAREHOUSE_ExamSessionAvailableItemsTable].[ExamSessionID] IN (
	   SELECT TEMP_WAREHOUSEExamsToKeep.ID
	   FROM dbo.TEMP_WAREHOUSEExamsToKeep
    );
SET IDENTITY_INSERT [dbo].[TEMP_WAREHOUSE_ExamSessionAvailableItemsTable] OFF;

/*Copy data from Warehouse Exam StateAudit Table*/
INSERT INTO [dbo].[TEMP_WAREHOUSE_ExamStateAuditTable]
    ([WarehouseExamSessionID]
    ,[ExamState]
    ,[Date])
    SELECT [WarehouseExamSessionID]
	   ,[ExamState]
	   ,[Date]
    FROM [dbo].[WAREHOUSE_ExamStateAuditTable]
    WHERE [WAREHOUSE_ExamStateAuditTable].[WarehouseExamSessionID] IN (
	   SELECT TEMP_WAREHOUSEExamsToKeep.ID
	   FROM dbo.TEMP_WAREHOUSEExamsToKeep
    );

/*Copy data from Warehouse Exam Session Documents Table*/
INSERT INTO [dbo].[TEMP_WAREHOUSE_ExamSessionDocumentTable]
    ([ID]
    ,[warehouseExamSessionID]
    ,[itemId]
    ,[documentName]
    ,[Document]
    ,[uploadDate])
    SELECT [ID]
	   ,[warehouseExamSessionID]
	   ,[itemId]
	   ,[documentName]
	   ,[Document]
	   ,[uploadDate]
    FROM [dbo].[WAREHOUSE_ExamSessionDocumentTable]
    WHERE [WAREHOUSE_ExamSessionDocumentTable].[warehouseExamSessionID] IN (
	   SELECT TEMP_WAREHOUSEExamsToKeep.ID
	   FROM dbo.TEMP_WAREHOUSEExamsToKeep
    );

/*Copy data from Warehouse Exam Session Item Responses Table*/
SET IDENTITY_INSERT [dbo].[TEMP_WAREHOUSE_ExamSessionItemResponseTable] ON;
INSERT INTO [dbo].[TEMP_WAREHOUSE_ExamSessionItemResponseTable]
    ([ID]
    ,[WAREHOUSEExamSessionID]
    ,[ItemID]
    ,[ItemVersion]
    ,[ItemResponseData]
    ,[MarkerResponseData]
    ,[ItemMark]
    ,[DerivedResponse]
    ,[ShortDerivedResponse]
    ,[MarkingIgnored])
    SELECT [ID]
	   ,[WAREHOUSEExamSessionID]
	   ,[ItemID]
	   ,[ItemVersion]
	   ,[ItemResponseData]
	   ,[MarkerResponseData]
	   ,[ItemMark]
	   ,[DerivedResponse]
	   ,[ShortDerivedResponse]
	   ,[MarkingIgnored]
    FROM [dbo].[WAREHOUSE_ExamSessionItemResponseTable]
    WHERE [WAREHOUSE_ExamSessionItemResponseTable].[WAREHOUSEExamSessionID] IN (
	   SELECT TEMP_WAREHOUSEExamsToKeep.ID
	   FROM dbo.TEMP_WAREHOUSEExamsToKeep
    );
SET IDENTITY_INSERT [dbo].[TEMP_WAREHOUSE_ExamSessionItemResponseTable] OFF;

/*Copy data from Warehouse Exam Session Result History Table*/
SET IDENTITY_INSERT [dbo].[TEMP_WAREHOUSE_ExamSessionResultHistoryTable] ON;
INSERT INTO [dbo].[TEMP_WAREHOUSE_ExamSessionResultHistoryTable]
    ([ID]
    ,[resultData]
    ,[resultDataFull]
    ,[warehouseTime]
    ,[WareHouseExamSessionTableID]
    ,[UserID])
    SELECT [ID]
    ,[resultData]
    ,[resultDataFull]
    ,[warehouseTime]
    ,[WareHouseExamSessionTableID]
    ,[UserID]
    FROM [dbo].[WAREHOUSE_ExamSessionResultHistoryTable]
    WHERE [WAREHOUSE_ExamSessionResultHistoryTable].[WareHouseExamSessionTableID] IN (
	   SELECT TEMP_WAREHOUSEExamsToKeep.ID
	   FROM dbo.TEMP_WAREHOUSEExamsToKeep
    );
SET IDENTITY_INSERT [dbo].[TEMP_WAREHOUSE_ExamSessionResultHistoryTable] OFF;

/*Copy data from Warehouse Shredded Exam Sessions Table*/
INSERT INTO [dbo].[TEMP_WAREHOUSE_ExamSessionTable_Shreded]
    ([examSessionId]
    ,[structureXml]
    ,[examVersionName]
    ,[examVersionRef]
    ,[examVersionId]
    ,[examName]
    ,[examRef]
    ,[qualificationid]
    ,[qualificationName]
    ,[qualificationRef]
    ,[resultData]
    ,[submittedDate]
    ,[originatorId]
    ,[centreName]
    ,[centreCode]
    ,[foreName]
    ,[dateOfBirth]
    ,[gender]
    ,[candidateRef]
    ,[surName]
    ,[scheduledDurationValue]
    ,[previousExamState]
    ,[examStateInformation]
    ,[examResult]
    ,[passValue]
    ,[closeValue]
    ,[ExternalReference]
    ,[examType]
    ,[warehouseTime]
    ,[CQN]
    ,[actualDuration]
    ,[appeal]
    ,[reMarkStatus]
    ,[qualificationLevel]
    ,[centreId]
    ,[ULN]
    ,[AllowPackageDelivery]
    ,[ExportToSecureMarker]
    ,[ContainsBTLOffice]
    ,[ExportedToIntegration]
    ,[WarehouseExamState]
    ,[language]
    ,[KeyCode]
    ,[TargetedForVoid]
    ,[EnableOverrideMarking]
    ,[IsExternal]
    ,[AddressLine1]
    ,[AddressLine2]
    ,[Town]
    ,[County]
    ,[Country]
    ,[Postcode]
    ,[ScoreBoundaryData])
    SELECT [examSessionId]
	   ,[structureXml]
	   ,[examVersionName]
	   ,[examVersionRef]
	   ,[examVersionId]
	   ,[examName]
	   ,[examRef]
	   ,[qualificationid]
	   ,[qualificationName]
	   ,[qualificationRef]
	   ,[resultData]
	   ,[submittedDate]
	   ,[originatorId]
	   ,[centreName]
	   ,[centreCode]
	   ,[foreName]
	   ,[dateOfBirth]
	   ,[gender]
	   ,[candidateRef]
	   ,[surName]
	   ,[scheduledDurationValue]
	   ,[previousExamState]
	   ,[examStateInformation]
	   ,[examResult]
	   ,[passValue]
	   ,[closeValue]
	   ,[ExternalReference]
	   ,[examType]
	   ,[warehouseTime]
	   ,[CQN]
	   ,[actualDuration]
	   ,[appeal]
	   ,[reMarkStatus]
	   ,[qualificationLevel]
	   ,[centreId]
	   ,[ULN]
	   ,[AllowPackageDelivery]
	   ,[ExportToSecureMarker]
	   ,[ContainsBTLOffice]
	   ,[ExportedToIntegration]
	   ,[WarehouseExamState]
	   ,[language]
	   ,[KeyCode]
	   ,[TargetedForVoid]
	   ,[EnableOverrideMarking]
	   ,[IsExternal]
	   ,[AddressLine1]
	   ,[AddressLine2]
	   ,[Town]
	   ,[County]
	   ,[Country]
	   ,[Postcode]
	   ,[ScoreBoundaryData]
    FROM [dbo].[WAREHOUSE_ExamSessionTable_Shreded]
    WHERE [WAREHOUSE_ExamSessionTable_Shreded].[examSessionId] IN (
	   SELECT TEMP_WAREHOUSEExamsToKeep.ID
	   FROM dbo.TEMP_WAREHOUSEExamsToKeep
    );

/*Copy data from Warehouse Shredded Exam Sessions Items Table*/
SET IDENTITY_INSERT [dbo].[TEMP_WAREHOUSE_ExamSessionTable_ShrededItems] ON;
INSERT INTO [dbo].[TEMP_WAREHOUSE_ExamSessionTable_ShrededItems]
    ([examSessionId]
    ,[ItemRef]
    ,[ItemName]
    ,[userMark]
    ,[markerUserMark]
    ,[examPercentage]
    ,[candidateId]
    ,[ItemVersion]
    ,[responsexml]
    ,[OptionsChosen]
    ,[selectedCount]
    ,[correctAnswerCount]
    ,[TotalMark]
    ,[userAttempted]
    ,[markingIgnored]
    ,[markedMetadataCount]
    ,[ItemType]
    ,[FriendItems])
    SELECT [examSessionId]
	   ,[ItemRef]
	   ,[ItemName]
	   ,[userMark]
	   ,[markerUserMark]
	   ,[examPercentage]
	   ,[candidateId]
	   ,[ItemVersion]
	   ,[responsexml]
	   ,[OptionsChosen]
	   ,[selectedCount]
	   ,[correctAnswerCount]
	   ,[TotalMark]
	   ,[userAttempted]
	   ,[markingIgnored]
	   ,[markedMetadataCount]
	   ,[ItemType]
	   ,[FriendItems]
    FROM [dbo].[WAREHOUSE_ExamSessionTable_ShrededItems]
    WHERE [WAREHOUSE_ExamSessionTable_ShrededItems].[examSessionId] IN (
	   SELECT TEMP_WAREHOUSEExamsToKeep.ID
	   FROM dbo.TEMP_WAREHOUSEExamsToKeep
    );
SET IDENTITY_INSERT [dbo].[TEMP_WAREHOUSE_ExamSessionTable_ShrededItems] OFF;

/*Copy data from Warehouse Shredded Exam Sessions Item Marks Table*/
INSERT INTO [dbo].[TEMP_WAREHOUSE_ExamSessionTable_ShreddedItems_Mark]
    ([ExamSessionID]
    ,[ItemID]
    ,[Mark]
    ,[LearningOutcome]
    ,[DisplayText]
    ,[MaxMark])
    SELECT [ExamSessionID]
	   ,[ItemID]
	   ,[Mark]
	   ,[LearningOutcome]
	   ,[DisplayText]
	   ,[MaxMark]
    FROM [dbo].[WAREHOUSE_ExamSessionTable_ShreddedItems_Mark]
    WHERE [WAREHOUSE_ExamSessionTable_ShreddedItems_Mark].[ExamSessionID] IN (
	   SELECT TEMP_WAREHOUSEExamsToKeep.ID
	   FROM dbo.TEMP_WAREHOUSEExamsToKeep
    );

/*TODO - Re-Seed new tables*/
/*Not entirely sure we'll need to do this with identity insert*/
