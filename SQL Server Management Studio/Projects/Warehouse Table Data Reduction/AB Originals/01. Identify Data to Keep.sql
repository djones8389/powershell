/*Notes: This script will select exam session id's to ensure every 
    exam version and every item is included at least once in the 
    warehouse tables upon completion.*/

/*TODO - Optimisation will be needed based on initial data expectations*/

CREATE TABLE TEMP_WAREHOUSEExamsToKeep (ID INT);

/*Get a distinct list of exam versions*/
WITH CTE_Versions AS
(
    SELECT MIN(WAREHOUSE_ExamSessionTable.ID) AS [FirstID]
		,MAX(WAREHOUSE_ExamSessionTable.ID) AS [LastID]
    FROM dbo.WAREHOUSE_ScheduledExamsTable
    INNER JOIN dbo.WAREHOUSE_ExamSessionTable
    ON WAREHOUSE_ScheduledExamsTable.ID = WAREHOUSE_ExamSessionTable.WAREHOUSEScheduledExamID
    GROUP BY QualificationID
	   ,ExamID
	   ,ExamVersionID
)
INSERT INTO TEMP_WAREHOUSEExamsToKeep (ID)
    SELECT FirstID 
    FROM CTE_Versions
    UNION
    SELECT LastID
    FROM CTE_Versions;

--WITH CTE_Versions AS
--(
--    SELECT ROW_NUMBER() OVER(
--	   PARTITION BY
--		   QualificationID
--		  ,ExamID
--		  ,ExamVersionID
--	   ORDER BY 
--		   QualificationID
--		  ,ExamID
--		  ,ExamVersionID
--	   ) AS [N]
--	   ,ID
--	   ,QualificationID
--	   ,ExamID
--	   ,ExamVersionID
--    FROM dbo.WAREHOUSE_ScheduledExamsTable
--)
--INSERT INTO TEMP_WAREHOUSEExamsToKeep (ID)
--    SELECT WAREHOUSE_ExamSessionTable.ID
--    FROM CTE_Versions
--    INNER JOIN dbo.WAREHOUSE_ExamSessionTable
--    ON CTE_Versions.ID = WAREHOUSE_ExamSessionTable.WAREHOUSEScheduledExamID
--    WHERE N = 1;

/*Get a distinct list of items*/
WITH CTE_Items AS
(
	SELECT ROW_NUMBER() OVER(
		  PARTITION BY
			  ItemID
		  ORDER BY
			  ItemID
			 ,WAREHOUSEExamSessionID
	   ) AS [N]
	   ,WAREHOUSEExamSessionID AS [FirstID]
	   ,ItemID
    FROM dbo.WAREHOUSE_ExamSessionItemResponseTable
)
INSERT INTO TEMP_WAREHOUSEExamsToKeep (ID)
    SELECT FirstID 
    FROM CTE_Items
    WHERE N = 1;

WITH CTE_Items AS
(
	SELECT ROW_NUMBER() OVER(
		  PARTITION BY
			  ItemID
		  ORDER BY
			  ItemID
			 ,WAREHOUSEExamSessionID DESC
	   ) AS [N]
	   ,WAREHOUSEExamSessionID AS [LastID]
	   ,ItemID
    FROM dbo.WAREHOUSE_ExamSessionItemResponseTable
)
INSERT INTO TEMP_WAREHOUSEExamsToKeep (ID)
    SELECT LastID 
    FROM CTE_Items
    WHERE N = 1;
--WITH CTE_Items AS
--(
--    SELECT ROW_NUMBER() OVER(
--		  PARTITION BY
--			  ItemID
--		  ORDER BY
--			  ItemID
--	   ) AS [N]
--	   ,WAREHOUSEExamSessionID
--	   ,ItemID
--    FROM dbo.WAREHOUSE_ExamSessionItemResponseTable
--)
--INSERT INTO TEMP_WAREHOUSEExamsToKeep (ID)
--    SELECT WAREHOUSEExamSessionID
--    FROM CTE_Items
--    WHERE N = 1;
