/*Notes: This script create a carbon-copy of the warehouse 
    tables except indexes, foreign keys, defaults etc...*/

/*TODO - I'd like not include computed columns for performance reasons*/

/*Create temp Warehouse Exam Session Available Items table*/
CREATE TABLE [dbo].[TEMP_WAREHOUSE_ExamSessionAvailableItemsTable](
    [ID] [int] IDENTITY(1, 1) NOT NULL,
    [ExamSessionID] [int] NOT NULL,
    [ItemID] [nvarchar](50) NOT NULL,
    [ItemVersion] [int] NOT NULL,
    [ItemXML] [xml] NOT NULL
);

/*Create temp Warehouse Exam Session Document table*/
CREATE TABLE [dbo].[TEMP_WAREHOUSE_ExamSessionDocumentTable](
    [ID] [int] NOT NULL,
    [warehouseExamSessionID] [int] NOT NULL,
    [itemId] [nvarchar](20) NOT NULL,
    [documentName] [nvarchar](200) NOT NULL,
    [Document] [image] NOT NULL,
    [uploadDate] [datetime] NOT NULL
);

/*Create temp Warehouse Exam Session Items Response table*/
CREATE TABLE [dbo].[TEMP_WAREHOUSE_ExamSessionItemResponseTable](
    [ID] [int] IDENTITY(1, 1) NOT NULL,
    [WAREHOUSEExamSessionID] [int] NOT NULL,
    [ItemID] [nvarchar](15) NOT NULL,
    [ItemVersion] [int] NOT NULL,
    [ItemResponseData] [xml] NULL,
    [MarkerResponseData] [xml] NULL,
    [ItemMark] [int] NULL,
    [DerivedResponse] [nvarchar](max) NULL,
    [ShortDerivedResponse] [nvarchar](500) NULL,
    [MarkingIgnored] [tinyint] NULL
);

/*Create temp Warehouse Exam Session Result History table*/
CREATE TABLE [dbo].[TEMP_WAREHOUSE_ExamSessionResultHistoryTable](
    [ID] [int] IDENTITY(1, 1) NOT NULL,
    [resultData] [xml] NULL,
    [resultDataFull] [xml] NULL,
    [warehouseTime] [datetime] NULL,
    [WareHouseExamSessionTableID] [int] NOT NULL,
    [UserID] [int] NULL
);

/*Create temp Warehouse Exam Session table*/
CREATE TABLE [dbo].[TEMP_WAREHOUSE_ExamSessionTable](
	[ID] [int] IDENTITY(1, 1) NOT NULL,
	[ExamSessionID] [int] NOT NULL,
	[WAREHOUSEScheduledExamID] [int] NOT NULL,
	[WAREHOUSEUserID] [int] NOT NULL,
	[StructureXML] [xml](CONTENT [dbo].[structureXmlXsd]) NULL, --Fingers-crossed the schema is going!
	[MarkerData] [xml] NULL,
	[KeyCode] [varchar](12) NOT NULL,
	[PreviousExamState] [int] NOT NULL,
	[pinNumber] [nvarchar](15) NULL,
	[duration] [int] NULL,
	[ExamStateChangeAuditXml] [xml] NOT NULL,
	[resultData] [xml] NULL,
	[resultDataFull] [xml] NULL,
	[warehouseTime] [datetime] NULL,
	[completionDate] [datetime] NULL,
	[CQN] [nvarchar](20) NULL,
	[examIPAuditData] [xml] NULL,
	[appeal] [bit] NULL,
	[reMarkStatus] [int] NOT NULL,
	[reMarkUser] [int] NULL,
	[downloadInformation] [xml] NULL,
	[clientInformation] [xml] NULL,
	[availableForCentreReview] [bit] NULL,
	[ExportToSecureMarker] [bit] NOT NULL,
	[WarehouseExamState] [int] NOT NULL,
	[AllowPackageDelivery] [bit] NOT NULL,
	[ContainsBTLOffice] [bit] NULL,
	[ExportedToIntegration] [bit] NULL,
	[copiedFromExamSessionID] [int] NULL,
	[submissionExported] [bit] NOT NULL,
	[EnableOverrideMarking] [bit] NOT NULL,
	[TargetedForVoid] [xml] NULL
);

/*Create temp Warehouse Shredded Exam Session Items Mark table*/
CREATE TABLE [dbo].[TEMP_WAREHOUSE_ExamSessionTable_ShreddedItems_Mark](
    [ExamSessionID] [int] NOT NULL,
    [ItemID] [varchar](15) NOT NULL,
    [Mark] [decimal](6, 3) NOT NULL,
    [LearningOutcome] [int] NOT NULL,
    [DisplayText] [nvarchar](max) NOT NULL,
    [MaxMark] [decimal](6, 3) NOT NULL
);

/*Create temp Warehouse Shredded Exam Session table*/
CREATE TABLE [dbo].[TEMP_WAREHOUSE_ExamSessionTable_Shreded](
    [examSessionId] [int] NOT NULL,
    [structureXml] [xml](CONTENT [dbo].[structureXmlXsd]) NULL, --Fingers-crossed this column is going!
    [examVersionName] [nvarchar](100) NULL,
    [examVersionRef] [nvarchar](100) NULL,
    [examVersionId] [int] NULL,
    [examName] [nvarchar](200) NULL,
    [examRef] [nvarchar](100) NULL,
    [qualificationid] [int] NULL,
    [qualificationName] [nvarchar](100) NULL,
    [qualificationRef] [nvarchar](100) NULL,
    [resultData] [xml] NULL,
    [submittedDate] [datetime] NULL,
    [originatorId] [int] NULL,
    [centreName] [nvarchar](100) NULL,
    [centreCode] [nvarchar](50) NULL,
    [foreName] [nvarchar](50) NULL,
    [dateOfBirth] [smalldatetime] NULL,
    [gender] [char](1) NULL,
    [candidateRef] [nvarchar](300) NULL,
    [surName] [nvarchar](50) NULL,
    [scheduledDurationValue] [int] NULL,
    [previousExamState] [int] NULL,
    [examStateInformation] [xml] NULL,
    [examResult] [float] NULL,
    [passValue] [bit] NULL,
    [closeValue] [bit] NULL,
    [ExternalReference] [nvarchar](100) NULL,
    [examType] [int] NULL,
    [warehouseTime] [datetime] NULL,
    [CQN] [nvarchar](20) NULL,
    [actualDuration] [int] NULL,
    [appeal] [bit] NULL,
    [reMarkStatus] [int] NULL,
    [qualificationLevel] [int] NULL,
    [centreId] [int] NULL,
    [ULN] [nvarchar](10) NULL,
    [AllowPackageDelivery] [bit] NOT NULL,
    [ExportToSecureMarker] [bit] NOT NULL,
    [ContainsBTLOffice] [bit] NULL,
    [ExportedToIntegration] [bit] NULL,
    [WarehouseExamState] [int] NOT NULL,
    [grade] AS (case when [previousExamState]=(10) then 'Voided' when NOT isnull([dbo].[fn_getValueFromResultData]([resultData],'GRADE'),'')='' then [dbo].[fn_getValueFromResultData]([resultData],'GRADE') when CONVERT([int],[dbo].[fn_getValueFromResultData]([resultData],'PASSVALUE'),(0))=(1) then 'Pass' when CONVERT([int],[dbo].[fn_getValueFromResultData]([resultData],'PASSVALUE'),(0))=(0) AND CONVERT([int],[dbo].[fn_getValueFromResultData]([resultData],'CLOSEVALUE'),(0))=(1) then 'Close' else 'Fail' end) PERSISTED,
    [userMark] AS (CONVERT([float],[dbo].[fn_getValueFromResultData]([resultData],'USERMARK'),(0))) PERSISTED,
    [userPercentage] AS (CONVERT([float],[dbo].[fn_getValueFromResultData]([resultData],'USERPERCENTAGE'),(0))) PERSISTED,
    [adjustedGrade] AS (case when NOT isnull([dbo].[fn_getValueFromResultData]([resultData],'ORIGINALGRADE'),[dbo].[fn_getValueFromResultData]([resultData],'GRADE'))=[dbo].[fn_getValueFromResultData]([resultData],'GRADE') then case when NOT isnull([dbo].[fn_getValueFromResultData]([resultData],'GRADE'),'')='' then [dbo].[fn_getValueFromResultData]([resultData],'GRADE') when CONVERT([int],[dbo].[fn_getValueFromResultData]([resultData],'PASSVALUE'),(0))=(1) then 'Pass' when CONVERT([int],[dbo].[fn_getValueFromResultData]([resultData],'PASSVALUE'),(0))=(0) AND CONVERT([int],[dbo].[fn_getValueFromResultData]([resultData],'CLOSEVALUE'),(0))=(1) then 'Close' else 'Fail' end else '' end) PERSISTED,
    [language] [nchar](10) NULL,
    [KeyCode] [varchar](12) NOT NULL,
    [TargetedForVoid] [xml] NULL,
    [EnableOverrideMarking] [bit] NOT NULL,
    [originalGrade] AS (case when NOT isnull([dbo].[fn_getValueFromResultData]([resultData],'ORIGINALGRADE'),[dbo].[fn_getValueFromResultData]([resultData],'GRADE'))=[dbo].[fn_getValueFromResultData]([resultData],'GRADE') then [dbo].[fn_getValueFromResultData]([resultData],'ORIGINALGRADE') else case when [previousExamState]=(10) then 'Voided' when NOT isnull([dbo].[fn_getValueFromResultData]([resultData],'GRADE'),'')='' then [dbo].[fn_getValueFromResultData]([resultData],'GRADE') when CONVERT([int],[dbo].[fn_getValueFromResultData]([resultData],'PASSVALUE'),(0))=(1) then 'Pass' when CONVERT([int],[dbo].[fn_getValueFromResultData]([resultData],'PASSVALUE'),(0))=(0) AND CONVERT([int],[dbo].[fn_getValueFromResultData]([resultData],'CLOSEVALUE'),(0))=(1) then 'Close' else 'Fail' end end),
    [IsExternal] [bit] NOT NULL,
    [AddressLine1] [nvarchar](100) NULL,
    [AddressLine2] [nvarchar](100) NULL,
    [Town] [nvarchar](100) NULL,
    [County] [nvarchar](70) NULL,
    [Country] [nvarchar](50) NULL,
    [Postcode] [nvarchar](12) NULL,
    [ScoreBoundaryData] [xml] NULL
);

/*Create temp Warehouse Shredded Exam Session Items table*/
CREATE TABLE [dbo].[TEMP_WAREHOUSE_ExamSessionTable_ShrededItems](
    [examSessionId] [int] NOT NULL,
    [ItemRef] [varchar](15) NULL,
    [ItemName] [nvarchar](200) NULL,
    [userMark] [decimal](6, 3) NULL,
    [markerUserMark] [nvarchar](max) NULL,
    [examPercentage] [decimal](6, 3) NULL,
    [candidateId] [int] IDENTITY(1, 1) NOT NULL,
    [ItemVersion] [int] NULL,
    [responsexml] [xml] NULL,
    [OptionsChosen] [varchar](200) NULL,
    [selectedCount] [int] NULL,
    [correctAnswerCount] [int] NULL,
    [TotalMark] [decimal](6, 3) NOT NULL,
    [userAttempted] [bit] NULL,
    [markingIgnored] [tinyint] NULL,
    [markedMetadataCount] [int] NULL,
    [ItemType] [int] NULL,
    [FriendItems] [nvarchar](max) NULL
);

/*Exam State Audit*/
CREATE TABLE [dbo].[TEMP_WAREHOUSE_ExamStateAuditTable](
    [WarehouseExamSessionID] [int] NOT NULL,
    [ExamState] [int] NOT NULL,
    [Date] [datetime] NOT NULL
);

/*Create temp Warehouse Scheduled Exams table*/
CREATE TABLE [dbo].[TEMP_WAREHOUSE_ScheduledExamsTable](
    [ID] [int] IDENTITY(1, 1) NOT NULL,
    [ScheduledExamID] [int] NOT NULL,
    [ExamID] [int] NOT NULL,
    [WAREHOUSECentreID] [int] NOT NULL,
    [WAREHOUSECreatedBy] [int] NOT NULL,
    [CreatedDateTime] [datetime] NOT NULL,
    [ScheduledStartDateTime] [datetime] NOT NULL,
    [ScheduledEndDateTime] [datetime] NOT NULL,
    [ActiveStartTime] [int] NOT NULL,
    [ActiveEndTime] [int] NOT NULL,
    [qualificationId] [int] NOT NULL,
    [examName] [nvarchar](200) NULL,
    [groupState] [int] NOT NULL,
    [invigilated] [bit] NOT NULL,
    [humanMarked] [bit] NOT NULL,
    [AdvanceContentDownloadTimespanInHours] [int] NOT NULL,
    [OriginatorID] [int] NOT NULL,
    [qualificationName] [nvarchar](100) NULL,
    [examVersionId] [int] NULL,
    [ExternalReference] [nvarchar](100) NULL,
    [QualificationRef] [nvarchar](100) NULL,
    [IsExternal] [bit] NOT NULL,
    [language] [nchar](10) NULL,
    [qualificationLevel] [int] NULL,
    [purchaseOrder] [nvarchar](100) NULL
);

/*TODO - Other tables*/