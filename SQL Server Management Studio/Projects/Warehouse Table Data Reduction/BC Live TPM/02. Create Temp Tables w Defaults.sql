if exists (select 1 from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'temp_ScheduledPackageCandidateExamItems')
	DROP TABLE [dbo].[temp_ScheduledPackageCandidateExamItems];
	
	CREATE TABLE [dbo].[temp_ScheduledPackageCandidateExamItems](
	[ScheduledPackageCandidateExamItemId] [int] IDENTITY(1,1) NOT NULL,
	[ExternalItemId] [nvarchar](50) NOT NULL,
	[CandidateExam_ScheduledPackageCandidateExamId] [int] NOT NULL,
	[OriginalMark] [decimal](18, 10) NOT NULL,
	[Mark] [decimal](18, 10) NULL,
	[OriginalTotalMark] [decimal](18, 10) NOT NULL,
	[TotalMark] [decimal](18, 0) NOT NULL,
	[CreateDate] [smalldatetime] NOT NULL
)

if exists (select 1 from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'temp_ScheduledPackageCandidateExams')
	DROP TABLE [dbo].[temp_ScheduledPackageCandidateExams];

CREATE TABLE [dbo].[temp_ScheduledPackageCandidateExams](
	[ScheduledPackageCandidateExamId] [int] IDENTITY(1,1) NOT NULL,
	[ScheduledExamRef] [varchar](100) NULL,
	[PackageExamId] [int] NOT NULL,
	[IsCompleted] [bit] NOT NULL,
	[DateCompleted] [datetime] NULL,
	[Score] [decimal](18, 2) NULL,
	[Candidate_ScheduledPackageCandidateId] [int] NOT NULL,
	[IsVoided] [bit] NOT NULL CONSTRAINT [DF__Scheduled__IsVoi__1A14E395]  DEFAULT ((0)),
	[ShouldIncludeScale] [bit] NOT NULL CONSTRAINT [DF__Scheduled__Shoul__1DE57479]  DEFAULT ((0)),
	[ShouldIncludeCEFR] [bit] NOT NULL CONSTRAINT [DF__Scheduled__Shoul__1ED998B2]  DEFAULT ((0)),
	[ShouldIncludeFinalScore] [bit] NOT NULL CONSTRAINT [DF__Scheduled__Shoul__1FCDBCEB]  DEFAULT ((0)),
	[IsRescheduled] [bit] NOT NULL DEFAULT ((0)),
	[ExamVersionRef] [nvarchar](100) NULL,
	[IsLocalScan] [bit] NOT NULL CONSTRAINT [DF_ScheduledPackageCandidateExams_IsLocalScan]  DEFAULT ((0)),
	[ResultXml] [nvarchar](max) NULL,
	[ExamCompletionDate]  AS (CONVERT([datetimeoffset],isnull([DateCompleted],getdate()),(0)))
)

if exists (select 1 from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'temp_ScheduledPackageCandidates')
	DROP TABLE [dbo].[temp_ScheduledPackageCandidates];

CREATE TABLE [dbo].[temp_ScheduledPackageCandidates] (
	[ScheduledPackageCandidateId] [int] IDENTITY(1,1) NOT NULL,
	[SurpassCandidateId] [int] NOT NULL,
	[SurpassCandidateRef] [nvarchar](400) NULL,
	[FirstName] [nvarchar](400) NULL,
	[LastName] [nvarchar](400) NULL,
	[MiddleName] [nvarchar](400) NULL,
	[BirthDate] [datetime] NOT NULL,
	[IsCompleted] [bit] NOT NULL,
	[DateCompleted] [datetime] NULL,
	[PackageScore] [decimal](18, 2) NULL,
	[ScheduledPackage_ScheduledPackageId] [int] NOT NULL,
	[IsVoided] [bit] NOT NULL CONSTRAINT [DF__Scheduled__IsVoi__1920BF5C]  DEFAULT ((0)),
	[IsLocalScan] [bit] NOT NULL CONSTRAINT [DF_ScheduledPackageCandidates_IsLocalScan]  DEFAULT ((0)),
	[TestPackageCompletionDate]  AS (CONVERT([datetimeoffset],isnull([DateCompleted],getdate()),(0)))
)

if exists (select 1 from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'temp_ScheduledPackages')
	DROP TABLE [dbo].[temp_ScheduledPackages];

CREATE TABLE [dbo].[temp_ScheduledPackages](
	[ScheduledPackageId] [int] IDENTITY(1,1) NOT NULL,
	[CenterId] [int] NOT NULL,
	[CenterRef] [nvarchar](4000) NULL,
	[CenterName] [nvarchar](4000) NULL,
	[QualificationId] [int] NOT NULL,
	[QualificationRef] [nvarchar](4000) NULL,
	[QualificationName] [nvarchar](4000) NULL,
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NULL,
	[StartTime] [nvarchar](4000) NULL,
	[EndTime] [nvarchar](4000) NULL,
	[DateCreated] [datetime] NOT NULL,
	[CreatedBy] [nvarchar](4000) NULL,
	[StatusValue] [tinyint] NOT NULL,
	[PackageId] [int] NOT NULL,
	[PackageDate] [datetime] NOT NULL CONSTRAINT [DF__Scheduled__Packa__1CF15040]  DEFAULT (getdate())
)