USE [PPD_BCIntegration_TestPackage];

/*
select 
	'ALTER TABLE ' + QUOTENAME(s.Name) + '.' + QUOTENAME(O.Name) 
	+ ' ADD CONSTRAINT ' + QUOTENAME(I.name)
	+ ' PRIMARY KEY ' + I.type_desc  collate Latin1_General_CI_AS

	+ '(' + ISNULL(STUFF((SELECT ', ' + QUOTENAME(c2.name) + 
						CASE ic2.is_descending_key
						WHEN 0 THEN ' ASC'
						ELSE ' DESC'
						END						 
						FROM sys.indexes i2 INNER JOIN sys.index_columns ic2
						ON i2.object_id = ic2.object_id AND i2.index_id = ic2.index_id
						INNER JOIN  sys.columns c2 ON ic2.object_id = c2.object_id AND ic2.column_id = c2.column_id						
						WHERE ic2.object_id = i.object_id AND ic2.index_id = i.index_id
						ORDER BY ic2.object_id	 
						FOR XML PATH(N''), TYPE).value(N'.[1]', N'NVARCHAR(MAX)'), 1, 1, N''), 'HEAP') 
						+ ');'

from sys.indexes I 
inner join sys.objects O
on o.object_id = i.object_id
inner join sys.schemas S
on S.schema_id = o.schema_id
where is_primary_key = 1
 ORDER BY s.Name
 */


--ALTER TABLE [dbo].[UserAccesses] ADD CONSTRAINT [PK_UserAccesses] PRIMARY KEY CLUSTERED( [UserName] ASC);
ALTER TABLE [dbo].[ScheduledPackages] ADD CONSTRAINT [PK_ScheduledPackages] PRIMARY KEY CLUSTERED( [ScheduledPackageId] ASC);
--ALTER TABLE [dbo].[ExamTypeGrades] ADD CONSTRAINT [PK_ExamTypeGrades] PRIMARY KEY CLUSTERED( [ExamTypeGradeId] ASC);
ALTER TABLE [dbo].[ScheduledPackageCandidates] ADD CONSTRAINT [PK_ScheduledPackageCandidates] PRIMARY KEY CLUSTERED( [ScheduledPackageCandidateId] ASC);
ALTER TABLE [dbo].[ScheduledPackageCandidateExams] ADD CONSTRAINT [PK_ScheduledPackageCandidateExams] PRIMARY KEY CLUSTERED( [ScheduledPackageCandidateExamId] ASC);
ALTER TABLE [dbo].[ScheduledPackageCandidateExamItems] ADD CONSTRAINT [PK_ScheduledPackageCandidateExamItems] PRIMARY KEY CLUSTERED( [ScheduledPackageCandidateExamItemId] ASC);
--ALTER TABLE [dbo].[ExamTypes] ADD CONSTRAINT [PK_ExamTypes] PRIMARY KEY CLUSTERED( [ExamTypeId] ASC);
--ALTER TABLE [dbo].[CommonSettings] ADD CONSTRAINT [PK_CommonSettings] PRIMARY KEY CLUSTERED( [Key] ASC);
--ALTER TABLE [dbo].[UserCenterAccesses] ADD CONSTRAINT [PK_UserCenterAccesses] PRIMARY KEY CLUSTERED( [UserName] ASC, [CenterId] ASC);
--ALTER TABLE [dbo].[UserQualificationAccesses] ADD CONSTRAINT [PK_UserQualificationAccesses] PRIMARY KEY CLUSTERED( [UserName] ASC, [QualificationId] ASC);
--ALTER TABLE [dbo].[sysdiagrams] ADD CONSTRAINT [PK__sysdiagr__C2B05B6125869641] PRIMARY KEY CLUSTERED( [diagram_id] ASC);
--ALTER TABLE [dbo].[RescheduleDetails] ADD CONSTRAINT [PK_RescheduleDetails] PRIMARY KEY CLUSTERED( [RescheduleDetailId] ASC);
--ALTER TABLE [dbo].[ExamGrades] ADD CONSTRAINT [PK_ExamGrades] PRIMARY KEY CLUSTERED( [Id] ASC);
--ALTER TABLE [dbo].[PackageGrades] ADD CONSTRAINT [PK_PackageGrades] PRIMARY KEY CLUSTERED( [PackageId] ASC, [ExamGradeId] ASC);
--ALTER TABLE [dbo].[ExamGradeDescriptions] ADD CONSTRAINT [PK_ExamGradeDescriptions] PRIMARY KEY CLUSTERED( [ExamGradeId] ASC, [ExamTypeId] ASC);
--ALTER TABLE [dbo].[ItemScalingQuestions] ADD CONSTRAINT [PK_ItemScalingQuestions] PRIMARY KEY CLUSTERED( [Id] ASC);
--ALTER TABLE [dbo].[PackageExamScoreBoundaries] ADD CONSTRAINT [PK_PackageExamScoreBoundary] PRIMARY KEY CLUSTERED( [PackageExamId] ASC, [ExamGradeId] ASC);
--ALTER TABLE [dbo].[QuestionScalings] ADD CONSTRAINT [PK_QuestionScalings] PRIMARY KEY CLUSTERED( [ItemScalingQuestionId] ASC, [Total] ASC);
--ALTER TABLE [dbo].[PackageExamVersions] ADD CONSTRAINT [PK_PackageExamVersion] PRIMARY KEY CLUSTERED( [PackageExamVersionId] ASC);
--ALTER TABLE [dbo].[Packages] ADD CONSTRAINT [PK_Packages] PRIMARY KEY CLUSTERED( [PackageId] ASC);
--ALTER TABLE [dbo].[ExamVersionScalings] ADD CONSTRAINT [PK_ExamVersionScalings] PRIMARY KEY CLUSTERED( [PackageExamVersionId] ASC, [Total] ASC);
--ALTER TABLE [dbo].[PackageExams] ADD CONSTRAINT [PK_PackageExams] PRIMARY KEY CLUSTERED( [PackageExamId] ASC);
