RESTORE DATABASE [PPD_BCIntegration_TestPackage] FROM DISK = N'T:\FTP\BC\BritishCouncil_TestPackage.2016.04.08.bak' WITH FILE = 1, NOUNLOAD, NOREWIND, REPLACE
	, MEDIAPASSWORD = '', STATS = 10
	, MOVE 'BritishCouncil_TestPackage' TO N'S:\DATA\PPD_BCIntegration_TestPackage.mdf'
	, MOVE 'BritishCouncil_TestPackage_log' TO N'L:\LOGS\PPD_BCIntegration_TestPackage.ldf'; 	