DECLARE @MaxDateTime DateTime = (	
									select max(warehouseTime)
									FROM [PPD_BCIntegration_SecureAssess].dbo.WAREHOUSE_ExamSessionTable
								)


SELECT ID, KeyCode, warehouseTime
INTO [PPD_BCIntegration_SecureAssess].dbo.[DATATOKEEP]
FROM WAREHOUSE_ExamSessionTable
where warehouseTime > DATEADD(MONTH, -3, @MaxDateTime);

SELECT ExternalSessionID, Keycode
INTO [PPD_BCIntegration_SecureMarker].dbo.[DATATOKEEP]
FROM PPD_BCIntegration_SecureMarker.dbo.CandidateExamVersions
where ExternalSessionID in (SELECT ID FROM [PPD_BCIntegration_SecureAssess].dbo.[DATATOKEEP]);

SELECT Candidate_ScheduledPackageCandidateId, ScheduledExamRef
INTO [PPD_BCIntegration_TestPackage].dbo.DATATOKEEP
FROM [PPD_BCIntegration_TestPackage]..ScheduledPackageCandidateExams
where ScheduledExamRef collate Latin1_General_CI_AS in (SELECT KeyCode FROM [PPD_BCIntegration_SecureAssess].dbo.[DATATOKEEP]);


