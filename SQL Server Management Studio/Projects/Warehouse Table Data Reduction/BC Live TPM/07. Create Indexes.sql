USE [PPD_BCIntegration_TestPackage]

CREATE NONCLUSTERED INDEX [IX_ExternalItemId] ON [dbo].[ScheduledPackageCandidateExamItems]
(
	[ExternalItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_Candidate_ScheduledPackageCandidateId]    Script Date: 12/04/2016 11:03:26 ******/
CREATE NONCLUSTERED INDEX [IX_Candidate_ScheduledPackageCandidateId] ON [dbo].[ScheduledPackageCandidateExams]
(
	[Candidate_ScheduledPackageCandidateId] ASC
)
INCLUDE ( 	[ScheduledExamRef]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_PackageExamId]    Script Date: 12/04/2016 11:03:26 ******/
CREATE NONCLUSTERED INDEX [IX_PackageExamId] ON [dbo].[ScheduledPackageCandidateExams]
(
	[PackageExamId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ScheduledPackageCandidateExams_Completed]    Script Date: 12/04/2016 11:03:26 ******/
CREATE NONCLUSTERED INDEX [ScheduledPackageCandidateExams_Completed] ON [dbo].[ScheduledPackageCandidateExams]
(
	[IsCompleted] ASC
)
INCLUDE ( 	[Candidate_ScheduledPackageCandidateId]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ScheduledPackageCandidateExams_Scheduled]    Script Date: 12/04/2016 11:03:26 ******/
CREATE NONCLUSTERED INDEX [ScheduledPackageCandidateExams_Scheduled] ON [dbo].[ScheduledPackageCandidateExams]
(
	[PackageExamId] ASC,
	[ScheduledPackageCandidateExamId] ASC,
	[Candidate_ScheduledPackageCandidateId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UNIQUE_ScheduledExamRef_notnull]    Script Date: 12/04/2016 11:03:26 ******/
CREATE UNIQUE NONCLUSTERED INDEX [UNIQUE_ScheduledExamRef_notnull] ON [dbo].[ScheduledPackageCandidateExams]
(
	[ScheduledExamRef] ASC
)
WHERE ([ScheduledExamRef] IS NOT NULL)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_ScheduledPackage_ScheduledPackageId]    Script Date: 12/04/2016 11:03:26 ******/
CREATE NONCLUSTERED INDEX [IX_ScheduledPackage_ScheduledPackageId] ON [dbo].[ScheduledPackageCandidates]
(
	[ScheduledPackage_ScheduledPackageId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_Package_PackageId]    Script Date: 12/04/2016 11:03:26 ******/
CREATE NONCLUSTERED INDEX [IX_Package_PackageId] ON [dbo].[ScheduledPackages]
(
	[PackageId] ASC
)
INCLUDE ( 	[CenterId],
	[CenterName],
	[CenterRef],
	[CreatedBy],
	[DateCreated],
	[EndDate],
	[EndTime],
	[PackageDate],
	[QualificationId],
	[QualificationName],
	[QualificationRef],
	[StartDate],
	[StartTime],
	[StatusValue]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO