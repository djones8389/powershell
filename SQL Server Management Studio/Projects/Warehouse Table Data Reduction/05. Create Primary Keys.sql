/*Notes: This script created primary keys on the newly 
    renamed tables to bring them inline with originals.*/

/*Create primary keys*/
ALTER TABLE [dbo].[WAREHOUSE_ExamSessionTable_ShreddedItems_Mark] 
    ADD CONSTRAINT [PK_WAREHOUSEExamSessionItemMark] 
    PRIMARY KEY CLUSTERED ([ExamSessionID] ASC, [ItemID] ASC, [LearningOutcome] ASC);

ALTER TABLE [dbo].[WAREHOUSE_ExamSessionTable_ShrededItems] 
    ADD CONSTRAINT [PK_WAREHOUSE_ExamSessionTable_ShrededItems] 
    PRIMARY KEY CLUSTERED ([examSessionId] ASC, [candidateId] ASC);

ALTER TABLE [dbo].[WAREHOUSE_ExamSessionTable_Shreded] 
    ADD CONSTRAINT [PK_WAREHOUSE_ExamSessionTable_Shreded] 
    PRIMARY KEY CLUSTERED ([examSessionId] ASC);

ALTER TABLE [dbo].[WAREHOUSE_ExamSessionAvailableItemsTable] 
    ADD CONSTRAINT [PK_WAREHOUSE_ExamSessionAvailableItemsTable] 
    PRIMARY KEY CLUSTERED ([ID] ASC);

ALTER TABLE [dbo].[WAREHOUSE_ExamSessionItemResponseTable] 
    ADD CONSTRAINT [PK_WAREHOUSEExamSessionItemResponseTable_1] 
    PRIMARY KEY NONCLUSTERED ([ID] ASC);

ALTER TABLE [dbo].[WAREHOUSE_ExamSessionResultHistoryTable] 
    ADD CONSTRAINT [PK_WAREHOUSE_ExamSessionResultHistoryTable] 
    PRIMARY KEY CLUSTERED ([ID] ASC);

ALTER TABLE [dbo].[WAREHOUSE_ExamStateAuditTable] 
    ADD CONSTRAINT [PK_Warehouse_ExamStateAuditTable] 
    PRIMARY KEY CLUSTERED ([WarehouseExamSessionID] ASC, [ExamState] ASC, [Date] ASC);

ALTER TABLE [dbo].[WAREHOUSE_ExamSessionTable] 
    ADD CONSTRAINT [PK_WAREHOUSE_ExamSessionTable_1] 
    PRIMARY KEY CLUSTERED ([ID] ASC);

ALTER TABLE [dbo].[WAREHOUSE_ScheduledExamsTable] 
    ADD CONSTRAINT [PK_WAREHOUSE_ScheduledExamsTable] 
    PRIMARY KEY NONCLUSTERED ([ID] ASC);

