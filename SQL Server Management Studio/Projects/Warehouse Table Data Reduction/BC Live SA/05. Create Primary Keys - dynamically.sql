USE [PPD_BCIntegration_SecureAssess]

ALTER TABLE [dbo].[WAREHOUSE_ExamSessionAvailableItemsTable] ADD CONSTRAINT [PK_WAREHOUSE_ExamSessionAvailableItemsTable] PRIMARY KEY CLUSTERED( [ID] ASC);
ALTER TABLE [dbo].[WAREHOUSE_ExamSessionDocumentTable] ADD CONSTRAINT [PK_WAREHOUSE_ExamSessionDocumentTable] PRIMARY KEY CLUSTERED( [ID] ASC);
ALTER TABLE [dbo].[WAREHOUSE_ExamSessionItemResponseTable] ADD CONSTRAINT [PK_WAREHOUSEExamSessionItemResponseTable_1] PRIMARY KEY CLUSTERED( [ID] ASC);
ALTER TABLE [dbo].[WAREHOUSE_ExamSessionResultHistoryTable] ADD CONSTRAINT [PK_WAREHOUSE_ExamSessionResultHistoryTable] PRIMARY KEY CLUSTERED( [ID] ASC);
ALTER TABLE [dbo].[WAREHOUSE_ExamSessionTable] ADD CONSTRAINT [PK_WAREHOUSE_ExamSessionTable_1] PRIMARY KEY CLUSTERED( [ID] ASC);
ALTER TABLE [dbo].[WAREHOUSE_ExamSessionTable_Shreded] ADD CONSTRAINT [PK_WAREHOUSE_ExamSessionTable_Shreded] PRIMARY KEY CLUSTERED( [examSessionId] ASC);
ALTER TABLE [dbo].[WAREHOUSE_ExamSessionTable_ShrededItems] ADD CONSTRAINT [PK_WAREHOUSE_ExamSessionTable_ShrededItems] PRIMARY KEY CLUSTERED( [examSessionId] ASC, [candidateId] ASC);
ALTER TABLE [dbo].[WAREHOUSE_ExamStateAuditTable] ADD CONSTRAINT [PK_Warehouse_ExamStateAuditTable] PRIMARY KEY CLUSTERED( [WarehouseExamSessionID] ASC, [ExamState] ASC, [Date] ASC);
ALTER TABLE [dbo].[WAREHOUSE_ScheduledExamsTable] ADD CONSTRAINT [PK_WAREHOUSE_ScheduledExamsTable] PRIMARY KEY CLUSTERED( [ID] ASC);






/*
select 
	'ALTER TABLE ' + QUOTENAME(s.Name) + '.' + QUOTENAME(O.Name) 
	+ ' ADD CONSTRAINT ' + QUOTENAME(I.name)
	+ ' PRIMARY KEY ' + I.type_desc  collate Latin1_General_CI_AS

	+ '(' + ISNULL(STUFF((SELECT ', ' + QUOTENAME(c2.name) + 
						CASE ic2.is_descending_key
						WHEN 0 THEN ' ASC'
						ELSE ' DESC'
						END						 
						FROM sys.indexes i2 INNER JOIN sys.index_columns ic2
						ON i2.object_id = ic2.object_id AND i2.index_id = ic2.index_id
						INNER JOIN  sys.columns c2 ON ic2.object_id = c2.object_id AND ic2.column_id = c2.column_id						
						WHERE ic2.object_id = i.object_id AND ic2.index_id = i.index_id
						ORDER BY ic2.object_id	 
						FOR XML PATH(N''), TYPE).value(N'.[1]', N'NVARCHAR(MAX)'), 1, 1, N''), 'HEAP') 
						+ ');'

from sys.indexes I 
inner join sys.objects O
on o.object_id = i.object_id
inner join sys.schemas S
on S.schema_id = o.schema_id
where is_primary_key = 1
 ORDER BY s.Name
 */



