USE [PPD_BCIntegration_SecureAssess]

SET IDENTITY_INSERT [temp_WAREHOUSE_ExamSessionAvailableItemsTable]  ON 

INSERT [temp_WAREHOUSE_ExamSessionAvailableItemsTable] (ID, ExamSessionID, ItemID, ItemVersion, ItemXML)
SELECT ID, ExamSessionID, ItemID, ItemVersion, ItemXML
FROM dbo.[WAREHOUSE_ExamSessionAvailableItemsTable] NOLOCK
where ExamSessionID in (
	SELECT ID
	from [PPD_BCIntegration_SecureAssess].dbo.DATATOKEEP NOLOCK
)

SET IDENTITY_INSERT [temp_WAREHOUSE_ExamSessionAvailableItemsTable]  OFF


--SET IDENTITY_INSERT [temp_WAREHOUSE_ExamSessionDocumentTable]  ON 

INSERT [temp_WAREHOUSE_ExamSessionDocumentTable] (ID, warehouseExamSessionID, itemId, documentName, Document, uploadDate)
SELECT ID, warehouseExamSessionID, itemId, documentName, Document, uploadDate
FROM dbo.[WAREHOUSE_ExamSessionDocumentTable]
where [WAREHOUSE_ExamSessionDocumentTable].warehouseExamSessionID in (
	SELECT ID
	from [PPD_BCIntegration_SecureAssess].dbo.DATATOKEEP
)

--SET IDENTITY_INSERT [temp_WAREHOUSE_ExamSessionDocumentTable]  OFF


SET IDENTITY_INSERT [temp_WAREHOUSE_ExamSessionItemResponseTable]  ON 

INSERT [temp_WAREHOUSE_ExamSessionItemResponseTable] (ID, WAREHOUSEExamSessionID, ItemID, ItemVersion, ItemResponseData, MarkerResponseData, ItemMark, MarkingIgnored, DerivedResponse, ShortDerivedResponse)
SELECT ID, WAREHOUSEExamSessionID, ItemID, ItemVersion, ItemResponseData, MarkerResponseData, ItemMark, MarkingIgnored, DerivedResponse, ShortDerivedResponse
FROM dbo.[WAREHOUSE_ExamSessionItemResponseTable]
where [WAREHOUSE_ExamSessionItemResponseTable].WAREHOUSEExamSessionID in (
	SELECT ID
	from [PPD_BCIntegration_SecureAssess].dbo.DATATOKEEP
)

SET IDENTITY_INSERT [temp_WAREHOUSE_ExamSessionItemResponseTable]  OFF


SET IDENTITY_INSERT [temp_WAREHOUSE_ExamSessionResultHistoryTable]  ON 

INSERT [temp_WAREHOUSE_ExamSessionResultHistoryTable] (ID, resultData, resultDataFull, warehouseTime, WareHouseExamSessionTableID, UserID)
SELECT ID, resultData, resultDataFull, warehouseTime, WareHouseExamSessionTableID, UserID
FROM dbo.[WAREHOUSE_ExamSessionResultHistoryTable]
where [WAREHOUSE_ExamSessionResultHistoryTable].WareHouseExamSessionTableID in (
	SELECT ID
	from [PPD_BCIntegration_SecureAssess].dbo.DATATOKEEP
)

SET IDENTITY_INSERT [temp_WAREHOUSE_ExamSessionResultHistoryTable]  OFF


SET IDENTITY_INSERT [temp_WAREHOUSE_ExamSessionTable]  ON 

INSERT [temp_WAREHOUSE_ExamSessionTable] (ID, ExamSessionID, WAREHOUSEScheduledExamID, WAREHOUSEUserID, StructureXML, MarkerData, KeyCode, PreviousExamState, pinNumber, ExamStateChangeAuditXml, resultData, resultDataFull, warehouseTime, completionDate, CQN, examIPAuditData, appeal, reMarkStatus, reMarkUser, downloadInformation, clientInformation, availableForCentreReview, ExportToSecureMarker, WarehouseExamState, AllowPackageDelivery, ContainsBTLOffice, ExportedToIntegration, copiedFromExamSessionID, submissionExported, EnableOverrideMarking, TargetedForVoid, itemMarksUploaded, TakenThroughLocalScan, LocalScanDownloadDate, LocalScanUploadDate, LocalScanNumPages, CertifiedForTablet, IsProjectBased, AssignedMarkerUserID, AssignedModeratorUserID)
SELECT ID, ExamSessionID, WAREHOUSEScheduledExamID, WAREHOUSEUserID, StructureXML, MarkerData, KeyCode, PreviousExamState, pinNumber, ExamStateChangeAuditXml, resultData, resultDataFull, warehouseTime, completionDate, CQN, examIPAuditData, appeal, reMarkStatus, reMarkUser, downloadInformation, clientInformation, availableForCentreReview, ExportToSecureMarker, WarehouseExamState, AllowPackageDelivery, ContainsBTLOffice, ExportedToIntegration, copiedFromExamSessionID, submissionExported, EnableOverrideMarking, TargetedForVoid, itemMarksUploaded, TakenThroughLocalScan, LocalScanDownloadDate, LocalScanUploadDate, LocalScanNumPages, CertifiedForTablet, IsProjectBased, AssignedMarkerUserID, AssignedModeratorUserID
FROM dbo.[WAREHOUSE_ExamSessionTable]
where [WAREHOUSE_ExamSessionTable].ID in (
	SELECT ID
	from [PPD_BCIntegration_SecureAssess].dbo.DATATOKEEP
)

SET IDENTITY_INSERT [temp_WAREHOUSE_ExamSessionTable]  OFF


--SET IDENTITY_INSERT [temp_WAREHOUSE_ExamSessionTable_Shreded]  ON 

INSERT [temp_WAREHOUSE_ExamSessionTable_Shreded] (examSessionId, structureXml, examVersionName, examVersionRef, examVersionId, examName, examRef, qualificationid, qualificationName, qualificationRef, resultData, submittedDate, originatorId, centreName, centreCode, foreName, dateOfBirth, gender, candidateRef, surName, scheduledDurationValue, previousExamState, examStateInformation, examResult, passValue, closeValue, ExternalReference, examType, warehouseTime, CQN, actualDuration, appeal, reMarkStatus, qualificationLevel, centreId, ULN, AllowPackageDelivery, ExportToSecureMarker, ContainsBTLOffice, ExportedToIntegration, WarehouseExamState,  language, KeyCode, TargetedForVoid, EnableOverrideMarking,  IsExternal, AddressLine1, AddressLine2, Town, County, Country, Postcode, ScoreBoundaryData, itemMarksUploaded, passMark, totalMark, passType, voidJustificationLookupTableId, automaticVerification, resultSampled, started, submitted, validFromDate, expiryDate, totalTimeSpent, defaultDuration, scheduledDurationReason, WAREHOUSEScheduledExamID, WAREHOUSEUserID, itemDataFull, examId, userId, TakenThroughLocalScan, LocalScanDownloadDate, LocalScanUploadDate, LocalScanNumPages, CertifiedForTablet, IsProjectBased, AssignedMarkerUserID, AssignedModeratorUserID, originalGradewithVoidReason, ValidationCount)
SELECT examSessionId, structureXml, examVersionName, examVersionRef, examVersionId, examName, examRef, qualificationid, qualificationName, qualificationRef, resultData, submittedDate, originatorId, centreName, centreCode, foreName, dateOfBirth, gender, candidateRef, surName, scheduledDurationValue, previousExamState, examStateInformation, examResult, passValue, closeValue, ExternalReference, examType, warehouseTime, CQN, actualDuration, appeal, reMarkStatus, qualificationLevel, centreId, ULN, AllowPackageDelivery, ExportToSecureMarker, ContainsBTLOffice, ExportedToIntegration, WarehouseExamState,  language, KeyCode, TargetedForVoid, EnableOverrideMarking,  IsExternal, AddressLine1, AddressLine2, Town, County, Country, Postcode, ScoreBoundaryData, itemMarksUploaded, passMark, totalMark, passType, voidJustificationLookupTableId, automaticVerification, resultSampled, started, submitted, validFromDate, expiryDate, totalTimeSpent, defaultDuration, scheduledDurationReason, WAREHOUSEScheduledExamID, WAREHOUSEUserID, itemDataFull, examId, userId, TakenThroughLocalScan, LocalScanDownloadDate, LocalScanUploadDate, LocalScanNumPages, CertifiedForTablet, IsProjectBased, AssignedMarkerUserID, AssignedModeratorUserID, originalGradewithVoidReason, ValidationCount
FROM dbo.[WAREHOUSE_ExamSessionTable_Shreded]
where [WAREHOUSE_ExamSessionTable_Shreded].ExamSessionID in (
	SELECT ID
	from [PPD_BCIntegration_SecureAssess].dbo.DATATOKEEP
)

--SET IDENTITY_INSERT [temp_WAREHOUSE_ExamSessionTable_Shreded] OFF


SET IDENTITY_INSERT [temp_WAREHOUSE_ExamSessionTable_ShrededItems]  ON 

INSERT [temp_WAREHOUSE_ExamSessionTable_ShrededItems] (examSessionId, ItemRef, ItemName, userMark, markerUserMark, examPercentage, candidateId, ItemVersion, responsexml, OptionsChosen, selectedCount, correctAnswerCount, TotalMark, userAttempted, markingIgnored, markedMetadataCount, ItemType, FriendItems)
SELECT examSessionId, ItemRef, ItemName, userMark, markerUserMark, examPercentage, candidateId, ItemVersion, responsexml, OptionsChosen, selectedCount, correctAnswerCount, TotalMark, userAttempted, markingIgnored, markedMetadataCount, ItemType, FriendItems
FROM dbo.[WAREHOUSE_ExamSessionTable_ShrededItems]
where [WAREHOUSE_ExamSessionTable_ShrededItems].ExamSessionID in (
	SELECT ID
	from [PPD_BCIntegration_SecureAssess].dbo.DATATOKEEP
)

SET IDENTITY_INSERT [temp_WAREHOUSE_ExamSessionTable_ShrededItems] OFF

SET IDENTITY_INSERT [temp_WAREHOUSE_ExamStateAuditTable]  ON 


INSERT [temp_WAREHOUSE_ExamStateAuditTable] (WarehouseExamSessionID, ExamState, Date)
SELECT WarehouseExamSessionID, ExamState, Date
FROM dbo.[WAREHOUSE_ExamStateAuditTable]
where [WAREHOUSE_ExamStateAuditTable].WarehouseExamSessionID in (
	SELECT ID
	from [PPD_BCIntegration_SecureAssess].dbo.DATATOKEEP
)

SET IDENTITY_INSERT [temp_WAREHOUSE_ExamStateAuditTable] OFF


SET IDENTITY_INSERT [temp_WAREHOUSE_ScheduledExamsTable]  ON 

INSERT [temp_WAREHOUSE_ScheduledExamsTable] (ID, ScheduledExamID, ExamID, WAREHOUSECentreID, WAREHOUSECreatedBy, CreatedDateTime, ScheduledStartDateTime, ScheduledEndDateTime, ActiveStartTime, ActiveEndTime, qualificationId, examName, groupState, invigilated, humanMarked, AdvanceContentDownloadTimespanInHours, OriginatorID, qualificationName, examVersionId, ExternalReference, QualificationRef, IsExternal, language, qualificationLevel, purchaseOrder)
SELECT ID, ScheduledExamID, ExamID, WAREHOUSECentreID, WAREHOUSECreatedBy, CreatedDateTime, ScheduledStartDateTime, ScheduledEndDateTime, ActiveStartTime, ActiveEndTime, qualificationId, examName, groupState, invigilated, humanMarked, AdvanceContentDownloadTimespanInHours, OriginatorID, qualificationName, examVersionId, ExternalReference, QualificationRef, IsExternal, language, qualificationLevel, purchaseOrder
FROM dbo.[WAREHOUSE_ScheduledExamsTable]
where [WAREHOUSE_ScheduledExamsTable].id in (
	SELECT WAREHOUSEScheduledExamID
	from [PPD_BCIntegration_SecureAssess].dbo.WAREHOUSE_ExamSessionTable
	where ID IN (select ID 
				 FROM dbo.DataToKeep
				)
)

SET IDENTITY_INSERT [temp_WAREHOUSE_ScheduledExamsTable] OFF