/*Notes: This script creates foreign keys on the newly 
    renamed tables to bring them inline with the originals.*/

/*TODO - Might need to NOCHECK these for performance reasons*/

/*Foreign keys*/
ALTER TABLE [dbo].[WAREHOUSE_ExamSessionAvailableItemsTable] 
    ADD CONSTRAINT [FK_WAREHOUSE_ExamSessionAvailableItemsTable_WAREHOUSE_ExamSessionTable] 
    FOREIGN KEY ([ExamSessionID]) 
    REFERENCES [dbo].[WAREHOUSE_ExamSessionTable] ([ID]) ON DELETE CASCADE;

ALTER TABLE [dbo].[WAREHOUSE_ExamSessionDocumentTable] 
    ADD CONSTRAINT [FK_WAREHOUSE_ExamSessionDocumentTable_WAREHOUSE_ExamSessionTable] 
    FOREIGN KEY ([warehouseExamSessionID]) 
    REFERENCES [dbo].[WAREHOUSE_ExamSessionTable] ([ID]);

ALTER TABLE [dbo].[WAREHOUSE_ExamSessionItemResponseTable] 
    ADD CONSTRAINT [FK_WAREHOUSE_ExamSessionItemResponseTable_WAREHOUSE_ExamSessionTable] 
    FOREIGN KEY ([WAREHOUSEExamSessionID]) 
    REFERENCES [dbo].[WAREHOUSE_ExamSessionTable] ([ID]) ON DELETE CASCADE;

/*We're doining this at the end as it relies on a unique index*/
--ALTER TABLE [dbo].[WAREHOUSE_ExamSessionTable_ShreddedItems_Mark] 
--    ADD CONSTRAINT [FK_WAREHOUSE_ExamSessionTable_ShreddedItems_Mark_WAREHOUSE_ExamSessionTable_ShrededItems]
--    FOREIGN KEY ([ExamSessionID], [ItemID]) 
--    REFERENCES [dbo].[WAREHOUSE_ExamSessionTable_ShrededItems] ([examSessionId], [ItemRef]);

ALTER TABLE [dbo].[WAREHOUSE_ExamSessionResultHistoryTable] 
    ADD CONSTRAINT [FK_WAREHOUSE_ExamSessionResultHistoryTable_WAREHOUSE_ExamSessionTable] 
    FOREIGN KEY ([WareHouseExamSessionTableID]) 
    REFERENCES [dbo].[WAREHOUSE_ExamSessionTable] ([ID]);

ALTER TABLE [dbo].[WAREHOUSE_ExamSessionTable_Shreded] 
    ADD CONSTRAINT [FK_WAREHOUSE_ExamSessionTable_Shreded_WAREHOUSE_ExamSessionTable] 
    FOREIGN KEY ([examSessionId]) 
    REFERENCES [dbo].[WAREHOUSE_ExamSessionTable] ([ID]) ON DELETE CASCADE;

ALTER TABLE [dbo].[WAREHOUSE_ExamSessionTable_ShrededItems] 
    ADD CONSTRAINT [FK_WAREHOUSE_ExamSessionTable_ShrededItems_WAREHOUSE_ExamSessionTable_Shreded] 
    FOREIGN KEY ([examSessionId]) 
    REFERENCES [dbo].[WAREHOUSE_ExamSessionTable_Shreded] ([examSessionId]) ON DELETE CASCADE;

ALTER TABLE [dbo].[WAREHOUSE_ScheduledExamsTable] 
    ADD CONSTRAINT [FK_WAREHOUSE_ScheduledExamsTable_WAREHOUSE_CentreTable] 
    FOREIGN KEY ([WAREHOUSECentreID]) 
    REFERENCES [dbo].[WAREHOUSE_CentreTable] ([ID]) ON DELETE CASCADE;

ALTER TABLE [dbo].[WAREHOUSE_ScheduledExamsTable] 
    ADD CONSTRAINT [FK_WAREHOUSE_ScheduledExamsTable_WAREHOUSE_UserTable] 
    FOREIGN KEY ([WAREHOUSECreatedBy]) 
    REFERENCES [dbo].[WAREHOUSE_UserTable] ([ID]);

