use master
go
RESTORE DATABASE [Develop-MI-SA] FROM DISK = N'E:\Backup\Develop-MI-SA_backup_2016_08_09_154812_1476456.bak' WITH FILE = 1
	, NOUNLOAD, NOREWIND, REPLACE, STATS = 5
	, MOVE 'SecureAssess' TO N'E:\Data\Develop-MI-SA.mdf'
	, MOVE 'SecureAssess_log' TO N'E:\Log\Develop-MI-SA.ldf'; 
GO
USE [master]
GO
ALTER DATABASE [Develop-MI-SA] MODIFY FILE ( NAME = N'SecureAssess_log', SIZE = 102400KB , FILEGROWTH = 102400KB )
GO

/*
select 'kill ' + convert(varchar(5),spid)
	,db_name(dbid)
from sys.sysprocesses s
where dbid > 4
*/

--USE [Develop-MI-SA]

----SELECT ID FROM [dbo].[WAREHOUSE_ExamSessionTable] WHERE WarehouseTime > DATEADD(MONTH, -36, SYSDATETIME());

----SELECT ID FROM [dbo].[WAREHOUSE_ExamSessionTable] WHERE WarehouseTime < DATEADD(MONTH, -2, SYSDATETIME());

--select 'kill ' + convert(varchar(5),spid)
--	,db_name(dbid)
--from sys.sysprocesses s
--where dbid > 4
--	--and db_name(dbid) like 'replica%'
