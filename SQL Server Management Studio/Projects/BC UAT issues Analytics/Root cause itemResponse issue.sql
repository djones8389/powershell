SELECT ID
into #ESIDS
from PRV_BritishCouncil_SecureAssess.dbo.WAREHOUSE_ExamSessionTable
where warehouseTime > '01 Jan 2016'
	and KeyCode IN (SELECT KeyCode from PRV_BritishCouncil_SurpassDataWarehouse..FactExamSessions)

SELECT WESIRT.WAREHOUSEExamSessionID
	, WESIRT.ItemID
	, ItemVersion
from PRV_BritishCouncil_SecureAssess.dbo.WAREHOUSE_ExamSessionItemResponseTable WESIRT
INNER JOIN #ESIDS A
on A.ID = WESIRT.WAREHOUSEExamSessionID
order by 1,2

select A.ID
	, CPID
	, CPVersion
from PRV_BritishCouncil_SurpassDataWarehouse..FactQuestionResponses FQR
INNER JOIN #ESIDS A
on A.ID = FQR.ExamSessionKey
order by 1,2











select fes.ExamSessionKey	
	, KeyCode
	, warehousetime
	, fqr.*
FROM PRV_BritishCouncil_SurpassDataWarehouse.dbo.FactExamSessions FES
INNER JOIN PRV_BritishCouncil_SurpassDataWarehouse.dbo.FactQuestionResponses FQR
ON FQR.ExamSessionKey = fes.ExamSessionKey
where --keycode in ('49PHLY99','99YPDC99','LN8HJX89','HWB7FG89','D3WKT889')
	 FES.ExamSessionKey in (1443173,
1448535,
1450514,
1451819,
1451830
)
order by 1














/*

select ID
	, resultData
	, warehouseTime
from PRV_BritishCouncil_SecureAssess.dbo.WAREHOUSE_ExamSessionTable
where keycode in ('49PHLY99','99YPDC99','LN8HJX89','HWB7FG89','D3WKT889')
order by 1

select fes.ExamSessionKey	
	, KeyCode
	, warehousetime
	, fqr.*
FROM PRV_BritishCouncil_SurpassDataWarehouse.dbo.FactExamSessions FES
INNER JOIN PRV_BritishCouncil_SurpassDataWarehouse.dbo.FactQuestionResponses FQR
ON FQR.ExamSessionKey = fes.ExamSessionKey
where keycode in ('49PHLY99','99YPDC99','LN8HJX89','HWB7FG89','D3WKT889')
order by 1

*/