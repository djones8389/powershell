use PRV_BritishCouncil_SurpassDataWarehouse


--select *
--from DimCandidate
--where candidateref like 'abj/%'


select FES.ExamSessionKey
	, KeyCode
	, CandidateRef
	, Forename
	, Surname
	, dc.CandidateKey
	,LatestWHCandidateKey
from FactExamSessions FES
INNER JOIN DimCandidate DC
on DC.CandidateKey = FES.CandidateKey
where Keycode in ('BVHRNY89')--in ('BVHRNY89','7BQM3989','XBNKDN89','KFCMKH89','3BF83P89','3GQYKY89','MVRK3G89','RXMKMG89','6DG4QK89','3CHXWM89','DMXPMV89','JJCBCX89','DFF6VW89','497K9F89','BF8MH689','TGN4JR89','YWWWXC89','VMPW3D89','3P87N789','73XNDV89','44KYRG89','4VQ3PB89','36PTTK89','7JRDPM89','PBLNY789','9KVPBW89','C4X3YM89','T6JBTD89','CX33WD89','VYHBW989','83K4KM89','J8NYBM89','XR96CN89','8HDL8F89','3H9GFJ89')
	order by Keycode

--select * from DimCandidate where CandidateKey = 375324


USE PRV_BritishCouncil_SecureAssess

SELECT west.id
	, wests.KeyCode
	, wests.CandidateRef
	, wut.Forename
	, wut.Surname
	, WUT.UserId CandidateKey
	, WUT.[ID] LatestWHCandidateKey
FROM WAREHOUSE_ExamSessionTable_Shreded WESTS
inner join WAREHOUSE_ExamSessionTable WEST
on west.id = wests.examsessionid
inner join Warehouse_usertable wut
on wut.id = west.WAREHOUSEUserID
where west.Keycode in ('BVHRNY89')--('BVHRNY89','7BQM3989','XBNKDN89','KFCMKH89','3BF83P89','3GQYKY89','MVRK3G89','RXMKMG89','6DG4QK89','3CHXWM89','DMXPMV89','JJCBCX89','DFF6VW89','497K9F89','BF8MH689','TGN4JR89','YWWWXC89','VMPW3D89','3P87N789','73XNDV89','44KYRG89','4VQ3PB89','36PTTK89','7JRDPM89','PBLNY789','9KVPBW89','C4X3YM89','T6JBTD89','CX33WD89','VYHBW989','83K4KM89','J8NYBM89','XR96CN89','8HDL8F89','3H9GFJ89')
	order by Keycode

--SELECT * FROM  Warehouse_usertable NOLOCK where ID IN (453488, 453665)

SELECT count(*)
FROM PRV_BritishCouncil_SurpassDataWarehouse..DimCandidate SDW
INNER JOIN PRV_BritishCouncil_SecureAssess..WAREHOUSE_UserTable SA
on Sa.UserId = sdw.CandidateKey
where sa.CandidateRef <> sdw.CandidateRef