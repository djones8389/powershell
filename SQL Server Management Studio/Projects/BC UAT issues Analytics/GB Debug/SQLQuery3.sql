SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

;WITH itemlist0 AS (
    SELECT EST.ID
        ,t.c.value('@id', 'nvarchar(50)') CPID
        ,t.c.value('@version', 'int') Ver
        ,CAST(t.c.exist('p') AS INT) PExist
    FROM 
		WAREHOUSE_ExamSessionTable EST
		CROSS APPLY EST.resultDataFull.nodes('assessmentDetails/assessment/section/item') t(c)
    WHERE 
		--EST.warehouseTime > '1900-01-01 00:00:00'
		--AND EST.warehouseTime < '2050-01-01 00:00:00'
		EST.warehouseTime > '1900-01-01 00:00:00.000'    
		AND EST.warehouseTime < '2050-01-01 00:00:00.000'  
		AND EST.warehouseExamState = 1
		AND EST.ID in (1451059,1450693,1450791,1450838,1450864,1451086,1450656,1450700,1450988,1451110,1450620,1450674,1450696,1451201,1451014,1451044,1451104,1451108,1450901,1450910,1450931,1450980,1451002,1450852,1450868,1450872,1450894,1450795,1450826,1450830,1450707,1450717,1450725,1450730,1450774)
)
,itemlist AS (
	SELECT 
		CPID
		, Ver
		, CHARINDEX('P', CPID) ix
		, MAX(ID) MaxID
	FROM 
		itemlist0
	WHERE 
		PExist = 1
	GROUP BY 
		CPID, Ver

	UNION ALL

	SELECT 
		CPID
		, Ver
		, CHARINDEX('P', CPID) ix
		, MAX(ID) MaxID
	FROM 
		itemlist0
	GROUP BY 
		CPID, Ver
	HAVING 
		SUM(PExist) = 0
)

,EST AS (
	SELECT 
		ID
		,CONVERT(NVARCHAR(50), CONVERT(VARCHAR(MAX), t.c.query('data(@id)'))) CPID
		,CONVERT(NVARCHAR(50), CONVERT(VARCHAR(MAX), t.c.query('data(@version)'))) CPVersion
		,CONVERT(INT, CONVERT(VARCHAR(MAX), t.c.query('data(@type)'))) CPType
		,CONVERT(NVARCHAR(100), CONVERT(NVARCHAR(MAX), t.c.query('data(@name)'))) QuestionName
		,CONVERT(VARCHAR(MAX), t.c.query('data(@totalMark)')) TotalMark
		,CONVERT(VARCHAR(MAX), t.c.query('data(@markingType)')) MarkingTypeKey
		,t.c.query('.') ItemXML
	FROM 
		WAREHOUSE_ExamSessionTable EST
		CROSS APPLY EST.resultDataFull.nodes('assessmentDetails/assessment/section/item') t(c)
		WHERE EST.ID in (1451059,1450693,1450791,1450838,1450864,1451086,1450656,1450700,1450988,1451110,1450620,1450674,1450696,1451201,1451014,1451044,1451104,1451108,1450901,1450910,1450931,1450980,1451002,1450852,1450868,1450872,1450894,1450795,1450826,1450830,1450707,1450717,1450725,1450730,1450774)
)
SELECT 
	IL.CPID
	,SUBSTRING(IL.CPID, 1, IL.ix-1) ProjectKey
	,SUBSTRING(IL.CPID, IL.ix+1, LEN(IL.CPID) - IL.ix) ItemKey
	,IL.Ver ItemVersion
	,EST.CPType
	,EST.QuestionName
	,CASE ISNUMERIC(EST.TotalMark)
		WHEN 0 THEN NULL
		ELSE CAST(EST.TotalMark AS FLOAT)
	END TotalMark
	,CONVERT(NVARCHAR(MAX), CPAT.ItemXML.query('/P/metaData/HTML/markingCriteria')) MarkingCriteria
	,CASE When ISNUMERIC(EST.MarkingTypeKey) = 1 Then Convert(int,EST.MarkingTypeKey) Else NULL END MarkingTypeKey
	,ISNULL(CPAT.CorrectAnswers, '') DerivedCorrectAnswer
	,ISNULL(CPAT.ShortCorrectAnswers, '') ShortDerivedCorrectAnswer
	,ISNULL(CPAT.QuestionTypes, 'Not Known') QuestionTypes
	,CASE 
		WHEN CPAT.ItemXML IS NULL THEN 0
		ELSE 1
	END HasCPAuditEntry
	,ISNULL(CPAT.ItemXML, EST.ItemXML) ItemXML
	--,0 Priority
	,0 Priority
	,1 Source
FROM 
	EST
	JOIN itemlist IL ON EST.ID = IL.MaxID
        AND EST.CPID = IL.CPID
        AND EST.CPVersion = IL.Ver
	LEFT JOIN CPAuditTable CPAT ON EST.CPID = CPAT.ItemID
		AND EST.CPVersion = CPAT.ItemVersion


select ExamSessionID
	,qualificationName
	,examName
	, examVersionName
	, examVersionRef
	, examVersionId
from WAREHOUSE_ExamSessionTable_Shreded NOLOCK
where ExamSessionID in (1451059,1450693,1450791,1450838,1450864,1451086,1450656,1450700,1450988,1451110,1450620,1450674,1450696,1451201,1451014,1451044,1451104,1451108,1450901,1450910,1450931,1450980,1451002,1450852,1450868,1450872,1450894,1450795,1450826,1450830,1450707,1450717,1450725,1450730,1450774)











DECLARE 
	@LastExamSessionTime nvarchar(100) = '1900-01-01 00:00:00.000' 
	,@ExecutionStartTime nvarchar(100) = '2050-01-01 00:00:00.000'
	
SELECT 
	WSET.[ExamId] as ExamKey
	,WSET.[examVersionId] ExamVersionKey
	,Convert(Nvarchar(100), WSET.[ExternalReference]) AS ExamVersionReference
	,EST.StructureXML.value('data(/assessmentDetails/assessmentName)[1]','NVARCHAR(100)') ExamVersionName 
FROM 
	[dbo].[WAREHOUSE_ScheduledExamsTable] WSET 
	INNER JOIN [dbo].[WAREHOUSE_ExamSessionTable] EST 
	ON EST.WAREHOUSEScheduledExamID = WSET.ID 
WHERE 
	EST.warehouseTime > @LastExamSessionTime
	AND EST.warehouseTime < @ExecutionStartTime
	AND EST.warehouseExamState = 1 
	AND WSET.ID IN 	(
		SELECT MAX(s.ID) IDMAX 
		FROM 
			[dbo].[WAREHOUSE_ScheduledExamsTable] s 
			INNER JOIN [dbo].[WAREHOUSE_ExamSessionTable] e
				ON e.WAREHOUSEScheduledExamID = s.ID AND e.warehouseExamState = 1 
		WHERE 
			e.warehouseTime > @LastExamSessionTime
			AND e.warehouseTime < @ExecutionStartTime
		GROUP BY [ExamID], [examVersionId]
	) 	
	AND EST.ID IN (	
		SELECT MAX(ID) IDMAX 
		FROM 
			[dbo].[WAREHOUSE_ExamSessionTable] e 
		WHERE 
			e.WAREHOUSEScheduledExamID = WSET.ID 
			AND e.warehouseExamState = 1  
			AND e.warehouseTime > @LastExamSessionTime
			AND e.warehouseTime < @ExecutionStartTime
		GROUP BY WAREHOUSEScheduledExamID
	)

	order by 2 desc