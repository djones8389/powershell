exec sp_executesql N'------SELECT STUFF(CAST((SELECT '', '' + CAST(ExamVersionKey AS nvarchar(50)) FROM DimExamVersions FOR XML PATH('''')) AS nvarchar(max)),1,2,'''')
------SELECT STUFF(CAST((SELECT '', '' + CAST(CentreKey AS nvarchar(50)) FROM DimCentres FOR XML PATH('''')) AS nvarchar(max)),1,2,'''')
--DECLARE @ExamVersionKey nvarchar(max) = ''723''
--	,@CentreKey nvarchar(max) = ''31''
--	,@StartDate datetime = ''2014-06-08''
--	,@EndDate datetime = ''2015-12-31''
		
DECLARE @Examversions table(ExamVersionKey int)--PRIMARY KEY CLUSTERED - produces lower elapsed time but higher CPU
DECLARE @Centres table(CentreKey int )--PRIMARY KEY CLUSTERED

INSERT INTO @Examversions
SELECT value FROM dbo.fn_ParamsToList(@ExamVersionKey, 0) OPTION(MAXRECURSION 0)

INSERT INTO @Centres
SELECT value FROM dbo.fn_ParamsToList(@CentreKey, 0) OPTION(MAXRECURSION 0)

;WITH ES AS (
	SELECT 
		FES.ExamSessionKey
		, FES.UserMarks
		, FES.TotalMarksAvailable
		, FES.FinalExamState
		, FES.ExamVersionKey
		, FES.CentreKey
		, FES.CandidateKey
		, Keycode
		,DT.FullDateAlternateKey + FES.CompletionTime DateCompleted
	FROM
		FactExamSessions FES
		JOIN DimTime DT ON FES.CompletionDateKey = DT.TimeKey
		JOIN @Examversions EV ON FES.ExamVersionKey = EV.ExamVersionKey
		JOIN @Centres C ON FES.CentreKey = C.CentreKey
	WHERE 
		FES.FinalExamState<>10
		AND DT.FullDateAlternateKey BETWEEN @StartDate AND @EndDate
), MarkerMarks AS(
      SELECT 
            ROW_NUMBER() OVER(PARTITION BY FMR.ExamSessionKey, FMR.CPID ORDER BY SeqNo DESC) N
            ,FMR.ExamSessionKey
            ,FMR.CPID
            ,FMR.assignedMark
            ,FMR.ExaminerID
      FROM 
            ES
            JOIN FactMarkerResponse FMR
                  ON FMR.ExamSessionKey = ES.ExamSessionKey
)
,ComponentResponse AS(
      SELECT
			FCR.ExamSessionKey 
            ,FCR.CID
            ,FCR.CPVersion
            ,FCR.Mark
            ,ISNULL(FCR.ResponseKey,FCR.DerivedResponse) ResponseSelected
            --,FCR.DerivedResponse ResponseSelected --increases IO and time 
            --,CASE WHEN ISNULL(FCR.DerivedResponse,'''')='''' THEN FCR.ShortDerivedResponse ELSE FCR.DerivedResponse END ResponseSelected --same as above
      FROM 
            ES
            JOIN FactComponentResponses FCR
                  ON FCR.ExamSessionKey = ES.ExamSessionKey
)
SELECT
      ES.ExamSessionKey
      ,ES.CentreKey
      ,ES.ExamVersionKey
      ,DCa.CandidateRef
	  ,Keycode
      ,DCa.Username CandidateUsername
      ,DCa.Forename CandidateForename
      ,DCa.Surname CandidateSurname
      ,DCa.DOB
      ,ES.DateCompleted
      ,ES.UserMarks ExamUserMarks
      ,ES.TotalMarksAvailable ExamTotalMarks
      ,ES.FinalExamState
      -------Questions-------
      ,DQu.CPID
      ,DQu.TotalMark QuestionTotalMark
      ,ROUND(ISNULL(MM.assignedMark, DQu.TotalMark*FQR.Mark),2) QuestionUserMark
      ,MM.assignedMark
      ,MM.ExaminerID
      ,DCa2.Username ExaminerUsername
      ,DCa2.Forename ExaminerForename
      ,DCa2.Surname ExaminerSurname
      ,ROUND(FQR.ViewingTime/1000.,2) ViewingTime
      -------Components-------
      ,STUFF(DCo.CID,1,LEN(DQu.CPID), '''') CID
      ,ROUND(CASE 
            WHEN 
                  MM.CPID IS NULL --AutoMarked
                  AND ES.FinalExamState=12 --Without SecureMarker
            THEN FCR.Mark*DCo.TotalMark
            ELSE NULL --ISNULL(MM.assignedMark, DQu.TotalMark*FQR.Mark)*DCo.Weight
      END, 2) ComponentUserMark
      ,ROUND(DCo.TotalMark, 2) ComponentTotalMark
      --,DCo.ShortDerivedCorrectAnswer ResponseKey
      --,DCo.DerivedCorrectAnswer ResponseKey
      ,ISNULL(DCo.AnswerKey,DCo.DerivedCorrectAnswer) ResponseKey
      ,FCR.ResponseSelected
      -------Drag and drop options-------
      ,DO.OptionKey
      ,ISNULL(FR.Mark,
		CASE WHEN 
			FR.ExamSessionKey IS NULL AND DO.CorrectAnswersCount>0 --If a drop zone has correct answer but a candidate hasn''t placed any item there, he gets 0.
		THEN 0 ELSE NULL END) Mark
      ,DO.AnswerText
      ,FR.DerivedResponse
      --,DO.AnswerKey
      --,FR.ResponseKey
	  
FROM 
	ES
	JOIN FactQuestionResponses FQR 
		ON    FQR.ExamSessionKey = ES.ExamSessionKey
	JOIN DimQuestions DQu 
		ON    DQu.CPID = FQR.CPID AND 
			  DQu.CPVersion = FQR.CPVersion
	JOIN DimComponents DCo
		ON DCo.CPID = DQu.CPID
	AND DCo.CPVersion = DQu.CPVersion
	AND DCo.Weight>0
	LEFT JOIN ComponentResponse FCR 
		ON DCo.CID = FCR.CID
		AND ES.ExamSessionKey = FCR.ExamSessionKey
	LEFT JOIN MarkerMarks MM ON MM.ExamSessionKey = FQR.ExamSessionKey AND MM.CPID = FQR.CPID AND MM.N = 1
	LEFT JOIN DimCandidate DCa2 ON MM.ExaminerID = DCa2.CandidateKey
	JOIN DimCandidate DCa ON DCa.CandidateKey = ES.CandidateKey
	LEFT JOIN DimOptions DO
		ON DO.CID = FCR.CID and DO.CPVersion = FCR.CPVersion AND (DO.OptionType=17 OR DO.OptionType=13 OR DO.OptionType=12)
	LEFT JOIN FactOptionResponses FR
		ON FR.OID = DO.OID AND FR.ExamSessionKey = FCR.ExamSessionKey
OPTION (RECOMPILE);',N'@ExamVersionKey nvarchar(4),@CentreKey nvarchar(3),@StartDate datetime,@EndDate datetime',@ExamVersionKey=N'1364',@CentreKey=N'113',@StartDate='24/01/2016 00:00:00',@EndDate='25/04/2016 00:00:00'