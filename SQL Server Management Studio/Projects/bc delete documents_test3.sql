DECLARE @Min int = 1;
DECLARE @Max int = (
					select count(A.ID) 
					from [WH_OldDocsToDelete] A WITH (NOLOCK)
					INNER JOIN [Warehouse_ExamSessionDocumentTable] B WITH (NOLOCK)
					ON A.ID = B.ID
					);

DECLARE @Increment int = 100;

--497900  24/07/17  08:50 
--454829  24/07/17  10:38

WHILE (@Min < @Max)

BEGIN

	DELETE TOP (@Increment) C
	FROM [Warehouse_ExamSessionDocumentTable] C
	INNER JOIN [WH_OldDocsToDelete] D
	on D.ID = c.ID
	
	SELECT @Min = @Min + @Increment

	WAITFOR DELAY '00:00:10'

END

--558515/638090