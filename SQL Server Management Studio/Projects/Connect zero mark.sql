use Connect_SecureAssess

set transaction isolation level read uncommitted;

----5050P2982 -- v7


select ID 
from examsessiontable NOLOCK
where examState = 4
	and cast(structurexml as xml).exist('assessmentDetails/assessment/section/item[@id="5050P2982" and @version != "10"]') = 1




select id, west.resultData, usermark, userPercentage, keycode, west.previousexamstate, structurexml
from warehouse_examsessiontable west
inner join WAREHOUSE_ExamSessionTable_Shreded wests on wests.examsessionid = west.id
where --keycode = 'L3N4GL2G'
	 west.resultData.exist('/exam[@userMark=0 and @totalMark=0]') = 1


--select top 10 id, west.resultData, usermark, userPercentage
--from warehouse_examsessiontable west
--inner join WAREHOUSE_ExamSessionTable_Shreded wests on wests.examsessionid = west.id
--where userMark = 0

/*

update warehouse_examsessiontable
set resultData = '<exam passMark="1" passType="1" originalPassMark="0" originalPassType="1" totalMark="2" userMark="0" userPercentage="0" passValue="1" originalPassValue="1" totalTimeSpent="0" closeBoundaryType="0" closeBoundaryValue="0" grade="Pass" originalGrade="Pass">
  <gradeBoundaryData>
    <gradeBoundaries passLevelType="1" adjustedGradeBoundaries="0">
      <grade modifier="lt" value="0" description="Fail" />
      <grade modifier="gt" value="0" description="Pass" />
    </gradeBoundaries>
  </gradeBoundaryData>
  <section id="1" passMark="0" passType="1" name="Section" totalMark="0" userMark="0" userPercentage="0" passValue="1" totalTimeSpent="0" itemsToMark="0">
    <item id="5050P2982" name="GCSE WSL - candidate" totalMark="0" userMark="0" actualUserMark="0" markingType="1" markerUserMark="" viewingTime="0" userAttempted="0" version="7" LO="" type="1" nonScored="0" />
  </section>
</exam>'
	, structurexml = '<assessmentDetails>
  <assessmentID>83</assessmentID>
  <qualificationID>152</qualificationID>
  <qualificationName>Welsh 2nd Language 1 GCSE NEA</qualificationName>
  <qualificationReference>302201</qualificationReference>
  <assessmentGroupName>e-Submission Welsh</assessmentGroupName>
  <assessmentGroupID>85</assessmentGroupID>
  <assessmentGroupReference>302201</assessmentGroupReference>
  <assessmentName>e-Submission Welsh Second Language</assessmentName>
  <validFromDate>09 Apr 2018</validFromDate>
  <expiryDate>05 May 2018</expiryDate>
  <startTime>00:00:00</startTime>
  <endTime>23:59:59</endTime>
  <duration>129600</duration>
  <defaultDuration>129600</defaultDuration>
  <scheduledDuration>
    <value>37440</value>
    <reason />
  </scheduledDuration>
  <sRBonus>0</sRBonus>
  <sRBonusMaximum>0</sRBonusMaximum>
  <externalReference>302201</externalReference>
  <passLevelValue>0</passLevelValue>
  <passLevelType>1</passLevelType>
  <status>2</status>
  <testFeedbackType>
    <passFail>0</passFail>
    <percentageMark>0</percentageMark>
    <allowSummaryFeedback>0</allowSummaryFeedback>
    <summaryFeedbackType>1</summaryFeedbackType>
    <itemSummary>0</itemSummary>
    <itemReview>0</itemReview>
    <itemReviewCorrectAnswer>0</itemReviewCorrectAnswer>
    <itemFeedback>0</itemFeedback>
    <printableSummary>0</printableSummary>
    <candidateDetails>0</candidateDetails>
    <feedbackByReference>0</feedbackByReference>
    <allowFeedbackDuringExam>0</allowFeedbackDuringExam>
    <allowFeedbackDuringSummary>0</allowFeedbackDuringSummary>
  </testFeedbackType>
  <itemFeedback>0</itemFeedback>
  <allowCalculator>0</allowCalculator>
  <lastInUseDate>09 Apr 2018 17:18:19</lastInUseDate>
  <completedScriptReview>0</completedScriptReview>
  <assessment currentSection="0" totalTime="0" currentTime="0">
    <intro id="0" name="Introduction" currentItem="" />
    <outro id="0" name="Finish" currentItem="" />
    <tools id="0" name="Tools" currentItem="" />
    <section id="1" name="Section" passLevelValue="0" passLevelType="1" itemsToMark="0" currentItem="" fixed="1">
      <item id="5050P2982" name="GCSE WSL - candidate" totalMark="1" version="7" markingType="1" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" displayName="" type="1" LO="" unit="" quT="22" />
    </section>
  </assessment>
  <isValid>1</isValid>
  <isPrintable>0</isPrintable>
  <qualityReview>0</qualityReview>
  <numberOfGenerations>1</numberOfGenerations>
  <requiresInvigilation>0</requiresInvigilation>
  <advanceContentDownloadTimespan>999</advanceContentDownloadTimespan>
  <automaticVerification>0</automaticVerification>
  <offlineMode>0</offlineMode>
  <requiresValidation>1</requiresValidation>
  <requiresSecureClient>0</requiresSecureClient>
  <examType>0</examType>
  <advanceDownload>0</advanceDownload>
  <gradeBoundaryData>
    <gradeBoundaries passLevelType="1" adjustedGradeBoundaries="0">
      <grade modifier="lt" value="0" description="Fail" />
      <grade modifier="gt" value="0" description="Pass" />
    </gradeBoundaries>
  </gradeBoundaryData>
  <autoViewExam>0</autoViewExam>
  <autoProgressItems>0</autoProgressItems>
  <confirmationText>
    <confirmationText />
  </confirmationText>
  <requiresConfirmationCheck>0</requiresConfirmationCheck>
  <allowCloseWithoutSubmit>1</allowCloseWithoutSubmit>
  <useSecureMarker>0</useSecureMarker>
  <annotationVersion>0</annotationVersion>
  <allowPackagingDelivery>1</allowPackagingDelivery>
  <scoreBoundaryData>
    <scoreBoundaries showScoreBoundaryColumn="0">
      <score modifier="lt" value="40" description="Not met" higherBoundarySet="1" />
      <score modifier="lt" value="40" description="Not met" higherBoundarySet="0" />
      <score modifier="gt" value="40" description="Close to met" higherBoundarySet="1" />
      <score modifier="gt" value="40" description="Met" higherBoundarySet="0" />
      <score modifier="gt" value="45" description="Met" higherBoundarySet="1" />
      <score modifier="gt" value="80" description="Exceeded" higherBoundarySet="1" />
    </scoreBoundaries>
  </scoreBoundaryData>
  <showCandidateReportResultColumn>0</showCandidateReportResultColumn>
  <showCandidateReportPercentageColumn>1</showCandidateReportPercentageColumn>
  <certifiedAccessible>0</certifiedAccessible>
  <markingProgressVisible>1</markingProgressVisible>
  <markingProgressMode>1</markingProgressMode>
  <secureClientOperationMode>1</secureClientOperationMode>
  <examDeliverySystemStyleType>delivery_flat</examDeliverySystemStyleType>
  <markingAutoVoidPeriod>365</markingAutoVoidPeriod>
  <showPageRequiresScrollingAlert>1</showPageRequiresScrollingAlert>
  <project ID="5050" />
</assessmentDetails>'
where id = 510

update WAREHOUSE_ExamSessionTable_Shreded
set resultData = '<exam passMark="1" passType="1" originalPassMark="0" originalPassType="1" totalMark="2" userMark="0" userPercentage="0" passValue="1" originalPassValue="1" totalTimeSpent="0" closeBoundaryType="0" closeBoundaryValue="0" grade="Pass" originalGrade="Pass">
  <gradeBoundaryData>
    <gradeBoundaries passLevelType="1" adjustedGradeBoundaries="0">
      <grade modifier="lt" value="0" description="Fail" />
      <grade modifier="gt" value="0" description="Pass" />
    </gradeBoundaries>
  </gradeBoundaryData>
  <section id="1" passMark="0" passType="1" name="Section" totalMark="0" userMark="0" userPercentage="0" passValue="1" totalTimeSpent="0" itemsToMark="0">
    <item id="5050P2982" name="GCSE WSL - candidate" totalMark="0" userMark="0" actualUserMark="0" markingType="1" markerUserMark="" viewingTime="0" userAttempted="0" version="7" LO="" type="1" nonScored="0" />
  </section>
</exam>'
where examSessionid = 510


update warehouse_examsessiontableexec sa_ASPIREINTEGRATION_GetDetailedResult_sp @examSessionId=510,@includeExamsInMarking=1
set resultData = '<exam passMark="0" passType="1" originalPassMark="0" originalPassType="1" totalMark="1" userMark="0" userPercentage="0" passValue="1" originalPassValue="1" totalTimeSpent="0" closeBoundaryType="0" closeBoundaryValue="0" grade="Pass" originalGrade="Pass">
  <gradeBoundaryData>
    <gradeBoundaries passLevelType="1" adjustedGradeBoundaries="0">
      <grade modifier="lt" value="0" description="Fail" />
      <grade modifier="gt" value="0" description="Pass" />
    </gradeBoundaries>
  </gradeBoundaryData>
  <section id="1" passMark="0" passType="1" name="Section" totalMark="0" userMark="0" userPercentage="0" passValue="1" totalTimeSpent="0" itemsToMark="0">
    <item id="5050P2982" name="GCSE WSL - candidate" totalMark="0" userMark="0" actualUserMark="0" markingType="1" markerUserMark="" viewingTime="0" userAttempted="0" version="7" LO="" type="1" nonScored="0" />
  </section>
</exam>'
	, structurexml = '<assessmentDetails>
  <assessmentID>83</assessmentID>
  <qualificationID>152</qualificationID>
  <qualificationName>Welsh 2nd Language 1 GCSE NEA</qualificationName>
  <qualificationReference>302201</qualificationReference>
  <assessmentGroupName>e-Submission Welsh</assessmentGroupName>
  <assessmentGroupID>85</assessmentGroupID>
  <assessmentGroupReference>302201</assessmentGroupReference>
  <assessmentName>e-Submission Welsh Second Language</assessmentName>
  <validFromDate>09 Apr 2018</validFromDate>
  <expiryDate>05 May 2018</expiryDate>
  <startTime>00:00:00</startTime>
  <endTime>23:59:59</endTime>
  <duration>129600</duration>
  <defaultDuration>129600</defaultDuration>
  <scheduledDuration>
    <value>37440</value>
    <reason />
  </scheduledDuration>
  <sRBonus>0</sRBonus>
  <sRBonusMaximum>0</sRBonusMaximum>
  <externalReference>302201</externalReference>
  <passLevelValue>0</passLevelValue>
  <passLevelType>1</passLevelType>
  <status>2</status>
  <testFeedbackType>
    <passFail>0</passFail>
    <percentageMark>0</percentageMark>
    <allowSummaryFeedback>0</allowSummaryFeedback>
    <summaryFeedbackType>1</summaryFeedbackType>
    <itemSummary>0</itemSummary>
    <itemReview>0</itemReview>
    <itemReviewCorrectAnswer>0</itemReviewCorrectAnswer>
    <itemFeedback>0</itemFeedback>
    <printableSummary>0</printableSummary>
    <candidateDetails>0</candidateDetails>
    <feedbackByReference>0</feedbackByReference>
    <allowFeedbackDuringExam>0</allowFeedbackDuringExam>
    <allowFeedbackDuringSummary>0</allowFeedbackDuringSummary>
  </testFeedbackType>
  <itemFeedback>0</itemFeedback>
  <allowCalculator>0</allowCalculator>
  <lastInUseDate>09 Apr 2018 17:18:19</lastInUseDate>
  <completedScriptReview>0</completedScriptReview>
  <assessment currentSection="0" totalTime="0" currentTime="0">
    <intro id="0" name="Introduction" currentItem="" />
    <outro id="0" name="Finish" currentItem="" />
    <tools id="0" name="Tools" currentItem="" />
    <section id="1" name="Section" passLevelValue="0" passLevelType="1" itemsToMark="0" currentItem="" fixed="1">
      <item id="5050P2982" name="GCSE WSL - candidate" totalMark="1" version="7" markingType="1" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" displayName="" type="1" LO="" unit="" quT="22" />
    </section>
  </assessment>
  <isValid>1</isValid>
  <isPrintable>0</isPrintable>
  <qualityReview>0</qualityReview>
  <numberOfGenerations>1</numberOfGenerations>
  <requiresInvigilation>0</requiresInvigilation>
  <advanceContentDownloadTimespan>999</advanceContentDownloadTimespan>
  <automaticVerification>0</automaticVerification>
  <offlineMode>0</offlineMode>
  <requiresValidation>1</requiresValidation>
  <requiresSecureClient>0</requiresSecureClient>
  <examType>0</examType>
  <advanceDownload>0</advanceDownload>
  <gradeBoundaryData>
    <gradeBoundaries passLevelType="1" adjustedGradeBoundaries="0">
      <grade modifier="lt" value="0" description="Fail" />
      <grade modifier="gt" value="0" description="Pass" />
    </gradeBoundaries>
  </gradeBoundaryData>
  <autoViewExam>0</autoViewExam>
  <autoProgressItems>0</autoProgressItems>
  <confirmationText>
    <confirmationText />
  </confirmationText>
  <requiresConfirmationCheck>0</requiresConfirmationCheck>
  <allowCloseWithoutSubmit>1</allowCloseWithoutSubmit>
  <useSecureMarker>0</useSecureMarker>
  <annotationVersion>0</annotationVersion>
  <allowPackagingDelivery>1</allowPackagingDelivery>
  <scoreBoundaryData>
    <scoreBoundaries showScoreBoundaryColumn="0">
      <score modifier="lt" value="40" description="Not met" higherBoundarySet="1" />
      <score modifier="lt" value="40" description="Not met" higherBoundarySet="0" />
      <score modifier="gt" value="40" description="Close to met" higherBoundarySet="1" />
      <score modifier="gt" value="40" description="Met" higherBoundarySet="0" />
      <score modifier="gt" value="45" description="Met" higherBoundarySet="1" />
      <score modifier="gt" value="80" description="Exceeded" higherBoundarySet="1" />
    </scoreBoundaries>
  </scoreBoundaryData>
  <showCandidateReportResultColumn>0</showCandidateReportResultColumn>
  <showCandidateReportPercentageColumn>1</showCandidateReportPercentageColumn>
  <certifiedAccessible>0</certifiedAccessible>
  <markingProgressVisible>1</markingProgressVisible>
  <markingProgressMode>1</markingProgressMode>
  <secureClientOperationMode>1</secureClientOperationMode>
  <examDeliverySystemStyleType>delivery_flat</examDeliverySystemStyleType>
  <markingAutoVoidPeriod>365</markingAutoVoidPeriod>
  <showPageRequiresScrollingAlert>1</showPageRequiresScrollingAlert>
  <project ID="5050" />
</assessmentDetails>'
where id = 511

update WAREHOUSE_ExamSessionTable_Shreded
set resultData = '<exam passMark="0" passType="1" originalPassMark="0" originalPassType="1" totalMark="1" userMark="0" userPercentage="0" passValue="1" originalPassValue="1" totalTimeSpent="0" closeBoundaryType="0" closeBoundaryValue="0" grade="Pass" originalGrade="Pass">
  <gradeBoundaryData>
    <gradeBoundaries passLevelType="1" adjustedGradeBoundaries="0">
      <grade modifier="lt" value="0" description="Fail" />
      <grade modifier="gt" value="0" description="Pass" />
    </gradeBoundaries>
  </gradeBoundaryData>
  <section id="1" passMark="0" passType="1" name="Section" totalMark="0" userMark="0" userPercentage="0" passValue="1" totalTimeSpent="0" itemsToMark="0">
    <item id="5050P2982" name="GCSE WSL - candidate" totalMark="0" userMark="0" actualUserMark="0" markingType="1" markerUserMark="" viewingTime="0" userAttempted="0" version="7" LO="" type="1" nonScored="0" />
  </section>
</exam>'
where examSessionid = 511






update warehouse_examsessiontable
set resultData = '<exam passMark="0" passType="1" originalPassMark="0" originalPassType="1" totalMark="1" userMark="0" userPercentage="0" passValue="1" originalPassValue="1" totalTimeSpent="168016" closeBoundaryType="0" closeBoundaryValue="0" grade="Pass" originalGrade="Pass">
  <gradeBoundaryData>
    <gradeBoundaries passLevelType="1" adjustedGradeBoundaries="0">
      <grade modifier="lt" value="0" description="Fail" />
      <grade modifier="gt" value="0" description="Pass" />
    </gradeBoundaries>
  </gradeBoundaryData>
  <section id="1" passMark="0" passType="1" name="Section" totalMark="0" userMark="0" userPercentage="0" passValue="1" totalTimeSpent="168016" itemsToMark="0">
    <item id="5050P2982" name="GCSE WSL - candidate" totalMark="0" userMark="0" actualUserMark="0" markingType="1" markerUserMark="0" viewingTime="168016" userAttempted="1" version="7" LO="" type="1" nonScored="0" />
  </section>
</exam>'
	, structurexml = '<assessmentDetails>
  <assessmentID>83</assessmentID>
  <qualificationID>152</qualificationID>
  <qualificationName>Welsh 2nd Language 1 GCSE NEA</qualificationName>
  <qualificationReference>302201</qualificationReference>
  <assessmentGroupName>e-Submission Welsh</assessmentGroupName>
  <assessmentGroupID>85</assessmentGroupID>
  <assessmentGroupReference>302201</assessmentGroupReference>
  <assessmentName>e-Submission Welsh Second Language</assessmentName>
  <validFromDate>09 Apr 2018</validFromDate>
  <expiryDate>05 May 2018</expiryDate>
  <startTime>00:00:00</startTime>
  <endTime>23:59:59</endTime>
  <duration>129600</duration>
  <defaultDuration>129600</defaultDuration>
  <scheduledDuration>
    <value>37440</value>
    <reason />
  </scheduledDuration>
  <sRBonus>0</sRBonus>
  <sRBonusMaximum>0</sRBonusMaximum>
  <externalReference>302201</externalReference>
  <passLevelValue>0</passLevelValue>
  <passLevelType>1</passLevelType>
  <status>2</status>
  <testFeedbackType>
    <passFail>0</passFail>
    <percentageMark>0</percentageMark>
    <allowSummaryFeedback>0</allowSummaryFeedback>
    <summaryFeedbackType>1</summaryFeedbackType>
    <itemSummary>0</itemSummary>
    <itemReview>0</itemReview>
    <itemReviewCorrectAnswer>0</itemReviewCorrectAnswer>
    <itemFeedback>0</itemFeedback>
    <printableSummary>0</printableSummary>
    <candidateDetails>0</candidateDetails>
    <feedbackByReference>0</feedbackByReference>
    <allowFeedbackDuringExam>0</allowFeedbackDuringExam>
    <allowFeedbackDuringSummary>0</allowFeedbackDuringSummary>
  </testFeedbackType>
  <itemFeedback>0</itemFeedback>
  <allowCalculator>0</allowCalculator>
  <lastInUseDate>09 Apr 2018 17:18:19</lastInUseDate>
  <completedScriptReview>0</completedScriptReview>
  <assessment currentSection="0" totalTime="0" currentTime="0">
    <intro id="0" name="Introduction" currentItem="" />
    <outro id="0" name="Finish" currentItem="" />
    <tools id="0" name="Tools" currentItem="" />
    <section id="1" name="Section" passLevelValue="0" passLevelType="1" itemsToMark="0" currentItem="" fixed="1">
      <item id="5050P2982" name="GCSE WSL - candidate" totalMark="1" version="7" markingType="1" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" displayName="" type="1" LO="" unit="" quT="22" />
    </section>
  </assessment>
  <isValid>1</isValid>
  <isPrintable>0</isPrintable>
  <qualityReview>0</qualityReview>
  <numberOfGenerations>1</numberOfGenerations>
  <requiresInvigilation>0</requiresInvigilation>
  <advanceContentDownloadTimespan>999</advanceContentDownloadTimespan>
  <automaticVerification>0</automaticVerification>
  <offlineMode>0</offlineMode>
  <requiresValidation>1</requiresValidation>
  <requiresSecureClient>0</requiresSecureClient>
  <examType>0</examType>
  <advanceDownload>0</advanceDownload>
  <gradeBoundaryData>
    <gradeBoundaries passLevelType="1" adjustedGradeBoundaries="0">
      <grade modifier="lt" value="0" description="Fail" />
      <grade modifier="gt" value="0" description="Pass" />
    </gradeBoundaries>
  </gradeBoundaryData>
  <autoViewExam>0</autoViewExam>
  <autoProgressItems>0</autoProgressItems>
  <confirmationText>
    <confirmationText />
  </confirmationText>
  <requiresConfirmationCheck>0</requiresConfirmationCheck>
  <allowCloseWithoutSubmit>1</allowCloseWithoutSubmit>
  <useSecureMarker>0</useSecureMarker>
  <annotationVersion>0</annotationVersion>
  <allowPackagingDelivery>1</allowPackagingDelivery>
  <scoreBoundaryData>
    <scoreBoundaries showScoreBoundaryColumn="0">
      <score modifier="lt" value="40" description="Not met" higherBoundarySet="1" />
      <score modifier="lt" value="40" description="Not met" higherBoundarySet="0" />
      <score modifier="gt" value="40" description="Close to met" higherBoundarySet="1" />
      <score modifier="gt" value="40" description="Met" higherBoundarySet="0" />
      <score modifier="gt" value="45" description="Met" higherBoundarySet="1" />
      <score modifier="gt" value="80" description="Exceeded" higherBoundarySet="1" />
    </scoreBoundaries>
  </scoreBoundaryData>
  <showCandidateReportResultColumn>0</showCandidateReportResultColumn>
  <showCandidateReportPercentageColumn>1</showCandidateReportPercentageColumn>
  <certifiedAccessible>0</certifiedAccessible>
  <markingProgressVisible>1</markingProgressVisible>
  <markingProgressMode>1</markingProgressMode>
  <secureClientOperationMode>1</secureClientOperationMode>
  <examDeliverySystemStyleType>delivery_flat</examDeliverySystemStyleType>
  <markingAutoVoidPeriod>365</markingAutoVoidPeriod>
  <showPageRequiresScrollingAlert>1</showPageRequiresScrollingAlert>
  <project ID="5050" />
</assessmentDetails>'
where id = 749

update WAREHOUSE_ExamSessionTable_Shreded
set resultData = '<exam passMark="0" passType="1" originalPassMark="0" originalPassType="1" totalMark="1" userMark="0" userPercentage="0" passValue="1" originalPassValue="1" totalTimeSpent="168016" closeBoundaryType="0" closeBoundaryValue="0" grade="Pass" originalGrade="Pass">
  <gradeBoundaryData>
    <gradeBoundaries passLevelType="1" adjustedGradeBoundaries="0">
      <grade modifier="lt" value="0" description="Fail" />
      <grade modifier="gt" value="0" description="Pass" />
    </gradeBoundaries>
  </gradeBoundaryData>
  <section id="1" passMark="0" passType="1" name="Section" totalMark="0" userMark="0" userPercentage="0" passValue="1" totalTimeSpent="168016" itemsToMark="0">
    <item id="5050P2982" name="GCSE WSL - candidate" totalMark="0" userMark="0" actualUserMark="0" markingType="1" markerUserMark="0" viewingTime="168016" userAttempted="1" version="7" LO="" type="1" nonScored="0" />
  </section>
</exam>'
where examSessionid = 749

*/