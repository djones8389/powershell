IF OBJECT_ID (N'dbo.fn_getAuditXML', N'FN') IS NOT NULL  
    DROP FUNCTION dbo.fn_getAuditXML;  
GO  

create function [fn_getAuditXML](@ID nvarchar(max))
returns XML 
as
BEGIN
Declare @XML XML
SELECT @XML = 
CAST((
    SELECT TAG
     , PARENT
     , [exam!1!Element]
     , [exam!1!id]
     , [stateChange!2!newStateID!Element]
     , [stateChange!2!newState!Element]
     , [stateChange!2!changeDate!Element]
     , [stateChange!2!information!xml]
    FROM (
     SELECT 1 AS TAG
      , NULL AS Parent
      , NULL AS [exam!1!Element]
      , NULL AS [exam!1!id]
      , NULL AS [stateChange!2!newStateID!Element]
      , NULL AS [stateChange!2!newState!Element]
      , NULL AS [stateChange!2!changeDate!Element]
      , NULL AS [stateChange!2!information!xml]
     
     UNION ALL
     
     SELECT 2 AS Tag
      , 1 AS Parent
      , NULL
      , ESCAT.ExamSessionID
      , ESCAT.NewState
      , ESLT.StateName
      , CONVERT(VARCHAR(23), ESCAT.StateChangeDate, 113)
      , ESCAT.StateInformation
     FROM ExamStateChangeAuditTable ESCAT
     INNER JOIN ExamStateLookupTable ESLT
      ON ESLT.ID = ESCAT.NewState
     WHERE ESCAT.ExamSessionID IN (
       SELECT ID
       FROM ExamSessionTable
       WHERE ID = @ID
       )
     ) AS stateChangeXml
    FOR XML EXPLICIT
    ) AS XML) 
	return @XML
END
GO


WITH Limbos
AS (
 SELECT ROW_NUMBER() OVER (
   PARTITION BY SME.ExamSessionID ORDER BY SME.ExamSessionID
    , SME.DateStamp DESC
   ) AS RowNum
  , SME.ID AS StateManagementExceptionID
  , SME.ExamSessionID
  , dbo.[fn_getAuditXML] (est.id) [Auditing]
 FROM ExamSessionTable AS EST
 LEFT JOIN StateManagementException AS SME
  ON EST.ID = SME.ExamSessionID
 LEFT JOIN WAREHOUSE_ExamSessionTable AS WEST
  ON EST.ID = WEST.ExamSessionID
 LEFT JOIN ExamSessionDurationAuditTable AS ESDAT
  ON EST.ID = ESDAT.examSessionId
 INNER JOIN ExamStateChangeAuditTable AS ESCAT
  ON EST.ID = ESCAT.ExamSessionID
 WHERE examState = 99
  AND SME.MovedToLimbo = 1
 )
SELECT *
FROM Limbos
WHERE rownum = 1


IF OBJECT_ID (N'dbo.fn_getAuditXML', N'FN') IS NOT NULL  
    DROP FUNCTION dbo.fn_getAuditXML;  
GO  