USE [SANDBOX_Demo_SecureAssess]

IF OBJECT_ID('tempdb..#DataHolder') IS NOT NULL DROP TABLE #DataHolder;

SELECT F.*
	, ItemResponseData
INTO #DataHolder
FROM (
	select E.*
	FROM (
	select  
		 WAREHOUSEExamSessionID
		 ,ItemID
		, a.b.value('@id','tinyint') Scene
		, c.d.value('@id','tinyint') Component
	from WAREHOUSE_ExamSessionItemResponseTable
	cross apply itemresponsedata.nodes('p/s') a(b)
	cross apply a.b.nodes('c') c(d)

	) E
	group by 
		 WAREHOUSEExamSessionID, ItemID, Scene, Component
		having count(*) > 1
) F
INNER JOIN WAREHOUSE_ExamSessionItemResponseTable G
on G.WAREHOUSEExamSessionID = F.WAREHOUSEExamSessionID
	and G.ItemID = F.ItemID

where ItemResponseData.exist('p/s/c[@id=5]') = 0
	order by 1,2


DECLARE myCursor CURSOR FOR
SELECT WAREHOUSEExamSessionID	
	, ItemID
	, Scene
	, Component
	, ItemResponseData
FROM #DataHolder

DECLARE @WarehouseExamSessionID int, @ItemID nvarchar(12), @Scene CHAR(1), @Component CHAR(1), @ItemResponseData XML

OPEN myCursor

FETCH NEXT FROM myCursor INTO @WarehouseExamSessionID,@ItemID,@Scene,@Component,@ItemResponseData 

WHILE @@FETCH_STATUS = 0

BEGIN

UPDATE #DataHolder
SET ItemResponseData.modify('replace value of(/p/s/c/@id)[1] with ("5")')
	where WAREHOUSEExamSessionID = @WarehouseExamSessionID
		and ItemID = @ItemID

select @WarehouseExamSessionID,@ItemID,@Scene,@Component,@ItemResponseData 

FETCH NEXT FROM myCursor INTO @WarehouseExamSessionID,@ItemID,@Scene,@Component,@ItemResponseData 

END
CLOSE myCursor
DEALLOCATE myCursor



SELECT  ItemResponseData.query('.')
	, *
FROM #DataHolder


