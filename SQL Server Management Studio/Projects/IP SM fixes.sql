use PPD_BCIntegration_SecureMarker

ALTER TABLE [UniqueGroupResponses] DROP CONSTRAINT [FK_UniqueGroupResponses_GroupDefinitions];
ALTER TABLE [UniqueGroupResponses] DROP CONSTRAINT [FK_UniqueGroupResponses_TokenStore];
ALTER TABLE [UniqueGroupResponses] DROP CONSTRAINT [FK_UniqueGroupResponses_UniqueGroupResponses]

ALTER TABLE [dbo].[UniqueGroupResponseParkedReasonCrossRef] DROP CONSTRAINT [FK_UniqueGroupResponseParkedReasonCrossRef_UniqueGroupResponseParkedReasonCrossRef];
ALTER TABLE [dbo].[AffectedCandidateExamVersions] DROP CONSTRAINT [FK_AffectedCandidateExamVersions_UniqueGroupResponses]
ALTER TABLE [dbo].[AssignedGroupMarks] DROP CONSTRAINT [FK_AssignedGroupMarks_UniqueGroupResponses]
ALTER TABLE [dbo].[CandidateGroupResponses] DROP CONSTRAINT [FK_CandidateGroupResponses_UniqueGroupResponses]
ALTER TABLE [dbo].[UniqueGroupResponseLinks] DROP CONSTRAINT [FK_UniqueGroupResponseLinks_UniqueGroupResponses]
ALTER TABLE [UniqueGroupResponses] DROP CONSTRAINT [PK_UniqueGroupResponses]


DROP FUNCTION [ag_CLR_Concat];
DROP FUNCTION [ParamsToList];
DROP FUNCTION [ParamsToThreeColumnTable];
DROP FUNCTION [sm_checkGroupAndItemsMarkedInTolerance_fn];
DROP FUNCTION [sm_checkGroupCIsExceeded_fn];
DROP FUNCTION [sm_checkIsUniqueGroupResponseInConfirmedScript];
DROP FUNCTION [sm_checkUser1CiawayFromSuspend];
DROP FUNCTION [sm_getAffectedUGRs_fn_OLD];
DROP FUNCTION [sm_getAffectedUGRsCount_fn_OLD];
DROP FUNCTION [sm_getExamPermissionsForScriptReview_fn];
DROP FUNCTION [sm_getIsNewItem_fn];
DROP FUNCTION [sm_getMarkingExaminerReport_fn];
DROP FUNCTION [sm_getMarkingReportItems_fn];
DROP FUNCTION [sm_getQuotaForUserForExam_fn];
DROP FUNCTION [sm_getUserQuotaForGroup_fn];
DROP FUNCTION [fn_diagramobjects];
DROP FUNCTION [getExamsWithRestrictions_fn];
DROP FUNCTION [getAffectedScripts_fn];
DROP FUNCTION [getAffectedUniqueResponses_fn];
DROP FUNCTION [getAvailableUniqueGroupResponses_fn];
DROP FUNCTION [getUserExamsWithMarkingPermissions_fn];
DROP FUNCTION [getGroupDefinitionsJoined_fn];
DROP FUNCTION [getGroupSourceTable_fn];
DROP FUNCTION [getGroupAggregatedSourceForMarking_fn];
DROP FUNCTION [getGroupsWithAffectedResponsesForMarking_fn];
DROP FUNCTION [sm_MARKINGSERVICE_GetAllGroupsForUser_fn];
DROP FUNCTION [getCentreNameAndCodeForUniqueGroupResponse_fn];
DROP FUNCTION [sm_getExamPermissionsForEscalations_fn];
DROP FUNCTION [getGroupDefinitionsForEscalations_fn];
DROP FUNCTION [sm_EscalatedUGRs_fn];
DROP FUNCTION [getGroupAggregatedSourceForHome_fn];
DROP FUNCTION [getGroupsWithAffectedResponsesForHome_fn];
DROP FUNCTION [sm_getAffectedScripts_fn_OLD];
DROP FUNCTION [sm_getQuotaManagementInfoForUser_fn];
DROP FUNCTION [sm_hasUserRealMarkedResponsesForExam_fn];
DROP FUNCTION [sm_MANAGEEXAMINERS_GetExaminers_fn];

DROP TABLE [UniqueGroupResponses];

SELECT 'DROP FUNCTION '  + QUOTENAME(name) + ';'
FROM sys.objects 
WHERE RIGHT(type_desc, 8) = 'FUNCTION'

SELECT 'DROP VIEW '  + QUOTENAME(name) + ';'
FROM sys.objects 
WHERE RIGHT(type_desc, 8) = 'View'



exec sp_rename 'temp_UniqueGroupResponses','UniqueGroupResponses'

ALTER TABLE [dbo].[UniqueGroupResponses] ADD CONSTRAINT [PK_UniqueGroupResponses] PRIMARY KEY CLUSTERED( [ID] ASC);


ALTER TABLE [dbo].[UniqueGroupResponseParkedReasonCrossRef]  WITH CHECK ADD  CONSTRAINT [FK_UniqueGroupResponseParkedReasonCrossRef_UniqueGroupResponseParkedReasonCrossRef] FOREIGN KEY([UniqueGroupResponseId])
REFERENCES [dbo].[UniqueGroupResponses] ([ID])

ALTER TABLE [dbo].[UniqueGroupResponseParkedReasonCrossRef] CHECK CONSTRAINT [FK_UniqueGroupResponseParkedReasonCrossRef_UniqueGroupResponseParkedReasonCrossRef]
GO


ALTER TABLE [dbo].[AffectedCandidateExamVersions]  WITH CHECK ADD  CONSTRAINT [FK_AffectedCandidateExamVersions_UniqueGroupResponses] FOREIGN KEY([UniqueGroupResponseId])
REFERENCES [dbo].[UniqueGroupResponses] ([ID])
GO



ALTER TABLE [dbo].[UniqueGroupResponses]  WITH CHECK ADD  CONSTRAINT [FK_UniqueGroupResponses_GroupDefinitions] FOREIGN KEY([GroupDefinitionID])
REFERENCES [dbo].[GroupDefinitions] ([ID])
GO
ALTER TABLE [dbo].[UniqueGroupResponses] CHECK CONSTRAINT [FK_UniqueGroupResponses_GroupDefinitions]
GO
ALTER TABLE [dbo].[UniqueGroupResponses]  WITH NOCHECK ADD  CONSTRAINT [FK_UniqueGroupResponses_TokenStore] FOREIGN KEY([TokenId])
REFERENCES [dbo].[TokenStore] ([id])
GO
ALTER TABLE [dbo].[UniqueGroupResponses] CHECK CONSTRAINT [FK_UniqueGroupResponses_TokenStore]
GO
ALTER TABLE [dbo].[UniqueGroupResponses]  WITH CHECK ADD  CONSTRAINT [FK_UniqueGroupResponses_UniqueGroupResponses] FOREIGN KEY([OriginalUniqueGroupResponseId])
REFERENCES [dbo].[UniqueGroupResponses] ([ID])
GO
ALTER TABLE [dbo].[UniqueGroupResponses] CHECK CONSTRAINT [FK_UniqueGroupResponses_UniqueGroupResponses]
GO
ALTER TABLE [dbo].[AssignedGroupMarks]  WITH CHECK ADD  CONSTRAINT [FK_AssignedGroupMarks_UniqueGroupResponses] FOREIGN KEY([UniqueGroupResponseId])
REFERENCES [dbo].[UniqueGroupResponses] ([ID])
GO
ALTER TABLE [dbo].[CandidateGroupResponses]  WITH CHECK ADD  CONSTRAINT [FK_CandidateGroupResponses_UniqueGroupResponses] FOREIGN KEY([UniqueGroupResponseID])
REFERENCES [dbo].[UniqueGroupResponses] ([ID])
GO
ALTER TABLE [dbo].[UniqueGroupResponseLinks]  WITH CHECK ADD  CONSTRAINT [FK_UniqueGroupResponseLinks_UniqueGroupResponses] FOREIGN KEY([UniqueGroupResponseID])
REFERENCES [dbo].[UniqueGroupResponses] ([ID])
GO

ALTER TABLE [dbo].[UniqueGroupResponseLinks] CHECK CONSTRAINT [FK_UniqueGroupResponseLinks_UniqueGroupResponses]
GO
