    declare @rc int
    declare @TraceID int
    declare @maxfilesize bigint
    declare @DateTime datetime

    set @maxfilesize = 500
    set @DateTime = (select dateadd(DAY, 7, getdate()))

    exec @rc = sp_trace_create @TraceID output, 0, N'E:\EAL_SecureAssessRPCComplete', @maxfilesize, @Datetime 
    if (@rc != 0) goto error

    declare @on bit
    set @on = 1

    exec sp_trace_setfilter @TraceID,35,0,0,N'EAL_SecureAssess'
    
	/*SP Complete*/
    --exec sp_trace_setevent @TraceID, 43, 12, @on
    --exec sp_trace_setevent @TraceID, 43, 13, @on
    --exec sp_trace_setevent @TraceID, 43, 34, @on    
    
	/*RPC Complete*/
    --exec sp_trace_setevent @TraceID, 10, 12, @on
	--exec sp_trace_setevent @TraceID, 10, 13, @on
	--exec sp_trace_setevent @TraceID, 10, 34, @on
                
    declare @intfilter int
    declare @bigintfilter bigint

    exec sp_trace_setfilter @TraceID, 34, 0, 7, N'sp_reset_connection'
    exec sp_trace_setfilter @TraceID, 34, 0, 7, N'sp_executesql'
    
    exec sp_trace_setstatus @TraceID, 1

    
    select TraceID=@TraceID
    goto finish

    error: 
    select ErrorCode=@rc

    finish:
  