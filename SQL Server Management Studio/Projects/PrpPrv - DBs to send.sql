use master

ALTER DATABASE [PRV_NCFE_CPProjectAdmin] SET OFFLINE;
ALTER DATABASE [PRV_NCFE_Itembank] SET OFFLINE;
ALTER DATABASE [PRV_NCFE_SecureAssess] SET OFFLINE;
--sp_whoisactive

SELECT
    db.name AS DBName,
	'ALTER DATABASE ' + QUOTENAME(db.name) + ' SET OFFLINE;',
	'ALTER DATABASE ' + QUOTENAME(db.name) + ' SET ONLINE;',
    type_desc AS FileType,
    Physical_Name AS Location,mf.size/128 as Size_in_MB
FROM
    sys.master_files mf
INNER JOIN 
    sys.databases db ON db.database_id = mf.database_id

where type_desc = 'ROWS'
	and db.name in (

	select name
	from sys.databases
	where database_id > 4
		and name not like '%ocr%'
		and name not like '%demo%'
		and name  like '%ncfe%'
		and name not like '%AbcAwards%'
		and name not like '%aat%'
		and name not like '%evolve%'
)

order by 1;
--ORDER BY  Size_in_MB DESC,DBName


select name
	
from sys.databases
where database_id > 4
	and state_desc = 'Online'
order by 1
