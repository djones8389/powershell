SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
SET NOCOUNT ON;

IF OBJECT_ID('tempdb..#ItemIDs') IS NOT NULL DROP TABLE #ItemIDs; 
IF OBJECT_ID('tempdb..#Questions') IS NOT NULL DROP TABLE #Questions;
IF OBJECT_ID('tempdb..#Answers') IS NOT NULL DROP TABLE #Answers;

USE [PRV_Evolve_ItemBank];

	DECLARE @ExamPrefix smallint = '6008';

	DECLARE @ExamSuffix TABLE (
		ID INT
	);
	INSERT @ExamSuffix
	VALUES(601),(602),(603),(604),(605),(606),(608),(609),(611),(614),(615),(616),(617),(690),(701),(702),(703),(705),(706),(709),(710),(711),(790); 

CREATE TABLE #ItemIDs (
	ItemID nvarchar(20)
	,[FacilityValue] float
	,[ItemUsageCount] smallint
);

INSERT #ItemIDs (ItemID)
SELECT REPLACE(REPLACE(cast(a.b.query('.') as NVARCHAR(MAX)), '<XPath>@ID="',''), '"</XPath>','') ItemID
FROM AssessmentTable AT
		
CROSS APPLY AssessmentRules.nodes('PaperRules//XPath') a(b)

WHERE LEFT(a.b.value('.','nvarchar(MAX)'), 3) = '@ID'
	AND AT.ID IN (
			SELECT AT.ID
			FROM [dbo].[AssessmentGroupTable] AGT
			INNER JOIN AssessmentTable AT
			ON AGT.ID = AT.AssessmentGroupID
			INNER JOIN @ExamSuffix A
			on A.ID = substring(Name, CHARINDEX('-',Name)+1,3)
			where [Name] like '%' + cast(@ExamPrefix as nvarchar(4)) + '%'
		);


UPDATE #ItemIDs
set FacilityValue = ITS.FacilityValue
	, ItemUsageCount = ITS.ItemUsageCount
from #ItemIDs A
INNER JOIN ItemStatsTable ITS
on (cast(ITS.ProjectID as nvarchar(10)) + 'P' + cast(ITS.ItemID as nvarchar(10))) = A.itemid;

CREATE CLUSTERED INDEX [IX_itemID] ON #itemids (itemID);



USE [PRV_Evolve_CPProjectAdmin];


	select 
		   substring(Q.ID, 0, charindex('S', Q.ID)+2) UpToScene
		   , SUBSTRING(Q.ID,charindex('C',Q.ID)+1, (Len(Q.ID)-charindex('I',Q.ID)))  C
		   , cast(Q.Question.query('ItemValue/TEXTFORMAT/P//FONT/.//text()') as nvarchar(MAX)) Question 
	INTO #Questions
	from (
				  select ITT.ID 
						 , CAST(ItemValue AS xml)  Question
				  from [ItemTextBoxTable] ITT WITH (READUNCOMMITTED)
				  INNER JOIN #ItemIDs I
				  ON I.ItemID = substring(ITT.ID, 0, charindex('S', ITT.ID))
				  where i.ItemID not like '445P460%'
	) Q;

	CREATE NONCLUSTERED INDEX [IX_NC] on #QUESTIONS (UpToScene,C) INCLUDE(Question);

	SELECT  ROW_NUMBER() OVER(PARTITION BY ParentID ORDER BY ID, [asC]) AS [N],
		   ParentID,
		   [asC] AS [AnswerAlias], 
		   CAST(ItemXml.query('.//text()') AS nvarchar(max)) AS [AnswerText],
		   [Key]
	INTO #Answers
	FROM (
	SELECT MCQ.ID,
			MCQ.ParentID,
			 MCQ.[asC],
			 CAST(MCQ.ItemValue AS xml) AS [ItemXml],
			 cor [Key]
	FROM dbo.ItemMultipleChoiceTable MCQ WITH (READUNCOMMITTED)
	INNER JOIN #ItemIDs I
	ON I.ItemID = substring(MCQ.ID, 0, charindex('S', MCQ.ID))
	where i.ItemID not like '445P460%'
	
	) AS [A];

	CREATE NONCLUSTERED INDEX [IX] ON [dbo].[#Answers] ([ParentID]) INCLUDE ([N],[AnswerAlias],[AnswerText]);

 
	declare @n table (
		   n float 
	);

	INSERT @n
	SELECT  DISTINCT N
	FROM  #Answers
	order by N;

	DECLARE @OptionList nvarchar(max) = (

		   SELECT 
				  distinct
				  substring((
						 SELECT ', ' + QUOTENAME(cast([N] as varchar(10)))
						 FROM @n 
						 order by [N]
						 FOR XML PATH('')
				  ), 2,1000)
		   FROM @n 
	);

	DECLARE @ISNULL Nvarchar(max) = (SELECT REPLACE(REPLACE(@OptionList, '[','ISNULL(['), ']','],'''') [Answer Option]'))

	DECLARE @DynamicString nvarchar(MAX) = '';
	SELECT @DynamicString += '

	SELECT  
		   [Item Ref]
		   ,ProjectInfo.[Item Name]
		   ,ISNULL(Comment.checkedInCmt, '''') [Comment]
		   ,(
				  SELECT Question + ''  ''
				  from #QUESTIONS Q2
				  where q2.UpToScene = b.UpToScene
						 and (cast(b.c as int) - cast(q2.c as int)) in (1,2)
				  for xml path ('''')
		   ) Question
		   ,'+@ISNULL+'
		   ,AnswerAlias [Key]
		   ,ItemUsageCount
		   ,FacilityValue
		   ,ProjectInfo.Unit
		   ,ProjectInfo.LO
		   ,ProjectInfo.SyllabusReference
		   ,ProjectInfo.TestSpecReference
		   ,ISNULL(ProjectInfo.EnemyItems,'''') EnemyItems
		   ,ProjectInfo.DateImported
	FROM (
				  SELECT p.ParentID
						 , substring(ParentID, 0, charindex(''S'',ParentID)+2) UpToScene
						 , SUBSTRING(ParentID,charindex(''C'',ParentID)+1, (Len(ParentID)-charindex(''I'',ParentID))) C
				  ,'+@OptionList+'
                     
				  FROM (
						 SELECT ParentID, N, AnswerAlias + N'': '' + AnswerText AS [AnswerText]
						 FROM #Answers
				  ) AS [S]
				  PIVOT (
						 MAX(AnswerText)
						 FOR N IN ('+@OptionList+')
				  ) AS [P]
	)  B
	
	INNER JOIN (

	select 
		cast(ID as nvarchar(10)) + ''P'' + pro.pag.value(''@ID'',''nvarchar(500)'') [Item Ref]
		,pro.pag.value(''@Nam'',''nvarchar(500)'') [Item Name]
		,ItemUsageCount
		,FacilityValue
		,pro.pag.value(''@Unit'',''nvarchar(500)'') [Unit]
		,pro.pag.value(''@LO'',''nvarchar(500)'') [LO]
		,pro.pag.value(''@SyllabusReference'',''nvarchar(500)'') [SyllabusReference]
		,pro.pag.value(''@TestSpecReference'',''nvarchar(500)'') [TestSpecReference]
		,pro.pag.value(''@SurpassEnemyItems'',''nvarchar(500)'') [EnemyItems]
		,pro.pag.value(''@DateImported'',''nvarchar(500)'') [DateImported]
	from ProjectListTable plt
	cross apply ProjectStructureXml.nodes(''Pro//Pag'') pro(pag)
	INNER JOIN #itemids ON #itemids.ItemID = cast(ID as nvarchar(10)) + ''P'' + pro.pag.value(''@ID'',''nvarchar(500)'')
	WHERE ID IN (select distinct substring(itemid, 0,CHARINDEX(''P'',itemid)) from #itemids)

	) ProjectInfo
		on substring(B.ParentID, 0, CHARINDEX(''S'',B.ParentID)) = [Item Ref]


    INNER JOIN #Answers A
	ON substring(a.ParentID, 0, charindex(''S'',a.ParentID)) = ProjectInfo.[Item Ref]
		and [Key] = 1
	
	LEFT JOIN (
		select LastComment.ID
			, LastComment.checkedInCmt
		FROM (
			select ID
				, checkedInCmt
				,MAX(R) R
			FROM (
				select ID
					, ROW_NUMBER() OVER(PARTITION BY ID ORDER  BY ID) R
					, c.d.value(''.'',''nvarchar(500)'') checkedInCmt
				FROM (
				select  ID
					, cast(itemvalue as xml) itemvalue
				from PageTable
				INNER JOIN #itemids ON #itemids.ItemID = PageTable.ID
				) A
				cross apply itemvalue.nodes(''ItemValue/metaData/HISTORY/USER/checkedInCmt/text()'') c(d)
			) b
			group by ID,checkedInCmt  
		) LastComment

	) Comment
		ON Comment.ID = substring(a.ParentID, 0, charindex(''S'',a.ParentID))

	'
EXEC(@DynamicString);

