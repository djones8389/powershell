|SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

use [PRV_BritishCouncil_TestPackageManager]

select ScheduledExamRef
	--, SurpassCandidateRef
	--, SurpassCandidateRef
	--, CenterName
	--, QualificationId
	, QualificationName
	, QualificationRef
	--, SurpassExamId
	, SurpassExamRef [Exam Name]
	, pack.Name [Package Name]
	, pe.name [Component Name]
	, PackageDate
	, rs.RescheduleReason
	, rs.RescheduledBy
	, rs.RescheduleDate
from ScheduledPackageCandidateExams as SPCE

	inner join ScheduledPackageCandidates as SPC
	on SPC.ScheduledPackageCandidateId =  SPCE.Candidate_ScheduledPackageCandidateId
	inner join ScheduledPackages as SP
     on SP.ScheduledPackageId =  SPC.ScheduledPackage_ScheduledPackageId
    inner join [dbo].[PackageExams] pe
    on pe.[PackageExamId] = SPCE.[PackageExamId]
    inner join [dbo].[Packages] pack
    on pack.[PackageId] = pe.[PackageId]
	inner join [RescheduleDetails] rs 
	on spce.[ScheduledPackageCandidateExamID] = rs.[ScheduledPackageCandidateExamId]

where ScheduledExamRef = 'BWTGFP99'

--Because they failed their exam - BTLDaveJ--
/*	RescheduleDetailId	ScheduledPackageCandidateExamId	RescheduleReason	RescheduleDate	RescheduledBy
	4504				4504	1880761	Because they failed their exam - BTLDaveJ	2017-03-07 12:20:22.373	superuser
*/

select *
from [RescheduleDetails] rs
inner join [ScheduledPackageCandidateExams] spce
on spce.[ScheduledPackageCandidateExamID] = rs.[ScheduledPackageCandidateExamId]



[ScheduledPackageCandidateExams].[ScheduledPackageCandidateExamID]

select *
from ScheduledPackageCandidateExams
where ScheduledExamRef = 'BWTGFP99'

--1880761  = rescheduled exam.