SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

use ICAEW_SecureAssess

SELECT  wesirt.ID
	, west.ID
	, ItemID
	, ItemVersion
	, KeyCode
	, ItemResponseData
FROM WAREHOUSE_ExamSessionItemResponseTable wesirt (READUNCOMMITTED)
inner join WAREHOUSE_ExamSessionTable west (READUNCOMMITTED)
on west.id = wesirt.warehouseexamsessionid
where   itemid = '368p1000'
	and ItemVersion = 134
	and warehouseTime > DATEADD(DAY, -8, GETDATE())
	and keycode = 'CTRC97F6'

	select itemdatafull
	from WAREHOUSE_ExamSessionTable_Shreded
	where examsessionid = 8017

