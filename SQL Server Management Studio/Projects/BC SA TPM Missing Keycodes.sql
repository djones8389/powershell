SELECT ID, keycode, warehouseTime
FROM [PRV_BritishCouncil_SecureAssess].dbo.WAREHOUSE_ExamSessionTable  SA
LEFT JOIN [PRV_BritishCouncil_TestPackageManager].dbo.ScheduledPackageCandidateExams  TPM
on SA.Keycode =  TPM.ScheduledExamRef collate SQL_Latin1_General_CP1_CI_AS
where TPM.ScheduledExamRef IS NULL
	order by warehouseTime desc;

/****** Script for SelectTopNRows command from SSMS  ******/
SELECT [Key]
      ,[Value]
  FROM [PRV_BritishCouncil_TestPackageManager].[dbo].[CommonSettings]
  --Key	Value UpdateResultsLastCall	03/03/2016 15:02:19

--UPDATE [PRV_BritishCouncil_TestPackageManager].[dbo].[CommonSettings]
--set value = '03/03/2016 14:48:19'