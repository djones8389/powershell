 
RESTORE DATABASE [SANDBOX_EBS_SecureAssess] FROM DISK = N'T:\Backup\EBS_SecureAssess.2017.12.07.bak' WITH FILE = 1
, NOUNLOAD, NOREWIND, REPLACE, STATS = 10
, MOVE 'Data' TO N'S:\DATA\SANDBOX_EBS_SecureAssess.mdf'
, MOVE 'Log' TO N'L:\LOGS\SANDBOX_EBS_SecureAssess.ldf'; 
go

IF NOT EXISTS (SELECT 1 FROM Sys.sql_logins where name ='SANDBOX_EBS_SecureAssess_Login') 
	CREATE LOGIN [SANDBOX_EBS_SecureAssess_Login] WITH PASSWORD = 'Q~0X\,L?CU=5zrgtAG@S';  
	USE [SANDBOX_EBS_SecureAssess];  	
	IF EXISTS (
		SELECT 1
		FROM sys.database_principals
		WHERE NAME ='SANDBOX_EBS_SecureAssess_User'
		)
	DROP USER [SANDBOX_EBS_SecureAssess_User];
	
	CREATE USER [SANDBOX_EBS_SecureAssess_User] FOR LOGIN [SANDBOX_EBS_SecureAssess_Login]; EXEC sp_addrolemember 'db_owner','SANDBOX_EBS_SecureAssess_User'
	