USE [PPD_BCIntegration_SecureMarker]
GO
/****** Object:  View [dbo].[UserExamPermissions_view]    Script Date: 16/05/2016 14:18:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[UserExamPermissions_view]
WITH SCHEMABINDING
AS
	SELECT E.ID AS ExamID, RP.PermissionID, AUR.UserID
			FROM	dbo.Exams E
			JOIN	dbo.AssignedUserRoles AUR ON AUR.ExamID = E.ID OR AUR.ExamID = 0
			JOIN	dbo.RolePermissions RP ON  (AUR.RoleID = RP.RoleID AND RP.PermissionID IN (12 ,13 ,14, 42))



GO
/****** Object:  View [dbo].[UserExamUnrestrictedExaminer_view]    Script Date: 16/05/2016 14:18:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[UserExamUnrestrictedExaminer_view]
WITH SCHEMABINDING
AS
	SELECT UserId, ExamId, 0 AS MarkingRestriction 
			FROM dbo.UserExamPermissions_view 
			WHERE PermissionID = 42
			GROUP BY UserID, ExamId




GO
/****** Object:  View [dbo].[UserExamExaminer_view]    Script Date: 16/05/2016 14:18:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[UserExamExaminer_view]
WITH SCHEMABINDING
AS
	SELECT UserId, ExamId, 0 AS MarkingRestriction 
			FROM dbo.UserExamPermissions_view 
			WHERE PermissionID IN (12 ,13 ,14)
			GROUP BY UserID, ExamId





GO
/****** Object:  View [dbo].[GroupCentreCandidate_view]    Script Date: 16/05/2016 14:18:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [dbo].[GroupCentreCandidate_view]
WITH SCHEMABINDING
AS
	SELECT CGR.UniqueGroupResponseID AS ID
	, CGR.CandidateExamVersionID
	, CEV.CandidateForename
	, CEV.CandidateSurname
	, CEV.CandidateRef
	, CEV.CentreName
	, CEV.CentreCode
	, CEV.ID as ScriptId
	FROM dbo.CandidateExamVersions CEV
	INNER JOIN dbo.CandidateGroupResponses CGR ON CGR.CandidateExamVersionID = CEV.ID





GO
/****** Object:  View [dbo].[CandidateCentreResponse_view]    Script Date: 16/05/2016 14:18:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [dbo].[CandidateCentreResponse_view]
WITH SCHEMABINDING
AS
	SELECT ID,
			(CASE 
				WHEN COUNT(CandidateExamVersionID) = 1 
					THEN 
						MAX(CandidateForename)+ ' ' + MAX(CandidateSurname)
					ELSE
						''
			END) AS CandidateName,
			(CASE
				WHEN COUNT(CandidateExamVersionID) = 1 
					THEN 
						MAX(CandidateRef)
					ELSE
						''
			END) AS CandidateRef,
			(CASE 
				WHEN COUNT(CandidateExamVersionID) = 1 
					THEN 
						MAX(CentreName)
					ELSE
						''
			END) AS CentreName,
			(CASE 
				WHEN COUNT(CandidateExamVersionID) = 1 
					THEN 
						MAX(CentreCode)
					ELSE
						''
			END) AS CentreCode,
			(CASE 
				WHEN COUNT(CandidateExamVersionID) = 1 
					THEN 
						MAX(ScriptId)
					ELSE
						-1
			END) AS ScriptId
	FROM dbo.GroupCentreCandidate_view
	GROUP BY Id



GO
/****** Object:  View [dbo].[EscalatedUniqueGroupResponses_view]    Script Date: 16/05/2016 14:18:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





CREATE VIEW [dbo].[EscalatedUniqueGroupResponses_view]
WITH SCHEMABINDING
AS
	SELECT [ID]
	,[GroupDefinitionID]
	,[CheckSum]
	,[InsertionDate]
	,[CI_Review]
	,[CI]
	,[VIP]
	,[ConfirmedMark]
	,[ParkedAnnotation]
	,[ParkedDate]
	,[TokenId]
	,[ParkedUserID]
	,[ScriptType]
	,[Feedback]
	,[IsEscalated]
	,[OriginalUniqueGroupResponseId]
    FROM dbo.UniqueGroupResponses
	WHERE IsEscalated = 1


GO
/****** Object:  View [dbo].[EscalatedUniqueGroupResponsesForCandidateCentre_view]    Script Date: 16/05/2016 14:18:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[EscalatedUniqueGroupResponsesForCandidateCentre_view]
WITH SCHEMABINDING
AS
	SELECT UGR.ParkedUserID AS ExaminerId
		, UGR.ParkedDate AS DateEscalated
		, UGR.ID AS ID
		, UGR.ParkedUserID as ParkedUserId
		, UGR.GroupDefinitionId
		, UGR.ParkedDate
		, T.CandidateName AS CandidateName
		, T.CandidateRef AS CandidateRef
		, T.CentreName AS CentreName
		, T.CentreCode AS CentreCode
		, T.ScriptId AS ScriptId
	FROM dbo.EscalatedUniqueGroupResponses_view UGR
		INNER JOIN dbo.CandidateCentreResponse_view T ON T.ID = UGR.ID



GO
/****** Object:  View [dbo].[GroupIdExamVersionName_view]    Script Date: 16/05/2016 14:18:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[GroupIdExamVersionName_view]
WITH SCHEMABINDING
AS
	SELECT GDI.GroupID
			, EV.Name
			, EV.ExternalExamReference
	FROM dbo.GroupDefinitionItems GDI
		JOIN dbo.Items I 
			ON (I.ID = GDI.ItemID)
		JOIN dbo.ExamVersionItemsRef EVIR
			ON EVIR.ItemID = I.ID
		JOIN dbo.ExamVersions EV
			ON (EV.ID = EVIR.ExamVersionID)



GO
/****** Object:  View [dbo].[ExamVersionNames_view]    Script Date: 16/05/2016 14:18:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[ExamVersionNames_view]
WITH SCHEMABINDING
AS
	SELECT groupSource.ExamID as ExamId
				, groupSource.Id as GroupId
				, GEV.Name as VersionName
				, GEV.ExternalExamReference
            FROM dbo.GroupDefinitions groupSource
			JOIN dbo.GroupIdExamVersionName_view GEV
				ON GEV.GroupId = groupSource.Id



GO
/****** Object:  View [dbo].[ExamVersionNamesConcatenated_view]    Script Date: 16/05/2016 14:18:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






CREATE VIEW [dbo].[ExamVersionNamesConcatenated_view]
WITH SCHEMABINDING
AS
	SELECT GD.ID AS GroupId,
		   GD.ExamID AS ExamId,
		   STUFF(
			(SELECT ', ' + VN.VersionName
				FROM dbo.ExamVersionNames_view VN
				WHERE VN.GroupId = GD.ID AND ExamID = GD.ExamID
				GROUP BY VN.VersionName
				ORDER BY VN.VersionName
				FOR XML PATH(''), TYPE
			).value('.','nvarchar(max)')
			,1,2, ''
          ) as VersionNames,
		   STUFF(
			(SELECT ', ' + VN.ExternalExamReference
				FROM dbo.ExamVersionNames_view VN
				WHERE VN.GroupId = GD.ID AND ExamID = GD.ExamID
				GROUP BY VN.ExternalExamReference
				ORDER BY VN.ExternalExamReference
				FOR XML PATH(''), TYPE
			).value('.','nvarchar(max)')
			,1,2, '') as ExternalExamReferences
	FROM dbo.GroupDefinitions GD


GO
/****** Object:  View [dbo].[ActiveGroups_view]    Script Date: 16/05/2016 14:18:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[ActiveGroups_view]
WITH SCHEMABINDING
AS
	SELECT GDSCR.GroupDefinitionID, COUNT_BIG(*) as RecordsCount
		FROM   dbo.GroupDefinitionStructureCrossRef GDSCR
			   INNER JOIN dbo.ExamVersionStructures EVS
					ON  EVS.ID = GDSCR.ExamVersionStructureID
		WHERE  GDSCR.Status = 0 -- 0 = Active
			   AND EVS.StatusID = 0 -- 0 = Released
		GROUP BY GDSCR.GroupDefinitionID


GO
/****** Object:  View [dbo].[CandidateExamVersionGroups_view]    Script Date: 16/05/2016 14:18:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[CandidateExamVersionGroups_view]
WITH SCHEMABINDING
AS
SELECT  UGR2.GroupDefinitionID, CGR2.CandidateExamVersionID, COUNT_BIG(*) AS CTBIG
FROM dbo.CandidateGroupResponses CGR2
 INNER JOIN dbo.UniqueGroupResponses UGR2 ON UGR2.ID = CGR2.UniqueGroupResponseID
GROUP BY UGR2.GroupDefinitionID, CGR2.CandidateExamVersionID



GO
/****** Object:  View [dbo].[ControlGroups_view]    Script Date: 16/05/2016 14:18:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





CREATE VIEW [dbo].[ControlGroups_view]
AS
	WITH cteCreator AS
	(
		SELECT
			 AGM.UniqueGroupResponseId
			,AGM.Timestamp AS CreationDate
			,U.id AS CreatorID
			,CASE 
				WHEN U.ID IS NOT NULL THEN U.Surname + ', ' + U.Forename
				ELSE AGM.FullName
			 END AS CreatorFullName 
		FROM dbo.AssignedGroupMarks AGM
		LEFT JOIN dbo.Users U ON U.ID = AGM.UserId 
		WHERE AGM.MarkingMethodId = 5 and AGM.IsConfirmedMark = 1
	),
	cteReviewer AS
	(
		SELECT
			 AGM.UniqueGroupResponseId
			,AGM.Timestamp AS ReviewDate
			,U.id AS ReviewerID
			,CASE 
				WHEN U.ID IS NOT NULL THEN U.Surname + ', ' + U.Forename
				ELSE AGM.FullName
			 END AS ReviewerFullName
		FROM dbo.AssignedGroupMarks AGM
		LEFT JOIN dbo.Users U ON U.ID = AGM.UserId 
		WHERE AGM.ID = (SELECT MAX(ID) FROM dbo.AssignedGroupMarks AGM1 
						WHERE AGM1.UniqueGroupResponseId = AGM.UniqueGroupResponseId
						AND AGM1.MarkingMethodId = 6)
	)
	,ctePresented as
	(
		SELECT
			 UniqueGroupResponseId 
			,COUNT(*) as CNT
		FROM dbo.AssignedGroupMarks AGM
		WHERE AGM.MarkingMethodId IN (2, 3)
		GROUP BY UniqueGroupResponseId 
	)
	,cteFailed as
	(
		SELECT
			 UniqueGroupResponseId 
			,COUNT(*) as CNT
		FROM dbo.AssignedGroupMarks AGM
			JOIN dbo.UniqueGroupResponses UGR ON UGR.ID = AGM.UniqueGroupResponseId
			JOIN dbo.GroupDefinitions GD on GD.ID = UGR.GroupDefinitionID
		WHERE 
			AGM.MarkingMethodId IN (2, 3)
			AND (AGM.IsEscalated = 1 OR 
			dbo.sm_checkGroupAndItemsMarkedInTolerance_fn(
				AGM.ID, 
				AGM.UniqueGroupResponseId,
				AGM.MarkingDeviation, 
				GD.CIMarkingTolerance, GD.IsAutomaticGroup) = 1
			)
		GROUP BY UniqueGroupResponseId 
	)
	,cteNotExact as
	(
		SELECT
			 UniqueGroupResponseId 
			,COUNT(*) as CNT
		FROM dbo.AssignedGroupMarks AGM
			JOIN dbo.UniqueGroupResponses UGR ON UGR.ID = AGM.UniqueGroupResponseId
			JOIN dbo.GroupDefinitions GD on GD.ID = UGR.GroupDefinitionID
		WHERE 
			AGM.MarkingMethodId IN (2, 3)
			AND AGM.IsEscalated = 0
			AND (AGM.MarkingDeviation > 0 AND 
			dbo.sm_checkGroupAndItemsMarkedInTolerance_fn(
				AGM.ID, 
				AGM.UniqueGroupResponseId,
				AGM.MarkingDeviation, 
				GD.CIMarkingTolerance, GD.IsAutomaticGroup) = 0)
		GROUP BY UniqueGroupResponseId 
	)
	,cteMeanMark as
	(
		SELECT
			 UniqueGroupResponseId 
			,CASE
				WHEN COUNT(*) <> 0 THEN SUM(AGM.AssignedMark) / COUNT(*)
				ELSE 0
			 END as MeanMark
		FROM dbo.AssignedGroupMarks AGM
		WHERE 
			AGM.MarkingMethodId IN (2, 3)
			AND AGM.IsEscalated = 0
		GROUP BY UniqueGroupResponseId 
	)
	,cteIsInConfirmedScript AS
	(
		SELECT 
			   CGR.UniqueGroupResponseID
			  ,1 AS IsInConfirmedScript
		FROM   dbo.CandidateGroupResponses CGR
			   INNER JOIN dbo.CandidateExamVersions CEV ON  CEV.ID = CGR.CandidateExamVersionID
		WHERE  CEV.ExamSessionState IN (6 ,7)
	)

SELECT DISTINCT
	 UGR.ID AS UniqueGroupResponseId
	,GD.ID AS GroupDefinitionID
	,UGR.ConfirmedMark
	,GD.TotalMark
    ,CAST(ISNULL(CONFIRMEDSCRIPTS.IsInConfirmedScript ,0) AS BIT) AS IsInConfirmedScript
	,CAST((
		CASE
			WHEN UGR.Feedback IS NULL OR UGR.Feedback = '' THEN 0
            ELSE 1
        END
	) AS BIT) AS 'Feedback'

	-- Statistics
	,ISNULL(PRESENTED.CNT, 0) AS NumPresented
	,CASE
			WHEN ISNULL(PRESENTED.CNT, 0) = 0 THEN 0
            ELSE CAST(ROUND(ISNULL(FAILED.CNT, 0)*100/CAST(PRESENTED.CNT AS FLOAT),0) AS INT) 
     END AS 'FailedPercent'     
	,CASE
			WHEN ISNULL(PRESENTED.CNT, 0) = 0 THEN 0
            ELSE CAST(ROUND(ISNULL(NOTEXACT.CNT, 0)*100/CAST(PRESENTED.CNT AS FLOAT),0) AS INT)
     END AS 'PassedNotExactPercent'	
	,ISNULL(MEANMARK.MeanMark, 0) AS MeanMark

	-- Creator
	,CRE.CreationDate
	,CRE.CreatorID
	,CRE.CreatorFullName

	-- Reviewer
	,REV.ReviewDate
	,REV.ReviewerID
	,REV.ReviewerFullName
FROM
	dbo.UniqueGroupResponses AS UGR 
	INNER JOIN dbo.GroupDefinitions AS GD ON UGR.GroupDefinitionID = GD.ID 
	LEFT JOIN cteCreator CRE ON CRE.UniqueGroupResponseId = UGR.ID
	LEFT JOIN cteReviewer REV ON REV.UniqueGroupResponseId = UGR.ID
	-- Statistics
	LEFT JOIN ctePresented PRESENTED ON  UGR.ID = PRESENTED.UniqueGroupResponseId 
	LEFT JOIN cteFailed FAILED ON  UGR.ID = FAILED.UniqueGroupResponseId 
	LEFT JOIN cteNotExact NOTEXACT ON  UGR.ID = NOTEXACT.UniqueGroupResponseId 
	LEFT JOIN cteMeanMark MEANMARK ON  UGR.ID = MEANMARK.UniqueGroupResponseId
    LEFT JOIN cteIsInConfirmedScript CONFIRMEDSCRIPTS ON  UGR.ID = CONFIRMEDSCRIPTS.UniqueGroupResponseId	 	
WHERE     
	UGR.CI = 1 AND UGR.CI_Review = 1




GO
/****** Object:  View [dbo].[EscalatedGroups_view]    Script Date: 16/05/2016 14:18:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Anton Burdyugov
-- Create date: 2013-06-26
-- Description:	Data about escalated(parked) groups.

-- Author:		Anton Burdyugov
-- Create date: 2013-07-03
-- Description:	Data from deleted user
-- =============================================
CREATE VIEW [dbo].[EscalatedGroups_view]
AS

SELECT GD.ExamID                     AS ExamID
      ,UGR.Id                        AS ID
      ,GD.Name                       AS NAME
      ,ISNULL(U.Surname+', '+U.Forename ,'Unknown') AS Examiner
      ,UGR.ParkedDate                AS ParkedDate
      ,UGR.ParkedAnnotation          AS ParkedAnnotation
      ,ISNULL(U.Surname ,'Unknown')  AS Surname
      ,ISNULL(U.Id ,-1)              AS UserID
      ,(
            SELECT RTRIM(UGRPRCS.ParkedReasonId) + ',' 
            FROM UniqueGroupResponseParkedReasonCrossRef UGRPRCS 
            WHERE UGRPRCS.UniqueGroupResponseId = UGR.Id 
            FOR XML PATH('')
        ) AS ParkedReasonIDs
      ,(
            SELECT RTRIM(PRL.Name) + ', ' 
            FROM UniqueGroupResponseParkedReasonCrossRef UGRPRCS 
				INNER JOIN ParkedReasonsLookup PRL ON UGRPRCS.ParkedReasonId = PRL.ID
            WHERE UGRPRCS.UniqueGroupResponseId = UGR.Id 
            ORDER BY PRL.Id
            FOR XML PATH('')
        ) AS ParkedReasons
       ,
		(
			SELECT DISTINCT RTRIM(CEV.CountryName) + ', '
			FROM CandidateExamVersions CEV
			INNER JOIN CandidateGroupResponses CGR
			ON CGR.CandidateExamVersionID = CEV.ID
			WHERE CGR.UniqueGroupResponseID = UGR.ID
			FOR XML PATH('')
		)												AS 'Countries'
       ,(
			SELECT DISTINCT RTRIM(CEV.CentreName) + ', '
			FROM CandidateExamVersions CEV
			INNER JOIN CandidateGroupResponses CGR
			ON CGR.CandidateExamVersionID = CEV.ID
			WHERE CGR.UniqueGroupResponseID = UGR.ID
			FOR XML PATH('')
		)												AS 'Centres'
        
FROM   UniqueGroupResponses UGR
       INNER JOIN GroupDefinitions GD
            ON  GD.ID = UGR.GroupDefinitionID
       LEFT JOIN Users U
            ON  U.ID = UGR.ParkedUserID
WHERE  UGR.IsEscalated = 1
       AND (U.InternalUser=0 OR U.Username = 'superuser' OR U.ID IS NULL)
       AND EXISTS (
               SELECT *
               FROM   GroupDefinitionStructureCrossRef GDSCR
                      INNER JOIN ExamVersionStructures EVS
                           ON  EVS.ID = GDSCR.ExamVersionStructureID
               WHERE  GDSCR.Status = 0
                      AND GDSCR.GroupDefinitionID = GD.ID
                      AND EVS.StatusID = 0
           )







GO
/****** Object:  View [dbo].[GroupMarkedResponses_view]    Script Date: 16/05/2016 14:18:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Anton Burdyugov
-- Create date: 20/05/2013
-- Description:	Data about group marked responses.

-- Modifier:	Vitaly Bevzik
-- Modify date: 24/06/2013
-- Description:	Add responses wich were disagree from Review Ci
-- =============================================
CREATE VIEW [dbo].[GroupMarkedResponses_view]
AS
SELECT AM.id                          AS AssignedMarkID
      ,UGR.ID                         AS UniqueGroupResponseID
      ,ISNULL(U.ID ,-1)               AS UserID
      ,ISNULL(U.Surname ,AM.Surname)  AS Surname      
      ,GD.TotalMark                   AS TotalMark
      ,AM.assignedMark                AS AssignedMark
      ,AM.timestamp                   AS TIMESTAMP
      ,GD.Name                        AS GroupName
      ,ISNULL(U.Surname+', '+U.Forename ,AM.fullName) AS FullName
      ,GD.ExamID                      AS ExamID
      ,UGR.GroupDefinitionID          AS GroupID
      ,dbo.sm_checkIsUniqueGroupResponseInConfirmedScript(UGR.ID) AS IsInConfirmedScript
      ,UGR.CI						  AS CI
FROM   [dbo].[AssignedGroupMarks] AM
       LEFT OUTER JOIN [dbo].[Users] U ON  AM.userId = U.ID
       INNER JOIN [dbo].[UniqueGroupResponses] UGR ON  AM.uniqueGroupResponseId = UGR.ID
       INNER JOIN [dbo].[GroupDefinitions] GD ON  UGR.GroupDefinitionID = GD.ID
WHERE  U.ID <> 0
       AND AM.ID IN 
       (
			SELECT TOP 1 AGM.ID
			FROM AssignedGroupMarks AGM
			WHERE AGM.UniqueGroupResponseId = UGR.ID
			AND AGM.IsConfirmedMark = 1
			AND AGM.MarkingMethodId IN (4, 7, 15, 17)
			ORDER BY Timestamp DESC
	   )
       AND EXISTS ( SELECT TOP 1 *
                         FROM   dbo.GroupDefinitionStructureCrossRef AS CR
                                INNER JOIN dbo.ExamVersionStructures AS EVS ON EVS.ID = CR.ExamVersionStructureID
                         WHERE  ( CR.Status = 0 )
                                AND ( CR.GroupDefinitionID = GD.ID )
                                AND ( EVS.StatusID = 0 ) )











GO
/****** Object:  View [dbo].[ManualMarkedGroups_view]    Script Date: 16/05/2016 14:18:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[ManualMarkedGroups_view]
WITH SCHEMABINDING
AS
SELECT GD.ID
FROM   dbo.Items I
       INNER JOIN dbo.GroupDefinitionItems GDI ON I.ID = GDI.ItemID
       INNER JOIN dbo.GroupDefinitions GD ON GDI.GroupID = GD.ID
WHERE  (
			I.MarkingType = 1
			AND 
			GD.IsAutomaticGroup = 1
	   )
	   OR GD.IsAutomaticGroup = 0
GROUP BY GD.ID




GO
/****** Object:  View [dbo].[MarkedCIGroups_view]    Script Date: 16/05/2016 14:18:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



/* =============================================
 Author:		Vitaly Bevzik
 Create date: 2013-05-30
 Description:	Data about group marked.
 =============================================*/
CREATE VIEW [dbo].[MarkedCIGroups_view]
AS

WITH cteIsTolerant AS
(
    SELECT AGM.ID
          ,AGM.UniqueGroupResponseId
          ,1 AS 'NotInTolerance'
    FROM   dbo.AssignedGroupMarks AGM
           INNER JOIN dbo.GroupDefinitions GD ON  GD.ID = AGM.GroupDefinitionID
    WHERE  AGM.MarkingDeviation>GD.CIMarkingTolerance
           OR (
                  AGM.MarkingDeviation<=GD.CIMarkingTolerance
                  AND GD.IsAutomaticGroup=0
                  AND EXISTS(
                          SELECT *
                          FROM   dbo.AssignedItemMarks AIM
                                 INNER JOIN dbo.UniqueResponses UR ON  UR.id = aim.UniqueResponseId
                                 INNER JOIN dbo.GroupDefinitionItems GDI ON  GDI.ItemID = UR.itemId
                                 INNER JOIN dbo.Items I ON  I.ID = GDI.itemId
                          WHERE  AIM.GroupMarkId = AGM.ID
                                 AND GDI.GroupID = GD.ID
                                 AND GDI.ViewOnly = 0
                                 AND AIM.MarkingDeviation>I.CIMarkingTolerance
                      )
              )
)
	
	
SELECT UGR.ID                           AS 'UniqueGroupResponseID'
      ,GD.ID                            AS 'GroupID'
      ,GD.Name                          AS 'GroupName'
      ,GD.TotalMark                     AS 'TotalMark'
      ,GD.CIMarkingTolerance            AS 'CIMarkingTolerance'
      ,GD.ExamID                        AS 'ExamID'
      ,ISNULL(U.Surname ,AGM.Surname)   AS 'Surname'
      ,ISNULL(U.Surname+', '+U.Forename ,AGM.FullName) AS 'Fullname'
      ,ISNULL(U.ID ,-1)                 AS 'UserId'
      ,AGM.ID                           AS 'AssignedMarkID'
      ,AGM.Timestamp                    AS 'Timestamp'
      ,AGM.AssignedMark                 AS 'AssignedMark'
      ,AGM.MarkingDeviation             AS 'MarkingDeviation'
      ,(
           CASE 
                WHEN UGR.ConfirmedMark IS NULL THEN (
                         SELECT TOP 1 AM.AssignedMark
                         FROM   dbo.AssignedGroupMarks AM
                         WHERE  AM.UniqueGroupResponseId=UGR.ID 
                                AND AM.MarkingMethodId=6
                         ORDER BY
                                TIMESTAMP DESC
                     )
                ELSE UGR.ConfirmedMark
           END
       )                                AS 'ConfirmedMark'
      ,AGM.MarkingMethodId              AS 'MarkingMethod'
      ,CAST(ISNULL(IT.NotInTolerance ,0) AS BIT) AS 'IsOutOfTolerance'
      ,AGM.IsEscalated                  AS 'IsEscalated'
FROM   dbo.AssignedGroupMarks           AGM
       LEFT OUTER JOIN dbo.Users U ON  AGM.UserId = U.ID
       INNER JOIN dbo.UniqueGroupResponses UGR ON  AGM.UniqueGroupResponseId = UGR.ID
       INNER JOIN dbo.GroupDefinitions  GD ON  GD.ID = UGR.GroupDefinitionID
       LEFT JOIN cteIsTolerant IT ON  IT.ID = AGM.ID
                AND IT.UniqueGroupResponseId = AGM.UniqueGroupResponseId
WHERE  (AGM.MarkingMethodId=2 OR AGM.MarkingMethodId=3)
       AND UGR.CI_Review = 1
       AND EXISTS (
               SELECT TOP 1 *
               FROM   dbo.GroupDefinitionStructureCrossRef CR
                      INNER JOIN dbo.ExamVersionStructures EVS ON  EVS.ID = CR.ExamVersionStructureID
               WHERE  CR.Status=0
                      AND CR.GroupDefinitionID=GD.ID
                      AND EVS.StatusID=0
           )





GO
/****** Object:  View [dbo].[MarkingReport_view]    Script Date: 16/05/2016 14:18:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO











CREATE VIEW  [dbo].[MarkingReport_view] 
	AS
SELECT I.ID                      AS ID
	  ,I.ExternalItemID          AS ExternalItemId
	  ,I.ExternalItemName        AS Name
	  ,Q.Reference               AS QualificationRef
	  ,Q.Name                    AS Qualification
	  ,Q.ID						 AS QualificationId
	  ,E.Reference               AS ExamRef
	  ,E.Name                    AS Exam
	  ,E.ID	                     AS ExamId
	  ,AGM.UserId				 AS UserId
	  ,U.Username                AS Username
	  ,U.Forename                AS Forename
	  ,U.Surname                 AS Surname
	  ,AGM.GroupDefinitionID     AS GroupDefinitionId
	  ,AGM.UniqueGroupResponseId AS UniqueGroupResponseId
	  ,CASE WHEN (AGM.MarkingMethodId = 11 AND AGM.EscalatedWithoutMark = 1) THEN 1 ELSE 0 END  AS EscalatedWithoutMark
	  ,(CASE WHEN AGM.MarkingMethodId IN (2 ,3) THEN 1 ELSE 0 END) AS ControlItem
	  ,(CASE WHEN AGM.MarkingMethodId NOT IN (2 ,3) THEN 1 ELSE 0 END) AS UniqueResponse
	  ,AGM.Timestamp             AS Timestamp
FROM   dbo.GroupDefinitionItems GDI
	   JOIN dbo.Items I ON  I.ID = GDI.ItemID
	   JOIN dbo.AssignedGroupMarks AGM ON  AGM.GroupDefinitionID = GDI.GroupID
	   JOIN dbo.Exams E ON  E.ID = I.ExamID
	   JOIN dbo.Qualifications Q ON  Q.ID = E.QualificationID
	   LEFT JOIN dbo.Users U ON  U.id = AGM.UserId
WHERE  I.ID>0 AND GDI.ViewOnly = 0
		AND ((AGM.MarkingMethodId = 4 AND AGM.IsActualMarking = 1)
			OR (AGM.MarkingMethodId IN (2, 3, 10, 11))
			OR (AGM.MarkingMethodId = 5 AND AGM.IsReportableCI = 1))
	   AND EXISTS (
			   SELECT TOP 1 *
			   FROM   dbo.GroupDefinitionStructureCrossRef CR
					  INNER JOIN dbo.ExamVersionStructures EVS
						   ON  CR.ExamVersionStructureID = EVS.ID
			   WHERE  CR.GroupDefinitionID = GDI.GroupID
					  AND CR.Status IN (0 ,2)
					  AND EVS.StatusID = 0
		   )








GO
/****** Object:  View [dbo].[MarkingReport_view_DJTEST]    Script Date: 16/05/2016 14:18:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW  [dbo].[MarkingReport_view_DJTEST] 
	AS
SELECT I.ID                      AS ID
	  ,I.ExternalItemID          AS ExternalItemId
	  ,I.ExternalItemName        AS Name
	  ,Q.Reference               AS QualificationRef
	  ,Q.Name                    AS Qualification
	  ,Q.ID						 AS QualificationId
	  ,E.Reference               AS ExamRef
	  ,E.Name                    AS Exam
	  ,E.ID	                     AS ExamId
	  ,AGM.UserId				 AS UserId
	  ,U.Username                AS Username
	  ,U.Forename                AS Forename
	  ,U.Surname                 AS Surname
	  ,AGM.GroupDefinitionID     AS GroupDefinitionId
	  ,AGM.UniqueGroupResponseId AS UniqueGroupResponseId
	  ,CASE WHEN (AGM.MarkingMethodId = 11 AND AGM.EscalatedWithoutMark = 1) THEN 1 ELSE 0 END  AS EscalatedWithoutMark
	  ,(CASE WHEN AGM.MarkingMethodId IN (2 ,3) THEN 1 ELSE 0 END) AS ControlItem
	  ,(CASE WHEN AGM.MarkingMethodId NOT IN (2 ,3) THEN 1 ELSE 0 END) AS UniqueResponse
	  ,AGM.Timestamp             AS Timestamp
	  ,AGM.MarkingMethodId
	  ,AGM.IsActualMarking
FROM   dbo.GroupDefinitionItems GDI
	   JOIN dbo.Items I ON  I.ID = GDI.ItemID
	   JOIN dbo.AssignedGroupMarks AGM ON  AGM.GroupDefinitionID = GDI.GroupID
	   JOIN dbo.Exams E ON  E.ID = I.ExamID
	   JOIN dbo.Qualifications Q ON  Q.ID = E.QualificationID
	   LEFT JOIN dbo.Users U ON  U.id = AGM.UserId
WHERE  I.ID>0 AND GDI.ViewOnly = 0
		AND ((AGM.MarkingMethodId = 4 AND AGM.IsActualMarking = 1)
			OR (AGM.MarkingMethodId IN (2, 3, 10, 11))
			OR (AGM.MarkingMethodId = 5 AND AGM.IsReportableCI = 1))
	   AND EXISTS (
			   SELECT TOP 1 *
			   FROM   dbo.GroupDefinitionStructureCrossRef CR
					  INNER JOIN dbo.ExamVersionStructures EVS
						   ON  CR.ExamVersionStructureID = EVS.ID
			   WHERE  CR.GroupDefinitionID = GDI.GroupID
					  AND CR.Status IN (0 ,2)
					  AND EVS.StatusID = 0
		   )










GO
/****** Object:  View [dbo].[MarkingReport_view_OLD]    Script Date: 16/05/2016 14:18:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[MarkingReport_view_OLD]
AS
SELECT I.ID                      AS ID
	  ,I.ExternalItemID          AS ExternalItemId
	  ,I.ExternalItemName        AS Name
	  ,Q.Reference               AS QualificationRef
	  ,Q.Name                    AS Qualification
	  ,Q.ID						 AS QualificationId
	  ,E.Reference               AS ExamRef
	  ,E.Name                    AS Exam
	  ,E.ID	                     AS ExamId
	  ,AGM.UserId				 AS UserId
	  ,U.Username                AS Username
	  ,U.Forename                AS Forename
	  ,U.Surname                 AS Surname
	  ,AGM.GroupDefinitionID     AS GroupDefinitionId
	  ,AGM.UniqueGroupResponseId AS UniqueGroupResponseId

	  ,(CASE WHEN AGM.MarkingMethodId IN (2 ,3) THEN 1 ELSE 0 END) AS ControlItem
	  ,(CASE WHEN AGM.MarkingMethodId NOT IN (2 ,3) THEN 1 ELSE 0 END) AS UniqueResponse
	  ,AGM.Timestamp             AS Timestamp
FROM   dbo.GroupDefinitionItems GDI
	   JOIN dbo.Items I ON  I.ID = GDI.ItemID
	   JOIN dbo.AssignedGroupMarks AGM ON  AGM.GroupDefinitionID = GDI.GroupID
	   JOIN dbo.Exams E ON  E.ID = I.ExamID
	   JOIN dbo.Qualifications Q ON  Q.ID = E.QualificationID
	   LEFT JOIN dbo.Users U ON  U.id = AGM.UserId
WHERE  I.ID>0 AND GDI.ViewOnly = 0
		AND ((AGM.MarkingMethodId = 4)
			OR (AGM.MarkingMethodId IN (2, 3, 10))
			OR (AGM.MarkingMethodId = 11 AND AGM.AssignedMark > 0)
			OR (AGM.MarkingMethodId = 5 AND AGM.IsReportableCI = 1))
	   AND EXISTS (
			   SELECT TOP 1*
			   FROM   dbo.GroupDefinitionStructureCrossRef CR
					  INNER JOIN dbo.ExamVersionStructures EVS
						   ON  CR.ExamVersionStructureID = EVS.ID
			   WHERE  CR.GroupDefinitionID = GDI.GroupID
					  AND CR.Status IN (0 ,2)
					  AND EVS.StatusID = 0
		   )

GO
/****** Object:  View [dbo].[NotMarkedResponses_view]    Script Date: 16/05/2016 14:18:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[NotMarkedResponses_view]
WITH SCHEMABINDING
AS
SELECT ID as UniqueGroupResponseId
	, InsertionDate
	, TokenId
	, GroupDefinitionID
	, VIP
	FROM dbo.UniqueGroupResponses AS UGR
	WHERE UGR.ParkedUserID IS NULL
			AND UGR.CI = 0
			AND UGR.confirmedMark IS NULL





GO
/****** Object:  View [dbo].[QualificationExams_view]    Script Date: 16/05/2016 14:18:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[QualificationExams_view]
WITH SCHEMABINDING
AS
	SELECT Q.Name AS Qualification
		, Q.ID AS QualificationId
		, E.Name AS Exam
		, E.ID AS ExamId
		FROM dbo.Exams E
			INNER JOIN dbo.Qualifications Q ON Q.ID = E.QualificationID



GO
/****** Object:  View [dbo].[QuotaGroupCandidateResponses_view]    Script Date: 16/05/2016 14:18:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[QuotaGroupCandidateResponses_view]
WITH SCHEMABINDING
AS
	SELECT DISTINCT UGR.TokenId
			, GD.ID AS GroupId
			, CGR1.UniqueGroupResponseID
			, CGR1.CandidateExamVersionID
	FROM dbo.QuotaManagement QM
			JOIN dbo.GroupDefinitions GD ON  GD.ID = QM.GroupId
			JOIN dbo.UniqueGroupResponses UGR ON UGR.GroupDefinitionID = GD.ID
			JOIN dbo.CandidateGroupResponses CGR1 ON CGR1.UniqueGroupResponseID = UGR.ID
	WHERE UGR.IsEscalated = 0
		AND UGR.CI = 0
		AND UGR.confirmedMark IS NULL


GO
/****** Object:  View [dbo].[ScriptReview_view]    Script Date: 16/05/2016 14:18:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[ScriptReview_view]
AS
	SELECT CEV.ID                 AS 'ID'
          ,q.Name                 AS 'Qualification'
          ,q.ID                   AS 'QualificationID'
          ,e.Name                 AS 'Exam'
          ,e.ID                   AS 'ExamID'
          ,ev.Name                AS 'ExamVersion'
          ,ev.ID                  AS 'ExamVersionID'
          ,CEV.Keycode            AS 'Keycode'
          ,(
               CASE 
                    WHEN CEV.ExamSessionState IN (3 ,4 ,6 ,7) THEN CEV.Mark
                    ELSE - 1
               END
           )                      AS 'Mark'
          ,CEV.TotalMark          AS 'TotalMarks'
          ,ISNULL(
               CASE 
                    WHEN CEV.ExamSessionState IN (3 ,4 ,6 ,7) THEN CEV.Mark
                        *100/NULLIF(CEV.TotalMark ,0)
                    ELSE -1
               END
              ,0
           )                      AS 'Percentage'
          ,CEV.ExamSessionState   AS 'ExamSessionState'
          ,ISNULL(CEV.PercentageMarkingComplete ,0) AS 'PercentageMarked'
          ,CEV.CompletionDate	  AS 'Submitted'
          ,CEV.DateSubmitted      AS 'Imported'
          ,CEV.IsFlagged		  AS 'IsFlagged'
          ,CEV.VIP                AS 'VIP'
          ,CEV.CandidateSurname   AS 'CandidateSurname'
          ,CEV.CandidateForename  AS 'CandidateForename'
          ,CEV.CandidateForename + ' ' + CEV.CandidateSurname  AS 'CandidateFullName'          
          ,CEV.CandidateRef       AS 'CandidateRef'
          ,CEV.CentreName         AS 'CentreName'
          ,CEV.CentreCode         AS 'CentreCode'
          ,CEV.CountryName		  AS 'Country'
          ,CEV.IsModerated		  AS 'IsModerated'
      FROM   dbo.CandidateExamVersions CEV
           INNER JOIN dbo.ExamVersions ev
                ON  ev.ID = CEV.ExamVersionID
           INNER JOIN dbo.Exams e
                ON  e.ID = ev.ExamID
           INNER JOIN dbo.Qualifications q
                ON  q.ID = e.QualificationID




GO
/****** Object:  View [dbo].[ScriptToUpdate_view]    Script Date: 16/05/2016 14:18:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[ScriptToUpdate_view]
WITH SCHEMABINDING
AS
	SELECT CEV.ID, CEV.ExamSessionState, CEV.LastManagedDate
    FROM   dbo.CandidateExamVersions CEV
    WHERE CEV.ExamSessionState IN (1,2,3,4,5) 


GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[20] 2[31] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "AGM"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 246
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "U"
            Begin Extent = 
               Top = 126
               Left = 38
               Bottom = 245
               Right = 243
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "UGR"
            Begin Extent = 
               Top = 6
               Left = 284
               Bottom = 125
               Right = 459
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "GD"
            Begin Extent = 
               Top = 246
               Left = 38
               Bottom = 365
               Right = 387
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 17
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         S' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'MarkedCIGroups_view'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'ortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'MarkedCIGroups_view'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=NULL , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'MarkedCIGroups_view'
GO
