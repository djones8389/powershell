if exists (
select 1
from sys.indexes 
where name = 'IX_ExamSessionID_ID_ESCandidateLog'
	and object_name(object_ID)= 'ExamSessionCandidateInteractionLogsTable'
) PRINT 'ExamSessionCandidateInteractionLogsTable:  Index exists'
else PRINT 'ExamSessionCandidateInteractionLogsTable:  Index doesn''t exist'

SELECT COUNT(*)
FROM [dbo].[ExamSessionCandidateInteractionLogsTable];

if exists (
select 1
from sys.indexes 
where name = 'IX_ExamSessionID_ID_WSCandidateLog'
	and object_name(object_ID)= 'WAREHOUSE_ExamSessionCandidateInteractionLogsTable'
) PRINT 'WAREHOUSE_ExamSessionCandidateInteractionLogsTable:  Index exists'
else PRINT 'WAREHOUSE_ExamSessionCandidateInteractionLogsTable:  Index doesn''t exist'