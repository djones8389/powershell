SELECT  S.Title [Subject]
	, i.id [ItemID]
	, i.Name [ItemName]
	, I.Version [ContentAuthor Version]
	, IT.Version [TestCreation Version]
FROM [_ContentAuthor].[dbo].[Subjects] S
INNER JOIN  [_ContentAuthor].[dbo].[Items]  I
ON I.SubjectId = S.Id
INNER JOIN (
	SELECT ProjectID
		, ItemRef
		, MAX(Version) [Version]
	FROM [_ItemBank].[dbo].[ItemTable] 
	group by ProjectID
		, ItemRef
	) IT
on IT.ProjectID = S.ProjectId
	and IT.ItemRef = I.id
where I.[Status] = 3					
	and I.Version <> IT.Version;

