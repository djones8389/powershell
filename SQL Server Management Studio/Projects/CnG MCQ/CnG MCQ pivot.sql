set transaction isolation level read uncommitted

Use PRV_Evolve_CPProjectAdmin

IF OBJECT_ID('tempdb..##Questions') IS NOT NULL DROP TABLE ##Questions;
IF OBJECT_ID('tempdb..##Answers') IS NOT NULL DROP TABLE ##Answers;

select Q.ID
	, cast(Q.Question.query('ItemValue/TEXTFORMAT/P//FONT/.//text()') as nvarchar(MAX)) Question 
INTO ##Questions
from (
		select ITT.ID	
			, CAST(ItemValue AS xml)  Question
		from [ItemTextBoxTable] ITT WITH (READUNCOMMITTED)
) Q

CREATE CLUSTERED INDEX [IX] ON ##QUESTIONS (id);

SELECT  ROW_NUMBER() OVER(PARTITION BY ParentID ORDER BY ID, [asC]) AS [N],
	 ParentID,
	 ID, 
	 [asC] AS [AnswerAlias], 
	CAST(ItemXml.query('.//text()') AS nvarchar(max)) AS [AnswerText]
INTO ##Answers
FROM (
 SELECT ID,
	  ParentID,
	  [asC],
	  CAST(ItemValue AS xml) AS [ItemXml]
 FROM dbo.ItemMultipleChoiceTable WITH (READUNCOMMITTED)
) AS [X];

--03:33 up to here

--(1461168 row(s) affected)
--(1143450 row(s) affected)

CREATE NONCLUSTERED INDEX [IX] ON [dbo].[##Answers] ([ParentID]) INCLUDE ([N],[AnswerAlias],[AnswerText]);

SELECT p.ParentID,   [1] AS [Answer1], [2] AS [Answer2], [3] AS [Answer3], [4] AS [Answer4], [5] AS [Answer5]
FROM (
 SELECT ParentID, N, AnswerAlias + N': ' + AnswerText AS [AnswerText]
 FROM ##Answers
) AS [S]
PIVOT (
 MAX(AnswerText)
 FOR N IN ([1], [2], [3], [4], [5])
) AS [P]

INNER JOIN (
	SELECT  
		(
			SELECT Question + '  '
			from ##QUESTIONS Q2
			where substring(Q2.id,0,charindex('C',Q2.id)) = substring(b.parentid,0,charindex('C',b.parentid))
				and (cast(substring(b.Parentid, charindex('C',b.Parentid)+1, 10) as int) - cast(substring(q2.id, charindex('C',Q2.id)+1, (LEN(Q2.id) - CHARINDEX('I', q2.id))) as int)) in (1,2)
				--and id like '344P1045%'
			for xml path ('')
		) Question
		, b.parentid
	FROM (
			SELECT 
				  a.*
				, ROW_NUMBER() over (PARTITION BY ParentID ORDER BY ParentID) R
			FROM ##QUESTIONS Q
			INNER JOIN ##ANSWERS A
			on substring(Q.id,0,charindex('C',Q.id)) = substring(A.parentid,0,charindex('C',A.parentid))
				and substring(q.id, charindex('C',Q.id)+1, (LEN(Q.id) - CHARINDEX('I', q.id))) < substring(A.Parentid, charindex('C',A.Parentid)+1, 10)
			where (cast(substring(A.Parentid, charindex('C',A.Parentid)+1, 10) as int) - cast(substring(q.id, charindex('C',Q.id)+1, (LEN(Q.id) - CHARINDEX('I', q.id))) as int)) in (1,2)
				--and Q.id like '344P1045%'
	) B
	where R = 1
) Q
on q.parentid = p.ParentID






--where p.ParentID in ('344P1045S1C3', '344P1045S1C7')



select a.id
	, substring(a.ID, 0, charindex('C',a.id))
	, substring(a.id, charindex('C',a.id)+1, (LEN(a.id) - CHARINDEX('I', a.id)))
	, a.Question
from  ##QUESTIONS A			
where id like '344P1045S2%'

----INNER JOIN (

	select b.id
		, substring(b.ID, 0, charindex('C',b.id))
		, substring(b.id, charindex('C',b.id)+1, (LEN(b.id) - CHARINDEX('I', b.id)))
		, b.Question

		, substring((
			SELECT ' ' + [Question]  
			FROM ##Questions D
			--INNER JOIn ##ANSWERS A
			--on substring(A.id, 0, CHARINDEX('S', A.ID)+1) = substring(D.ID, 0, charindex('C',D.id)) --Scene
			WHERE substring(D.ID, 0, charindex('C',D.id)) = substring(b.ID, 0, charindex('C',b.id)) 
				and d.id like '344P1045S2%'

			FOR XML PATH('')
		), 2,1000)
	FROM ##Questions b
	where b.id like '344P1045S2%'

Select * from ##QUESTIONS where id like '344P1045S2%'
Select *
	,substring(id, charindex('C',id)+1, (LEN(id) - CHARINDEX('I', id)))
	,substring(id, 0, CHARINDEX('S', id)+1)
 from ##ANSWERS where id like '344P1045S2%'





--	SELECT  SUBSTRING(results.id,0,charindex('I',results.ID)) ID
--	, substring((
--			SELECT ', ' + [question]  
--			FROM ##QUESTIONS 
--			WHERE (SUBSTRING(id,0,charindex('I',ID)) = SUBSTRING(results.id,0,charindex('I',results.ID))) 
--				AND id like '344P1045%'
--			FOR XML PATH('')
--		), 2,1000)

--	FROM ##QUESTIONS Results
--	where id like '344P1045%'
--	GROUP BY (SUBSTRING(results.id,0,charindex('I',results.ID)))




--inner join (
--	SELECT 
--	  ID,
--	 substring((
--			SELECT ', ' + [question]  
--			FROM ##QUESTIONS 
--			WHERE (SUBSTRING(id,0,charindex('I',ID)) = SUBSTRING(results.id,0,charindex('I',results.ID))) 
--			FOR XML PATH('')
--		), 2,1000)

--	FROM ##QUESTIONS Results
--	GROUP BY results.ID
--) a


--select top 10 *
--,	SUBSTRING(id,0,charindex('I',ID))
--from  ##QUESTIONS 
--order by id


	--SELECT 
	-- (SUBSTRING(results.id,0,charindex('I',results.ID))),
	-- substring((
	--		SELECT ', ' + [question]  
	--		FROM ##QUESTIONS 
	--		WHERE (SUBSTRING(id,0,charindex('I',ID)) = SUBSTRING(results.id,0,charindex('I',results.ID))) 
	--			AND id like '602P2300%'
	--		FOR XML PATH('')
	--	), 2,1000)

	--FROM ##QUESTIONS Results
	--where id like '602P2300%'
	--GROUP BY (SUBSTRING(results.id,0,charindex('I',results.ID)))

	--


	--select *
	--from ItemTextBoxTable
	--where id like '602P2300%'
	--select *
	--from ItemMultipleChoiceTable
	--where id like '602P2300%'

--SELECT --TOP(10000)
-- ROW_NUMBER() 
--  OVER(PARTITION BY ParentID
--    ORDER BY ID, [asC]) AS [N],
-- ParentID,
-- ID, 
-- [asC] AS [AnswerAlias], 
-- CAST(ItemXml.query('.//text()') AS nvarchar(max)) AS [AnswerText]
--INTO ##Answers
--FROM (
-- SELECT ID,
--  ParentID,
--  [asC],
--  CAST(ItemValue AS xml) AS [ItemXml]
-- FROM dbo.ItemMultipleChoiceTable WITH (READUNCOMMITTED)
-- where parentid like '344P%'
--) AS [X];

--SELECT ParentID, [1] AS [Answer1], [2] AS [Answer2], [3] AS [Answer3], [4] AS [Answer4], [5] AS [Answer5]
--FROM (
-- SELECT ParentID, N, AnswerAlias + N': ' + AnswerText AS [AnswerText]
-- FROM ##Answers
--) AS [S]
--PIVOT (
-- MAX(AnswerText)
-- FOR N IN ([1], [2], [3], [4], [5])
--) AS [P]




--DROP TABLE ##Answers;


--FROM ItemMultipleChoiceTable MCQ
--INNER JOIN  (

--) ITT
--on substring(ITT.ID, 0, charindex('S',ITT.ID)) = substring(MCQ.ID, 0, charindex('S',MCQ.ID));




--SELECT ParentID, [1] AS [Answer1], [2] AS [Answer2], [3] AS [Answer3], [4] AS [Answer4], [5] AS [Answer5]
--FROM (
-- SELECT ParentID, N, AnswerAlias + N': ' + AnswerText AS [AnswerText]
-- FROM ##Answers
--) AS [S]
--PIVOT (
-- MAX(AnswerText)
-- FOR N IN ([1], [2], [3], [4], [5])
--) AS [P]
