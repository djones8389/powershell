--select b.ItemID
--	, b.ID
--	, cast(b.Question.query('ItemValue/TEXTFORMAT/P//FONT/text()') as nvarchar(MAX)) + cast(b.Question.query('ItemValue/TEXTFORMAT/P//FONT/B/text()') as nvarchar(MAX)) Question
--from (
--	select substring(ID, 0, charindex('S',ID)) [ItemID]	
--		,ITT.ID
--		, CAST(ItemValue AS xml)  Question
--	from [ItemTextBoxTable] ITT
--	where substring(ID, 0, charindex('S',ID))  = '344P1162'
--) b





--	select b.ID
--		, cast(b.Question.query('ItemValue/TEXTFORMAT/P//FONT/.//text()') as nvarchar(MAX)) 
--	from (
--		select ITT.ID	
--			, CAST(ItemValue AS xml)  Question
--		from [ItemTextBoxTable] ITT
--			where ID like '344P1037%'
--		) b

--SELECT *
--from [ItemTextBoxTable] ITT
--where id like '521P989%'

--SELECT *
--FROM ItemMultipleChoiceTable
--where id like '521P989%'









/*




USE PRV_Evolve_CPProjectAdmin;
GO


SELECT TOP(100)
 ROW_NUMBER() 
  OVER(PARTITION BY ParentID
    ORDER BY ID, [asC]) AS [N],
 ParentID,
 [asC] AS [AnswerAlias], 
 CAST(ItemXml.query('.//text()') AS nvarchar(max)) AS [AnswerText]
INTO #Answers
FROM (
 SELECT ID,
  ParentID,
  [asC],
  CAST(ItemValue AS xml) AS [ItemXml]
 FROM dbo.ItemMultipleChoiceTable WITH (READUNCOMMITTED)
  --where  ID  like '344P1151%'
) AS [X];



INSERT #Questions(ItemID,Question)
select distinct 
	itt.itemID
	, itt.Question
FROM ItemMultipleChoiceTable MCQ
INNER JOIN  (
	select a.itemID
		, a.Question
	from (
		select b.ItemID
			, cast(b.Question.query('ItemValue/TEXTFORMAT/P//FONT/text()') as nvarchar(MAX)) + cast(b.Question.query('ItemValue/TEXTFORMAT/P//FONT/B/text()') as nvarchar(MAX)) Question
		from (
			select substring(ID, 0, charindex('S',ID)) [ItemID]	
				, CAST(ItemValue AS xml)  Question
				--, ROW_NUMBER() OVER(PARTITION BY substring(ITT.ID, 0, charindex('S',ITT.ID)) ORDER BY ITT.Y ASC) R
			from [ItemTextBoxTable] ITT
			-- where ID like '344P%'
			where substring(ID, 0, charindex('S',ID))  = '344P1162'
		) b
		--where r = 1
	) a 
) ITT
on ITT.itemID = substring(MCQ.ID, 0, charindex('S',MCQ.ID));





SELECT p.ParentID, q.Question, [1] AS [Answer1], [2] AS [Answer2], [3] AS [Answer3], [4] AS [Answer4], [5] AS [Answer5]
FROM (
 SELECT ParentID, N, AnswerAlias + N': ' + AnswerText AS [AnswerText]
 FROM #Answers
) AS [S]
PIVOT (
 MAX(AnswerText)
 FOR N IN ([1], [2], [3], [4], [5])
) AS [P]
INNER JOIN (
	SELECT
		(
			SELECT Questions + '  '
			from #QUESTIONS Q2
			where substring(Q2.id,0,charindex('C',Q2.id)) = substring(b.parentid,0,charindex('C',b.parentid))
				and (cast(substring(b.Parentid, charindex('C',b.Parentid)+1, 10) as int) - cast(substring(q2.id, charindex('C',Q2.id)+1, (LEN(Q2.id) - CHARINDEX('I', q2.id))) as int)) in (1,2)
			for xml path ('')
		) Question
		, b.parentid
	FROM (
			SELECT 
				  a.*
				, ROW_NUMBER() over (PARTITION BY ParentID ORDER BY ParentID) R
			FROM #QUESTIONS Q
			INNER JOIN #ANSWERS2 A
			on substring(Q.id,0,charindex('C',Q.id)) = substring(A.parentid,0,charindex('C',A.parentid))
				and substring(q.id, charindex('C',Q.id)+1, (LEN(Q.id) - CHARINDEX('I', q.id))) < substring(A.Parentid, charindex('C',A.Parentid)+1, 10)
			where (cast(substring(A.Parentid, charindex('C',A.Parentid)+1, 10) as int) - cast(substring(q.id, charindex('C',Q.id)+1, (LEN(Q.id) - CHARINDEX('I', q.id))) as int)) in (1,2)
	) B
	where R = 1
) Q
on q.parentid = p.ParentID


--where ParentID like '344P1151%'

/*
/**/
select a.ID
	, CAST(ItemXml.query('.//text()') AS nvarchar(max)) QUESTIONS
INTO ##QUESTIONS
FROM (
select  
	 ID
	, cast(itemValue as XML) ItemXml
from ItemTextBoxTable
where id like '344P1151%'
) a

DROP TABLE ##Answers;

*/

*/