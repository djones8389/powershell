SELECT *
FROM ##ANSWERS

--SELECT ParentID, [1] AS [Answer1], [2] AS [Answer2], [3] AS [Answer3], [4] AS [Answer4], [5] AS [Answer5]
--INTO ##ANSWERS2
--FROM (
-- SELECT ParentID, N, AnswerAlias + N': ' + AnswerText AS [AnswerText]
-- FROM ##Answers
--) AS [S]
--PIVOT (
-- MAX(AnswerText)
-- FOR N IN ([1], [2], [3], [4], [5])
--) AS [P]
--where ParentID like '344P1151%'


SELECT
	(
		SELECT Questions + '  '
		from ##QUESTIONS Q2
		where substring(Q2.id,0,charindex('C',Q2.id)) = substring(b.parentid,0,charindex('C',b.parentid))
			and (cast(substring(b.Parentid, charindex('C',b.Parentid)+1, 10) as int) - cast(substring(q2.id, charindex('C',Q2.id)+1, (LEN(Q2.id) - CHARINDEX('I', q2.id))) as int)) in (1,2)
		for xml path ('')
	) Question
	, b.parentid
FROM (
		SELECT 
			  a.*
			, ROW_NUMBER() over (PARTITION BY ParentID ORDER BY ParentID) R
		FROM ##QUESTIONS Q
		INNER JOIN ##ANSWERS2 A
		on substring(Q.id,0,charindex('C',Q.id)) = substring(A.parentid,0,charindex('C',A.parentid))
			and substring(q.id, charindex('C',Q.id)+1, (LEN(Q.id) - CHARINDEX('I', q.id))) < substring(A.Parentid, charindex('C',A.Parentid)+1, 10)
		where (cast(substring(A.Parentid, charindex('C',A.Parentid)+1, 10) as int) - cast(substring(q.id, charindex('C',Q.id)+1, (LEN(Q.id) - CHARINDEX('I', q.id))) as int)) in (1,2)
) B
where R = 1


/*

select *
, substring(Q.id,0,charindex('C',Q.id)+2) 
, substring(q.id, charindex('C',Q.id)+1, (LEN(Q.id) - CHARINDEX('I', q.id)))
FROM ##QUESTIONS Q




SELECT *
, substring(A.Parentid,0,charindex('C',A.Parentid)+2) 
, substring(A.Parentid, charindex('C',A.Parentid)+1, 10)
FROM ##ANSWERS2 A

*/