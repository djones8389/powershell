set transaction isolation level read uncommitted

Use PRV_Evolve_CPProjectAdmin

IF OBJECT_ID('tempdb..##Questions') IS NOT NULL DROP TABLE ##Questions;
IF OBJECT_ID('tempdb..##Answers') IS NOT NULL DROP TABLE ##Answers;

select Q.ID
	,substring(Q.ID, 0, charindex('S', Q.ID)+2) UpToScene
	,SUBSTRING(Q.ID,charindex('C',Q.ID)+1, (Len(Q.ID)-charindex('I',Q.ID)))  C
	, cast(Q.Question.query('ItemValue/TEXTFORMAT/P//FONT/.//text()') as nvarchar(MAX)) Question 
INTO ##Questions
from (
		select ITT.ID	
			, CAST(ItemValue AS xml)  Question
		from [ItemTextBoxTable] ITT WITH (READUNCOMMITTED)
) Q

CREATE CLUSTERED INDEX [IX] ON ##QUESTIONS (id);
CREATE NONCLUSTERED INDEX [IX_NC] on ##QUESTIONS (UpToScene,C);

SELECT  ROW_NUMBER() OVER(PARTITION BY ParentID ORDER BY ID, [asC]) AS [N],
	 ParentID,
	 --ID, 
	 substring(ID, 0, charindex('S',ID)+2) UpToScene,
	 SUBSTRING(ID,charindex('C',ID)+1, (Len(ID)-charindex('I',ID))) C,
	 [asC] AS [AnswerAlias], 
	CAST(ItemXml.query('.//text()') AS nvarchar(max)) AS [AnswerText]
INTO ##Answers
FROM (
 SELECT ID,
	  ParentID,
	  [asC],
	  CAST(ItemValue AS xml) AS [ItemXml]
 FROM dbo.ItemMultipleChoiceTable WITH (READUNCOMMITTED)
) AS [X];

CREATE NONCLUSTERED INDEX [IX] ON [dbo].[##Answers] ([ParentID]) INCLUDE ([N],[AnswerAlias],[AnswerText]);
CREATE NONCLUSTERED INDEX [IX_NC] on [##Answers] (UpToScene,C);

--drop table #answers2

SELECT p.ParentID, substring(ParentID, 0, charindex('S',ParentID)+2) UpToScene
,SUBSTRING(ParentID,charindex('C',ParentID)+1, (Len(ParentID)-charindex('I',ParentID))) C
,[1] AS [Answer1], [2] AS [Answer2], [3] AS [Answer3], [4] AS [Answer4], [5] AS [Answer5]
INTO #answers2
FROM (
 SELECT ParentID, N, AnswerAlias + N': ' + AnswerText AS [AnswerText]
 FROM ##Answers
) AS [S]
PIVOT (
 MAX(AnswerText)
 FOR N IN ([1], [2], [3], [4], [5])
) AS [P]

-- up to here 03 49

select top 10 ParentID
,	substring(ParentID, 0, charindex('S',ParentID)+2)
from #answers2 

--select *
--from ##Answers a
--where a.ParentID like '344P1045%'

--select *
--from ##Questions a
--where a.ID like '344P1045%'

	--,substring(ID, 0, charindex('S',ID)+2) UpToScene
	--,SUBSTRING(ID,charindex('S',ID)+1, (Len(ID)-charindex('I',ID)))  S
	--,SUBSTRING(ID,charindex('C',ID)+1, (Len(ID)-charindex('I',ID)))  C
	
	
	--30 seconds
	SELECT  
		b.parentID
		,(
			SELECT Question + '  '
			from ##QUESTIONS Q2
			where q2.UpToScene =  b.uptoscene-- substring(b.ParentID, 0, charindex('S',b.ParentID)+2)
				and (cast(b.c as int) - cast(q2.c as int)) in (1,2)
				--and Q2.id like '344P1045%'
			for xml path ('')
		) Question
		,b.Answer1
		,b.Answer2
		,b.Answer3
		,b.Answer4
		,b.Answer5
	FROM (
			select  *
			from #answers2 
			--where parentid like '344P1045%'
	) B


