if OBJECT_ID('tempdb..#WHIDs') is not null DROP TABLE #WHIDs;

CREATE TABLE #WHIDs (
	ID INT
);

INSERT #WHIDs
select esdt.ID --sum(cast(DATALENGTH(esdt.Document) as bigint))/1024/1024 [MB]
from Warehouse_ScheduledExamsTable scet
inner join Warehouse_ExamSessionTable est
on est.WAREHOUSEScheduledExamID = scet.id
inner join Warehouse_ExamSessionDocumentTable esdt
on esdt.warehouseExamSessionID = est.ID
where ExportToSecureMarker = 1
	--and DATEADD(MONTH, -6, getdate()) < scet.ScheduledStartDateTime 
	and (qualificationName != 'International Comparisons Research')


--select *
--from #WHIDs 

DECLARE @Min tinyint = 1
DECLARE @Max int = (select count(ID) from #WHIDs)
DECLARE @Increment smallint = 250

WHILE (@Min < @Max)

BEGIN

	DELETE TOP (@Increment) C
	FROM Warehouse_ExamSessionDocumentTable C
	INNER JOIN #WHIDs D
	on D.ID = c.ID

	SELECT @Min = @Min + @Increment

	PRINT @Increment

END


SELECT C.*
INTO Warehouse_ExamSessionDocumentTable_Test
FROM Warehouse_ExamSessionDocumentTable C
INNER JOIN #WHIDs D
on D.ID = c.ID