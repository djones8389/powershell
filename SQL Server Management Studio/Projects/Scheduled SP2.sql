IF OBJECT_ID('tempdb..#TEMP_CentreQualificationLevels_TABLE') IS NOT NULL DROP TABLE #TEMP_CentreQualificationLevels_TABLE;
	
DECLARE 
	@RowEndqualificationName NVARCHAR(MAX),
	@pageSize						INT,
	@pageIndex						INT,
	@filterType						VARCHAR(3),
	--FILTER MAIN PARAMETERS
	@centres						NVARCHAR(MAX),
	@surname						NVARCHAR(MAX),
	@examName						NVARCHAR(MAX),
	@onlyCreatedByMe				INT,
    @startDateStartRange			NVARCHAR(MAX),
	@startDateEndRange				NVARCHAR(MAX),
	@endDateStartRange				NVARCHAR(MAX),
	@endDateEndRange				NVARCHAR(MAX),
	@startTimeStartRange			NVARCHAR(MAX),
	@startTimeEndRange				NVARCHAR(MAX),
	@endTimeStartRange				NVARCHAR(MAX),
	@endTimeEndRange				NVARCHAR(MAX),
	@dateCreatedStartRange			NVARCHAR(MAX),
	@dateCreatedEndRange			NVARCHAR(MAX),
	@qualification					NVARCHAR(MAX),	
	@sorting						VARCHAR(MAX),
	@showOnlyInvigilatedExams		BIT,
	@invigilated					CHAR(1),
	@groupState						NVARCHAR(MAX),
	--OTHER POSSIBLE
    @candidateSurname				NVARCHAR(MAX),
    @candidateRef					NVARCHAR(MAX),
    @candidateUln					NVARCHAR(MAX),
    @dateOfBirthStartRange			NVARCHAR(MAX),
    @dateOfBirthEndRange			NVARCHAR(MAX),
	--OTHER PARAMETERS
	@includeQualityReviewExams		BIT,
	@centreQualificationLevelsXml	NVARCHAR(MAX),
	@assignedQualificationIdList	NVARCHAR(MAX),	
	@useExamSessions				BIT
	--WITH_ENCRYPTION_REPLACE_ME_FOR_LOCAL--
SET @filterType = 'AND'
SET @pageIndex = 0
SET @pageSize = 50
SET @centres = N''
SET @surname = N'%'
SET @examName = N'%'
SET @onlyCreatedByMe = - 1
SET @startDateStartRange = N''
SET @startDateEndRange = N''
SET @endDateStartRange = N''
SET @endDateEndRange = N''
SET @startTimeStartRange = N''
SET @startTimeEndRange = N''
SET @endTimeStartRange = N''
SET @endTimeEndRange = N''
SET @dateCreatedStartRange = N''
SET @dateCreatedEndRange = N''
SET @qualification = N''
SET @sorting = 'qualification ASC, examName ASC, dateCreated DESC, surname ASC, candidateRef ASC'
SET @showOnlyInvigilatedExams = N'0'
SET @invigilated = N''
SET @groupState = N''
SET @candidateSurname = N'%'
SET @candidateRef = N'%'
SET @candidateUln = N'%'
SET @dateOfBirthStartRange = N''
SET @dateOfBirthEndRange = N''
SET @includeQualityReviewExams = 1
SET @centreQualificationLevelsXml = 
	N'<list><i level="1" centre="261" editable="1"/><i level="1" centre="249" editable="1"/><i level="1" centre="62" editable="1"/><i level="1" centre="251" editable="1"/><i level="1" centre="152" editable="1"/><i level="1" centre="252" editable="1"/><i level="1" centre="236" editable="1"/><i level="1" centre="211" editable="1"/><i level="1" centre="71" editable="1"/><i level="1" centre="202" editable="1"/><i level="1" centre="231" editable="1"/><i level="1" centre="212" editable="1"/><i level="1" centre="171" editable="1"/><i level="1" centre="129" editable="1"/><i level="1" centre="223" editable="1"/><i level="1" centre="56" editable="1"/><i level="1" centre="123" editable="1"/><i level="1" centre="46" editable="1"/><i level="1" centre="175" editable="1"/><i level="1" centre="265" editable="1"/><i level="1" centre="31" editable="1"/><i level="1" centre="72" editable="1"/><i level="1" centre="38" editable="1"/><i level="1" centre="47" editable="1"/><i level="1" centre="113" editable="1"/><i level="1" centre="213" editable="1"/><i level="1" centre="32" editable="1"/><i level="1" centre="41" editable="1"/><i level="1" centre="186" editable="1"/><i level="1" centre="260" editable="1"/><i level="1" centre="191" editable="1"/><i level="1" centre="122" editable="1"/><i level="1" centre="203" editable="1"/><i level="1" centre="220" editable="1"/><i level="1" centre="244" editable="1"/><i level="1" centre="137" editable="1"/><i level="1" centre="136" editable="1"/><i level="1" centre="110" editable="1"/><i level="1" centre="121" editable="1"/><i level="1" centre="233" editable="1"/><i level="1" centre="192" editable="1"/><i level="1" centre="227" editable="1"/><i level="1" centre="144" editable="1"/><i level="1" centre="35" editable="1"/><i level="1" centre="253" editable="1"/><i level="1" centre="116" editable="1"/><i level="1" centre="196" editable="1"/><i level="1" centre="66" editable="1"/><i level="1" centre="206" editable="1"/><i level="1" centre="94" editable="1"/><i level="1" centre="153" editable="1"/><i level="1" centre="189" editable="1"/><i level="1" centre="82" editable="1"/><i level="1" centre="112" editable="1"/><i level="1" centre="145" editable="1"/><i level="1" centre="95" editable="1"/><i level="1" centre="98" editable="1"/><i level="1" centre="159" editable="1"/><i level="1" centre="93" editable="1"/><i level="1" centre="97" editable="1"/><i level="1" centre="135" editable="1"/><i level="1" centre="139" editable="1"/><i level="1" centre="127" editable="1"/><i level="1" centre="106" editable="1"/><i level="1" centre="173" editable="1"/><i level="1" centre="194" editable="1"/><i level="1" centre="87" editable="1"/><i level="1" centre="218" editable="1"/><i level="1" centre="228" editable="1"/><i level="1" centre="214" editable="1"/><i level="1" centre="124" editable="1"/><i level="1" centre="215" editable="1"/><i level="1" centre="184" editable="1"/><i level="1" centre="59" editable="1"/><i level="1" centre="254" editable="1"/><i level="1" centre="230" editable="1"/><i level="1" centre="64" editable="1"/><i level="1" centre="174" editable="1"/><i level="1" centre="141" editable="1"/><i level="1" centre="69" editable="1"/><i level="1" centre="57" editable="1"/><i level="1" centre="115" editable="1"/><i level="1" centre="257" editable="1"/><i level="1" centre="209" editable="1"/><i level="1" centre="237" editable="1"/><i level="1" centre="99" editable="1"/><i level="1" centre="242" editable="1"/><i level="1" centre="155" editable="1"/><i level="1" centre="100" editable="1"/><i level="1" centre="262" editable="1"/><i level="1" centre="264" editable="1"/><i level="1" centre="101" editable="1"/><i level="1" centre="142" editable="1"/><i level="1" centre="143" editable="1"/><i level="1" centre="217" editable="1"/><i level="1" centre="229" editable="1"/><i level="1" centre="205" editable="1"/><i level="1" centre="118" editable="1"/><i level="0" centre="1" editable="1"/><i level="1" centre="1" editable="1"/><i level="1" centre="102" editable="1"/><i level="1" centre="170" editable="1"/><i level="1" centre="210" editable="1"/><i level="1" centre="77" editable="1"/><i level="1" centre="169" editable="1"/><i level="1" centre="91" editable="1"/><i level="1" centre="255" editable="1"/><i level="1" centre="190" editable="1"/><i level="1" centre="239" editable="1"/><i level="1" centre="33" editable="1"/><i level="1" centre="53" editable="1"/><i level="1" centre="138" editable="1"/><i level="1" centre="268" editable="1"/><i level="1" centre="266" editable="1"/><i level="1" centre="34" editable="1"/><i level="1" centre="74" editable="1"/><i level="1" centre="248" editable="1"/><i level="1" centre="207" editable="1"/><i level="1" centre="204" editable="1"/><i level="1" centre="201" editable="1"/><i level="1" centre="226" editable="1"/><i level="1" centre="232" editable="1"/><i level="1" centre="84" editable="1"/><i level="1" centre="83" editable="1"/><i level="1" centre="128" editable="1"/><i level="1" centre="81" editable="1"/><i level="1" centre="247" editable="1"/><i level="1" centre="150" editable="1"/><i level="1" centre="73" editable="1"/><i level="1" centre="222" editable="1"/><i level="1" centre="200" editable="1"/><i level="1" centre="125" editable="1"/><i level="1" centre="78" editable="1"/><i level="1" centre="177" editable="1"/><i level="1" centre="167" editable="1"/><i level="1" centre="120" editable="1"/><i level="1" centre="234" editable="1"/><i level="1" centre="130" editable="1"/><i level="1" centre="85" editable="1"/><i level="1" centre="131" editable="1"/><i level="1" centre="86" editable="1"/><i level="1" centre="132" editable="1"/><i level="1" centre="269" editable="1"/><i level="1" centre="183" editable="1"/><i level="1" centre="133" editable="1"/><i level="1" centre="92" editable="1"/><i level="1" centre="50" editable="1"/><i level="1" centre="61" editable="1"/><i level="1" centre="185" editable="1"/><i level="1" centre="154" editable="1"/><i level="1" centre="216" editable="1"/><i level="1" centre="221" editable="1"/><i level="1" centre="208" editable="1"/><i level="1" centre="119" editable="1"/><i level="1" centre="36" editable="1"/><i level="1" centre="224" editable="1"/><i level="1" centre="134" editable="1"/><i level="1" centre="193" editable="1"/><i level="1" centre="88" editable="1"/><i level="1" centre="70" editable="1"/><i level="1" centre="241" editable="1"/><i level="1" centre="238" editable="1"/><i level="1" centre="147" editable="1"/><i level="1" centre="39" editable="1"/><i level="1" centre="67" editable="1"/><i level="1" centre="68" editable="1"/><i level="1" centre="195" editable="1"/><i level="1" centre="146" editable="1"/><i level="1" centre="104" editable="1"/><i level="1" centre="60" editable="1"/><i level="1" centre="90" editable="1"/><i level="1" centre="164" editable="1"/><i level="1" centre="89" editable="1"/><i level="1" centre="75" editable="1"/><i level="1" centre="105" editable="1"/><i level="1" centre="96" editable="1"/><i level="1" centre="181" editable="1"/><i level="1" centre="65" editable="1"/><i level="1" centre="243" editable="1"/><i level="1" centre="180" editable="1"/><i level="1" centre="188" editable="1"/><i level="1" centre="246" editable="1"/><i level="1" centre="219" editable="1"/><i level="1" centre="107" editable="1"/><i level="1" centre="149" editable="1"/><i level="1" centre="140" editable="1"/><i level="1" centre="197" editable="1"/><i level="1" centre="160" editable="1"/><i level="1" centre="162" editable="1"/><i level="1" centre="40" editable="1"/><i level="1" centre="103" editable="1"/><i level="1" centre="42" editable="1"/><i level="1" centre="235" editable="1"/><i level="1" centre="240" editable="1"/><i level="1" centre="63" editable="1"/><i level="1" centre="225" editable="1"/><i level="1" centre="151" editable="1"/><i level="1" centre="76" editable="1"/><i level="1" centre="55" editable="1"/><i level="1" centre="43" editable="1"/><i level="1" centre="165" editable="1"/><i level="1" centre="156" editable="1"/><i level="1" centre="158" editable="1"/><i level="1" centre="44" editable="1"/><i level="1" centre="157" editable="1"/><i level="1" centre="45" editable="1"/><i level="1" centre="199" editable="1"/><i level="1" centre="109" editable="1"/><i level="1" centre="163" editable="1"/><i level="1" centre="108" editable="1"/><i level="1" centre="48" editable="1"/><i level="1" centre="126" editable="1"/><i level="1" centre="263" editable="1"/><i level="1" centre="49" editable="1"/><i level="1" centre="250" editable="1"/><i level="1" centre="80" editable="1"/><i level="1" centre="37" editable="1"/><i level="1" centre="178" editable="1"/><i level="1" centre="161" editable="1"/><i level="1" centre="111" editable="1"/><i level="1" centre="198" editable="1"/><i level="1" centre="256" editable="1"/><i level="1" centre="187" editable="1"/><i level="1" centre="258" editable="1"/><i level="1" centre="182" editable="1"/><i level="1" centre="51" editable="1"/><i level="1" centre="176" editable="1"/><i level="1" centre="148" editable="1"/><i level="1" centre="168" editable="1"/><i level="1" centre="58" editable="1"/><i level="1" centre="172" editable="1"/><i level="1" centre="267" editable="1"/><i level="1" centre="52" editable="1"/><i level="1" centre="114" editable="1"/><i level="1" centre="179" editable="1"/><i level="1" centre="166" editable="1"/><i level="1" centre="245" editable="1"/><i level="1" centre="79" editable="1"/><i level="1" centre="259" editable="1"/><i level="1" centre="117" editable="1"/><i level="1" centre="54" editable="1"/></list>'
SET @assignedQualificationIdList = N''
SET @useExamSessions = 1	
	
--AS
	--BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
		SET NOCOUNT ON;
		SET DATEFORMAT dmy;
		--Paging vars
		DECLARE @TotalCount			INT,
				@TotalResultCount	INT,
				@RowStart			INT,
				@RowEnd				INT;
		--Dynamic Sql vars
		DECLARE	@selectDetails				VARCHAR(MAX),
				@whereclause				VARCHAR(MAX),
				@qualityReviewWhereClause	VARCHAR(MAX),
				@pagingwhereclause			VARCHAR(MAX),
				@sqlPrePaging				NVARCHAR(MAX),
				@sqlPostPaging				NVARCHAR(MAX),
				@xmlUserSearch				VARCHAR(MAX),
				@orderby					VARCHAR(MAX),
			    @startDateClause			NVARCHAR(MAX),
				@endDateClause				NVARCHAR(MAX),
				@endDateSelect				NVARCHAR(MAX),
				@startTimeClause			NVARCHAR(MAX),
				@endTimeClause				NVARCHAR(MAX);
		-- Other vars
		DECLARE @myCheckAssignedQualifications	BIT,
				@myCheckQualifications			BIT,
				@myCandidateCountSelect			NVARCHAR(MAX),
				@myCandidateDetailsSelect		NVARCHAR(MAX),
				@myCandidateDetailsPart			NVARCHAR(MAX);
		-- XML Document integer
		DECLARE @hdoc INT;
		--BEGIN TRY
			-- SET row request vars
			SET @RowStart = @pageSize * @pageIndex +1;
			SET @RowEnd = @RowStart + @pageSize -1;
			-- Prepare the xml document
			EXEC sp_xml_preparedocument @hdoc OUTPUT, @centreQualificationLevelsXml;
			-- Create the Temp Table for the Centre Qualification Levels
			CREATE TABLE #TEMP_CentreQualificationLevels_TABLE([qualLevel] INT, [centreId] INT);
			INSERT INTO #TEMP_CentreQualificationLevels_TABLE(qualLevel, centreId)
				SELECT *
				FROM OPENXML(@hdoc, '//list/i', 1)
				WITH
				(
					[qualLevel]      int  '@level',
					[centreId]     int  '@centre'
				)
			-- SET @myCheckAssignedQualifications value
			IF(LEN(@assignedQualificationIdList) = 0)
				SET @myCheckAssignedQualifications = 0;
			ELSE
     			SET @myCheckAssignedQualifications = 1;
			-- SET @myCheckQualifications value
			IF(LEN(@qualification) = 0)
				SET @myCheckQualifications = 0;
			ELSE
				SET @myCheckQualifications = 1;

			-- Replace the sorting values with values that map to the DB            
			SET @sorting = REPLACE(@sorting, 'centres', 'centreName')            
			SET @sorting = REPLACE(@sorting, 'qualification', 'qualificationName')            
			SET @sorting = REPLACE(@sorting, 'examName', 'examName')            
			SET @sorting = REPLACE(@sorting, 'examWindow', 'startDate')            
			SET @sorting = REPLACE(@sorting, 'endDate', 'endDate')            
			SET @sorting = REPLACE(@sorting, 'dateCreated', 'dateCreated')            
			SET @sorting = REPLACE(@sorting, 'createdBySurname', 'createdBySurname')            
			SET @sorting = REPLACE(@sorting, 'groupState', 'groupState')            
			-- SET @qualityReviewWhereClause starting value
			IF @includeQualityReviewExams=0
				SET @qualityReviewWhereClause = ' AND dbo.ScheduledExamsTable.qualityReview=0';
			ELSE
				SET @qualityReviewWhereClause = '';       
			-- SET @startDateClause starting value
			IF @startDateStartRange <> ''
				SET @startDateClause =' Convert(DATE, dbo.ScheduledExamsTable.[ScheduledStartDateTime]) >= ''' + @startDateStartRange +'''
									AND Convert(DATE, dbo.ScheduledExamsTable.[ScheduledStartDateTime]) <= ''' + @startDateEndRange +''' ';
			ELSE
				SET @startDateClause ='';
			-- SET @startTimeClause starting value
			IF @startTimeStartRange  <> ''	
				SET @startTimeClause =' dbo.ScheduledExamsTable.[ActiveStartTime] >= ' + @startTimeStartRange + '
									AND dbo.ScheduledExamsTable.[ActiveStartTime] <= ' + @startTimeEndRange;
			ELSE
				SET @startTimeClause ='';
			-- SET @endTimeClause starting value
			IF @endTimeStartRange <> ''
				SET @endTimeClause  = ' dbo.ScheduledExamsTable.[ActiveEndTime] >= ' + @endTimeStartRange + '
									AND dbo.ScheduledExamsTable.[ActiveEndTime] <= ' + @endTimeEndRange;
			-- SET Default values
			SET @myCandidateCountSelect = '';
			SET @myCandidateDetailsSelect = '';
			SET @myCandidateDetailsPart = '';
			SET @myCandidateCountSelect = '(SELECT count(*) FROM [dbo].[ExamSessionTable] where ScheduledExamID = dbo.ScheduledExamsTable.[ID]) as totalCandidates,';
			SET @endDateSelect = 'dbo.ScheduledExamsTable.ScheduledEndDateTime ';
	
			IF(@useExamSessions = 1)
				--BEGIN
					SET @myCandidateDetailsSelect = ', (SELECT Forename FROM UserTable WHERE UserTable.ID = ExamSessionTable.UserID) AS firstName,
													   (SELECT Surname FROM UserTable WHERE UserTable.ID = ExamSessionTable.UserID) AS surname,
													   (SELECT CandidateRef FROM UserTable WHERE UserTable.ID = ExamSessionTable.UserID) AS candidateRef,
													   (SELECT isNULL(ULN, '''') FROM UserTable WHERE UserTable.ID = ExamSessionTable.UserID) AS candidateUln,
													   (SELECT DOB FROM UserTable WHERE UserTable.ID = ExamSessionTable.UserID) AS candidateDateOfBirth,
														UserID AS candidateID,
														ExamSessionTable.ID AS examSessionID';
					SET @myCandidateDetailsPart = ', firstName, surname, candidateRef, candidateID, examSessionID, candidateUln, candidateDateOfBirth  ';
					SET @endDateSelect = 'CASE WHEN dbo.ExamSessionTable.IsProjectBased = 1
										  THEN
											 CASE WHEN dbo.ExamSessionTable.StructureXML IS NULL
											 THEN
												 -- no strucutreXML so use durationXML
												 DATEADD(MINUTE, ExamSessionTable.ScheduledDurationXml.value(''(/duration/value)[1]'', ''int'')-1440, dbo.ScheduledExamsTable.ScheduledStartDateTime)
											 ELSE
												 -- use structureXML
												 DATEADD(MINUTE, ExamSessionTable.StructureXML.value(''(/assessmentDetails/scheduledDuration/value)[1]'', ''int'')-1440, dbo.ScheduledExamsTable.ScheduledStartDateTime)
											 END
										  ELSE
											 -- use scheduledExam end date
											 dbo.ScheduledExamsTable.ScheduledEndDateTime
										  END ';
				--END
				
			
			-- SET @endDateClause starting value
			IF @endDateEndRange <> ''
				SET @endDateClause =' Convert(DATE, '+@endDateSelect+') >= ''' + @endDateStartRange +'''
								  AND Convert(DATE, '+@endDateSelect+') <= ''' + @endDateEndRange +'''';
			ELSE
				SET @endDateClause ='';
				
			-- SET the selection details. This should map to columns identified in the select xml above, it must include the full select. If all the data cannot be retreived at this level            
			-- then the procedure must be re designed. the use of subqueries and joins is advised for complex queries.            
			-- This select also include the sort order passed in            
			SET @selectDetails = 'SELECT CentreName as [Centre Name],
										 qualificationName as [Qualification],
										 qualificationLevel,
										 examName,
										 startDate,
										 endDate,
										 scheduledExamEndDate,
										 startTime,
										 endTime,
										 totalCandidates,
										 dateCreated,
										 examId,
										 centreName,
										 centreId,
										 
										 createdByForename,
										 createdBySurname,
										 scheduledExamId,
										 groupState,
										 examVersionId,
										 Invigilated,
										 ScheduledForInvigilate AS ''scheduledForInvigilate'',
										 StrictControlOnDDA'
										 + @myCandidateDetailsPart + '
								  FROM
								  (            
									  SELECT  ROW_NUMBER() OVER (ORDER BY ' + @sorting + ') as RowNumber,
											  CentreName as [Centre Name],
											  qualificationName,
											  qualificationLevel,
											  examName,
											  startDate,
											  endDate,
											  scheduledExamEndDate,
											  startTime,
											  endTime,
											  totalCandidates,
											  dateCreated,
											  examId,
											  centreName,
											  centreId,
										
											  createdByForename,
											  createdBySurname,
											  scheduledExamId,
											  groupState,
											  examVersionId,
											  Invigilated,
											  ScheduledForInvigilate,
											  StrictControlOnDDA'
											  + @myCandidateDetailsPart + '
									  FROM             
									  (
									      SELECT CentreName as [Centre Name],
												 dbo.IB3QualificationLookup.[QualificationName]	    AS qualificationName,
												 dbo.IB3QualificationLookup.[QualificationLevel]	AS qualificationLevel,
												 dbo.ScheduledExamsTable.[examName]					AS examName,
												 dbo.ScheduledExamsTable.[ScheduledStartDateTime]	AS startDate,'
											 	 + @endDateSelect + '								AS endDate,
												 dbo.ScheduledExamsTable.[ScheduledEndDateTime]		AS scheduledExamEndDate,
												 dbo.ScheduledExamsTable.[ActiveStartTime]			AS startTime,
												 dbo.ScheduledExamsTable.[ActiveEndTime]			AS endTime, '
												 + @myCandidateCountSelect + '
												 dbo.ScheduledExamsTable.[CreatedDateTime]			AS dateCreated,
												 dbo.ScheduledExamsTable.[ExamID]					AS examId,
												 dbo.CentreTable.CentreName							AS centreName,
												 dbo.ScheduledExamsTable.CentreID					AS centreId,
												 
												 dbo.UserTable.forename								AS createdByForename,
												 dbo.UserTable.surname								AS createdBySurname,
												 dbo.ScheduledExamsTable.[ID]						AS scheduledExamId,
												 dbo.ScheduledExamsTable.[groupState]				AS groupState,
												 dbo.ScheduledExamsTable.[examVersionId]			AS examVersionId,
												 dbo.ScheduledExamsTable.[Invigilated]				AS Invigilated,
												 dbo.ScheduledExamsTable.[ScheduledForInvigilate]	AS ScheduledForInvigilate, 
												 dbo.ScheduledExamsTable.StrictControlOnDDA			AS StrictControlOnDDA'
												 + @myCandidateDetailsSelect;

				IF(@useExamSessions = 1)
					SET @selectDetails = @selectDetails + ' FROM [dbo].[ExamSessionTable]
															INNER JOIN
																[dbo].[ScheduledExamsTable] ON [dbo].[ScheduledExamsTable].ID = [dbo].[ExamSessionTable].ScheduledExamID
															INNER JOIN
																CentreTable on dbo.ScheduledExamsTable.CentreID = dbo.CentreTable.ID
															INNER JOIN
																UserTable on dbo.ScheduledExamsTable.CreatedBy = dbo.UserTable.ID
															INNER JOIN
																IB3QualificationLookup on IB3QualificationLookup.id = dbo.ScheduledExamsTable.qualificationId';
				ELSE
					SET @selectDetails = @selectDetails + ' FROM [dbo].[ScheduledExamsTable]
															INNER JOIN
																CentreTable on dbo.ScheduledExamsTable.CentreID = dbo.CentreTable.ID
															INNER JOIN
																UserTable on dbo.ScheduledExamsTable.CreatedBy = dbo.UserTable.ID
															INNER JOIN
																IB3QualificationLookup on IB3QualificationLookup.id = dbo.ScheduledExamsTable.qualificationId';
            
				-- Split Surname 
				DECLARE @SurnameClause VARCHAR(MAX);
				IF CHARINDEX(' ',@surname) > 0
					SET @SurnameClause = ' (dbo.UserTable.forename LIKE ''' + RTRIM(LTRIM(SUBSTRING(@surname,1 , CHARINDEX(' ', @surname)))) + '%''
										 OR dbo.UserTable.surname LIKE ''%' + LTRIM(RTRIM(SUBSTRING(@surname, CHARINDEX(' ', @surname), len(@surname)))) + ''')';
				ELSE
					SET @SurnameClause = ' (dbo.UserTable.surname LIKE ''' + @surname + ''' OR dbo.UserTable.forename LIKE ''' + @surname + ''')';
	 
				-- Change the operators for createdby here based on @onlyCreatedByMe            
				DECLARE @createdBySearchParam VARCHAR(MAX);
				SET @createdBySearchParam = ' <> -1';

				IF @onlyCreatedByMe <> -1        
					SET @createdBySearchParam = ' = ' + CONVERT(VARCHAR(MAX), @onlyCreatedByMe);

				DECLARE @myCandidateWhereClause VARCHAR(MAX);
				SET @myCandidateWhereClause = '';
				-- Handle filtertype operator scenarios of AND and OR
				IF @filterType = 'AND'
					--BEGIN
						IF @centres <> ''
							SET @whereclause =  ' WHERE dbo.CentreTable.ID IN( ' + @centres + ' )   AND  ';
						ELSE
							SET @whereclause =  ' WHERE ';
     
						SET @whereclause =  @whereclause + @SurnameClause +	' AND dbo.ScheduledExamsTable.[examName] LIKE ''' + @examName +'''' +
							CASE @showOnlyInvigilatedExams WHEN 1 THEN
								' AND (dbo.ScheduledExamsTable.invigilated = 1 OR dbo.ScheduledExamsTable.ScheduledForInvigilate = 1) '
							ELSE
								' '
								END
					--END;
				IF(@myCheckQualifications = 1)
					SET @whereclause = @whereclause + ' AND (qualificationID IN (' + @qualification + '))';

				IF(@myCheckAssignedQualifications = 1)
					SET @whereclause = @whereclause + ' AND (qualificationID IN (' + @assignedQualificationIdList + '))';

				--IF (@totalNoOfCandidates > 0)
				--	SET @whereclause = @whereclause + ' AND (totalCandidates >= '+ @totalNoOfCandidates +')';

			IF(@useExamSessions = 1)
				--BEGIN
					SET @myCandidateWhereClause = ' AND (SELECT Surname FROM UserTable WHERE UserTable.ID = ExamSessionTable.UserID) LIKE ''' + @candidateSurname + '''
													AND (SELECT CandidateRef FROM UserTable WHERE UserTable.ID = ExamSessionTable.UserID) LIKE ''' + @candidateRef + '''
													AND (SELECT isNULL(ULN, '''') FROM UserTable WHERE UserTable.ID = ExamSessionTable.UserID) LIKE ''' + @candidateUln + '''';
					IF(LEN(@dateOfBirthStartRange) > 0)
						SET @myCandidateWhereClause = @myCandidateWhereClause + ' AND (SELECT DOB FROM UserTable WHERE UserTable.ID = ExamSessionTable.UserID) >= Convert(smalldatetime, ''' + @dateOfBirthStartRange + ''')';
					IF(LEN(@dateOfBirthEndRange) > 0)
						SET @myCandidateWhereClause = @myCandidateWhereClause + ' AND (SELECT DOB FROM UserTable WHERE UserTable.ID = ExamSessionTable.UserID) <= Convert(smalldatetime, ''' + @dateOfBirthEndRange + ''') ';
				--END
	
			IF @dateCreatedStartRange != ''
				SET @whereclause = @whereclause + ' AND dbo.ScheduledExamsTable.[CreatedDateTime] >= Convert(smalldatetime, ''' + @dateCreatedStartRange +''') ';
			
			IF @dateCreatedEndRange != ''
				SET @whereclause = @whereclause + ' AND dbo.ScheduledExamsTable.[CreatedDateTime] <= Convert(smalldatetime, ''' + @dateCreatedEndRange +''') ';

			IF @startDateClause <> ''
				SET @whereclause = @whereclause + ' AND ' + @startDateClause;

   			IF @endDateClause <> ''
   				SET @whereclause = @whereclause + ' AND ' + @endDateClause;

   			IF @startTimeClause <> ''
   				SET @whereclause = @whereclause + ' AND ' + @startTimeClause;
   				   								
   			IF @endTimeClause <> ''
   				SET @whereclause = @whereclause + ' AND ' + @endTimeClause

			IF @invigilated <> ''
				SET @whereclause = @whereclause + ' AND ( dbo.ScheduledExamsTable.invigilated = '+ CONVERT(NVARCHAR(1), @invigilated) +' OR dbo.ScheduledExamsTable.ScheduledForInvigilate = '+ CONVERT(NVARCHAR(1), @invigilated) + ' ) ';

			IF 	@groupState <> ''
				SET @whereclause = @whereclause + ' AND dbo.ScheduledExamsTable.groupState IN (  '+ @groupState +' ) ';
			
			SET @whereclause = @whereclause + ' AND (dbo.ScheduledExamsTable.groupState = 1 OR dbo.ScheduledExamsTable.groupState = 2)
												AND dbo.ScheduledExamsTable.CreatedBy ' + @createdBySearchParam + '
												' + @qualityReviewWhereClause + @myCandidateWhereClause + '
												AND EXISTS(
															SELECT TOP (1) ID FROM #TEMP_CentreQualificationLevels_TABLE
															INNER JOIN IB3QualificationLookup
																ON IB3QualificationLookup.QualificationLevel = #TEMP_CentreQualificationLevels_TABLE.qualLevel
															WHERE ID = dbo.ScheduledExamsTable.qualificationId
															AND (centreId = 1 OR centreId = dbo.CentreTable.ID)
														   )
													)as ExamDetails) as examSearch';
		--END
		-- Build the pre and post paging strings
		--SET @pagingwhereclause = ' WHERE RowNumber BETWEEN @RowStart AND @RowEnd' + @sorting;
		SET @sqlPrePaging = 'SELECT @TotalResultCount =  Count(*) FROM (' + @selectDetails + @whereclause + ') as TotalRowCount';
		-- ** We need to execute as an ansi string for the parameterised sorting to work **
		EXEC sp_executesql @sqlPrePaging, N'@TotalResultCount INT OUTPUT'  , @TotalResultCount OUTPUT                
		-- Required variables set before paging applied            
		SET @TotalCount = CEILING((@TotalResultCount * 1.0) / @PageSize)                     

		DECLARE @xmlSelectStart NVARCHAR(MAX);
		DECLARE @xmlSelectEnd NVARCHAR(MAX);
		--SET @xmlSelectStart = 'SELECT ''0'' AS ''@errorCode'',(
								--SELECT @PageNumber AS ''@pageIndex'',
									--	@PageSize AS ''@pageSize'',
									--	@TotalCount AS ''@totalCount'',
									--	@TotalResultCount AS ''@totalRecords'',(';
		--SET @xmlSelectEnd = ' FOR XML PATH(''exam''), TYPE) FOR XML PATH(''return''), TYPE) FOR XML PATH(''result'')';
		SET @sqlPostPaging =  @selectDetails + @whereclause --+ @pagingwhereclause --+ @xmlSelectEnd;

	
		EXEC sp_executesql @sqlPostPaging, N'@RowStart INT, @RowEnd INT, @TotalResultCount INT, @TotalCount INT, @PageNumber INT, @PageSize INT',    @RowStart, @RowEnd, @TotalResultCount, @TotalCount, @PageIndex, @PageSize;
		-- Clean the filter doc from memory
		EXEC sp_xml_removedocument @hdoc;
		-- DROP TEMP TABLES
		DROP TABLE #TEMP_CentreQualificationLevels_TABLE;
	--END TRY            
	--BEGIN CATCH            
	--	EXEC sa_SHARED_GetErrorDetails_sp   
	--END CATCH;           
--END


--GO
--

