select *
from [UserCentreSubjectRoles_DeleteAudit]
go


INSERT [UserCentreSubjectRoles_DeleteAudit]
VALUES(1,2,3,4,5,6,7,(SELECT getDATE()))


ALTER TRIGGER [fireEmail] on [UserCentreSubjectRoles_DeleteAudit] 
	AFTER INSERT
AS
	BEGIN

	DECLARE @query nvarchar(MAX);
	DECLARE @subject nvarchar(200) = 'OCR Deleted exams:  ' + (Select CONVERT(VARCHAR,GETDATE(), 21))

	if OBJECT_ID('tempdb..##deletedentries') is not null drop table ##deletedentries;

	CREATE TABLE ##deletedentries(
		[Id] [bigint] NULL,
		[UserId] [bigint] NULL,
		[CentreId] [bigint] NULL,
		[SubjectId] [bigint] NULL,
		[SubjectLinkType] [int] NULL,
		[Assignable] [bit] NULL,
		[RoleId] [int] NULL,
		[Timestamp] [datetime] NULL
	);

	INSERT ##deletedentries
	SELECT Id, UserId, CentreId, SubjectId, SubjectLinkType, Assignable, RoleId, Timestamp
	from inserted;


	--SELECT * FROM ##deletedentries
	--BEGIN
	--	SET @query = 'SELECT Id, UserId, CentreId, SubjectId, SubjectLinkType, Assignable, RoleId, Timestamp FROM ##deletedentries WITH (NOLOCK)';  --SELECT * FROM ##deletedentries
	--END
		--BEGIN
			EXECUTE msdb.dbo.sp_send_dbmail
					@profile_name = 'SMTP Virtual Server',
			            --@recipients = N'liveservices@btl.com; support@btl.com',
					@recipients = N'dave.jones@btl.com',
					@subject = @subject,
					@body = N'These exams have been deleted',
					@execute_query_database = N'OCR_SurpassManagement',
					@query ='SELECT Id, UserId, CentreId, SubjectId, SubjectLinkType, Assignable, RoleId, Timestamp FROM ##deletedentries WITH (NOLOCK)',
					@attach_query_result_as_file = 1,
					@query_attachment_filename = N'DeletedExams.csv',
					@query_result_separator = N',',
					@query_result_no_padding = 1;
		--END
	END
GO

