set nocount on;

if OBJECT_ID('tempdb..##RawData') is not null drop table ##RawData;
if OBJECT_ID('tempdb..##Formatted') is not null drop table ##Formatted;

SELECT [ItemID]
	  , Itemversion
	  ,cast(item.node.query('data(.)[1]') as nvarchar(max)) [Q/A]
	  ,ROW_NUMBER() OVER (PARTITION BY ItemID, itemversion ORDER BY ItemID) R
INTO ##RawData
FROM [PRV_SkillsFirst_SecureAssess].[dbo].[CPAuditTable]
CROSS APPLY ItemXML.nodes('P/S/C/I/TEXTFORMAT/P/FONT') item(node)
where ItemXML.exist('P/S/C[@typ=10]') = 1
--where itemid in ('334P345', '334P466')
--select * from ##RawData

--select top 10 * FROM [PRV_SkillsFirst_SecureAssess].[dbo].[CPAuditTable]

create table ##Formatted (

	Itemid varchar(12)
	, Itemversion smallint
	, Question varchar(max)
	, Answer varchar(max)
)
INSERT ##Formatted(Itemid,Itemversion,Question)
select ItemID
	, ItemVersion
	, [Q/A]
from ##RawData
where R = 1;

select * from ##Formatted
select * from CPAuditTable where itemid = '821P1218'

declare myCursor cursor for
select distinct itemid
from ##RawData

declare @itemid varchar(200)

open myCursor

fetch next from myCursor into @itemid

while @@FETCH_STATUS = 0

begin

	UPDATE ##Formatted
	set Answer = (
		SELECT SUBSTRING(
			(
			SELECT DISTINCT N', '+QUOTENAME([Q/A])
			FROM ##RawData 
			where R > 1
				and ItemID = @itemid
			FOR XML PATH('')
			), 2, 10000)
			)
	where ItemID = @itemid

fetch next from myCursor into @itemid

end
close myCursor
deallocate myCursor

select *
from ##Formatted





