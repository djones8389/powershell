SELECT 
	A.Instance
	, A.[database_name]
	, cast([StartDate] as date) [Date]
	, round((SUM(a.data_kb) + SUM(a.index_size))/1024/1024,0) [total]
	--, SUM(a.data_kb) data_kb
	--, SUM(a.index_size) index_size
FROM (
SELECT 
	[Instance]
      ,[database_name]
      ,cast(replace([data_kb], 'KB','') as float) [data_kb]
      ,cast(replace([index_size], 'KB','') as float) [index_size]
      ,cast([StartDate] as nvarchar(16)) [StartDate]
  FROM [PSCollector].[dbo].[DBGrowthMetrics]
) A

where Instance in ('430069-AAT-SQL\SQL1')
	and database_name in ('AAT_SecureAssess')
	--and [StartDate] BETWEEN @StartDate AND @EndDate
group by 
	A.Instance
	, A.[database_name]
	, a.StartDate
order by [StartDate] desc;

2018-04-09		493
2017-04-11		356

137 per year
2.634615384615385 per year
