/*Update non xml based columns*/
UPDATE WAREHOUSE_ExamSessionTable_Shreded
SET  WAREHOUSEScheduledExamID = WAREHOUSE_ScheduledExamsTable.ID
                ,WAREHOUSEUserID = WAREHOUSE_UserTable.ID
                ,examId = WAREHOUSE_ScheduledExamsTable.ExamID
                ,userId = WAREHOUSE_UserTable.UserID
FROM WAREHOUSE_ScheduledExamsTable
INNER JOIN WAREHOUSE_ExamSessionTable ON WAREHOUSE_ScheduledExamsTable.ID = WAREHOUSE_ExamSessionTable.WAREHOUSEScheduledExamID
INNER JOIN WAREHOUSE_ExamSessionTable_Shreded ON WAREHOUSE_ExamSessionTable.ID = WAREHOUSE_ExamSessionTable_Shreded.examSessionId
INNER JOIN WAREHOUSE_UserTable ON WAREHOUSE_ExamSessionTable.WAREHOUSEUserID = WAREHOUSE_UserTable.ID
where  WAREHOUSE_ExamSessionTable_Shreded.examSessionId between 1 and 3230;

/*Update voids*/
UPDATE WAREHOUSE_ExamSessionTable_Shreded
SET voidJustificationLookupTableId = WAREHOUSE_ExamSessionTable.ExamStateChangeAuditXml.value('(exam/stateChange[newStateID=10][1]/information/stateChangeInformation/reason)[1]', 'int')
FROM WAREHOUSE_ExamSessionTable
INNER JOIN WAREHOUSE_ExamSessionTable_Shreded ON WAREHOUSE_ExamSessionTable.ID = WAREHOUSE_ExamSessionTable_Shreded.examSessionId
WHERE WAREHOUSE_ExamSessionTable.previousExamState = 10
	and WAREHOUSE_ExamSessionTable.id between 1 and 3230;

/*Update StateChnage xml based data*/
UPDATE WAREHOUSE_ExamSessionTable_Shreded
SET         [started] = WAREHOUSE_ExamSessionTable.ExamStateChangeAuditXml.value('(exam/stateChange[newStateID=6][1]/changeDate)[1]', 'DATETIME')
                ,submitted = WAREHOUSE_ExamSessionTable.ExamStateChangeAuditXml.value('(exam/stateChange[newStateID=9 or newStateID=10]/changeDate)[last()]', 'DATETIME')
FROM WAREHOUSE_ExamSessionTable
INNER JOIN WAREHOUSE_ExamSessionTable_Shreded ON WAREHOUSE_ExamSessionTable.ID = WAREHOUSE_ExamSessionTable_Shreded.examSessionId
where  WAREHOUSE_ExamSessionTable_Shreded.examSessionId between 1 and 3230;

/*Condition based*/
UPDATE WAREHOUSE_ExamSessionTable_Shreded
SET resultSampled = WAREHOUSE_ExamSessionTable.ExamStateChangeAuditXml.exist('exam/stateChange[newStateID=12]/information/stateChangeInformation/type[text()="release"]')
FROM WAREHOUSE_ExamSessionTable
INNER JOIN WAREHOUSE_ExamSessionTable_Shreded ON WAREHOUSE_ExamSessionTable.ID = WAREHOUSE_ExamSessionTable_Shreded.examSessionId
where  WAREHOUSE_ExamSessionTable_Shreded.examSessionId between 1 and 3230;

/*StructureXML*/
UPDATE WAREHOUSE_ExamSessionTable_Shreded
SET  automaticVerification = WAREHOUSE_ExamSessionTable.StructureXML.value('(assessmentDetails/automaticVerification)[1]', 'BIT')
                ,validFromDate = WAREHOUSE_ExamSessionTable.StructureXML.value('(/assessmentDetails/validFromDate)[1]', 'DATETIME')
                ,expiryDate = WAREHOUSE_ExamSessionTable.StructureXML.value('(/assessmentDetails/expiryDate)[1]', 'DATETIME')
                ,defaultDuration = WAREHOUSE_ExamSessionTable.StructureXML.value('(/assessmentDetails/defaultDuration)[1]', 'INT')
                ,scheduledDurationReason = WAREHOUSE_ExamSessionTable.StructureXML.value('(/assessmentDetails/scheduledDuration/reason)[1]', 'VARCHAR(MAX)')
FROM WAREHOUSE_ExamSessionTable
INNER JOIN WAREHOUSE_ExamSessionTable_Shreded ON WAREHOUSE_ExamSessionTable.ID = WAREHOUSE_ExamSessionTable_Shreded.examSessionId
where  WAREHOUSE_ExamSessionTable_Shreded.examSessionId between 1 and 3230;

/*Result data*/
UPDATE WAREHOUSE_ExamSessionTable_Shreded
SET  passMark = resultData.value('(/exam/@passMark)[1]', 'DECIMAL(6, 3)')
                ,totalMark = resultData.value('(/exam/@totalMark)[1]', 'DECIMAL(6, 3)')
                ,passType = resultData.value('(/exam/@passType)[1]', 'DECIMAL(6, 3)')
                ,totalTimeSpent = resultData.value('(/exam/@totalTimeSpent)[1]', 'INT')
FROM WAREHOUSE_ExamSessionTable_Shreded
where  WAREHOUSE_ExamSessionTable_Shreded.examSessionId between 1 and 3230;


/*Result data full*/
UPDATE WAREHOUSE_ExamSessionTable_Shreded
SET itemDataFull = convert(xml,'<itemData>' + convert(nvarchar(MAX),(SELECT WAREHOUSE_ExamSessionTable.resultDataFull.query('/assessmentDetails[1]/assessment[1]//item'))) + '</itemData>')
FROM WAREHOUSE_ExamSessionTable
INNER JOIN WAREHOUSE_ExamSessionTable_Shreded ON WAREHOUSE_ExamSessionTable.ID = WAREHOUSE_ExamSessionTable_Shreded.examSessionId
where  WAREHOUSE_ExamSessionTable_Shreded.examSessionId between 1 and 3230;

