/*Update non xml based columns*/
UPDATE WAREHOUSE_ExamSessionTable_Shreded
SET  WAREHOUSEScheduledExamID = WAREHOUSE_ScheduledExamsTable.ID
                ,WAREHOUSEUserID = WAREHOUSE_UserTable.ID
                ,examId = WAREHOUSE_ScheduledExamsTable.ExamID
                ,userId = WAREHOUSE_UserTable.UserID
FROM WAREHOUSE_ScheduledExamsTable
INNER JOIN WAREHOUSE_ExamSessionTable ON WAREHOUSE_ScheduledExamsTable.ID = WAREHOUSE_ExamSessionTable.WAREHOUSEScheduledExamID
INNER JOIN WAREHOUSE_ExamSessionTable_Shreded ON WAREHOUSE_ExamSessionTable.ID = WAREHOUSE_ExamSessionTable_Shreded.examSessionId
INNER JOIN WAREHOUSE_UserTable ON WAREHOUSE_ExamSessionTable.WAREHOUSEUserID = WAREHOUSE_UserTable.ID;

/*Update voids*/
UPDATE WAREHOUSE_ExamSessionTable_Shreded
SET voidJustificationLookupTableId = WAREHOUSE_ExamSessionTable.ExamStateChangeAuditXml.value('(exam[1]/stateChange[newStateID=10][1]/information/stateChangeInformation/reason)[1]', 'int')
FROM WAREHOUSE_ExamSessionTable
INNER JOIN WAREHOUSE_ExamSessionTable_Shreded ON WAREHOUSE_ExamSessionTable.ID = WAREHOUSE_ExamSessionTable_Shreded.examSessionId
WHERE WAREHOUSE_ExamSessionTable.previousExamState = 10;

/*Update StateChnage xml based data*/
UPDATE WAREHOUSE_ExamSessionTable_Shreded
SET         [started] = WAREHOUSE_ExamSessionTable.ExamStateChangeAuditXml.value('(exam[1]/stateChange[newStateID=6][1]/changeDate)[1]', 'DATETIME')
                ,submitted = WAREHOUSE_ExamSessionTable.ExamStateChangeAuditXml.value('(exam[1]/stateChange[newStateID=9 or newStateID=10]/changeDate)[last()]', 'DATETIME')
FROM WAREHOUSE_ExamSessionTable
INNER JOIN WAREHOUSE_ExamSessionTable_Shreded ON WAREHOUSE_ExamSessionTable.ID = WAREHOUSE_ExamSessionTable_Shreded.examSessionId;


/*Condition based*/
UPDATE WAREHOUSE_ExamSessionTable_Shreded
SET resultSampled = WAREHOUSE_ExamSessionTable.ExamStateChangeAuditXml.exist('exam[1]/stateChange[newStateID=12]/information/stateChangeInformation/type[text()="release"]')
FROM WAREHOUSE_ExamSessionTable
INNER JOIN WAREHOUSE_ExamSessionTable_Shreded ON WAREHOUSE_ExamSessionTable.ID = WAREHOUSE_ExamSessionTable_Shreded.examSessionId;


/*StructureXML*/
UPDATE WAREHOUSE_ExamSessionTable_Shreded
SET  automaticVerification = WAREHOUSE_ExamSessionTable.StructureXML.value('(assessmentDetails[1]/automaticVerification)[1]', 'BIT')
                ,validFromDate = WAREHOUSE_ExamSessionTable.StructureXML.value('(/assessmentDetails[1]/validFromDate)[1]', 'DATETIME')
                ,expiryDate = WAREHOUSE_ExamSessionTable.StructureXML.value('(/assessmentDetails[1]/expiryDate)[1]', 'DATETIME')
                ,defaultDuration = WAREHOUSE_ExamSessionTable.StructureXML.value('(/assessmentDetails[1]/defaultDuration)[1]', 'INT')
                ,scheduledDurationReason = WAREHOUSE_ExamSessionTable.StructureXML.value('(/assessmentDetails[1]/scheduledDuration/reason)[1]', 'VARCHAR(MAX)')
FROM WAREHOUSE_ExamSessionTable
INNER JOIN WAREHOUSE_ExamSessionTable_Shreded ON WAREHOUSE_ExamSessionTable.ID = WAREHOUSE_ExamSessionTable_Shreded.examSessionId;


/*Result data*/
UPDATE WAREHOUSE_ExamSessionTable_Shreded
SET  passMark = resultData.value('(/exam[1]/@passMark)[1]', 'DECIMAL(6, 3)')
                ,totalMark = resultData.value('(/exam[1]/@totalMark)[1]', 'DECIMAL(6, 3)')
                ,passType = resultData.value('(/exam[1]/@passType)[1]', 'DECIMAL(6, 3)')
                ,totalTimeSpent = resultData.value('(/exam[1]/@totalTimeSpent)[1]', 'INT')
FROM WAREHOUSE_ExamSessionTable_Shreded;


/*Result data full*/
UPDATE WAREHOUSE_ExamSessionTable_Shreded
SET itemDataFull = convert(xml,'<itemData>' + convert(nvarchar(MAX),(SELECT WAREHOUSE_ExamSessionTable.resultDataFull.query('/assessmentDetails[1]/assessment[1]//item'))) + '</itemData>')
FROM WAREHOUSE_ExamSessionTable
INNER JOIN WAREHOUSE_ExamSessionTable_Shreded ON WAREHOUSE_ExamSessionTable.ID = WAREHOUSE_ExamSessionTable_Shreded.examSessionId;

