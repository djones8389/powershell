set transaction isolation level read uncommitted;

select Keycode
	, cev.DateSubmitted [Exam Imported to SM]
	, ur.insertionDate [Item Imported to SM]
	, i.ExternalItemName
	, isnull(coalesce(urd.id,ufd.id), 0) [HasDocuments?]
	, aim.*
from CandidateExamVersions cev
inner join CandidateResponses cr
on cr.CandidateExamVersionID = cev.ID
inner join UniqueResponses ur
on ur.id = cr.UniqueResponseID
inner join Items i
on i.id = ur.itemId
inner join AssignedItemMarks aim
on aim.UniqueResponseId = ur.id

left join UniqueResponseDocuments urd
on urd.UniqueResponseID = ur.id
left join UploadedFilesDetails  ufd
on ufd.UniqueResponseDocumentID = urd.ID

where  DATEDIFF(DAY,ur.insertionDate, cev.DateSubmitted) > 1
	and DATEADD(DAY, 100, cev.DateSubmitted) > GETDATE()
	order by DateSubmitted desc;


--Moderations.Comment
