UPDATE WAREHOUSE_ExamSessionTable
SET ExamStateChangeAuditXml.modify('replace value of (/exam/stateChange[newStateID=''10'']/information/stateChangeInformation/reason/text())[1] with ""')
WHERE ID IN  (

	SELECT 
		WAREHOUSE_ExamSessionTable.id
	FROM WAREHOUSE_ExamSessionTable

	WHERE PreviousExamState = 10
		and examStatechangeauditxml.exist('exam/stateChange[newStateID=10]/information/stateChangeInformation/reason/text()') = 1
		and ISNUMERIC(examStatechangeauditxml.value('data(exam/stateChange[newStateID=10]/information//reason/text())[1]','varchar(200)')) = 0
);


SELECT resultDataFull, resultData,StructureXML
FROM dbo.WAREHOUSE_ExamSessionTable
WHERE ID = 127501


select ID
	, a.b.value('@id','varchar(100)')
	, a.b.value('@markingType','decimal(6,3)')
	, StructureXML
from WAREHOUSE_ExamSessionTable
cross apply StructureXML.nodes('assessmentDetails/assessment/section/item') a(b)
where a.b.value('@markingType','varchar(10)') like '%.%'


