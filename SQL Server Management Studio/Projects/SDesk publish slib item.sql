SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED


SELECT DISTINCT Items.id [ItemID]
  --      , Items.ver
		--, ITEMS.ali
		--, SUBSTRING(Items.id, 0, CHARINDEX('P',Items.id)) [ProjectID]
FROM
(
      Select i.id, i.ver, i.moD, it.aLI
      from ItemGraphicTable it 
            INNER JOIN ComponentTable c ON c.ID = it.ParentID 
            INNER JOIN SceneTable s ON s.ID = c.ParentID 
            INNER JOIN PageTable i ON i.id = S.ParentID
      UNION
      Select i.id, i.ver, i.moD, it.aLI  
      from ItemVideoTable it 
            INNER JOIN ComponentTable c ON c.ID = it.ParentID 
            INNER JOIN SceneTable s ON s.ID = c.ParentID 
            INNER JOIN PageTable i ON i.id = S.ParentID
      UNION
      Select i.id, i.ver, i.moD, it.aLI  
            from ItemHotSpotTable it 
            INNER JOIN ComponentTable c ON c.ID = it.ParentID 
            INNER JOIN SceneTable s ON s.ID = c.ParentID 
            INNER JOIN PageTable i ON i.id = S.ParentID
      UNION
      Select i.id, i.ver, i.moD, it.aLI 
      from ItemCustomQuestionTable it 
            INNER JOIN ComponentTable c ON c.ID = it.ParentID 
            INNER JOIN SceneTable s ON s.ID = c.ParentID 
            INNER JOIN PageTable i ON i.id = S.ParentID
) AS Items

INNER JOIN PageTable as PT
on PT.ID = Items.ID and PT.ver = Items.ver   

INNER JOIN (

	SELECT 
		   PMT.ProjectId
			, PMT.name
			,[SLT].[itemID]
			, SLT.itemName
	FROM [dbo].[ProjectManifestTable]  PMT
	INNER JOIN  (
			SELECT ProjectId
				, a.b.value('@id','nvarchar(100)') [itemID]
				, a.b.value('@name','nvarchar(200)') [itemName]
			FROM (
			SELECT ProjectId
				,CAST(structureXML AS xml) structureXML
			FROM dbo.SharedLibraryTable 
			) X
		CROSS APPLY x.structureXML.nodes('sharedLibrary/item') a(b)
		WHERE a.b.value('@name','nvarchar(200)') = 'tableextensionrelease.swf'
		)	SLT
	ON SLT.ProjectId = PMT.ProjectId
		WHERE PMT.name = 'tableextensionrelease.swf'
) SLib
 ON SLib.ProjectId = SUBSTRING(Items.id, 0, CHARINDEX('P',Items.id))
	AND items.ali IN (SLib.itemID, SLib.itemName)