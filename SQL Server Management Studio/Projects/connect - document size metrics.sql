use connect_secureassess

--Exam States

	select a.DBName
		, ExamState
		, count(ID) [Count]
	FROM (
	select db_name() DBName
		, ID
		, examState
	from ExamSessionTable NOLOCK
	) a
	group by DBName,examState
	order by 1,2

--Table Size

	IF OBJECT_ID('tempdb..##ClientTableMetrics') IS NOT NULL DROP TABLE ##ClientTableMetrics;

	CREATE TABLE ##ClientTableMetrics (
		 table_name nvarchar(100)
		, rows int
		, reserved_kb nvarchar(20)
		, data_kb nvarchar(20)
		, index_size nvarchar(20)
		, unused_kb nvarchar(20)
	);

		declare @dynamic nvarchar(MAX) = '';

		select @dynamic +=CHAR(13) +
			'exec sp_spaceused @objname = '''+s.name+'.'+t.name + ''' '
		from sys.tables T
		inner join sys.schemas S
		on s.schema_id = t.schema_id
		where type = 'u'
		order by t.name;

		insert ##ClientTableMetrics
		exec(@dynamic)
				


		select sum((cast(replace([data_kb], 'KB','') as float) + cast(replace([index_size], 'KB','') as float)))/1024/1024 Whole_Database_GB
		from ##ClientTableMetrics


		--SELECT table_name
		--	, [rows]
		--	,cast(replace([data_kb], 'KB','') as float) [data_kb]
		--	,cast(replace([index_size], 'KB','') as float) [index_size]
		--FROM ##ClientTableMetrics
		--order by (cast(replace([data_kb], 'KB','') as float)+cast(replace([index_size], 'KB','') as float)) desc


		SELECT table_name
			, [rows]
			,cast(replace([data_kb], 'KB','') as float)/1024/1024 [Data_GB]
			--,cast(replace([index_size], 'KB','') as float) [index_size]
		FROM ##ClientTableMetrics
		where table_name = 'dbo.ExamSessionDocumentTable'



-- Document size

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	select sum(cast(datalength(esdt.Document) as bigint))/1024/1024--/1024
	from ExamSessionTable est 
	inner join ExamSessionDocumentTable esdt
	on est.id = esdt.examSessionId
	--inner join ExamStateChangeAuditTable escat
	--on escat.examSessionID = est.id
	where examState = 6
		--and newstate = 6
		--and stateChangeDate > '2018-05-09'