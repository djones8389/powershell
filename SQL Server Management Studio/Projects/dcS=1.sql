use master

IF OBJECT_ID('tempdb..##DATALENGTH') IS NOT NULL DROP TABLE ##DATALENGTH;

CREATE TABLE ##DATALENGTH (
	client nvarchar(1000)
	, ItemID nvarchar(20)
	,  ItemVersion smallint
);

DECLARE @dynamic nvarchar(MAX)='';

select @dynamic +=CHAR(13)+ '
use '+QUOTENAME([NAME])+'

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

INSERT ##DATALENGTH
select db_name()
	   , ItemID
       , ItemVersion
from CPAuditTable (READUNCOMMITTED)
where CAST(ItemXML as nvarchar(MAX)) like ''%dcS="1"%''
'
from sys.databases
where state_desc = 'ONLINE'
	and name like '%[_]SecureAssess';

exec(@dynamic);



select substring(client, 0, CHARINDEX('_',client)) client
	, ItemID
	, ItemVersion
from ##DATALENGTH
order by 1



select DecimalSeparator from Training_ContentAuthor..SiteSettings
select DecimalSeparator from Training_SurpassManagement..SiteSettings
select DecimalSeparator from Demo_ContentAuthor..SiteSettings
select DecimalSeparator from Demo_SurpassManagement..SiteSettings


