use master

IF OBJECT_ID('tempdb..##DATALENGTH') IS NOT NULL DROP TABLE ##DATALENGTH;

CREATE TABLE ##DATALENGTH (
	client nvarchar(1000)
	, ExamDeliverySystemStyleType  nvarchar(200)
);

DECLARE @dynamic nvarchar(MAX)='';

select @dynamic +=CHAR(13)+ '
use '+QUOTENAME([NAME])+'

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

INSERT ##DATALENGTH
select  db_name() Client
	, ExamDeliverySystemStyleType 
from AssessmentGroupTable (READUNCOMMITTED)
'
from sys.databases
where state_desc = 'ONLINE'
	and name like '%[_]ItemBank';

exec(@dynamic);



select distinct
	substring(client, 0, charindex('_',client))
	, ExamDeliverySystemStyleType
from ##DATALENGTH
order by 1