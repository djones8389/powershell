SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

SELECT 
	ExamSessionTable.ID AS [ExamSessionKey],
	CentreTable.CentreCode AS [CentreCode],
	CentreTable.CentreName AS [CentreName],
	CentreTable.ID AS [CentreKey],
	IB3QualificationLookup.QualificationRef AS [QualificationRef],
	IB3QualificationLookup.QualificationName AS [QualificationName],
	ScheduledExamsTable.ExamRef AS [ExamReference],
	ScheduledExamsTable.ExamName AS [ExamName],
	ScheduledExamsTable.examID AS [ExamKey],
	ScheduledExamsTable.purchaseOrder AS [Purchase Order],
	ExamSessionTable.KeyCode AS [Keycode],
	StartedTime.StateChangeDate AS [StartedDate],
	CompletedTime.StateChangeDate AS [CompletedDate],
	UserTable.CandidateRef AS [CandidateRef],
	UserTable.Surname AS [Surname],
	UserTable.Forename AS [Forename],
	NULL AS [Exported],
	CASE WHEN VoidedTime.ID IS NULL THEN 0 ELSE 1 END AS [Voided],
	VoidJustificationLookupTable.Name AS [VoidReason]
FROM dbo.ScheduledExamsTable
INNER JOIN dbo.IB3QualificationLookup
ON ScheduledExamsTable.QualificationID = IB3QualificationLookup.ID
INNER JOIN dbo.CentreTable
ON ScheduledExamsTable.CentreID = CentreTable.ID
INNER JOIN dbo.ExamSessionTable
ON ScheduledExamsTable.ID = ExamSessionTable.ScheduledExamID
INNER JOIN dbo.UserTable
ON ExamSessionTable.UserID = UserTable.ID
INNER JOIN dbo.ExamStateChangeAuditTable
ON ExamSessionTable.ID = ExamStateChangeAuditTable.ExamSessionID
AND ExamStateChangeAuditTable.NewState IN (@ExamStates)
AND CAST(ExamStateChangeAuditTable.StateChangeDate AS date) BETWEEN CAST(@StartDate AS date) AND CAST(@EndDate AS date)
LEFT JOIN dbo.ExamStateChangeAuditTable AS [StartedTime]
ON ExamSessionTable.ID = StartedTime.ExamSessionID
	AND StartedTime.NewState = 6
LEFT JOIN (
	SELECT COUNT(ExamStateChangeAuditTable.ID) AS [No],
		ExamStateChangeAuditTable.ExamSessionID,
		ExamStateChangeAuditTable.NewState,
		MIN(ExamStateChangeAuditTable.StateChangeDate) AS [StateChangeDate]
	FROM dbo.ExamStateChangeAuditTable
	WHERE ExamStateChangeAuditTable.NewState = 9
	GROUP BY ExamStateChangeAuditTable.ExamSessionID,
		ExamStateChangeAuditTable.NewState) AS [CompletedTime]
ON ExamSessionTable.ID = CompletedTime.ExamSessionID
LEFT JOIN dbo.ExamStateChangeAuditTable AS [VoidedTime]
ON ExamSessionTable.ID = VoidedTime.ExamSessionID
AND VoidedTime.NewState = 10
LEFT JOIN dbo.VoidJustificationLookupTable
ON VoidedTime.StateInformation.value('(stateChangeInformation/reason)[1]', 'int') = VoidJustificationLookupTable.ID
WHERE NOT EXISTS (SELECT 1 from ExamStateChangeAuditTable 
					where ExamStateChangeAuditTable.ExamSessionID = ExamSessionTable.ID
						and NewState = 13
						)

UNION

SELECT 
	WAREHOUSE_ExamSessionTable.ExamSessionID  AS [ExamSessionKey],
	WAREHOUSE_CentreTable.CentreCode AS [CentreCode],
	WAREHOUSE_CentreTable.CentreName AS [CentreName],
	WAREHOUSE_CentreTable.CentreID AS [CentreKey],
	IB3QualificationLookup.QualificationRef AS [QualificationRef],
	IB3QualificationLookup.QualificationName AS [QualificationName],
	WAREHOUSE_ExamSessionTable_Shreded.examRef AS [ExamReference],
	WAREHOUSE_ScheduledExamsTable.ExamName AS [ExamName],
	WAREHOUSE_ExamSessionTable_Shreded.examID AS [ExamKey],
	Warehouse_ScheduledExamsTable.purchaseOrder AS [Purchase Order],
	WAREHOUSE_ExamSessionTable.KeyCode AS [Keycode],
	WAREHOUSE_ExamSessionTable_Shreded.[started] AS [StartedDate],
	WAREHOUSE_ExamSessionTable.completionDate AS [completionDate],
	WAREHOUSE_UserTable.CandidateRef AS [CandidateRef],
	WAREHOUSE_UserTable.Surname AS [Surname],
	WAREHOUSE_UserTable.Forename AS [Forename],
	WAREHOUSE_ExamSessionTable.submissionExported AS [Exported],
	CASE WHEN VoidJustificationLookupTable.ID IS NULL THEN 0 ELSE 1 END AS [Voided],
	VoidJustificationLookupTable.Name AS [VoidReason]
FROM WAREHOUSE_ScheduledExamsTable
INNER JOIN dbo.IB3QualificationLookup
ON WAREHOUSE_ScheduledExamsTable.QualificationID = IB3QualificationLookup.ID
INNER JOIN WAREHOUSE_CentreTable
ON WAREHOUSE_CentreTable.ID = WAREHOUSE_ScheduledExamsTable.WAREHOUSECentreID
INNER JOIN WAREHOUSE_ExamSessionTable
on WAREHOUSE_ExamSessionTable.WAREHOUSEScheduledExamID = WAREHOUSE_ScheduledExamsTable.ID
INNER JOIN WAREHOUSE_UserTable
on WAREHOUSE_UserTable.ID = WAREHOUSE_ExamSessionTable.WAREHOUSEUserID
INNER JOIN WAREHOUSE_ExamSessionTable_Shreded
on WAREHOUSE_ExamSessionTable_Shreded.examSessionId = WAREHOUSE_ExamSessionTable.ID
LEFT JOIN dbo.VoidJustificationLookupTable 
ON VoidJustificationLookupTable.ID = WAREHOUSE_ExamSessionTable_Shreded.examStateInformation.value('(stateChangeInformation/reason)[1]', 'int')
where WAREHOUSE_ExamSessionTable.ID IN (@ExamSessionIDs)













SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

select ExamSessionID AS [ExamSessionID]
	, VoidJustificationLookupTable.name AS [VoidReason]
from ExamStateChangeAuditTable
LEFT JOIN VoidJustificationLookupTable
on VoidJustificationLookupTable.id = ExamStateChangeAuditTable.StateInformation.value('(stateChangeInformation[1]/reason[1])[1]', 'int')	
where NewState = 10
	and cast(StateInformation as nvarchar(MAX)) != ''

UNION

SELECT examsessionid AS [ExamSessionID]
	, VoidJustificationLookupTable.name AS [VoidReason]
FROM WAREHOUSE_ExamSessionTable_Shreded
LEFT JOIN VoidJustificationLookupTable
on VoidJustificationLookupTable.id = WAREHOUSE_ExamSessionTable_Shreded.examStateInformation.value('(stateChangeInformation[1]/reason[1])[1]', 'int')	
where examsessionid in (@examSessionids)