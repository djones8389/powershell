SET NOCOUNT ON;

IF EXISTS (
SELECT 1 from sys.configurations where Name = 'xp_cmdshell' and value_in_use = 0
)
	PRINT 'You need to enable CMDShell, or copy files manually!!'

IF (SELECT CONVERT(SYSNAME, SERVERPROPERTY('instancedefaultdatapath'))) IS NULL
	PRINT 'Default Data Path isn''t defined, set this in the variable manually'
	
IF (SELECT CONVERT(SYSNAME, SERVERPROPERTY('instancedefaultlogpath'))) IS NULL
	PRINT 'Default Log Path isn''t defined, set this in the variable manually'

DECLARE @SQL NVARCHAR(MAX) = ''
	  , @OnlineOffline NVARCHAR(MAX) = '';

DECLARE @DefaultDataLoc NVARCHAR(1000) = CONVERT(SYSNAME, SERVERPROPERTY('instancedefaultdatapath'))
	  ,  @DefaultLogLoc NVARCHAR(1000) = CONVERT(SYSNAME, SERVERPROPERTY('instancedefaultlogpath'));

--SELECT @DefaultDataLoc = 'E:\Data\'
--SELECT @DefaultLogLoc = 'E:\Log\'


DECLARE @Tables TABLE(databaseName nvarchar(100), [LogicalName] nvarchar(250), [CurrentLocation] nvarchar(1000), [NewLocation] nvarchar(1000))
INSERT @Tables
SELECT D.[Name] [DatabaseName]
	  , F.Name  [LogicalName]
	  , F.physical_name [CurrentLocation]
	  ,  case f.type_desc when 'ROWS' then   @DefaultDataLoc + SUBSTRING(F.physical_name,LEN(REVERSE(SUBSTRING(REVERSE(F.physical_name), CHARINDEX('\',REVERSE(F.physical_name)), LEN(F.physical_name))))+1,LEN(F.physical_name))
					 when 'LOG' then   @DefaultLogLoc + SUBSTRING(F.physical_name,LEN(REVERSE(SUBSTRING(REVERSE(F.physical_name), CHARINDEX('\',REVERSE(F.physical_name)), LEN(F.physical_name))))+1,LEN(F.physical_name))
	END as [NewLocation]
FROM sys.master_files as F
inner join (
	SELECT database_id, [Name]
	FROM sys.databases
	) as D
on D.database_id = F.database_id 
where D.[Name] in  ('tempdb');

--DECLARE myCursor CURSOR FOR
Select [DatabaseName]
	, [LogicalName]
	, [CurrentLocation]
	, [NewLocation]
	, case 
		when RIGHT([CurrentLocation],3)  in ('mdf','ndf')
		then  'ALTER DATABASE ' + QUOTENAME (databaseName) + ' SET OFFLINE; ALTER DATABASE ' + QUOTENAME (databaseName) + ' MODIFY FILE (NAME = ''' + [LogicalName] + ''',' + 'Filename = ''' + NewLocation + ''');'
				+ ' EXECUTE xp_cmdshell ''' + 'move /Y ' + CurrentLocation + '  ' +  NewLocation + ''''
	    when RIGHT([CurrentLocation],3)  in ('ldf')
				then 'ALTER DATABASE ' + QUOTENAME (databaseName) + ' MODIFY FILE (NAME = ''' + [LogicalName] + ''',' + 'Filename = ''' + NewLocation + ''');'
				+ ' EXECUTE xp_cmdshell ''' + 'move /Y ' + CurrentLocation + '  ' +  NewLocation + '''' +  'ALTER DATABASE ' + QUOTENAME (databaseName) + ' SET ONLINE;'
		end as [statement]
from @Tables
order by [DatabaseName], RIGHT([CurrentLocation],3) desc;



/*
C:\Program Files\Microsoft SQL Server\MSSQL13.SQL2016\MSSQL\DATA\TempDB\tempdb_mssql_2.ndf
C:\Program Files\Microsoft SQL Server\MSSQL13.SQL2016\MSSQL\DATA\TempDB\tempdb_mssql_3.ndf
C:\Program Files\Microsoft SQL Server\MSSQL13.SQL2016\MSSQL\DATA\TempDB\tempdb_mssql_4.ndf
C:\Program Files\Microsoft SQL Server\MSSQL13.SQL2016\MSSQL\DATA\TempDB\tempdb.mdf
C:\Program Files\Microsoft SQL Server\MSSQL13.SQL2016\MSSQL\DATA\TempDB\templog.ldf
*/


--ALTER DATABASE [tempdb] MODIFY FILE  (NAME = 'temp1',Filename = 'D:\Data\SQL2016TempDB\tempdb_mssql_2.ndf'); --EXECUTE xp_cmdshell 'move /Y C:\Program Files\Microsoft SQL Server\MSSQL13.SQL2016\MSSQL\DATA\TempDB\tempdb_mssql_2.ndf  D:\Data\SQL2016TempDB\tempdb_mssql_2.ndf'
--ALTER DATABASE [tempdb] MODIFY FILE  (NAME = 'temp2',Filename = 'D:\Data\SQL2016TempDB\tempdb_mssql_3.ndf'); --EXECUTE xp_cmdshell 'move /Y C:\Program Files\Microsoft SQL Server\MSSQL13.SQL2016\MSSQL\DATA\TempDB\tempdb_mssql_3.ndf  D:\Data\SQL2016TempDB\tempdb_mssql_3.ndf'
--ALTER DATABASE [tempdb] MODIFY FILE  (NAME = 'temp3',Filename = 'D:\Data\SQL2016TempDB\tempdb_mssql_4.ndf'); --EXECUTE xp_cmdshell 'move /Y C:\Program Files\Microsoft SQL Server\MSSQL13.SQL2016\MSSQL\DATA\TempDB\tempdb_mssql_4.ndf  D:\Data\SQL2016TempDB\tempdb_mssql_4.ndf'
--ALTER DATABASE [tempdb] MODIFY FILE  (NAME = 'temp0',Filename = 'D:\Data\SQL2016TempDB\tempdb.mdf'); --EXECUTE xp_cmdshell 'move /Y C:\Program Files\Microsoft SQL Server\MSSQL13.SQL2016\MSSQL\DATA\TempDB\tempdb.mdf  D:\Data\SQL2016TempDB\tempdb.mdf'
--ALTER DATABASE [tempdb] MODIFY FILE (NAME = 'templog',Filename = 'D:\Log\templog.ldf'); --EXECUTE xp_cmdshell 'move /Y C:\Program Files\Microsoft SQL Server\MSSQL13.SQL2016\MSSQL\DATA\TempDB\templog.ldf  D:\Log\templog.ldf'ALTER DATABASE [tempdb] SET ONLINE;