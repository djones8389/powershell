RESTORE DATABASE [SANDBOX_SkillsFirst_SecureAssess] 
	FROM DISK = N'T:\FTP\Skillsfirst\SecureAssess.bak' WITH FILE = 1
	, MOVE 'SkillsFirst_SecureAssess_9.0' TO N'S:\DATA\SANDBOX_SkillsFirst_SecureAssess.mdf'
	, MOVE 'SkillsFirst_SecureAssess_9.0_log' TO N'L:\LOGS\SANDBOX_SkillsFirst_SecureAssess.ldf', STATS = 5;
GO
ALTER DATABASE [SANDBOX_SkillsFirst_SecureAssess] SET RECOVERY SIMPLE
GO
USE [SANDBOX_SkillsFirst_SecureAssess]
GO
DBCC SHRINKFILE (N'SkillsFirst_SecureAssess_9.0' , 61585)
GO
USE [SANDBOX_SkillsFirst_SecureAssess]
GO
checkpoint;
GO
DBCC SHRINKFILE (N'SkillsFirst_SecureAssess_9.0_log' , 100)
GO
