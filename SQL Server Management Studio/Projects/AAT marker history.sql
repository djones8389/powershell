--select top 100 examSessionId
--from WAREHOUSE_ExamSessionTable_Shreded
--order by 1 desc

SELECT	CASE WHEN wests.WarehouseExamState = 4 THEN 'Result Pending From SecureMarker'
			 ELSE weslt.Description
		END AS WarehouseState,
		wests.examSessionid,
		wests.CentreName,
		wests.QualificationName,
		wests.examName AS AssessmentName,
		wests.examVersionRef AS AssessmentVersionRef,
		wests.KeyCode,
		wests.Forename,
		wests.surName,
		wests.candidateRef AS MembershipID,
		CASE WHEN wests.originalGrade = 'Pass' THEN 'Competent'
			 WHEN wests.originalGrade = 'Fail' THEN 'Not Yet Competent'
			 ELSE wests.originalGrade
		END AS OriginalResult,
		CASE WHEN wests.adjustedGrade = 'Pass' THEN 'Competent'
			 WHEN wests.adjustedGrade = 'Fail' THEN 'Not Yet Competent'
			 ELSE wests.adjustedGrade
		END AS AdjustedResult,
		( wests.userMark / ( CASE wests.totalMark
							   WHEN 0 THEN 1
							   ELSE wests.totalMark
							 END ) * 100 ) AS Percentage,
		wests.userMark AS Mark,
		CAST(wests.submittedDate AS DATE) AS CompletedDate,
		CASE WHEN wests.started = CONVERT(SMALLDATETIME,'1 Jan 1900 00:00:00') THEN CAST(wests.submittedDate AS DATE)
			 ELSE CAST(wests.started AS DATE)
		END AS AssessmentDate,
		westsi.ItemRef AS ItemID,
		westsi.ItemName,
		westsi.TotalMark AS MarkAvailable,
		CASE WHEN westsi.markerUserMark = '' THEN westsi.TotalMark * westsi.responsexml.value('(/p/@um)[1]','FLOAT')
			 ELSE westsi.markerUserMark
		END AS MarkScored,
		CASE WHEN MarkerResponseData.value('data(entries[1]/entry[last()]/userId)[1]','int') = -2
			 THEN MarkerResponseData.value('data(entries[1]/entry[last()]/userName)[1]','nvarchar(50)')
			 ELSE MarkerResponseData.value('data(entries[1]/entry[last()]/userForename)[1]','nvarchar(100)') + ' '
				  + MarkerResponseData.value('data(entries[1]/entry[last()]/userSurname)[1]','nvarchar(100)')
		END AS MarkerName
		,wesirt.MarkerResponseData
FROM	dbo.WAREHOUSE_ExamSessionTable_Shreded wests
		INNER JOIN dbo.WAREHOUSE_ExamStateLookupTable weslt ON weslt.ID = wests.WarehouseExamState
		LEFT OUTER JOIN dbo.WAREHOUSE_ExamSessionTable_ShrededItems westsi ON westsi.examSessionId = wests.examSessionId
		LEFT OUTER JOIN dbo.WAREHOUSE_ExamSessionItemResponseTable wesirt ON wesirt.WAREHOUSEExamSessionID = wests.examSessionId
																			 AND wesirt.ItemID = westsi.ItemRef
WHERE wests.examSessionid in (2274838,2274840) --2274845,2274844,2274843,2274842,2274841,2274840,2274839,2274838,2274837,2274836,2274835,2274834,2274833,2274832,2274831,2274830,2274829,2274828,2274827,2274826,2274825,2274824,2274823,2274822,2274821,2274820,2274819,2274818,2274817,2274816,2274815,2274814,2274813,2274812,2274811,2274810,2274809,2274808,2274807,2274806,2274805,2274804,2274803,2274802,2274801,2274800,2274799,2274798,2274797,2274796,2274795,2274794,2274793,2274792,2274791,2274790,2274789,2274788,2274787,2274786,2274785,2274784,2274783,2274782,2274781,2274780,2274779,2274778,2274777,2274776,2274775,2274774,2274773,2274772,2274771,2274770,2274769,2274768,2274767,2274766,2274765,2274764,2274763,2274762,2274761,2274760,2274759,2274758,2274757,2274756,2274755,2274754,2274753,2274752,2274751,2274750,2274749,2274748,2274747,2274746)
--WHERE	CAST(wests.submittedDate AS DATE) &gt;= @StartDate
--		AND CAST(wests.submittedDate AS DATE) &lt;= @EndDate
--		AND wests.WarehouseExamState IN ( 1,2,3,4,5,6 );