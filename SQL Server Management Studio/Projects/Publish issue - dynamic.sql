use master

if OBJECT_ID('tempdb..#results') is not null drop table #results;

create table #results (

	client nvarchar(100)
	, subject nvarchar(100)
	, itemid nvarchar(100)
	, itemName nvarchar(100)
	, ContentAuthorVersion smallint
	, TestCreationVersion smallint
	, [URL] nvarchar(1000)
)

declare @dynamic nvarchar(MAX) = ''

select @dynamic +=CHAR(13) + 
	'
INSERT #results
SELECT '''+name+''' [Client]
	, S.Title [Subject]
       , i.id [ItemID]
       , i.Name [ItemName]
       , I.Version [ContentAuthor Version]
       , IT.Version [TestCreation Version]
	   , ''https://''+'''+name+'.surpass.com/#ItemAuthoring/Subject/''+cast(S.id as varchar(10))+''/Item/Edit/''+cast(i.id  as varchar(10))+ '''' [URL]
FROM ['+name+'_ContentAuthor].[dbo].[Subjects] S
INNER JOIN ['+name+'_ContentAuthor].[dbo].[Items]  I
ON I.SubjectId = S.Id
INNER JOIN (
       SELECT ProjectID
              , ItemRef
              , MAX(Version) [Version]
       FROM '+name+'_ItemBank.[dbo].[ItemTable] 
       group by ProjectID
              , ItemRef
       ) IT
on IT.ProjectID = S.ProjectId
       and IT.ItemRef = I.id
where I.[Status] = 3               /*  Live Items  */                       
      and I.Version > IT.Version

	  '
from (
	select distinct B.name
	FROM (
		select SUBSTRING(name, 0, charindex('_',name)) [Name]
			, ROW_NUMBER() OVER(PARTITION BY SUBSTRING(name, 0, charindex('_',name)) ORDER BY SUBSTRING(name, 0, charindex('_',name)) ) R
		from sys.databases
		where state_desc = 'ONLINE'
			and [Name] like '%/_%' ESCAPE '/'
			and name not like 'test%'
		) B
	where R > 3
) a


exec(@dynamic) ;


select *
from #results
order by 1;