USE CONNECT_SecureAssess

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

if OBJECT_ID('tempdb..#WHDocumentsToDelete') is not null 
	drop table #WHDocumentsToDelete;

--43 mins
SELECT A.ID
INTO #WHDocumentsToDelete
FROM (
select ID
, ROW_NUMBER() OVER(
		PARTITION BY warehouseExamSessionID
		 , itemId
		 , documentName
		 , CHECKSUM(cast(Document as varbinary(MAX)))
		 ORDER BY uploadDate desc
		 ) R
from [WAREHOUSE_ExamSessionDocumentTable] (READUNCOMMITTED)
) A
where [R] > 1

create clustered index [IX_ID] on #WHDocumentsToDelete (ID);

SELECT cast(SUM(cast(DATALENGTH(Document) as bigint))/1024/1024 as bigint) [Mb]
FROM [WAREHOUSE_ExamSessionDocumentTable] A(READUNCOMMITTED)
INNER JOIN #WHDocumentsToDelete B
on A.ID = b.ID;





	--;with sixteenth as (
	--select examSessionID
	--	,  datalength(document) DL
	--from examsessiondocumenttable (READUNCOMMITTED)
	--where uploaddate between '2018-05-16 00:00:00' and '2018-05-16 23:59:59'

	--union

	--select wesdt.warehouseexamsessionid
	--	,  datalength(document) DL
	--from warehouse_Examsessiondocumenttable wesdt (READUNCOMMITTED)
	--where uploaddate between '2018-05-16 00:00:00' and '2018-05-16 23:59:59'
	--)

	--select avg(cast(DL as bigint))/1024 [kb]--/1024/1024
	--	--, examSessionID
	--from sixteenth
	----group by examSessionID


	--;with seventeenth as (
	--select examSessionID
	--	,  datalength(document) DL
	--from examsessiondocumenttable (READUNCOMMITTED)
	--where uploaddate between '2018-05-17 00:00:00' and '2018-05-17 23:59:59'

	--union

	--select wesdt.warehouseexamsessionid
	--	,  datalength(document) DL
	--from warehouse_Examsessiondocumenttable wesdt (READUNCOMMITTED)
	--where uploaddate between '2018-05-17 00:00:00' and '2018-05-17 23:59:59'
	--)

	--select avg(cast(DL as bigint))/1024 [kb]--/1024/1024
	--	--, examSessionID
	--from seventeenth
	----group by examSessionID