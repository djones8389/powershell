DECLARE @table table (
	xml xml
)

DECLARE @XML XML = '<ParameterValues>
	<ParameterValue>
		<Name>TO</Name>
		<Value>Bernadette.craven@northland.ac.uk</Value>
	</ParameterValue>
	<ParameterValue>
		<Name>IncludeReport</Name>
		<Value>True</Value>
	</ParameterValue>
	<ParameterValue>
		<Name>RenderFormat</Name>
		<Value>CSV</Value>
	</ParameterValue>
	<ParameterValue>
		<Name>Subject</Name>
		<Value>Assessments awaiting marking in SecureAssess</Value>
	</ParameterValue>
	<ParameterValue>
		<Name>Comment</Name>
		<Value>Dear colleague  In order to support you with your SecureAssess administration, we will send you a weekly report which will contain a list of the current assessments waiting to be marked at your centre.   This will allow you to assign staff workloads and monitor assessing times. Please share these reports with other members of your team.  As SecureAssess is a live system, details listed on the report may have changed but they will still give you an indication of the amount of assessments still to be assessed and verified.  SecureAssess support  Did you know we have added a range of support materials for using SecureAssess? From troubleshooting guides to instructional videos, all this and more can be found at www.aat.org.uk/cba.  Kind regards,  Centre Support</Value>
	</ParameterValue>
	<ParameterValue>
		<Name>IncludeLink</Name>
		<Value>False</Value>
	</ParameterValue>
	<ParameterValue>
		<Name>Priority</Name>
		<Value>NORMAL</Value>
	</ParameterValue>
</ParameterValues>'

INSERT @table
SELECT @XML

update @table
set xml.modify('replace value of (/ParameterValues/ParameterValue/Value/text())[1] with ("dave.jones@btl.com")')

select *
from @table

create myCursor cursor for
select subscriptionid
, ExtensionSettings

declare

OPEN myCursor

FETCH NEXT FROM myCursor INTO 

WHILE @@FETCH_STATUS = 0

BEGIN



FETCH NEXT FROM myCursor INTO 

END
CLOSE myCursor
DEALLOCATE myCursor