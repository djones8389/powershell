IF OBJECT_ID('tempdb..#XMLHolder') IS NOT NULL DROP TABLE #XMLHolder;

CREATE TABLE #XMLHolder  (
	myXML XML
);

DECLARE @myXML XML=

  '<assessment currentSection="1" totalTime="0" currentTime="0">
    <intro id="0" name="Introduction" currentItem="">
      <item id="445P1384" name="Page 1" totalMark="1" version="6" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="445P1386" name="Page 2" totalMark="1" version="7" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="445P1387" name="Page 3" totalMark="1" version="8" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="445P1385" name="Page 4" totalMark="1" version="7" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
    </intro>
    <outro id="0" name="Finish" currentItem="" />
    <tools id="0" name="Tools" currentItem="" />
    <section id="1" name="Section 1" passLevelValue="0" passLevelType="0" itemsToMark="0" currentItem="716P936" fixed="1">
      <item id="716P942" name="L1VAIntroV2" totalMark="0" version="17" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="20654" flagged="0" type="3" LO="For internal use" Unit="For internal use" />
      <item id="716P940" name="LVAQ01RECRUITMENTV2" totalMark="2" version="20" markingType="1" markingState="0" userMark="0" markerUserMark="" userAttempted="1" viewingTime="100370" flagged="0" type="1" LO="Web browser" Unit="3748-924 FS ICT In-app Level 1" quT="11" />
      <item id="716P939" name="LVAQ02RECRUITMENTV2" totalMark="1" version="29" markingType="0" markingState="0" userMark="1" markerUserMark="" userAttempted="1" viewingTime="27924" flagged="0" type="1" LO="Web browser" Unit="3748-924 FS ICT In-app Level 1" quT="10" />
      <item id="716P938" name="LVAQ03RECRUITMENTV2" totalMark="1" version="36" markingType="1" markingState="0" userMark="0" markerUserMark="" userAttempted="1" viewingTime="2678395" flagged="0" type="1" LO="Web browser" Unit="3748-924 FS ICT In-app Level 1" quT="22" />
      <item id="716P937" name="LVAQ04RECRUITMENTV2" totalMark="3" version="47" markingType="1" markingState="0" userMark="0" markerUserMark="" userAttempted="1" viewingTime="272314" flagged="0" type="1" LO="E-mail" Unit="3748-924 FS ICT In-app Level 1" quT="20" />
      <item id="716P936" name="LVAQ05RECRUITMENTV2" totalMark="13" version="36" markingType="1" markingState="0" userMark="0" markerUserMark="" userAttempted="1" viewingTime="5470722" flagged="0" type="1" LO="Spreadsheet" Unit="3748-924 FS ICT In-app Level 1" quT="22" />
      <item id="716P935" name="LVAQ06RECRUITMENTV2" totalMark="15" version="34" markingType="1" markingState="0" userMark="0" markerUserMark="" userAttempted="1" viewingTime="513085" flagged="0" type="1" LO="Presentation" Unit="3748-924 FS ICT In-app Level 1" quT="22" />
      <item id="716P934" name="LVAQ07RECRUITMENTV2" totalMark="3" version="35" markingType="1" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="236278" flagged="0" type="1" LO="File management" Unit="3748-924 FS ICT In-app Level 1" quT="22" />
      <item id="716P933" name="LVAQ08RECRUITMENTV2" totalMark="1" version="41" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="0" flagged="0" type="1" LO="Knowledge and understanding" Unit="3748-924 FS ICT In-app Level 1" quT="10" />
      <item id="716P932" name="LVAQ09RECRUITMENTV2" totalMark="1" version="42" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="0" flagged="0" type="1" LO="Knowledge and understanding" Unit="3748-924 FS ICT In-app Level 1" quT="10" />
      <item id="716P941" name="L1VAendV2" totalMark="0" version="15" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="0" flagged="0" type="3" LO="For internal use" Unit="For internal use" />
    </section>
	</assessment>';

INSERT #XMLHolder
SELECT @myXML;

SELECT A.viewingTime/1000 as Seconds
,	 A.viewingTime/1000/60 as Minutes
,	 A.viewingTime

FROM (
select SUM(a.b.value('@viewingTime','int'))   as viewingTime
from #XMLHolder
cross apply myXML.nodes('assessment/section/item') a(b)
) A