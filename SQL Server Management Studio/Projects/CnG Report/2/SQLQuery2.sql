USE PRV_EVolve_itembank

IF EXISTS  (select 1 from sys.objects where type = 'v' and name = 'vw_MCQReport_FV')
	DROP VIEW [vw_MCQReport_FV];
GO
CREATE VIEW [vw_MCQReport_FV] 
AS

SELECT cast(ProjectID as nvarchar(10)) + 'P' + cast(ItemID as nvarchar(10)) [ItemID]
	, ItemUsageCount
	, FacilityValue
from ItemStatsTable
GO



IF EXISTS (select 1 from sys.objects where type = 'v' and name = 'vw_MCQReport_Items')
	DROP VIEW [vw_MCQReport_Items];
GO
CREATE VIEW [vw_MCQReport_Items] 
AS
--DECLARE @Prefix smallint = '6008';
--DECLARE @Suffix nvarchar(500) = '601,602,603,604,605,606,608,609,611,614,615,616,617,690,701,702,703,705,706,709,710,711,790';

--INSERT @ItemIDs(ItemID)
SELECT REPLACE(REPLACE(cast(a.b.query('.') as NVARCHAR(MAX)), '<XPath>@ID="',''), '"</XPath>','') ItemID
FROM AssessmentTable AT
		
CROSS APPLY AssessmentRules.nodes('PaperRules//XPath') a(b)

WHERE LEFT(a.b.value('.','nvarchar(MAX)'), 3) = '@ID'
	AND AT.ID IN (
			SELECT AT.ID
			FROM [dbo].[AssessmentGroupTable] AGT
			INNER JOIN AssessmentTable AT
			ON AGT.ID = AT.AssessmentGroupID
			where substring(Name, CHARINDEX('-',Name)+1,3) IN (

			601,602,603,604,605,606,608,609,611,614,615,616,617,690,701,702,703,705,706,709,710,711,790
				--SELECT [Value]
				--FROM [dbo].[ParmsToList](601,602,603,604,605,606,608,609,611,614,615,616,617,690,701,702,703,705,706,709,710,711,790)
			)
			AND  [Name] like '%' + cast(6008 as nvarchar(4)) + '%'
		)

GO

select *
from [vw_MCQReport_Items]

select *
from [vw_MCQReport_FV]



