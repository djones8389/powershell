--select
--		id
--		, cast(itemvalue as xml).value('data(ItemValue/metaData/HISTORY/USER [last()]/checkedInCmt)[1]','nvarchar(1000)')
--from PageTable
--where id = '344P1000'


DECLARE @XML XML = '
<ItemValue>
  <metaData>
    <HISTORY>
      <USER>
        <userName>ashah</userName>
        <userId>798</userId>
        <clientDataTime>08/02/2012 14:31:33</clientDataTime>
        <serverDataTime>08/02/2012 14:31:32</serverDataTime>
        <checkedInStatus>Release</checkedInStatus>
        <checkedInCmt />
        <attributesSet />
      </USER>
      <USER>
        <userName>barryporter</userName>
        <userId>797</userId>
        <clientDataTime>09/02/2012 11:17:25</clientDataTime>
        <serverDataTime>09/02/2012 11:17:25</serverDataTime>
        <checkedInStatus>Gold 3</checkedInStatus>
        <checkedInCmt />
        <attributesSet />
      </USER>
      <USER>
        <userName>barryporter</userName>
        <userId>797</userId>
        <clientDataTime>16/02/2012 17:29:04</clientDataTime>
        <serverDataTime>16/02/2012 17:29:04</serverDataTime>
        <checkedInStatus>Release</checkedInStatus>
        <checkedInCmt />
        <attributesSet />
      </USER>
      <USER>
        <userName>ashah</userName>
        <userId>798</userId>
        <clientDataTime>27/02/2012 17:25:26</clientDataTime>
        <serverDataTime>27/02/2012 17:25:25</serverDataTime>
        <checkedInCmt>Hello</checkedInCmt>
        <attributesSet />
      </USER>
      <USER>
        <userName>eleanora</userName>
        <userId>664</userId>
        <clientDataTime>08/06/2012 11:46:00</clientDataTime>
        <serverDataTime>08/06/2012 11:46:00</serverDataTime>
        <checkedInCmt />
        <attributesSet />
      </USER>
    </HISTORY>
  </metaData>
</ItemValue>'

--select b.c.query('.')
--from @XML A
--cross apply ('ItemValue/metaData/HISTORY/USER') b(c)

SELECT mm.m.value('(text())[1]', 'varchar(100)') 		--Same thing, different syntax
	,mm.m.value('(.)[1]','varchar(100)')
FROM (VALUES (@XML)) xx(x)
CROSS APPLY xx.x.nodes('ItemValue/metaData/HISTORY/USER/checkedInCmt') mm(m);

select LastComment.ID
	, LastComment.checkedInCmt
FROM (
	select ID
		, checkedInCmt
		,MAX(R) R
	FROM (
		select ID
			, ROW_NUMBER() OVER(PARTITION BY ID ORDER  BY ID) R
			, c.d.value('.','nvarchar(500)') checkedInCmt
		FROM (
		select top 10 ID
			, cast(itemvalue as xml) itemvalue
		from PageTable
		where id = '717P3106'
		) A
		cross apply itemvalue.nodes('ItemValue/metaData/HISTORY/USER/checkedInCmt') c(d)
	) b
	group by ID,checkedInCmt
) LastComment





