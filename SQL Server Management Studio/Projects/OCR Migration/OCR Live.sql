--1   Kill SPIDs

DECLARE @KillSpids nvarchar(MAX) = ''

SELECT @KillSpids +=CHAR(13) + 'kill ' + convert(varchar(5),spid)
from sys.sysprocesses s
where dbid = DB_ID()
	and spid != @@SPID
	
EXEC(@KillSpids);


--2  Backup

DECLARE @Location nvarchar(100) = 'T:\Backup'
DECLARE @BackupDB nvarchar(MAX) = ''

SELECT @BackupDB += N'BACKUP DATABASE ['+name+N'] TO DISK = N''' +  @Location + '\' + NAME + '.bak'' WITH NAME = N'''+name+N'- Database Backup'', COPY_ONLY, NOFORMAT, NOINIT, SKIP, COMPRESSION, NOREWIND, NOUNLOAD, STATS = 10, MEDIAPASSWORD = ''bKyrfe3k7rDO4KDLwFeBP6sM0eqXQiu3'';'  
FROM sys.databases 
where database_id = db_id()

EXEC(@BackupDB)


-- 3  RestoreVerifyOnly

DECLARE @Location nvarchar(100) = 'T:\Backup'
DECLARE @RestoreVerifyOnly nvarchar(MAX) = ''

SELECT @RestoreVerifyOnly += N'RESTORE VERIFYONLY FROM DISK = N''' +  @Location + '\' + NAME + '.bak'' WITH MEDIAPASSWORD = ''bKyrfe3k7rDO4KDLwFeBP6sM0eqXQiu3'';'  
FROM sys.databases 
where database_id = db_id()

EXEC(@RestoreVerifyOnly)