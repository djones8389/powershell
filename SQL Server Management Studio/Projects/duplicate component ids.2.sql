SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

use PRV_WJEC_SecureAssess

select E.*
FROM (

select ItemID
	, WAREHOUSEExamSessionID
	, a.b.value('@id','tinyint') S
	, c.d.value('@id','tinyint') C
--, itemresponsedata
from WAREHOUSE_ExamSessionItemResponseTable
cross apply itemresponsedata.nodes('p/s') a(b)
cross apply a.b.nodes('c') c(d)

--where WAREHOUSEExamSessionID = 10402
--	and itemid = '818P1343'

) E
group by 
	ItemID,WAREHOUSEExamSessionID, S, C
	having count(*) > 1



/*
select ItemID
--, ItemResponseData
, a.b.value('@id','tinyint') S
, c.d.value('@id','tinyint') C
from WAREHOUSE_ExamSessionItemResponseTable
cross apply itemresponsedata.nodes('p/s') a(b)
cross apply a.b.nodes('c') c(d)

where WAREHOUSEExamSessionID = 162594
	and itemid = '349P548'
order by 1 desc
*/