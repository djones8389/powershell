--DROP DATABASE [SANDBOX_PRV_OCR_ASPState]
--DROP DATABASE [SANDBOX_PRV_SQA_BTLSurpassAudit]
--DROP DATABASE [SANDBOX_SHA_QEYADAH_CP_grfrggfg]

DROP DATABASE SANDBOX_Test
DROP DATABASE SANDBOX_Test1
--RESTORE HEADERONLY FROM DISK = 'T:\Backup\PRV.OCR.ALL.2016.04.04.bak'

use msdb

DECLARE @DropJobs nvarchar(MAX) = '';

select @DropJobs += CHAR(13) +
	  'EXEC sp_delete_job @job_name =''' + name + ''';'
from dbo.sysjobs
where name like '%Remove%'

EXEC(@DropJobs);

DECLARE @DropLogin nvarchar(MAX) = '';

select @DropLogin += CHAR(13) +'DROP LOGIN ' + QUOTENAME(NAME) + ';'
from sys.sql_logins
where name like 'Dave.Jones%'

EXEC(@DropLogin);
