use master	

declare @kill nvarchar(max)='';
select @kill +=CHAR(13)+ 'KILL ' + convert(varchar(3),s.spid)
from sys.databases D
INNER JOIN sys.sysprocesses S
on D.database_id = s.dbid
where d.name = 'SANDBOX_AQA_SecureAssess'

exec(@kill)

--RESTORE DATABASE [SANDBOX_AQA_CPProjectAdmin] FROM DISK = N'T:\FTP\AQA\AQATraining.2016.10.13.bak' WITH FILE = 1, NOUNLOAD, NOREWIND, REPLACE, MEDIAPASSWORD = 's_!85WrenukEtrut+4f5a#-a&ac*aKUs', STATS = 10, MOVE 'AQA_CPProjectAdmin_11.1' TO N'S:\DATA\SANDBOX_AQA_CPProjectAdmin.mdf', MOVE 'AQA_CPProjectAdmin_11.1_log' TO N'L:\LOGS\SANDBOX_AQA_CPProjectAdmin.ldf'; 
--RESTORE DATABASE [SANDBOX_AQA_Itembank] FROM DISK = N'T:\FTP\AQA\AQATraining.2016.10.13.bak' WITH FILE = 2, NOUNLOAD, NOREWIND, REPLACE, MEDIAPASSWORD = 's_!85WrenukEtrut+4f5a#-a&ac*aKUs', STATS = 10, MOVE 'Itembank7.1' TO N'S:\DATA\SANDBOX_AQA_Itembank.mdf', MOVE 'Itembank7.1_log' TO N'L:\LOGS\SANDBOX_AQA_Itembank.ldf'; 
RESTORE DATABASE [SANDBOX_AQA_SecureAssess] FROM DISK = N'T:\FTP\AQA\AQATraining.2016.10.13.bak' WITH FILE = 3, NOUNLOAD, NOREWIND, REPLACE, MEDIAPASSWORD = 's_!85WrenukEtrut+4f5a#-a&ac*aKUs', STATS = 10, MOVE 'SecureAssess7.1' TO N'S:\DATA\SANDBOX_AQA_SecureAssess.mdf', MOVE 'SecureAssess7.1_log' TO N'L:\LOGS\SANDBOX_AQA_SecureAssess.ldf'; 
--RESTORE DATABASE [SANDBOX_AQA_SecureMarker_10.0] FROM DISK = N'T:\FTP\AQA\AQATraining.2016.10.13.bak' WITH FILE = 4, NOUNLOAD, NOREWIND, REPLACE, MEDIAPASSWORD = 's_!85WrenukEtrut+4f5a#-a&ac*aKUs', STATS = 10, MOVE 'AQA_SecureMarker_10.0' TO N'S:\DATA\SANDBOX_AQA_SecureMarker_10.0.mdf', MOVE 'AQA_SecureMarker_10.0_log' TO N'L:\LOGS\SANDBOX_AQA_SecureMarker_10.0.ldf'; 

USE [SANDBOX_AQA_SecureAssess]; 

	IF NOT EXISTS(SELECT 1 FROM sys.database_principals WHERE name = N'Powershell')
		CREATE USER [Powershell] FOR LOGIN [Powershell]

EXEC sp_addrolemember N'db_owner', N'Powershell'

--exec msdb..sp_start_job @job_name = 'Grant dbo for Admin Group'

