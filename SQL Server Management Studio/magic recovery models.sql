select name, recovery_model_desc
	, 'use ' + quotename(name) + '; checkpoint; use [master]; alter database ' + quotename(name) + ' set recovery simple with no_wait;'
	, 'use ' + quotename(name) + 'alter database ' + quotename(name) + ' set recovery full with no_wait;'
from sys.databases
where database_id > 4
	and recovery_model_desc = 'FULL'

 
 
/*
--set simple

use [MAGXXXXX]; checkpoint; use [master]; alter database [MAGXXXXX] set recovery simple with no_wait;
use [MAGWW]; checkpoint; use [master]; alter database [MAGWW] set recovery simple with no_wait;
use [MAGUT]; checkpoint; use [master]; alter database [MAGUT] set recovery simple with no_wait;
use [MAGUR]; checkpoint; use [master]; alter database [MAGUR] set recovery simple with no_wait;
use [MAGUP]; checkpoint; use [master]; alter database [MAGUP] set recovery simple with no_wait;
use [MAGUM]; checkpoint; use [master]; alter database [MAGUM] set recovery simple with no_wait;
use [MagicAudit]; checkpoint; use [master]; alter database [MagicAudit] set recovery simple with no_wait;
use [MagicLogging]; checkpoint; use [master]; alter database [MagicLogging] set recovery simple with no_wait;
use [MAGTM]; checkpoint; use [master]; alter database [MAGTM] set recovery simple with no_wait;
use [MAGTF]; checkpoint; use [master]; alter database [MAGTF] set recovery simple with no_wait;
use [MAGSC]; checkpoint; use [master]; alter database [MAGSC] set recovery simple with no_wait;
use [MAGMG]; checkpoint; use [master]; alter database [MAGMG] set recovery simple with no_wait;
use [MAGLL]; checkpoint; use [master]; alter database [MAGLL] set recovery simple with no_wait;
use [MagicPerformance]; checkpoint; use [master]; alter database [MagicPerformance] set recovery simple with no_wait;
use [MAGEV]; checkpoint; use [master]; alter database [MAGEV] set recovery simple with no_wait;
use [MAGEU]; checkpoint; use [master]; alter database [MAGEU] set recovery simple with no_wait;
use [MAGES]; checkpoint; use [master]; alter database [MAGES] set recovery simple with no_wait;
use [MAGEP]; checkpoint; use [master]; alter database [MAGEP] set recovery simple with no_wait;
use [Hiscox_Shared]; checkpoint; use [master]; alter database [Hiscox_Shared] set recovery simple with no_wait;
use [MAGPI]; checkpoint; use [master]; alter database [MAGPI] set recovery simple with no_wait;
use [MAGHH]; checkpoint; use [master]; alter database [MAGHH] set recovery simple with no_wait;
use [MAGFILES]; checkpoint; use [master]; alter database [MAGFILES] set recovery simple with no_wait;
use [MagPE]; checkpoint; use [master]; alter database [MagPE] set recovery simple with no_wait;
use [MagED]; checkpoint; use [master]; alter database [MagED] set recovery simple with no_wait;
use [EDIConfig]; checkpoint; use [master]; alter database [EDIConfig] set recovery simple with no_wait;
use [MAGMP]; checkpoint; use [master]; alter database [MAGMP] set recovery simple with no_wait;
 

--set full


alter database [MAGXXXXX] set recovery full with no_wait;
alter database [MAGWW] set recovery full with no_wait;
alter database [MAGUT] set recovery full with no_wait;
alter database [MAGUR] set recovery full with no_wait;
alter database [MAGUP] set recovery full with no_wait;
alter database [MAGUM] set recovery full with no_wait;
alter database [MagicAudit] set recovery full with no_wait;
alter database [MagicLogging] set recovery full with no_wait;
alter database [MAGTM] set recovery full with no_wait;
alter database [MAGTF] set recovery full with no_wait;
alter database [MAGSC] set recovery full with no_wait;
alter database [MAGMG] set recovery full with no_wait;
alter database [MAGLL] set recovery full with no_wait;
alter database [MagicPerformance] set recovery full with no_wait;
alter database [MAGEV] set recovery full with no_wait;
alter database [MAGEU] set recovery full with no_wait;
alter database [MAGES] set recovery full with no_wait;
alter database [MAGEP] set recovery full with no_wait;
alter database [Hiscox_Shared] set recovery full with no_wait;
alter database [MAGPI] set recovery full with no_wait;
alter database [MAGHH] set recovery full with no_wait;
alter database [MAGFILES] set recovery full with no_wait;
alter database [MagPE] set recovery full with no_wait;
alter database [MagED] set recovery full with no_wait;
alter database [EDIConfig] set recovery full with no_wait;
alter database [MAGMP] set recovery full with no_wait;
*/