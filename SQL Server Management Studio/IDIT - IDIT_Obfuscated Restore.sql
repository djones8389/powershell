USE [msdb]
GO

/****** Object:  Job [IDIT - IDIT_Obfuscated Restore]    Script Date: 06/02/2019 11:43:09 ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [Database Maintenance]    Script Date: 06/02/2019 11:43:09 ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'Database Maintenance' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'Database Maintenance'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'IDIT - IDIT_Obfuscated Restore', 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=3, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'Restore a native backup from production-idit to IDIT_Obfuscated', 
		@category_name=N'Database Maintenance', 
		@owner_login_name=N'dbs', 
		@notify_email_operator_name=N'DBA', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Set offline]    Script Date: 06/02/2019 11:43:09 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Set offline', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'alter database [IDIT_Obfuscated] set offline with rollback immediate;', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Restore IDIT_Obfuscated]    Script Date: 06/02/2019 11:43:09 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Restore IDIT_Obfuscated', 
		@step_id=2, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=1, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'RESTORE DATABASE IDIT_Obfuscated FROM disk = ''\\pr0303c844ed-00\SQL_Backups\LS\IDIT.bak1'',
disk = ''\\pr0303c844ed-00\SQL_Backups\LS\IDIT.bak2'',
disk = ''\\pr0303c844ed-00\SQL_Backups\LS\IDIT.bak3'',
disk = ''\\pr0303c844ed-00\SQL_Backups\LS\IDIT.bak4'',
disk = ''\\pr0303c844ed-00\SQL_Backups\LS\IDIT.bak5'',
disk = ''\\pr0303c844ed-00\SQL_Backups\LS\IDIT.bak6'',
disk = ''\\pr0303c844ed-00\SQL_Backups\LS\IDIT.bak7'',
disk = ''\\pr0303c844ed-00\SQL_Backups\LS\IDIT.bak8'',
disk = ''\\pr0303c844ed-00\SQL_Backups\LS\IDIT.bak9'',
disk = ''\\pr0303c844ed-00\SQL_Backups\LS\IDIT.bak10''
WITH MOVE ''HSCX_DEV'' TO ''E:\SQL_Databases\IDIT_Obfuscated.mdf'',    
MOVE ''HSCX_DEV_log'' TO ''E:\SQL_Logs\IDIT_Obfuscated_log.ldf'', 
REPLACE, RECOVERY
GO
', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Restore IDIT_PROD_MI Restore]    Script Date: 06/02/2019 11:43:09 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Restore IDIT_PROD_MI Restore', 
		@step_id=3, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'exec sp_start_job @job_name = ''IDIT - IDIT_PROD_MI Restore'';', 
		@database_name=N'msdb', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Initiate decryption]    Script Date: 06/02/2019 11:43:09 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Initiate decryption', 
		@step_id=4, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'ALTER DATABASE IDIT_Obfuscated SET ENCRYPTION OFF;', 
		@database_name=N'IDIT_Obfuscated', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Set to simple recovery]    Script Date: 06/02/2019 11:43:09 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Set to simple recovery', 
		@step_id=5, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'ALTER DATABASE IDIT_Obfuscated SET RECOVERY SIMPLE; ', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Truncate tables]    Script Date: 06/02/2019 11:43:09 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Truncate tables', 
		@step_id=6, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'declare @truncate nvarchar(max)='''';

select @truncate+=CHAR(13) +
	''if exists (select 1 from sys.tables where name = ''''''+name+'''''') truncate table ''+QUOTENAME(name)+'';''
from sys.tables
where name in (''ST_GENERAL_LOG'',''P_COVER_CALC_STEP_BACKUP'',''ST_USER_MNG_LOG'',''ST_SERVICE_AUDIT_BLOB'',''ST_SERVICE_AUDIT'',''ST_TASK_LOG'')

exec(@truncate);', 
		@database_name=N'IDIT_Obfuscated', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Obfuscate IDIT_Obfuscated]    Script Date: 06/02/2019 11:43:09 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Obfuscate IDIT_Obfuscated', 
		@step_id=7, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'exec usp_ObfuscateIditData_new', 
		@database_name=N'ObfuscationControl', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Set to full recovery]    Script Date: 06/02/2019 11:43:09 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Set to full recovery', 
		@step_id=8, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'ALTER DATABASE [IDIT_Obfuscated] SET RECOVERY FULL WITH NO_WAIT;', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Run backup if decryption is also complete]    Script Date: 06/02/2019 11:43:09 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Run backup if decryption is also complete', 
		@step_id=9, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'DECLARE @Body nvarchar(MAX)='''';

if exists (
SELECT 1
FROM sys.databases db    
LEFT OUTER JOIN sys.dm_database_encryption_keys dm        
ON db.database_id = dm.database_id
where name = ''IDIT_Obfuscated''
	and is_encrypted = 0
)
BEGIN

	SELECT @Body = ''Database is both obfuscated & decrypted, backing up to E:\SQL_Backups\''+(select cast((select cast(getDATE() as date)) as nvarchar(100)))+''.bak now...''
	
	EXECUTE [msdb].[dbo].[sp_send_dbmail]
    @profile_name = ''SMTP''
    ,@recipients  = ''!Pitstop_Devops@HISCOX.com;dave.jones@hiscox.com; Victoria.Sims@HISCOX.com''
    ,@body        =  @Body
	,@subject     = ''IDIT_Live_Supp - Post-Deployment''
	
	declare @backup nvarchar(max)='''';

	select @backup+=CHAR(13) + 
	''
	BACKUP DATABASE [IDIT_Obfuscated] 
	to DISK = ''''E:\SQL_Backups\IDIT_Obfuscated_''+(select cast((select cast(getDATE() as date)) as nvarchar(100)))+''.bak''''
	WITH copy_only, init, compression, MAXTRANSFERSIZE = 4194304;

	''
	exec(@backup);
END

ELSE 
BEGIN

	SELECT @Body = ''Database is obfuscated, but not decrypted,  check back again later''

	EXECUTE [msdb].[dbo].[sp_send_dbmail]
    @profile_name = ''SMTP''
    ,@recipients  = ''!Pitstop_Devops@HISCOX.com;dave.jones@hiscox.com; Victoria.Sims@HISCOX.com''
    ,@body        =  @Body
	,@subject     = ''IDIT_Live_Supp - Post-Deployment''

END', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Refresh IDIT_PROD_MI', 
		@enabled=0, 
		@freq_type=1, 
		@freq_interval=0, 
		@freq_subday_type=0, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20190205, 
		@active_end_date=99991231, 
		@active_start_time=200000, 
		@active_end_time=235959, 
		@schedule_uid=N'83d6dc41-9153-41cb-89ee-54f78e5fa4c4'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO


