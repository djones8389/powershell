--HXP20290\dwrpt_prod_db,50187

USE [LMDR_Reporting]
GO
/****** Object:  StoredProcedure [dbo].[spLoadBucket1Thresholds]    Script Date: 25/07/2018 10:01:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[spLoadBucket1Thresholds]
AS
BEGIN
BEGIN TRY
BEGIN TRANSACTION
  
BEGIN
	SET NOCOUNT ON;

	-- Delete previous version of table
	IF OBJECT_ID ('dbo.Bucket1MonthlyThresholds', 'U') IS NOT NULL
	DROP TABLE dbo.Bucket1MonthlyThresholds;

	-- Create table
	CREATE TABLE LMDR_Reporting.dbo.Bucket1MonthlyThresholds(bSheet VARCHAR(100) NULL,
															 MonthYear VARCHAR(100) NULL,
															 RRM VARCHAR(8000) NOT NULL,
															 TI VARCHAR(8000) NOT NULL,
															 Threshold VARCHAR(8000) NULL,
															 PeriodMonth INT NULL,
															 PeriodYear VARCHAR(4) NULL,
															 ForScript VARCHAR(8000) NULL,
															 LoadDate DATETIME);
	
	-- Load current values from Cognos
	INSERT INTO LMDR_Reporting.dbo.Bucket1MonthlyThresholds
	SELECT dim_2_projection_c,
		   dim_3_applicable_m,
		   ISNULL(exportvalue_rrm, 0),
		   ISNULL(exportvalue_ti, 0),
		   exportvalue_thresh,
		   CASE
			  WHEN LEFT(dim_3_applicable_m, 3) = 'Jan' THEN 1
			  WHEN LEFT(dim_3_applicable_m, 3) = 'Feb' THEN 2
			  WHEN LEFT(dim_3_applicable_m, 3) = 'Mar' THEN 3
			  WHEN LEFT(dim_3_applicable_m, 3) = 'Apr' THEN 4
			  WHEN LEFT(dim_3_applicable_m, 3) = 'May' THEN 5
			  WHEN LEFT(dim_3_applicable_m, 3) = 'Jun' THEN 6
			  WHEN LEFT(dim_3_applicable_m, 3) = 'Jul' THEN 7
			  WHEN LEFT(dim_3_applicable_m, 3) = 'Aug' THEN 8
			  WHEN LEFT(dim_3_applicable_m, 3) = 'Sep' THEN 9
			  WHEN LEFT(dim_3_applicable_m, 3) = 'Oct' THEN 10
			  WHEN LEFT(dim_3_applicable_m, 3) = 'Nov' THEN 11
			  WHEN LEFT(dim_3_applicable_m, 3) = 'Dec' THEN 12
		   END,
		   '20' + RIGHT(dim_3_applicable_m, 2),
		   'INSERT INTO dbo.TOM_TI_RRM_THRESHOLDS VALUES(''' + dim_2_projection_c + ''', ' + ISNULL(exportvalue_rrm, 0) + ', ' + ISNULL(exportvalue_ti, 0) + ', ' + exportvalue_thresh + ', ' + 
		   CASE
			  WHEN LEFT(dim_3_applicable_m, 3) = 'Jan' THEN '1'
			  WHEN LEFT(dim_3_applicable_m, 3) = 'Feb' THEN '2'
			  WHEN LEFT(dim_3_applicable_m, 3) = 'Mar' THEN '3'
			  WHEN LEFT(dim_3_applicable_m, 3) = 'Apr' THEN '4'
			  WHEN LEFT(dim_3_applicable_m, 3) = 'May' THEN '5'
			  WHEN LEFT(dim_3_applicable_m, 3) = 'Jun' THEN '6'
			  WHEN LEFT(dim_3_applicable_m, 3) = 'Jul' THEN '7'
			  WHEN LEFT(dim_3_applicable_m, 3) = 'Aug' THEN '8'
			  WHEN LEFT(dim_3_applicable_m, 3) = 'Sep' THEN '9'
			  WHEN LEFT(dim_3_applicable_m, 3) = 'Oct' THEN '10'
			  WHEN LEFT(dim_3_applicable_m, 3) = 'Nov' THEN '11'
			  WHEN LEFT(dim_3_applicable_m, 3) = 'Dec' THEN '12'
		   END + ', ' + '20' + RIGHT(dim_3_applicable_m, 2) + ')',
		   GETDATE()
	FROM COGNOS.gmi_ti_rrm_output_analyst.dbo.ev_output_rrm___ti
	WHERE exportvalue_thresh IS NOT NULL
	ORDER BY dim_2_projection_c ASC


END

  COMMIT TRANSACTION

END TRY
BEGIN CATCH

    DECLARE @ErrorNumber INT = ERROR_NUMBER();
    DECLARE @ErrorLine INT = ERROR_LINE();
    DECLARE @ErrorMessage NVARCHAR(4000) = ERROR_MESSAGE();
    DECLARE @ErrorSeverity INT = ERROR_SEVERITY();
    DECLARE @ErrorState INT = ERROR_STATE();
 
    PRINT 'Actual error number: ' + CAST(@ErrorNumber AS VARCHAR(10));
    PRINT 'Actual line number: ' + CAST(@ErrorLine AS VARCHAR(10));

    RAISERROR(@ErrorMessage, @ErrorSeverity, @ErrorState);

  END CATCH
		  Insert into [dbo].[DataLoadLog]
						(ErrorNumber,
						LoadName,
						RunStatus,
						ErrorSeverity,
						ErrorState,
						ErrorLine,
						ErrorMessage,
						LoadDate,
						[Source])

		Values ( @ErrorNumber,
						'dbo.spLoadBucket1Thresholds',
						Case when @ErrorMessage is null then 'Successful' else 'Failed' end,
						@ErrorSeverity,
						@ErrorState,
						@ErrorLine,
						@ErrorMessage,
						getdate (),
						'OBX')
END



sp_linkedservers
