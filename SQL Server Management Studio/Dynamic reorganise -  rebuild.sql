kill 79;

EXECUTE sp_MSforeachdb N'
	SET QUOTED_IDENTIFIER ON;
	USE [?];
	IF DB_ID() > 4
	BEGIN
	DECLARE IndexMaintenance CURSOR FOR
		SELECT	''ALTER INDEX '' + quotename(ix.name) + '' ON '' + quotename(schema_name(schema_id)) + ''.'' + quotename(B.name) + 
				CASE
				WHEN (avg_fragmentation_in_percent BETWEEN 5 AND 30) THEN '' REORGANIZE''
				WHEN (avg_fragmentation_in_percent > 30) THEN '' REBUILD''
				END + '';'' [Statement]
			FROM	sys.dm_db_index_physical_stats(DB_ID(''?''), NULL, NULL, NULL, NULL) ix_phy
			INNER JOIN sys.indexes ix
			ON ix_phy.object_id = ix.object_id
				AND ix_phy.index_id = ix.index_id
			INNER JOIN sys.objects B
			ON ix_phy.object_id = B.object_id
		WHERE ix.index_id > 0	
		ORDER BY database_id;
	DECLARE @SQL NVARCHAR(1000);
	OPEN IndexMaintenance;
	FETCH NEXT FROM IndexMaintenance INTO @SQL;
	WHILE @@FETCH_STATUS = 0
	BEGIN
		EXEC(@SQL);
		FETCH NEXT FROM IndexMaintenance INTO @SQL;
	END
	CLOSE IndexMaintenance;
	DEALLOCATE IndexMaintenance;
	END';


EXECUTE sp_MSforeachdb N'		
		USE [?];
		exec sp_updatestats;
'
