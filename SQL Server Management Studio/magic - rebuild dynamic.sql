use CDR_Local

sp_whoisactive


declare @dynamic nvarchar(max) = '';

select @dynamic+=CHAR(13) + 
	'use '+QUOTENAME(db_name()) +' ALTER INDEX ' +  quotename(B.name)  +  ' ON ' + quotename(B.name) + ' rebuild;'
from INFORMATION_SCHEMA.TABLES a
inner join sys.indexes b
on a.TABLE_NAME	 = OBJECT_NAME(b.object_id)
where TABLE_NAME like 'POLOPTN%'
	and type_desc = 'CLUSTERED'
print(@dynamic);

if exists (
select 1
from INFORMATION_SCHEMA.COLUMNS a
inner join sys.indexes b
on a.TABLE_NAME	 = OBJECT_NAME(b.object_id)
where TABLE_NAME like 'POLOPTN%'
	and type_desc = 'CLUSTERED'
)

 