USE [ODS_RAQ]
GO

/****** Object:  StoredProcedure [dbo].[Refresh_tblContact]    Script Date: 13/12/2018 16:06:57 ******/
DROP PROCEDURE [dbo].[Refresh_tblContact]
GO

/****** Object:  StoredProcedure [dbo].[Refresh_tblContact]    Script Date: 13/12/2018 16:06:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


	
		create procedure [dbo].[Refresh_tblContact] (
			@Date date,
			@updatecount int output,
			@InsertCount int output
		) AS
	
		DECLARE @temp table(
			[Rowguid] uniqueidentifier)
			
		insert into @temp (
			[Rowguid]
		)
		select A.[Rowguid] 
			from [RAQ].[dbo].[tblContact] A
			where not exists (
				select *
				from [ODS_RAQ].[dbo].[tblContact] B 
				where ods_DateTo = '31 dec 2999' and A.[Rowguid] = B.[Rowguid]
				and (((A.[ParentTable] IS NULL AND B.[ParentTable] IS NULL) OR (A.[ParentTable] = B.[ParentTable])) and ((A.[ParentKey] IS NULL AND B.[ParentKey] IS NULL) OR (A.[ParentKey] = B.[ParentKey])) and ((A.[ContactType] IS NULL AND B.[ContactType] IS NULL) OR (A.[ContactType] = B.[ContactType])) and ((A.[ContactName] IS NULL AND B.[ContactName] IS NULL) OR (A.[ContactName] = B.[ContactName])) and ((A.[AccountNumber] IS NULL AND B.[AccountNumber] IS NULL) OR (A.[AccountNumber] = B.[AccountNumber])) and ((A.[PayeeName] IS NULL AND B.[PayeeName] IS NULL) OR (A.[PayeeName] = B.[PayeeName])) and ((A.[Address1] IS NULL AND B.[Address1] IS NULL) OR (A.[Address1] = B.[Address1])) and ((A.[Address2] IS NULL AND B.[Address2] IS NULL) OR (A.[Address2] = B.[Address2])) and ((A.[Address3] IS NULL AND B.[Address3] IS NULL) OR (A.[Address3] = B.[Address3])) and ((A.[Address4] IS NULL AND B.[Address4] IS NULL) OR (A.[Address4] = B.[Address4])) and ((A.[Postcode] IS NULL AND B.[Postcode] IS NULL) OR (A.[Postcode] = B.[Postcode])) and ((A.[Salutation] IS NULL AND B.[Salutation] IS NULL) OR (A.[Salutation] = B.[Salutation])) and ((A.[Telephone] IS NULL AND B.[Telephone] IS NULL) OR (A.[Telephone] = B.[Telephone])) and ((A.[Fax] IS NULL AND B.[Fax] IS NULL) OR (A.[Fax] = B.[Fax])) and ((A.[Email] IS NULL AND B.[Email] IS NULL) OR (A.[Email] = B.[Email])) and ((A.[TheirRef] IS NULL AND B.[TheirRef] IS NULL) OR (A.[TheirRef] = B.[TheirRef])) and ((A.[ExtraInfo1] IS NULL AND B.[ExtraInfo1] IS NULL) OR (A.[ExtraInfo1] = B.[ExtraInfo1])) and ((A.[ExtraInfo2] IS NULL AND B.[ExtraInfo2] IS NULL) OR (A.[ExtraInfo2] = B.[ExtraInfo2])) and ((A.[ExtraInfo3] IS NULL AND B.[ExtraInfo3] IS NULL) OR (A.[ExtraInfo3] = B.[ExtraInfo3])) and ((A.[ExtraInfo4] IS NULL AND B.[ExtraInfo4] IS NULL) OR (A.[ExtraInfo4] = B.[ExtraInfo4])) and ((A.[ExtraInfo5] IS NULL AND B.[ExtraInfo5] IS NULL) OR (A.[ExtraInfo5] = B.[ExtraInfo5])) and ((A.[Rowguid] IS NULL AND B.[Rowguid] IS NULL) OR (A.[Rowguid] = B.[Rowguid])) and ((A.[ContactID] IS NULL AND B.[ContactID] IS NULL) OR (A.[ContactID] = B.[ContactID])))
			)
			
		insert into @temp (
			[Rowguid]
		)
		select A.[Rowguid] 
			from [ODS_RAQ].[dbo].[tblContact] A
			where ods_DateTo = '31 dec 2999'
			and not exists (
				select *
				from [RAQ].[dbo].[tblContact] B 
				where A.[Rowguid] = B.[Rowguid]
			)
			
		if not exists (
			select *
			from @temp
		) begin
			set @updatecount = 0
			set @InsertCount = 0
			return
		end	
		
		if exists (
			select *
			from [ODS_RAQ].[dbo].[tblContact]
			where ods_DateFrom > @Date) begin
						
				RAISERROR ('Future data exists in tblContact',11,1)
				return
		end
		
		update A
		set A.ods_DateTo = @Date
		from [ODS_RAQ].[dbo].[tblContact] A
		join @temp B on A.[Rowguid] = B.[Rowguid]
		where A.ods_DateTo = '31 dec 2999'
		
		set @updatecount = @@ROWCOUNT
		
		insert into [ODS_RAQ].[dbo].[tblContact] (
			ods_DateFrom, ods_DateTo, [ParentTable],[ParentKey],[ContactType],[ContactName],[AccountNumber],[PayeeName],[Address1],[Address2],[Address3],[Address4],[Postcode],[Salutation],[Telephone],[Fax],[Email],[TheirRef],[ExtraInfo1],[ExtraInfo2],[ExtraInfo3],[ExtraInfo4],[ExtraInfo5],[Rowguid],[ContactID]
		)
		select @Date , '31 dec 2999', A.[ParentTable],A.[ParentKey],A.[ContactType],A.[ContactName],A.[AccountNumber],A.[PayeeName],A.[Address1],A.[Address2],A.[Address3],A.[Address4],A.[Postcode],A.[Salutation],A.[Telephone],A.[Fax],A.[Email],A.[TheirRef],A.[ExtraInfo1],A.[ExtraInfo2],A.[ExtraInfo3],A.[ExtraInfo4],A.[ExtraInfo5],A.[Rowguid],A.[ContactID]
		from [RAQ].[dbo].[tblContact] A	
		join @temp B on A.[Rowguid] = B.[Rowguid]
		
		set @InsertCount = @@ROWCOUNT

	
GO


