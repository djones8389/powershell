--Run this from [HXP20203\DBA01]

USE [SQL_Inventory];
GO

DROP TABLE IF EXISTS #MAPSServerList;

CREATE TABLE #MAPSServerList (
	ServerName NVARCHAR(500)
);

select count(1)		--176 distinct servers
from #MAPSServerList

BULK INSERT #MAPSServerList
FROM 'E:\SQL_RND_01\SQL_Misc\MAPS_DJ\Prod\ServerNameOnly.csv' 
WITH (fieldterminator = ',',rowterminator = '\n')

	
SELECT ServerName	--49 missing
FROM #MAPSServerList
WHERE ServerName NOT IN (
		SELECT hostName
		FROM Hosts
		)
ORDER BY ServerName;


