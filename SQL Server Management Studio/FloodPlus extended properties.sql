USE model;
go

DECLARE
	@Application nvarchar(128) = N'FloodPlus',
	@BusinessOwner nvarchar(128) = N'Paul Baumgartner',
	@TechnicalOwner nvarchar(128) = N'Alex Sarafoglou / James Sherborne'--,
	--@MaintenanceWindow nvarchar(128) = N'18:00 - 08:00',
	--@TechnicalExpert nvarchar(128) = N'David Lee',
	--@VSR nvarchar(128) = N'4105',
	--@Notes nvarchar(4000) = N'N/A'

IF EXISTS (SELECT 1 FROM sys.extended_properties WHERE name = 'Application')
EXEC sys.sp_updateextendedproperty @name = N'Application', @value = @Application;
ELSE
EXEC sys.sp_addextendedproperty @name = N'Application', @value = @Application;

IF EXISTS (SELECT 1 FROM sys.extended_properties where name = 'BusinessOwner')
EXEC sys.sp_updateextendedproperty @name = N'BusinessOwner', @value = @BusinessOwner;
ELSE
EXEC sys.sp_addextendedproperty @name = N'BusinessOwner', @value = @BusinessOwner;

IF EXISTS (SELECT 1 FROM sys.extended_properties WHERE name = 'TechnicalOwner')
EXEC sys.sp_updateextendedproperty @name = N'TechnicalOwner', @value = @TechnicalOwner;
ELSE
EXEC sys.sp_addextendedproperty @name = N'TechnicalOwner', @value = @TechnicalOwner;

--IF EXISTS (SELECT 1 FROM sys.extended_properties WHERE name = 'MaintenanceWindow')
--EXEC sys.sp_updateextendedproperty @name = N'MaintenanceWindow', @value = @MaintenanceWindow;
--ELSE
--EXEC sys.sp_addextendedproperty @name = N'MaintenanceWindow', @value = @MaintenanceWindow;

--IF EXISTS (SELECT 1 FROM sys.extended_properties WHERE name = 'TechnicalExpert')
--EXEC sys.sp_updateextendedproperty @name = N'TechnicalExpert', @value = @TechnicalExpert;
--ELSE
--EXEC sys.sp_addextendedproperty @name = N'TechnicalExpert', @value = @TechnicalExpert;

--IF EXISTS (SELECT 1 FROM sys.extended_properties WHERE name = 'Notes')
--EXEC sys.sp_updateextendedproperty @name = N'Notes', @value = @Notes;
--ELSE
--EXEC sys.sp_addextendedproperty @name = N'Notes', @value = @Notes;

--IF EXISTS (SELECT 1 FROM sys.extended_properties WHERE name = 'VSR')
--EXEC sys.sp_updateextendedproperty @name = N'VSR', @value = @VSR;
--ELSE
--EXEC sys.sp_addextendedproperty @name = N'VSR', @value = @VSR;


DECLARE
	@Notes nvarchar(4000) = N'Flood Risk repository for HXP20219
	
	DaveJ - This is now pr0503-13002-00.hiscox.com\FLOODPLUS'


IF EXISTS (SELECT 1 FROM sys.extended_properties WHERE name = 'Notes')
EXEC sys.sp_updateextendedproperty @name = N'Notes', @value = @Notes;
ELSE
EXEC sys.sp_addextendedproperty @name = N'Notes', @value = @Notes;