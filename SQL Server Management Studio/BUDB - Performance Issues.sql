USE OBX_BUD_REPL

SELECT NEWID() [id]
	  ,@@ServerName [Instance]
	  ,DB_NAME(database_id) AS DBName
      ,OBJECT_SCHEMA_NAME(object_id,database_id) AS [SCHEMA_NAME] 
      ,OBJECT_NAME(object_id,database_id)AS [OBJECT_NAME]
      ,cached_time
      ,last_execution_time
      ,execution_count
      ,total_worker_time CPU
      ,total_elapsed_time ELAPSED
	  , total_elapsed_time/execution_count [AVGELAPSEDMICRO]
      ,total_logical_reads LOGICAL_READS
      ,total_logical_writes LOGICAL_WRITES
      ,total_physical_reads PHYSICAL_READS
FROM sys.dm_exec_procedure_stats 
where database_id > 4
	and DB_NAME(database_id) = 'OBX_BUD_REPL'
	--and OBJECT_NAME(object_id,database_id) in ('BUD_SynchronisePolicyDetails','sp_BUD_SynchroniseBUDSearch')
	and database_id != 32767
	order by execution_count DESC;


SELECT --distinct 
	B.name AS TableName
	, C.name AS IndexName
	, C.fill_factor AS IndexFillFactor
	, D.rows AS RowsCount
	, A.avg_fragmentation_in_percent
	, A.page_count
	 ,'ALTER INDEX ' + quotename(C.name) +  ' ON ' + quotename(SCHEMA_NAME(SCHEMA_ID)) + '.' + quotename(B.name) + ' REORGANIZE;'
	, case when (A.avg_fragmentation_in_percent > 30)
		then 'ALTER INDEX ' + quotename(C.name) +  ' ON ' + quotename(SCHEMA_NAME(SCHEMA_ID)) + '.' + quotename(b.name) + ' REBUILD;'
		else 'ALTER INDEX ' + quotename(C.name) +  ' ON ' + quotename(SCHEMA_NAME(SCHEMA_ID)) + '.' + quotename(b.name) + ' REORGANIZE;'
		end as 'IndexMaintenance'
FROM sys.dm_db_index_physical_stats(DB_ID(),NULL,NULL,NULL,NULL) A
INNER JOIN sys.objects B
ON A.object_id = B.object_id
INNER JOIN sys.indexes C
ON B.object_id = C.object_id AND A.index_id = C.index_id
INNER JOIN sys.partitions D
ON B.object_id = D.object_id AND A.index_id = D.index_id
WHERE C.index_id > 0
	and A.avg_fragmentation_in_percent > 5
	and b.name in ('PolicyActivity','Policy','Policysection','PolicyFolder','Contact')
order by 1;






SELECT	newID() as [ID],
			@@SERVERNAME as [Instance],
			 DB_NAME() AS [database],
			 OBJECT_NAME(s.OBJECT_ID) AS [table],
			 i.name AS [index], 
			 s.user_updates,
			 [last_user_update] ,
			 s.user_seeks , 
			 s.user_scans, 
			 s.user_lookups
			 ,  [last_user_seek]
			 ,  [last_user_scan]
			 ,  [last_user_lookup]
	 FROM		sys.dm_db_index_usage_stats AS s WITH (NOLOCK)
	 INNER JOIN sys.indexes AS i WITH (NOLOCK)
			 ON s.[object_id] = i.[object_id]
			AND i.index_id = s.index_id
	WHERE	 OBJECTPROPERTY(s.[OBJECT_ID],'IsUserTable') = 1
	 AND	 i.index_id > 1
 	 and     s.database_id = DB_ID()
	 and  OBJECT_NAME(s.OBJECT_ID) in ('PolicyActivity','Policy','Policysection','PolicyFolder','Contact')
	 and (s.user_seeks + s.user_scans + s.user_lookups) < 100
	ORDER BY OBJECT_NAME(s.OBJECT_ID)
	OPTION	 (RECOMPILE);