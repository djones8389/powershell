--select [DATABASE_NAME]
--	, [DataLoad Type]
--	, TABLE_SCHEMA
--	, TABLE_NAME
--	, ErrorMessage
select * 
from ODS.ReportingMetaData.DataLoadTable ('2019-01-20') 
where ErrorMessage is not NULL
	and TABLE_NAME = 'tblNLAlloc' 

use ODS_RAQ;

select *
from sys.syscomments sc
inner join sys.sysobjects so
on so.id = sc.id 
where sc.text like '%tblNLAlloc%'

--Refresh_tblNLAlloc: Violation of PRIMARY KEY constraint 'PK_dbo_tblNLAlloc'. 
--Cannot insert duplicate key in object 'dbo.tblNLAlloc'. 
--The duplicate key value is (2019-01-16, 550992, R0159425, 996845).

use RAQ

select accountid
	, ParentRef
	, OtherRef
	, InstalID
	, Amount
from tblNLAlloc
where accountid = '550992'
	--and ods_DateFrom = '2019-01-16'
	and ParentRef = 'R0159425'
	and OtherRef = '996845'

USE [ODS_RAQ]
GO

SET ANSI_PADDING ON
GO

/****** Object:  Index [PK_dbo_tblNLAlloc]    Script Date: 18/01/2019 11:46:48 ******/
ALTER TABLE [dbo].[tblNLAlloc] ADD  CONSTRAINT [PK_dbo_tblNLAlloc] PRIMARY KEY CLUSTERED 
(
	[ods_DateFrom] ASC,
	[AccountID] ASC,
	[ParentRef] ASC,
	[OtherRef] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [TABLES]
GO


