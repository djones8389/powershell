USE [ObfuscationControl]
GO

/****** Object:  StoredProcedure [dbo].[usp_ObfuscateIditData_new]    Script Date: 05/02/2019 11:52:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[usp_ObfuscateIditData_new]
AS

SET NOCOUNT ON

DECLARE @recoverymodel varchar(255)
DECLARE @ispublished bit
DECLARE @sql varchar(255)
DECLARE @db varchar(255)

DECLARE @ContactID int
DECLARE @CompanyContactID int
DECLARE @FirstName varchar(255)
DECLARE @LastName varchar(255)
DECLARE @Zip varchar(9)
DECLARE @StreetName varchar(500)
DECLARE @HouseNumber varchar(9)
DECLARE @CityName varchar(255)
DECLARE @CountyName varchar(255)
DECLARE @AppartmentNumber varchar(10)
DECLARE @DateOfBirthYear int
DECLARE @DateOfBirth varchar(10)
DECLARE @BankAccHSCXContactID int
DECLARE @CompanyName varchar(500)
DECLARE @CompanyType varchar(20)

DECLARE @Current_record_count int
DECLARE @ID_Count int

DECLARE @audit_string nvarchar(200) 
DECLARE @ContactID_temp nvarchar(30) 
DECLARE @CompanyContactID_temp nvarchar(30) 

SET @db = 'IDIT_Obfuscated'

--IF @ispublished = 0 and @recoverymodel <> ' SIMPLE'
	SET @sql = 'ALTER DATABASE ' + @db + ' SET RECOVERY SIMPLE WITH NO_WAIT'

EXEC (@sql)

IF NOT EXISTS (SELECT * FROM [IDIT_Obfuscated].sys.indexes WHERE object_id = OBJECT_ID(N'[IDIT_Obfuscated].[dbo].[AC_PMNT_INTERFACE_IN]') AND name = N'nci_AC_PMNT_INTERFACE_IN_Obf')
	CREATE NONCLUSTERED INDEX [nci_AC_PMNT_INTERFACE_IN_Obf] ON [IDIT_Obfuscated].[dbo].[AC_PMNT_INTERFACE_IN]
(
	[CLIENT_INTERNAL_ID] ASC
)

IF NOT EXISTS (SELECT * FROM [IDIT_Obfuscated].sys.indexes WHERE object_id = OBJECT_ID(N'[IDIT_Obfuscated].[dbo].[CN_CONTACT]') AND name = N'CN_CONTACT_temp_idx')
CREATE NONCLUSTERED INDEX [CN_CONTACT_temp_idx] ON [IDIT_Obfuscated].[dbo].[CN_CONTACT] ([NAME]) INCLUDE ([ID])

exec ObfuscationControl..sp_audit_log 'truncate', 'Truncate audit table'

--[AC_BACS_DDI_MESSAGING_ADVICE_HSCX]
exec ObfuscationControl..sp_audit_log 'Start', 'obfuscate AC_BACS_DDI_MESSAGING_ADVICE_HSCX'
UPDATE [IDIT_Obfuscated].[dbo].[AC_BACS_DDI_MESSAGING_ADVICE_HSCX] SET PAYER_NAME = 'Payer Name', PAYER_NEW_NAME = 'Joe Bloggs', BUILD_SOCIETY_ROLL_NUMBER = NULL,
ORIG_ACCOUNT_NUMBER = 16221648, ORIG_SORT_CODE = 601801, PAYER_ACCOUNT_NUMBER = 16221648, PAYER_NEW_ACCOUNT_NUMBER = 16221648, PAYER_NEW_SORT_CODE = 601801, PAYER_SORT_CODE = 601801
exec ObfuscationControl..sp_audit_log 'Finish', 'obfuscate AC_BACS_DDI_MESSAGING_ADVICE_HSCX', @@rowcount

--[AC_PMNT_INTERFACE_IN]
exec ObfuscationControl..sp_audit_log 'Start', 'obfuscate AC_PMNT_INTERFACE_IN'
UPDATE [IDIT_Obfuscated].[dbo].[AC_PMNT_INTERFACE_IN] SET BANK_ACCOUNT_NUMBER = NULL, BANK_NAME = 'Test Bank UK', BENEFICIARY_ACCT_NR = 16221648, PAYER_CARD_VALID_UNTIL = NULL,
BENEFICIARY_BANK_NR = 16221648, CLIENT_BANK_ACCOUNT_NUMBER = 16221648, CLIENT_PLACE_DSC = NULL, CLIENT_REGION_NAME = NULL, PAYER_ACCT_NR = 16221648, PAYER_BANK_NR = 16221648, CLIENT_PROVINCE_NAME = NULL
exec ObfuscationControl..sp_audit_log 'Finish', 'obfuscate AC_PMNT_INTERFACE_IN', @@rowcount

-- AC_PMNT_INTERFACE_OUT
exec ObfuscationControl..sp_audit_log 'Start', 'obfuscate AC_PMNT_INTERFACE_OUT'
UPDATE [IDIT_Obfuscated].[dbo].[AC_PMNT_INTERFACE_OUT] SET BANK_BIC_NR = 'MIDLGB22123', BANK_CLEARING_NR = NULL, BANK_NAME = 'Test Bank UK', BENEFICIARY_BANK_NR = 16221648, CLIENT_PLACE_DSC = NULL,
CLIENT_PROVINCE_NAME = NULL, CLIENT_REGION_NAME = NULL, COMPANY_BIC_NR = 'MIDLGB22123', PAYER_BANK_NR = 16221648, BANK_BRANCH = NULL, PAYER_ACCT_NR = 16221648, CLIENT_CITY_NAME = 'Test City',
CLIENT_STREET_NAME = 'Test Street', CLIENT_HOUSE_NR = '22', CLIENT_APPT_NR = '1a' 
exec ObfuscationControl..sp_audit_log 'Finish', 'obfuscate AC_PMNT_INTERFACE_OUT', @@rowcount

--[AS_PROPERTY_HSCX]
exec ObfuscationControl..sp_audit_log 'Start', 'obfuscate AS_PROPERTY_HSCX'
UPDATE [IDIT_Obfuscated].[dbo].[AS_PROPERTY_HSCX] SET JEWELLERY_WATCHES_BANK_NAME = 'Test Bank UK';
exec ObfuscationControl..sp_audit_log 'Finish', 'obfuscate AS_PROPERTY_HSCX', @@rowcount

--[CN_CONTACT_EMAIL]
exec ObfuscationControl..sp_audit_log 'Start', 'obfuscate CN_CONTACT_EMAIL'
UPDATE [IDIT_Obfuscated].[dbo].[CN_CONTACT_EMAIL] SET EMAIL = 'PITstopTest@hiscox.com'
exec ObfuscationControl..sp_audit_log 'Finish', 'obfuscate CN_CONTACT_EMAIL', @@rowcount

--[C_CLAIM_EVENT_LOCATION]
/* removed NC
exec ObfuscationControl..sp_audit_log 'Start', 'obfuscate C_CLAIM_EVENT_LOCATION'
UPDATE [IDIT_Obfuscated].[dbo].[C_CLAIM_EVENT_LOCATION] SET COUNTY_NAME = NULL, DISTRICT_NAME = NULL, PROVINCE_NAME = NULL, REGION_NAME = NULL
exec ObfuscationControl..sp_audit_log 'Finish', 'obfuscate C_CLAIM_EVENT_LOCATION', @@rowcount
*/

--[CN_ADDRESS]
exec ObfuscationControl..sp_audit_log 'Start', 'obfuscate CN_ADDRESS'
UPDATE [IDIT_Obfuscated].[dbo].[CN_ADDRESS] SET BUILDING_NAME = 'Test Building', PROVINCE_NAME = 'Test Province', COUNTY_NAME = 'Test County', DISTRICT_NAME = 'Test District', REGION_NAME = 'Test Region', STREET_NAME = 'Test Steet',
HOUSE_NR = '22', APPARTMENT_NR = '1a', CITY_NAME = 'Test City', BLOCK_NUMBER = ABS(CAST(CAST(NEWID() AS VARBINARY) AS INT)) / 10000000, STREET_NAME_LOWER = LOWER('Test Steet'),
HOUSE_NR_LOWER = 22, CITY_NAME_LOWER = LOWER('Test City'), ADDRESS_LINE_1 = case when ADDRESS_LINE_1 is not null then 'Test ADDRESS_LINE_1' end,
ADDRESS_LINE_4 = case when ADDRESS_LINE_4 is not null then 'Test ADDRESS_LINE_4' end
exec ObfuscationControl..sp_audit_log 'Finish', 'obfuscate CN_ADDRESS', @@rowcount

--[CN_CONTACT]
exec ObfuscationControl..sp_audit_log 'Start', 'obfuscate CN_CONTACT'
UPDATE [IDIT_Obfuscated].[dbo].[CN_CONTACT] 
SET LATIN_NAME = NULL, LATIN_NAME_LOWER = NULL, MIDDLE_NAME = NULL, MIDDLE_NAME_LOWER = NULL, SHORT_NAME = NULL, 
SHORT_NAME_LOWER = NULL
--New code added 13/12/2017
where ID not in(select CONTACT_ID from [IDIT_Obfuscated].[dbo].CN_CONTACT_ROLE where ROLE_ID in(4,5,10016,10001,1000001,1000004,1000016,1000029,1005030,28,1000012))
exec ObfuscationControl..sp_audit_log 'Finish', 'obfuscate CN_CONTACT', @@rowcount

-- [CN_CONTACT_BANK_ACCOUNT]
exec ObfuscationControl..sp_audit_log 'Start', 'obfuscate CN_CONTACT_BANK_ACCOUNT'
UPDATE [IDIT_Obfuscated].[dbo].[CN_CONTACT_BANK_ACCOUNT] SET BANK_NAME = 'Test Bank UK', BRANCH_NAME = NULL, ACCOUNT_NR = 16221648, IBAN_REFERENCE = 'GB15MIDL60180116221648',
SWIFT_CODE = NULL, BIC_CODE = 'MIDLGB22123', BBAN = NULL, VERIFICATION_STATUS_REMARKS = NULL
exec ObfuscationControl..sp_audit_log 'Finish', 'obfuscate CN_CONTACT_BANK_ACCOUNT', @@rowcount

--[CN_CONTACT_BANK_ACCOUNT_HSCX]
exec ObfuscationControl..sp_audit_log 'Start', 'obfuscate CN_CONTACT_BANK_ACCOUNT_HSCX'
UPDATE [IDIT_Obfuscated].[dbo].[CN_CONTACT_BANK_ACCOUNT_HSCX] SET ADDRESS_LINE_2 = NULL, ADDRESS_LINE_3 = NULL, ADDRESS_LINE_4 = NULL, ADDRESS_LINE_5 = NULL,
ABA_ACCOUNT_NUMBER = NULL, ABA_ROUTING_NUMBER = NULL
exec ObfuscationControl..sp_audit_log 'Finish', 'obfuscate CN_CONTACT_BANK_ACCOUNT_HSCX', @@rowcount

--CN_CONTACT_HISTORY
exec ObfuscationControl..sp_audit_log 'Start', 'obfuscate CN_CONTACT_HISTORY'
UPDATE [IDIT_Obfuscated].[dbo].[CN_CONTACT_HISTORY] SET CONTACT_LATIN_NAME = NULL, CONTACT_MIDDLE_NAME = NULL, CONTACT_SHORT_NAME = NULL
exec ObfuscationControl..sp_audit_log 'Finish', 'obfuscate CN_CONTACT_HISTORY', @@rowcount

--[CN_CONTACT_TELEPHONE]
exec ObfuscationControl..sp_audit_log 'Start', 'obfuscate CN_CONTACT_TELEPHONE'
UPDATE [IDIT_Obfuscated].[dbo].[CN_CONTACT_TELEPHONE] SET TELEPHONE_NUMBER = '123456', TELEPHONE_PREFIX = '01206';
exec ObfuscationControl..sp_audit_log 'Finish', 'obfuscate CN_CONTACT_TELEPHONE', @@rowcount

--[E_REFFERAL]
exec ObfuscationControl..sp_audit_log 'Start', 'obfuscate E_REFFERAL'
UPDATE [IDIT_Obfuscated].[dbo].[E_REFFERAL] SET EMAIL = 'PITstopTest@hiscox.com', EXPERT_TL_NR = '01206 123456'
exec ObfuscationControl..sp_audit_log 'Finish', 'obfuscate E_REFFERAL', @@rowcount

--[SH_DDI_REQUEST_HSCX]
exec ObfuscationControl..sp_audit_log 'Start', 'obfuscate SH_DDI_REQUEST_HSCX'
UPDATE [IDIT_Obfuscated].[dbo].[SH_DDI_REQUEST_HSCX] SET DESTINATION_ACCOUNT_NAME = 'Test Account Name', DESTINATION_ACCOUNT_NUMBER = 16221648, DESTINATION_SORT_CODE = 601801
exec ObfuscationControl..sp_audit_log 'Finish', 'obfuscate SH_DDI_REQUEST_HSCX', @@rowcount

--[SH_OUTGOING_PMNT_HSCX]
exec ObfuscationControl..sp_audit_log 'Start', 'obfuscate SH_OUTGOING_PMNT_HSCX'
UPDATE [IDIT_Obfuscated].[dbo].[SH_OUTGOING_PMNT_HSCX] SET DESTINATION_ACCOUNT_NAME = 'Test Account Name', DESTINATION_ACCOUNT_NUMBER = 16221648, DESTINATION_SORT_CODE = 601801,
ORIGINATION_ACCOUNT_NAME = 'TEST', ORIGINATION_ACCOUNT_NUMBER = 16221648, ORIGINATION_SORT_CODE = 601801 
exec ObfuscationControl..sp_audit_log 'Finish', 'obfuscate SH_OUTGOING_PMNT_HSCX', @@rowcount

-- ########
-- NEW COLS
-- ########

UPDATE [IDIT_Obfuscated].[dbo].AC_AGENT_ROLE SET CANCELLATION_REMARKS = case when CANCELLATION_REMARKS is not null then 'Remarks' end
UPDATE [IDIT_Obfuscated].[dbo].AC_AGENT_STATEMENT SET REMARKS = case when REMARKS is not null then 'Remarks' end
UPDATE [IDIT_Obfuscated].[dbo].AC_TRANSACTION SET CHEQUE_NUMBER	= NULL
UPDATE [IDIT_Obfuscated].[dbo].AC_TRANSACTION_HSCX SET PAYER_NAME = case when PAYER_NAME is not null then 'Test PAYER_NAME' end
UPDATE [IDIT_Obfuscated].[dbo].AC_TRANSACTION_QUERY SET DESCRIPTION = case when DESCRIPTION is not null then 'Remarks' end
UPDATE [IDIT_Obfuscated].[dbo].AS_ASSET_ITEM_HSCX SET REMARK = case when REMARK is not null then 'Remarks' end
UPDATE [IDIT_Obfuscated].[dbo].AS_BI_SUPPLIER_HSCX SET CITY = case when CITY is not null then 'Test CITY' end, NAME = case when NAME is not null then 'Test NAME' end, REGION = case when REGION is not null then 'Test REGION' end
UPDATE [IDIT_Obfuscated].[dbo].AS_COMPANY_EL_HSCX SET REMARK = case when REMARK is not null then 'Remarks' end
UPDATE [IDIT_Obfuscated].[dbo].AS_COMPANY_MLP_HSCX SET REMARK = case when REMARK is not null then 'Remarks' end
UPDATE [IDIT_Obfuscated].[dbo].AS_COMPANY_PPL_HSCX SET REMARK = case when REMARK is not null then 'Remarks' end
UPDATE [IDIT_Obfuscated].[dbo].AS_DC_PROPERTY_HSCX SET PROPERTY_BUSINESS_DETAIL = case when PROPERTY_BUSINESS_DETAIL is not null then 'Test PROPERTY_BUSINESS_DETAIL' end
UPDATE [IDIT_Obfuscated].[dbo].AS_HEAD_COVER_HSCX SET REMARK = case when REMARK is not null then 'Remarks' end
UPDATE [IDIT_Obfuscated].[dbo].AS_POLICY_CONT_BY_COVER_HSCX SET REMARK = case when REMARK is not null then 'Remarks' end
UPDATE [IDIT_Obfuscated].[dbo].AS_PROPERTY_ROOMS SET REMARKS = case when REMARKS is not null then 'Remarks' end
UPDATE [IDIT_Obfuscated].[dbo].AS_UNDERWRITING_AGGREGATION_HSCX SET UNDERWRITING_REMARKS = case when UNDERWRITING_REMARKS is not null then 'Remarks' end
UPDATE [IDIT_Obfuscated].[dbo].C_CLAIM_TRANSACTION SET CANCEL_ACTION_REMARKS = case when CANCEL_ACTION_REMARKS is not null then 'Remarks' end
UPDATE [IDIT_Obfuscated].[dbo].C_CLAIM_TRANSACTION_HSCX SET CHEQUE_PAYEE_NAME = case when CHEQUE_PAYEE_NAME is not null then 'JOE BLOGGS' end
UPDATE [IDIT_Obfuscated].[dbo].C_DAMAGED_OBJECT_PAYMENT_ADDRESS_HSCX SET FLAT_NUMBER = case when FLAT_NUMBER is not null then '1234578' end, HOUSE_NAME = case when HOUSE_NAME is not null then 'Test HOUSE_NAME' end, HOUSE_NUMBER = case when HOUSE_NUMBER is not null then '1234578' end, STREET_NAME = case when STREET_NAME is not null then 'Test STREET_NAME' end
UPDATE [IDIT_Obfuscated].[dbo].C_GROUPED_CLAIM_HISTORY SET GROUPED_CLAIM_REMARKS = case when GROUPED_CLAIM_REMARKS is not null then 'Remarks' end, REMARKS = case when REMARKS is not null then 'Remarks' end
UPDATE [IDIT_Obfuscated].[dbo].C_MOTOR_CLAIM_MATCH SET CLAIMANT_EMAIL = case when CLAIMANT_EMAIL is not null then 'pitstoptest@hiscox.com' end, CLAIMANT_NAME = case when CLAIMANT_NAME is not null then 'Test CLAIMANT_NAME' end, CLAIMANT_TELEPHONE_NUMBER = case when CLAIMANT_TELEPHONE_NUMBER is not null then '1234578' end
UPDATE [IDIT_Obfuscated].[dbo].C_WRITE_OFF_HISTORY SET WRITE_OFF_ACTION_REMARKS = case when WRITE_OFF_ACTION_REMARKS is not null then 'Remarks' end
UPDATE [IDIT_Obfuscated].[dbo].DB_MANAGEMENT SET LIVE_DB_NAME = case when LIVE_DB_NAME is not null then 'Test LIVE_DB_NAME' end
UPDATE [IDIT_Obfuscated].[dbo].P_COMPANY_LIABILITY SET POLICY_OWNER_ADDRESS = case when POLICY_OWNER_ADDRESS is not null then 'Test POLICY_OWNER_ADDRESS' end, POLICY_OWNER_NAME = case when POLICY_OWNER_NAME is not null then 'Test POLICY_OWNER_NAME' end
UPDATE [IDIT_Obfuscated].[dbo].P_LOB_ASSET_NON_RNL_RSN SET REMARKS = case when REMARKS is not null then 'Remarks' end
UPDATE [IDIT_Obfuscated].[dbo].P_LOB_NON_RNL_RSN SET REMARKS = case when REMARKS is not null then 'Remarks' end
UPDATE [IDIT_Obfuscated].[dbo].P_POLICY SET UW_DECISION_REMARKS = case when UW_DECISION_REMARKS is not null then 'Remarks' end
UPDATE [IDIT_Obfuscated].[dbo].P_POLICY_HSCX SET COMMENT = case when COMMENT is not null then 'Remarks' end
--New code added 13/12/2017
UPDATE [IDIT_Obfuscated].[dbo].P_POL_HEADER_REMARKS SET SPECIAL_REMARK = case when SPECIAL_REMARK is not null then 'Remarks' end
--New code added 10/05/2018
update [IDIT_Obfuscated].[dbo].[T_OUTGOING_INTERFACE_PROP] set service_URI = 'dummyurl' where id in (1000002,1000003,1000004,1000006,1000010,1000011,1000012,1000013,1000014,1000015,1000016,1000017);
--following line updated on 02/10/2018
update [IDIT_Obfuscated].[dbo].[t_application_params] set value = 'dummyurl' where ID in(1000042, 1000043, 1000044, 1000045, 1000078);

UPDATE [IDIT_Obfuscated].[dbo].P_SURVEY_REQUEST_HSCX SET ADDITIONAL_COMMENTS = case when ADDITIONAL_COMMENTS is not null then 'Remarks' end, BROKER_COMPANY_NAME = case when BROKER_COMPANY_NAME is not null then 'Test BROKER_COMPANY_NAME' end,
BROKER_EMAIL_ADDRESS = case when BROKER_EMAIL_ADDRESS is not null then 'Test BROKER_EMAIL_ADDRESS' end, BROKER_EXTENSION = case when BROKER_EXTENSION is not null then 'Test BROKER_EXTENSION' end,
BROKER_TELEPHONE_NUMBER = case when BROKER_TELEPHONE_NUMBER is not null then '1234578' end, CONTACT_EMAIL_ADDRESS = case when CONTACT_EMAIL_ADDRESS is not null then 'Test CONTACT_EMAIL_ADDRESS' end,
CONTACT_EXTENSION = case when CONTACT_EXTENSION is not null then 'Test CONTACT_EXTENSION' end, CONTACT_NAME = case when CONTACT_NAME is not null then 'Test CONTACT_NAME' end,
CONTACT_TELEPHONE_NUMBER = case when CONTACT_TELEPHONE_NUMBER is not null then '1234578' end, SURVEYOR_COMPANY_NAME = case when SURVEYOR_COMPANY_NAME is not null then 'Test SURVEYOR_COMPANY_NAME' end,
SURVEYOR_EMAIL_ADDRESS = case when SURVEYOR_EMAIL_ADDRESS is not null then 'pitstoptest@hiscox.com' end, SURVEYOR_EXTENSION = case when SURVEYOR_EXTENSION is not null then '111' end,
SURVEYOR_TELEPHONE_NUMBER = case when SURVEYOR_TELEPHONE_NUMBER is not null then '1234578' end

UPDATE [IDIT_Obfuscated].[dbo].QRTZ_JOB_DETAILS_BCK SET DESCRIPTION = NULL	
UPDATE [IDIT_Obfuscated].[dbo].QRTZ_TRIGGERS_BCK SET DESCRIPTION = NULL
UPDATE [IDIT_Obfuscated].[dbo].S_AUTH_EX_ADTNL_DATA SET REMARK = case when REMARK is not null then 'Remarks' end

UPDATE [IDIT_Obfuscated].[dbo].SH_ELTO_REPORT SET EMPLOYER_NAME = case when EMPLOYER_NAME is not null then 'Test EMPLOYER_NAME' end, EMPLOYER_REF_NUM = case when EMPLOYER_REF_NUM is not null then '1234578' end,
POLICY_HOLDER_NAME = case when POLICY_HOLDER_NAME is not null then 'Test POLICY_HOLDER_NAME' end, SUPPLIED_FULL_ADDRESS = case when SUPPLIED_FULL_ADDRESS is not null then 'Test SUPPLIED_FULL_ADDRESS' end

UPDATE [IDIT_Obfuscated].[dbo].SH_SUN_GL_HSCX SET PAYER_NAME = case when PAYER_NAME is not null then 'Test PAYER_NAME' end
UPDATE [IDIT_Obfuscated].[dbo].TMP_S_AUTH_EX SET CONTEXT = NULL, REMARKS = case when REMARKS is not null then 'Remarks' end
UPDATE [IDIT_Obfuscated].[dbo].TMP_S_AUTH_EX_UNIQUE SET CONTEXT = NULL, REMARKS = case when REMARKS is not null then 'Remarks' end
UPDATE [IDIT_Obfuscated].[dbo].TMP_S_AUTH_EXCEPT_BACKUP SET CONTEXT = NULL, REMARKS = case when REMARKS is not null then 'Remarks' end
update [IDIT_Obfuscated].[dbo].t_com_channel set EMAIL_SERVER_HOST = 'relay.hiscox.nonprod' where ID > 12;

--New code added 25/07/2018

UPDATE [IDIT_Obfuscated].[dbo].AS_MID_DRIVER_HSCX SET DRIVER_NAME = case when DRIVER_NAME is not null then 'Test DRIVER_NAME' end

UPDATE [IDIT_Obfuscated].[dbo].P_MID_POLICY_HSCX SET ADDRESS_LINE_1 = case when ADDRESS_LINE_1 is not null then 'Test ADDRESS_LINE_1' end,
ADDRESS_LINE_2 = case when ADDRESS_LINE_2 is not null then 'Test ADDRESS_LINE_2' end,
ADDRESS_LINE_3 = case when ADDRESS_LINE_3 is not null then 'Test ADDRESS_LINE_3' end,
ADDRESS_LINE_4 = case when ADDRESS_LINE_4 is not null then 'Test ADDRESS_LINE_4' end,
ADDRESS_LINE_5 = case when ADDRESS_LINE_5 is not null then 'Test ADDRESS_LINE_5' end,
ADDRESS_LINE_6 = case when ADDRESS_LINE_6 is not null then 'Test ADDRESS_LINE_6' end,
POLICY_HOLDER_NAME = case when POLICY_HOLDER_NAME is not null then 'Test POLICY_HOLDER_NAME' end,
POSTCODE = case when POSTCODE is not null then 'Test POSTCODE' end

UPDATE [IDIT_Obfuscated].[dbo].AS_VEHICLE SET MAIN_DRIVER_REMARKS = case when MAIN_DRIVER_REMARKS is not null then 'Test MAIN_DRIVER_REMARKS' end

update [IDIT_Obfuscated].[dbo].CN_PERSON_DRIVING_DATA_HSCX
set DRIVING_LICENCE_NUMBER = 
left(cn.[NAME] + '99999', 5) +
substring(cast(datepart(yy, cch.DATE_OF_BIRTH) as char(4)), 3, 1) +
case when cch.GENDER = 1 then RIGHT('5' + RTRIM(MONTH(cch.DATE_OF_BIRTH)), 2)
else RIGHT('0' + RTRIM(MONTH(cch.DATE_OF_BIRTH)), 2)
end +
RIGHT('0' + RTRIM(DAY(cch.DATE_OF_BIRTH)), 2) +
substring(cast(datepart(yy, cch.DATE_OF_BIRTH) as char(4)), 4, 1) +
case when cn.middle_name is null then left(cn.[FIRST_NAME],1) + '9'
else left(cn.[FIRST_NAME],1) + left(cn.[middle_name],1)
end +
'9' +
char(cast((90 - 65 )*rand() + 65 as integer)) + char(cast((90 - 65 )*rand() + 65 as integer)) +
'01'
from [IDIT_Obfuscated].[dbo].CN_CONTACT cn, [IDIT_Obfuscated].[dbo].CN_PERSON cch, [IDIT_Obfuscated].[dbo].CN_PERSON_DRIVING_DATA cnp, [IDIT_Obfuscated].[dbo].CN_PERSON_DRIVING_DATA_HSCX cnph
where cn.id=cch.contact_id
and cn.ID = cnp.CONTACT_ID 
and cnp.ID = cnph.ID  
and cnph.DRIVING_LICENCE_NUMBER is not null

update [IDIT_Obfuscated].[dbo].AS_VEHICLE_DRIVERS
set DRIVING_LICENE_NR = 
left(cn.[NAME] + '99999', 5) +
substring(cast(datepart(yy, cch.DATE_OF_BIRTH) as char(4)), 3, 1) +
case when cch.GENDER = 1 then RIGHT('5' + RTRIM(MONTH(cch.DATE_OF_BIRTH)), 2)
else RIGHT('0' + RTRIM(MONTH(cch.DATE_OF_BIRTH)), 2)
end +
RIGHT('0' + RTRIM(DAY(cch.DATE_OF_BIRTH)), 2) +
substring(cast(datepart(yy, cch.DATE_OF_BIRTH) as char(4)), 4, 1) +
case when cn.middle_name is null then left(cn.[FIRST_NAME],1) + '9'
else left(cn.[FIRST_NAME],1) + left(cn.[middle_name],1)
end +
'9' +
char(cast((90 - 65 )*rand() + 65 as integer)) + char(cast((90 - 65 )*rand() + 65 as integer)) +
'01'
from [IDIT_Obfuscated].[dbo].CN_CONTACT cn, [IDIT_Obfuscated].[dbo].CN_PERSON cch, [IDIT_Obfuscated].[dbo].CN_PERSON_DRIVING_DATA cnp, [IDIT_Obfuscated].[dbo].CN_PERSON_DRIVING_DATA_HSCX cnph, [IDIT_Obfuscated].[dbo].AS_VEHICLE_DRIVERS avd
where cn.id=cch.contact_id
and cn.ID = cnp.CONTACT_ID 
and cnp.ID = cnph.ID
and cnph.ID = avd.iD
and avd.DRIVING_LICENE_NR is not null

exec ObfuscationControl..sp_audit_log 'Finish', 'Non-Randomised obfuscation complete'

SET XACT_ABORT ON;

select @ID_Count = 	count(*) FROM [IDIT_Obfuscated].[dbo].[CN_CONTACT] a LEFT JOIN [IDIT_Obfuscated].[dbo].[CN_COMPANY] b
	ON a.ID = b.CONTACT_ID WHERE a.name NOT IN('Hiscox Underwriting Limited','DO NOT USE - Hiscox','Hiscox Insurance Company Limited')

select @Current_record_count = 1
    
DECLARE Contact CURSOR FAST_FORWARD READ_ONLY
FOR

	SELECT ID,CONTACT_ID FROM [IDIT_Obfuscated].[dbo].[CN_CONTACT] a LEFT JOIN [IDIT_Obfuscated].[dbo].[CN_COMPANY] b ON a.ID = b.CONTACT_ID
	--WHERE a.name NOT IN('Hiscox Underwriting Limited','DO NOT USE - Hiscox','Hiscox Insurance Company Limited')
	WHERE a.id not in(select CONTACT_ID from [IDIT_Obfuscated].[dbo].CN_CONTACT_ROLE where ROLE_ID in(4,5,10016,10001,1000004,1000016,1000029,1005030,28,1000012))
	order by ID,CONTACT_ID
	
	OPEN Contact
	FETCH NEXT FROM Contact INTO @ContactID, @CompanyContactID

	WHILE @@FETCH_STATUS = 0
	BEGIN TRY
		BEGIN TRANSACTION
		--PRINT @ContactID

			SET @AppartmentNumber = ABS(CAST(CAST(NEWID() AS VARBINARY) AS INT)) / 10000000
			SET @HouseNumber = ABS(CAST(CAST(NEWID() AS VARBINARY) AS INT)) / 10000000

			if @ContactID is null set @ContactID_temp = 'NULL' else set @ContactID_temp = @ContactID
			if @CompanyContactID is null set @CompanyContactID_temp = 'NULL' else set @CompanyContactID_temp = @ContactID

			select @audit_string = 'Processing ContactID: ' + @ContactID_temp + ' CompanyContactID: ' + @CompanyContactID_temp + ' (Record ' + convert(varchar(20),@Current_record_count) + ' of ' + convert(varchar(20),@ID_Count) + ')'
			exec ObfuscationControl..sp_audit_log 'Start', @audit_string

			SELECT TOP 1 @FirstName = FirstName
			FROM [ObfuscationControl].[dbo].[FirstNames]
			ORDER BY NEWID()

			SELECT TOP 1 @LastName = LastName
			FROM [ObfuscationControl].[dbo].[LastNames]
			ORDER BY NEWID()

			SELECT @Zip = ZIP FROM [IDIT_Obfuscated].[dbo].[CN_ADDRESS]
			WHERE ID = @ContactID
			IF @Zip IS NULL
				SET @Zip = 'EC3A6HX'

			SELECT TOP 1 @StreetName = Street 
			FROM [ObfuscationControl].[dbo].[UKStreets]
			ORDER BY NEWID()

			SELECT TOP 1 @HouseNumber = StreetNumber
			FROM [ObfuscationControl].[dbo].[StreetNumbers]
			ORDER BY NEWID()

			SELECT TOP 1 @CityName = City
			FROM [ObfuscationControl].[dbo].[UKCities]
			ORDER BY NEWID()

			SELECT TOP 1 @CountyName = County
			FROM [ObfuscationControl].[dbo].[UKCounties]
			ORDER BY NEWID()

			SELECT @DateOfBirthYear = DATEPART(yyyy,DATE_OF_BIRTH)
			FROM [IDIT_Obfuscated].[dbo].[CN_PERSON]
			WHERE CONTACT_ID = @ContactID

			IF @DateOfBirthYear IS NULL
			SET @DateOfBirthYear = DATEPART(yyyy,DATEADD(yyyy,-30,GETDATE()))

			SET @DateOfBirth = '01/01/' + CAST(@DateOfBirthYear as varchar(4))

			IF @CompanyContactID IS NULL
			BEGIN
				UPDATE [IDIT_Obfuscated].[dbo].[CN_CONTACT] 
				SET 
					NAME = @LastName, 
					FIRST_NAME = @FirstName, 
					FIRST_NAME_LOWER = LOWER(@FirstName),
					MIDDLE_NAME_LOWER = NULL, 
					NAME_LOWER = LOWER(@LastName),
					AGENT_AJAX_PRESENT = @FirstName + ' '  + @LastName + ' '  + @HouseNumber + ' '  + @StreetName + ' ' + @Zip
				WHERE ID = @ContactID
				--New code to test 13/12/2017
				AND NAME is not null

			END
			ELSE
			BEGIN
				SELECT TOP 1 @CompanyName = Company FROM [ObfuscationControl].[dbo].[CompanyNames] ORDER BY NEWID()
				SELECT TOP 1 @CompanyType = CompanyType FROM [ObfuscationControl].[dbo].Company_Type ORDER BY NEWID()

				UPDATE [IDIT_Obfuscated].[dbo].[CN_CONTACT] 
				SET 
					NAME = @CompanyName + ' ' + @CompanyType, 
					NAME_LOWER = LOWER(@CompanyName + ' ' + @CompanyType),
					AGENT_AJAX_PRESENT = @CompanyName + ' ' + @CompanyType + ' ' + @HouseNumber + ' ' + @StreetName + ' ' + @Zip
				WHERE ID = @ContactID
			END

			SET @BankAccHSCXContactID = (SELECT MIN(ID)
			FROM [IDIT_Obfuscated].[dbo].[CN_CONTACT_BANK_ACCOUNT] WHERE CONTACT_ID = @ContactID)
			
			WHILE @BankAccHSCXContactID IS NOT NULL
			BEGIN
				UPDATE [IDIT_Obfuscated].[dbo].[CN_CONTACT_BANK_ACCOUNT_HSCX]
				SET ACCOUNT_HOLDER = @FirstName + ' ' +  @LastName, ADDRESS_LINE_1 = @HouseNumber + ' ' + @StreetName
				WHERE ID = @BankAccHSCXContactID

				SET @BankAccHSCXContactID = (SELECT MIN(ID)
				FROM [IDIT_Obfuscated].[dbo].[CN_CONTACT_BANK_ACCOUNT] WHERE CONTACT_ID = @ContactID AND ID > @BankAccHSCXContactID)
			END

			--NEW Code to test 28/04/2015
			UPDATE [IDIT_Obfuscated].[dbo].[E_REFFERAL]
			SET EXPERT_NAME = @FirstName + ' ' + @LastName, EXPERT_ADRS = @HouseNumber + ' ' + @StreetName + ' ' + @Zip
			WHERE CLIENT_EXPERT IN (SELECT DISTINCT b.ID FROM [IDIT_Obfuscated].[dbo].[CN_SERVICE_PROVIDER] b INNER JOIN [IDIT_Obfuscated].[dbo].[CN_CONTACT] a ON b.CONTACT_ID = a.ID WHERE a.ID = @ContactId)

			--NEW Code to test 28/04/2015
			UPDATE [IDIT_Obfuscated].[dbo].[CN_ADDRESS]
			SET STREET_NAME = @StreetName, HOUSE_NR = @HouseNumber, APPARTMENT_NR = @AppartmentNumber, CITY_NAME = @CityName, BLOCK_NUMBER = NULL, STREET_NAME_LOWER = LOWER(@StreetName), HOUSE_NR_LOWER = LOWER(@HouseNumber), CITY_NAME_LOWER = LOWER(@CityName)
			WHERE ID IN (SELECT ADRESS_ID FROM [IDIT_Obfuscated].[dbo].[CN_CONTACT_ADDRESS] a INNER JOIN [IDIT_Obfuscated].[dbo].[CN_CONTACT] b ON a.CONTACT_ID = b.ID WHERE b.ID = @ContactID)

			--NEW Code to test 29/04/2015
			UPDATE [IDIT_Obfuscated].[dbo].[AC_PMNT_INTERFACE_IN]
			SET CLIENT_NAME = @FirstName + ' ' + @LastName, CLIENT_CITY_NAME = @CityName, CLIENT_STREET_NAME = @StreetName,CLIENT_HOUSE_NR = @HouseNumber, CLIENT_APPT_NR = @AppartmentNumber
			WHERE CLIENT_INTERNAL_ID = @ContactID

			--NEW Code to test 29/04/2015
			UPDATE [IDIT_Obfuscated].[dbo].[AC_PMNT_INTERFACE_OUT]
			SET CLIENT_NAME = @FirstName + ' ' + @LastName ,CLIENT_CITY_NAME = @CityName, CLIENT_STREET_NAME = @StreetName, CLIENT_HOUSE_NR = @HouseNumber, CLIENT_APPT_NR = @AppartmentNumber, CLIENT_ADDRESS = @HouseNumber + ' ' + @AppartmentNumber + ' ' + @StreetName + ' ' + @Zip, PAYER_CARD_VALID_UNTIL = DATEADD(mm,CAST(RAND()*10000 AS INT) % 12,GETDATE())
			WHERE CLIENT_INTERNAL_ID = @ContactID;

			--NEW Code to test 29/04/2015
			/* removed NC
			--UPDATE [IDIT_Obfuscated].[dbo].[C_CLAIM_EVENT_LOCATION]
			--SET CITY_NAME = @CityName, STREET_NAME = @StreetName, HOUSE_NR = @HouseNumber, APPARTMENT_NR = @AppartmentNumber, BLOCK_NUMBER = NULL
			--WHERE CLAIM_ID IN (SELECT a.ID FROM [IDIT_Obfuscated].[dbo].[C_CLAIM] a inner join [IDIT_Obfuscated].[dbo].[C_CLAIM_CONTACT] b on b.CLAIM_ID = a.ID where b.CONTACT_ID = @ContactID)
			*/

			UPDATE [IDIT_Obfuscated].[dbo].[CN_CONTACT_HISTORY]
			SET CONTACT_NAME = @LastName, CONTACT_FIRST_NAME = @FirstName, CONTACT_DATE_OF_BIRTH = @DateOfBirth
			WHERE CONTACT_ID = @ContactID;

			UPDATE [IDIT_Obfuscated].[dbo].[CN_PERSON]
			SET DATE_OF_BIRTH = @DateOfBirth
			WHERE CONTACT_ID = @ContactID

			UPDATE [IDIT_Obfuscated].[dbo].[CN_PERSON_HSCX]
			SET CORRESPONDENCE_NAME = @FirstName + ' ' + @LastName
			WHERE ID = @ContactID

	   COMMIT TRANSACTION

       select @Current_record_count = @Current_record_count + 1

	FETCH NEXT FROM Contact INTO @ContactID, @CompanyContactID

END TRY

BEGIN CATCH
 IF (XACT_STATE()) = -1
    BEGIN
        PRINT
            N'The transaction is in an uncommittable state.' +
            'Rolling back transaction.'
        ROLLBACK TRANSACTION;
    END;

    -- Test whether the transaction is committable.
    IF (XACT_STATE()) = 1
    BEGIN
        PRINT
            N'The transaction is committable.' +
            'Committing transaction.'
        COMMIT TRANSACTION;  

    END;

END CATCH

CLOSE Contact
DEALLOCATE CONTACT 

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[IDIT_Obfuscated].[dbo].[AC_PMNT_INTERFACE_IN]') AND name = N'nci_AC_PMNT_INTERFACE_IN_Obf')
	DROP INDEX [nci_AC_PMNT_INTERFACE_IN_Obf] ON [IDIT_Obfuscated].[dbo].[AC_PMNT_INTERFACE_IN]

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[IDIT_Obfuscated].[dbo].[CN_CONTACT]') AND name = N'CN_CONTACT_temp_idx')
DROP INDEX [CN_CONTACT_temp_idx] ON [IDIT_Obfuscated].[dbo].[CN_CONTACT]

DECLARE @logicallogfilename nvarchar(max)
SELECT @logicallogfilename = name from sys.master_files
where file_id = 2 and database_id = (select database_id from sys.databases where name = @db)

SET @sql = 'USE ' + QUOTENAME(@db) + ';'
SET @sql = @sql + 'CHECKPOINT;'
EXEC (@sql)

SET @sql = 'USE ' + QUOTENAME(@db) + ';'
SET @sql = @sql + 'DBCC SHRINKFILE (N''' + @logicallogfilename + ''',0, TRUNCATEONLY);'
EXEC (@sql)

exec ObfuscationControl..sp_audit_log 'Finish', 'Randomised obfuscation complete for all ContactIDs'
exec ObfuscationControl..sp_audit_log 'Finish', 'All obfuscation complete!'
GO


