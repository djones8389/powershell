USE [msdb]

declare @enableAlerting nvarchar(max) = '';

select @enableAlerting +=CHAR(13) 
	+ ' EXEC msdb.dbo.sp_update_job @job_name=N'''+name+''', 
		@notify_level_email=2, 
		@notify_level_page=2, 
		@notify_email_operator_name=N''DBA Team''
	'
from msdb.dbo.sysjobs
where name like 'Job_Sync%'

EXEC(@enableAlerting);

USE [msdb]
GO
EXEC msdb.dbo.sp_update_operator @name=N'DBA Team', 
		@enabled=1, 
		@pager_days=0, 
		@email_address=N'!SQLAlerts@hiscox.com; !DevLMBUDTeam@HISCOX.com;', 
		@pager_address=N''
GO
