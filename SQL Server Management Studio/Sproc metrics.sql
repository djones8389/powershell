select db_name(db_id()) DBName
	, OBJECT_NAME(object_id) ProcName
	--, execution_count
	--, cached_time
	--, last_execution_time
	--, (total_elapsed_time/execution_count)/1000000 [AvgElapsedTime(seconds)]
	, (total_elapsed_time/execution_count)/1000000/60 [AvgElapsedTime(minutes)]
	, total_worker_time/execution_count/1000000/60 [AvgCPUTime (minutes)]	
	, total_logical_reads [AvgLogicalReads]
from sys.dm_exec_procedure_stats
where OBJECT_NAME(object_id) = 'p_ControlClaimTransfer'