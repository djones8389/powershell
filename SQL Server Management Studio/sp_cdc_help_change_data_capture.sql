declare @CDC table (
	[schema] varchar(max),
	srctable varchar(max),
	captins varchar(max),
	objectID varchar(max),
	srcObj varchar(max),
	startlsn varchar(max),
	endlsn varchar(max),
	supportnet varchar(max),
	hasdrop varchar(max),
	rolename varchar(max),
	idxname varchar(max),
	filegroupname varchar(max),
	createdate datetime,
	idxcollist varchar(max),
	capturedcollist varchar(max)
)
insert @CDC
exec('sys.sp_cdc_help_change_data_capture')


select *
from @CDC c
inner join sys.objects so
on so.object_id = c.objectID
where so.modify_date > '2018-10-13'
order by 2