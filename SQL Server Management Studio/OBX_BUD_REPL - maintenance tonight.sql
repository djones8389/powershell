use OBX_BUD_REPL;

DECLARE @Dynamic nvarchar(MAX) = '';

SELECT @Dynamic+=CHAR(13) +
	'ALTER INDEX ' + quotename(C.name) +  ' ON ' + quotename(SCHEMA_NAME(SCHEMA_ID)) + '.' + quotename(B.name) + ' REBUILD;'
FROM sys.dm_db_index_physical_stats(DB_ID(),NULL,NULL,NULL,NULL) A
INNER JOIN sys.objects B
ON A.object_id = B.object_id
INNER JOIN sys.indexes C
ON B.object_id = C.object_id AND A.index_id = C.index_id
INNER JOIN sys.partitions D
ON B.object_id = D.object_id AND A.index_id = D.index_id
WHERE C.index_id > 0
	and A.avg_fragmentation_in_percent > 5

EXEC(@Dynamic);

ALTER FULLTEXT CATALOG openbox_catalog REBUILD;

ALTER FULLTEXT INDEX ON [Contact]  START FULL POPULATION;
ALTER FULLTEXT INDEX ON [TypeOfRiskCode]  START FULL POPULATION;
ALTER FULLTEXT INDEX ON [TypeOfVessel]  START FULL POPULATION;

ALTER INDEX [pkBUDQuickSearch#Id] ON BUD.BUDQuickSearch REBUILD;

UPDATE STATISTICS BUD.BUDQuickSearch WITH FULLSCAN;

exec sp_updatestats;

