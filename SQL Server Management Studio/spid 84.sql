if exists(select 1 
from sys.databases 
where name = 'integration'
	and user_access_desc = 'SINGLE_USER'
)

declare  @dynamic nvarchar(max) = '';

select @dynamic += CHAR(13) + 
	 'kill ' + cast(spid as char(10))
	from sys.sysprocesses
where db_name(dbid) = 'integration'
	and spid > 51
print(@dynamic);

kill 84
ALTER DATABASE Integration SET MULTI_USER;

select *
from sys.sysprocesses
where dbid = 18

--HXP57315                                                                                                                        