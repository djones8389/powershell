USE [RAQ]
GO

/****** Object:  View [dbo].[SY_Policy]    Script Date: 18/01/2019 13:22:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--select * from RAQ.[dbo].[SY_Policy] where PolicyID in (276593, 259993)

ALTER view [dbo].[SY_Policy] as

with countries as 
(
	select [code]
		, Name
	from dbo.tblLookup
	where ListID = 'AccCountry' 
		and Deleted = 0
)

, statuses as 
(
	select [code]
		, Name
		, ExtraInfo1
	from dbo.tblLookup
	where ListID = 'Status' 
		and Deleted = 0
)

, MGAOffice as
(
	select	[Code],
			Name
	from	RAQ.dbo.tblLookup
	where	ListID = 'MGAOff'
		and Deleted = 0
)


select  
 p.PolicyID 
 , p.ContractID 
 , p.Status 
 , sg.SchemeGroupID
 , sg.SchemeGroupName 'SchemeGroup'
 , s.SchemeID
 , s.SchemeName 'Scheme'
 , p.ExtraInfo5 'Reinsurance' 
 , p.PolicyHolder  
 , p.CertificateNumber  
 , p.CoverFrom  
 , p.CoverTo 
 , p.CurrencyID 
 , p.SettlementCurrencyID
 , isnull(p.Address1, '') 
		+ case when rtrim(ltrim(ISNULL(p.Address2, ''))) <> '' then ', ' else '' end + isnull(p.Address2, '')
		+ case when rtrim(ltrim(ISNULL(p.Address3, ''))) <> '' then ', ' else '' end + isnull(p.Address3, '')
		+ case when rtrim(ltrim(ISNULL(p.Address4, ''))) <> '' then ', ' else '' end + isnull(p.Address4, '') 'Address'
 , p.Address1 
 , p.Address2 
 , p.Address3 
 , p.Address4 
 , p.PostCode 
 , pc.CountryCode
 , cny.Name 'Country'
 , tcny.ISOCode 'TaxCountryCode'
 , tcny.Name 'TaxCountry'
 , case when status.ExtraInfo1 = -1 and p.Status <> 'Referred' then 1 else 0 end IsWritten
 , p.AccountName COLLATE Latin1_General_CI_AS AccountName
 , cy.ContractYear
 , YEAR(cy.FromDate) UnderwritingYear
 , cy.ContractID + cy.ContractYear ContractYearID
 , p.RecordType
 , CASE 
	WHEN p.Status IS NULL 
			OR p.Status = ''
			OR p.RecordType = 'T'
		THEN 'Ignore'
	WHEN p.Status = 'Cancelled'
		THEN 'Cancelled'
	WHEN p.Status = 'Declined'
		THEN 'Declined'
	--WHEN p.Status = 'Reviewing'
	--		OR (p.Status IN ('Quote', 'Referred') 
	--			AND p.RecordType = 'N')
	WHEN p.Status IN ('Quote', 'Referred', 'Reviewing')
		THEN 'Pending'
	WHEN p.Status IN ('OnRisk', 'HeldCover', 'Renewed', 'Lapsed')
			--OR (p.Status IN ('Quote', 'Referred', 'HeldCover')
			--	AND p.RecordType = 'R')
		THEN 'OnRisk'
	WHEN p.Status IN ('NTU', 'Closed')
		THEN 'NTU'
	END COLLATE Latin1_General_CI_AS AS PolicyStatus
, CASE 
	WHEN p.Status IS NULL 
			OR p.Status = ''
			OR p.RecordType = 'T'
		THEN 'Ignore'
	WHEN p.RecordType = 'N'
		THEN 'NewBusiness'
	WHEN p.RecordType = 'R'
		THEN 'Renewal'
	END COLLATE Latin1_General_CI_AS AS PremiumStatus
, p.AgentID
, p.QuoteDate
, case when a.ExtraInfo4 is null or RTRIM(a.ExtraInfo4) = '' then 'Lon' else a.ExtraInfo4 end BranchID
, case when a.ExtraInfo4 is null or RTRIM(a.ExtraInfo4) = '' then 'London' else o.Name end Branch
 from dbo.tblPolicy p 
  join dbo.tblScheme s on p.SchemeID = s.SchemeID 
  join dbo.tblSchemeGroup sg on s.SchemeGroupID = sg.SchemeGroupID 
  left join ODS_RAQ_NonSystemData.dbo.PolicyCountry_Latest pc on p.PolicyID = pc.PolicyID
	and pc.LocationID = 0
  left join dbo.SY_Country cny on pc.CountryCode  = cny.[Code]  collate Latin1_General_CI_AS 
  left join dbo.SY_Country tcny on p.IPTCode = tcny.[Code]
  left join statuses status on p.Status = status.[Code]
  left join dbo.tblContractYear cy on p.ContractID = cy.ContractID
		and p.CoverFrom >= cy.FromDate
		and p.CoverFrom <= cy.ToDate
  left join dbo.tblAccount a on p.AgentID = a.AccountID
  left join MGAOffice o on a.ExtraInfo4 = o.Code
GO

select a.*
from (
select *
	, ROW_NUMBER() OVER(PARTITION BY Code ORDER BY Code) R
from dbo.SY_Country
) a
where R > 1

select Deleted, Code,Name 
from tblLookup where code = 'mtx'


select *
from dbo.tblLookup 
where ListID = 'AccCountry' 
