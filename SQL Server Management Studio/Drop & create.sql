USE [OPENBOX]
GO

/****** Object:  Table [ALT].[LBSSCM]    Script Date: 07/11/2018 10:53:48 ******/
DROP TABLE [ALT].[LBSSCM]
GO

/****** Object:  Table [ALT].[LBSSCM]    Script Date: 07/11/2018 10:53:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [ALT].[LBSSCM](
	[LBSSCMKey] [int] IDENTITY(1,1) NOT NULL,
	[lClaimMovementKey] [int] NOT NULL,
	[sBPRDirect] [varchar](50) NULL,
	[sBPRRI] [varchar](50) NULL,
	[sBrokerRef1Direct] [varchar](50) NULL,
	[sBrokerRef1RI] [varchar](50) NULL,
	[sBrokerRef2Direct] [varchar](50) NULL,
	[sBrokerRef2RI] [varchar](50) NULL,
	[lBureauLeadDirect] [int] NULL,
	[lBureauLeadRI] [int] NULL,
	[lCarrierCodeDirect] [int] NULL,
	[lCarrierCodeRI] [int] NULL,
	[sCarrierPseudonymnDirect] [varchar](5) NULL,
	[sCarrierPseudonymnRI] [varchar](5) NULL,
	[sCarrierRefDirect] [varchar](50) NULL,
	[sCarrierRefRI] [varchar](50) NULL,
	[dCarrierShareDirect] [decimal](18, 7) NULL,
	[dCarrierShareRI] [decimal](18, 7) NULL,
	[dClaimAmountDirect] [decimal](18, 2) NULL,
	[dClaimAmountRI] [decimal](18, 2) NULL,
	[sCORDirect] [varchar](50) NULL,
	[sCORRI] [varchar](50) NULL,
	[sCountryOfOriginDirect] [varchar](50) NULL,
	[sCountryOfOriginRI] [varchar](50) NULL,
	[nCurrentBrokerCodeDirect] [int] NULL,
	[nCurrentBrokerCodeRI] [int] NULL,
	[sFilCode1Direct] [varchar](10) NULL,
	[sFilCode1RI] [varchar](10) NULL,
	[sFilCode2Direct] [varchar](10) NULL,
	[sFilCode2RI] [varchar](10) NULL,
	[sInsuredDirect] [varchar](150) NULL,
	[sInsuredRI] [varchar](150) NULL,
	[sMovementRefDirect] [varchar](10) NULL,
	[sMovementRefRI] [varchar](10) NULL,
	[sOriginalInsuredDirect] [varchar](150) NULL,
	[sOriginalInsuredRI] [varchar](150) NULL,
	[sBrokerDirect] [varchar](50) NULL,
	[sBrokerRI] [varchar](50) NULL,
	[sOriginalCurrencyDirect] [varchar](5) NULL,
	[sOriginalCurrencyRI] [varchar](5) NULL,
	[nOSNDDIrect] [bigint] NULL,
	[nOSNDRI] [bigint] NULL,
	[sPayeeBrokerDirect] varchar(5) NULL,
	[sPayeeBrokerRI] varchar(5) NULL,
	[sReinsuredDirect] [varchar](150) NULL,
	[sReinsuredRI] [varchar](150) NULL,
	[sRiskCodeDirect] [varchar](5) NULL,
	[sRiskCodeRI] [varchar](5) NULL,
	[nTDNDDirect] [bigint] NULL,
	[nTDNDRI] [bigint] NULL,
	[sTRDirect] [varchar](50) NULL,
	[sTRRI] [varchar](50) NULL,
	[sUCRDIrect] [varchar](50) NULL,
	[sUCRRI] [varchar](50) NULL,
	[sUMRDirect] [varchar](50) NULL,
	[sUMRRI] [varchar](50) NULL,
	[sExactMatchIndicator] [varchar](50) NULL,
 CONSTRAINT [PK_LBSSCM] PRIMARY KEY CLUSTERED 
(
	[LBSSCMKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


