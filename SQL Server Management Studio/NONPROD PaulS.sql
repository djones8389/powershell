use master

exec sp_MSforeachdb '

use [?];

if (db_id() > 4)

begin
	
	IF NOT EXISTS(SELECT 1 FROM sys.database_principals WHERE name = ''NONPROD\PaulS'')
	begin
		CREATE USER [NONPROD\PaulS] FOR LOGIN [NONPROD\PaulS]
	end
		exec sp_addrolemember ''db_datareader'', [NONPROD\PaulS];
		exec sp_addrolemember ''db_datawriter'', [NONPROD\PaulS];

end
'