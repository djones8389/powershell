<?query --
SELECT
	vHPAS.sStatus AS 'Combined Status'
	,Case
		When p.lmasterpolicysectionkey is not null Then ps2.sSectionReference
		Else ''
	End as 'Master Policy Section'
	,ps.sSectionReference AS 'UW Reference'
	,ISNULL(toa.sAsheetCode, '') AS 'Business Area'
	,ISNULL(c1.sContactReference, '') AS 'Insured'
	,tos.sCode AS 'COB'
	,mop.sCode AS 'MOP'
	,pa.nVersion AS 'Active PA Version'
	,pa.dtPeriodFrom as 'Inception Date'
	,ISNULL(c2.sContactReference, '') AS 'Principal UW'
	,ast.dtTransition AS 'UWSO Date'
	,su1.sforename + ' ' + su1.ssurname AS 'UWSO Status By'
FROM
	PolicyFolder AS pf
	INNER JOIN Policy AS p ON p.lPolicyFolderKey = pf.lPolicyFolderKey
	INNER JOIN PolicyActivity AS pa ON pa.lPolicyActivityKey = p.lActivePolicyActivityKey
	INNER JOIN PolicySection AS ps ON ps.lPolicyActivityKey = pa.lPolicyActivityKey
	LEFT JOIN vHiscoxPolicyActivityStatus AS vHPAS ON vHPAS.lPolicyActivityKey = pa.lPolicyActivityKey
	LEFT JOIN Contact AS c1 ON c1.lContactKey = pa.lOriginalInsuredContactKey
	LEFT JOIN Contact AS c2 ON c2.lContactKey = ps.lUnderwriterContactKey
	LEFT JOIN TypeOfSection AS tos ON tos.lTypeOfSectionKey = ps.lTypeOfSectionKey
	LEFT JOIN TypeOfAsheet AS toa ON toa.lTypeOfAsheetKey = ps.lTypeOfAsheetKey
	LEFT JOIN ClientMOP AS mop ON mop.lClientMOPKey = pa.lTypeOfClientMOPKey
	INNER JOIN AuditStateTransitions AS ast ON ast.lAuditStateTransitionKey =
               (SELECT     MAX(lAuditStateTransitionKey) AS Expr1
               FROM          AuditStateTransitions AS ast2
               WHERE      (lInstanceKey = pa.lPolicyActivityKey) AND (lEntityStateMemberKey = 
			   2336  --UWSO
			   ))
	INNER JOIN zygoV2its.dbo.users AS su1 ON su1.lZygoUserKey = ast.lSecurityUserKey
	LEFT JOIN PolicySection ps2 on ps2.lPolicySectionKey = p.lmasterpolicysectionkey
WHERE
	(
		ps.lTypeOfAsheetKey in
			(
			4	--AWAR	Aviation War
			,27	--PA	Personal Accident
			,29	--POLIT	Political Risk
			,32	--SPACE	Space
			,35	--TERR	Terrorism
			,46	--AVN	Aviation
			,48	--PAINS	PA Insurance
			,54	--APD	Auto Physical Damage
			,55	--AEW	Auto Extended Warranty
			,61	--POR	Portfolios
			,69	--PROD	Product Recall
			)
		OR
			(
			ps.lTypeOfAsheetkey = 	7	--CONT	Contingency
			AND
			pa.lTypeOfClientMOPKey <> 18	--R  HUL UK Based
			)
	)	
	AND
	vHPAS.sStatus = 'Live - Underwriter Sign Off Required'
ORDER BY
	ast.dtTransition
	,pf.lPolicyFolderKey
	,p.nYearOfAccount DESC
	,p.lPolicyKey DESC
	,'Active PA Version' DESC
	,ps.lPolicySectionKey
--?>