use SQL_Inventory;

DECLARE @dt AS DATETIME,
		@ScriptName VARCHAR(128) = 'UpsertFailedAgentJobs.ps1';
		 
IF DATENAME(dw,GETDATE()) = 'Monday' 
	SELECT @dt = CONVERT(DATETIME,DATEADD(hh,-72,GETDATE()));
ELSE 
	SELECT @dt = CONVERT(DATETIME,DATEADD(hh,-24,GETDATE()));

;with CTE as (
SELECT
	tb.instanceName AS InstanceName, 
	tb.hostname AS HostName, 
	tb.status AS [Status], 
	tb.lastUpdated,
	saj.jobName AS [Job], 
	faj.errorMessage AS [Error],
	CONVERT(DATETIME,faj.lastRunDate,120) AS [LastErrorDate],
	faj.eServiceReference AS [eServiceRef],
	ROW_NUMBER() OVER(PARTITION BY tb.instanceName, tb.hostname, tb.status, tb.lastUpdated ORDER BY tb.lastUpdated) R
FROM  (
	SELECT s.serverID
		,s.instanceName
		,h.hostname
		,s.STATUS
		,sh.LastUpdated
	FROM dbo.Servers s
	INNER JOIN dbo.Hosts h ON s.hostid = h.hostid
	LEFT JOIN dbo.udf_ScriptHistory(@ScriptName) sh ON sh.hostID = h.hostID
	WHERE serverState = 'Commissioned'

	UNION

	SELECT s.serverID
		,s.instanceName
		,c.sqlclustername
		,s.STATUS
		,sh.LastUpdated
	FROM dbo.Servers s
	INNER JOIN dbo.Clusters c ON c.clusterID = s.clusterID
	LEFT JOIN dbo.udf_ScriptHistory(@ScriptName) sh ON c.clusterID = sh.ClusterID
	WHERE serverState = 'Commissioned'
) TB 
	INNER JOIN dbo.FailedAgentJobs faj ON faj.serverID = tb.serverid
	INNER JOIN dbo.ServerAgentJobs saj ON faj.jobID = saj.jobID
WHERE faj.lastRunDate > @dt
	AND faj.lastRunDate = (SELECT MAX(lastRunDate)FROM dbo.FailedAgentJobs where jobID = faj.jobID)               
)
select distinct HostName
	, InstanceName
	, case when InstanceName = 'MSSQLSERVER' then HostName
	else HostName + '\' + InstanceName
	end as [connect]
	, e.Environment
	, Job
	, LastErrorDate
from CTE
INNER JOIN Environments e
on e.ShortCode = [Status]
order by e.Environment