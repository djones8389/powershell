select TextData
	, LEN(cast(TextData as nvarchar(max))) [QueryLength]
	, SUBSTRING(cast(TextData as nvarchar(max)),  charindex('@combinedstatus',cast(TextData as nvarchar(max))), 100) combinedstatus
	, SUBSTRING(cast(TextData as nvarchar(max)),  charindex('@searchText',cast(TextData as nvarchar(max))), 100) searchText
	--, charindex('@combinedstatus',cast(TextData as nvarchar(max)))
	, DATEDIFF(SECOND, StartTime, EndTime) [TimeTaken(s)]
	, Reads
	, CPU
from ::fn_trace_gettable('C:\QuickSearch.trc',default)
where TextData is not null
order by DATEDIFF(SECOND, StartTime, EndTime) desc 