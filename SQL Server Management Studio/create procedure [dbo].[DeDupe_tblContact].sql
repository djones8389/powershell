USE [ODS_RAQ]
GO

/****** Object:  StoredProcedure [dbo].[DeDupe_tblContact]    Script Date: 13/12/2018 17:23:01 ******/
DROP PROCEDURE [dbo].[DeDupe_tblContact]
GO

/****** Object:  StoredProcedure [dbo].[DeDupe_tblContact]    Script Date: 13/12/2018 17:23:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


	
		create procedure [dbo].[DeDupe_tblContact] (
			@Date date,
			@DuplicateCount int output
		) AS
		
		delete 
		from [ODS_RAQ].[dbo].[tblContact]
		where ods_DateFrom = ods_DateTo
			and ods_DateTo <> '31 Dec 2999'

		;with duplicates as (
			select count(*) ods_RecordCount, min(ods_DateFrom) min_ods_DateFrom, max(ods_DateFrom) max_ods_DateFrom, max(ods_DateTo) max_ods_DateTo
				,[ParentTable],[ParentKey],[ContactType],[ContactName],[AccountID],[PayeeName],[Address1],[Address2],[Address3],[Address4],[Postcode],[Salutation],[Telephone],[Fax],[Email],[TheirRef],[ExtraInfo1],[ExtraInfo2],[ExtraInfo3],[ExtraInfo4],[ExtraInfo5],[Rowguid],[ContactID]
			from [ODS_RAQ].[dbo].[tblContact]
			group by [ParentTable],[ParentKey],[ContactType],[ContactName],[AccountID],[PayeeName],[Address1],[Address2],[Address3],[Address4],[Postcode],[Salutation],[Telephone],[Fax],[Email],[TheirRef],[ExtraInfo1],[ExtraInfo2],[ExtraInfo3],[ExtraInfo4],[ExtraInfo5],[Rowguid],[ContactID]
			having COUNT(*) > 1
		)
		select A.[min_ods_DateFrom], A.[max_ods_DateFrom], A.[max_ods_DateTo], A.[ods_RecordCount]
			,A.[ParentTable],A.[ParentKey],A.[ContactType],A.[ContactName],A.[AccountID],A.[PayeeName],A.[Address1],A.[Address2],A.[Address3],A.[Address4],A.[Postcode],A.[Salutation],A.[Telephone],A.[Fax],A.[Email],A.[TheirRef],A.[ExtraInfo1],A.[ExtraInfo2],A.[ExtraInfo3],A.[ExtraInfo4],A.[ExtraInfo5],A.[Rowguid],A.[ContactID]
		into #temp
		from duplicates A
			join [ODS_RAQ].[dbo].[tblContact] B on A.[Rowguid] = B.[Rowguid]
				and A.[min_ods_DateFrom] <= B.[ods_DateFrom]
				and A.[max_ods_DateFrom] >= B.[ods_DateFrom]
		group by A.[min_ods_DateFrom], A.[max_ods_DateFrom], A.[max_ods_DateTo], A.[ods_RecordCount]
			,A.[ParentTable],A.[ParentKey],A.[ContactType],A.[ContactName],A.[AccountID],A.[PayeeName],A.[Address1],A.[Address2],A.[Address3],A.[Address4],A.[Postcode],A.[Salutation],A.[Telephone],A.[Fax],A.[Email],A.[TheirRef],A.[ExtraInfo1],A.[ExtraInfo2],A.[ExtraInfo3],A.[ExtraInfo4],A.[ExtraInfo5],A.[Rowguid],A.[ContactID]
		having Count(*) = A.[ods_RecordCount]
		
		if @@ROWCOUNT <> 0 begin
			delete A
			from [ODS_RAQ].[dbo].[tblContact] A
			join #temp B on A.[Rowguid] = B.[Rowguid]
				and (A.ods_DateFrom >= B.min_ods_DateFrom and A.ods_DateFrom <= B.max_ods_DateFrom)

			insert into [ODS_RAQ].[dbo].[tblContact] (
				ods_DateFrom, ods_DateTo,[ParentTable],[ParentKey],[ContactType],[ContactName],[AccountID],[PayeeName],[Address1],[Address2],[Address3],[Address4],[Postcode],[Salutation],[Telephone],[Fax],[Email],[TheirRef],[ExtraInfo1],[ExtraInfo2],[ExtraInfo3],[ExtraInfo4],[ExtraInfo5],[Rowguid],[ContactID]
			)
			select min_ods_DateFrom, max_ods_DateTo,[ParentTable],[ParentKey],[ContactType],[ContactName],[AccountID],[PayeeName],[Address1],[Address2],[Address3],[Address4],[Postcode],[Salutation],[Telephone],[Fax],[Email],[TheirRef],[ExtraInfo1],[ExtraInfo2],[ExtraInfo3],[ExtraInfo4],[ExtraInfo5],[Rowguid],[ContactID]
			from #temp 

			set @DuplicateCount = @@ROWCOUNT
		end else begin
			set @DuplicateCount = 0
		end


	
GO


