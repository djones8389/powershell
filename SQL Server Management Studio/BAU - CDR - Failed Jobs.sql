 USE [SQL_Inventory];

DECLARE @dt AS DATETIME,
		@ScriptName VARCHAR(128) = 'UpsertFailedAgentJobs.ps1';

IF DATENAME(dw,GETDATE()) = 'Monday'
	SELECT @dt = CONVERT(DATETIME,DATEADD(hh,-72,GETDATE()))
ELSE 
	SELECT @dt = CONVERT(DATETIME,DATEADD(hh,-24,GETDATE()))

;WITH tb(serverID,instanceName,hostName,status, LastUpdated)
AS
(
SELECT 
	s.serverID,s.instanceName,h.hostname, s.status, sh.LastUpdated
FROM 
	dbo.Servers s 
	INNER JOIN dbo.Hosts h ON s.hostid = h.hostid
	LEFT JOIN dbo.udf_ScriptHistory(@ScriptName) sh on sh.hostID = h.hostID
WHERE
	serverState = 'Commissioned'
	
UNION
SELECT 
	s.serverID,s.instanceName,c.sqlclustername, s.status, sh.LastUpdated
FROM 
	dbo.Servers s  
	INNER JOIN dbo.Clusters c ON c.clusterID = s.clusterID
	LEFT JOIN dbo.udf_ScriptHistory(@ScriptName) sh on c.clusterID = sh.ClusterID
WHERE
	serverState = 'Commissioned'
)

SELECT A.*
FROM (
SELECT
	tb.hostname AS HostName, 
	tb.instanceName AS InstanceName, 	
	CASE tb.status
		WHEN 'B' THEN 'Development'
		WHEN 'D' THEN 'UAT'
		WHEN 'E' THEN 'Staging'
		WHEN 'F' THEN 'LiveSupport'
		WHEN 'R' THEN 'DR'
		WHEN 'P' THEN 'Production'
	ELSE  tb.status
		END 
		 AS [Status], 	
	LEFT(saj.jobName,25) AS [Job], 
	CONVERT(DATETIME,faj.lastRunDate,120) AS [LastErrorDate],
	--tb.lastUpdated,
	CASE 
		WHEN tb.instanceName = 'MSSQLSERVER'
		 THEN tb.hostname
	 ELSE tb.hostname + '\' + tb.instanceName 
	END AS [QuickCheck],
	faj.errorMessage AS [Error],
	faj.eServiceReference AS [eServiceRef],
	ROW_NUMBER() OVER (PARTITION BY tb.hostname, tb.instanceName ORDER BY tb.hostname, tb.instanceName) R
FROM 
	tb 
	INNER JOIN dbo.FailedAgentJobs faj ON faj.serverID = tb.serverid
	INNER JOIN dbo.ServerAgentJobs saj ON faj.jobID = saj.jobID
WHERE faj.lastRunDate > @dt
	AND faj.lastRunDate = (SELECT MAX(lastRunDate)FROM [SQL_Inventory].[dbo].vw_FailedAgentJobs where jobID = faj.jobID)      
	AND tb.status IN (N'A',N'B',N'C',N'D',N'E',N'F',N'G',N'P',N'R')
) A
where R = 1