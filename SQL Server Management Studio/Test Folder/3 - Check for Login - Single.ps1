﻿Clear-Host;

#Set relative location 

    $Location = split-path -parent $MyInvocation.MyCommand.Definition;
    $storeErrors = ($Location + "\Errors");

    Set-Location $Location;

#Import my modules:  Connect-SQL, Check-DBALogin, Store-Output

    GCI "$Location\Library" | ?{$_.Extension -eq '.ps1'} | foreach {
        Import-Module $_.FullName
    };

#Connect to the server which contains the SQL instance that I want to query

    $srv = "hxe20276\bud_stg";
    
#The login I am interested in

    $DBALogin = "HISCOX\zSQLAdmin"          #Is HISCOX\zSQLAdmin there?
    
#Drop Errors folder (if exists) and create one

    if(Get-ChildItem | ?{$_.Name -eq "Errors"}) {
        Remove-Item -Path ($Location + "\Errors") -Recurse;
        New-Item -Path $Location -Name "Errors" -ItemType "Directory" | Out-Null;
    } else {
        New-Item -Path $Location -Name "Errors" -ItemType "Directory" | Out-Null;
    }

#Get SQL instances to report on
    
    $connect = (Connect-SQL -instance $srv -windowsAuthentication 1);                
    
    Check-DBALogin -connect $connect -DBALogin $DBALogin;
         
