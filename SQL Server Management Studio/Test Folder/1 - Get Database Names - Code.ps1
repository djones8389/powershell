﻿#Load assembly

    [Reflection.Assembly]::LoadWithPartialName("Microsoft.SqlServer.Smo") | Out-Null;

#Connect to your SQL instance

    $srv = New-Object Microsoft.SqlServer.Management.Smo.Server "hxe20276\bud_stg";
    $srv.ConnectionContext.LoginSecure = $True;
    $srv.ConnectionContext.Connect();    

#List databases

    $srv.Databases.Name;

#List only user databases

    ($srv.Databases | ?{$_.IsSystemObject -eq $false}).Name;