Function Store-Output {

[Cmdletbinding()]
    Param (
        [Parameter(Mandatory=$true, Position=1)] [String]$location
        , [Parameter(Mandatory=$true, Position=2)] [String]$instance
        , [Parameter(Mandatory=$true, Position=3)] [String]$message
        , [Parameter(Mandatory=$false, Position=4)] [String]$severity
    )

        Try {
            $filePath = ($location+"\"+$connectionString+".txt");
            $message | Out-File ($filePath) -Append;
        }
        Catch {
                Throw New-Object System.Exception("Unable to create new file to" -f $filePath);
        }
         
}      

