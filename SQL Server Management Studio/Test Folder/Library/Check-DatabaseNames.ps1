Function Check-DatabaseNames {

[Cmdletbinding()]
    Param(
        [Parameter(Mandatory=$true, Position=1)] $srv
		,[Parameter(Mandatory=$false, Position=2)] $includeSystemDbs
	
    )


	
    Begin {    
        [Reflection.Assembly]::LoadWithPartialName("Microsoft.SqlServer.Smo") | Out-Null;
    }


    Process {	

		
		if($includeSystemDbs -eq 0) {
			$databases = ($srv.Databases | ?{$_.IsSystemObject -eq $false}).Name
		}
		else {
			$databases = $srv.Databases.Name;
		}
		
		return $databases;
	}
	
	

}

