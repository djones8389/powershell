﻿#Set relative location 

    $Location = split-path -parent $MyInvocation.MyCommand.Definition;
    Set-Location $Location;

#Load assembly

    [Reflection.Assembly]::LoadWithPartialName("Microsoft.SqlServer.Smo") | Out-Null;


#Load cmdlets
   
    Get-ChildItem "$Location\Library" | foreach {
        Import-Module $_.FullName
    };

#Connect to your SQL instance

    $srv = (Connect-SQL -instance "hxe20276\bud_stg" -windowsAuthentication 1);

    Check-DatabaseNames -srv $srv -includeSystemDbs 1 -
   
