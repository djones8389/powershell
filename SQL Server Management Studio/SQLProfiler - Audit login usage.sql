/****************************************************/
/* Created by: SQL Server 2017 Profiler          */
/* Date: 12/03/2019  14:56:49         */
/***************************************************

�	hxp33445.hiscox.com			E:\TraceFile-DaveJ\Trace
�	hxp33446.hiscox.com			E:\SQL_RND_01\TraceFile-DaveJ\Trace
�	modsq35.hiscoxbm.local		D:\SQL_TempDB_001\TraceFile-DaveJ\Trace
�	hxp20250.hiscox.com\UK02    F:\SQL_RND_01\TraceFile-DaveJ\Trace
�	pr0603-08001-00.hiscox.com  E:\SQL_TempDB_01\TraceFile-DaveJ\Trace

--select * from sys.traces
--exec sp_trace_setstatus 1,0
--exec sp_trace_setstatus 1,2

select *
from ::fn_trace_gettable('F:\SQL_RND_01\TraceFile-DaveJ\Trace.trc',default)

*/

declare @Stoptime datetime = (SELECT DATEADD(DAY,7, getdate()));
declare @maxfilesize bigint = 50  --50 MB 
declare @filecount int = 10;   --10 files maximum
declare @login sysname = 'remote_prod-claimsmi-db'
declare @location nvarchar(200)= 'E:\SQL_TempDB_01\TraceFile-DaveJ\Trace';
declare @rc int
declare @TraceID int

-- Please replace the text InsertFileNameHere, with an appropriate
-- filename prefixed by a path, e.g., c:\MyFolder\MyTrace. The .trc extension
-- will be appended to the filename automatically. If you are writing from
-- remote server to local drive, please use UNC path and make sure server has
-- write access to your network share

exec @rc = sp_trace_create @TraceID output, 2, @location, @maxfilesize, @Stoptime, @filecount
if (@rc != 0) goto error

-- Client side File and Table cannot be scripted

-- Set the events
declare @on bit
set @on = 1
exec sp_trace_setevent @TraceID, 10, 1, @on
exec sp_trace_setevent @TraceID, 10, 10, @on
exec sp_trace_setevent @TraceID, 10, 6, @on
exec sp_trace_setevent @TraceID, 10, 8, @on
exec sp_trace_setevent @TraceID, 10, 11, @on
exec sp_trace_setevent @TraceID, 10, 12, @on
exec sp_trace_setevent @TraceID, 10, 14, @on
exec sp_trace_setevent @TraceID, 10, 15, @on
exec sp_trace_setevent @TraceID, 10, 26, @on
exec sp_trace_setevent @TraceID, 10, 35, @on
exec sp_trace_setevent @TraceID, 10, 64, @on
exec sp_trace_setevent @TraceID, 12, 1, @on
exec sp_trace_setevent @TraceID, 12, 11, @on
exec sp_trace_setevent @TraceID, 12, 6, @on
exec sp_trace_setevent @TraceID, 12, 8, @on
exec sp_trace_setevent @TraceID, 12, 10, @on
exec sp_trace_setevent @TraceID, 12, 12, @on
exec sp_trace_setevent @TraceID, 12, 14, @on
exec sp_trace_setevent @TraceID, 12, 15, @on
exec sp_trace_setevent @TraceID, 12, 26, @on
exec sp_trace_setevent @TraceID, 12, 35, @on
exec sp_trace_setevent @TraceID, 12, 64, @on
exec sp_trace_setevent @TraceID, 13, 1, @on
exec sp_trace_setevent @TraceID, 13, 11, @on
exec sp_trace_setevent @TraceID, 13, 6, @on
exec sp_trace_setevent @TraceID, 13, 8, @on
exec sp_trace_setevent @TraceID, 13, 10, @on
exec sp_trace_setevent @TraceID, 13, 12, @on
exec sp_trace_setevent @TraceID, 13, 14, @on
exec sp_trace_setevent @TraceID, 13, 26, @on
exec sp_trace_setevent @TraceID, 13, 35, @on
exec sp_trace_setevent @TraceID, 13, 64, @on


-- Set the Filters
declare @intfilter int
declare @bigintfilter bigint

exec sp_trace_setfilter @TraceID, 10, 0, 7, N'SQL Server Profiler - 904d90dc-6490-4c94-a2b0-76d2db89cb04'
exec sp_trace_setfilter @TraceID, 11, 0, 6, @login
-- Set the trace status to start
exec sp_trace_setstatus @TraceID, 1

-- display trace id for future references
select TraceID=@TraceID
goto finish

error: 
select ErrorCode=@rc

finish: 
go
