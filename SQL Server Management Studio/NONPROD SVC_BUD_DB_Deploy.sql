--For the BUD and BUDPricing Server

	use [master];

	if not exists (
		select 1
		from sys.syslogins
		where name = 'NONPROD\SVC_BUD_DB_Deploy'
	)
 
	CREATE LOGIN [NONPROD\SVC_BUD_DB_Deploy] FROM WINDOWS WITH DEFAULT_DATABASE=[master]

	GRANT ALTER ANY AVAILABILITY GROUP TO [NONPROD\SVC_BUD_DB_Deploy]

	ALTER SERVER ROLE [dbcreator] ADD MEMBER [NONPROD\SVC_BUD_DB_Deploy]
	GO
	ALTER SERVER ROLE [processadmin] ADD MEMBER [NONPROD\SVC_BUD_DB_Deploy]
	GO


	USE [msdb]
	GO
	ALTER ROLE [SQLAgentOperatorRole] ADD MEMBER [NONPROD\SVC_BUD_DB_Deploy]
	GO
	USE [msdb]
	GO
	ALTER ROLE [SQLAgentReaderRole] ADD MEMBER [NONPROD\SVC_BUD_DB_Deploy]
	GO
	USE [msdb]
	GO
	ALTER ROLE [SQLAgentUserRole] ADD MEMBER [NONPROD\SVC_BUD_DB_Deploy]
	GO

	if exists (

	SELECT   name 
	FROM     master.sys.server_principals 
	WHERE    IS_SRVROLEMEMBER ('sysadmin',name) = 1
		and name = 'NONPROD\SVC_BUD_DB_Deploy'
	)
	ALTER SERVER ROLE [sysadmin] DROP MEMBER [NONPROD\SVC_BUD_DB_Deploy]
	GO


--For the CRM Server

	use [master];

	if not exists (
		select 1
		from sys.syslogins
		where name = 'NONPROD\SVC_BUD_DB_Deploy'
	)
 
	CREATE LOGIN [NONPROD\SVC_BUD_DB_Deploy] FROM WINDOWS WITH DEFAULT_DATABASE=[master];

	ALTER SERVER ROLE [sysadmin] ADD MEMBER [NONPROD\SVC_BUD_DB_Deploy]
	GO


--For the OpenBox Server
	
	use [master];

	DECLARE @OBX NVARCHAR(MAX)='';

	select @OBX +=CHAR(13)+
	'use '+QUOTENAME(name)+';
	if not exists(select 1 from sys.database_principals where name = ''NONPROD\SVC_BUD_DB_Deploy'')
		create user [NONPROD\SVC_BUD_DB_Deploy] for login [NONPROD\SVC_BUD_DB_Deploy]

		exec sp_addrolemember ''db_owner'',''NONPROD\SVC_BUD_DB_Deploy''
		'
	from sys.databases 
	where name in ('OPENBOX_UAT','OPENBOX', 'zygoV2its')

	EXEC(@OBX);


	if exists (

	SELECT   name 
	FROM     master.sys.server_principals 
	WHERE    IS_SRVROLEMEMBER ('sysadmin',name) = 1
		and name = 'NONPROD\SVC_BUD_DB_Deploy'
	)
	ALTER SERVER ROLE [sysadmin] DROP MEMBER [NONPROD\SVC_BUD_DB_Deploy]
	GO
