USE [master]

DECLARE @DBName sysname = 'IDIT';
DECLARE @DataRestoreLocation nvarchar(MAX)= (select physical_name from sys.master_files where db_name(database_id) = @DBName and type_desc = 'ROWS')
DECLARE @DataLogicalName nvarchar(MAX)= (select name from sys.master_files where db_name(database_id) = @DBName and type_desc = 'ROWS')
DECLARE @LogRestoreLocation nvarchar(MAX)= (select physical_name from sys.master_files where db_name(database_id) = @DBName and type_desc = 'LOG');
DECLARE @LogLogicalName nvarchar(MAX)= (select name from sys.master_files where db_name(database_id) = @DBName and type_desc = 'LOG');
DECLARE @AG nvarchar(50) = (select name from sys.availability_groups);

declare  @dynamic nvarchar(max) = '';

select @dynamic += CHAR(13) +
        'kill ' + cast(spid as char(10))
       from sys.sysprocesses
where db_name(dbid) = 'IDIT'

exec(@dynamic);

declare @AddToAG NVARCHAR(MAX) = '';
select @AddToAG += CHAR(13) +  'ALTER AVAILABILITY GROUP ' + quotename(@AG) + ' ADD DATABASE '+quotename(@DBName)+';'
print (@AddToAG);


declare @RemoveFromAG NVARCHAR(MAX) = '';
select @RemoveFromAG += CHAR(13) + 'ALTER AVAILABILITY GROUP ' + quotename(@AG) + ' REMOVE DATABASE '+quotename(@DBName)+';'
exec (@RemoveFromAG);

ALTER AVAILABILITY GROUP [availabilitygroup] REMOVE DATABASE [IDIT]; 
RESTORE DATABASE IDIT
FROM DISK = 'E:\SQL_Backups\IDIT_Obfuscated_2019-01-25.bak'
WITH FILE = 1, MOVE @DataLogicalName TO @DataRestoreLocation,
                     MOVE @LogLogicalName TO @LogRestoreLocation
, RECOVERY, NOUNLOAD, REPLACE, NOREWIND, STATS = 1

BACKUP LOG IDIT TO DISK ='NUL';

declare @AddToAG1 NVARCHAR(MAX) = '';
select @AddToAG1 += CHAR(13) +  'ALTER AVAILABILITY GROUP ' + quotename(@AG) + ' ADD DATABASE '+quotename(@DBName)+';'
exec (@AddToAG1);


--ALTER AVAILABILITY GROUP [availabilitygroup] ADD DATABASE [IDIT];