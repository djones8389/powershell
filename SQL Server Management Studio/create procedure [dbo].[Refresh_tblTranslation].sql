USE [ODS_RAQ]
GO

/****** Object:  StoredProcedure [dbo].[Refresh_tblTranslation]    Script Date: 14/12/2018 08:31:51 ******/
DROP PROCEDURE [dbo].[Refresh_tblTranslation]
GO

/****** Object:  StoredProcedure [dbo].[Refresh_tblTranslation]    Script Date: 14/12/2018 08:31:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


	
		create procedure [dbo].[Refresh_tblTranslation] (
			@Date date,
			@updatecount int output,
			@InsertCount int output
		) AS
	
		DECLARE @temp table(
			[LanguageID] varchar (5) COLLATE SQL_Latin1_General_CP1_CI_AS,[Match] nvarchar (510) COLLATE SQL_Latin1_General_CP1_CI_AS)
			
		insert into @temp (
			[LanguageID],[Match]
		)
		select A.[LanguageID]  , A.[Match] 
			from [RAQ].[dbo].[tblTranslation] A
			where not exists (
				select *
				from [ODS_RAQ].[dbo].[tblTranslation] B 
				where ods_DateTo = '31 dec 2999' and A.[LanguageID]  COLLATE SQL_Latin1_General_CP1_CI_AS = B.[LanguageID]  COLLATE SQL_Latin1_General_CP1_CI_AS and A.[Match]  COLLATE SQL_Latin1_General_CP1_CI_AS = B.[Match]  COLLATE SQL_Latin1_General_CP1_CI_AS
				and (((A.[LanguageID]  COLLATE SQL_Latin1_General_CP1_CI_AS IS NULL AND B.[LanguageID]  COLLATE SQL_Latin1_General_CP1_CI_AS IS NULL) OR (A.[LanguageID]  COLLATE SQL_Latin1_General_CP1_CI_AS = B.[LanguageID]  COLLATE SQL_Latin1_General_CP1_CI_AS)) and ((A.[Match]  COLLATE SQL_Latin1_General_CP1_CI_AS IS NULL AND B.[Match]  COLLATE SQL_Latin1_General_CP1_CI_AS IS NULL) OR (A.[Match] = B.[Match]  COLLATE SQL_Latin1_General_CP1_CI_AS)) and ((A.[Replace]  COLLATE SQL_Latin1_General_CP1_CI_AS IS NULL AND B.[Replace]  COLLATE SQL_Latin1_General_CP1_CI_AS IS NULL) OR (A.[Replace]  COLLATE SQL_Latin1_General_CP1_CI_AS = B.[Replace]  COLLATE SQL_Latin1_General_CP1_CI_AS)) and ((A.[UserName]  COLLATE SQL_Latin1_General_CP1_CI_AS IS NULL AND B.[UserName]  COLLATE SQL_Latin1_General_CP1_CI_AS IS NULL) OR (A.[UserName]  COLLATE SQL_Latin1_General_CP1_CI_AS = B.[UserName]  COLLATE SQL_Latin1_General_CP1_CI_AS)) and ((A.[DateEntered] IS NULL AND B.[DateEntered] IS NULL) OR (A.[DateEntered] = B.[DateEntered])) and ((A.[Rowguid] IS NULL AND B.[Rowguid] IS NULL) OR (A.[Rowguid] = B.[Rowguid])))
			)
			
		insert into @temp (
			[LanguageID],[Match]
		)
		select A.[LanguageID]  , A.[Match] 
			from [ODS_RAQ].[dbo].[tblTranslation] A
			where ods_DateTo = '31 dec 2999'
			and not exists (
				select *
				from [RAQ].[dbo].[tblTranslation] B 
				where A.[LanguageID] COLLATE SQL_Latin1_General_CP1_CI_AS = B.[LanguageID] COLLATE SQL_Latin1_General_CP1_CI_AS and A.[Match] COLLATE SQL_Latin1_General_CP1_CI_AS = B.[Match] COLLATE SQL_Latin1_General_CP1_CI_AS
			)
			
		if not exists (
			select *
			from @temp
		) begin
			set @updatecount = 0
			set @InsertCount = 0
			return
		end	
		
		if exists (
			select *
			from [ODS_RAQ].[dbo].[tblTranslation]
			where ods_DateFrom > @Date) begin
						
				RAISERROR ('Future data exists in tblTranslation',11,1)
				return
		end
		
		update A
		set A.ods_DateTo = @Date
		from [ODS_RAQ].[dbo].[tblTranslation] A
		join @temp B on A.[LanguageID] COLLATE SQL_Latin1_General_CP1_CI_AS = B.[LanguageID] COLLATE SQL_Latin1_General_CP1_CI_AS and A.[Match]  COLLATE SQL_Latin1_General_CP1_CI_AS = B.[Match] COLLATE SQL_Latin1_General_CP1_CI_AS
		where A.ods_DateTo = '31 dec 2999'
		
		set @updatecount = @@ROWCOUNT
		
		insert into [ODS_RAQ].[dbo].[tblTranslation] (
			ods_DateFrom, ods_DateTo, [LanguageID],[Match],[Replace],[UserName],[DateEntered],[Rowguid]
		)
		select @Date , '31 dec 2999', A.[LanguageID],A.[Match],A.[Replace],A.[UserName],A.[DateEntered],A.[Rowguid]
		from [RAQ].[dbo].[tblTranslation] A	
		join @temp B on A.[LanguageID] = B.[LanguageID] and A.[Match] = B.[Match]
		
		set @InsertCount = @@ROWCOUNT

	
GO


