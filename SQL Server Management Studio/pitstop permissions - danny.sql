--HISCOX\Pitstop_Devops

--staging-idit + idit_live_supp

--DBCreator										done
--MSDB - view, alter, run jobs					done
--ALTER ANY AVAILABILITY GROUP (remove AG)		done
--DROP database									done
--SQL Profiler									done
--DML											done
--Backup dbs									done
--SP's - view them, execute them				done


/*add this into a post-restore step*/
 


use [master];

--Create Login

	if not exists (
		select 1
		from sys.syslogins
		where name = 'HISCOX\Pitstop_Devops'
	)

	CREATE LOGIN [HISCOX\Pitstop_Devops] 
	FROM WINDOWS WITH DEFAULT_DATABASE=[master];

--Add and drop from AG

	GRANT ALTER ANY AVAILABILITY GROUP TO [HISCOX\Pitstop_Devops];

--Run SQL profiler

	GRANT VIEW SERVER STATE TO [HISCOX\Pitstop_Devops];

--Create, alter, drop, restore databases

	ALTER SERVER ROLE [dbcreator] ADD MEMBER [HISCOX\Pitstop_Devops];
	
--Kill SPIDs

	ALTER SERVER ROLE [processadmin] ADD MEMBER [HISCOX\Pitstop_Devops];
	

--View, alter, run SQL Agent jobs

	USE [msdb];
	GO

	if not exists (
		select 1
		from sys.database_principals
		where name = 'HISCOX\Pitstop_Devops'
	)
	BEGIN
		CREATE USER [HISCOX\Pitstop_Devops] FOR LOGIN [HISCOX\Pitstop_Devops]
	END 

	ALTER ROLE [SQLAgentOperatorRole] ADD MEMBER [HISCOX\Pitstop_Devops];
	GO
	USE [msdb]
	GO
	ALTER ROLE [SQLAgentReaderRole] ADD MEMBER [HISCOX\Pitstop_Devops];
	GO
	USE [msdb]
	GO
	ALTER ROLE [SQLAgentUserRole] ADD MEMBER [HISCOX\Pitstop_Devops];
	GO

--If this has sysadmin, remove the sysadmin role

use master;

	if exists (

	SELECT   name 
	FROM     master.sys.server_principals 
	WHERE    IS_SRVROLEMEMBER ('sysadmin',name) = 1
		and name = 'HISCOX\Pitstop_Devops'
	)
	ALTER SERVER ROLE [sysadmin] DROP MEMBER [HISCOX\Pitstop_Devops];
	GO


/*
--DML
--Backup dbs
--SP's - view them, execute them
*/

exec sp_MSforeachdb '

use [?];

if(db_ID()>4)

BEGIN
	
	if not exists (
		select 1
		from sys.database_principals
		where name = ''HISCOX\Pitstop_Devops''
	)
	BEGIN
		CREATE USER [HISCOX\Pitstop_Devops] FOR LOGIN [HISCOX\Pitstop_Devops]
	END 

	exec sp_addrolemember ''db_datareader'',''HISCOX\Pitstop_Devops''
	exec sp_addrolemember ''db_datawriter'',''HISCOX\Pitstop_Devops''
	exec sp_addrolemember ''db_backupoperator'',''HISCOX\Pitstop_Devops''

	DECLARE @GrantSP nvarchar(MAx)='''';
  
	select @GrantSP+=CHAR(13) + ''GRANT ALTER, EXECUTE, VIEW DEFINITION ON '' + QUOTENAME(SPECIFIC_SCHEMA) + ''.'' + QUOTENAME(SPECIFIC_NAME) + '' TO [HISCOX\Pitstop_Devops];''   
	from information_schema.routines
	where routine_type = ''PROCEDURE'';

	EXEC(@GrantSP);

END
'

--use CDR_Local;

--GRANT EXECUTE ON [sp_whoisactive] TO [HISCOX\Pitstop_Devops];