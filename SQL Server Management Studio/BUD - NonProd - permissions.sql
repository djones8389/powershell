if not exists (select 1 from sys.syslogins where name = 'NONPROD\BUD_DB_OWNER')
	CREATE LOGIN [NONPROD\BUD_DB_OWNER] FROM WINDOWS WITH DEFAULT_DATABASE=[master]
	 
use master;

	GRANT VIEW SERVER STATE TO [NONPROD\BUD_DB_OWNER];
	GRANT ALTER TRACE TO [NONPROD\BUD_DB_OWNER];

exec sp_msforeachdb '

use [?]

if(db_ID() > 4)

begin

	if not exists (select 1 from sys.database_principals where name = ''NONPROD\BUD_DB_OWNER'')
			begin
			create user [NONPROD\BUD_DB_OWNER] for login [NONPROD\BUD_DB_OWNER]
			exec sp_addrolemember ''db_owner'',''NONPROD\BUD_DB_OWNER''
			end
			else
				exec sp_addrolemember ''db_owner'',''NONPROD\BUD_DB_OWNER''
end
'

use msdb;

	if not exists (select 1 from sys.database_principals where name = 'NONPROD\BUD_DB_OWNER')
		begin
		create user [NONPROD\BUD_DB_OWNER] for login [NONPROD\BUD_DB_OWNER]
		exec sp_addrolemember 'SQLAgentReaderRole','NONPROD\BUD_DB_OWNER'
		end
	else 
		begin
			exec sp_addrolemember 'SQLAgentReaderRole','NONPROD\BUD_DB_OWNER'
		end


use CDR_Local;

	if not exists (select 1 from sys.database_principals where name = 'NONPROD\BUD_DB_OWNER')
		begin
		create user [NONPROD\BUD_DB_OWNER] for login [NONPROD\BUD_DB_OWNER]
		GRANT EXECUTE ON [dbo].[sp_WhoIsActive] TO [NONPROD\BUD_DB_OWNER]
		end
	else 
		begin
			GRANT EXECUTE ON [dbo].[sp_WhoIsActive] TO [NONPROD\BUD_DB_OWNER]
		end