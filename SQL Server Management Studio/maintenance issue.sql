/****** Script for SelectTopNRows command from SSMS  ******/
SELECT [DatabaseName]
      --,[SchemaName]
      ,[ObjectName]
      --,[ObjectType]
      ,[IndexName]
      ,case [IndexType] when 1 then 'Clustered' else 'NonClustered' end as [IndexType]
      --,[StatisticsName]
      --,[PartitionNumber]
      ,[ExtendedInfo].value('(ExtendedInfo/PageCount)[1]','smallint') PageCount
	  ,[ExtendedInfo].value('(ExtendedInfo/Fragmentation)[1]','decimal(8,2)') Fragmentation
      ,[Command]
      --,[CommandType]
      ,[StartTime]
      ,[EndTime]
	  , DATEDIFF(minute, starttime,endtime)
      --,[ErrorNumber]
      --,[ErrorMessage]
  FROM [CDR_Local].[dbo].[CommandLog]
  where CommandType = 'ALTER_INDEX'
	--and Command like '%REBUILD%'
  order by StartTime DESC;

--ALTER INDEX [idx_Session_8] ON [integration].[dbo].[Session] REBUILD;
--ALTER INDEX [PK__Session__3213E83F71382573] ON [integration].[dbo].[Session] REBUILD; 

--UPDATE STATISTICS [dbo].[Session];