BEGIN TRANSACTION;

	CREATE TABLE #UWMapping (ContactKey INT, ContactName VARCHAR(100), ContactSecurityUserKey INT, MapToKey INT);

	WITH TestUWUsers
	AS
	 (SELECT su.lSecurityUserKey, su.sName
	  FROM zygoV2its.dbo.SecurityUsers su
	  WHERE su.sName IN ('DORAISAK', 'BALAMURV', 'VISWANAJ', 'REDDYJIP', 'RAMASAMS'))

	INSERT INTO #UWMapping
	SELECT c.lContactKey, c.sContactReference, c.lSecurityUserKey,
			CASE
				WHEN c.sContactReference = 'Hardaker, Colin' THEN (SELECT uw.lSecurityUserKey FROM TestUWUsers uw WHERE uw.sName = 'DORAISAK')
				WHEN c.sContactReference = 'Allen, Tim' THEN (SELECT uw.lSecurityUserKey FROM TestUWUsers uw WHERE uw.sName = 'BALAMURV')
				WHEN c.sContactReference = 'Jordan, Michael MJO' THEN (SELECT uw.lSecurityUserKey FROM TestUWUsers uw WHERE uw.sName = 'VISWANAJ')
				WHEN c.sContactReference = 'Kickham, Alex' THEN (SELECT uw.lSecurityUserKey FROM TestUWUsers uw WHERE uw.sName = 'REDDYJIP')
				WHEN c.sContactReference = 'Morgan, Simon SIM' THEN (SELECT uw.lSecurityUserKey FROM TestUWUsers uw WHERE uw.sName = 'RAMASAMS')
			END as 'MapToUser'
	FROM Contact c
	WHERE c.sContactReference IN ('Hardaker, Colin',  'Allen, Tim', 'Jordan, Michael MJO', 'Kickham, Alex', 'Morgan, Simon SIM')
	and c.lTypeofMainRoleKey = 11 -- Underwriter

	UPDATE Contact
	SET lSecurityUserKey = uwm.MapToKey
	FROM Contact con
	INNER JOIN #UWMapping uwm 
		ON con.lSecurityUserKey = uwm.ContactSecurityUserKey
	WHERE con.sContactReference IN ('Hardaker, Colin',  'Allen, Tim', 'Jordan, Michael MJO', 'Kickham, Alex', 'Morgan, Simon SIM')

COMMIT TRANSACTION;